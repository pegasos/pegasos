package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class TimeDiffMessage extends ServerResponse {
  private static final long serialVersionUID = 4996378324293599988L;
  
  private int diff;
  private long servert;
  private long recievet;
  private long diff2;

	public TimeDiffMessage(int code) {
		super(code);
		recievet= System.currentTimeMillis();
	}

	@Override
	public void setData(Object... data) {
		assert(data.length == 2);
		
		diff= Integer.parseInt((String) data[0]);
		servert= Long.parseLong((String) data[1]);
		diff2= servert - recievet;
	}
	
	public int getDiff() {
		return diff;
	}
	
	public long getServerTime() {
		return servert;
	}
	
	public long getTimeRecieved() {
	  return recievet;
	}
	
	public long getRecieveDiff() {
	  return diff2;
	}
}
