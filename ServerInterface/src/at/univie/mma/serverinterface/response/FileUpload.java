package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class FileUpload extends ServerResponse {
  
  /**
   * 
   */
  private static final long serialVersionUID= -8507993507121239563L;
  
  public enum Type {
    CreateUpload,
    DataChunk,
    FinishUpload
  }
  
  private String[] data;
  private Type type;
  
  public FileUpload(int code)
  {
    super(code);
  }

  @Override
  public void setData(Object... data)
  {
    int type= Integer.parseInt((String) data[0]);
    this.data= new String[data.length - 1];
    
    for(int i= 0; i < data.length - 1; i++)
    {
      this.data[i]= "" + data[i+1];
    }

    switch(type)
    {
      case 1:
        this.type= Type.CreateUpload;
        break;
      
      case 2:
        this.type= Type.DataChunk;
        break;
        
      case 3:
        this.type= Type.FinishUpload;
        break;
    }
  }
  
  public String[] getData()
  {
    return data;
  }

  public Type getType()
  {
    return type;
  }
}
