package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

import java.util.Arrays;

public class GroupActivity extends ServerResponse {
  private static final long serialVersionUID= 1L;

  public static class GroupActivityItem
  {
    /**
     * True if the group has been started
     */
    public boolean started;
    /**
     * ID of the Group
     */
    public long group_id;
    /**
     * Name of the group
     */
    public String group_name;
    /**
     * Number of members active in this group
     */
    public int count;
  }
  
  public class Team
  {
    /**
     * ID of the team
     */
    public long id;
    
    /**
     * Name of the team
     */
    public String name;
  }
  
  public class TeamMember
  {
    /**
     * ID of the team
     */
    public long team_id;
    
    /**
     * ID of the user
     */
    public long user_id;
    
    public String forename;
    
    public String surname;
  }
  
  public enum Type
  {
    UsersReady,
    GroupStarted,
    GroupStopped,
    SessionStarted,
    RoundStarted,
    RoundStopped,
    Teams,
    TeamMembers,
    TeamGroupMembers,
    Error
  }
  
  GroupActivityItem items[];
  
  Team teams[];
  
  TeamMember members[];
  
  private Type type;

  private long session_id= -1;

  private int p1;

  private long group;
  
  private String[] data;
  
  public GroupActivity(int code)
  {
    super(code);
  }
  
  @Override
  public void setData(Object... data)
  {
    int type= Integer.parseInt((String) data[0]);
    
    if( type == 1 ) // users ready
    {
      assert(data.length%3 == 2);
      
      this.type= Type.UsersReady;
      
      int count= Integer.parseInt((String) data[1]);
      
      items= new GroupActivityItem[count];
      
      System.out.println(Arrays.toString(data));
      
      int i= 2;
      int j= 0;
      
      while(j < count)
      {
        items[j]= new GroupActivityItem();
        items[j].group_id= Long.parseLong((String) data[i++]);
        if( items[j].group_id < 0 )
        {
          items[j].started= true;
          items[j].group_id= Math.abs(items[j].group_id);
        }
        else
          items[j].started= false;
        items[j].group_name= (String) data[i++];
        items[j].count= Integer.parseInt((String) data[i++]);
        j++;
      }
    }
    else if( type == 2 ) // started
    {
      this.type= Type.GroupStarted;
      
      session_id= Long.parseLong((String) data[1]);
      p1= Integer.parseInt((String) data[2]);
      group= Long.parseLong((String) data[3]);
    }
    else if( type == 3 ) // stopped
    {
      this.type= Type.GroupStopped;
      group= Long.parseLong((String) data[1]);
      p1= Integer.parseInt((String) data[2]);
    }
    else if( type == 4 ) // Module on Server started
    {
      this.type= Type.SessionStarted;
      session_id= Long.parseLong((String) data[1]);
    }
    else if( type == 5 ) // round started
    {
      this.type= Type.RoundStarted;
      group= Long.parseLong((String) data[1]);
      p1= Integer.parseInt((String) data[2]);
    }
    else if ( type == 6 ) // round stopped
    {
      this.type= Type.RoundStopped;
      group= Long.parseLong((String) data[1]);
      p1= Integer.parseInt((String) data[2]);
      this.data= (String[]) Arrays.copyOfRange(data, 3, data.length);
    }
    else if( type == 7 ) // teams
    {
      this.type= Type.Teams;
      
      int count= Integer.parseInt((String) data[1]);
      
      teams= new Team[count];
      
      int i= 2;
      int j= 0;
      
      while(j < count)
      {
        teams[j]= new Team();
        teams[j].id= Long.parseLong((String) data[i++]);
        teams[j].name= (String) data[i++];
        j++;
      }
    }
    else if( type == 8 ) // Module on Server started
    {
      this.type= Type.TeamMembers;

      int count= Integer.parseInt((String) data[1]);
      
      members= new TeamMember[count];
      
      int i= 2;
      int j= 0;
      // <team-id-1>;<user-id-1>;<forename-1>;<surname-1>
      while(j < count)
      {
        members[j]= new TeamMember();
        members[j].team_id= Long.parseLong((String) data[i++]);
        members[j].user_id= Long.parseLong((String) data[i++]);
        members[j].forename= (String) data[i++];
        members[j].surname= (String) data[i++];
        j++;
      }
    }
    else if( type == 9 ) // Module on Server started
    {
      this.type= Type.TeamGroupMembers;

      int count_teams= Integer.parseInt((String) data[1]);
      System.out.println("Nteams: " + count_teams);
      
      teams= new Team[count_teams];
      
      int i= 2;
      int j= 0;
      
      while(j < count_teams)
      {
        teams[j]= new Team();
        teams[j].id= Long.parseLong((String) data[i++]);
        teams[j].name= (String) data[i++];
        System.out.println(teams[j].id + " " + teams[j].name);
        j++;
      }
      
      int count_assignments= Integer.parseInt((String) data[i++]);
      System.out.println("Nassign: " + count_assignments);
      
      members= new TeamMember[count_assignments];
      
      j= 0;
      // <team-id-1>;<user-id-1>;
      while(j < count_assignments)
      {
        members[j]= new TeamMember();
        members[j].team_id= Long.parseLong((String) data[i++]);
        members[j].user_id= Long.parseLong((String) data[i++]);
        j++;
      }
    }
    else
      this.type= Type.Error;
  }
  
  public GroupActivityItem[] getItems()
  {
    return items;
  }
  
  public Team[] getTeams()
  {
    return teams;
  }
  
  public TeamMember[] getTeamMembers()
  {
    return members;
  }
  
  /**
   * Returns the session_id for the group session which had been started. Only
   * available when MessageType = GroupStarted | SessionStarted
   * 
   * @return
   */
  public long getStartedSessionId()
  {
    assert(session_id != -1);
    return session_id;
  }
  
  /**
   * Returns p1 for the group session which had been started. Only available
   * when MessageType = GroupStarted. P1 is the selected activity
   * 
   * @return
   */
  public long getParam1()
  {
    return p1;
  }
  
  public long getGroupID()
  {
    return group;
  }
  
  public Type getMessageType()
  {
    return type;
  }
  
  /**
   * Returns a string array with arbitrary additional data.
   * Only available when MessageType = RoundStopped.
   *
   * @return The sent data in a string array.
   */
  public String[] getData()
  {
    return data;
  }
}
