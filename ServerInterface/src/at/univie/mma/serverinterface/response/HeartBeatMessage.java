package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class HeartBeatMessage extends ServerResponse {
  private static final long serialVersionUID = -563746127642127949L;
  
  private String message;
  private int status;

  public HeartBeatMessage(int code)
  {
    super(code);
  }

  @Override
  public void setData(Object... data)
  {
    assert(data.length == 1);
    
    message= (String) data[0];
    if( message.equals("OK") )
      status= 1;
  }
  
  public String getMessage()
  {
    return message;
  }
  
  public boolean isOk()
  {
    return status == 1;
  }
  
}
