package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class ControlCommand extends ServerResponse {
	private static final long serialVersionUID = 5279973969966468889L;
	
	public final static int TYPE_UNCLASSFIED= 0;
	public final static int TYPE_LOGIN_REQUIRED= 1;
  public static final int TYPE_SESSION_REQUIRED = 2;
	
	private String message;
	private int type;

	public ControlCommand(int code) {
		super(code);
		type= TYPE_UNCLASSFIED;
	}

	@Override
	public void setData(Object... data) {
		assert(data.length == 1);
		
		message= (String) data[0];
	}
	
	public String getMessage() {
		return message;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type= type;
	}
}
