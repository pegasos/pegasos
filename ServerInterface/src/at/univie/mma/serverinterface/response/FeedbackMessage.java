package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

import java.util.Arrays;

public class FeedbackMessage extends ServerResponse {
	private static final long serialVersionUID = 7566473834637658434L;
	
	private String message;
	private String[] extra;

	public FeedbackMessage(int code) {
		super(code);
	}

	@Override
	public void setData(Object... data) {
		assert(data.length == 1);
		
		message= (String) data[0];
	}
	
  public void setData(String[] data)
  {
    if( data.length > 1 )
    {
      message = (String) data[0];
      extra= Arrays.copyOfRange(data, 1, data.length);
    }
    else
      message = (String) data[0];
  }
	
	public String getMessage() {
		return message;
	}

	public String[] getExtra()
	{
	  return extra;
	}
}
