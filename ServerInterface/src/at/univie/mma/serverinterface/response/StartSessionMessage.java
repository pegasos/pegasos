package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.ServerResponse;

public class StartSessionMessage extends ServerResponse {
	private static final long serialVersionUID = 224912448569761071L;

	public StartSessionMessage(int code) {
		super(code);
	}

	private int session_id;
	private String message;
	private String activity_data;

	@Override
	public void setData(Object... data) {
		assert(data.length == 3);
		
		this.session_id= (Integer) data[0];
		this.message= (String) data[1];
		this.activity_data= (String) data[2];
	}

	public int getSession_id() {
		return session_id;
	}

	public String getMessage() {
		return message;
	}

	public String getActivity_data() {
		return activity_data;
	}
}
