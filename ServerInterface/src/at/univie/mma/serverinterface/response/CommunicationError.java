package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class CommunicationError extends ServerResponse {
  private static final long serialVersionUID = -3059937442618097549L;

  public CommunicationError(int code)
  {
    super(RESULT_ERROR);
  }

  @Override
  public void setData(Object... data) {
    
  }
}
