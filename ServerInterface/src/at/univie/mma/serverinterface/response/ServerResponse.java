package at.univie.mma.serverinterface.response;

@Deprecated
public abstract class ServerResponse extends at.pegasos.serverinterface.response.ServerResponse {
  public ServerResponse(int code) {
    super(code);
  }
}
