package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class FileWritingFailed extends ServerResponse {
	private static final long serialVersionUID = 1323693436351683616L;

	public FileWritingFailed() {
		super(RESULT_ERROR);
	}

	@Override
	public void setData(Object... data) {
		// TODO Auto-generated method stub

	}

}
