package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class ActivityStoppedMessage extends ServerResponse {
	private static final long serialVersionUID = -9198830817429060068L;
	
	private String message;

	public ActivityStoppedMessage(int code) {
		super(code);
	}

	@Override
	public void setData(Object... data) {
		assert(data.length == 1);
		
		message= (String) data[0];
	}
	
	public String getMessage() {
		return message;
	}
}
