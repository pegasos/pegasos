package at.univie.mma.serverinterface.response;

import at.pegasos.serverinterface.response.*;

public class RestartSessionMessage extends ServerResponse {
	private static final long serialVersionUID = -2290644917036862596L;
	
	private int session_id;
	
	public RestartSessionMessage(int code) {
		super(code);
	}

	@Override
	public void setData(Object... data) {
		assert(data.length == 1);
		
		this.session_id= (Integer) data[0];
	}
	
	public int getSessionID() {
		return session_id;
	}
}
