package at.univie.mma.serverinterface;

import java.io.*;

public class Packet extends at.pegasos.serverinterface.Packet {

  public Packet(byte[] bytes, boolean nocopy)
  {
    super(bytes, nocopy);
  }

  public Packet(byte[] bytes, int nbytes)
  {
    super(bytes, nbytes);
  }

  public Packet(ObjectInputStream in) throws IOException
  {
    super(in);
  }

  public Packet(FileInputStream in, byte[] b_len) throws IOException
  {
    super(in, b_len);
  }
}