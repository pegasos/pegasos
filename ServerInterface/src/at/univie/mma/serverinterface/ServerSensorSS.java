package at.univie.mma.serverinterface;

import at.pegasos.serverinterface.*;

public class ServerSensorSS extends ServerSensor {
  protected boolean sampling;

  /**
   * Create a new sensor.
   * It is important that the construction of the sensor is followed by a call to init @see{ @link #init}
   * @param nr
   * @param type
   * @param datasets_per_packet
   */
  public ServerSensorSS(int nr, int type, int datasets_per_packet)
  {
    super(nr, type, datasets_per_packet);
  }

	/**
	 * Calling this method does might not make sense as it repeats the last sample (with updated timestamp)
	 */
	public void sample()
	{
	  addData_fixed();
	}
	
	@Override
  public synchronized void start_sending() {
    sampling= true;
  }

  @Override
  public void stop_sending()
  {
    sampling = false;
  }
}
