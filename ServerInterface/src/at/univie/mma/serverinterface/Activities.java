package at.univie.mma.serverinterface;

public abstract class Activities {
	
	/**
	 * Run for a predefined distance --> use P2 to set the distance
	 */
	public static final int RUNNING_DISTANCE_C = 110;
	
	/**
	 * Run without any constraints
	 */
	public static final int RUNNING_FREE_C= 120;
	
	/**
	 * Calibrate PerPot on "Anf�nger"-Level
	 */
	public static final int RUNNING_PERPOT_EASY_C  = 131;
	/**
	 * Calibrate PerPot on "Fortgeschrittener"-Level
	 */
	public static final int RUNNING_PERPOT_MEDIUM_C= 132;
	/**
	 * Calibrate PerPot on "Profi"-Level
	 */
	public static final int RUNNING_PERPOT_HARD_C  = 133;
	
	public static final int RUNNING_INTERVALS_EASY_C  = 141;
	public static final int RUNNING_INTERVALS_MEDIUM_C= 142;
	public static final int RUNNING_INTERVALS_HARD_C  = 143;
	
	/**
	 * Calibrate foodpod. Use P2 for setting the distance
	 */
	public static final int FOODPOD_CALIBRATION_C= 150;
	
	public static final int RUNNING_BASE_TRAINING_EASY_C  = 161;
	public static final int RUNNING_BASE_TRAINING_MEDIUM_C= 162;
	public static final int RUNNING_BASE_TRAINING_HARD_C  = 163;
	
}
