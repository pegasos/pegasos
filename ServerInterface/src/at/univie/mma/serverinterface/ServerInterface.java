package at.univie.mma.serverinterface;

import at.pegasos.serverinterface.*;
import at.pegasos.serverinterface.Packet;
import at.pegasos.serverinterface.response.*;
import at.pegasos.serverinterface.response.ServerResponse;
import at.pegasos.serverinterface.util.Security;
import at.univie.mma.serverinterface.command.*;
import at.univie.mma.serverinterface.response.*;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.util.*;
import java.util.concurrent.*;

public class ServerInterface {
  public final static String protocolVersion= "2.1.3";

  public static final int CONNECTION_STATE_DISCONNECTED = 0;
  public static final int CONNECTION_STATE_CONNECTED = 1;
  public static final int CONNECTION_STATE_LOGGEDINi = 2;

  public static final int LANGUAGE_DE = 0;
  public static final int LANGUAGE_EN = 1;

  private final static String LOGIN_REQUIRED_MESSAGE = "Login required!";
  private final static String SESSION_REQUIRED_MESSAGE = "Session required!";

  private static final int ACTION_CODE_LOGIN = 0;
  public static final int ACTION_CODE_STARTSESSION = 1;
  private static final int ACTION_CODE_RESTARTSESSION = 2;
  private static final int ACTION_CODE_FEEDBACK = 3;
  private static final int ACTION_CODE_CONTROL_COMMAND = 4;
  private static final int ACTION_CODE_BMA_CONTROL = 6;
  private static final int ACTION_CODE_HR_PARAMETERS = 7;
  private static final int ACTION_CODE_TRAINING_PROGRAM = 8;
  private static final int ACTION_CODE_TRAINING_PARAMETERS = 9;
  private static final int ACTION_CODE_STORE_FEEDBACK = 10;
  private static final int ACTION_CODE_HEARTBEAT = 11;
  private static final int ACTION_CODE_STOP_ACTIVITY = 31;

  private static ServerInterface inst = null;

  private final String host;
  private final int    port;
  private final int sendDelay;
  private final int writeDelay= 60*1000;

  private Socket clientSocket;
  private DataOutputStream outToServer = null;
  private BufferedReader inFromServer = null;
  private FileOutputStream out;

  private Timer sendTimer;
  private Timer writeTimer;

  private final Queue<Packet> send_queue;
  private final Queue<Packet> send_queue_priority;
  private final ConcurrentLinkedQueue<ServerResponse> server_messages;
  private final ConcurrentLinkedQueue<Packet> write_queue;

  private boolean was_connected_before= false;
  private boolean active_login= false;
  private boolean connected= false;

  private int sensor_count= 0;

  private ServerSensor[] sensors= null;

  private int session_id;

  private MessageListeners responseHandler;
  private long last_heartbeat_ok;

  /**
   * Flag indicating whether a write operation for the datafile is currently running.
   */
  private boolean write_running = false;

  /**
   * Flag whether the client should always send some info to the server in order to check for
   * new messages. Should always be set to false after an operation.
   */
  boolean option_always_send = false;

  /**
   * Flag indicating if the interface has a output file (which it has not closed)
   */
  private boolean outFileOpen = false;
  private Cipher cipherEncrypt;

  private boolean ivNegotiated = false;

  private final static byte SemiColonByte;

  static {
    byte sm;
    try {
      sm = ";".getBytes("UTF-8")[0];
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      sm = 59;
    }
    SemiColonByte = sm;
  }

  private ServerInterface(String host, int port, int sendDelay)
  {
    this.host = host;
    this.port = port;
    this.sendDelay = sendDelay;

    send_queue = new ConcurrentLinkedQueue<Packet>();
    send_queue_priority = new ConcurrentLinkedQueue<Packet>();
    server_messages = new ConcurrentLinkedQueue<ServerResponse>();
    write_queue = new ConcurrentLinkedQueue<Packet>();
    responseHandler = new MessageListeners();
  }

  public static ServerInterface getInstance(String host, int port, int sendDelay)
  {
    ServerInterface old = inst;

    inst = new ServerInterface(host, port, sendDelay);

    // Retain the old responseHandler
    if( old != null && old.responseHandler != null )
      inst.responseHandler = old.responseHandler;

    return inst;
  }

  public static ServerInterface getInstance()
  {
    assert (inst != null);

    return inst;
  }

  public synchronized boolean connect()
  {
    // assert( connection_state == CONNECTION_STATE_DISCONNECTED );
    // assert( connected == false );
    if( connected == true )
      return false;

    boolean success = false;
    try
    {
      clientSocket = new Socket(host, port);

      outToServer = new DataOutputStream(clientSocket.getOutputStream());
      inFromServer = new BufferedReader(new InputStreamReader(
              clientSocket.getInputStream()));

      // connection_state= CONNECTION_STATE_CONNECTED;
      success = true;
      connected = true;
      ivNegotiated = false;
      if( was_connected_before )
        returnMessage(new ServerReconnected(1));
      was_connected_before = true;

    }
    catch( UnknownHostException e )
    {
      connected = false;
      returnMessage(new ServerDisconnectedMessage());

      e.printStackTrace();
    }
    catch( IOException e )
    {
      connected = false;
      ivNegotiated = false;
      returnMessage(new ServerDisconnectedMessage());

      e.printStackTrace();
    }

    return success;
  }

  public void singleSend()
  {
    new Timer().schedule(new TimerTask() {
      @Override
      public void run()
      {
        send();
      }
    }, 0);
  }

  public void startSending()
  {
    assert (sendTimer == null); //  throw new Exception("Already sending ...");

    sendTimer = new Timer();

    // Start in 1 sec and then run every sendDelay ms
    sendTimer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run()
      {
        send();
      }
    }, 1000, sendDelay);

    if( out != null )
    {
      writeTimer = new Timer();

      // Start in 1 min and then run every writeDelay ms
      writeTimer.scheduleAtFixedRate(new TimerTask() {
        @Override
        public void run()
        {
          write();
        }
      }, 60*1000, writeDelay);
    }
  }

  public void stopSending(boolean flush)
  {
    if( sendTimer != null )
      sendTimer.cancel();
    sendTimer= null;
    if( flush )
      send();
    
    if( writeTimer != null )
      writeTimer.cancel();
  }
	
  public void close()
  {
    connected= false;
    try
    {
      if( outToServer != null)
        outToServer.close();
      if( inFromServer != null )
        inFromServer.close();
    }
    catch( IOException e1 )
    {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    
    inst= null;
  }
	
	public int queueSize()
	{
	  return send_queue.size();
	}
	
	private void send() {
		// if( connection_state == CONNECTION_STATE_DISCONNECTED )
		if( connected == false )
		{
			returnMessage(new ServerDisconnectedMessage());
			if( !connect() )
				return;
		}
		
    if( option_always_send && (send_queue.size() > 0 || send_queue_priority.size() > 0) )
    {
      insertNoOperation();
    }
		
		if( send_queue.size() > 0 || send_queue_priority.size() > 0 ) {
			int sent_packets= 0;
			Packet p= null;
			boolean priority_done= false;
			boolean priority_sent= false;
			
			try{
				// First send priority messages
				while( send_queue_priority.size() > 0 ) {
					p= send_queue_priority.poll();
					if( p != null ) {
						outToServer.write(p.getBytes());
						sent_packets++;
						p= null;
					}
					priority_sent= true;
				}
				
				priority_done= true;
				
				// skip other messages in this turn if priority messages have been sent
				if( priority_sent == false ) {
					while( send_queue.size() > 0 ) {
						p= send_queue.poll();
						if( p != null ) {
							outToServer.write(p.getBytes());
							sent_packets++;
							p= null;
						}
					}
				}
				
				if( sent_packets > 0 ) {
					for (int i = 0; i < sent_packets; i++) {
						String ret = "";
						ret= inFromServer.readLine();
						if (ret != null) {
							ServerResponse r= handleRespsonse(ret);
							returnMessage(r);
						}
					}
				}
			} catch(IOException e) {
				//TODO: implement me
				
				// packet is only <> null if sending failed. Have to resend it
				if( p != null )
				{
					if(priority_done)
						send_queue.add(p);
					else
						send_queue_priority.add(p);
				}
				
				// Force a complete reconnect
				connected= false;
				try {
					outToServer.close();
					inFromServer.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

  public void addResponseCallback(ResponseCallback c)
  {
    responseHandler.addListenerUnique(c);
  }

  public void removeResponseCallback(ResponseCallback c)
  {
    responseHandler.removeListener(c);
  }

  public void removeResponseCallback(Class<? extends ResponseCallback> c)
  {
    responseHandler.removeResponseCallback(c);
  }

  public MessageListeners getMessageManager()
  {
    return responseHandler;
  }

  private void returnMessage(ServerResponse r)
  {
    if( responseHandler.getCount() == 0 )
    {
      if( r != null )
        server_messages.add(r);
    }
    else
      responseHandler.messageReceived(r);
  }

  private ServerResponse handleRespsonse(String return_data)
  {
    int action= -1;
    int result_code= 0;
    String tmp[]= null;
    ServerResponse ret= null;
    String raw;

    raw= return_data;
    tmp= return_data.split("[;]");
    try
    {
      action = Integer.parseInt(tmp[0]); // info on type of received message
      result_code = Integer.parseInt(tmp[1]); // 1--> evertything ok,
                                              // 0--> error happened
    }
    catch( NumberFormatException nfe )
    {
      System.out.println("Raw: " + raw);
      action = -1;
    }

    switch( action )
    {
      case -1:
        ret= new CommunicationError(0);
        break;

      case ACTION_CODE_LOGIN: // login returned
        // check if we have received 'IV negotiated'
        if( result_code == 1 && tmp.length == 3 && tmp[2].equals("OK") )
        {
          ret = null;
          // System.out.println("SI::iv negotiated");
          this.ivNegotiated = true;
          break;
        }
        ret= new LoginMessage(result_code);
        if( result_code == 1 )
        {
          // connection_state|= CONNECTION_STATE_LOGGEDIN;
          active_login= true;
          // TODO: this fails if someone sends a double/float as calibInt
          int fp_calib_int= Integer.parseInt(tmp[5]); // GET STANDARD-FP-CALIB_FAKTOR FROM SERVER
					/*int[] fp_calibration_factor= new int[14];
					for(int x=0; x < 14; x++)  // GET FP-CALIB_FAKTOR FROM SERVER
					{
						fp_calibration_factor[x]=Integer.valueOf(tmp[x+5]);
					}*/

          if( tmp.length > 7 )
          {
            ret.setData(tmp[2], tmp[3], tmp[4], fp_calib_int, Integer.parseInt(tmp[6]), Arrays.copyOfRange(tmp, 7, tmp.length));
          }
          else
          {
            ret.setData(tmp[2], tmp[3], tmp[4], fp_calib_int, Integer.parseInt(tmp[6]));
          }
        }
        else
          ret.setError(tmp[2]);
        break;

      case ACTION_CODE_STARTSESSION:
        ret = new StartSessionMessage(result_code);
        if( result_code == 1 )
        { // Start session returned
          this.session_id = Integer.parseInt(tmp[2]);

          Object activity_data;
          if( tmp.length < 5 )
            activity_data = null;
          else
            activity_data = tmp[4];
          ret.setData(this.session_id, tmp[3], activity_data);
        }
        else
          ret.setError(tmp[2]);
        break;

      case ACTION_CODE_RESTARTSESSION:
        ret = new RestartSessionMessage(result_code);
        if( result_code == 1 )
        {
          this.session_id = Integer.parseInt(tmp[2]);
          ret.setData(this.session_id);
        }
        else
          ret.setError(tmp[2]);
        break;

      case ACTION_CODE_FEEDBACK:
        ret = new FeedbackMessage(result_code);
        if( result_code == 1 )
        {
          if( tmp.length > 3 )
          {
            ((FeedbackMessage) ret).setData(Arrays.copyOfRange(tmp, 2, tmp.length));
          }
          else
            ret.setData(tmp[2]);
        }
        break;

      case ACTION_CODE_CONTROL_COMMAND:
        ret = new ControlCommand(result_code);
        ret.setData(tmp[2]);
        if( tmp[2].equals(LOGIN_REQUIRED_MESSAGE) )
        {
          active_login = false;
          ((ControlCommand) ret).setType(ControlCommand.TYPE_LOGIN_REQUIRED);
        }
        else if( tmp[2].equals(SESSION_REQUIRED_MESSAGE) )
        {
          ((ControlCommand) ret).setType(ControlCommand.TYPE_SESSION_REQUIRED);
        }
        break;

      case ACTION_CODE_STOP_ACTIVITY:
        ret= new ActivityStoppedMessage(result_code);
        ret.setData(tmp[2]);
        break;

      case ACTION_CODE_HEARTBEAT:
        ret= new HeartBeatMessage(result_code);
        ret.setData(tmp[2]);
        if( ((HeartBeatMessage) ret).isOk() )
          last_heartbeat_ok= System.currentTimeMillis();
        else
          active_login= false; // TODO: check whether this is ok
        break;

/* createTable - begin */
/* createTable - end */

      case 1000: // DO NOTHING
        break;

      default:
        ret= new FeedbackMessage(result_code);
        ret.setData(tmp[2]);
    }

    if( ret != null )
      ret.setRaw(raw);

    return ret;
  }

  /**
   * Retrieve message from the server manually. This method will return message only if no response handler has been set. 
   * @return
   */
  public List<ServerResponse> pollResonses()
  {
    boolean moremsg= false;
    ArrayList<ServerResponse> ret= new ArrayList<ServerResponse>();
    ServerResponse res;
    do
    {
      res= server_messages.poll();
      if( res != null )
      {
        ret.add(res);
        moremsg= true;
      }
      else
        moremsg= false;
    } while( moremsg );
    return ret;
  }

  /**
   * Send a login command to the server
   *
   * @param username
   * @param password
   * @param club_usage
   * @param activity
   * @param language
   * @return true if command was sent successfully (do not mistake this for a successful login!)
   */
  public boolean login(String username, String password, String club_usage, String activity, int language)
  {
    // LOGIN SEQUENCE
    byte[] one = new byte[10];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_LOGIN; // action = LOGIN
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    // one[9] = ";".getBytes(StandardCharsets.UTF_8)[0]; // unused
    // StandardCharsets becomes available only in API 19
    one[9] = SemiColonByte;

    // LOGIN (header(9byte);username;password;activity;language)
    String login_data = username + ";" + password + ";" + activity + ";" + club_usage + ";" + language;

    return sendEncryptedDataPrioritySymetric(one, login_data);
  }

  /**
   * Send a login command to the server
   *
   * @param username
   * @param password
   * @param club_usage
   * @param activity
   * @param language
   * @param sessionId  ID of the session to attach to
   */
  public boolean login(String username, String password, String club_usage, String activity, int language, long sessionId)
  {
    // LOGIN SEQUENCE
    byte[] one = new byte[10];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_LOGIN; // action = LOGIN
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[9] = SemiColonByte; // unused

    String login_data = username + ";" + password + ";" + activity + ";" + club_usage + ";" + language + ";" + sessionId;

    return sendEncryptedDataPrioritySymetric(one, login_data);
  }

  public boolean login(String token, boolean club_usage)
  {
    // LOGIN SEQUENCE
    byte[] one = new byte[10];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_LOGIN; // action = LOGIN
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[9] = ';';

    String login_data = token + ";" + (club_usage ? '1' : '0') + ";";

    return sendEncryptedDataPrioritySymetric(one, login_data);
  }

  public boolean login(String token, boolean club_usage, long sessionId)
  {
    // LOGIN SEQUENCE
    byte[] one = new byte[10];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_LOGIN; // action = LOGIN
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[9] = ';'; // unused

    String login_data = token + ";" + (club_usage ? '1' : '0') + ";" + sessionId;

    return sendEncryptedDataPrioritySymetric(one, login_data);
  }

  public boolean negotiateIV()
  {
    byte[] command = new byte[12];

    // LOGIN SEQUENCE
    command[0] = 0; // number of bytes in package
    command[1] = 0; // number of bytes in package
    command[2] = 0; // sensortype = ACTION
    command[3] = ACTION_CODE_LOGIN; // action = LOGIN
    command[4] = 0; // unused
    command[5] = 0; // unused
    command[6] = 0; // unused
    command[7] = 0; // unused
    command[8] = 0; // unused
    command[9] = ';';
    command[10] = '1';
    command[11] = ';';

    byte[] aesKey = new byte[32];
    new SecureRandom().nextBytes(aesKey);
    byte[] iv = new byte[16];
    new SecureRandom().nextBytes(iv);

    byte[] data = new byte[32+16];
    System.arraycopy(aesKey, 0, data, 0, 32);
    System.arraycopy(iv, 0, data, 32, 16);

    try
    {
      // Generate the key for AES Encryption
      cipherEncrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
      SecretKeySpec secretKeySpec = new SecretKeySpec(aesKey, "AES");
      IvParameterSpec ivspec = new IvParameterSpec(iv);
      cipherEncrypt.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);

      // Encrypt the payload before sending it to the server
      byte[] encrypted = Security.getInstance().encrypt(data);

      // Now this is a bit hacky here ...
      // Starting (most likely) openjdk-11 it is no longer possible to use -source/-target 1.5
      // Minimal target is (?) 1.7. While android sdk claims to support java 7 not all android
      // devices support it. which means we need to provide code that runs on older version
      // java.util.Base64 was only introduced in 1.8? which means we need to check if it's there
      // if it's not there we assume that we are running on an android device and thus have
      // access to android.util.Base64
      byte[] encoded;
      try
      {
        Class.forName("java.util.Base64");
        encoded = Base64.getEncoder().encode(encrypted);
      }
      catch( ClassNotFoundException e )
      {
        Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);

        encoded = (byte[]) m.invoke(null, encrypted, conv.getField("NO_WRAP").get(null));
      }

      byte[] buffer = new byte[command.length + encoded.length];
      System.arraycopy(command, 0, buffer, 0, command.length);
      System.arraycopy(encoded, 0, buffer, command.length, encoded.length);
      Packet.setLength(buffer, buffer.length);

      send_queue_priority.add(new Packet(buffer, true));
      send();
    }
    catch( InvalidKeyException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( java.security.InvalidAlgorithmParameterException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalBlockSizeException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( BadPaddingException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchAlgorithmException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchPaddingException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IOException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( ClassNotFoundException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchMethodException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchFieldException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( SecurityException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalAccessException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( java.lang.reflect.InvocationTargetException e )
    {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private boolean sendEncryptedDataPrioritySymetric(byte[] one, String data)
  {
    if( !ivNegotiated )
      negotiateIV();

    byte[] two;

    try
    {
      byte[] ldc = cipherEncrypt.doFinal(data.getBytes("UTF-8"));

      // Now this is a bit hacky here ...
      // Starting (most likely) openjdk-11 it is no longer possible to use -source/-target 1.5
      // Minimal target is (?) 1.7. While android sdk claims to support java 7 not all android
      // devices support it. which means we need to provide code that runs on older version
      // java.util.Base64 was only introduced in 1.8? which means we need to check if its there
      // if its not there we assume that we are running on an android device and thus have
      // access to android.util.Base64
      try
      {
        Class.forName("java.util.Base64");
        two = Base64.getEncoder().encode(ldc);
      }
      catch( ClassNotFoundException e )
      {
        Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);

        two = (byte[]) m.invoke(null, ldc, conv.getField("NO_WRAP").get(null));
      }

      byte[] combined = new byte[one.length + two.length];

      /*System.arraycopy(one, 0, combined, 0, one.length);
      System.arraycopy(two, 0, combined, one.length, two.length);

      Packet.setLength(combined, combined.length);

      System.out.printf("Sending symetric: %X %X %X %d\n", combined[0], combined[1], combined[10], one.length);
      System.out.println(new String(two));*/
      Packet.setLength(one, one.length + two.length);
      for(int i = 0; i < combined.length; i++)
      {
        combined[i] = i < one.length ? one[i] : two[i - one.length];
      }

      send_queue_priority.add(new Packet(combined, true));
      send();

      return true;
    }
    // TODO what can we do here? (in case of an exception?
    catch( IllegalBlockSizeException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( BadPaddingException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( ClassNotFoundException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchMethodException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchFieldException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( SecurityException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalAccessException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( java.lang.reflect.InvocationTargetException e )
    {
      e.printStackTrace();
      return false;
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
      return false;
    }
  }

  private boolean sendEncryptedDataPriority(byte[] one, String data)
  {
    byte[] two;

    try
    {
      byte[] ldc = Security.getInstance().encrypt(data.getBytes("UTF-8"));

      // Now this is a bit hacky here ...
      // Starting (most likely) openjdk-11 it is no longer possible to use -source/-target 1.5
      // Minimal target is (?) 1.7. While android sdk claims to support java 7 not all android
      // devices support it. which means we need to provide code that runs on older version
      // java.util.Base64 was only introduced in 1.8? which means we need to check if its there
      // if its not there we assume that we are running on an android device and thus have
      // access to android.util.Base64
      try
      {
        Class.forName("java.util.Base64");
        two = Base64.getEncoder().encode(ldc);
      }
      catch( ClassNotFoundException e )
      {
        Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);

        two = (byte[]) m.invoke(null, ldc, conv.getField("NO_WRAP").get(null));
      }

      byte[] combined = new byte[one.length + two.length];
      Packet.setLength(one, one.length + two.length);
      for(int i = 0; i < combined.length; i++)
      {
        combined[i] = i < one.length ? one[i] : two[i - one.length];
      }

      send_queue_priority.add(new Packet(combined, true));
      send();

      return true;
    }
    // TODO what can we do here? (in case of an exception?
    catch( InvalidKeyException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalBlockSizeException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( BadPaddingException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchAlgorithmException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchPaddingException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IOException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( ClassNotFoundException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchMethodException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( NoSuchFieldException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( SecurityException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalAccessException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
      return false;
    }
    catch( java.lang.reflect.InvocationTargetException e )
    {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Start an activity on the server. The server will use its current time as the starting time
   * @param param0 param0
   * @param param1 param1
   * @param param2 param2
   */
  public void startActivity(int param0, int param1, int param2)
  {
    int ai= 1;

    // LOGIN SEQUENCE
    byte[] one= new byte[9];
    one[0]= 0; // number of bytes in package
    one[1]= 0; // number of bytes in package
    one[2]= 0; // sensortype = ACTION
    one[3]= ACTION_CODE_STARTSESSION; // action = START SESSION
    one[4]= 0; // unused
    one[5]= 0; // unused
    one[6]= 0; // unused
    one[7]= 0; // unused
    one[8]= 0; // unused

    byte[] two= new byte[1000];
    // Leave out startime as it is ignored on the server when empty
    String session_data= ";" + param0 + ";;" + param1 + ";" + param2 + ";" + ai + ";";
    two= session_data.getBytes();
    byte[] combined= new byte[one.length + two.length];
    one[0]= (byte) (one.length + two.length);
    for(int i= 0; i < combined.length; ++i)
    {
      combined[i]= i < one.length ? one[i] : two[i - one.length];
    }

    send_queue.add(new Packet(combined, true));
  }
	
  public void startActivityT(int activity_c, long starttime)
  {
    int activity= (activity_c / 100) % 10;
    int param1= (activity_c / 10) % 10;
    int param2= activity_c % 10;

    startActivityT(activity, param1, param2, starttime);
  }

  public void startActivityT(int param0, int param1, int param2, long starttime)
  {
    int ai = 1;

    // start session SEQUENCE
    byte[] one= new byte[9];
    one[0]= 0; // number of bytes in package
    one[1]= 0; // number of bytes in package
    one[2]= 0; // sensortype = ACTION
    one[3]= ACTION_CODE_STARTSESSION; // action = START SESSION
    one[4]= 0; // unused
    one[5]= 0; // unused
    one[6]= 0; // unused
    one[7]= 0; // unused
    one[8]= 0; // unused

    byte[] two = new byte[1000];
    String session_data=";"+param0+";"+starttime+";"+param1+";"+param2+";"+ai+";"; // Start run (activity,time,fp-calib, 0 /distance / level)
    two=session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0]=(byte) (one.length + two.length);
    for (int i = 0; i < combined.length; ++i){combined[i] = i < one.length ? one[i] : two[i - one.length];}

    send_queue.add(new Packet(combined, true));
  }
	
  public void stopActivity()
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_STOP_ACTIVITY; // action = Stop activity
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[0] = (byte) (one.length);

    send_queue_priority.add(new Packet(one, true));
    send();
  }

	/**
	 * Stops an activity at the given time-stamp
	 * @param time time to be used as end-time for the session
	 */
  public void stopActivity(long time)
  {
    byte[] one = new byte[9];
    one[0]=0; // number of bytes in package
    one[1]=0; // number of bytes in package
    one[2]=0; // sensortype = ACTION
    one[3]=ACTION_CODE_STOP_ACTIVITY; // action = Stop activity
    one[4]=0; // unused
    one[5]=0; // unused
    one[6]=0; // unused
    one[7]=0; // unused
    one[8]=0; // unused

    byte[] two;
    String session_data = ";"+time+";";
    two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for (int i = 0; i < combined.length; ++i){combined[i] = i < one.length ? one[i] : two[i - one.length];}

    send_queue_priority.add(new Packet(combined, true));
    send();
  }

  /**
   * Restart the last session of the user. This requires the user to be logged in. It will restart the last session irrespective of whether or not it has been stopped before or not
   */
  public void restartSession()
  {
    byte[] one= new byte[9];
    one[0]= 0; // number of bytes in package
    one[1]= 0; // number of bytes in package
    one[2]= 0; // sensortype = ACTION
    one[3]= ACTION_CODE_RESTARTSESSION; // action = RESTART SESSION
    one[4]= 0; // unused
    one[5]= 0; // unused
    one[6]= 0; // unused
    one[7]= 0; // unused
    one[8]= 0; // unused
    one[0]= (byte) (one.length);

    send_queue_priority.add(new Packet(one, true));
    send();
  }

  public void BeatMyActivity_RequestUsersReady()
  {
    String session_data = ";0;";
    _bma_control(session_data);
  }

  public void BeatMyActivity_StartGroup(int group)
  {
    // 0--> just get number of users in groups; + --> start Beat my activity for group... - --> Stop bma for group
    String session_data = ";" + group + ";";
    _bma_control(session_data);
  }

  public void BeatMyActivity_StopGroup(int group)
  {
    // 0--> just get number of users in groups; + --> start Beat my activity for group... - --> Stop bma for group
    String session_data = ";-" + group + ";";
    _bma_control(session_data);
  }

  private void _bma_control(String session_data)
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_BMA_CONTROL; // action = get_b_values
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    byte[] two;
    two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for(int i = 0; i < combined.length; ++i)
    {
      combined[i] = i < one.length ? one[i] : two[i - one.length];
    }

    send_queue_priority.add(new Packet(combined, true));
    send();
  }

  public void requestHRParameters()
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_HR_PARAMETERS; // action = get_b_values
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    byte[] two;
    String session_data = ";0;";
    two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for(int i = 0; i < combined.length; ++i)
    {
      combined[i] = i < one.length ? one[i] : two[i - one.length];
    }

    send_queue_priority.add(new Packet(combined, true));
    send();
  }

  public void requestTrainingProgram()
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_TRAINING_PROGRAM; // action = GET TRAINING PROGRAM
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[0] = (byte) (one.length);

    send_queue_priority.add(new Packet(one, true));
    send();
  }

  public void requestTrainingParameters(int training_session_id)
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_TRAINING_PARAMETERS; // action =  GET INTERVALS PARAMETERS
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    byte[] two;
    String session_data = ";" + training_session_id + ";";
    two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for(int i = 0; i < combined.length; ++i)
    {
      combined[i] = i < one.length ? one[i] : two[i - one.length];
    }

    // socket.send_buffer(combined);
    send_queue_priority.add(new Packet(combined, true));
  }

  public void storeSessionFeedback(int a, int b, int c, int d, int e)
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_STORE_FEEDBACK; // action = Store individual feedback for a session on server
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused
    one[0] = (byte) (one.length);

    byte[] two;
    String data = ";" + session_id + ";" + a + ";" + b + ";" + c + ";" + d + ";" + e + ";"; // <v1>;<v2>;<v3>;<v4>;<v5>;
    two = data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for(int i = 0; i < combined.length; ++i)
    {
      combined[i] = i < one.length ? one[i] : two[i - one.length];
    }

    send_queue.add(new Packet(combined, true));
    send();
  }

  public void heartbeat()
  {
    byte[] one = new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ACTION_CODE_HEARTBEAT; // action: heartbeat
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    byte[] two;
    String session_data = ";" + session_id + ";";
    two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for(int i = 0; i < combined.length; ++i)
    {
      combined[i] = i < one.length ? one[i] : two[i - one.length];
    }

    send_queue.add(new Packet(combined, true));
    send();
  }

  /**
   * Insert a No-Operation command into the send-queue.
   */
  private void insertNoOperation()
  {
    byte[] one = new byte[9];
    one[0] = 9; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = 3; // action: no operation
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    send_queue.add(new Packet(one, true));
  }

  /**
   * This method is used to initialise the sensor-system
   *
   * @param count number of sensors to be used
   */
  public void setSensorCount(int count)
  {
    if( this.sensors != null )
      throw new IllegalStateException("Sensors allready deterimend");

    this.sensors = new ServerSensor[count];
  }

  /**
   * Attach a sensor to the Interface. This method assumes you know what you are doing, and you do
   * not attach more sensors than there are slots
   *
   * @param sensor sensor to attach
   */
  public void attachSensor(ServerSensor sensor)
  {
    assert (this.sensors != null);
    assert (sensor_count < this.sensors.length);

    this.sensors[sensor_count++] = sensor;
  }

  /**
   * Attach a new Sensor to the Interface. If all slots are taken create a new one
   *
   * @param sensor sensor to attach
   */
  public synchronized void safeSensorAttach(ServerSensor sensor)
  {
    // TODO: make this actually safe (ie thread-safe)
    if( sensor_count < this.sensors.length )
      this.sensors[sensor_count++] = sensor;
    else
    {
      // This assumes that sensor_count == sensor.length i.e. nothing bad has happened up to now
      ServerSensor[] sensors_new = new ServerSensor[this.sensors.length + 1];
      System.arraycopy(sensors, 0, sensors_new, 0, this.sensors.length);
      sensors_new[sensor_count] = sensor;
      this.sensors = sensors_new;
      sensor_count++;
    }
  }

  public void detachSensors()
  {
    this.sensors = null;
    this.sensor_count = 0;
  }

  public void startSensorTime(long starttime)
  {
    if( this.sensors != null )
    {
      for(ServerSensor s : this.sensors)
      {
        if( s != null )
          s.setStartTime(starttime);
      }
    }
  }

  public void addSensorData(Packet p)
  {
    this.send_queue.add(p);
    if( out != null )
      write_queue.add(p);

  }

  private void write()
  {
    if( out != null )
    {
      write_running = true;
      Packet p;
      try
      {
        while( (p = write_queue.poll()) != null )
        {
          out.write(p.getBytes());
          out.flush();
        }
      }
      catch( IOException e )
      {
        ServerResponse r = new FileWritingFailed();
        r.setError(e.getMessage());
        returnMessage(r);

        e.printStackTrace();
      }
      write_running = false;
    }
  }

  public void addPacket(Packet p)
  {
    this.send_queue.add(p);
  }

  /**
   * Sends a command to the server. This is done using the priority queue
   * assuming the command is important. Additionally, the command is also stored
   * in the data-file (if available).
   *
   * @param command Command to be sent
   */
  public void sendCommand(ICommand command)
  {
    Packet p = command.getPacket();
    this.send_queue_priority.add(p);
    if( out != null )
      write_queue.add(p);
  }

  /**
   * Sends a command to the server. The command is sent using the normal queue
   * and the packet is not stored in the data-file.
   *
   * @param command Command to be sent
   */
  public void sendCommandLow(ICommand command)
  {
    Packet p = command.getPacket();
    this.send_queue.add(p);
  }

  public boolean isConnected()
  {
    return connected;
  }

  public boolean isLoggedIn()
  {
    return active_login;
  }

  public long timeLastOKHeartbeat()
  {
    return last_heartbeat_ok;
  }

  /**
   * Set the output file. Per convention this file should be opened.
   *
   * @param stream output file
   */
  public void setSensorDataFile(FileOutputStream stream)
  {
    this.out = stream;
    this.outFileOpen = true;
  }

  /**
   * Caution: On Android use this in the background!
   */
  public void closeSensorDataFile()
  {
    if( sensors != null )
    {
      for(ServerSensor s : sensors)
      {
        if( s != null )
          s.finishPacket();
      }
    }

    if( out != null )
    {
      try
      {
        // if writeTimer has not been canceled -> cancel it
        if( writeTimer != null )
        {
          writeTimer.cancel();
          writeTimer = null;
          while( write_running )
            Thread.yield();
          // in order to be on the safe side: try to write again.
          write();
        }
        else
        {
          write();
        }
        this.out.close();
        this.outFileOpen = false;
      }
      catch( IOException e )
      {
        ServerResponse r = new FileWritingFailed();
        r.setError(e.getMessage());
        returnMessage(r);

        e.printStackTrace();
      }
    }
  }

  /**
   * Get sessionId as set by various commands
   *
   * @return id of the current or last session
   */
  public long getSessionId()
  {
    return session_id;
  }

  /**
   * Set whether the client should always send some data to the server in order to get something back.
   * When true the interface will send a 'nooperation' command to the server when nothing is present in the queue
   * during a periodic send event.
   *
   * @param status status of send-always
   */
  public void setSendAlways(boolean status)
  {
    option_always_send = status;
  }

  /**
   * Check if interface has access to an opened file for writing data.
   * Essentially, this checks if the file has been closed by the interface
   */
  public boolean isSensorDataFileOpen()
  {
    return outFileOpen;
  }
}

