package at.univie.mma.serverinterface;

import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import at.pegasos.serverinterface.response.ServerResponse;

public class MessageListeners {
  private ResponseCallback[] listeners;
  private Lock listenerlock;
  
  public MessageListeners()
  {
    listeners= new ResponseCallback[0];
    listenerlock= new ReentrantLock();
  }
  
  public void addListener(ResponseCallback listener)
  {
    System.out.println("Adding listener " + listener);
    
    listenerlock.lock();
    // first check whether this listener is not registered
    boolean found= false;
    for(ResponseCallback list : listeners)
    {
      if( list == listener )
      {
        found= true;
        break;
      }
    }
    
    if( !found )
    {
      ResponseCallback[] tmp= new ResponseCallback[listeners.length + 1];
      System.arraycopy(listeners, 0, tmp, 0, listeners.length);
      tmp[listeners.length]= listener;
      listeners= tmp;
    }
    listenerlock.unlock();
  }
  
  /**
   * Add a listener. If there is a instance of the same class remove the first occurrence
   * @param listener
   */
  public void addListenerUnique(ResponseCallback listener)
  {
    System.out.println("Adding listener " + listener);
    
    listenerlock.lock();
    
    // first check if this listener is registered
    boolean found= false;
    int pos= 0;
    for(ResponseCallback list : listeners)
    {
      if( list.getClass().equals(listener.getClass()) )
      {
        found= true;
        break;
      }
      pos++;
    }
    
    if( !found )
    {
      ResponseCallback[] tmp= new ResponseCallback[listeners.length + 1];
      System.arraycopy(listeners, 0, tmp, 0, listeners.length);
      tmp[listeners.length]= listener;
      listeners= tmp;
    }
    else
    {
      // Replace old instance
      listeners[pos]= listener;
    }
    listenerlock.unlock();
  }
  
  public void removeListener(ResponseCallback listener)
  {
    System.out.println("Removing listener " + listener);
    
    listenerlock.lock();
    // check whether this listener is actually registered
    boolean found= false;
    int pos= 0;
    for(ResponseCallback list : listeners)
    {
      if( list == listener )
      {
        found= true;
        break;
      }
      pos++;
    }
    
    if( found )
    {
      ResponseCallback[] tmp= new ResponseCallback[listeners.length - 1];
      System.arraycopy(listeners, 0, tmp, 0, pos);
      System.arraycopy(listeners, pos + 1, tmp, pos, listeners.length - pos - 1);
      
      listeners= tmp;
    }
    listenerlock.unlock();
  }
  
  public void removeResponseCallback(Class<? extends ResponseCallback> c)
  {
    listenerlock.lock();
    // check whether this listener is actually registered
    boolean found= false;
    int pos= 0;
    for(ResponseCallback list : listeners)
    {
      if( list.getClass().equals(c) )
      {
        found= true;
        break;
      }
      pos++;
    }
    
    if( found )
    {
      ResponseCallback[] tmp= new ResponseCallback[listeners.length - 1];
      System.arraycopy(listeners, 0, tmp, 0, pos);
      System.arraycopy(listeners, pos + 1, tmp, pos, listeners.length - pos - 1);
      
      listeners= tmp;
    }
    listenerlock.unlock();
  }
  
  /**
   * Send the ServerResponse to all registered receivers
   * @param r
   */
  public void messageReceived(ServerResponse r)
  {
    int i= 0;
    listenerlock.lock();
    for(ResponseCallback listener : listeners)
    {
      System.out.println("Sending message to receiver " + listener + " " + (i++) + " " + listeners.length);
      listener.messageRecieved(r);
    }
    listenerlock.unlock();
  }
  
  /**
   * Get number of registered receivers
   * @return
   */
  public int getCount()
  {
    return listeners.length;
  }
  
  public String toString()
  {
    return Arrays.toString(listeners);
  }
}
