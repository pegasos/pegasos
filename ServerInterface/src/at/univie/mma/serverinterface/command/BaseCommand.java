package at.univie.mma.serverinterface.command;

import at.univie.mma.serverinterface.Packet;

/**
 * A basic implementation of a command.
 * This class is not intended for concurrent use. If the packet is stuck in the queue there is no expected behaviour. 
 */
public abstract class BaseCommand implements ICommand {
  
  /**
   * This action code has to be set by the implementing subtype!
   */
  protected static byte ACTION_CODE= 1;
  
  // protected final int MAX_DATA_LEN= 1000;
  
  protected String send_data;
  
  private final byte one[];
  private byte[] two;
  
  protected BaseCommand()
  {
    one = new byte[9];
    one[0]=0; // number of bytes in package
    one[1]=0; // number of bytes in package
    one[2]=0; // sensortype = ACTION
    one[3]= ACTION_CODE; // action = ?
    one[4]=0; // unused
    one[5]=0; // unused
    one[6]=0; // unused
    one[7]=0; // unused
    one[8]=0; // unused
  }
  
  /**
   * Set raw data according to the protocol.
   * Is important that your data starts with ';' and ends with ';'. Otherwise, the data will not be parseable on the server
   * @param data
   */
  public void setData(String data)
  {
    this.send_data= data;
  }

  @Override
  public Packet getPacket()
  {
    assert(one.length == 9);
    
    int i;
    two= send_data.getBytes();
    
    byte[] combined = new byte[one.length + two.length];
    
    // one[0]=(byte) (one.length + two.length);
    Packet.setLength(one, one.length + two.length);
    for(i= 0; i < 9; i++)
      combined[i] = one[i];
    for (i= 0;i < two.length; i++)
      combined[i+9] = two[i];
    
    return new Packet(combined, true);
  }

}
