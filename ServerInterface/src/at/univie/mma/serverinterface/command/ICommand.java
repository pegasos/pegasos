package at.univie.mma.serverinterface.command;

import at.univie.mma.serverinterface.Packet;

public interface ICommand {
  public Packet getPacket();
}
