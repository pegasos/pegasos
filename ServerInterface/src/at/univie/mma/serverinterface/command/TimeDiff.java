package at.univie.mma.serverinterface.command;

import at.univie.mma.serverinterface.Commands.Codes;

public class TimeDiff extends BaseCommand {
  
  static{
    ACTION_CODE= Codes.TimeDiff;
  }
  
  public static TimeDiff requestTimeDiff()
  {
    TimeDiff ret= new TimeDiff();
    String h= ";1;" + System.currentTimeMillis() + ";";
    ret.setData(h);
    System.out.println(h);
    return ret;
  }
}
