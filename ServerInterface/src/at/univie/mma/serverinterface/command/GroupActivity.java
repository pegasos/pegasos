package at.univie.mma.serverinterface.command;

import at.univie.mma.serverinterface.Commands.Codes;

public class GroupActivity extends BaseCommand {
  static{
    ACTION_CODE= Codes.GroupActivity;
  }
  
  /**
   * Request the users which are ready for the activity (ie param1 = type)
   * @param type
   * @return
   */
  public static GroupActivity RequestUsersReady(int activity)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";1;8;" + activity + ";";
    ret.setData(h);
    return ret;
  }
  
  public static GroupActivity StartGroup(long group, int activity)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";2;" + group + ";8;" + activity + ";";
    ret.setData(h);
    System.out.println(h);
    return ret;
  }
  
  public static GroupActivity StopGroup(long group, int activity)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";3;" + group + ";8;" + activity + ";";
    ret.setData(h);
    return ret;
  }
  
  public static GroupActivity SendMessage(long group_session_id, String message)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";4;" + group_session_id + ";" + message + ";";
    ret.setData(h);
    return ret;
  }
  
  public static GroupActivity SendMessageP2(long group_session_id, int param)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";4;" + group_session_id + ";1;" + param + ";";
    ret.setData(h);
    return ret;
  }
  
  public static GroupActivity GetFeedback(long group_session_id)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";5;" + group_session_id + ";";
    ret.setData(h);
    return ret;
  }
  
  /**
   * Request Teams of logged in user
   * @return
   */
  public static GroupActivity GetTeams()
  {
    GroupActivity ret= new GroupActivity();
    String h= ";7;";
    ret.setData(h);
    return ret;
  }
  
  /**
   * Request members of teams. teams is empty the team members of all teams of the logged in user are shown
   * @return
   */
  public static GroupActivity GetTeamMembers(long... teams)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";8;";
    for(Long t : teams)
      h+= t + ";";
    ret.setData(h);
    return ret;
  }
  
  /**
   * Request assignment of users to groups in team
   * 
   * @param team
   *          team for which the request should be carried out
   * @param ngroups
   *          number of groups to be used
   * @param mode
   *          mode TODO: specify possible modes. For now use 'VO2M'
   * @param users
   *          users which should be assigned to groups
   */
  public static GroupActivity GetTeamMembers(long team, int ngroups, String mode, long... users)
  {
    GroupActivity ret= new GroupActivity();
    String h= ";9;" + team + ";" + ngroups + ";" + mode + ";";
    for(long user : users)
      h+= user + ";";
    ret.setData(h);
    return ret;
  }
}
