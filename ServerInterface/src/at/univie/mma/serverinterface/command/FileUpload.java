package at.univie.mma.serverinterface.command;

import java.util.Base64;

import at.univie.mma.serverinterface.Commands.Codes;

public class FileUpload extends BaseCommand {

  static{
    ACTION_CODE= Codes.FileUpload;
  }

  /**
   * Create an upload
   * @param filename name of the file to be uploaded
   * @param size number of chunks (1024 bytes)
   * @return
   */
  public static FileUpload createUpload(String filename, int size)
  {
    FileUpload ret= new FileUpload();
    String h= ";1;" + filename + ";" + size + ";";
    ret.setData(h);
    return ret;
  }

  /**
   * * Send data to the server for an upload
   * @param uploadId
   * @param nr
   * @param data (max 1024 bytes)
   * @return
   */
  public static FileUpload sendDataChunk(long uploadId, int nr, byte[] data)
  {
    String x1 = null;
    try
    {
      Class.forName("java.util.Base64");
      x1 = Base64.getEncoder().encodeToString(data);;
    }
    catch( ClassNotFoundException e )
    {
      try
      {
        Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);

        x1 = new String((byte[]) m.invoke(null, data, conv.getField("NO_WRAP").get(null)));
      } catch( ClassNotFoundException e1 ) {}
        catch( NoSuchMethodException e1 ) {}
        catch( NoSuchFieldException e1 ) {}
        catch( IllegalAccessException e1 ) {}
        catch( java.lang.reflect.InvocationTargetException e1 ) {}
    }
    catch( SecurityException e )
    {
      e.printStackTrace();
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
    }

    FileUpload ret= new FileUpload();
    String h= ";2;" + uploadId + ";" + nr + ";" + x1 + ";";
    ret.setData(h);
    return ret;
  }

  /**
   * Tell server that we have sent all chunks. Server will respond whether all chunks have been received
   * 
   * @param uploadId
   * @return
   */
  public static FileUpload finishUpload(long uploadId)
  {
    FileUpload ret= new FileUpload();
    String h= ";3;" + uploadId + ";";
    ret.setData(h);
    return ret;
  }

  public static ICommand finishUpload(long uploadId, String meta)
  {
    String x1 = null;
    try
    {
      Class.forName("java.util.Base64");
      x1 = Base64.getEncoder().encodeToString(meta.getBytes());;
    }
    catch( ClassNotFoundException e )
    {
      try
      {
        Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);

        x1 = new String((byte[]) m.invoke(null, meta.getBytes(), conv.getField("NO_WRAP").get(null)));
      } catch( ClassNotFoundException e1 ) {}
        catch( NoSuchMethodException e1 ) {}
        catch( NoSuchFieldException e1 ) {}
        catch( IllegalAccessException e1 ) {}
        catch( java.lang.reflect.InvocationTargetException e1 ) {}
    }
    catch( SecurityException e )
    {
      e.printStackTrace();
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
    }
    FileUpload ret= new FileUpload();
    String h= ";3;" + uploadId + ";" + x1 + ";";
    ret.setData(h);
    return ret;
  }
}