package at.univie.mma.serverinterface.tools;

import java.io.*;
import java.nio.file.*;

public class SensorDataParser extends at.pegasos.serverinterface.tools.SensorDataParser {

  public SensorDataParser(Path file)
  {
    super(file);
  }

  public SensorDataParser(File file)
  {
    super(file);
  }
}