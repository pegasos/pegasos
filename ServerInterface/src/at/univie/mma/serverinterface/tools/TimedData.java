package at.univie.mma.serverinterface.tools;

public class TimedData extends at.pegasos.serverinterface.tools.TimedData {
  public TimedData(long time, String type, String data, int sensornr)
  {
    super(time, type, data, sensornr);
  }
}