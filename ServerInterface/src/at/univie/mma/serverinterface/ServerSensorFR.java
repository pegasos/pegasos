package at.univie.mma.serverinterface;

import at.pegasos.serverinterface.*;

import java.util.Timer;
import java.util.TimerTask;

public class ServerSensorFR extends ServerSensor {
  private Timer send_timer;
  private final int sampling_rate;

  /**
   * Create a new sensor.
   * It is important that the construction of the sensor is followed by a call to init @see{ @link #init}
   * @param nr
   * @param type
   * @param datasets_per_packet
   * @param sampling_rate
   */
  public ServerSensorFR(int nr, int type, int datasets_per_packet, int sampling_rate)
  {
    super(nr, type, datasets_per_packet);

    this.sampling_rate = sampling_rate;
  }

	@Override
	public void sample()
	{
	  addData_fixed();
	}
	
	@Override
	public synchronized void start_sending() {
		if( sampling_rate > 0 )
		{
			send_timer = new Timer();
			send_timer.schedule(new TimerTask() {
				@Override
				public void run() {
					addData_fixed();
				}
			}, 0, sampling_rate);
		}
	}

	@Override
	public void stop_sending() {
		if( send_timer != null ) {
			try {
				send_timer.cancel();
				send_timer= null;
			} catch (Exception e) {
				//TODO:
				e.printStackTrace();
      }
    }
  }
}
