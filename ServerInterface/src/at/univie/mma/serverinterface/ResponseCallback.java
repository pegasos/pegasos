package at.univie.mma.serverinterface;

import at.pegasos.serverinterface.response.ServerResponse;

public interface ResponseCallback {
	public void messageRecieved(ServerResponse r);
}
