package at.univie.mma.serverinterface.sensors;

import java.nio.ByteBuffer;

import at.univie.mma.serverinterface.SensorTypes;
import at.univie.mma.serverinterface.ServerSensorFR;

public class GpsSensor extends ServerSensorFR {
	ByteBuffer buffer1= ByteBuffer.allocate(8);
	ByteBuffer buffer2= ByteBuffer.allocate(8);
	
	public GpsSensor(int nr, int datasets_per_packet) {
		super(nr, SensorTypes.GPS, datasets_per_packet, 1000);
		
		sample_length= 24;
		
		init();
	}
	
	public void setData(short alt, short speed, short accur, short bearing, long lat, long lon) {
		byte[] bytes1 = buffer1.putLong(0, lat).array();
		byte[] bytes2 = buffer2.putLong(0, lon).array();
		
		System.arraycopy(bytes1, 0, data_register, 0, 8); // LAT
		System.arraycopy(bytes2, 0, data_register, 8, 8); // LON

		data_register[16]= (byte) (alt & 0xFF);
		data_register[17]= (byte) ((alt >> 8) & 0xff);
		data_register[18]= (byte) (speed & 0xFF);
		data_register[19]= (byte) ((speed >> 8) & 0xff);
		data_register[20]= (byte) (accur & 0xFF);
		data_register[21]= (byte) ((accur >> 8) & 0xff);
		data_register[22]= (byte) (bearing & 0xFF);
		data_register[23]= (byte) ((bearing >> 8) & 0xff);
		
		addData_fixed();
	}
}
