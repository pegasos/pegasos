package at.univie.mma.serverinterface.sensors;

import at.univie.mma.serverinterface.SensorTypes;
import at.univie.mma.serverinterface.ServerSensorFR;

public class FootPodSensor extends ServerSensorFR {
  
  private boolean changed;
  
  public FootPodSensor(int nr, int datasets_per_packet)
  {
    super(nr, SensorTypes.FOOTPOD, datasets_per_packet, 1000);
    
    sample_length= 8;
    
    init();
    
    changed= false;
  }
  
  @Override
  public void sample()
  {
    if( changed )
      super.sample();
    changed= false;
  }
  
  public void setCurrentSpeed(double fp_speed)
  {
    changed= true;
    data_register[0]= (byte) (fp_speed); // SPEED_MM LOW
    data_register[1]= (byte) (fp_speed / 256); // SPEED_MM HIGH
  }
  
  public void setDistance(double fp_distance)
  {
    changed= true;
    data_register[2]= (byte) (fp_distance); // DIST_M
    data_register[3]= (byte) (fp_distance / 256); // DIST_M HIGH 0....25500
  }
  
  public void setStrideCount(long fp_strides)
  {
    changed= true;
    data_register[4]= (byte) (fp_strides); // STRIDES LOW 0....255
    data_register[5]= (byte) (fp_strides / 256); // STRIDES HIGH 0....255
  }
  
  public void setCalories(long fp_calories)
  {
    changed= true;
    data_register[6]= (byte) (fp_calories); // CALORIES LOW
    data_register[7]= (byte) (fp_calories / 256); // CALORIES HIGH
  }
}
