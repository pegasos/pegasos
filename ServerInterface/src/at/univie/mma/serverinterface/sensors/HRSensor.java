package at.univie.mma.serverinterface.sensors;

import at.univie.mma.serverinterface.SensorTypes;
import at.univie.mma.serverinterface.ServerSensorFR;

public class HRSensor extends ServerSensorFR {
  
  private int hrv_counter;
  private boolean changed;
  
  public HRSensor(int nr, int datasets_per_packet)
  {
    super(nr, SensorTypes.HR, datasets_per_packet, 1000);
    
    sample_length= 11;
    
    init();
    data_register= new byte[100];
    changed= false;
  }
  
  public void setCurrentHR(int hr)
  {
    changed= true;
    data_register[0]= (byte) hr;
  }
  
  @Override
  public void sample()
  {
    System.out.println("Sample");
    if( changed )
      super.sample();
    changed= false;
  }
  
  public void addRR(int intervall)
  {
    // TODO: this here is some temporary workaround --> before the SensorInterface is not started
    // the register is not cleared and would overflow
    if( hrv_counter == 4 )
      hrv_counter= 0;
    changed= true;
    hrv_counter++;
    data_register[2]= (byte) hrv_counter;
    data_register[1 + hrv_counter * 2]= (byte) (intervall & 0xFF); // hrv low
    data_register[2 + hrv_counter * 2]= (byte) ((intervall >> 8) & 0xff); // hrv High
  }
  
  @Override
  protected void resetPacket()
  {
    super.resetPacket();
    
    hrv_counter= 0;
  }
}
