package at.univie.mma.serverinterface.sensors;


import java.nio.charset.Charset;

import at.univie.mma.serverinterface.SensorTypes;
import at.univie.mma.serverinterface.ServerSensorSS;

public class Setting extends ServerSensorSS {
  
  private final static Charset cs= Charset.forName("UTF-8");
  
  public Setting(int nr, int datasets_per_packet)
  {
    super(nr, SensorTypes.SETTING, datasets_per_packet);
    
    sample_length= 92;
    
    sampling= false;
    init();
  }
  
  public void setData(String name, String value)
  {
    byte[] bname= name.getBytes(cs);
    byte[] bvalue= value.getBytes(cs);
    
    byte ln= (byte) (bname.length > 30 ? 30 : bname.length);
    byte lv= (byte) (bvalue.length > 60 ? 60 : bvalue.length);
    data_register[0]= ln;
    data_register[1]= lv;
    
    System.arraycopy(bname, 0, data_register, 2, ln);
    System.arraycopy(bvalue, 0, data_register, 2+ln, lv);
    
    addData_fixed();
  }
}
// End of generated class

