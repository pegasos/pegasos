package at.pegasos.serverinterface.sensors;

public class ServerSensorCompactSS extends ServerSensorCompact {

  /**
   * Create a new sensor.
   * It is important that the construction of the sensor is followed by a call to init @see{ @link #init}
   *
   * @param nr                  Sensor Number
   * @param type                Type number (as per interface spec)
   * @param datasets_per_packet datasets contained in one data packet
   */
  public ServerSensorCompactSS(int nr, int type, int datasets_per_packet)
  {
    super(nr, type, datasets_per_packet);
  }

  @Override
  public synchronized void start_sending()
  {
    sampling = true;
  }

  @Override
  public void stop_sending()
  {
    sampling = false;
  }
}
