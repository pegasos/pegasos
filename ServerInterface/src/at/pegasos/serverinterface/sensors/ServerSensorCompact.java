package at.pegasos.serverinterface.sensors;

import at.pegasos.serverinterface.*;
import at.pegasos.serverinterface.Packet;

public abstract class ServerSensorCompact extends ServerSensor {
  protected boolean sampling;

  private int packetLength;

  /**
   * Create a new sensor.
   * It is important that the construction of the sensor is followed by a call to init @see{ @link #init}
   *
   * @param nr                  Sensor Number
   * @param type                Type number (as per interface spec)
   * @param datasets_per_packet datasets contained in one data packet
   */
  public ServerSensorCompact(int nr, int type, int datasets_per_packet)
  {
    super(nr, type, datasets_per_packet);
  }

  /**
   * Calling this method does might not make sense as it repeats the last sample (with updated timestamp)
   */
  public void sample()
  {
    throw new IllegalStateException();
  }

	protected void addData(int dataLength)
	{
		addData(dataLength, System.currentTimeMillis());
	}

  protected void addData(int dataLength, long sampleTimestamp)
  {
    addHeader(sampleTimestamp);

    int i = (int) (sampleTimestamp - last_measurement);

    packet[pidx++] = (byte) (i & 0xFF); // counter
    packet[pidx++] = (byte) ((i >> 8) & 0xFF); // counter

    for(int x = 0; x < dataLength; x++)
    {
      packet[pidx++] = data_register[x];
    }
    packetLength += 2 + dataLength;

    last_measurement = sampleTimestamp;
    sample_counter++;

    if( sample_counter == datasets_per_packet )
    {
      // Add footer
      packetLength += /* header_length */ 9 + 2;
      packet[0] = (byte) (packetLength & 0xff); // Number of bytes in Package LOW
      packet[1] = (byte) ((packetLength >> 8) & 0xff); // Number of bytes in Package HIGH
      packet[pidx++] = (byte) (packetLength & 0xff);
      packet[pidx++] = (byte) ((packetLength >> 8) & 0xff);
      assert (pidx == packetLength);

      Packet p = new Packet(packet, packetLength);
      si.addSensorData(p);

      resetPacket();
    }
  }

  @Override
  public void resetPacket()
  {
    super.resetPacket();
    packetLength = 0;
  }

  @Override
  public void finishPacket()
  {
    // if there is no data to be sent we are done
    if( sample_counter == 0 )
      return;

    // Update header
    packetLength += /* header_length */ 9 + 2;
    packet[0] = (byte) (packetLength & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((packetLength >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[3] = (byte) (((sample_counter >> 8 << 5)) + this.nr);
    packet[4] = (byte) (sample_counter & 0xff); // Number of Sensor values in Package - LOW

    // No need to copy data as it has been copied to the packet already

    // Add footer
    packet[pidx++] = (byte) (packetLength & 0xff);
    packet[pidx++] = (byte) ((packetLength >> 8) & 0xff);
    // other than the regular add there cannot be an assert here
    assert (pidx == packetLength);

    Packet p = new Packet(packet, packetLength);
    si.addSensorData(p);

    resetPacket();
  }
}
