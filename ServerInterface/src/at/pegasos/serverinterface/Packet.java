package at.pegasos.serverinterface;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Packet implements Serializable {
  private static final long serialVersionUID= -7559351673721248131L;

  private final byte[] bytes;

  /**
   * Create a packet from a sequence of bytes If nocopy is true this packet contains a reference to
   * the original array, otherwise a copy of the input bytes is created
   * 
   * @param bytes
   *          the bytes to be contained within the packed
   * @param nocopy
   *          True: a reference is stored, False: the input bytes are copied.
   */
  public Packet(byte[] bytes, boolean nocopy)
  {
    if( nocopy )
      this.bytes= bytes;
    else
    {
      this.bytes= new byte[bytes.length];
      System.arraycopy(bytes, 0, this.bytes, 0, bytes.length);
    }
  }

  /**
   * Create a packet from a sequence of bytes If nocopy is true this packet contains a reference to
   * the original array, otherwise a copy of the input bytes is created
   * 
   * @param bytes
   *          the bytes to be contained within the packed
   * @param nbytes
   *          Number of bytes to be used in this packet (i.e. how much to copy)
   */
  public Packet(byte[] bytes, int nbytes)
  {
    this.bytes= new byte[nbytes];
    System.arraycopy(bytes, 0, this.bytes, 0, nbytes);
  }

  public static void setLength(byte[] data, int length)
  {
    data[0]= (byte) (length & 0xff); // Number of bytes in Package LOW
    data[1]= (byte) ((length >> 8) & 0xff); // Number of bytes in HIGH
  }

  public Packet(ObjectInputStream in) throws IOException
  {
    byte[] b_len= new byte[2];

    in.read(b_len, 0, 2); // Number of bytes in Package

    int number_of_bytes= ((int) b_len[0] & 0xFF) | ((int) b_len[1] & 0xFF) << 8;

    this.bytes= new byte[number_of_bytes];
    this.bytes[0]= b_len[0];
    this.bytes[1]= b_len[1];

    for(int i= 2; i < number_of_bytes; i++)
    {
      this.bytes[i]= in.readByte();
    }
  }

  /**
   * Reads a packet of a file. Starting at the current position
   * 
   * @param in
   *          the file stream
   * @param b_len
   *          length of the packet (as two bytes, read from the input stream, yes you have to do
   *          that)
   * @throws IOException
   */
  public Packet(FileInputStream in, byte[] b_len) throws IOException
  {
    int number_of_bytes= ((int) b_len[0] & 0xFF) | ((int) b_len[1] & 0xFF) << 8;

    this.bytes= new byte[number_of_bytes];

    this.bytes[0]= (byte) (number_of_bytes & 0xff); // Number of bytes in Package LOW
    this.bytes[1]= (byte) ((number_of_bytes >> 8) & 0xff); // Number of bytes in HIGH

    for(int i= 2; i < number_of_bytes; i++)
    {
      this.bytes[i]= (byte) in.read();
    }
    // in.read(this.bytes, 2, number_of_bytes - 2);
  }

  public final byte[] getBytes()
  {
    return this.bytes;
  }

  /**
   * If this packet is a sensor-data packet the method will return the time_since_start field.
   * Otherwise the return value is undefined and might result in an out-of-bounds exception
   * 
   * @return time_since_start
   */
  public long getTimeSinceStart()
  {
    long ret= ((int) bytes[5] & 0xFF) | ((int) bytes[6] & 0xFF) << 8 | ((int) bytes[7] & 0xFF) << 16 | ((int) bytes[8] & 0xFF) << 24;
    return ret;
  }

  public String toString()
  {
    StringBuilder ret= new StringBuilder("[");
    for(int i= 0; i < bytes.length; i++)
    {
      if( i > 0 )
        ret.append(" ");
      ret.append(bytes[i]);
    }
    ret.append("]");
    return ret.toString();
  }
}