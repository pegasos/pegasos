package at.pegasos.serverinterface.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Security {

  private final RSAPublicKey pubKey;
  private final Cipher cipherEncrypt;
  
  private Security() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
  {
    pubKey= readPubKeyFromFile("/public.key");

    cipherEncrypt= Cipher.getInstance("RSA/ECB/PKCS1Padding");
    cipherEncrypt.init(Cipher.ENCRYPT_MODE, pubKey);
  }
  
  public byte[] encrypt(byte[] data) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
      NoSuchPaddingException, IOException, InvalidKeyException
  {
    byte[] cipherData= cipherEncrypt.doFinal(data);
    return cipherData;
  }
  
  public byte[] encrypt(String msg) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
      NoSuchPaddingException, IOException, InvalidKeyException
  {
    byte[] cipherData= cipherEncrypt.doFinal(msg.getBytes(StandardCharsets.UTF_8));
    return cipherData;
  }

  private RSAPublicKey readPubKeyFromFile(String keyFileName) throws IOException
  {
    InputStream in= null;
    
    try
    {
      // System.out.println(getClass().getResource(keyFileName));
      // byte[] keyBytes = Files.readAllBytes(Paths.get(Security.class.getResource(keyFileName).getFile()));
      in= getClass().getResourceAsStream(keyFileName);
      if( in == null )
      {
        System.out.println(getClass().getClassLoader().getResource(keyFileName));
        in= getClass().getClassLoader().getResourceAsStream(keyFileName);
        if( in == null )
        {
          // TODO:
        }
      }
      
      // now read the actual key from the file
      ByteArrayOutputStream buffer= new ByteArrayOutputStream(2048);

      // Just to be safe we read it in loop
      int nRead;
      byte[] data= new byte[2048];

      while( (nRead= in.read(data, 0, data.length)) != -1 )
      {
        buffer.write(data, 0, nRead);
      }

      byte[] keyBytes= buffer.toByteArray();

      X509EncodedKeySpec keySpec= new X509EncodedKeySpec(keyBytes);
      KeyFactory fact= KeyFactory.getInstance("RSA");
      PublicKey pubKey= fact.generatePublic(keySpec);

      // System.out.println("keyBytes: " + Arrays.toString(keyBytes) + " " + keySpec.getFormat() + " " + Arrays.toString(keySpec.getEncoded()));

      return (RSAPublicKey) pubKey;
    }
    catch( Exception e )
    {
      // throw new RuntimeException("Spurious serialisation error", e);
      System.err.println(e);
      e.printStackTrace();
      throw new RuntimeException("Spurious serialisation error", e);
    }
    finally
    {
      if( in != null )
      {
        in.close();
      }
    }
  }

  public static PrivateKey readPrivKeyFromFile(String keyFileName) throws IOException
  {
    InputStream in= Security.class.getResourceAsStream(keyFileName);
    ObjectInputStream oin= new ObjectInputStream(new BufferedInputStream(in));
    try
    {
      BigInteger m= (BigInteger) oin.readObject();
      BigInteger e= (BigInteger) oin.readObject();
      RSAPrivateKeySpec keySpec= new RSAPrivateKeySpec(m, e);
      KeyFactory fact= KeyFactory.getInstance("RSA");
      PrivateKey privKey= fact.generatePrivate(keySpec);
      return privKey;
    }
    catch( Exception e )
    {
      throw new RuntimeException("Spurious serialisation error", e);
    }
    finally
    {
      oin.close();
    }
  }

  public static Security getInstance() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException
  {
    return new Security();
  }
}
