package at.pegasos.serverinterface.tools;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

import at.pegasos.serverinterface.Packet;
import at.univie.mma.serverinterface.tools.*;

public class SensorDataParser {
  private final File file;
  private final ArrayList<TimedData> data;
  private long starttime;

  public SensorDataParser(Path file)
  {
    this.file= file.toFile();
    data= new ArrayList<TimedData>();
    starttime= 0;
  }
  
  public SensorDataParser(File file)
  {
    this.file= file;
    data= new ArrayList<TimedData>();
    starttime= 0;
  }
  
  public void setStartTime(long starttime)
  {
    this.starttime= starttime;
  }

  public void parse() throws IOException
  {
    FileInputStream in= new FileInputStream(file);
    Packet p= null;

    byte[] b_len = new byte[2];

    // long pos= in.getChannel().position();
    while( (b_len[0]= (byte) in.read()) != -1 )
    {
      b_len[1]= (byte) in.read();
      p= new Packet(in, b_len);

      data.addAll(PacketConverter.convert(starttime, p));
    }

    in.close();
  }

  public List<TimedData> getData()
  {
    return data;
  }
}
