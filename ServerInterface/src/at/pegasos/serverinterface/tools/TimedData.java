package at.pegasos.serverinterface.tools;

public class TimedData {
  public final long time;
  public final int sensornr;
  public final String type;
  public final String data;
  
  public TimedData(long time, String type, String data, int sensornr)
  {
    this.time= time;
    this.type= type;
    this.data= data;
    this.sensornr= sensornr;
  }

  @Override public String toString()
  {
    return "TimedData{" + "time=" + time + ", sensornr=" + sensornr + ", type='" + type + '\'' + ", data='" + data + '\'' + '}';
  }
}
