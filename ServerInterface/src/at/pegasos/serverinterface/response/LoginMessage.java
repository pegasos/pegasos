package at.pegasos.serverinterface.response;

import java.io.Serializable;

public class LoginMessage extends ServerResponse {
  private static final long serialVersionUID= -5775238623165457100L;

  public static class UserData implements Serializable {
    private static final long serialVersionUID= 7859455853535510409L;

    public enum Type {
      INT, DEC, TEXT
    }

    public String name;
    public Type type;
    public Object value;
    
    public UserData(String name, String type, String value)
    {
      this.name= name;
      this.type= Type.valueOf(type.toUpperCase());
      switch( this.type )
      {
        case DEC:
          this.value= Double.parseDouble(value);
          break;
        case INT:
          this.value= Integer.parseInt(value);
          break;
        case TEXT:
          this.value= value;
          break;
        default:
          this.value= null;
          throw new IllegalArgumentException("Unrecognised type:" + type);
      }
    }
  }

  private String username;
  private String surname;
  private String lastname;
  private int footpod_calib;
  // private int[] footpod_calib_vect;
  private int max_hr;
  private UserData[] userdata;
  
  public LoginMessage(int result_code)
  {
    super(result_code);
  }
  
  @Override
  public void setData(Object... data)
  {
    assert (data.length >= 4);

    this.username = (String) data[0];
    this.surname = (String) data[1];
    this.lastname = (String) data[2];
    this.footpod_calib = (Integer) data[3];
    // this.footpod_calib_vect= (int[]) data[3];
    this.max_hr = (Integer) data[4];
    
    if( data.length > 5 )
    {
      String[] dd= (String[]) data[5];
      final int count= dd.length / 3;
      this.userdata= new UserData[count];
      for(int i= 0, idx= 0; i < count; i++, idx+= 3)
      {
        userdata[i]= new UserData(dd[idx], dd[idx+1], dd[idx+2]);
      }
    }
    else
    {
      userdata= new UserData[0];
    }
  }

  public String getUsername()
  {
    return username;
  }

  public String getSurname()
  {
    return surname;
  }

  public void setSurname(String surname)
  {
    this.surname= surname;
  }
  
  public String getLastname()
  {
    return lastname;
  }
  
  public void setLastname(String lastname)
  {
    this.lastname= lastname;
  }
  
  public int getFootpod_calib()
  {
    return footpod_calib;
  }
  
  public void setFootpod_calib(int footpod_calib)
  {
    this.footpod_calib= footpod_calib;
  }
  
  /*public int[] getFootpod_calib_vect() {
    return footpod_calib_vect;
  }
  
  public void setFootpod_calib_vect(int[] footpod_calib_vect) {
    this.footpod_calib_vect = footpod_calib_vect;
  }*/
  
  public int getMaxHr()
  {
    return this.max_hr;
  }
  
  public UserData[] getUserData()
  {
    return this.userdata;
  }
}
