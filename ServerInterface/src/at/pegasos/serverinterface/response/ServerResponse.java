package at.pegasos.serverinterface.response;

import java.io.Serializable;

public abstract class ServerResponse implements Serializable {
  private static final long serialVersionUID = -6676504886919893332L;

  public static final int RESULT_ERROR = 0;
  public static final int RESULT_OK = 1;
  private String error;

  private final int code;
  private String raw;

  public ServerResponse(int code)
  {
    this.code = code;
  }

  public int getResultCode()
  {
    return code;
  }

  public void setError(String error)
  {
    this.error = error;
  }

  public String getError()
  {
    return this.error;
  }

  public abstract void setData(Object... data);

  public void setRaw(String ret)
  {
    this.raw = ret;
  }

  public String getRaw()
  {
    return this.raw;
  }
}