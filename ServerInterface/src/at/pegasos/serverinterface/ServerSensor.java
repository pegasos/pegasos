package at.pegasos.serverinterface;

import at.univie.mma.serverinterface.*;

public abstract class ServerSensor {
  protected final int nr;
  protected final int datasets_per_packet;
  private final int type;
  private final int header_length = 9;
  protected byte[] packet;
  protected byte[] data_register;
  /**
   * Packet index
   */
  protected int pidx;
  protected int sample_length;
  protected long starttime = 0;
  protected long last_measurement = 0;
  protected int sample_counter;
  private int packet_length;
  private boolean header_needed;

  /**
   * Create a new sensor.
   * It is important that the construction of the sensor is followed by a call to init @see{ @link #init}
   *
   * @param nr                  SensorNumber. Can be a random number or type number.
   * @param type                Type-Number of the sensor. Use SensorTypes (generated class) or be very careful about what you are doing
   * @param datasets_per_packet How many datasets are contained in one packet. Tuning parameter
   */
  public ServerSensor(int nr, int type, int datasets_per_packet)
  {
    this.nr = nr;
    this.type = type;
    this.datasets_per_packet = datasets_per_packet;
    assert (datasets_per_packet < Byte.MAX_VALUE);
  }

  /**
   * Initialise the sensors internal data structures. Before this method is invoked sample_lenght @see {@link #sample_length} has to be set.
   * It is important that sensors call this function before doing anything else!
   */
  protected void init()
  {
    packet_length = header_length + ((sample_length+2) * this.datasets_per_packet) + 2;

    data_register = new byte[sample_length];

    createPacket();
  }

  protected final ServerInterface si = ServerInterface.getInstance();

  /**
   * Add one sample. Use with care!
   */
  public abstract void sample();

  /**
   * Put sensor into sending mode
   */
  public abstract void start_sending();

  /**
   * put sensor into a stopped mode
   */
  public abstract void stop_sending();

  private void createPacket()
  {
    sample_counter = 0;
    header_needed = true;
    packet = new byte[packet_length];

    packet[0] = (byte) (packet_length & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((packet_length >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[2] = (byte) (this.type); // Sensortype
    packet[3] = (byte) (((this.datasets_per_packet >> 8 << 5)) + this.nr);
    packet[4] = (byte) (this.datasets_per_packet & 0xff); // Number of Sensor values in Package - LOW
    pidx = 5;
  }

  protected void addHeader()
  {
    addHeader(System.currentTimeMillis());
  }

  protected void addHeader(long time)
  {
    if( this.header_needed )
    {
      assert (pidx == 5);
      assert (this.starttime != 0);
      int i = (int) (time - this.starttime);
      packet[5] = (byte) (i & 0xFF);
      packet[6] = (byte) ((i >> 8) & 0xFF);
      packet[7] = (byte) ((i >> 16) & 0xFF);
      packet[8] = (byte) ((i >> 24) & 0xFF);
      this.header_needed = false;
      this.pidx = 9;
    }
  }

  protected void resetPacket()
  {
    pidx = 5;
    header_needed = true;
    sample_counter = 0;
  }

  protected void addData_fixed()
  {
    addHeader();

    long time = System.currentTimeMillis();
    int i = (int) (time - last_measurement);

    packet[pidx++] = (byte) (i & 0xFF); // counter
    packet[pidx++] = (byte) ((i >> 8) & 0xFF); // counter

    for(int x = 0; x < sample_length; x++)
    {
      packet[pidx++] = data_register[x];
    }

    last_measurement = time;
    sample_counter++;

    if( sample_counter == datasets_per_packet )
    {
      // Add footer
      packet[pidx++] = (byte) (packet_length & 0xff);
      packet[pidx++] = (byte) ((packet_length >> 8) & 0xff);
      assert (pidx == packet_length);

      Packet p = new Packet(packet, false);
      si.addSensorData(p);

      resetPacket();
    }
  }

  /**
   * Add a data packet with a specified time-stamp
   *
   * @param time time-stamp
   */
  protected void addData_fixed(long time)
  {
    addHeader();

    int i = (int) (time - last_measurement);

    packet[pidx++] = (byte) (i & 0xFF); // counter
    packet[pidx++] = (byte) ((i >> 8) & 0xFF); // counter

    for(int x = 0; x < sample_length; x++)
    {
      packet[pidx++] = data_register[x];
    }

    last_measurement = time;
    sample_counter++;

    if( sample_counter == datasets_per_packet )
    {
      // Add footer
      packet[pidx++] = (byte) (packet_length & 0xff);
      packet[pidx++] = (byte) ((packet_length >> 8) & 0xff);
      assert (pidx == packet_length);

      Packet p = new Packet(packet, false);
      si.addSensorData(p);

      resetPacket();
    }
  }

  /**
   * Get number of samples currently collected by this sensor
   * @return number of samples
   */
  public int samples()
  {
    return sample_counter;
  }

  /**
   * Finish this packet and send it to the server. 
   * This method can be used before closing your connection and to ensure that all unsent data is sent to the server.
   * After this method the state of the sensor is the same as after a regular send
   */
  public void finishPacket()
  {
    // if there is no data to be sent we are done
    if( sample_counter == 0 )
      return;

    int actual_length= header_length + ((sample_length + 2) * sample_counter) + 2;

    packet[0] = (byte) (actual_length & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((actual_length >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[2] = (byte) (this.type); // Sensortype
    packet[3] = (byte) (((sample_counter >> 8 << 5)) + this.nr);
    packet[4] = (byte) (sample_counter & 0xff); // Number of Sensor values in Package - LOW

    // No need to copy data as it has been copied to the packet already

    // Add footer
    packet[pidx++]= (byte) (actual_length & 0xff);
    packet[pidx++]= (byte) ((actual_length >> 8) & 0xff);
    // other than the regular add there cannot be an assert here
    assert (pidx == actual_length);

    Packet p= new Packet(packet, actual_length);
    si.addSensorData(p);

    resetPacket();
  }

  public int getNumber()
  {
    return this.nr;
  }

  public int getType()
  {
    return type;
  }

  public void setStartTime(long starttime)
  {
    this.starttime = starttime;
    this.last_measurement = starttime;
  }
}