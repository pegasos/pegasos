package at.pegasos.serverinterface.command;

import at.univie.mma.serverinterface.Commands.Codes;
import at.univie.mma.serverinterface.command.*;

public class AIMessage extends BaseCommand {
  static{
    ACTION_CODE= Codes.AIMessage;
  }

  public static AIMessage SendMessage(String message)
  {
    AIMessage ret= new AIMessage();
    String h= ";1;" + message;
    if( !message.endsWith(";"))
      h+= ";";
    ret.setData(h);
    return ret;
  }
}
