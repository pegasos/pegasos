package at.pegasos.serverinterface.command;

import at.univie.mma.serverinterface.*;
import at.univie.mma.serverinterface.command.*;

import java.util.*;

public class StartSession implements ICommand {

  private Packet data;

  public static StartSession createSession(int param0, int param1, int param2, long startTime, long endTime)
  {
    StartSession ret = new StartSession();

    byte[] one= new byte[9];
    one[0] = 0; // number of bytes in package
    one[1] = 0; // number of bytes in package
    one[2] = 0; // sensortype = ACTION
    one[3] = ServerInterface.ACTION_CODE_STARTSESSION; // action = START SESSION
    one[4] = 0; // unused
    one[5] = 0; // unused
    one[6] = 0; // unused
    one[7] = 0; // unused
    one[8] = 0; // unused

    int tzLocal = TimeZone.getDefault().getOffset(startTime) / 1000 / 60 / 60 * 2;
    // <Activity>;<Starttimestamp in ms>;<Param1>;<Param2>;<use AI/param3>;[tzLocal];[endTime]
    String session_data =";"+param0+";"+startTime+";"+param1+";"+param2+";0;" + tzLocal + ";" + endTime + ";";
    byte[] two = session_data.getBytes();
    byte[] combined = new byte[one.length + two.length];
    one[0] = (byte) (one.length + two.length);
    for (int i = 0; i < combined.length; ++i){combined[i] = i < one.length ? one[i] : two[i - one.length];}

    ret.data = new Packet(combined, combined.length);

    return ret;
  }

  @Override
  public Packet getPacket()
  {
    return data;
  }
}
