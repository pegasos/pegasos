package at.univie.mma;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import at.univie.mma.R;

public class PermissionChecker {
  
  /**
   * ID for multiple simultaneous permission requests.
   */
  public static final int PERMISSION_REQUEST_MULTIPLE= 100;
  
  /**
   * Checks if permissions have been granted and requests only those that have
   * not been granted yet.
   * 
   * @param permissions
   *          The request permissions to be checked.
   * @param activity
   *          The activity which requires the permissions.
   */
  public static void checkAndRequestPermissions(final String[] permissions, final Activity activity)
  {
    ArrayList<String> permissionsToRequest= new ArrayList<String>();
    
    // check if permissions have already been granted
    for(String permission : permissions)
      if( ActivityCompat.checkSelfPermission(activity.getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED )
        permissionsToRequest.add(permission);
    
    if( !permissionsToRequest.isEmpty() )
      ActivityCompat.requestPermissions(activity, permissionsToRequest.toArray(new String[0]), PERMISSION_REQUEST_MULTIPLE);
  }
  
  /**
   * Shows a permission rationale and requests permissions again after
   * dismissing the dialog window.
   * 
   * @param activity
   *          The activity requesting the rationale and permissions.
   * @param rationale
   *          The rationale text to be displayed to the user.
   * @param permissions
   *          The permissions to be requested.
   */
  public static void showRationale(final Activity activity, final String rationale, final String... permissions)
  {
    AlertDialog.Builder builder= new AlertDialog.Builder(activity);
    
    builder.setTitle(R.string.permission_required);
    builder.setMessage(rationale);
    
    builder.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which)
      {
        checkAndRequestPermissions(permissions, activity);
        dialog.dismiss();
      }
    });
    
    builder.create().show();
  }
  
  /**
   * Asks the user to open the app's settings page. Does so on yes, closes the
   * calling activity on no.
   * 
   * @param activity
   *          The app requesting the dialog.
   */
  public static void showSettingsDialog(final Activity activity)
  {
    AlertDialog.Builder builder= new AlertDialog.Builder(activity);
    
    builder.setTitle(R.string.permission_required);
    builder.setMessage(R.string.permission_open_settings).setPositiveButton(
        android.R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i)
          {
            Intent intent= new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + activity.getPackageName()));
            activity.startActivity(intent);
          }
        }).setNegativeButton(android.R.string.no,
            new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i)
              {
                activity.finish();
              }
            });
    
    builder.create().show();
  }
}
