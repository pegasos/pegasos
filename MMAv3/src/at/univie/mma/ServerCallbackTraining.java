package at.univie.mma;

import android.util.*;
import at.pegasos.client.*;
import at.pegasos.client.controller.*;
import at.pegasos.client.ui.*;
import at.pegasos.serverinterface.response.*;
import at.pegasos.serverinterface.response.ServerResponse;
import at.univie.mma.R;
import at.univie.mma.serverinterface.*;
import at.univie.mma.serverinterface.response.*;
import at.univie.mma.ui.controller.*;
import org.acra.*;

public class ServerCallbackTraining implements ResponseCallback {

  private final TrainingActivity activity;
  private final Soundmachine soundmachine;
  private ITrainingController controller;

  private boolean silent_server_disconnected = false;
  private boolean silent_sever_disconnected_retell = false;
  private boolean disconnect_reported = false;
  private long disconnect_reported_time;
  private int login_required_counts = 0;
  private int session_required_counts = 0;
  private boolean restart_required = false;
  private long sessionId = 0;

  public ServerCallbackTraining(TrainingActivity activity)
  {
    this.activity = activity;
    this.soundmachine = activity.getSoundmachine();
  }

  public void setController(ITrainingController controller)
  {
    this.controller = controller;
  }

  @Override public void messageRecieved(ServerResponse r)
  {
    if( r != null )
    {
      if( Config.LOGGING )
        Log.d("Message recieved", (r.getRaw() != null ? r.getRaw() : "null"));

      if( controller != null && controller.onMessage(r) )
        return;

      if( r instanceof FeedbackMessage )
      {
        Log.e("Mesaage", ((FeedbackMessage) r).getMessage());
        readMessage(((FeedbackMessage) r).getMessage());
      }
      else if( r instanceof StartSessionMessage )
      {
        Log.d("Message1", "Session started");
        session_required_counts = 0;
        this.sessionId = ((StartSessionMessage) r).getSession_id();
        activity.onSessionStarted();
      }
      else if( r instanceof LoginMessage )
      {
        login_required_counts = 0;

        if( restart_required )
        {
          ServerInterface.getInstance().restartSession();
        }

        activity.showToast("Login Message recieved");
      }
      else if( r instanceof ActivityStoppedMessage )
      {
        //TODO: Handle this
        activity.showToast("Activity Stopped");
        readMessage("Activity ended");
      }
      else if( r instanceof ServerDisconnectedMessage )
      {
        if( Config.LOGGING )
          Log.d("Training", "Server disconnected recieved");

        if( disconnect_reported )
        {
          long delta = System.currentTimeMillis() - disconnect_reported_time;
          if( delta > Config.SILENT_SERVER_DISCONNECTED_RETELL_INTERVALL )
          {
            disconnect_reported_time = System.currentTimeMillis();
            if( readServerConnectionLostRetell() )
              readMessage(activity.getString(R.string.server_connection_lost_retell));
          }
        }
        else
        {
          disconnect_reported = true;
          disconnect_reported_time = System.currentTimeMillis();
          if( readServerConnectionLost() )
            readMessage(activity.getString(R.string.server_connection_lost));

          // ServerInterface.getInstance().stopSending(false);
          ServerInterface.getInstance().connect();

          activity.setSensorIconsVisibilityGuiThread();
        }
      }
      else if( r instanceof ServerReconnected )
      {
        disconnect_reported = false;

        if( !Config.SILENT_SERVER_RECONNECTED )
          readMessage(activity.getString(R.string.server_connection_reestablished));
        LoginData l = LoginData.getLoginData();
        if( l == null )
          ACRA.getErrorReporter().handleException(new Exception("Das sollte nicht passieren. LoginData ist null"));
        else
        {
          // TODO: this is a bit hacky but ok ...
          if( this.sessionId != 0 )
          {
            Tasks.login(l, this.sessionId);
            restart_required = false;
          }
          else
          {
            Tasks.login(l);
            restart_required = true;
          }
        }

        activity.setSensorIconsVisibilityGuiThread();
      }
      else if( r instanceof RestartSessionMessage )
      {
        if( restart_required )
        {
          restart_required = false;
          ServerInterface.getInstance().startSending();
        }
        session_required_counts = 0;
      }
      else if( r instanceof ControlCommand )
      {
        ControlCommand command = (ControlCommand) r;
        if( command.getType() == ControlCommand.TYPE_LOGIN_REQUIRED )
        {
          login_required_counts++;
        }
        else if( command.getType() == ControlCommand.TYPE_SESSION_REQUIRED )
        {
          session_required_counts++;
          // if the server sends this only once we can assume that it handled automatically
          if( session_required_counts > 2 )
          {
            Tasks.restartSession();
            if( session_required_counts > 3 )
            {
              // readMessage("Mehr als drei Mal session required. Sollte nicht sein. Bitte Fehler melden");
              ACRA.getErrorReporter().handleException(new IllegalStateException("Mehr als drei Mal session required. Sollte nicht sein."));
            }
          }
        }
        else
        {
          if( Config.LOGGING )
            Log.d("ServerCallbackTraining", "Kontrollkommando: " + ((ControlCommand) r).getMessage());
          readMessage(((ControlCommand) r).getMessage());
          TrainingUIController.getInstance().stopTraining();
        }
      }
      else
      {
        // if( Config.DEBUG ) readMessage("Unbekannte Rueckmeldung: " + r.getRaw());
        Log.e("ServerCallbackTraining", "Message: " + (r.getRaw() != null ? r.getRaw() : "null"));
      }
    }
    else
    {
      Log.e("ServerCallbackTraining", "Message recieved empty");
    }
  }

  private boolean readServerConnectionLost()
  {
    return !Config.SILENT_SERVER_DISCONNECTED && !silent_server_disconnected;
  }

  private boolean readServerConnectionLostRetell()
  {
    return !Config.SILENT_SERVER_DISCONNECTED_RETELL && !silent_sever_disconnected_retell;
  }

  public void setSilentServerDisconnected(boolean value)
  {
    this.silent_server_disconnected = value;
  }

  public void setSilentServerDisconnectedRetell(boolean value)
  {
    this.silent_sever_disconnected_retell = value;
  }

  public boolean isConnected()
  {
    // TODO: this is actually incorrect. When never connected this does not work.
    //   when no data is sent -> does not work
    return disconnect_reported == false;
  }

  private void readMessage(String text)
  {
    soundmachine.say_it(text);
  }
};
