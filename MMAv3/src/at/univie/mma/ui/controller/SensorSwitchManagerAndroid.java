package at.univie.mma.ui.controller;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import at.univie.mma.R;
import at.pegasos.client.ui.PegasosClientView;

public class SensorSwitchManagerAndroid extends SensorSwitchManager {
  
  private PegasosClientView a;

  public void setup(PegasosClientView a, SensorSwitchManagerCallback callback)
  {
    this.a= a;
    super.setup(a, callback);
  }
  
  public void update(final PegasosClientView a, boolean can_ble, boolean can_ant)
  {
    this.a= a;
    super.update(a, can_ble, can_ant);

    Switch switchBLE= a.findViewById(R.id.ble_switch);
    switchBLE.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
      {
        OnBleSwitchToggled(isChecked);
      }
    });
    
    Switch switchAnt= a.findViewById(R.id.ant_plus_switch);
    switchAnt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
      {
        OnAntSwitchToggled(isChecked);
      }
    });
  }

  protected void setBleVisible(boolean hideBleButton)
  {
    if( hideBleButton )
    {
      a.findViewById(R.id.ble_switch_layout).setVisibility(View.INVISIBLE);
      a.findViewById(R.id.ble_switch_layout).setVisibility(View.GONE);
    }
    else
      a.findViewById(R.id.ble_switch_layout).setVisibility(View.VISIBLE);
  }

  protected void setSwitchBLEChecked(boolean checked)
  {
    ((Switch) a.findViewById(R.id.ble_switch)).setChecked(checked);
  }

  protected void setAntVisible(boolean hideAntButton)
  {
    if( hideAntButton )
    {
      a.findViewById(R.id.ant_plus_switch_layout).setVisibility(View.INVISIBLE);
      a.findViewById(R.id.ant_plus_switch_layout).setVisibility(View.GONE);
    }
    else
      a.findViewById(R.id.ant_plus_switch_layout).setVisibility(View.VISIBLE);
  }

  protected void setSwitchAntChecked(boolean checked)
  {
    ((Switch) a.findViewById(R.id.ant_plus_switch)).setChecked(checked);
  }

  protected void removeSwitches()
  {
    LinearLayout layoutSensorSwitches= a.findViewById(R.id.sensor_type_switches);
    layoutSensorSwitches.setVisibility(View.GONE);
    if( a.findViewById(R.id.switches_spacer) != null )
    {
      a.findViewById(R.id.switches_spacer).setVisibility(View.GONE);
    }
  }
}
