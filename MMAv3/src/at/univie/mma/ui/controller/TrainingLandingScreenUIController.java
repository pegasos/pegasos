package at.univie.mma.ui.controller;

import at.pegasos.client.*;
import org.acra.ACRA;

import java.util.Timer;
import java.util.TimerTask;

import at.pegasos.client.util.PegasosLog;
import at.univie.mma.Config;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.sensors.Sensor;
import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.client.sports.SportFactory;
import at.univie.mma.ui.TrainingLandingScreen;

public class TrainingLandingScreenUIController {

  private class SensorStateUpdateTask extends TimerTask {
    @Override
    public void run()
    {
      boolean sens_changed= sensors.sensorsChanged();

      // If no sensors changed we do not need to update something
      if( !sens_changed )
      {
        checkAutoStart();
        return;
      }

      text_gps= sensors.getStatusTextGPS();
      text_hr= sensors.getStatusTextHR();
      text_fp= sensors.getStatusTextFootPod();

      imageIdGPS= sensors.getStatusGPS().state;
      imageIdFootPod= sensors.getStatusFootPod().state;
      imageIdHR= sensors.getStatusHR().state;

      ui.setSensorStatus(0, text_gps, imageIdGPS);
      ui.setSensorStatus(1, text_hr, imageIdHR);
      ui.setSensorStatus(2, text_fp, imageIdFootPod);

      int i= 0;
      for(String sensor : extras)
      {
        texts[i]= sensors.getStatusText(sensor);
        Sensor.Status status= sensors.getStatus(sensor);
        if( status != null )
          imageExtra[i]= status.state;
        else
          imageExtra[i]= Sensor.State.STATE_NOT_CONNECTED; // TODO: State for sensor which are not available?

        ui.setSensorStatus(i+3, texts[i], imageExtra[i]);

        i++;
      }

      checkAutoStart();
      ui.setStartBtnState(required_ok, !autostart_requested);
    }

    private void checkAutoStart()
    {
      required_ok= sensors.allRequiredSensorsOk();
      long h1= System.currentTimeMillis();
      long h2= ServerInterface.getInstance().timeLastOKHeartbeat();
      PegasosLog.d("SensorCheckerTask", "req: " + required_ok + " " + h1 + " " + h2 + " " + (h1 - h2));
      if( autostart_requested && required_ok && (h1 - h2) < 15000 )
      {
        // TODO: why was this assignment here?
        // autostart_requested= false;
        ui.startTraining(false);
      }
    }
  }

  private SensorSwitchManager switchManager;

  private Timer sensor_checker;

  private Sensor.State imageIdGPS;
  private Sensor.State imageIdFootPod;
  private Sensor.State imageIdHR;
  private Sensor.State[] imageExtra;

  private String text_gps;
  private String text_hr;
  private String text_fp;
  private String[] texts;

  /**
   * Use this flag to auto-start a training. Use it only once ie when the start
   * was requested. Not if the user returns. ie only change it onCreate
   */
  private final boolean autostart_requested;
  /**
   * Flag indicating if all required sensors for starting the activity are available.
   */
  private boolean required_ok= false;

  private boolean started;

  private final TrainingLandingScreen ui;

  private SensorControllerService sensors;

  private final String sport;

  /**
   * Names of Sensors to be used
   */
  private String[] extras;

  private boolean gps= false;
  private boolean hr= false;
  private boolean fp= false;
  private boolean[] using_extras;

  /**
   * It is important that any controller object is only created AFTER the
   * SensorControllerService has been created.
   * 
   * @param landingScreen
   *          reference to the screen
   */
  public TrainingLandingScreenUIController(TrainingLandingScreen landingScreen, boolean autostart_requested, String sport)
  {
    this.ui= landingScreen;
    this.sensors= SensorControllerService.createService(landingScreen, SportFactory.getSportFromString(sport), false);
    this.switchManager= ui.getSwitchManager();
    this.autostart_requested= autostart_requested;
    this.sport= sport;
  }

  public void startController()
  {
    imageExtra= new Sensor.State[3];

    // Set State of start button as it should display as disabled in the beginning
    ui.setStartBtnState(required_ok, !autostart_requested);

    // Tell SensorService which sensors (ant/ble) to use and start the sensor
    // search
    updateSensorTypes();

    if( ui.canWrite() )
      sensors.startSearchingSensors();
    else
      ui.notifyWritePermissionRequired();

    startSensorCheckerTask();

    PegasosClient.getInstance().startHeartBeats();

    ui.setVisibilities(gps, hr, fp, using_extras);

    started= false;
  }

  public void OnResume()
  {
    PegasosLog.e(getClass().getCanonicalName(), "OnResume");
    updateSensorTypes();

    if( started )
    {
      startSensorCheckerTask();

      // TODO: start sensor discovery?
    }
    else
    {
      PegasosClient.getInstance().startHeartBeats();
    }

    ui.setStartContinueButtonState(started);
    ui.setVisibilities(gps, hr, fp, using_extras);
    updateUI();
  }

  public void stop()
  {
    PegasosClient.getInstance().stopHeartBeats();

    stopSensorCheckerTask();

    if( !TrainingUIController.getInstance().isRunning() )
    {
      if( sensors != null )
        sensors.stopService();
    }
  }

  public void OnTrainingStartTimedout()
  {
    ServerInterface.getInstance().detachSensors();
    sensors.stopService();
    started= false;

    if( ui.canWrite() )
      sensors.startSearchingSensors();
    else
      ui.notifyWritePermissionRequired();

    startSensorCheckerTask();

    updateUI();
  }

  public void pause()
  {
    PegasosClient.getInstance().stopHeartBeats();

    stopSensorCheckerTask();
  }

  public void OnStartTraining(boolean continued)
  {
    started= true;

    stopSensorCheckerTask();
  }

  private void updateUI()
  {
    switchManager= ui.getSwitchManager();

    // We have to manually fetch the states here.
    // There is no guarantee that the checker task will have filled in the
    // states before we try to put them on the UI.
    text_gps= sensors.getStatusTextGPS();
    text_hr= sensors.getStatusTextHR();
    text_fp= sensors.getStatusTextFootPod();

    imageIdGPS= sensors.getStatusGPS().state;
    imageIdFootPod= sensors.getStatusFootPod().state;
    imageIdHR= sensors.getStatusHR().state;

    ui.setStartBtnState(required_ok, !autostart_requested);

    ui.setSensorStatus(0, text_gps, imageIdGPS);
    ui.setSensorStatus(1, text_hr, imageIdFootPod);
    ui.setSensorStatus(2, text_fp, imageIdHR);

    for(int i= 0; i < extras.length; i++)
    {
      ui.setSensorStatus(i+3, texts[i], imageExtra[i]);
    }
  }

  public void restart()
  {
    ServerInterface.getInstance().detachSensors();

    sensors= SensorControllerService.getInstance();

    startSensorCheckerTask();

    updateUI();
  }

  private void startSensorCheckerTask()
  {
    // Set the schedule function and rate at 1sec
    sensors.checkSensorsIntervall(Config.SENSORCHECKER_INTERVAL_LANDING);

    if( sensor_checker == null )
    {
      sensor_checker = new Timer();
      sensor_checker.scheduleAtFixedRate(new SensorStateUpdateTask(), 0, Config.SENSORCHECKER_INTERVAL_LANDING);
    }
    else
    {
      PegasosLog.e(getClass().getCanonicalName(), "Starting already started sensor checker task");
    }
  }

  private void stopSensorCheckerTask()
  {
    sensors.stopCheckingSensorState();
    if( sensor_checker != null )
    {
      sensor_checker.cancel();
      sensor_checker= null;
    }
    else
    {
      PegasosLog.e(getClass().getCanonicalName(), "Stopping already stopped sensor checker task");
    }
  }

  private void updateSensorTypes()
  {
    // TODO: check if change of status is necessary
    sensors.setAntPlusStatus(switchManager.getAntEnabled());
    sensors.setBLEStatus(switchManager.getBleEnabled() && ui.isBleCapable());

    gps= sensors.isUsingGps();
    hr= sensors.isUsingHr();
    fp= sensors.isUsingFootPod();

    extras= sensors.getExtraSensorTypeNames();
    texts= new String[extras.length];

    using_extras= new boolean[3];

    try
    {
      switch( extras.length )
      {
        case 0:
          using_extras[0]= using_extras[1]= using_extras[2]= false;
          break;
        case 1:
          using_extras[0]= true;
          using_extras[1]= using_extras[2]= false;
          break;

        case 2:
          using_extras[0]= using_extras[1]= true;
          using_extras[2]= false;
          break;

        case 3:
          using_extras[0]= using_extras[1]= using_extras[2]= true;
          break;

        default:
          using_extras[0]= using_extras[1]= using_extras[2]= true;
          break;
      }
    }
    catch( IllegalArgumentException e )
    {
      e.printStackTrace();
      ACRA.getErrorReporter().handleException(e);
    }

    ui.updateSensorTypes(extras);
    PegasosLog.d("TrainingLanding", "Showing " + gps + " " + hr + " " + fp + " " + using_extras[0] + " " + using_extras[1] + " " + using_extras[2]);
  }

  public SensorControllerService getSensorService()
  {
    return sensors;
  }
  
  /**
   * Check if starting training is possible from a sensors perspective (i.e. if
   * all required sensors are ok)
   * 
   * @return true if starting training is possible
   */
  public boolean canStartSensors()
  {
    return sensors.allRequiredSensorsOk();
  }
}
