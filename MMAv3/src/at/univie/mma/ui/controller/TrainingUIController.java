package at.univie.mma.ui.controller;

import android.annotation.SuppressLint;

import at.pegasos.client.*;
import at.pegasos.client.controller.*;
import at.pegasos.client.sensors.*;
import at.pegasos.client.sports.*;
import at.pegasos.client.util.*;
import org.acra.ACRA;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import at.univie.mma.Config;
import at.univie.mma.ServerCallbackTraining;
import at.univie.mma.Soundmachine;
import at.univie.mma.serverinterface.ServerInterface;
import at.univie.mma.ui.TimeoutUtil;
import at.pegasos.client.*;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.ui.TrainingActivity;
import at.pegasos.client.values.*;

public class TrainingUIController {

  private TrainingActivity ui;

  public static TrainingUIController getInstance()
  {
    if( instance == null )
      instance= new TrainingUIController();
    return instance;
  }

  private static TrainingUIController instance;

  private boolean running= false;

  /**
   * Controller ID as specified in the list of available controllers
   */
  private int activity_c;

  private String controller_argument;
  private boolean controller_argument_set;

  /**
   * Name of the sport as specified in the list of available sports
   */
  private String sportname;

  /**
   * Timestamp when the activity was started
   */
  private long starttime;

  private boolean started;

  private ServerInterface server;

  private SensorControllerService sensors;

  private ServerCallbackTraining callback;

  private ITrainingController controller;
  private Sport sport;

  private Soundmachine soundmachine;

  private TimeoutUtil timeoutUtil;

  public File getDataFile()
  {
    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf= new SimpleDateFormat("yyy-MM-dd_HHmmss");
    String date_string= sdf.format(new Date(starttime));
    String filename= date_string + ".ser";

    return new File(PegasosClient.getDirData(), filename);
  }

  /**
   * Open the file for storing data and set it for the server interface
   */
  private void openFile()
  {
    try
    {
      server.setSensorDataFile(new FileOutputStream(getDataFile()));
    }
    catch( FileNotFoundException e )
    {
      PegasosLog.e("Training", "FileNotFoundException: " + e.getLocalizedMessage());
      ui.showToast(e.getLocalizedMessage());
      e.printStackTrace();
      ACRA.getErrorReporter().handleException(e);
    }
  }

  /**
   * Open the file of the last training session again
   */
  public void reopenDataFile()
  {
    try
    {
      ServerInterface.getInstance().setSensorDataFile(new FileOutputStream(getDataFile(), true));
    }
    catch( FileNotFoundException e )
    {
      PegasosLog.e("Training", "FileNotFoundException: " + e.getLocalizedMessage());
      ui.showToast(e.getLocalizedMessage());
      e.printStackTrace();
      ACRA.getErrorReporter().handleException(e);
    }
  }

  private void writeMetadata()
  {
    new Thread() {

      // we can suppress this lint as we really want this dateformat
      // @SuppressLint("SimpleDateFormat")
      @Override
      public void run()
      {
        BufferedWriter writer= null;
        try
        {
          SimpleDateFormat sdf= new SimpleDateFormat("yyy-MM-dd_HHmmss");
          final String date_string= sdf.format(new Date(starttime));
          writer= new BufferedWriter(new FileWriter(new File(PegasosClient.getDirData(), date_string + ".meta")));
          writer.write("Starttime:" + starttime + "\n");
          LoginData login= LoginData.getLoginData();
          if( login.username != null )
          {
            writer.write("User:" + login.username + "\n");
            writer.write("club:" + login.clubsession + "\n");
          }
          else
          {
            ACRA.getErrorReporter().handleException(new Exception("Das sollte nicht passieren. LoginData.username ist null. War davor ein anderer Crash?"));
            writer.write("User:\n");
            writer.write("club:\n");
          }
          writer.write("Activity:" + activity_c + "\n");
          writer.write("Arguments:" + controller_argument + "\n");
          writer.write("Param0:" + controller.getActivity() + "\n");
          writer.write("Param1:" + controller.getParam1() + "\n");
          writer.write("Param2:" + controller.getParam2() + "\n");
          writer.write("ServerInterface:" + ServerInterface.protocolVersion + "\n");
          writer.write("timezone:" + (TimeZone.getDefault().getOffset(starttime) / 1000 / 60 / 60 * 2) + "\n");
          // Writing the sessionid at this point does not work as we might not yet have received it
          // writer.write("SessionID:" + server.getSessionId() + "\n");
        }
        catch( Exception e )
        {
          e.printStackTrace();
          ACRA.getErrorReporter().handleException(e);
        }
        finally
        {
          try
          {
            if( writer != null )
              writer.close();
          }
          catch( Exception e )
          {
            e.printStackTrace();
            ACRA.getErrorReporter().handleException(e);
          }
        }
      }
    }.start();
  }

  public void OnSessionIdReceived()
  {
    new Thread() {

      // we can suppress this lint as we really want this dateformat
      @SuppressLint("SimpleDateFormat")
      @Override
      public void run()
      {
        BufferedWriter writer= null;
        try
        {
          SimpleDateFormat sdf= new SimpleDateFormat("yyy-MM-dd_HHmmss");
          final String date_string= sdf.format(new Date(starttime));
          writer= new BufferedWriter(new FileWriter(new File(PegasosClient.getDirData(), date_string + ".meta"), true));
          // Writing the sessionid at this point does not work as we might not yet have received it
          writer.write("SessionID:" + server.getSessionId() + "\n");
        }
        catch( Exception e )
        {
          e.printStackTrace();
          ACRA.getErrorReporter().handleException(e);
        }
        finally
        {
          try
          {
            if( writer != null )
              writer.close();
          }
          catch( Exception e )
          {
            e.printStackTrace();
            ACRA.getErrorReporter().handleException(e);
          }
        }
      }
    }.start();
  }

  private void startTraining()
  {
    PegasosLog.d("Training", "" + sensors);
    PegasosLog.d("Training", "" + server);

    // Stop running background tasks of SensorControllerService
    sensors.stopCheckingSensorState();
    sensors.stopSearchingNewSensors();

    // Reset sensor metrics (distance, etc.)
    sensors.resetMetrics();

    Sensor[] sens= sensors.getSensors();
    server.setSensorCount(sens.length);

    if( sens.length == 0 )
    {
      soundmachine.say_it("Keine Sensoren verbunden. Training wird abgebrochen");
      stopTraining();
      return;
    }

    if( this.controller_argument_set )
      controller.setActivity(this.activity_c, this.controller_argument);
    else
      controller.setActivity(this.activity_c);

    this.activity_c= controller.getActivity();
    int param1 = controller.getParam1();
    int param2= controller.getParam2();

    PegasosLog.d("Training", "Starting Activity " + activity_c + " distance: " + param2 + " param2_set:" + controller_argument_set);

    server.startActivity(activity_c, param1, param2);
    started= true;

    for(int i= 0; i < sens.length; i++)
    {
      Sensor s= sens[i];
      PegasosLog.d("Training", s.getClass().getName());
      if( s.getServerSensor() != null )
        server.attachSensor(s.getServerSensor());
      else
      {
        PegasosLog.d("Training", s.getClass().getName() + " has no ServerSensor");
      }
    }

    callback.setController(controller);

    server.startSending();
    server.startSensorTime(starttime);

    PegasosLog.d("Training", "Number of sensors: " + sens.length);
    for(int i= 0; i < sens.length; i++)
    {
      Sensor s= sens[i];
      PegasosLog.d("Training", "Start sensor " + s.getClass().getName());
      s.startSensor();
    }

    ValueStore.getInstance().resetMetrics();

    if( !server.isConnected() && Config.CAN_START_OFFLINE )
      controller.onSessionStarted(true);
    else
    {
      timeoutUtil.start();
    }

    running= true;
  }

  /**
   * Request the stop of the training. This will also trigger the stop on the user interface.
   * The user interface in turn will have to call OnTrainingStopped once it is done with shutting down
   */
  public void stopTraining()
  {
    PegasosLog.d("TrainingActivity", "Stop Training");

    soundmachine.blockingWaitForSpeaking();

    ui.OnStopTraining();

    GarbageCollector.stop();
  }

  /**
   * Lifecycle method to be invoked once a training has been fully stopped
   */
  public void OnTrainingStopped()
  {
    running= false;

    ITrainingController controller= TrainingUIController.getInstance().getController();
    if( controller != null )
      controller.onSessionStopped();

    if( sensors != null )
      sensors.stopService();

    if( ServerInterface.getInstance() != null )
      ServerInterface.getInstance().closeSensorDataFile();

    if( started )
    {
      (new Thread() {
        public void run()
        {
          ServerInterface.getInstance().stopActivity();
          ServerInterface.getInstance().detachSensors();
          ServerInterface.getInstance().stopSending(true);
          if( !ServerInterface.getInstance().isConnected() )
          {
            if( ServerInterface.getInstance().queueSize() > 0 )
            {
              PegasosClient.getInstance().setTrainingUploadRequired(true);
            }
          }
        }
      }).start();
    }
    else
    {
      ServerInterface.getInstance().detachSensors();

      (new Thread() {
        public void run()
        {
          ServerInterface.getInstance().stopSending(true);
          if( !ServerInterface.getInstance().isConnected() )
          {
            if( ServerInterface.getInstance().queueSize() > 0 )
            {
              PegasosClient.getInstance().setTrainingUploadRequired(true);
            }
          }
        }
      }).start();
    }
  }

  /**
   * Begin the start process of the Training or resetup stuff when the UI has been destroyed but the training has not been stopped
   */
  public boolean onUiStart()
  {
    // Get Server and set callback
    server= ServerInterface.getInstance();

    callback= new ServerCallbackTraining(ui);
    server.removeResponseCallback(ServerCallback.class);
    server.addResponseCallback(callback);

    // Check whether we're recreating a previously destroyed instance
    if( running )
    {
      callback.setController(controller);
      sport.setupAdditionalScreens(ui);

      // Recreate the UI for the activity
      controller.initUI(ui);

      return true;
    }
    else
    {
      TrainingControllerFactory fact= new TrainingControllerFactory();
      controller= fact.getControllerForActivity(activity_c);
      
      // Create the UI for the activity
      controller.initUI(ui);

      sensors= SensorControllerService.getInstance();
      sport= SportFactory.getSportFromString(sportname);
      sport.setupAdditionalScreens(ui);

      return false;
    }
  }
  
  public void start()
  {
    if( !running )
    {
      starttime= System.currentTimeMillis();

      openFile();
      startTraining();
      writeMetadata();
    }
  }

  public ITrainingController getController()
  {
    return controller;
  }

  public long getStartTime()
  {
    return starttime;
  }

  /**
   * Set the parameters for this training
   * 
   * @param activity_c
   *          ID of the controller as listed in the list of available
   *          controllers
   * @param cargument
   *          parameters for the controller. null if no parameters are to be
   * @param sport
   *          name of the sport
   */
  public void setParams(int activity_c, String cargument, String sport)
  {
    this.activity_c= activity_c;
    this.controller_argument= cargument;
    this.controller_argument_set= cargument != null;
    this.sportname= sport;
  }

  /**
   * Callback method to be invoked when the starting of the training fails
   */
  public void onStartTimedOut()
  {
    running= false;
  }

  public void setTimeoutUtil(TimeoutUtil timeoutUtil)
  {
    this.timeoutUtil= timeoutUtil;
  }

  public void setSoundmachine(Soundmachine soundmachine)
  {
    this.soundmachine= soundmachine;
  }

  public void setUi(TrainingActivity ui)
  {
    this.ui = ui;
  }

  public Sport getSport() {
    return sport;
  }

  public boolean isRunning()
  {
    return running;
  }

  public ServerCallbackTraining getServerCallback()
  {
    return callback;
  }
}
