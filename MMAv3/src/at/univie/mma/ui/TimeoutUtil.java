package at.univie.mma.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import at.pegasos.client.ui.*;
import at.univie.mma.R;

public class TimeoutUtil {
  private TimeoutListener timeoutListener;
  private PegasosClientView activity;
  private Dialog progressDialog;
  private Timer timer;
  private int time;
  private boolean showDialog;
  private boolean open;
  private String waitMessage;
  private String waitTitle;
  private String failMessage;
  private boolean istraining;
  
  public interface TimeoutListener {
    /**
     * Called when the timer runs out.
     */
    void onTimeout(TimeoutUtil util);

    /**
     * Called when the user has acknowledged the timeout using the dialog.
     * This callback is not invoked if no dialog has been shown
     */
    void onTimeoutAcknowledged();
  }
  
  /**
   * Utility class for starting a timer including dialogs for timeouts and aborts.
   *
   * @param activity
   *          The activity which starts the timer.
   * @param timeoutListener
   *          Listener for callbacks.
   * @param time
   *          The time after which the timer runs out.
   * @param showDialog
   *          If a dialog should be shown in case the timeout has been reached. True to show a
   *          dialog with confirmation button, false to show a short toast.
   * @param waitTitle
   *          The title for the waiting dialog.
   * @param waitMessage
   *          The message for the waiting dialog.
   * @param failMessage
   *          The message shown when the timer runs out.
   */
  public TimeoutUtil(PegasosClientView activity, TimeoutListener timeoutListener, int time, boolean showDialog, final String waitTitle,
      final String waitMessage, final String failMessage)
  {
    this.activity= activity;
    this.timeoutListener= timeoutListener;
    this.time= time;
    this.waitMessage= waitMessage;
    this.waitTitle= waitTitle;
    this.failMessage= failMessage;
    this.showDialog= showDialog;
    open= false;
    istraining= ((activity instanceof TrainingLandingScreen) || (activity instanceof TrainingActivity) ? true : false);
  }
  
  public void setShowDialog(boolean showDialog)
  {
    this.showDialog= showDialog;
  }
  
  /**
   * Starts the timer and shows the respective dialogs/toasts.
   *
   */
  public void start()
  {
    // Create the dialog
    activity.runOnUiThread(new Runnable() {
      @Override
      public void run()
      {
        if( istraining )
          progressDialog= new Dialog(activity, R.style.TrainingDialogTheme);
        else
          progressDialog= new Dialog(activity);
        progressDialog.setContentView(R.layout.dialog_progress);
        if( waitTitle != null )
          progressDialog.setTitle(waitTitle);
        if( waitMessage != null )
          ((TextView) progressDialog.findViewById(R.id.txtMessage)).setText(waitMessage);
        progressDialog.show();
      }
    });
    
    // Start the timer
    timer= new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run()
      {
        timeoutListener.onTimeout(TimeoutUtil.this);
        activity.runOnUiThread(new Runnable() {
          @Override
          public void run()
          {
            progressDialog.dismiss();
            if( showDialog )
            {
              AlertDialog.Builder builder;
              if( istraining )
                builder= new AlertDialog.Builder(activity, R.style.TrainingDialogTheme);
              else
                builder= new AlertDialog.Builder(activity);
              builder.setMessage(failMessage);
              builder.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                  timeoutListener.onTimeoutAcknowledged();
                }
              });
              builder.create().show();
            }
            else
            {
              activity.showToast(failMessage);
              // timeoutListener.onTimeout();
            }
            open= false;
          }
        });
      }
    }, time);
    open= true;
  }
  
  /**
   * Called to abort the timer and show an optional message.
   *
   * @param message
   *          Optional toast message, set to null if unused.
   */
  public void abort(@Nullable final String message)
  {
    if( open )
    {
      timer.cancel();
      progressDialog.dismiss();
      if( message != null )
      {
        activity.showToast(message);
      }
    }
  }
  
  /**
   * Called to abort the timer without showing a message
   */
  public void abort()
  {
    abort(null);
  }
}