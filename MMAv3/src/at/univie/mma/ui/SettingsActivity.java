package at.univie.mma.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import at.pegasos.client.ui.PegasosClientView;
import at.univie.mma.R;

public class SettingsActivity extends PegasosClientView {
  
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    
    getSupportActionBar().setTitle(R.string.activity_settings);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
    getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
  }
}
