package at.univie.mma.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import at.univie.mma.utils.colormaps.JetColorMap;
import at.univie.mma.utils.colormaps.MMAColorMap;

public abstract class ValueView extends View
{
  protected static String LOG_TAG= "";
  
  protected double value= 0;
  protected double minValue= -50;
  protected double maxValue= 50;
  
  protected MMAColorMap colorMap= new JetColorMap();
  
  public ValueView(Context context)
  {
    super(context);
  }
  
  public ValueView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }
  
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
  {
    int widthMode= MeasureSpec.getMode(widthMeasureSpec);
    int widthSize= MeasureSpec.getSize(widthMeasureSpec);
    int heightMode= MeasureSpec.getMode(heightMeasureSpec);
    int heightSize= MeasureSpec.getSize(heightMeasureSpec);
    int width, height;
    if( widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST )
      width= widthSize;
    else
      width= -1;
    if( heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST )
      height= heightSize;
    else
      height= -1;
    setMeasuredDimension(width, height);
  }
  
  public void setColorMap(MMAColorMap colorMap)
  {
    this.colorMap= colorMap;
    invalidate();
  }
  
  public void setMinMaxValue(double minValue, double maxValue)
  {
    this.minValue= minValue;
    this.maxValue= maxValue;
    invalidate();
  }
  
  public void setValue(double value)
  {
    if( value > maxValue )
      this.value= maxValue;
    else if( value < minValue )
      this.value= minValue;
    else
      this.value= value;
    invalidate();
  }
  
}
