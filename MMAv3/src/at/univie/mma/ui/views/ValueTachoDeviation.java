package at.univie.mma.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;

public class ValueTachoDeviation extends ValueTacho
{
  
  private Paint colorOLinePaint;
  private float optimal_start;
  private float optimal_amount;

  public ValueTachoDeviation(Context context)
  {
    super(context);
  }
  
  public ValueTachoDeviation(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }
    
  @Override
  protected void onDraw(Canvas canvas)
  {
    super.onDraw(canvas);
    
    // Draw Optimal Area
    drawOptimal(canvas);
  }
  
  private void drawOptimal(Canvas canvas)
  {
    RectF smallOval= getOval(canvas, 0.7f);
    
    canvas.drawArc(smallOval, optimal_start, optimal_amount, false, colorOLinePaint);
  }
  
  @Override
  protected void init()
  {
    super.init();
    LOG_TAG= ValueTachoDeviation.class.getSimpleName();
    
    colorOLinePaint= new Paint(Paint.ANTI_ALIAS_FLAG);
    colorOLinePaint.setStyle(Paint.Style.STROKE);
    colorOLinePaint.setStrokeWidth(Math.round(lineThickness*0.85));
    colorOLinePaint.setColor(Color.rgb(180, 0, 0));
  }
  
  /**
   * Set range for which should be marked 'optimal' in degrees
   * Normal range starts at 190 and runs for 160 degrees
   * 
   * @param optimal_start
   * @param optimal_amount
   */
  public void setOptimal(float optimal_start, float optimal_amount)
  {
    this.optimal_start= optimal_start;
    this.optimal_amount= optimal_amount;
  }
  
  /**
   * Set range which should be marked 'optimal'. This range is specified in
   * relation to the toal amount (maximum)
   * 
   * @param percent
   */
  public void setOptimal(float percent)
  {
    float degrees= 80f * percent;
    this.optimal_start= 270 - degrees;
    this.optimal_amount= degrees *2;
  }
}
