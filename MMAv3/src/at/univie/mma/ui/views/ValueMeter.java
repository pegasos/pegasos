package at.univie.mma.ui.views;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

public class ValueMeter extends ValueView
{
  
  private float usedWidthP= 0.7f;
  private float bottomOWidthP= 0.2f;
  private float compr= 0.5f;
  private float scaleWidthFactor= 1.2f;
  private int arcAngleOffset= 10;
  private int availableAngle= 180 - 2 * arcAngleOffset;
  private int tilesWidth= 2;
  private int ovalCenter;
  private int topBottomPaddingPortraitP= 10;
  private int topPaddingLandscapeP= 5;
  private int needleStrokePortrait= 10;
  private int needleStrokeLandscape= 20;
  
  private Paint needlePaint;
  private Paint smallArcPaint;
  private Paint colorLinePaint;
  
  public ValueMeter(Context context)
  {
    super(context);
    init();
  }
  
  public ValueMeter(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    init();
  }
  
  @Override
  protected void onDraw(Canvas canvas)
  {
    super.onDraw(canvas);
    canvas.drawColor(Color.TRANSPARENT); // Clear canvas
    drawScale(canvas);
    drawNeedle(canvas);
    positionText(canvas);
  }
  
  private void drawNeedle(Canvas canvas)
  {
    RectF oval= getOval(canvas, 1, scaleWidthFactor * compr);
    RectF comprOval= getOval(canvas, usedWidthP, compr);
    RectF smallOval= getOval(canvas, bottomOWidthP);
    float angle= 180 + arcAngleOffset + (float) ((value - minValue) / (maxValue - minValue) * availableAngle);
    PointF ep= getElipsePoint(angle, comprOval);
    float bottom= getHeight() - getPaddingBottom();
    smallOval.offset(0, getHeight() - 0.5f * smallOval.height() - getPaddingTop() - getPaddingBottom());
    canvas.drawLine(comprOval.centerX(), bottom, oval.centerX() - ep.x, oval.centerY() - ep.y, needlePaint);
    canvas.drawArc(smallOval, 180, 180, true, smallArcPaint);
  }
  
  private void drawScale(Canvas canvas)
  {
    RectF oval= getOval(canvas, 1, scaleWidthFactor * compr);
    RectF smallOval= getOval(canvas, usedWidthP, compr);
    for(int i= 0; i < availableAngle; i++)
    {
      colorLinePaint.setColor(colorMap.getColorPos((float) (i) / availableAngle));
      int startAngle= 180 + arcAngleOffset + i;
      PointF ep= getElipsePoint(startAngle, oval);
      PointF epsmall= getElipsePoint(startAngle, smallOval);
      Path colorPath= new Path();
      colorPath.moveTo(oval.centerX() - epsmall.x, oval.centerY() - epsmall.y);
      colorPath.lineTo(oval.centerX() - ep.x, oval.centerY() - ep.y);
      int endAngle= 180 + arcAngleOffset + i + tilesWidth;
      ep= getElipsePoint(endAngle, oval);
      epsmall= getElipsePoint(endAngle, smallOval);
      colorPath.lineTo(oval.centerX() - ep.x, oval.centerY() - ep.y);
      colorPath.lineTo(oval.centerX() - epsmall.x, oval.centerY() - epsmall.y);
      canvas.drawPath(colorPath, colorLinePaint);
    }
  }
  
  private void positionText(Canvas canvas)
  {
    RectF oval= getOval(canvas, 1, scaleWidthFactor * compr);
    ovalCenter= (int) oval.centerY();
  }
  
  private RectF getOval(Canvas canvas, float factor)
  {
    return getOval(canvas, factor, 1);
  }
  
  private RectF getOval(Canvas canvas, float factor, float compression)
  {
    return getOval(canvas, factor, compression, 0);
  }
  
  private RectF getOval(Canvas canvas, float factor, float compression, float yoffset)
  {
    RectF oval;
    final int canvasWidth= canvas.getWidth() - getPaddingLeft() - getPaddingRight();
    oval= new RectF(0, 0, canvasWidth * factor, canvasWidth * factor * compression);
    oval.offset((canvasWidth - oval.width()) / 2 + getPaddingLeft(), yoffset + getPaddingTop());
    return oval;
  }
  
  private PointF getElipsePoint(float angle, RectF oval)
  {
    float a= oval.width() * 0.5f;
    float b= oval.height() * 0.5f;
    double rad= angle * Math.PI / 180.0f;
    double tan= Math.tan(rad);
    double denominator= Math.sqrt(Math.pow(b, 2) + Math.pow(a, 2) * Math.pow(tan, 2));
    float x= (float) (a * b / denominator);
    float y= (float) (a * b * tan / denominator);
    if( (angle < 90) || (angle > 270) )
    {
      x= -x;
      y= -y;
    }
    return new PointF(x, y);
  }
  
  private void init()
  {
    LOG_TAG= ValueTacho.class.getSimpleName();
    
    if( Build.VERSION.SDK_INT >= 11 && !isInEditMode() )
    {
      setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }
    colorLinePaint= new Paint(Paint.ANTI_ALIAS_FLAG);
    colorLinePaint.setStyle(Paint.Style.FILL);
    smallArcPaint= new Paint(Paint.ANTI_ALIAS_FLAG);
    smallArcPaint.setStyle(Paint.Style.FILL);
    smallArcPaint.setColor(Color.rgb(127, 127, 127));
    needlePaint= new Paint(Paint.ANTI_ALIAS_FLAG);
    needlePaint.setStyle(Paint.Style.STROKE);
    needlePaint.setColor(Color.argb(200, 255, 0, 0));
    setPadding(getResources().getConfiguration());
  }
  
  @Override
  public void onConfigurationChanged(Configuration newConfig)
  {
    super.onConfigurationChanged(newConfig);
    setPadding(newConfig);
  }
  
  private void setPadding(Configuration config)
  {
    if( config.orientation == Configuration.ORIENTATION_PORTRAIT )
    {
      setPadding(0, (int) (getResources().getDisplayMetrics().heightPixels * topBottomPaddingPortraitP / 100.0), 0,
          (int) (getResources().getDisplayMetrics().heightPixels * topBottomPaddingPortraitP / 100.0));
      needlePaint.setStrokeWidth(needleStrokePortrait);
    }
    else
    {
      setPadding(0, (int) (getResources().getDisplayMetrics().heightPixels * topPaddingLandscapeP / 100.0), 0, 0);
      needlePaint.setStrokeWidth(needleStrokeLandscape);
    }
  }
  
  public int getBottomPosition()
  {
    return getHeight() - getPaddingBottom();
  }
  
  public int getOvalCenter()
  {
    return ovalCenter;
  }
  
}
