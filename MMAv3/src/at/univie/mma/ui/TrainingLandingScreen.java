package at.univie.mma.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import at.pegasos.client.ui.*;
import org.acra.ACRA;

import at.univie.mma.Config;
import at.univie.mma.R;
import at.pegasos.client.sensors.*;
import at.univie.mma.sensors.SensorTypeNames;
import at.univie.mma.ui.controller.SensorSwitchManager;
import at.univie.mma.ui.controller.SensorSwitchManager.SensorSwitchManagerCallback;
import at.univie.mma.ui.controller.SensorSwitchManagerAndroid;
import at.univie.mma.ui.controller.TrainingLandingScreenUIController;
import at.univie.mma.ui.controller.TrainingUIController;
import at.univie.mma.ui.dialog.CalibrationListDialog;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH_ADMIN;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class TrainingLandingScreen extends PegasosClientView implements SensorSwitchManagerCallback {

  private int stateImage(Sensor.State state)
  {
    switch( state )
    {
      case STATE_OK:
      case STATE_CONNECTED:
        return R.drawable.connected;
      case STATE_NOT_CONNECTED:
        return R.drawable.disconnected;
      case STATE_CONNECTING:
        return R.drawable.connecting;
      case STATE_SEARCHING:
        return R.drawable.search;
      default:
        throw new IllegalArgumentException("Illegal Argument");
    }
  }

  public static final String EXTRA_ACTIVITY= "ACTIVITY";
  public static final String EXTRA_SPORT= "SPORT";
  public static final String EXTRA_ARGUMENTS = "DISTANCE";
  public static final String EXTRA_AUTOSTART= "AUTOSTART";

  // need to be processed in onRequestPermissionsResult
  public static final String[] PERMISSIONS= {WRITE_EXTERNAL_STORAGE,
      BLUETOOTH_ADMIN, ACCESS_FINE_LOCATION};

  private int activity_c;
  private String arguments;
  private String sport;

  private TextView txtGps;
  private TextView txtHR;
  private TextView txtFP;
  private TextView txtExtra1;
  private TextView txtExtra2;
  private TextView txtExtra3;

  private Button btnStartStop;
  private Button btnCont;
  private Button btnStop;
  private ImageButton btnCalibration;
  private ImageView imgStatusGps;
  private ImageView imgStatusFP;
  private ImageView imgStatusHR;
  private ImageView[] imgStatusExtras;

  private SensorSwitchManager switchManager;

  private TrainingLandingScreenUIController controller;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_training_landing_screen);

    Intent i= getIntent();
    this.activity_c= i.getIntExtra(EXTRA_ACTIVITY, 0);
    sport= i.getStringExtra(EXTRA_SPORT);

    /**
     * Use this flag to auto-start a training. Use it only once ie when the start
     * was requested. Not if the user returns. ie only change it onCreate
     */
    boolean autostart_requested = i.getBooleanExtra(EXTRA_AUTOSTART, false);

    // check whether extra argument parameter were passed onto the current activity.
    // if yes --> pass along
    arguments= i.getStringExtra(EXTRA_ARGUMENTS);

    switchManager= new SensorSwitchManagerAndroid();
    switchManager.setup(this, this);

    // Setup controller for the UI
    controller= new TrainingLandingScreenUIController(this, autostart_requested, sport);

    initGuiFields();

    checkAndRequestPermissions();

    controller.startController();
  }

  /**
   * Set the state of the start button
   *
   * @param required_ok
   *          whether all required sensors are ok
   * @param visible
   *          whether the start button should be visible
   */
  public void setStartBtnState(boolean required_ok, boolean visible)
  {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        btnStartStop.setTextColor(required_ok ? Color.BLACK : Color.GRAY);
        if( !visible )
          btnStartStop.setVisibility(android.view.View.INVISIBLE);
      }
    });
  }

  public void setStartContinueButtonState(boolean started)
  {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if( started )
        {
          btnStartStop.setVisibility(View.INVISIBLE);
          btnStop.setVisibility(View.VISIBLE);
          btnCont.setVisibility(android.view.View.VISIBLE);
        }
        else
        {
          btnStartStop.setVisibility(View.VISIBLE);
          btnStop.setVisibility(View.INVISIBLE);
          btnCont.setVisibility(android.view.View.INVISIBLE);
        }
      }
    });
  }

  private void initGuiFields()
  {
    txtGps= findViewById(R.id.txtGps);
    txtHR= findViewById(R.id.txtHr);
    txtFP= findViewById(R.id.txtFootpod);
    txtExtra1= findViewById(R.id.txtExtra1);
    txtExtra2= findViewById(R.id.txtExtra2);
    txtExtra3= findViewById(R.id.txtExtra3);

    btnStartStop= findViewById(R.id.btnStartStop);
    if( Build.VERSION.SDK_INT == 19 )
    {
      btnStartStop.setVisibility(View.INVISIBLE);

      RelativeLayout layout= findViewById(R.id.training_landing_screen);
      android.support.v7.view.ContextThemeWrapper wrappedContext = new android.support.v7.view.ContextThemeWrapper(layout.getContext(), R.style.TrainingButtonStyle);
      Button b= new Button(wrappedContext, null, 0);
      b.setText(R.string.StartTraining);

      RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
              RelativeLayout.LayoutParams.WRAP_CONTENT);
      params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
      params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

      layout.addView(b, params);
    /*
        android:layout_marginBottom="0dp"
        android:layout_marginRight="5dp"
        android:layout_marginEnd="5dp"
     */

      btnStartStop= b;
    }

    btnStartStop.setOnClickListener(onStartClicked);

    imgStatusGps= findViewById(R.id.statusGPS);
    imgStatusFP= findViewById(R.id.statusFootPod);
    imgStatusHR= findViewById(R.id.statusHR);
    imgStatusExtras= new ImageView[3];
    imgStatusExtras[0]= findViewById(R.id.statusExtra1);
    imgStatusExtras[1]= findViewById(R.id.statusExtra2);
    imgStatusExtras[2]= findViewById(R.id.statusExtra3);

    btnCalibration= findViewById(R.id.calibrationButton);
    btnCalibration.setOnClickListener(calibrationClicked);

    btnCont= findViewById(R.id.btnContinue);
    btnStop= findViewById(R.id.buttonStop);
    btnStop.setVisibility(View.INVISIBLE);
    btnStop.setOnClickListener(onStopClicked);

    btnCont.setVisibility(android.view.View.INVISIBLE);
    btnCont.setOnClickListener(onContinueClicked);
  }

  @Override
  protected void onResume()
  {
    super.onResume();

    controller.OnResume();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    boolean stop= false;
    boolean timeout= false;
    if( data != null )
    {
      stop= data.getBooleanExtra(TrainingActivity.EXTRA_STOP_TRAINING, false);
      timeout= data.getBooleanExtra(TrainingActivity.EXTRA_SERVER_TIMEOUT, false);
    }

    if( Config.LOGGING ) Log.d("TrainingLanding", "onActivityResult: " + requestCode + " " + resultCode + " " + stop);
    if( requestCode == TrainingActivity.REQUEST_CODE_TRAINING
        && resultCode == Activity.RESULT_OK )
    {
      if( stop )
      {
        stopTraining();
        finish();
      }
    }
    else if( requestCode == TrainingActivity.REQUEST_CODE_TRAINING && resultCode == Activity.RESULT_CANCELED && timeout )
    {
      controller.OnTrainingStartTimedout();
    }
  }

  @Override
  protected String[] getRequiredPermissions()
  {
    return PERMISSIONS;
  }

  protected void onCapabilitiesChanged()
  {
    super.onCapabilitiesChanged();

    if( capabilities.isCapable(ACCESS_FINE_LOCATION) )
    {
      controller.getSensorService().startSensorsType(SensorTypeNames.GPS);
    }

    // TODO: can_ant ...
    switchManager.update(this, capabilities.isCapable(BLUETOOTH_ADMIN), true);
  }

  /**
   * Changed onPause to on Stop in order to fix http://motionadvisor.schmelz.univie.ac.at/trac/ticket/26
   * Remove this comment when resolved
   * @artsem please fix
   */
  @Override
  protected void onStop()
  {
    super.onStop();

    controller.stop();
  }

  public void setVisibilities(boolean gps, boolean hr, boolean fp, boolean using_extras[])
  {
    if( gps )
    {
      findViewById(R.id.txtGps).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgGps).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusGPS).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtGps).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgGps).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusGPS).setVisibility(View.INVISIBLE);
    }
    if( hr )
    {
      findViewById(R.id.txtHr).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgHr).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusHR).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtHr).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgHr).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusHR).setVisibility(View.INVISIBLE);
    }
    if( fp )
    {
      findViewById(R.id.txtFootpod).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgFootpod).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusFootPod).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtFootpod).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgFootpod).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusFootPod).setVisibility(View.INVISIBLE);
    }
    if( using_extras[0] )
    {
      findViewById(R.id.txtExtra1).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgExtra1).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusExtra1).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtExtra1).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgExtra1).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusExtra1).setVisibility(View.INVISIBLE);
    }
    if( using_extras[1] )
    {
      findViewById(R.id.txtExtra2).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgExtra2).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusExtra2).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtExtra2).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgExtra2).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusExtra2).setVisibility(View.INVISIBLE);
    }
    if( using_extras[2] )
    {
      findViewById(R.id.txtExtra3).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.imgExtra3).setVisibility(android.view.View.VISIBLE);
      findViewById(R.id.statusExtra3).setVisibility(View.VISIBLE);
    }
    else
    {
      findViewById(R.id.txtExtra3).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.imgExtra3).setVisibility(android.view.View.INVISIBLE);
      findViewById(R.id.statusExtra3).setVisibility(View.INVISIBLE);
    }
  }

  private OnClickListener onStartClicked= new OnClickListener() {

    @Override
    public void onClick(View v)
    {
      // Check the rare occasion that a sensor just flipped its status
      boolean canStart= controller.canStartSensors();
      if( canStart )
      {
        startTraining(false);
      }
      else
      {
        showToast(R.string.required_sensors_unavailable);
      }
    }
  };

  private OnClickListener onStopClicked= new OnClickListener() {

    @Override
    public void onClick(View v)
    {
      Log.e("ACTIVITY", "Stop it?????");

      stopTraining();

      finish();
    }
  };

  private OnClickListener calibrationClicked= new OnClickListener() {
    @Override
    public void onClick(View view)
    {
      Dialog dlg= CalibrationListDialog.CreateDialog(TrainingLandingScreen.this, controller.getSensorService());
      if( dlg != null )
        dlg.show();
      else
        showToast(getString(R.string.message_no_calibration_avail));
    }
  };

  private void stopTraining()
  {
    TrainingUIController.getInstance().OnTrainingStopped();
  }

  @Override
  public void onBackPressed()
  {
    stopTraining();

    super.onBackPressed();
  }

  private OnClickListener onContinueClicked= new OnClickListener() {

    @Override
    public void onClick(View v)
    {
      startTraining(true);
    }
  };

  /**
   * Callback method to start training. The current implementation features two
   * use cases: (1) being called by OnClickListener or as a result of
   * checkAutoStart from the controller
   *
   * @param continued
   */
  public void startTraining(boolean continued)
  {
    controller.OnStartTraining(continued);

    Intent i= new Intent(getApplicationContext(), TrainingActivity.class);
    i.putExtra(TrainingActivity.EXTRA_ACTIVITY, activity_c);
    i.putExtra(TrainingActivity.EXTRA_SPORT, sport);

    // check wheter a distance parameter was passed onto the current activity.
    // if yes --> pass along
    if( arguments != null )
    {
      i.putExtra(TrainingActivity.EXTRA_ARGUMENT, arguments);
    }

    startActivityForResult(i, TrainingActivity.REQUEST_CODE_TRAINING);
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig)
  {
    super.onConfigurationChanged(newConfig);

    setContentView(R.layout.activity_training_landing_screen);

    initGuiFields();

    controller.OnResume();

    // TODO: can_ant ...
    switchManager.update(this, capabilities.isCapable(BLUETOOTH_ADMIN), true);
  }

  /**
   * Callback method (from uicontroller) for setting the displayed sensor status
   *
   * @param sensorNumber number of the sensor (0 gps, 1 hr, 2 fp, 3: extra[0], ...)
   * @param text Text to be displayed
   * @param state State to be displayed
   */
  public void setSensorStatus(final int sensorNumber, final String text, final Sensor.State state)
  {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if( sensorNumber == 0 )
        {
          txtGps.setText(text);
          imgStatusGps.setImageResource(stateImage(state));
        }
        else if( sensorNumber == 1 )
        {
          txtHR.setText(text);
          imgStatusHR.setImageResource(stateImage(state));
        }
        else if( sensorNumber == 2 )
        {
          txtFP.setText(text);
          imgStatusFP.setImageResource(stateImage(state));
        }
        else if( sensorNumber == 3 )
        {
          txtExtra1.setText(text);
          imgStatusExtras[0].setImageResource(stateImage(state));
        }
        else if( sensorNumber == 4 )
        {
          txtExtra2.setText(text);
          imgStatusExtras[1].setImageResource(stateImage(state));
        }
        else if( sensorNumber == 5 )
        {
          txtExtra3.setText(text);
          imgStatusExtras[2].setImageResource(stateImage(state));
        }
      }
    });
  }

  public void updateSensorTypes(String[] extras)
  {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int id;

        try {
          switch (extras.length) {
            case 0:
              break;
            case 1:
              id = (Integer) Sensors.class.getDeclaredField(extras[0] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra1)).setImageResource(id);
              break;

            case 2:
              id = (Integer) Sensors.class.getDeclaredField(extras[0] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra1)).setImageResource(id);
              id = (Integer) Sensors.class.getDeclaredField(extras[1] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra2)).setImageResource(id);
              break;

            case 3:
              id = (Integer) Sensors.class.getDeclaredField(extras[0] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra1)).setImageResource(id);
              id = (Integer) Sensors.class.getDeclaredField(extras[1] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra2)).setImageResource(id);
              id = (Integer) Sensors.class.getDeclaredField(extras[2] + "_icon").get(null);
              ((ImageView) findViewById(R.id.imgExtra3)).setImageResource(id);
              break;

            default:
              break;
          }
        }
        catch (IllegalAccessException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        catch (NoSuchFieldException e)
        {
          e.printStackTrace();
          ACRA.getErrorReporter().handleException(e);
        }
      }
    });
  }

  public SensorSwitchManager getSwitchManager()
  {
    return switchManager;
  }

  @Override
  public void setBleEnabled(boolean status)
  {
    controller.getSensorService().setBLEStatus(switchManager.getBleEnabled() && isBleCapable());
  }

  @Override
  public void setAntEnabled(boolean status)
  {
    controller.getSensorService().setAntPlusStatus(switchManager.getAntEnabled());
  }

  public boolean isBleCapable()
  {
    if( Build.VERSION.SDK_INT > 17 )
      return capabilities.isCapable("android.permission.BLUETOOTH_ADMIN");
    else
      return false;
  }

  public boolean canWrite()
  {
    return capabilities.isCapable(WRITE_EXTERNAL_STORAGE);
  }

  public void notifyWritePermissionRequired()
  {
    showToast(getString(R.string.permission_write_external_required));
  }
}

