package at.univie.mma.ui.sensormanager;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.pegasos.client.sensors.SensorConfigService;
import at.pegasos.client.sensors.SensorParam;
import at.pegasos.client.ui.PegasosClientView;
import at.univie.mma.ui.sensormanager.SensorAdapter.Entry;

/**
 * Activity for viewing, editing and deleting connected sensors.
 */
public class SensorManagerActivity extends PegasosClientView {
  private static final String LOG_TAG= "SensorManager";
  // need to be processed in onRequestPermissionsResult
  public static final String[] PERMISSIONS= new String[] {
      WRITE_EXTERNAL_STORAGE};

  private Resources resources;
  private InputMethodManager inputMethodManager;

  private ListView listView;
  private TextView txtNoSensors;
  private Button btnDeleteSensors;
  private Button btnCancel;
  private Button btnSelectAll;
  private LinearLayout layoutCancelSelectButtons;
  /**
   * dark transparent background overlay for darkening
   * background when showing popup windows
   */
  private RelativeLayout layoutOverlay;
  private PopupWindow popupSensorDetails;
  private SensorAdapter sensorAdapter;
  
  /**
   * list of connected sensors
   */
  private ArrayList<SensorAdapter.Entry> sensorList;
  /**
   * list of user selected (via checkboxes) sensors
   */
  private ArrayList<SensorAdapter.Entry> selectedSensors;
  /**
   * if inDeleteMode, buttons for selecting and deleting sensors are displayed
   */
  private boolean inDeleteMode;
  
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sensor_manager);

    resources= getResources();
    inputMethodManager= (InputMethodManager) getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE);
    
    getSupportActionBar().setTitle(getString(R.string.activity_sensor_manager));
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    txtNoSensors = (TextView) findViewById(R.id.noSensorsText);

    sensorList= new ArrayList<SensorAdapter.Entry>();
    selectedSensors= new ArrayList<SensorAdapter.Entry>();
    sensorAdapter= new SensorAdapter(this, sensorList, selectedSensors);

    listView= (ListView) findViewById(R.id.sensorsList);
    listView.setAdapter(sensorAdapter);
    // show popup with details on click or delete buttons if inDeleteMode
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id)
      {
        if( inDeleteMode )
        {
          SensorAdapter.Entry selectedSensor= sensorAdapter.getItem(position);
          if( selectedSensors.contains(selectedSensor) )
          {
            selectedSensors.remove(selectedSensor);
          }
          else
          {
            selectedSensors.add(selectedSensor);
          }
          changeSelectAllButtonText();
          sensorAdapter.notifyDataSetChanged();
        }
        else
        {
          showDetailsPopup(view, sensorAdapter.getItem(position));
        }
      }
    });
    listView
            .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
              @Override
              public boolean onItemLongClick(AdapterView<?> parent, View view,
                                             int position, long id)
              {
                changeMode();
                sensorAdapter.selectSensor(sensorAdapter.getItem(position));
                return true;
              }
            });

    btnDeleteSensors = (Button) findViewById(R.id.btn_delete_sensors);
    btnDeleteSensors.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        showDeleteDialog();
      }
    });

    btnCancel = (Button) findViewById(R.id.btn_cancel_delete);
    btnCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        changeMode();
      }
    });

    btnSelectAll = (Button) findViewById(R.id.btn_selectall_delete);
    btnSelectAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        selectAll();
      }
    });

    layoutCancelSelectButtons = (LinearLayout) findViewById(
            R.id.cancel_select_buttons);

    layoutOverlay = (RelativeLayout) findViewById(R.id.overlay);

    inDeleteMode= false;
    
    checkAndRequestPermissions();
    
    refresh();
  }

  @Override
  public void onResume()
  {
    super.onResume();
    refresh();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    getMenuInflater().inflate(R.menu.sensor_manager, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    int id= item.getItemId();

    if( id == R.id.action_add_sensor )
    {
      Intent i= new Intent(this, SensorManagerPairingActivity.class);

      startActivity(i);
    }
    else if( id == R.id.action_remove_sensor )
    {
      if( inDeleteMode && selectedSensors.size() > 0 )
        showDeleteDialog();
      else
        changeMode();
    }
    else if( id == R.id.action_search_sensors )
    {
      // search sensors
    }

    return true;
  }

  /**
   * Load all paired sensors and populate list view sorted alphabetically by
   * sensor type. En-/Disable "No sensors added" text accordingly.
   */
  private void refresh()
  {
    sensorList.clear();
    if( capabilities.isCapable(WRITE_EXTERNAL_STORAGE) )
    {
      sensorList.addAll(SensorListHelper.getSensorList());
      if( Config.LOGGING )
      {
        for(Entry sensor : sensorList)
        {
          Log.d("SensorManager", "paired sensor: " + sensor.type + "/" + sensor.param);
        }
      }
      
      Collections.sort(sensorList, new Comparator<SensorAdapter.Entry>() {
        @Override
        public int compare(SensorAdapter.Entry o1, SensorAdapter.Entry o2)
        {
          // return o1.getKey().compareTo(o2.getKey());
          return o1.type.compareTo(o2.type);
        }
      });
    }
    
    if( sensorList.isEmpty() )
    {
      txtNoSensors.setVisibility(View.VISIBLE);
      listView.setVisibility(View.GONE);
    }
    else
    {
      txtNoSensors.setVisibility(View.GONE);
      listView.setVisibility(View.VISIBLE);
    }

    sensorAdapter.notifyDataSetChanged();
  }

  /**
   * Changes activity into delete mode and back,
   * allowing the user to delete/unpair sensors.
   */
  private void changeMode()
  {
    selectedSensors.clear();

    if( inDeleteMode )
    {
      getSupportActionBar()
              .setTitle(getString(R.string.activity_sensor_manager));
      inDeleteMode= false;
      sensorAdapter.setEditMode(inDeleteMode);
      btnDeleteSensors.setVisibility(View.GONE);
      layoutCancelSelectButtons.setVisibility(View.GONE);
    }
    else
    {
      getSupportActionBar()
              .setTitle(getString(R.string.activity_sensor_manager_delete));
      inDeleteMode= true;
      sensorAdapter.setEditMode(inDeleteMode);
      changeSelectAllButtonText();
      btnDeleteSensors.setVisibility(View.VISIBLE);
      layoutCancelSelectButtons.setVisibility(View.VISIBLE);
    }
    refresh();
  }
  
  /**
   * Selects all sensors or if already selected, deselects all sensors.
   */
  private void selectAll()
  {
    if( selectedSensors.size() == sensorList.size() )
    {
      selectedSensors.clear();
    }
    else
    {
      selectedSensors.clear();
      selectedSensors.addAll(sensorList);
    }
    changeSelectAllButtonText();
    sensorAdapter.notifyDataSetChanged();
  }
  
  /**
   * Changes the select all button text according to the number of selected sensors.
   */
  private void changeSelectAllButtonText()
  {
    if( selectedSensors.size() == sensorList.size() )
    {
      btnSelectAll.setText(R.string.btn_deselect_all);
    }
    else
    {
      btnSelectAll.setText(R.string.btn_select_all);
    }
  }
  
  /**
   * Opens up a confirmation dialog for deleting sensors.
   */
  private void showDeleteDialog()
  {
    new AlertDialog.Builder(SensorManagerActivity.this)
            .setTitle(R.string.dialog_delete_title)
            .setMessage(R.string.dialog_delete_message)
            .setPositiveButton(android.R.string.yes,
                    new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which)
                      {
                        Log.d(LOG_TAG, selectedSensors.toString());
                        for (SensorAdapter.Entry sensor : selectedSensors) {
                          SensorConfigService.removeSensorInstance(sensor.clasz, sensor.param);
                        }
                        changeMode();
                      }
                    })
            .setNegativeButton(android.R.string.no, null).show();
  }
  
  /**
   * Opens a Android.Widget.PopupWindow displaying the selected sensor and its
   * parameters. Allows editing and adding of parameters.
   *
   * @param view
   *          The view from which this method is accessed.
   * @param sensor
   *          The sensor for which to show data.
   */
  private void showDetailsPopup(final View view,
                                final SensorAdapter.Entry sensor)
  {
    final LayoutInflater inflater= (LayoutInflater) SensorManagerActivity.this
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    final View layout= inflater.inflate(R.layout.popup_sensor_details,
            (ViewGroup) findViewById(R.id.popup_sensor_details_root));

    // parameter section, fields added dynamically later on
    final LinearLayout params= (LinearLayout) layout.findViewById(R.id.params);
    final LinearLayout sensorIDLayout= new LinearLayout(view.getContext());

    final EditText newSensorID= new EditText(view.getContext());
    final ArrayList<Map.Entry<String, EditText>> newParams= new ArrayList<Map.Entry<String, EditText>>();

    final LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.setMargins(0, 0, 0, resources
            .getDimensionPixelSize(R.dimen.popup_sensor_details_marginBottom));

    Button closeButton= (Button) layout.findViewById(R.id.closeButton);
    closeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        popupSensorDetails.dismiss();
      }
    });

    Button newParamButton= (Button) layout.findViewById(R.id.newParamButton);
    newParamButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        LinearLayout linearLayout= new LinearLayout(v.getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        final EditText newParamKey= new EditText(v.getContext());
        newParamKey.setHint(R.string.popup_new_param_key);
        linearLayout.addView(newParamKey);

        final EditText newParamValue= new EditText(view.getContext());
        newParamValue.setLayoutParams(layoutParams);
        newParamValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        new AlertDialog.Builder(view.getContext())
                .setTitle(R.string.popup_new_param_title).setView(linearLayout)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which)
                          {
                            if( newParamKey.getText() != null )
                            {
                              newParamValue.setHint(newParamKey.getText().toString());
                              newParams
                                      .add(new AbstractMap.SimpleEntry<String, EditText>(
                                              newParamKey.getText().toString(), newParamValue));
                              // add new parameter to popup window
                              buildSensorDetailsEditPopup(layout, sensor, newParams);
                            }
                            else
                            {
                              dialog.dismiss();
                            }
                          }
                        })
                .setNegativeButton(android.R.string.cancel, null).show();
      }
    });

    Button saveButton= (Button) layout.findViewById(R.id.saveButton);
    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        ArrayList<Map.Entry<String, String>> sensorID= new ArrayList<Map.Entry<String, String>>();
        sensorID.add(new AbstractMap.SimpleEntry<String, String>(null,
                newSensorID.getText().toString()));

        ArrayList<Map.Entry<String, String>> params= new ArrayList<Map.Entry<String, String>>();
        
        // add new parameters
        for(Map.Entry<String, EditText> newParam : newParams)
        {
          if( newParam.getValue().getText().toString().isEmpty() )
          {
            // use old parameter if no new input
            params.add(new AbstractMap.SimpleEntry<String, String>(
                    newParam.getKey(), sensor.param.getParam(newParam.getKey())));
          }
          else
          {
            params.add(new AbstractMap.SimpleEntry<String, String>(
                    newParam.getKey(), newParam.getValue().getText().toString()));
          }
        }

        SensorParam out= new SensorParam(sensorID, params);
  
        // overwrite old parameters with new ones
        SensorParam[] allParams= SensorConfigService.loadParams(sensor.clasz);
        int idx= Arrays.asList(allParams).indexOf(sensor.param);
        SensorConfigService.replaceSensorInstance(sensor.clasz, idx, out);

        sensor.param.sensor_id= sensorID;
        SensorAdapter.Entry newSensor= new SensorAdapter.Entry(sensor);
        newSensor.param= out;

        // refresh and rebuild popup
        refresh();
        newSensorID.setText("");
        newParams.clear();
        buildSensorDetailsPopup(layout, newSensor, newParams);
        inputMethodManager.hideSoftInputFromWindow(newSensorID.getWindowToken(),
                0);
      }
    });
    
    Button editButton= (Button) layout.findViewById(R.id.editButton);
    // remove TextViews and build layout for editing parameters
    editButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        params.removeAllViews();
        sensorIDLayout.removeAllViews();

        sensorIDLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView sensorID= new TextView(v.getContext());
        sensorID.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        sensorID.setTextColor(Color.BLACK);
        sensorID.setText(resources.getString(R.string.popup_sensor_id, ""));

        sensorIDLayout.addView(sensorID);

        newSensorID.setLayoutParams(layoutParams);
        newSensorID.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        newSensorID.setText(sensor.param.getSensorId());

        sensorIDLayout.addView(newSensorID);

        params.addView(sensorIDLayout);

        buildSensorDetailsEditPopup(layout, sensor, newParams);
      }
    });

    buildSensorDetailsPopup(layout, sensor, newParams);

    popupSensorDetails = new PopupWindow(layout,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT, true);
    popupSensorDetails.setAnimationStyle(R.style.AnimationPopupSensorDetails);
    popupSensorDetails.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
    popupSensorDetails.setOutsideTouchable(true);
    popupSensorDetails.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
    popupSensorDetails
            .setOnDismissListener(new PopupWindow.OnDismissListener() {
              @Override
              public void onDismiss()
              {
                layoutOverlay.setVisibility(View.INVISIBLE);
              }
            });

    popupSensorDetails.showAtLocation(layout, Gravity.BOTTOM, 0, 0);
    layoutOverlay.setVisibility(View.VISIBLE);
  }

  /**
   * Generates a new details popup window from the provided sensor data.
   * 
   * @param layout
   *          The layout to be used for the popup.
   * @param sensor
   *          The sensor being viewed.
   * @param newParams
   *          Previously added new parameters
   */
  private void buildSensorDetailsPopup(View layout, SensorAdapter.Entry sensor,
                                       ArrayList<Map.Entry<String, EditText>> newParams)
  {
    LinearLayout params= (LinearLayout) layout.findViewById(R.id.params);
    params.removeAllViews();
    LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.setMargins(0, 0, 0, resources
            .getDimensionPixelSize(R.dimen.popup_sensor_details_marginBottom));

    layout.findViewById(R.id.editButton).setVisibility(View.VISIBLE);
    layout.findViewById(R.id.editButtons).setVisibility(View.GONE);

    ImageView sensorTypeImg= (ImageView) layout
            .findViewById(R.id.sensorTypeImg);

    // set sensor icons
    if( sensor.icon != 0 )
      sensorTypeImg.setImageResource(sensor.icon);
    else
      sensorTypeImg.setImageResource(R.drawable.anroid_white);

    TextView sensorType= (TextView) layout.findViewById(R.id.sensorType);
    sensorType
            .setText(resources.getString(R.string.popup_sensor_type, sensor.type));

    TextView sensorID= new TextView(this);
    sensorID.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
    sensorID.setTextColor(Color.BLACK);
    sensorID.setLayoutParams(layoutParams);
    sensorID.setText(resources.getString(R.string.popup_sensor_id,
            sensor.param.getSensorId()));

    params.addView(sensorID);

    // add old parameters to layout
    for(Map.Entry<String, String> param : sensor.param.getAllParams())
    {
      TextView paramText= new TextView(this);
      paramText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
      paramText.setTextColor(Color.BLACK);
      paramText.setLayoutParams(layoutParams);
      paramText.setText(resources.getString(R.string.popup_sensor_param,
              param.getKey(), param.getValue()));

      params.addView(paramText);
    }
  
    // add previously added new parameters to layout
    for(Map.Entry<String, EditText> newParam : newParams)
    {
      TextView paramText= new TextView(this);
      paramText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
      paramText.setTextColor(Color.BLACK);
      paramText.setLayoutParams(layoutParams);
      paramText.setText(resources.getString(R.string.popup_sensor_param,
              newParam.getKey(), newParam.getValue().getText().toString()));

      params.addView(paramText);
    }
  }
  
  /**
   * Generates a new details popup window in editing mode.
   *
   * @param layout
   *          The layout to be used for the popup.
   * @param sensor
   *          The sensor being viewed.
   * @param newParams
   *          Previously added new parameters
   */
  private void buildSensorDetailsEditPopup(View layout,
                                           SensorAdapter.Entry sensor,
                                           ArrayList<Map.Entry<String, EditText>> newParams)
  {
    LinearLayout params= (LinearLayout) layout.findViewById(R.id.params);
    LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);

    layout.findViewById(R.id.editButton).setVisibility(View.GONE);
    layout.findViewById(R.id.editButtons).setVisibility(View.VISIBLE);

    // add previously added new parameters to layout
    for(Map.Entry<String, EditText> newParam : newParams)
    {
      LinearLayout row= new LinearLayout(layout.getContext());
      row.setOrientation(LinearLayout.HORIZONTAL);

      TextView label= new TextView(this);
      label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
      label.setTextColor(Color.BLACK);
      label.setLayoutParams(layoutParams);
      label.setText(resources.getString(R.string.popup_sensor_param,
              newParam.getKey(), ""));

      row.addView(label);
      row.addView(newParam.getValue());

      params.addView(row);
    }

    // add old parameters to layout, set up textfields for editing parameters
    for(Map.Entry<String, String> param : sensor.param.getAllParams())
    {
      LinearLayout row= new LinearLayout(layout.getContext());
      row.setOrientation(LinearLayout.HORIZONTAL);

      TextView label= new TextView(this);
      label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
      label.setTextColor(Color.BLACK);
      label.setLayoutParams(layoutParams);
      label.setText(
              resources.getString(R.string.popup_sensor_param, param.getKey(), ""));

      row.addView(label);

      // text field for new parameter value
      EditText editParam= new EditText(this);
      editParam.setLayoutParams(layoutParams);
      editParam.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
      editParam.setText(param.getValue());

      row.addView(editParam);

      params.addView(row);
      newParams.add(new AbstractMap.SimpleEntry<String, EditText>(
              param.getKey(), editParam));
    }
  }
  
  @Override
  protected String[] getRequiredPermissions()
  {
    return PERMISSIONS;
  }
}
