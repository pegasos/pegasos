package at.univie.mma.ui.sensormanager;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import at.univie.mma.R;
import at.pegasos.client.sensors.Sensor;
import at.pegasos.client.sensors.SensorParam;
import at.pegasos.client.ui.ParameterQuestion;

/**
 * Custom adapter for displaying sensors in a list.
 */
public class SensorAdapter extends BaseAdapter {
  public static class Entry {
    /**
     * Copy constructor
     * @param sensor
     */
    public Entry(Entry sensor)
    {
      this.icon= sensor.icon;
      this.param= sensor.param;
      this.clasz= sensor.clasz;
      this.type= sensor.type;
      this.instance= sensor.instance;

    }

    public Entry()
    {
      
    }
    
    @Override
    public boolean equals(Object o)
    {
      if( !o.getClass().equals(this.getClass()) )
        return false;
      
      return this.param.equals(((Entry)o).param);
    }
    
    /**
     * The icon which should be displayed. 0 if no icon is available
     */
    public int icon;
    /**
     * The actual param of the sensor
     */
    public SensorParam param;
    
    /**
     * Class of the sensor (as reported by Sensor.getClass().getCanonicalName())
     */
    public String clasz;
    
    /**
     * Type of the sensor as noted in SensorTypeNames
     */
    public String type;

    /**
     * A list of requested parameters.
     */
    public List<ParameterQuestion> questions;

    /**
     * The specific instance of the Sensor class.
     */
    public Sensor instance;
  }
  
  /**
   * list of all sensors
   */
	private ArrayList<Entry> sensorList;
  /**
   * list of user selected (via checkboxes) sensors
   */
	private ArrayList<Entry> selectedSensors;
	private LayoutInflater inflater;
	private boolean inEditMode;
	
	public SensorAdapter(Context context,
			ArrayList<Entry> sensorList,
			ArrayList<Entry> selectedSensors)
	{
		this.sensorList= sensorList;
		this.selectedSensors= selectedSensors;
		inflater= LayoutInflater.from(context);
		inEditMode= false;
	}
  
  /**
   * Holds GUI elements for a sensor.
   */
	private class ViewHolder {
		TextView sensorClass;
		TextView sensorID;
		ImageView sensorTypeImg;
		CheckBox deleteCheckBox;
	}
	
  @Override
  public int getCount()
  {
    return sensorList.size();
  }
  
  @Override
  public long getItemId(int position)
  {
    return position;
  }
  
  @Nullable
  @Override
  public Entry getItem(int position)
  {
    return sensorList.get(position);
  }
  
  public boolean removeItem(Entry entry)
  {
    return sensorList.remove(entry);
  }
	
  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView,
      @NonNull ViewGroup parent)
  {
    ViewHolder viewHolder;
    
    // construct view if null, else use old one
    if( convertView == null )
    {
      convertView= inflater.inflate(R.layout.item_sensor, parent, false);
      viewHolder= new ViewHolder();
      
      viewHolder.sensorID= (TextView) convertView.findViewById(R.id.sensorID);
      viewHolder.sensorClass= (TextView) convertView.findViewById(R.id.sensorType);
      viewHolder.sensorTypeImg= (ImageView) convertView.findViewById(R.id.sensorTypeImg);
      viewHolder.deleteCheckBox= (CheckBox) convertView.findViewById(R.id.deleteCheckBox);
      
      convertView.setTag(viewHolder);
    }
    else
    {
      viewHolder= (ViewHolder) convertView.getTag();
    }
    
    // set text values and icon
    Entry sensor= getItem(position);
    viewHolder.sensorID.setText("ID: " + sensor.param.getSensorId());
    viewHolder.sensorClass.setText(sensor.type);
    if( sensor.icon != 0 )
      viewHolder.sensorTypeImg.setImageResource(sensor.icon);
    else
      viewHolder.sensorTypeImg.setImageResource(R.drawable.anroid_white);
    // TODO: here needs to be something when no icon is present (ie a default icon)
    
    // display checkboxes for selecting multiple sensors
    if( inEditMode )
    {
      viewHolder.deleteCheckBox.setVisibility(View.VISIBLE);
      viewHolder.deleteCheckBox.setChecked(selectedSensors.contains(getItem(position)));
    }
    else
    {
      viewHolder.deleteCheckBox.setVisibility(View.GONE);
    }
    
    return convertView;
  }
	
	/**
	 * Call to enable checkboxes and allow for selections.
	 * 
	 * @param flag
	 *          True to enable checkboxes, false to disable them.
	 */
	protected void setEditMode(boolean flag)
	{
		selectedSensors.clear();
		inEditMode= flag;
	}

	protected void selectSensor(Entry sensor)
	{
		selectedSensors.add(sensor);
	}
}
