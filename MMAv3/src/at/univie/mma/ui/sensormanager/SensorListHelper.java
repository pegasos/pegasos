package at.univie.mma.ui.sensormanager;

import java.util.ArrayList;

import at.univie.mma.R;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.sensors.*;
import at.univie.mma.sensors.SensorTypeNames;

/**
 * Helper class for retrieving paired sensors or icons for a sensor type.
 */
public class SensorListHelper {
  /**
   * Retrieve all stored/configured/paired sensors from SensorControllerService
   */
  public static ArrayList<SensorAdapter.Entry> getSensorList()
  {
    ArrayList<SensorAdapter.Entry> sensorList= new ArrayList<SensorAdapter.Entry>();
    
    for(String sensorType : SensorTypeNames.ALL)
    {
      for(String sensorClass : SensorControllerService.getSensorClassNamesForType(sensorType))
      {
        // Can be null if no implementation for a sensor exists
        if( sensorClass != null )
        {
          // Load paired sensors for this sensor
          SensorParam[] sensorParams= SensorConfigService.loadParams(sensorClass);
          
          if( sensorParams != null )
          {
            int icon= getIconForType(sensorType);
            
            for(SensorParam sensorParam : sensorParams)
            {
              SensorAdapter.Entry sensor= new SensorAdapter.Entry();
              sensor.clasz= sensorClass;
              sensor.icon= icon;
              sensor.type= sensorType;
              sensor.param= sensorParam;
              sensorList.add(sensor);
            }
          }
        }
      }
    }
    
    return sensorList;
  }
  
  /**
   * Returns the corresponding icon for a sensor type.
   * 
   * @param sensorType
   *          The sensor type to retrieve an icon for.
   * @return The corresponding icon.
   */
  public static int getIconForType(String sensorType)
  {
    int icon= R.drawable.anroid_white;
    
    try
    {
      icon= (Integer) Sensors.class.getDeclaredField(sensorType + "_icon").get(null);
    }
    catch( IllegalAccessException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch( IllegalArgumentException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch( NoSuchFieldException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return icon;
  }
}
