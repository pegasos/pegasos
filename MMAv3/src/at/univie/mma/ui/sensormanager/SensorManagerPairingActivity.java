package at.univie.mma.ui.sensormanager;

import static android.Manifest.permission.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import at.univie.mma.R;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.sensors.Sensor;
import at.pegasos.client.sensors.SensorCallback;
import at.pegasos.client.sensors.SensorCallbackListener;
import at.pegasos.client.sensors.SensorConfigService;
import at.pegasos.client.sensors.SensorParam;
import at.univie.mma.sensors.SensorTypeNames;
import at.pegasos.client.ui.PegasosClientView;
import at.pegasos.client.ui.ParameterQuestion;
import at.pegasos.client.ui.ParameterQuestion.ParameterQuestionCallback;
import at.univie.mma.ui.controller.SensorSwitchManager.SensorSwitchManagerCallback;
import at.univie.mma.ui.controller.SensorSwitchManagerAndroid;

public class SensorManagerPairingActivity extends PegasosClientView
        implements SensorCallbackListener, ParameterQuestionCallback, SensorSwitchManagerCallback {
  public static final String LOG_TAG= "SensorPairingActivity";
  // need to be processed in onRequestPermissionsResult
  public static final String[] PERMISSIONS= new String[] {
      WRITE_EXTERNAL_STORAGE, BLUETOOTH_ADMIN, ACCESS_COARSE_LOCATION};

  private ListView listView;
  
  private Button btnPairSensors;
  private Button btnCancel;
  private Button btnSelectAll;
  private LinearLayout layoutCancelSelectButtons;
  private SensorAdapter sensorAdapter;
  /**
   * list of already paired sensors
   */
  private ArrayList<SensorAdapter.Entry> pairedSensors;
  /**
   * list of detected sensors not yet paired
   */
  private ArrayList<SensorAdapter.Entry> sensorList;
  /**
   * list of user selected (via checkboxes) sensors
   */
  private ArrayList<SensorAdapter.Entry> selectedSensors;
  private SensorControllerService sensorControllerService;
  /**
   * if inListMode multiple sensors can be selected via checkboxes
   */
  private boolean inListMode;
  
  /**
   * Reference to the sensor which is currently in 'pairing'. This field is only used when a Sensor with questions is being paired.
   */
  private SensorAdapter.Entry pairingSensor;
  
  private SensorSwitchManagerAndroid sensorSwitches;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sensor_manager_pairing);

    pairedSensors= SensorListHelper.getSensorList();

    getSupportActionBar().setTitle(getString(R.string.activity_sensor_manager_pairing));
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    sensorControllerService= SensorControllerService.createService(this, null, true);
    
    sensorSwitches= new SensorSwitchManagerAndroid();
    sensorSwitches.setup(this, this);;

    sensorControllerService.setAntPlusStatus(sensorSwitches.getAntEnabled());
    sensorControllerService.setBLEStatus(sensorSwitches.getBleEnabled());
    SensorCallback.getInstance().addListener(this);

    sensorList= new ArrayList<SensorAdapter.Entry>();
    selectedSensors= new ArrayList<SensorAdapter.Entry>();
    sensorAdapter= new SensorAdapter(this, sensorList, selectedSensors);

    listView= (ListView) findViewById(R.id.sensorListView);
    listView.setAdapter(sensorAdapter);
    // pair sensor or select if inListMode
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id)
      {
        if( inListMode )
        {
          SensorAdapter.Entry selectedSensor= sensorAdapter.getItem(position);
          if( selectedSensors.contains(selectedSensor) )
          {
            selectedSensors.remove(selectedSensor);
          }
          else
          {
            selectedSensors.add(selectedSensor);
          }
          changeSelectAllButtonText();
          sensorAdapter.notifyDataSetChanged();
        }
        else
        {
          selectedSensors.clear();
          selectedSensors.add(sensorAdapter.getItem(position));
          new AlertDialog.Builder(SensorManagerPairingActivity.this).setTitle(R.string.dialog_pair_title)
              .setMessage(R.string.dialog_pair_message).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                  pairSensors();
                }
              }).setNegativeButton(android.R.string.no, null).show();
        }
      }
    });
    listView
            .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
              @Override
              public boolean onItemLongClick(AdapterView<?> parent, View view,
                                             int position, long id)
              {
                changeMode();
                sensorAdapter.selectSensor(sensorAdapter.getItem(position));
                return true;
              }
            });

    // Configure pairing button (list-mode)
    btnPairSensors = (Button) findViewById(R.id.btn_pair_sensors);
    btnPairSensors.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        new AlertDialog.Builder(SensorManagerPairingActivity.this).setTitle(R.string.dialog_pair_title)
            .setMessage(R.string.dialog_pair_message).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
                pairSensors();
              }
            }).setNegativeButton(android.R.string.no, null).show();
      }
    });

    btnCancel = (Button) findViewById(R.id.btn_cancel_pair);
    btnCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        finish();
      }
    });

    btnSelectAll = (Button) findViewById(R.id.btn_selectall_pair);
    btnSelectAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        selectAll();
      }
    });

    layoutCancelSelectButtons = (LinearLayout) findViewById(
            R.id.cancel_select_buttons);

    inListMode= false;
  
    checkAndRequestPermissions();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    getMenuInflater().inflate(R.menu.sensor_manager_pairing, menu);
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    int id= item.getItemId();
    if( id == R.id.action_add_sensor_manually )
    {
      addSensorManually();
      return true;
    }
    
    return super.onOptionsItemSelected(item);
  }
  
  @Override
  protected void onStop()
  {
    super.onStop();

    if( SensorControllerService.getInstance() != null )
    {
      SensorControllerService.getInstance().stopSearchingNewSensors();
      SensorControllerService.getInstance().stopService();
    }
  }

  @Override
  public void onBackPressed()
  {
    if( SensorControllerService.getInstance() != null )
    {
      SensorControllerService.getInstance().stopSearchingNewSensors();
      SensorControllerService.getInstance().stopService();
    }

    super.onBackPressed();
  }

  @Override
  public void onSensorDetected(Sensor sensor, String[] types, String id, final List<ParameterQuestion> questions,
      List<Entry<String, String>> params)
  {
    SensorParam param= new SensorParam();
    param.addSensorId(null, id);
    param.param= new ArrayList<Map.Entry<String, String>>();
    if( params != null )
      param.param.addAll(params);

    // check all sensor types for new sensor
    for(String type : types)
    {
      SensorAdapter.Entry newSensor= new SensorAdapter.Entry();
      newSensor.clasz= sensor.getClass().getCanonicalName();
      newSensor.icon= SensorListHelper.getIconForType(type);
      newSensor.param= param;
      newSensor.type= type;
      newSensor.questions= questions;
      newSensor.instance= sensor;
      
      List<String> pairedIds= new ArrayList<String>();
      for(SensorAdapter.Entry entry : pairedSensors)
        pairedIds.add(entry.param.getSensorId());
  
      // do not display already paired sensors
      if( !sensorList.contains(newSensor)
              && !pairedSensors.contains(newSensor)
              && !pairedIds.contains(newSensor.param.getSensorId()) )
      {
        sensorList.add(newSensor);
      }
    }

    this.runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        sensorAdapter.notifyDataSetChanged();
      }
    });
  }
  
  /**
   * Opens a dialog for adding a sensor manually. Includes a spinner for
   * selecting the sensor type and a text field for entering an ID for the new
   * sensor.
   */
  private void addSensorManually()
  {
    View popup= LayoutInflater.from(this).inflate(R.layout.dialog_add_sensor_manually, null);
    AlertDialog.Builder builder= new AlertDialog.Builder(this);
    builder.setView(popup);
    final AlertDialog dialog= builder.create();
    final Spinner sensorTypeSpinner= (Spinner) popup.findViewById(R.id.sensorType);
    final EditText sensorId= (EditText) popup.findViewById(R.id.sensorId);
    
    final ArrayList<String> sensorTypes= new ArrayList<String>();
    for(String sensorType : SensorTypeNames.ALL)
      sensorTypes.add(sensorType);
    sensorTypeSpinner.setAdapter(new ArrayAdapter<String>(this,
        android.R.layout.simple_spinner_dropdown_item, sensorTypes));
    
    dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.add), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which)
      {
        if( sensorTypeSpinner.getSelectedItem() != null && !sensorId.getText().toString().isEmpty() )
        {
          SensorParam sensorParam= new SensorParam();
          sensorParam.addSensorId(null, sensorId.getText().toString());
          sensorParam.param= new ArrayList<Map.Entry<String, String>>();
          
          for(String sensorClass : SensorControllerService.getSensorClassNamesForType(sensorTypeSpinner.getSelectedItem().toString()))
            if( sensorClass != null )
              SensorConfigService.addSensorInstance(sensorClass, sensorParam);
          
          sensorControllerService.stopSearchingNewSensors();
          finish();
        }
        else
          showToast(R.string.dialog_missing_id);
      }
    });
    
    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which)
      {
        dialog.dismiss();
      }
    });
    
    dialog.setCanceledOnTouchOutside(false);
    dialog.show();
  }
  
  /**
   * Clear list view and restart sensorControllerService.
   */
  private void refresh()
  {
    sensorControllerService.stopSearchingNewSensors();

    sensorList.clear();
    selectedSensors.clear();
    sensorAdapter.notifyDataSetChanged();

    if( capabilities.isCapable(WRITE_EXTERNAL_STORAGE) )
      sensorControllerService.startSearchingSensors();
  }
  
  /**
   * Changes activity into selection mode and back, allowing
   * the user to select and add multiple sensors at once.
   */
  private void changeMode()
  {
    selectedSensors.clear();

    if( inListMode )
    {
      inListMode= false;
      sensorAdapter.setEditMode(inListMode);
      btnPairSensors.setVisibility(View.GONE);
      layoutCancelSelectButtons.setVisibility(View.GONE);
    }
    else
    {
      inListMode= true;
      sensorAdapter.setEditMode(inListMode);
      changeSelectAllButtonText();
      btnPairSensors.setVisibility(View.VISIBLE);
      layoutCancelSelectButtons.setVisibility(View.VISIBLE);
    }

    sensorAdapter.notifyDataSetChanged();
  }
  
  /**
   * Selects all sensors or if already selected, deselects all sensors.
   */
  private void selectAll()
  {
    if( selectedSensors.size() == sensorList.size() )
    {
      selectedSensors.clear();
    }
    else
    {
      selectedSensors.clear();
      selectedSensors.addAll(sensorList);
    }
    changeSelectAllButtonText();
    sensorAdapter.notifyDataSetChanged();
  }
  
  /**
   * Changes the select all button text according to the number of selected sensors.
   */
  private void changeSelectAllButtonText()
  {
    if( selectedSensors.size() == sensorList.size() )
    {
      btnSelectAll.setText(R.string.btn_deselect_all);
    }
    else
    {
      btnSelectAll.setText(R.string.btn_select_all);
    }
  }
  
  @Override
  protected String[] getRequiredPermissions()
  {
    return PERMISSIONS;
  }
  
  @Override
  protected void onCapabilitiesChanged()
  {
    super.onCapabilitiesChanged();
    
    if( this.capabilities.getCapability(WRITE_EXTERNAL_STORAGE) == CapabilityState.Can )
    {
      sensorControllerService.startSearchingSensors();
    }
    
    sensorSwitches.update(this, capabilities.isCapable(BLUETOOTH_ADMIN), true);
  }
  
  /**
   * The user has requested to pair the selected sensor(s).
   */
  private void pairSensors()
  {
    boolean can_finish= false;
    boolean next_pair= false;
    
    sensorControllerService.stopSearchingNewSensors();
    
    if( selectedSensors.size() > 0 )
    {
      SensorAdapter.Entry sensor= selectedSensors.remove(0);
      if( sensor.questions == null || sensor.questions.size() == 0 )
      {
        SensorConfigService.addSensorInstance(sensor.clasz, sensor.param);
        
        if( selectedSensors.size() == 0 )
          can_finish= true;
        else
        {
          sensorAdapter.removeItem(sensor);
          sensorAdapter.notifyDataSetChanged();
          
          next_pair= true;
        }
      }
      else
      {
        pairingSensor= sensor;
        
        ParameterQuestion q= sensor.questions.get(0);
        sensor.questions.remove(0);
        q.startAsking(SensorManagerPairingActivity.this, SensorManagerPairingActivity.this, sensor.param, sensor.questions);
      }
    }
    else
    {
      can_finish= true;
    }
    
    // return to SensorManagerActivity once done pairing
    if( can_finish )
      finish();
    
    // there is more to pair...
    if( next_pair )
      pairSensors();
  }

  @Override
  public void onSensorPairedQuestionsAnswered(SensorParam param, ArrayList<Entry<String, String>> additional_params)
  {
    if( param != null )
      param.addParams(additional_params);
    
    SensorConfigService.addSensorInstance(pairingSensor.instance.getClass().getCanonicalName(), param);
    
    sensorAdapter.removeItem(pairingSensor);
    sensorAdapter.notifyDataSetChanged();
    
    pairingSensor= null;
    
    // Done with the current sensor. Go to the next
    pairSensors();
  }
  
  @Override
  public void onSensoirPairedFailed()
  {
    pairingSensor.instance.stopSensor();
    pairingSensor.instance.initSensor();
    
    // Add sensor back to list
    selectedSensors.add(pairingSensor);
    
    pairingSensor= null;
    
    // Done with the current sensor. Do not go to the next.
  }

  @Override
  public void setBleEnabled(boolean status)
  {
    sensorControllerService.setBLEStatus(status);
    refresh();
  }

  @Override
  public void setAntEnabled(boolean status)
  {
    sensorControllerService.setAntPlusStatus(status);
    refresh();
  }
}
