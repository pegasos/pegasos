package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import at.univie.mma.R;
import at.univie.mma.ui.MeasurementFragment;
import at.pegasos.client.values.ValueStore;

public class HeartrateFragment extends MeasurementFragment implements ScalableMeasurementFragment {
  ValueStore vals;

  TextView txtBmp;
  ImageView hrLogo;
  ImageView hrLogoSmall;

  private int requiredWidth= 0;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_heartrate_current, container, false);

    txtBmp= view.findViewById(R.id.hr);
    hrLogo= view.findViewById(R.id.hrlogo);
    hrLogoSmall= view.findViewById(R.id.hrlogosmall);

    vals= ValueStore.getInstance();

    initialise(true);

    return view;
  }

  public void updateUI()
  {
    if( !init )
      return;

    int value= vals.getHeartRateBPM();
    if( value == -1 )
      txtBmp.setText("--");
    else
      txtBmp.setText(String.valueOf(value));
  }

  @Override
  public int getRequiredWidth()
  {
    if( isAdded() )
    {
      requiredWidth= ScalableMeasurementFragmentHelper.requiredWidthLarge("000");
      return requiredWidth;
    }
    else
      return 0;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    txtBmp.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    // txtBmp.setScaleX(scale);
    // txtBmp.setScaleY(scale);
    // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
    // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    txtBmp.requestLayout();

    if( requiredWidth + 1.75 * sizeLarge > availableWidth )
    {
      hrLogo.setVisibility(View.GONE);
      hrLogoSmall.setVisibility(View.VISIBLE);

      hrLogoSmall.getLayoutParams().height= (int) (sizeSmall * 1.2);
      hrLogoSmall.getLayoutParams().width= (int) (sizeSmall * 1.2);
    }
    else
    {
      hrLogo.setVisibility(View.VISIBLE);
      hrLogoSmall.setVisibility(View.GONE);

      hrLogo.getLayoutParams().height= (int) (sizeLarge * 0.8);
      hrLogo.getLayoutParams().width= (int) (sizeLarge * 0.8);
      // hrLogo.setScaleX(scale);
      // hrLogo.setScaleY(scale);
      // hrLogo.setMaxHeight((int) (sizeLarge * 0.9) );
      // hrLogo.setMaxHeight((int) sizeLarge);
      // ((RelativeLayout.LayoutParams) hrLogo.getLayoutParams()).addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
      // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM, txtBmp.getId());
      hrLogo.requestLayout();
    }
  }
}

