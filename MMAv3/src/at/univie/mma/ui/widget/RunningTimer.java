package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import at.univie.mma.R;
import at.pegasos.client.values.ValueStore;

public class RunningTimer extends TimerFragment
{
  private static final String ARGUMENT_NAME= "ARG_N";

  private static HashMap<String, RunningTimer> instances;
  static
  {
    instances= new HashMap<String, RunningTimer>();
  }

  /**
   * Name of the timer
   */
  private String name;

  private boolean running;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_elapsed_time, container, false);

    initialise(view);

    initialise(true);

    return view;
  }

  protected void initialise(boolean set_init)
  {
    Bundle args= getArguments();

    name= args.getString(ARGUMENT_NAME);

    super.initialise(true);
  }

  @Override
  public void onDestroy()
  {
    chrono.stop();
    super.onDestroy();
  }

  protected void start(long starttime)
  {
    if( name == null )
      initialise(false);

    running= true;
    // chrono can be null if we have not been on screen before
    if( chrono != null )
    {
      chrono.setBase(starttime);
      chrono.start();
    }
    ValueStore.getInstance().getValue(name).setDoubleValue((double) starttime);
  }

  protected void stop()
  {
    if( name == null )
      throw new IllegalStateException("Stopping a timer for which it seems that it hasn't been started");

    running= false;
    // It can happen that we never have been on screen. Yet we are being stopped
    if( chrono != null )
      chrono.stop();
    ValueStore.getInstance().getValue(name).setDoubleValue(0d);
  }

  @Override
  public void updateUI()
  {
    // Is this timer initialised?
    if( name == null )
      return;

    // Did someone start me but i am not running?
    if( running && !chrono.isStarted() )
    {
      chrono.setBase(ValueStore.getInstance().getValue(name).getDoubleValue().longValue());
      chrono.start();
    }
    // Did someone stop me but i am running?
    else if( !running && chrono.isStarted() )
    {
      chrono.stop();
    }
  }

  /**
   * Create an instance of a running timer (elapsed time). The timer will be
   * identified by the name
   * 
   * @param name
   *          name of the timer
   * @return instance of a timer
   */
  public static RunningTimer createTimer(String name)
  {
    if( instances.get(name) != null )
      return instances.get(name);

    RunningTimer t= new RunningTimer();

    Bundle args= new Bundle();
    args.putString(ARGUMENT_NAME, name);

    ValueStore.getInstance().getCreateNumberValue(name, 0d);

    t.setArguments(args);

    instances.put(name, t);

    return t;
  }

  /**
   * Start a timer. It is important that the timer has been created before
   * (otherwise an IllegalArgumentException will be thrown)
   *
   * @param name
   *          name of the timer to be started
   * @param starttime
   *          0 for the timer (i.e. at which point in time was t0 for this
   *          timer)
   */
  public static void Start(String name, long starttime)
  {
    RunningTimer t= instances.get(name);
    if( t == null )
      throw new IllegalArgumentException("Tried to start non existing timer '" + name + "'");

    t.start(starttime);
  }

  /**
   * Start a timer. It is important that the timer has been created before
   * (otherwise an IllegalArgumentException will be thrown)
   *
   * @param name
   *          name of the timer to be started
   */
  public static void Start(String name)
  {
    long starttime= System.currentTimeMillis();

    RunningTimer t= instances.get(name);
    if( t == null )
      throw new IllegalArgumentException("Tried to start non existing timer '" + name + "'");

    t.start(starttime);
  }

  /**
   * Stop a timer. This timer will no longer increment (not reset to 0)
   *
   * @param name
   *          name of the timer to be stopped.
   */
  public static void Stop(String name)
  {
    RunningTimer t= instances.get(name);
    if( t == null )
      throw new IllegalArgumentException("Tried to stop non existing timer '" + name + "'");

    t.stop();
  }
}
