package at.univie.mma.ui.widget;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.pegasos.client.ui.TrainingActivity;
import at.univie.mma.ui.controller.TrainingUIController;

public class StopButton extends MeasurementFragment {
  
  @Override
  public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    
    RelativeLayout layout = new RelativeLayout(container.getContext());
    
    Button b = new Button(container.getContext());
    b.setText(R.string.StopTraining);
    b.setOnLongClickListener(new OnLongClickListener() {
      
      @Override
      public boolean onLongClick(View v)
      {
        Activity a = getActivity();
        if( a instanceof TrainingActivity )
        {
          ((TrainingActivity) a).getVibrator().signal_confirmed();
        }
        else
          throw new IllegalStateException("Tried to stop training from activity != TrainingActivity");

        TrainingUIController.getInstance().stopTraining();
        return true;
      }
    });
    
    layout.addView(b, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
        RelativeLayout.LayoutParams.MATCH_PARENT));
    
    initialise(true);
        
    return layout;
  }
  
  public void updateUI()
  {
    
  }
}
