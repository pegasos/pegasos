package at.univie.mma.ui.widget;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import at.univie.mma.R;
import at.pegasos.client.ui.PegasosClientView;

public class ScalableMeasurementFragmentHelper {
  private static Paint paintLarge;
  private static Paint paintSmall;

  public static void setup(PegasosClientView activity)
  {
    // Construct a static member for repeated computations
    paintLarge= new Paint();

    // Load a font family from an asset, and then pick a specific style:
    //Typeface plain = Typeface.createFromAsset(assetManager, pathToFont);
    //Typeface bold = Typeface.create(plain, Typeface.DEFAULT_BOLD);
    // Or just reference a system font:
    paintLarge.setTypeface(Typeface.create("sans-serif",Typeface.BOLD));

    // Don't forget to specify your target font size. You can load from a resource:
    final float scaledSizeInPixels= activity.getResources().getDimension(R.dimen.textsize_training_fragment);
    paintLarge.setTextSize(scaledSizeInPixels);

    paintSmall = new Paint();
    paintSmall.setTypeface(Typeface.create("sans-serif",Typeface.BOLD));
    final float sscaledSizeInPixels= activity.getResources().getDimension(R.dimen.textsize_training_fragment_small);
    paintSmall.setTextSize(sscaledSizeInPixels);
  }

  public static Paint getPaintLarge()
  {
    return paintLarge;
  }

  public static Paint getPaintSmall()
  {
    return paintSmall;
  }

  public static int requiredWidthLarge(String string)
  {
    final Rect bounds = new Rect();
    ScalableMeasurementFragmentHelper.getPaintLarge().getTextBounds(string, 0, string.length(), bounds);

    return bounds.width();
  }

  public static int requiredWidth(String string, float textSize)
  {
    final Rect bounds = new Rect();
    Paint p = new Paint();
    p.setTypeface(Typeface.create("sans-serif",Typeface.BOLD));
    p.setTextSize(textSize);
    p.getTextBounds(string, 0, string.length(), bounds);

    return bounds.width();
  }

  public static int requiredWidthSmall(String string)
  {
    final Rect bounds = new Rect();
    ScalableMeasurementFragmentHelper.getPaintSmall().getTextBounds(string, 0, string.length(), bounds);

    return bounds.width();
  }
}
