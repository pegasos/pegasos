package at.univie.mma.ui.widget;

import android.annotation.*;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import at.univie.mma.Config;
import at.univie.mma.R;

public class DoubleValueFragment extends ValueFragment {
  TextView txtPre;
  TextView txtPost;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    view= inflater.inflate(R.layout.fragment_double_value, container, false);

    txtPre= view.findViewById(R.id.pre);
    txtPost= view.findViewById(R.id.post);

    requiredWidthString= "00.00";

    initialise(true);

    return view;
  }

  @SuppressLint("DefaultLocale") @Override
  public void updateUI()
  {
    if( !init )
      return;

    int s1, s2;

    if( Config.LOGGING) Log.d("DoubleValueFragment", name + " " + value);
    Double display = null;
    if( value != null )
      display= value.getDoubleValue();

    if( display == null )
    {
      txtPre.setText("--");
      txtPost.setText("--");
    }
    else
    {
      s1= display.intValue();
      s2= ((int) (display * 100)) % 100;
      txtPre.setText(String.format("%d", s1));
      if( s2 < 10 )
        txtPost.setText(String.format("%02d", s2));
      else
        txtPost.setText(String.format("%d", s2));
    }
  }

  public static DoubleValueFragment create(String value_name)
  {
    DoubleValueFragment ret= new DoubleValueFragment();
    ret.setArguments(createArguments(value_name));
    return ret;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    super.setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);

    txtPre.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    txtPost.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
  }
}

