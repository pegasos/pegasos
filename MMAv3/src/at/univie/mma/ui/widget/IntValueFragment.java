package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.util.*;
import android.view.*;
import android.widget.TextView;
import at.univie.mma.Config;
import at.univie.mma.R;

public class IntValueFragment extends ValueFragment {
  TextView txt;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    view= inflater.inflate(R.layout.fragment_integer_value, container, false);

    txt= view.findViewById(R.id.txt);

    requiredWidthString= "000";

    initialise(true);

    return view;
  }

  @Override
  public void updateUI()
  {
    if( !init )
      return;

    if( Config.LOGGING ) Log.d("IntValueFragment", name + " " + value);
    Integer disp = null;
    if( value != null )
      disp = value.getIntValue();

    if( disp == null )
    {
      txt.setText("--");
    }
    else
    {
      txt.setText(String.valueOf(disp));
    }
  }

  public static IntValueFragment create(String value_name)
  {
    IntValueFragment ret= new IntValueFragment();
    ret.setArguments(createArguments(value_name));
    return ret;
  }

  @Override
  public int getRequiredWidth()
  {
    if( !isAdded() )
      return 0;

    /*final Rect bounds = new Rect();
    txt.getPaint().getTextBounds(requiredWidthString, 0, requiredWidthString.length(), bounds);
    int x;
    if( sizeLarge != null )
      x = ScalableMeasurementFragmentHelper.requiredWidth(requiredWidthString, sizeLarge);
    else
      x = (int) (ScalableMeasurementFragmentHelper.requiredWidthLarge(requiredWidthString) * 1.5);
    Log.d("IntValueFragment", "requiredWidth: " + bounds.width() + "/" + x + " super: " + super.getRequiredWidth() + " sizeLarge: " + (sizeLarge != null ? sizeLarge : "null"));

    return x;*/
    return (int) (super.getRequiredWidth() * 1.75);
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    Log.d("IntValueFragment", "setScaleWidth: tSL" + this.sizeLarge + " " + sizeLarge);
    super.setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    Log.d("IntValueFragment", "setScaleWidth: tSL" + this.sizeLarge + " " + sizeLarge);

    txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
  }
}

