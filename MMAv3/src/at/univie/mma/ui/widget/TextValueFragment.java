package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import at.univie.mma.R;

/**
 * Value fragment for displaying a TextValue on screen.
 * Currently, this fragment does not praticipate in autoscaling
 */
public class TextValueFragment extends ValueFragment {
  TextView txt;
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    view= inflater.inflate(R.layout.fragment_text, container, false);
    
    txt= (TextView) view.findViewById(R.id.txt);
    txt.setMaxLines(10);

    // In the current configuration we do not participate in scaling
    requiredWidthString= "";

    initialise(true);

    return view;
  }
  
  @Override
  public void updateUI()
  {
    if( !init )
      return;
    
    txt.setText(value.getStringValue());
    /*if( Build.VERSION.SDK_INT >= 26 )
      txt.setAutoSizeTextTypeWithDefaults(android.widget.TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
    else
    {
      TextViewCompat.setAutoSizeTextTypeWithDefaults(txt, 1);
    }*/
  }
  
  public static TextValueFragment create(String value_name)
  {
    TextValueFragment ret= new TextValueFragment();
    ret.setArguments(createArguments(value_name));
    return ret;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {

  }
}

