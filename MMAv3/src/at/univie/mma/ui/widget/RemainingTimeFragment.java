package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import at.univie.mma.R;
import at.pegasos.client.values.ValueStore;

public class RemainingTimeFragment extends TimerFragment
{
  private static final String ARGUMENT_NAME= "ARG_N";
  // TODO: make this class thread safe
  /*
   * potential issues are mBase & duration which are used to determine whether the timer has been started
   * stop sets them to 0
   */
  
  private static HashMap<String, RemainingTimeFragment> instances;
  static
  {
    instances= new HashMap<String, RemainingTimeFragment>();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_remaining_time, container, false);

    initialise(view);
    if( mBase != 0 && duration != 0 )
    {
      chrono.start(mBase, duration);
    }

    initialise(true);

    return view;
  }

  @Override
  public void onDestroy()
  {
    chrono.stop();
    super.onDestroy();
  }
  
  @Override
  public void updateUI()
  {
    
  }
  
  private long mBase= 0;
  private long duration= 0;
  
  protected void startCountdown(long miliseconds)
  {
    if( chrono != null )
    {
      mBase= System.currentTimeMillis();
      duration= miliseconds;
      chrono.start(mBase, miliseconds);
    }
    else
    {
      // Chrono has not yet been put up to screen
      mBase= System.currentTimeMillis();
      duration= miliseconds;
    }
  }
  
  protected void stop()
  {
    chrono.stop();
    mBase= 0;
    duration= 0;
  }
  
  public static RemainingTimeFragment createTimer(String name)
  {
    RemainingTimeFragment old= instances.get(name);
    
    RemainingTimeFragment t= new RemainingTimeFragment();
    
    Bundle args= new Bundle();
    args.putString(ARGUMENT_NAME, name);
    
    ValueStore.getInstance().getCreateValueDouble(name, 0d);
    
    t.setArguments(args);
    
    if( old != null && old.mBase != 0 && old.duration != 0 )
    {
      t.mBase= old.mBase;
      t.duration= old.duration;
    }
    
    instances.put(name, t);
    
    return t;
  }
  
  public static void StartCountdown(String name, long duration_ms)
  {
    RemainingTimeFragment t= instances.get(name);
    if( t == null )
      throw new IllegalArgumentException("Tried to start non existing timer '" + name + "'");
    
    t.startCountdown(duration_ms);
  }
  
  public static void Stop(String name)
  {
    RemainingTimeFragment t= instances.get(name);
    if( t == null )
      throw new IllegalArgumentException("Tried to stop non existing timer '" + name + "'");
    
    t.stop();
  }
}

