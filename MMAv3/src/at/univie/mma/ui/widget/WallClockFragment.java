package at.univie.mma.ui.widget;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DigitalClock;

import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.ui.MeasurementFragment;

public class WallClockFragment extends MeasurementFragment implements ScalableMeasurementFragment {
  private boolean analog;

  private DigitalClock clock;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    if( Config.LOGGING )
      Log.d("WallClockFragment", "Inflate");

    final Activity a= this.getActivity();
    SharedPreferences sharedPref= a.getPreferences(android.app.Activity.MODE_PRIVATE);
    analog= sharedPref.getBoolean("use_analog_clock", false);

    initialise(true);

    if( analog )
      return inflater.inflate(R.layout.clock_analog, container, false);
    else
    {
      View view= inflater.inflate(R.layout.clock_digital, container, false);

      clock= view.findViewById(R.id.clock);

      return view;
    }
  }

  @Override
  public void initialise(boolean set_init)
  {
    Bundle args= getArguments();
    if( args != null && args.getBoolean("analog") )
      analog= true;

    super.initialise(set_init);
  }

  @Override
  public void updateUI()
  {

  }

  @Override
  public int getRequiredWidth()
  {
    // If we are not added or analog we do not participate
    if( !isAdded() || analog )
      return 0;

    return ScalableMeasurementFragmentHelper.requiredWidthLarge("00:00");
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    if( !analog )
      clock.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
  }
}

