package at.univie.mma.ui.widget.target;

import android.os.Bundle;
import android.view.View;
import at.univie.mma.utils.colormaps.MMAColorMap;

public abstract class TargetColoredDeviationFragment extends TargetFragment
{
  /**
   * Which MMAColorMap to use. Available ColorMap names are available in MMAColorMap.colorMaps. Default: "RYGYR"
   */
  public static final String ARGUMENT_COLOR_MAP= "ARG_CM";
  
  /**
   * Whether the deviation should be measured in absolute or relative units. (Default: false=relative)
   */
  public static final String ARGUMENT_DEVIATION_ABSOLUTE= "ARG_DEV_ABS";
  
  /**
   * Maximum deviation in absolute or relative units. (Default: 50%)
   */
  public static final String ARGUMENT_DEVIATION_MAXIMUM= "ARG_DEV_MAX";
  
  /**
   * Allowed/optimal deviation in absolute or relative units. If not specified
   * no hint will be given (Default: not set / 0)
   */
  public static final String ARGUMENT_DEVIATION_OPTIMAL= "ARG_DEV_OPT";
  
  /**
   * Whether the view for the current value is invisible (Default: false)
   */
  public static final String ARGUMENT_HIDE_VALUE= "ARG_HV";
  
  /**
   * Whether the view for the deviation value is invisible (Default: false)
   */
  public static final String ARGUMENT_HIDE_DEVIATION= "ARG_HD";
  
  /**
   * Whether the view for the target value is invisible (Default: false)
   */
  public static final String ARGUMENT_HIDE_TARGET= "ARG_HT";
  
  /**
   * Whether deviation should be measured absolute or relative
   */
  protected boolean deviation_absolute;
  
  /**
   * Maximum absolute deviation
   */
  protected double deviation_max;
  
  /**
   * Allowed/Optimal deviation
   */
  protected double deviation_optimal;
  
  /**
   * Whether the view for the deviation value is invisible
   */
  protected boolean deviation_hidden= false;
  
  /**
   * Whether the view for the current value is invisible
   */
  protected boolean current_hidden= false;
  
  /**
   * Whether the view for the target value is invisible
   */
  protected boolean target_hidden= false;
  
  /**
   * View showing the deviation from target
   */
  View deviationvalue_view= null;
  
  /**
   * View showing the current value
   */
  View currentvalue_view= null;
  
  protected MMAColorMap colorMap= null;
  
  @Override
  protected void initialise(boolean set_init)
  {
    super.initialise(false);
    
    Bundle args= getArguments();
    
    deviation_absolute= args.getBoolean(ARGUMENT_DEVIATION_ABSOLUTE, false);
    if( deviation_absolute )
      deviation_max= args.getDouble(ARGUMENT_DEVIATION_MAXIMUM, 50);
    else
      deviation_max= args.getDouble(ARGUMENT_DEVIATION_MAXIMUM, 0.5);
    deviation_hidden= args.getBoolean(ARGUMENT_HIDE_TARGET, false);
    current_hidden= args.getBoolean(ARGUMENT_HIDE_VALUE, false);
    target_hidden= args.getBoolean(ARGUMENT_HIDE_TARGET, false);
    deviation_optimal= args.getDouble(ARGUMENT_DEVIATION_OPTIMAL, 0d);
    
    String mapName= args.getString(ARGUMENT_COLOR_MAP, "RYGYR");
    colorMap= MMAColorMap.fromString(mapName);
    
    if( set_init )
      init= true;
  }
  
  public static Bundle createArguments(String target, String value, boolean absolute)
  {
    Bundle args= createArguments(target, value);
    
    args.putBoolean(TargetBarFragment.ARGUMENT_DEVIATION_ABSOLUTE, absolute);
    
    return args;
  }
  
  public static Bundle createArguments(String target, String value, boolean absolute, double max_deviation)
  {
    Bundle args= createArguments(target, value, absolute);
    
    args.putDouble(ARGUMENT_DEVIATION_MAXIMUM, max_deviation);
    
    return args;
  }
  
  public static Bundle createArguments(String target, String value, boolean absolute, double max_deviation, int decimals)
  {
    Bundle args= createArguments(target, value, absolute, max_deviation);
    
    args.putInt(ARGUMENT_VALUE_PRECISION, decimals);
    
    return args;
  }
  
  public static Bundle createArguments(String target, String value, boolean absolute, String color_map)
  {
    Bundle args= createArguments(target, value, absolute);
    
    args.putString(ARGUMENT_COLOR_MAP, color_map);
    
    return args;
  }
  
  public static Bundle createArguments(String target, String value, boolean absolute, double max_deviation, String color_map)
  {
    Bundle args= createArguments(target, value, absolute, max_deviation);
    
    args.putString(ARGUMENT_COLOR_MAP, color_map);
    
    return args;
  }
  
  public void setDeviation(boolean absolute, double max_deviation)
  {
    Bundle args= getArguments();
    
    args.putBoolean(ARGUMENT_DEVIATION_ABSOLUTE, absolute);
    args.putDouble(ARGUMENT_DEVIATION_MAXIMUM, max_deviation);
    
    if( !init )
      setArguments(args);
    else
      getArguments().putAll(args);
    
    initialise(false);
  }
  
  public void setDeviation(boolean absolute, double max_deviation, double optimal)
  {
    Bundle args= getArguments();
    
    args.putBoolean(ARGUMENT_DEVIATION_ABSOLUTE, absolute);
    args.putDouble(ARGUMENT_DEVIATION_MAXIMUM, max_deviation);
    args.putDouble(ARGUMENT_DEVIATION_OPTIMAL, optimal);
    
    if( !init )
      setArguments(args);
    else
      getArguments().putAll(args);
    
    initialise(false);
  }
  
  public void setOptimalDeviation(double optimal)
  {
    Bundle args= getArguments();
    
    args.putDouble(ARGUMENT_DEVIATION_OPTIMAL, optimal);
    deviation_optimal= optimal;
    
    if( !init )
      setArguments(args);
    else
      getArguments().putAll(args);
    
    initialise(false);
  }
}
