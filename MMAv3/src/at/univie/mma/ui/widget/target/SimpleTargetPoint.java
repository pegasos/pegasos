package at.univie.mma.ui.widget.target;

import java.util.*;

public class SimpleTargetPoint extends SimpleTargetFragment {
  public void updateUI()
  {
    super.update();

    if( current_value == null )
      txtCurrent.setText("--");
    else
      txtCurrent.setText(String.format(Locale.ROOT, "%.2f", current_value));
    if( target_value == null )
      txtTarget.setText("--");
    else
      txtTarget.setText(String.format(Locale.ROOT, "%.2f", target_value));

    super.post();
  }
}