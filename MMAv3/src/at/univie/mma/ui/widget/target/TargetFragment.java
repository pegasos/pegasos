package at.univie.mma.ui.widget.target;

import android.os.*;
import android.view.*;

import at.pegasos.client.values.*;
import at.univie.mma.ui.*;

import java.util.*;

public abstract class TargetFragment extends MeasurementFragment {
  /**
   * Name of value which should be displayed
   */
  public static final String ARGUMENT_VALUE_NAME = "ARG_V";

  /**
   * Name of the value which reflects the target value
   */
  public static final String ARGUMENT_TARGET_NAME = "ARG_T";

  /**
   * Target value
   */
  public static final String ARGUMENT_TARGET_VALUE = "ARG_TV";

  /**
   * Number of decimals used for displaying values (Default: 0)
   */
  public static final String ARGUMENT_VALUE_PRECISION = "ARG_TP";
  /**
   * The target value (as target, set in init if constant or updated)
   */
  protected Double target_value;
  /**
   * The (actual) current value
   */
  protected Double current_value;
  /**
   * Whether or some value changed. ie an update of the UI is required
   */
  protected boolean changed;
  /**
   * Number of decimals in text
   */
  protected int decimals = 0;
  /**
   * View showing the target
   */
  protected View targetvalue_view = null;
  protected boolean init = false;
  protected String LOG_TAG;
  protected Double target_last;
  protected Double value_last;
  /**
   * The changing value
   */
  private Value current_value_value;
  /**
   * The target value (if present)
   */
  private Value target_value_value;
  /**
   * Whether or not the target is a constant value
   */
  private boolean constant;

  /**
   * Create a TargetFragment with target/value coming from the ValueStore
   *
   * @return Arguments for the fragment
   */
  protected static Bundle createArguments(String target, String value)
  {
    Bundle args = new Bundle();

    args.putString(TargetFragment.ARGUMENT_VALUE_NAME, value);
    args.putString(TargetFragment.ARGUMENT_TARGET_NAME, target);

    return args;
  }

  /**
   * Create a TargetFragment with target/value coming from the ValueStore
   *
   * @return Arguments for the fragment
   */
  protected static Bundle createArguments(String target, String value, int decimals)
  {
    Bundle args = createArguments(target, value);

    args.putInt(ARGUMENT_VALUE_PRECISION, decimals);

    return args;
  }

  /**
   * Create a TargetFragment with a constant target
   *
   * @return Arguments for the fragment
   */
  protected static Bundle createArguments(double target, String value)
  {
    Bundle args = new Bundle();

    args.putString(TargetFragment.ARGUMENT_VALUE_NAME, value);
    args.putDouble(TargetFragment.ARGUMENT_TARGET_VALUE, target);

    return args;
  }

  public static String format(Double value, int precision)
  {
    String form = "%." + precision + "f";
    return String.format(Locale.ROOT, form, value);
  }

  protected void initialise(boolean set_init)
  {
    LOG_TAG = this.getClass().getSimpleName();

    Bundle args = getArguments();

    if( args == null )
      throw new IllegalArgumentException("Need to set arguments for a targetfragment");

    String name = args.getString(ARGUMENT_VALUE_NAME);
    current_value_value = ValueStore.getInstance().getValue(name);

    name = args.getString(ARGUMENT_TARGET_NAME);
    if( name == null || name.equals("") )
    {
      constant = true;
      target_value = args.getDouble(ARGUMENT_TARGET_VALUE);
    }
    else
    {
      constant = false;
      name = args.getString(ARGUMENT_TARGET_NAME);
      target_value_value = ValueStore.getInstance().getValue(name);
    }
    decimals = args.getInt(ARGUMENT_VALUE_PRECISION);

    if( set_init )
      init = true;
  }

  protected void update()
  {
    changed = false;
    if( !constant )
    {
      target_value = target_value_value.getDoubleValue();
      if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
      {
        if( !Objects.equals(target_last, target_value) )
          changed = true;
      }
      else
      {
        if( target_last == null && target_value != null || target_last != null && !target_last.equals(target_value) )
          changed = true;
      }
    }

    current_value = current_value_value.getDoubleValue();
    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
    {
      if( !Objects.equals(current_value, value_last) )
        changed = true;
    }
    else
    {
      if( current_value == null && value_last != null || current_value != null && !current_value.equals(value_last) )
        changed = true;
    }
  }

  protected void post()
  {
    if( !constant )
      target_last = target_value;
    value_last = current_value;
  }
}
