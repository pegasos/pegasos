package at.univie.mma.ui.widget.target;

import android.os.*;
import android.view.*;
import android.widget.*;
import at.univie.mma.R;

public abstract class SimpleTargetFragment extends TargetFragment {
  public static final String ARGUMENT_IMG_L = "ARG_IMGL";
  public static final String ARGUMENT_IMG_R = "ARG_IMGR";

  TextView txtCurrent;
  TextView txtTarget;

  public static Bundle createArguments(String target, String value, int img_left)
  {
    Bundle args = createArguments(target, value);
    args.putInt(ARGUMENT_IMG_L, img_left);
    return args;
  }

  public static Bundle createArguments(String target, String value, int img_left, int img_right)
  {
    Bundle args = createArguments(target, value);
    args.putInt(ARGUMENT_IMG_L, img_left);
    args.putInt(ARGUMENT_IMG_R, img_right);
    return args;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.fragment_compare_target, container, false);

    txtCurrent = (TextView) view.findViewById(R.id.current);
    txtTarget = (TextView) view.findViewById(R.id.target);

    init(view);

    return view;
  }

  protected void init(View view)
  {
    super.initialise(false);

    Bundle args = getArguments();
    int imgLh = args.getInt(ARGUMENT_IMG_L, 0);
    int imgRh = args.getInt(ARGUMENT_IMG_R, 0);

    if( imgLh == 0 )
    {
      view.findViewById(R.id.img_left).setVisibility(android.view.View.INVISIBLE);
    }
    else
    {
      view.findViewById(R.id.img_left).setVisibility(android.view.View.VISIBLE);
      ((ImageView) view.findViewById(R.id.img_left)).setImageResource(imgLh);
    }

    if( imgRh == 0 )
    {
      view.findViewById(R.id.img_right).setVisibility(android.view.View.INVISIBLE);
    }
    else
    {
      view.findViewById(R.id.img_right).setVisibility(android.view.View.VISIBLE);
      ((ImageView) view.findViewById(R.id.img_left)).setImageResource(imgRh);
    }

    init = true;
  }
}
