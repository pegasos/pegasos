package at.univie.mma.ui.widget.target;

import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import at.univie.mma.R;
import at.univie.mma.ui.views.*;

public class TargetMeterFragment extends TargetColoredDeviationFragment {
  protected RelativeLayout.LayoutParams deviationvalue_rlayout = null;
  protected RelativeLayout.LayoutParams currentvalue_rlayout = null;
  protected RelativeLayout.LayoutParams targetvalue_rlayout = null;
  private ValueMeter meter = null;
  private int lastPos = -1;

  public static Bundle createArguments(String target, String value)
  {
    return TargetFragment.createArguments(target, value);
  }

  public static Bundle createArguments(String target, String value, double maxDiff)
  {
    Bundle args = createArguments(target, value);
    args.putDouble(ARGUMENT_DEVIATION_MAXIMUM, maxDiff);
    return args;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    RelativeLayout layout = new RelativeLayout(container.getContext());
    meter = new ValueMeter(container.getContext());
    layout.addView(meter);
    // move this to base classes
    deviationvalue_view = new TextView(container.getContext());
    deviationvalue_rlayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    deviationvalue_rlayout.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    deviationvalue_view.setLayoutParams(deviationvalue_rlayout);
    ((TextView) deviationvalue_view).setTextColor(Color.WHITE);
    ((TextView) deviationvalue_view).setTextSize(
        container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    ((TextView) deviationvalue_view).setText("--");
    if( deviation_hidden )
      deviationvalue_view.setVisibility(View.INVISIBLE);
    layout.addView(deviationvalue_view);

    currentvalue_view = new TextView(container.getContext());
    currentvalue_rlayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    currentvalue_rlayout.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
    currentvalue_view.setLayoutParams(currentvalue_rlayout);
    ((TextView) currentvalue_view).setTextColor(Color.WHITE);
    ((TextView) currentvalue_view).setTextSize(
        container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    ((TextView) currentvalue_view).setText("--");
    if( current_hidden )
      currentvalue_view.setVisibility(View.INVISIBLE);
    layout.addView(currentvalue_view);

    targetvalue_view = new TextView(container.getContext());
    targetvalue_rlayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    targetvalue_rlayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
    targetvalue_view.setLayoutParams(targetvalue_rlayout);
    ((TextView) targetvalue_view).setTextColor(Color.WHITE);
    ((TextView) targetvalue_view).setTextSize(container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    ((TextView) targetvalue_view).setText("--");
    if( target_hidden )
      targetvalue_view.setVisibility(View.INVISIBLE);
    layout.addView(targetvalue_view);

    // TODO add scala values

    initialise(false);

    meter.setColorMap(colorMap);
    meter.setMinMaxValue(-deviation_max, deviation_max);
    init = true;

    alignViews();

    return layout;
  }

  private void alignViews()
  {
    int newPos = meter.getOvalCenter();
    if( lastPos != newPos )
    {
      if( !deviation_hidden )
        deviationvalue_rlayout.topMargin = newPos / 2 + deviationvalue_view.getHeight() / 2;
      else
        deviationvalue_view.setVisibility(View.INVISIBLE);
      deviationvalue_view.requestLayout();
      if( !current_hidden )
        currentvalue_rlayout.topMargin = meter.getBottomPosition() - currentvalue_view.getHeight();
      else
        currentvalue_view.setVisibility(View.INVISIBLE);
      currentvalue_view.requestLayout();
      if( !target_hidden )
        targetvalue_rlayout.topMargin = meter.getBottomPosition() - targetvalue_view.getHeight();
      else
        targetvalue_view.setVisibility(View.INVISIBLE);
      targetvalue_view.requestLayout();
      lastPos = newPos;
    }
  }

  @Override
  public void updateUI()
  {
    if( !init )
      return;

    update();

    if( changed )
    {

      double deviation;
      if( current_value != null && target_value != null )
        deviation = current_value - target_value;
      else
        deviation = 0;

      if( !deviation_hidden )
        ((TextView) deviationvalue_view).setText(format(deviation, decimals));
      if( !current_hidden )
        ((TextView) currentvalue_view).setText(format(current_value, decimals));
      if( !target_hidden )
        ((TextView) targetvalue_view).setText(format(target_value, decimals));
      meter.setValue(deviation);
      meter.postInvalidate();
    }

    alignViews();

    post();
  }
}
