package at.univie.mma.ui.widget.target;

import java.util.*;

public class SimpleTargetInt extends SimpleTargetFragment {
  public void updateUI()
  {
    super.update();

    if( current_value == null )
      txtCurrent.setText("--");
    else
      txtCurrent.setText(String.format(Locale.ROOT, "%df", current_value.longValue()));
    if( target_value == null )
      txtTarget.setText("--");
    else
      txtTarget.setText(String.format(Locale.ROOT, "%d", target_value.longValue()));

    super.post();
  }
}