package at.univie.mma.ui.widget.target;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import at.univie.mma.ui.views.ValueTacho;
import at.univie.mma.ui.views.ValueTacho.LabelConverter;
import at.univie.mma.ui.views.ValueTachoDeviation;

public class TargetTachoFragment extends TargetColoredDeviationFragment
{
  /**
   * Number of minor ticks to be displayed (Default: 1)
   */
  public static final String ARGUMENT_MINOR_TICKS= "ARG_MT";
  
  /**
   * String to be added to tick numbers when mode is 'relative'. (Default ' %')
   */
  public static final String ARGUMENT_RELATIVE_STRING= "ARG_RELS";
  
  private ValueTacho tacho= null;
  
  private double minValue;
  private double maxValue;
  private double spread;
  
  RelativeLayout layout;
  
  private Context ctx;
  
  private boolean changed_optimal= false;
  private double deviation_optimal_last= 0;
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    LOG_TAG = TargetTachoFragment.class.getSimpleName();

    assert container != null;
    layout= new RelativeLayout(container.getContext());
    ctx= container.getContext();
    
    init();
    
    return layout;
  }
  
  private void init()
  {
    super.initialise(false);
    
    if( deviation_optimal != 0 )
      tacho= new ValueTachoDeviation(ctx);
    else
      tacho= new ValueTacho(ctx);
    
    if( deviation_absolute )
    {
      tacho.setLabelConverter(new LabelConverter() {
        
        @Override
        public String getLabelFor(double progress, double maxProgress)
        {
          double val= minValue + spread * progress / maxProgress;
          return format(val, decimals);
        }
      });
      
      minValue= target_value - deviation_max;
      maxValue= target_value + deviation_max;
      spread= deviation_max * 2;
      
      if( deviation_max >= 6 )
        tacho.setMajorTickStep(spread / 12d);
      else
        tacho.setMajorTickStep(1);
      
      tacho.setMinorTicks(getArguments().getInt(ARGUMENT_MINOR_TICKS, 1));
      
      tacho.setMinMaxValue(target_value - deviation_max, target_value + deviation_max);
    }
    else
    {
      final String addTick= getArguments().getString(ARGUMENT_RELATIVE_STRING, " %");
      tacho.setLabelConverter(new LabelConverter() {
        
        @Override
        public String getLabelFor(double progress, double maxProgress)
        {
          double val= minValue + spread * progress / maxProgress;
          return format(val, decimals) + addTick;
        }
      });
      
      minValue= -deviation_max;
      maxValue= deviation_max;
      spread= deviation_max * 2;
      
      if( deviation_max >= 6 )
        tacho.setMajorTickStep(spread / 12d);
      else
        tacho.setMajorTickStep(1);
      
      tacho.setMinorTicks(getArguments().getInt(ARGUMENT_MINOR_TICKS, 1));
      
      tacho.setMinMaxValue(minValue, maxValue);
    }
    
    if( deviation_optimal != 0 )
      ((ValueTachoDeviation) tacho).setOptimal((float) (deviation_optimal / deviation_max));
    
    layout.addView(tacho);
    
    init= true;
  }
  
  @Override
  protected void update()
  {
    super.update();
    if( deviation_optimal_last != deviation_optimal )
    {
      deviation_optimal_last= deviation_optimal;
      changed_optimal= true;
    }
    else
      changed_optimal= false;
  }
  
  @Override
  public void updateUI()
  {
    if( !init )
      return;
    
    update();
    
    if( target_value != null )
    {
      if( changed_optimal )
      {
        if( deviation_optimal != 0 && !(tacho instanceof ValueTachoDeviation)
            || deviation_optimal == 0 && tacho instanceof ValueTachoDeviation )
        {
          layout.removeAllViews();
          // Init also sets the deviation
          init();
        }
        else
        {
          assert tacho instanceof ValueTachoDeviation;
          ((ValueTachoDeviation) tacho).setOptimal((float) (deviation_optimal / deviation_max));
        }
      }

      if( changed )
      {
        if( deviation_absolute )
        {
          if( target_value != null )
          {
            minValue = target_value - deviation_max;
            maxValue = target_value + deviation_max;
          }
          else
          {
            minValue = maxValue = current_value;
          }
          spread= deviation_max * 2;
          tacho.setMinMaxValue(minValue, maxValue);
          
          tacho.setValue(current_value);
        }
        else
        {
          minValue= -deviation_max;
          maxValue= deviation_max;
          spread= deviation_max * 2;
          tacho.setMinMaxValue(minValue, maxValue);

          double diff= current_value / target_value - 1;
          tacho.setValue((float) diff);
        }
      }
    }
    else
    {
      tacho.setValue(0);
      tacho.setMinMaxValue(0, 0);
    }

    post();
  }
}
