package at.univie.mma.ui.widget.target;

import android.annotation.*;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.support.v4.content.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import at.univie.mma.R;

public class TargetBarFragment extends TargetColoredDeviationFragment {
  private RelativeLayout mLayout;
  private BarCanvas canvas;
  private TextView actualvalueDown_view;
  private TextView actualvalueUp_view;

  public static TargetBarFragment create(String target, String value, boolean absolute)
  {
    TargetBarFragment ret = new TargetBarFragment();

    ret.setArguments(createArguments(target, value, absolute));

    return ret;
  }

  @SuppressLint("InlinedApi") @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    assert container != null;
    mLayout = new RelativeLayout(container.getContext());

    canvas = new BarCanvas(container.getContext());
    RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
        RelativeLayout.LayoutParams.MATCH_PARENT);
    canvas.setLayoutParams(params1);
    mLayout.addView(canvas);

    targetvalue_view = new TextView(container.getContext());
    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    params2.addRule(RelativeLayout.RIGHT_OF, canvas.getId());
    params2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    targetvalue_view.setLayoutParams(params2);
    ((TextView) targetvalue_view).setTextColor(Color.WHITE);
    ((TextView) targetvalue_view).setTextSize(container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    targetvalue_view.setBackgroundColor(ContextCompat.getColor(container.getContext(), android.R.color.transparent));
    mLayout.addView(targetvalue_view);

    actualvalueUp_view = new TextView(container.getContext());
    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    // params3.addRule(RelativeLayout.LEFT_OF, canvas.getId());
    if( Build.VERSION.SDK_INT >= 17 )
      params3.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
    params3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
    params3.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
    actualvalueUp_view.setLayoutParams(params3);
    actualvalueUp_view.setTextColor(Color.WHITE);
    actualvalueUp_view.setTextSize(container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    actualvalueUp_view.setBackgroundColor(ContextCompat.getColor(container.getContext(), android.R.color.transparent));
    mLayout.addView(actualvalueUp_view);

    actualvalueDown_view = new TextView(container.getContext());
    RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT);
    params4.addRule(RelativeLayout.RIGHT_OF, canvas.getId());
    params4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
    if( Build.VERSION.SDK_INT >= 17 )
      params4.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
    params4.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
    actualvalueDown_view.setLayoutParams(params4);
    actualvalueDown_view.setTextColor(Color.WHITE);
    actualvalueDown_view.setTextSize(container.getContext().getResources().getDimension(R.dimen.textsize_training_fragment_small));
    actualvalueDown_view.setBackgroundColor(ContextCompat.getColor(container.getContext(), android.R.color.transparent));
    mLayout.addView(actualvalueDown_view);

    initialise(true);

    return mLayout;
  }

  @Override public void updateUI()
  {
    if( !init )
      return;

    update();

    if( target_value != null )
    {
      ((TextView) targetvalue_view).setText(format(target_value, decimals));

      if( deviation_absolute )
      {
        canvas.setValues(target_value, current_value, current_value - target_value, deviation_optimal);
      }
      else
      {
        //TODO: check this
        canvas.setValues(target_value, current_value, current_value / target_value - 1.0, deviation_optimal / target_value - 1.0);
      }

      if( changed )
      {
        canvas.invalidate();
        if( current_value > target_value )
        {
          actualvalueDown_view.setVisibility(View.INVISIBLE);
          actualvalueUp_view.setVisibility(View.VISIBLE);
          actualvalueUp_view.setText(format(current_value, decimals));
        }
        else if( current_value < target_value )
        {
          actualvalueUp_view.setVisibility(View.INVISIBLE);
          actualvalueDown_view.setVisibility(View.VISIBLE);
          actualvalueDown_view.setText(format(current_value, decimals));
        }
        else
        {
          actualvalueUp_view.setVisibility(View.INVISIBLE);
          actualvalueDown_view.setVisibility(View.INVISIBLE);
        }
      }
    }
    else
    {
      if( changed )
      {
        // No target set -> hide all
        actualvalueUp_view.setVisibility(View.INVISIBLE);
        actualvalueDown_view.setVisibility(View.INVISIBLE);
        ((TextView) targetvalue_view).setText("");
        canvas.setValues(0, 0, 0);
        canvas.invalidate();
      }
    }

    // TODO better (ie move to respective parts of code)
    if( target_hidden )
    {
      targetvalue_view.setVisibility(View.INVISIBLE);
      targetvalue_view.requestLayout();
    }
    if( current_hidden )
    {
      actualvalueUp_view.setVisibility(View.INVISIBLE);
      actualvalueDown_view.setVisibility(View.INVISIBLE);
    }

    post();
  }

  public class BarCanvas extends View {
    public int width;
    public int height;
    Context context;
    private Bitmap mBitmap;
    private Paint mPaintline;
    private Paint paint;

    private double deviation;

    private double optimal;

    private float center;
    private float heighthalf;

    private float density;
    private int margin;
    private int margin2;
    private int margin3;

    public BarCanvas(Context c)
    {
      super(c);
      context = c;

      setup();
    }

    public BarCanvas(Context c, AttributeSet attrs)
    {
      super(c, attrs);
      context = c;

      setup();
    }

    public BarCanvas(Context c, AttributeSet attrs, int defStyle)
    {
      super(c, attrs, defStyle);
      context = c;

      setup();
    }

    private void setup()
    {
      density = getResources().getDisplayMetrics().density;
      margin = Math.round(20 * density);
      margin2 = Math.round(30 * density);
      margin3 = Math.round(28 * density);

      mPaintline = new Paint();
      mPaintline.setAntiAlias(true);
      mPaintline.setColor(Color.YELLOW);
      mPaintline.setStyle(Paint.Style.STROKE);
      mPaintline.setStrokeJoin(Paint.Join.ROUND);
      mPaintline.setStrokeWidth(4f);

      paint = new Paint();
      paint.setColor(Color.RED);
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      paint.setStrokeWidth(4);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
      super.onSizeChanged(w, h, oldw, oldh);

      center = (float) (h / 2.0);
      width = w;
      height = h;
      heighthalf = height - center;

      // Canvas will draw onto the defined Bitmap
      mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
      new Canvas(mBitmap);
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas)
    {
      super.onDraw(canvas);

      // draw center line
      canvas.drawLine(margin, center, width - margin, center, mPaintline);

      // draw the mPath with the mPaint on the canvas when onDraw
      float leftx = margin2;
      float rightx = width - margin2;
      float topy;
      float bottomy;

      if( deviation_absolute )
      {
        // Do not scale with deviation_max here at this has been done already in setValues
        if( deviation > 0 )
        {
          topy = center - (float) deviation * heighthalf;
          bottomy = center;
        }
        else
        {
          topy = center;
          bottomy = center - (float) deviation * heighthalf;
        }
      }
      else
      {
        if( deviation > 0 )
        {
          topy = center - (float) deviation * heighthalf;
          bottomy = center;
        }
        else
        {
          topy = center;
          bottomy = center - (float) deviation * heighthalf;
        }
      }

      paint.setColor(colorMap.getColor((float) deviation));
      canvas.drawRect(leftx, topy, rightx, bottomy, paint);

      if( optimal != 0 )
      {
        float h = (float) (optimal / deviation_max) * heighthalf;
        canvas.drawRect(margin3, center - h, width - margin3, center + h, mPaintline);
      }
    }

    public void clearCanvas()
    {
      invalidate();
    }

    public void setValues(double target, double actual, double deviation, double optimal)
    {
      deviation = Math.min(deviation, deviation_max);
      deviation = Math.max(deviation, -deviation_max);
      deviation = deviation / deviation_max;

      this.optimal = optimal;

      this.deviation = deviation;
    }

    public void setValues(double target, double actual, double deviation)
    {
      setValues(target, actual, deviation, 0);
    }
  }
}
