package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import at.univie.mma.R;
import at.univie.mma.ui.MeasurementFragment;
import at.pegasos.client.values.Value;
import at.pegasos.client.values.ValueStore;

public abstract class ValueFragment extends MeasurementFragment implements ScalableMeasurementFragment {
  public static final String ARGUMENT_NAME= "ARG_N";
  public static final String ARGUMENT_TXT_L= "ARG_TXTL";
  public static final String ARGUMENT_TXT_R= "ARG_TXTR";
  public static final String ARGUMENT_IMG_L= "ARG_IMGL";
  public static final String ARGUMENT_IMG_R= "ARG_IMGR";
  public static final String ARGUMENT_RequiredWidthString= "ARG_REQ_WIDTH";

  private TextView txtL;
  private TextView txtR;
  private ImageView imgL;
  private ImageView imgR;
  private TextView txtLs;
  private TextView txtRs;
  private ImageView imgLs;
  private ImageView imgRs;
  protected View view;

  boolean hasImgL= false;
  boolean hasImgR= false;

  /**
   * Width required for the value. This is only for the value; units/images/etc. are not included!
   */
  protected int requiredWidth;

  protected String requiredWidthString= "000";

  protected Float sizeLarge;

  protected Value value;

  protected String name;

  protected void initialise(boolean set_init)
  {
    super.initialise(false);

    txtL= view.findViewById(R.id.txt1);
    txtLs= view.findViewById(R.id.txt1s);
    txtR= view.findViewById(R.id.txt2);
    txtRs= view.findViewById(R.id.txt2s);

    imgL= view.findViewById(R.id.img_left);
    imgLs= view.findViewById(R.id.img_lefts);
    imgR= view.findViewById(R.id.img_right);
    imgRs= view.findViewById(R.id.img_rights);

    imgL.setScaleType(ImageView.ScaleType.FIT_CENTER);
    imgLs.setScaleType(ImageView.ScaleType.FIT_CENTER);
    imgR.setScaleType(ImageView.ScaleType.FIT_CENTER);
    imgRs.setScaleType(ImageView.ScaleType.FIT_CENTER);

    Bundle args= getArguments();

    name= args.getString(ARGUMENT_NAME);
    value= ValueStore.getInstance().getValue(name);

    String txtLh= args.getString(ARGUMENT_TXT_L);
    String txtRh= args.getString(ARGUMENT_TXT_R);
    int imgLh= args.getInt(ARGUMENT_IMG_L, 0);
    int imgRh= args.getInt(ARGUMENT_IMG_R, 0);

    // Do we have an image specified for the left side?
    if( imgLh == 0 )
    {
      imgL.setVisibility(android.view.View.INVISIBLE);
      imgLs.setVisibility(android.view.View.INVISIBLE);
      txtL.setVisibility(android.view.View.VISIBLE);
      txtL.setText(txtLh);
      txtLs.setText(txtLh);
    }
    else
    {
      hasImgL= true;
      imgL.setVisibility(android.view.View.VISIBLE);
      txtL.setVisibility(android.view.View.INVISIBLE);
      txtLs.setVisibility(android.view.View.INVISIBLE);
      imgL.setImageResource(imgLh);
      imgLs.setImageResource(imgLh);
    }

    if( imgRh == 0 )
    {
      imgR.setVisibility(android.view.View.INVISIBLE);
      imgRs.setVisibility(android.view.View.INVISIBLE);
      txtR.setVisibility(android.view.View.VISIBLE);
      txtR.setText(txtRh);
      txtRs.setText(txtRh);
    }
    else
    {
      hasImgR= true;
      imgR.setVisibility(android.view.View.VISIBLE);
      txtR.setVisibility(android.view.View.INVISIBLE);
      txtRs.setVisibility(android.view.View.INVISIBLE);
      imgR.setImageResource(imgRh);
      imgRs.setImageResource(imgRh);
    }

    requiredWidthString = args.getString(ARGUMENT_RequiredWidthString, requiredWidthString);

    init= true;
  }

  @Override
  public int getRequiredWidth()
  {
    if( !isAdded() )
      return 0;
/*
    requiredWidth= ScalableMeasurementFragmentHelper.requiredWidthLarge(requiredWidthString);
    return requiredWidth;
*/
    if( sizeLarge != null )
      requiredWidth = ScalableMeasurementFragmentHelper.requiredWidth(requiredWidthString, sizeLarge);
    else
      requiredWidth = ScalableMeasurementFragmentHelper.requiredWidthLarge(requiredWidthString);

    return requiredWidth;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    this.sizeLarge = sizeLarge;
    txtL.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    txtLs.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeSmall);
    txtR.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    txtRs.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeSmall);

    if( requiredWidth + 2 * sizeLarge > availableWidth )
    {
      if( hasImgL )
      {
        imgL.setVisibility(View.INVISIBLE);
        imgLs.setVisibility(View.VISIBLE);
        txtL.setVisibility(View.INVISIBLE);
        txtLs.setVisibility(View.INVISIBLE);

        imgLs.getLayoutParams().height= (int) (sizeSmall * 0.8);
        imgLs.getLayoutParams().width= (int) (sizeSmall * 0.8);
        imgLs.requestLayout();
      }
      else
      {
        imgL.setVisibility(View.INVISIBLE);
        imgLs.setVisibility(View.INVISIBLE);
        txtL.setVisibility(View.INVISIBLE);
        txtLs.setVisibility(View.VISIBLE);
      }

      if( hasImgR )
      {
        imgR.setVisibility(View.INVISIBLE);
        imgRs.setVisibility(View.VISIBLE);
        txtR.setVisibility(View.INVISIBLE);
        txtRs.setVisibility(View.INVISIBLE);

        imgRs.getLayoutParams().height= (int) sizeSmall;
        imgRs.getLayoutParams().width= (int) sizeSmall;
      }
      else
      {
        imgR.setVisibility(View.INVISIBLE);
        imgRs.setVisibility(View.INVISIBLE);
        txtR.setVisibility(View.INVISIBLE);
        txtRs.setVisibility(View.VISIBLE);
      }
    }
    else
    {
      if( hasImgL )
      {
        imgL.setVisibility(View.VISIBLE);
        imgLs.setVisibility(View.INVISIBLE);
        txtL.setVisibility(View.INVISIBLE);
        txtLs.setVisibility(View.INVISIBLE);

        // imgL.setMaxHeight((int) (sizeLarge * 0.9) );
        imgL.getLayoutParams().height= (int) (sizeLarge * 0.8);
        imgL.getLayoutParams().width= (int) (sizeLarge * 0.8);
        imgL.requestLayout();
      }
      else
      {
        imgL.setVisibility(View.INVISIBLE);
        imgLs.setVisibility(View.INVISIBLE);
        txtL.setVisibility(View.VISIBLE);
        txtLs.setVisibility(View.INVISIBLE);
      }

      if( hasImgR )
      {
        imgR.setVisibility(View.VISIBLE);
        imgRs.setVisibility(View.INVISIBLE);
        txtR.setVisibility(View.INVISIBLE);
        txtRs.setVisibility(View.INVISIBLE);

        imgR.getLayoutParams().height= (int) sizeLarge;
        imgR.getLayoutParams().width= (int) sizeLarge;
      }
      else
      {
        imgR.setVisibility(View.INVISIBLE);
        imgRs.setVisibility(View.INVISIBLE);
        txtR.setVisibility(View.VISIBLE);
        txtRs.setVisibility(View.INVISIBLE);
      }
    }
  }

  /**
   * Create a the arguments for a value fragment. With nothing else (no pictures, ...) to display
   * 
   * @param value_name
   *          the name of the variable to display
   * @return
   */
  protected static Bundle createArguments(String value_name)
  {
    Bundle args = new Bundle();

    args.putString(ARGUMENT_NAME, value_name);

    return args;
  }
}
