package at.univie.mma.ui.widget;

public interface ScalableMeasurementFragment {
  int getRequiredWidth();

  void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth);
}
