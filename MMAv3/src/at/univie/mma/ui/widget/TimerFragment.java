package at.univie.mma.ui.widget;

import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import at.univie.mma.R;
import at.univie.mma.ui.MeasurementFragment;
import at.univie.mma.ui.MillisecondChronometer;

public abstract class TimerFragment extends MeasurementFragment implements ScalableMeasurementFragment {
  protected MillisecondChronometer chrono;
  protected TextView lbl;
  protected View view;

  protected void initialise(View view)
  {
    this.view= view;
    chrono= view.findViewById(R.id.elapsed_time);
    lbl= view.findViewById(R.id.chronometer_label);
  }

  private Float sizeLarge;

  @Override
  public int getRequiredWidth()
  {
    if( !isAdded() )
      return 0;

    if( sizeLarge != null )
      return ScalableMeasurementFragmentHelper.requiredWidth("00:00.0", sizeLarge);
    else
      return ScalableMeasurementFragmentHelper.requiredWidthLarge("00:00.0");
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    this.sizeLarge = sizeLarge;
    chrono.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);

    // For some time fragments it can happen that we have no label
    if( lbl != null )
      lbl.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLabel);
    view.requestLayout();
  }
}
