package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.pegasos.client.values.ValueStore;

public class CurrentSpeedFragmentMinKm extends MeasurementFragment {

  ValueStore vals;

  TextView txtSpeed1;
  TextView txtSpeed2;

  int s1, s2;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_current_speed_minkm, container, false);
    
    txtSpeed1= (TextView) view.findViewById(R.id.speed1);
    txtSpeed2= (TextView) view.findViewById(R.id.speed2);
    
    vals= ValueStore.getInstance();
    if( Config.LOGGING )
      Log.d("CSFmkm", "create " + vals);
    
    initialise(true);
    
    return view;
  }
  
  public void updateUI()
  {
    if( !init )
      return;
    
    s1= (int) vals.getCurrentSpeed_minkm_min();
    s2= (int) vals.getCurrentSpeed_minkm_sec();
    
    if( s1 == s2 && s1 == -1 )
    {
      txtSpeed1.setText("--");
      txtSpeed2.setText("--");
    }
    else
    {
      txtSpeed1.setText("" + String.format("%d", s1));
      txtSpeed2.setText("" + String.format("%02d", s2));
    }
  }
}
