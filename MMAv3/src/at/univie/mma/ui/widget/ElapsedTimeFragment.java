package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.pegasos.client.ui.TrainingActivity;

public class ElapsedTimeFragment extends MeasurementFragment
{
  private MillisecondChronometer chrono;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.fragment_elapsed_time, container, false);

    chrono = (MillisecondChronometer) view.findViewById(R.id.elapsed_time);
    chrono.setBase(TrainingActivity.getStartTime());
    chrono.start();

    initialise(true);

    return view;
  }

  @Override
  public void onDestroy()
  {
    if( chrono != null )
      chrono.stop();
    super.onDestroy();
  }

  @Override
  public void updateUI()
  {
  }
}
