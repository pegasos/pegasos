package at.univie.mma.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.pegasos.client.values.ValueStore;

public class CurrentSpeedFragment extends MeasurementFragment {

  ValueStore vals;

  TextView txtSpeed1;
  TextView txtSpeed2;

  int s1, s2;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_current_speed, container, false);
    
    txtSpeed1= (TextView) view.findViewById(R.id.speed1);
    txtSpeed2= (TextView) view.findViewById(R.id.speed2);
    
    vals= ValueStore.getInstance();
    
    initialise(true);
    
    return view;
  }
  
  public void updateUI()
  {
    if( !init )
      return;
    
    double speed= vals.getCurrentSpeed_kmh();
    if( speed == -1 )
    {
      txtSpeed1.setText("--");
      txtSpeed2.setText("--");
    }
    else
    {
      s1= (int) speed;
      s2= ((int) (speed * 100)) % 100;
      txtSpeed1.setText("" + String.format("%d", s1));
      if( s2 < 10 )
        txtSpeed2.setText("" + String.format("%02d", s2));
      else
        txtSpeed2.setText("" + String.format("%d", s2));
    }
  }
}
