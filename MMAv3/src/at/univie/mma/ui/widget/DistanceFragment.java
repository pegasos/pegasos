package at.univie.mma.ui.widget;

import android.annotation.*;
import android.os.Bundle;
import android.util.*;
import android.view.*;
import android.widget.*;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.pegasos.client.values.ValueStore;

public class DistanceFragment extends MeasurementFragment implements ScalableMeasurementFragment {

  private final static String requiredWidthString = "000.00";
  private final static String requiredWidthStringUnit = "km";

  private Float sizeLarge;
  private Float sizeSmall;

  ValueStore store;

  TextView txtDistance;
  TextView txtUnit;
  ImageView imgDistanceSymbol;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_distance, container, false);

    txtDistance = view.findViewById(R.id.txtDistance);
    txtUnit = view.findViewById(R.id.txtUnit);

    imgDistanceSymbol = view.findViewById(R.id.imgDistanceSymbol);

    store= ValueStore.getInstance();

    initialise(true);

    return view;
  }

  @SuppressLint({"DefaultLocale", "SetTextI18n"})
  public void updateUI()
  {
    if( !init )
      return;

    long dist = store.getDistance();

    if( dist == -1 )
    {
      txtDistance.setText("--");
      txtUnit.setText("");
    }
    else if( dist <= 1000 )
    {
      txtDistance.setText(String.format("%5d", dist));
      txtUnit.setText("m");
    }
    else if( dist < 10000 )
    {
      double d = dist / 1000.0;
      int s1 = (int) d;
      int s2 = ((int) (d * 1000)) % 1000;
      txtDistance.setText(String.format("%d.%03d", s1, s2));
      /*txtPre.setText(String.format("%d", s1));
      if( s2 < 10 )
        txtPost.setText(String.format("%02d", s2));
      else
        txtPost.setText(String.format("%d", s2));*/
      // txtDistance.setText("" + String.format("%.3f", dist / 1000.0));
      txtUnit.setText("km");
    }
    else if( dist < 100_000 )
    {
      double d = dist / 1000.0;
      int s1 = (int) d;
      int s2 = ((int) (d * 100)) % 100;
      txtDistance.setText(String.format("%d.%02d", s1, s2));
      // txtDistance.setText("" + String.format("%.2f", dist / 1000.0));
      txtUnit.setText("km");
    }
    else
    {
      double d = dist / 1000.0;
      int s1 = (int) d;
      int s2 = ((int) (d * 10)) % 10;
      txtDistance.setText(String.format("%d.%01d", s1, s2));
      // txtDistance.setText("" + String.format("%.1f", dist / 1000.0));
      txtUnit.setText("km");
    }
  }

  @Override
  public int getRequiredWidth()
  {
    if( !isAdded() )
      return 0;

    int requiredWidth;
    if( sizeLarge != null )
      requiredWidth = (int) (ScalableMeasurementFragmentHelper.requiredWidth(requiredWidthString, sizeLarge) +
                ScalableMeasurementFragmentHelper.requiredWidth(requiredWidthStringUnit, sizeSmall) * 1.5);
    else
      requiredWidth = (int) (ScalableMeasurementFragmentHelper.requiredWidthLarge(requiredWidthString) +
                ScalableMeasurementFragmentHelper.requiredWidthSmall(requiredWidthStringUnit) * 1.5);

    return requiredWidth;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    this.sizeLarge = sizeLarge;
    this.sizeSmall = sizeSmall;

    txtDistance.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    txtUnit.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeSmall);

    imgDistanceSymbol.setScaleType(ImageView.ScaleType.FIT_CENTER);
    imgDistanceSymbol.getLayoutParams().height = (int) (sizeSmall * 0.8);
    imgDistanceSymbol.getLayoutParams().width = (int) (sizeSmall * 0.8);
    imgDistanceSymbol.requestLayout();
  }
}
