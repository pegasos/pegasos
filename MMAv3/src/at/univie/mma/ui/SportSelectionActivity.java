package at.univie.mma.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

import at.pegasos.client.ui.*;
import at.pegasos.client.util.PreferenceManager;
import at.univie.mma.Config;
import at.pegasos.client.Preferences;
import at.univie.mma.R;
import at.pegasos.client.sports.Sports;

public class SportSelectionActivity extends PegasosClientView {
  int cur;
  private ImageView img;
  private TextView txt;
  private boolean started = false;
  private int activity_c;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sport_selection);

    ImageButton next = (ImageButton) findViewById(R.id.btnImgNext);
    ImageButton previous = (ImageButton) findViewById(R.id.btnImgPrev);

    final Intent intent = getIntent();

    this.activity_c = intent.getIntExtra(TrainingActivity.EXTRA_ACTIVITY, 0);

    img = (ImageView) findViewById(R.id.imgSportIcon);
    img.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        PreferenceManager.getInstance().storeInt(Preferences.sport_selection_last_sport, cur);

        Intent i = new Intent(SportSelectionActivity.this.getApplicationContext(), TrainingLandingScreen.class);
        i.putExtra(TrainingActivity.EXTRA_ACTIVITY, activity_c);
        i.putExtra(TrainingActivity.EXTRA_SPORT, Sports.sportNames[cur]);

        String arguments = intent.getStringExtra(TrainingActivity.EXTRA_ARGUMENT);
        if( arguments != null )
          i.putExtra(TrainingActivity.EXTRA_ARGUMENT, arguments);

        started = true;
        startActivity(i);
      }
    });

    txt = (TextView) findViewById(R.id.txtSport);

    cur = PreferenceManager.getInstance().getInt(Preferences.sport_selection_last_sport, 0);

    next.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        nextSport();
      }
    });

    previous.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        previousSport();
      }
    });

    loadCurrentSport();
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    if( started )
      finish();
  }

  private void loadCurrentSport()
  {
    if( Config.LOGGING ) Log.d("SportSelection", "Show icon: " + cur);
    img.setImageResource(Sports.sportIcons[cur]);
    txt.setText(Sports.sportNames[cur]);
  }

  private void nextSport()
  {
    cur++;
    if( cur >= Sports.sportIcons.length )
      cur = 0;
    if( Config.LOGGING ) Log.d("SportSelection", "Sport: " + cur);
    loadCurrentSport();
  }

  private void previousSport()
  {
    cur--;
    if( cur < 0 )
      cur = Sports.sportIcons.length - 1;
    if( Config.LOGGING ) Log.d("SportSelection", "Sport: " + cur);
    loadCurrentSport();
  }
}
