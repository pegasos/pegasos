package at.univie.mma.ui.extrascreen;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.ui.*;

public class PerPotZeitprognose extends MeasurementFragment
{
  private TextView chrono;
  private String   time;
  private long     last_timeestimate= 0;
  private boolean  update_required= true;
  
  /*public ElapsedTimeFragment() {
    
  }*/
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.fragment_perpot_prognose, container, false);
    
    if( last_timeestimate == 0 )
      time= "-:--:--";
    else
      time= String.format("%1d:%2d:%2d", new Object[] { Long.valueOf(last_timeestimate / 3600L), Long.valueOf(last_timeestimate % 3600L / 60L), Long.valueOf(last_timeestimate % 60L) });
    chrono= (TextView) view.findViewById(R.id.elapsed_time);
    chrono.setText(time);
    
    initialise(true);
    
    return view;
  }
  
  @Override
  public void updateUI()
  {
    if( !init ) return;
    if( Config.LOGGING ) Log.d("PerPotScreen", "updateUI: " + update_required + " " + time);
    if( update_required )
    {
      chrono.setText(time);
      update_required= false;
    }
  }

  public void setTimeEstimate(long zeitprognose)
  {
    if( Config.LOGGING ) Log.d("PerPotScreen", "zeitprognose " + zeitprognose);
    // time= String.format("%1d Stunden, %2d Minuten, %2d Sekunden.", new Object[] { Long.valueOf(zeitprognose / 3600L), Long.valueOf(zeitprognose % 3600L / 60L), Long.valueOf(zeitprognose % 60L) });
    if( zeitprognose != last_timeestimate )
    {
      time= String.format("%1d:%2d:%2d", new Object[] { Long.valueOf(zeitprognose / 3600L), Long.valueOf(zeitprognose % 3600L / 60L), Long.valueOf(zeitprognose % 60L) });
      last_timeestimate= zeitprognose;
      update_required= true;
    }
  }

}
