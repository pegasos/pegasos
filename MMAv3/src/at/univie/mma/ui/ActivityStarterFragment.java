package at.univie.mma.ui;

import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.pegasos.client.ui.*;
import org.acra.ACRA;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import at.univie.mma.BuildConfig;
import at.univie.mma.Config;
import at.univie.mma.R;

public class ActivityStarterFragment extends Fragment implements IActivityStarterFragment {

  /**
   * Callback interface for activity starter fragments
   */
  public interface ActivityStarterCallback {
    /**
     * Invoked when the fragment was clicked by the user i.e. when the activity should be started
     * 
     * @param fragment
     *          fragment which was clicked
     */
    public void onActivityClicked(ActivityStarterFragment fragment);
  }

  public static class StartActivitySport implements ActivityStarterCallback {
    private Context ctx;
    private int activity_c;
    private String sport;
    
    public StartActivitySport(final Context ctx, final int activity_c, final String sport)
    {
      this.ctx= ctx;
      this.activity_c= activity_c;
      this.sport= sport;
    }

    @Override
    public void onActivityClicked(ActivityStarterFragment fragment)
    {
      Intent i= new Intent(ctx, TrainingLandingScreen.class);
      i.putExtra(TrainingLandingScreen.EXTRA_ACTIVITY, activity_c);
      i.putExtra(TrainingLandingScreen.EXTRA_SPORT, sport);
      ActivityStarterFragment.setParameters(fragment, i);
      ctx.startActivity(i);
    }
  }

  public static class SelectSportForActivity implements ActivityStarterCallback {
    private Context ctx;
    private int activity;

    public SelectSportForActivity(final Context ctx, final int activity)
    {
      this.ctx= ctx;
      this.activity= activity;
    }
 
    public void onActivityClicked(ActivityStarterFragment fragment)
    {
      Intent i= new Intent(ctx, SportSelectionActivity.class);
      i.putExtra(TrainingActivity.EXTRA_ACTIVITY, activity);
      ActivityStarterFragment.setParameters(fragment, i);
      ctx.startActivity(i);
    }
  }

  public static class OpenMenu implements ActivityStarterCallback {
    private final MenuManager manager;
    private final int nr;
 
    public OpenMenu(MenuManager manager, int nr)
    {
      this.manager= manager;
      this.nr= nr;
    }

    @Override
    public void onActivityClicked(ActivityStarterFragment fragment)
    {
      // No need for setting intent parameters here. However, just some safety check
      if( BuildConfig.DEBUG && !(fragment.auto_start == false) )
        throw new AssertionError();

      manager.openMenu(nr);
    }
  }

  public static class OpenIntent implements ActivityStarterCallback {
    private Context ctx;
    private String classname;
    private final Map<String, String> params;

    public OpenIntent(Context ctx, String classname)
    {
      this.ctx= ctx;
      this.classname= classname;
      params= new HashMap<String, String>();
    }

    public OpenIntent addParam(String name, String value)
    {
      params.put(name, value);
      return this;
    }

    @Override
    public void onActivityClicked(ActivityStarterFragment fragment)
    {
      // No need for setting intent parameters here. However, just some safety check
      if( BuildConfig.DEBUG && !(fragment.auto_start == false) )
        throw new AssertionError();

      Intent i;
      try
      {
        i= new Intent(ctx, Class.forName(classname));
        for(Map.Entry<String, String> e : params.entrySet())
        {
          i.putExtra(e.getKey(), e.getValue());
        }
        ctx.startActivity(i);
      }
      catch( ClassNotFoundException e )
      {
        ACRA.getErrorReporter().handleException(e);
        e.printStackTrace();
      }
    }
  }

  private int pic;
  private int text;
  private String text_text;
  private ActivityStarterCallback handler;
  private boolean auto_start;
  
  /**
   * Resource which is the target for the user to click on (usually an image)
   */
  protected View clickable;

  public void setPicture(int picture)
  {
    this.pic= picture;
  }

  public void setText(int text)
  {
    this.text= text;
  }

  public void setText(String text)
  {
    this.text_text= text;
  }

  public void setAutoStart(boolean start)
  {
    auto_start= start;
  }

  @Override
  public void onSaveInstanceState(Bundle outState)
  {
    super.onSaveInstanceState(outState);
    outState.putInt("PIC", pic);
    if( text_text != null )
      outState.putString("TEXT_TEXT", text_text);
    else
      outState.putInt("TEXT", text);
    outState.putBoolean("AUTOSTART", auto_start);
  }

  protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view= inflater.inflate(R.layout.activity_starter, container, false);

    ImageView image= (ImageView) view.findViewById(R.id.picture_activity_starter);

    image.setImageResource(pic);
 
    // ((TextView) view.findViewById(R.id.activity_starter_text)).setText("asdf");
    if( text_text == null )
      ((TextView) view.findViewById(R.id.activity_starter_text)).setText(text);
    else
      ((TextView) view.findViewById(R.id.activity_starter_text)).setText(text_text);

    clickable= image;

    return view;
  }

  protected void restoreState(Bundle savedInstanceState)
  {
    // Restore old state
    if( savedInstanceState != null )
    {
      pic= savedInstanceState.getInt("PIC");
      text= savedInstanceState.getInt("TEXT", -1);
      if( text == -1 )
      {
        text= 0;
        text_text= savedInstanceState.getString("TEXT_TEXT");
      }
      auto_start= savedInstanceState.getBoolean("AUTOSTART", false);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    if( container == null ) // TODO: is this the correct way?
      return null;

    restoreState(savedInstanceState);

    View view= createView(inflater, container, savedInstanceState);

    if( handler != null )
    {
      clickable.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
          Log.d("ActivityStarterFragment", "OnClick handler" + handler);
          handler.onActivityClicked(ActivityStarterFragment.this);
        }
      });
    }
    else
    {
      clickable.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
          if( Config.LOGGING )
            Log.d("ActivityStarterFragment", "OnClick");
          Intent i= new Intent(ActivityStarterFragment.this.getActivity().getApplicationContext(), SportSelectionActivity.class);
          startActivity(i);
        }
      });
    }

    return view;
  }

  public void setCallback(ActivityStarterCallback activityStarterCallback)
  {
    this.handler= activityStarterCallback;
  }

  public static void setParameters(ActivityStarterFragment frag, Intent i)
  {
    i.putExtra(TrainingLandingScreen.EXTRA_AUTOSTART, frag.auto_start);
  }

  @Override public boolean getAutoStart()
  {
    return auto_start;
  }
}
