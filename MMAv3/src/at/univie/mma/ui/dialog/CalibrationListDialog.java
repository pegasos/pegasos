package at.univie.mma.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

import at.pegasos.client.ui.dialog.CalibrationDialog;
import at.univie.mma.R;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.sensors.Sensor;

public class CalibrationListDialog {
  private static CalibrationDialog[] dlgs;
  
  public static Dialog CreateDialog(Context ctx, SensorControllerService sensorservice)
  {
    List<CharSequence> namelist;
    List<CalibrationDialog> dlglist;
    
    Sensor[] sensors= sensorservice.getConnectedSensors();
    namelist= new ArrayList<CharSequence>(sensors.length);
    dlglist= new ArrayList<CalibrationDialog>(sensors.length);
    for(int i= 0; i < sensors.length; i++)
    {
      CalibrationDialog[] dlgs= sensors[i].getCalibrationDialog();
      if( dlgs != null )
      {
        for(CalibrationDialog dlg : dlgs)
        {
          namelist.add(dlg.getDisplayName());
          dlglist.add(dlg);
        }
      }
    }
    
    if( namelist.size() == 0 )
      return null;
    
    CharSequence[] names= new CharSequence[namelist.size()];
    dlgs= new CalibrationDialog[dlglist.size()];
    names= namelist.toArray(names);
    dlgs= dlglist.toArray(dlgs);
    
    AlertDialog.Builder builder;
    builder= new AlertDialog.Builder(ctx, R.style.TrainingDialogTheme);
    builder.setTitle(R.string.title_dlg_calibration);
    builder.setItems(names, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which)
      {
        dlgs[which].show();
      }
    });
    
    return builder.create();
  }
}
