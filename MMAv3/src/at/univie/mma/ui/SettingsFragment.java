package at.univie.mma.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import at.pegasos.client.*;
import at.univie.mma.Config;
import at.univie.mma.LocaleManager;
import at.pegasos.client.Preferences;
import at.univie.mma.R;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
  @SuppressLint("NewApi")
  @SuppressWarnings("deprecation")
  @Override
  public void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.preferences);
    Configuration configuration= getResources().getConfiguration();
    
    ListPreference appLanguagePreference= (ListPreference) findPreference(Preferences.pref_key_app_language);
    
    if( LocaleManager.isLanguageFixed(getActivity()) )
    {
      // set default app language
      if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N )
        appLanguagePreference.setValue(configuration.getLocales().get(0).getLanguage());
      else
        appLanguagePreference.setValue(configuration.locale.getLanguage());
    }
    else
      appLanguagePreference.setValue("");
  }
  
  @Override
  public void onResume()
  {
    super.onResume();
    getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }
  
  @Override
  public void onPause()
  {
    super.onPause();
    getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }
  
  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
  {
    if( key.equals(Preferences.pref_key_app_language) )
      setLocale(sharedPreferences.getString(key, ""));
  }
  
  @SuppressLint("NewApi")
  private void setLocale(String language)
  {
    LocaleManager.setNewLocale(getActivity().getApplicationContext(), language);
    
    if( Config.ASK_FOR_RESTART_ON_SETTING_CHANGE )
    {
      // Language is only changed when the app has been restarted
      // Ask use whether he/she wants to do this right now
      AlertDialog.Builder builder= new AlertDialog.Builder(getActivity());
      builder.setTitle(R.string.settings_restart_required_title);
      builder.setMessage(R.string.settings_restart_required);
      builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
          PegasosClient.restartApp(getActivity().getApplicationContext());
        }
      });
      builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
          dialogInterface.dismiss();
        }
      });
      builder.create().show();
    }
    else
    {
      PegasosClient.restartApp(getActivity().getApplicationContext());
    }
  }
}
