package at.univie.mma.ui;

import android.app.Fragment;

public abstract class MeasurementFragment extends Fragment {
  
  protected boolean init= false;

  /**
   * This method should be invoked when the UI is initialised
   */
  protected void initialise(boolean set_init)
  {
    if( set_init )
      init= true;
  }

  /**
   * Update the values on the UI. Usually a Fragment should only update its UI when this method is
   * called
   */
  public abstract void updateUI();
}
