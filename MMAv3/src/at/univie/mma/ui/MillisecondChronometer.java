package at.univie.mma.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.TextView;
import at.univie.mma.R;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;

public class MillisecondChronometer extends android.support.v7.widget.AppCompatTextView {
  // private static final String TAG = "Chronometer";
  
  private long mBase;
  private boolean mVisible;
  private boolean mStarted;
  private boolean mRunning;
  private long duration;
  
  private Handler mHandler;
  
  private static final int TICK_WHAT = 2;
  
  private final DecimalFormat df = new DecimalFormat("00");
  
  private long timeElapsed;
  
  public MillisecondChronometer(Context context)
  {
    this(context, null, 0);
    mHandler= new MyHandler(this);
  }
  
  public MillisecondChronometer(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
    TypedArray a= context.obtainStyledAttributes(attrs, R.styleable.MillisecondChronometer);
    if( a.getBoolean(R.styleable.MillisecondChronometer_countdown, false) )
      mHandler= new CountDownHandler(this);
    else
      mHandler= new MyHandler(this);
    a.recycle();
  }
  
  public MillisecondChronometer(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    TypedArray a= context.obtainStyledAttributes(attrs, R.styleable.MillisecondChronometer);
    if( a.getBoolean(R.styleable.MillisecondChronometer_countdown, false) )
      mHandler= new CountDownHandler(this);
    else
      mHandler= new MyHandler(this);
    a.recycle();
    init();
  }
  
  private void init()
  {
    mBase= SystemClock.elapsedRealtime();
    // setAutoSizeTextTypeWithDefaults();
  }
  
  public void setBase(long base)
  {// TODO: only use this when countdown=false
    mBase= base;
  }
  
  public long getBase()
  {
    return mBase;
  }
  
  public void start()
  {
    mStarted= true;
    updateRunning();
  }
  
  public void start(long base)
  {
    mStarted= true;
    mBase= base;
    updateRunning();
  }
  
  /**
   * Start the clock
   * @param base time[ms] when the countdown started
   * @param duration [ms] for how long the countdown should run
   */
  public void start(long base, long duration)
  {
    mStarted= true;
    mBase= base;
    this.duration= duration;
    updateRunning();
  }
  
  public void stop()
  {
    mStarted= false;
    updateRunning();
  }
  
  public void setStarted(boolean started)
  {
    mStarted= started;
    updateRunning();
  }
  
  public boolean isStarted()
  {
    return mStarted;
  }
  
  @Override
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    mVisible = false;
    updateRunning();
  }
  
  @Override
  protected void onWindowVisibilityChanged(int visibility)
  {
    super.onWindowVisibilityChanged(visibility);
    mVisible = visibility == VISIBLE;
    updateRunning();
  }
  
  private synchronized void updateText(long time)
  {
    int hours = (int) (time / (3600 * 1000));
    int remaining = (int) (time % (3600 * 1000));
    
    int minutes = (int) (remaining / (60 * 1000));
    remaining = (int) (remaining % (60 * 1000));
    
    int seconds = (int) (remaining / 1000);
    remaining = (int) (remaining % (1000));
    
    int milliseconds = (int) (((int) time % 1000) / 100);
    
    String text = "";
    
    if( hours > 0 )
    {
      text += hours + ":";
      text += df.format(minutes) + ":";
      text += df.format(seconds);
    }
    else
    {
      text += df.format(minutes) + ":";
      text += df.format(seconds) + ".";
      text += Integer.toString(milliseconds);
    }

    setText(text);
  }
  
  private void updateRunning()
  {
    boolean running = mVisible && mStarted;
    if( running != mRunning )
    {
      if( running )
      {
        /*if( countdown )
        {
          timeElapsed= SystemClock.elapsedRealtime() - mBase;
          updateText(duration - timeElapsed);
        }
        else
        {
          timeElapsed= SystemClock.elapsedRealtime() - mBase;
          updateText(timeElapsed);
        }*/
        // dispatchChronometerTick();
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), 100);
      } else
      {
        mHandler.removeMessages(TICK_WHAT);
      }
      mRunning = running;
    }
  }
  
  private static class MyHandler extends Handler {
    private final WeakReference<MillisecondChronometer> mActivity;

    public MyHandler(MillisecondChronometer activity) {
      mActivity = new WeakReference<MillisecondChronometer>(activity);
    }
    
    @Override
    public void handleMessage(Message msg) {
      MillisecondChronometer activity = mActivity.get();
      if (activity != null)
      {
        if( activity.mRunning )
        {
          activity.timeElapsed= System.currentTimeMillis() - activity.mBase;
          activity.updateText(activity.timeElapsed);
          sendMessageDelayed(Message.obtain(this, TICK_WHAT), 100);
        }
      }
    }
  };
  
  private static class CountDownHandler extends Handler {
    private final WeakReference<MillisecondChronometer> mActivity;

    public CountDownHandler(MillisecondChronometer activity) {
      mActivity = new WeakReference<MillisecondChronometer>(activity);
    }
    
    @Override
    public void handleMessage(Message msg) {
      MillisecondChronometer activity = mActivity.get();
      if (activity != null)
      {
        if( activity.mRunning )
        {
          activity.timeElapsed= System.currentTimeMillis() - activity.mBase;
          activity.updateText(activity.duration - activity.timeElapsed);
          sendMessageDelayed(Message.obtain(this, TICK_WHAT), 100);
        }
      }
    }
  };
}
