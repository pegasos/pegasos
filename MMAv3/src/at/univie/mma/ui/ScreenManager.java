package at.univie.mma.ui;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;
import at.pegasos.client.ui.*;
import at.univie.mma.Config;
import at.pegasos.client.controller.ITrainingController;
import at.pegasos.client.sports.Sport;
import at.pegasos.client.ui.screen.Screen;
import at.pegasos.client.ui.screen.ThreeSegments;
import at.univie.mma.ui.widget.CurrentSpeedFragment;
import at.univie.mma.ui.widget.CurrentSpeedFragmentMinKm;
import at.univie.mma.ui.widget.ElapsedTimeFragment;
import at.univie.mma.ui.widget.HeartrateFragment;
import at.univie.mma.ui.widget.ScalableMeasurementFragmentHelper;
import at.univie.mma.ui.widget.WallClockFragment;

public class ScreenManager
{
  private static final String LOG_TAG= "ScreenManager";
  
  private class Updater extends TimerTask {
    @Override
    public void run()
    {
      training.runOnUiThread(new Runnable() {

        @Override
        public void run()
        {
          screens[screen_idx].updateUI();
        }
      });
    }
  }
  
  private TrainingActivity training;
  
  private Screen[] screens;

  private int screen_idx;

  private Timer screenupdater;
  
  private static ScreenManager instance= null;
  
  private boolean in_pause= false;

  private Screen screen_to_be_removed= null;

  /**
   * Delay[ms] between screen updates
   */
  private long updateRate= 1000;
  
  public ScreenManager(TrainingActivity training)
  {
    this.training= training;
    ScalableMeasurementFragmentHelper.setup(training);
    screen_idx= 0;
    instance= this;
  }

  /**
   * 
   * @param training
   * @param resetup When set to true and an old manager exists the old state will be reinstalled
   */
  public ScreenManager(TrainingActivity training, boolean resetup)
  {
    this.training= training;
    ScalableMeasurementFragmentHelper.setup(training);
    if( resetup && instance != null )
      screen_idx= instance.screen_idx;
    instance= this;
  }

  public void setup(ITrainingController controller, Sport sport)
  {
    //TODO: rewrite and test this method.
    // The list does not make sense.
    // Moreover following gives empty array
    //  extra_screens=[], extra_screens_sport=[x] but disabled
    Screen[] extra_screens= controller.getAdditionalScreens();
    Screen[] extra_screens_sport= sport.getAdditionalScreens();
    ArrayList<Screen> list= new ArrayList<Screen>();
    Screen screen;
    
    ElapsedTimeFragment elapsed_time;
    elapsed_time= new ElapsedTimeFragment();
    MeasurementFragment time2= new WallClockFragment();
    MeasurementFragment hr_fragment= new HeartrateFragment();
    
    if( extra_screens.length + extra_screens_sport.length == 0 )
    {
      MeasurementFragment speed_fragment_kmh= new CurrentSpeedFragment();
      MeasurementFragment speed_fragment_minkm= new CurrentSpeedFragmentMinKm();
      
      screen= new ThreeSegments(training, speed_fragment_kmh, elapsed_time, hr_fragment);
      list.add(screen);
      screen= new ThreeSegments(training, speed_fragment_minkm, elapsed_time, hr_fragment);
      list.add(screen);
      screen= new ThreeSegments(training, speed_fragment_kmh, time2, hr_fragment);
      list.add(screen);
      screen= new ThreeSegments(training, speed_fragment_minkm, time2, hr_fragment);
      list.add(screen);
      
      this.screens= new Screen[list.size()];
      this.screens= list.toArray(this.screens);
    }
    else
    {
      if( controller.disableSportScreens() )
        this.screens= new Screen[list.size() + extra_screens.length];
      else
        this.screens= new Screen[list.size() + extra_screens.length + extra_screens_sport.length];
      this.screens= list.toArray(this.screens);
      int i= list.size();
      if( !controller.disableSportScreens() )
        for(Screen s : extra_screens_sport)
          this.screens[i++]= s;
      for(Screen s : extra_screens)
        this.screens[i++]= s;
    }
    
    this.screens[screen_idx].putOnUI(null);
  }

  /**
   * Rotate screen to the next in the list. Not to confuse with landscape/portrait rotation
   * @param dir
   */
  public void rotate(int dir)
  {
    assert(in_pause == false);
    
    Screen old= this.screens[screen_idx];
    if( screen_idx + dir < 0 )
      screen_idx= this.screens.length - 1;
    else if( screen_idx + dir >= this.screens.length )
      screen_idx= 0;
    else
      screen_idx+= dir;
      
    this.screens[screen_idx].putOnUI(old);
  }
  
  public void startUpdating()
  {
    screenupdater= new Timer();
    screenupdater.scheduleAtFixedRate(new Updater(), 0, updateRate);
  }
  
  public void stopUpdating()
  {
    screenupdater.cancel();
    screenupdater= null;
  }

  /**
   * Change the predefined update rate. If the screen is currently updating the
   * update will be stopped and restarted with the new values
   * 
   * @param updateRate
   *          delay[ms] between the updates
   */
  public void setUpdateRate(long updateRate)
  {
    this.updateRate= updateRate;
    if( screenupdater != null )
    {
      stopUpdating();
      startUpdating();
    }
  }
  
  public static ScreenManager getManager()
  {
    return instance;
  }
  
  public void addScreen(Screen screen)
  {
    if( Config.LOGGING ) Log.d(LOG_TAG, "addScreen new screen: " + screen);
    
    Screen[] newscreens= new Screen[screens.length];
    
    System.arraycopy(screens, 0, newscreens, 0, screens.length);
    newscreens[screens.length]= screen;
    
    this.screens= newscreens;
  }
  
  public void removeScreen(Screen screen)
  {
    if( Config.LOGGING ) Log.d(LOG_TAG, "removeScreen new screen: " + screen);
    
    int l= screens.length;
    
    Screen[] newscreens= new Screen[l-1];
    for(int i= 0, j= 0; i < l; i++)
    {
      if( screens[i] != screen )
        newscreens[j++]= screens[i];
    }
    
    this.screens= newscreens;
  }

  public static ScreenManager getInstance()
  {
    return instance;
  }

  public void putOn(Screen new_screen)
  {
    if( Config.LOGGING ) Log.d(LOG_TAG, "putOn new screen: " + new_screen + " old: " + screens[screen_idx] + " in_pause:" + in_pause);
    
    if( new_screen == screens[screen_idx] )
      return;
    
    int idx= 0;
    boolean found= false;
    for(Screen s : this.screens)
    {
      if( s == new_screen )
      {
        found= true;
        break;
      }
      idx++;
    }
    
    if( found )
    {
      putOn(idx);
    }
  }

  /**
   * Show a screen.
   * @param index
   */
  public void putOn(int index)
  {
    if( index > 0 && index < this.screens.length )
    {
      if( this.screen_idx == index )
        return;

      final Screen old= this.screens[screen_idx];
      screen_idx= index;

      // Only change the screen when we are not paused
      if( in_pause )
      {
        if( screen_to_be_removed == null )
        {
          this.screen_to_be_removed= old;
        }
      }
      else
      {
        this.training.runOnUiThread(new Runnable() {

          @Override
          public void run()
          {
            ScreenManager.this.screens[screen_idx].putOnUI(old);
          }
        });
      }
    }
  }

  public Screen getVisibleScreen()
  {
    return this.screens[screen_idx];
  }

  public int getScreenIndex()
  {
    return screen_idx;
  }

  public void setInPause(boolean in_pause)
  {
    // was the previous state paused and now un-paused?
    if( this.in_pause == false && in_pause == true )
    {
      // is there a transaction which we have to perform?
      if( screen_to_be_removed != null )
      {
        this.training.runOnUiThread(new Runnable() {
          
          @Override
          public void run()
          {
            ScreenManager.this.screens[screen_idx].putOnUI(screen_to_be_removed);
            screen_to_be_removed= null;
          }
        });
      }
    }
    
    this.in_pause= in_pause;
  }
}
