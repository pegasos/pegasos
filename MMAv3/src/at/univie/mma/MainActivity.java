package at.univie.mma;

import at.pegasos.client.util.*;
import org.acra.ACRA;

import android.app.*;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import at.pegasos.client.*;
import at.pegasos.client.ServerCallback;
import at.pegasos.client.activitystarter.*;
import at.pegasos.client.ui.*;
import at.pegasos.serverinterface.response.*;
import at.univie.mma.serverinterface.ServerInterface;
import at.univie.mma.serverinterface.response.ServerResponse;
import at.univie.mma.R;
import at.univie.mma.ui.*;
import at.univie.mma.ui.TimeoutUtil;

public class MainActivity extends PegasosClientView {
  private static final String LOG_TAG = MainActivity.class.getSimpleName();

  /**
   * Frames to be used in the main menu
   */
  final int[] frames = {R.id.frame1, R.id.frame2, R.id.frame3, R.id.frame4, R.id.frame5, R.id.frame6};
  /**
   * Transaction used during adding and removing frames of the menu
   */
  FragmentTransaction trans;
  ServerCallback callback = new ServerCallback(this);
  TimeoutUtil loginTimeout;
  private boolean loggedin = false;
  private ServerInterface server;
  private boolean autologin = false;
  private MenuManager menu;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getSupportActionBar().setTitle(Config.APP_NAME);

    PegasosClient.getInstance().setMainActivity(this);

    autologin = PreferenceManager.getInstance().getBoolean(Preferences.auto_login, false);

    server = ServerInterface.getInstance();
    server.addResponseCallback(callback);

    // connect to server
    reconnect();

    if( !loggedin && !autologin )
    {
      Intent i = new Intent(this, LoginActivity.class);
      startActivityForResult(i, LoginActivity.REQUEST_LOGIN);
    }

    menu = new MenuManagerAndroid(this);
    menu.createMainMenu();
  }

  @Override
  protected void onResume()
  {
    super.onResume();

    PegasosClient.getInstance().setMainActivity(this);

    server.addResponseCallback(callback);
  }

  @Override
  protected void onStop()
  {
    PegasosClient.getInstance().setMainActivity(null);
    super.onStop();
  }

  @Override
  protected void onDestroy()
  {
    PegasosClient.getInstance().setMainActivity(null);
    super.onDestroy();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    if( !Config.USE_SENSORMANAGER )
    {
      menu.removeItem(R.id.action_manage_sensors);
    }
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if( id == R.id.action_settings )
    {
      Intent i = new Intent(this, SettingsActivity.class);
      startActivity(i);

      return true;
    }
    else if( id == R.id.action_logout )
    {
      // remove login info

      // SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
      // TODO: why are we not remvoing username and password as well?
      //editor.remove("password");
      //editor.remove("username");
      PreferenceManager.getInstance().remove(Preferences.auto_login);
      // editor.remove(Preferences.auto_login);
      // editor.commit();

      if( server.isConnected() )
      {
        PegasosClient.getInstance().closeConnection();
      }

      autologin = false;

      // same as in onCreate. Important to get new reference to SI as connectToServer changes it
      ((PegasosClient) getApplication()).connectToServer();
      server = ServerInterface.getInstance();
      server.addResponseCallback(callback);

      // connect to server
      reconnect();

      Intent i = new Intent(this, LoginActivity.class);
      startActivityForResult(i, LoginActivity.REQUEST_LOGIN);

      return true;
    }
    else if( id == R.id.action_quit )
    {
      quitApp();
    }
    else if( id == R.id.action_manage_sensors )
    {
      try
      {
        Intent i = new Intent(this, Class.forName("at.univie.mma.ui.sensormanager.SensorManagerActivity"));
        startActivity(i);
      }
      catch( ClassNotFoundException e )
      {
        e.printStackTrace();
        ACRA.getErrorReporter().handleException(e);
      }
    }

    if( Config.LOGGING ) Log.d("MainAcitivity", "" + id);
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    // Check which request it is that we're responding to
    if( requestCode == LoginActivity.REQUEST_LOGIN )
    {
      // Make sure the request was successful
      if( resultCode == RESULT_OK )
      {
        PegasosLog.d(LOG_TAG, "Returned from LoginActivity: successful");
      }
      else
      {
        if( Config.LOGGING ) Log.d(LOG_TAG, "LoginActivity did not return OK -> finish");
        finish();
      }
    }
  }

  public void setLoggedIn(boolean loggedin)
  {
    this.loggedin = loggedin;
  }

  public void onLoginMessageReceived(LoginMessage m)
  {
    if( loginTimeout != null )
      loginTimeout.abort();
    loginTimeout = null;

    if( m.getResultCode() == ServerResponse.RESULT_OK )
    {
      UserValues.getInstance().setFromLoginMessage(m);

      showToast(R.string.login_message_received);
    }
    else
    {
      loggedin = false;
      showToast(R.string.login_failed);
      if( m.getError().equals("token") || m.getError().equals("login_failed_pw") )
      {
        Intent i = new Intent(this, LoginActivity.class);
        startActivityForResult(i, LoginActivity.REQUEST_LOGIN);
      }
    }
  }

  private void onServerConnect(boolean connected)
  {
    showToast("onServerConnected");

    if( autologin )
    {
      loginTimeout = new TimeoutUtil(this, new TimeoutUtil.TimeoutListener() {

        @Override
        public void onTimeout(TimeoutUtil util)
        {
          if( Config.CAN_START_OFFLINE )
          {
            UserValues.getInstance().restoreFromLast();

            util.setShowDialog(false);
          }
          else if( !Config.LOGIN_TIMEOUT_SHOWDIALOG )
          {
            quitApp();
          }
        }

        @Override
        public void onTimeoutAcknowledged()
        {
          // We only reach this point when the dialog has been shown
          quitApp();
        }
      }, Config.LOGIN_TIMEOUT, Config.LOGIN_TIMEOUT_SHOWDIALOG, getString(R.string.logging_in_text), getString(R.string.please_wait),
              "Login timeout");
      loginTimeout.start();

      try
      {
        LoginData loginData = LoginData.createFromPreferences();
        if( loginData != null )
          Tasks.login(loginData);
        else
          LoginData.removePreferences();
      }
      catch( Exception e )
      {
        LoginData.removePreferences();
      }
    }
  }

  public void reconnect()
  {
    // TODO: check if reconnect necessary
    ConnectTask task = new ConnectTask();
    task.execute();
  }

  @Override
  public void onBackPressed()
  {
    if( !menu.onBackPress() )
      super.onBackPressed();
  }

  public void addStarter(int index, IActivityStarterFragment starter)
  {
    if( trans == null )
    {
      trans = getFragmentManager().beginTransaction();
    }

    trans.replace(frames[index], (Fragment) starter);
    findViewById(frames[index]).setVisibility(android.view.View.VISIBLE);
  }

  public void add()
  {
    trans.commit();
    // trans.addToBackStack(null);
  }

  public void clearFrames()
  {
    for(int i = 0; i < frames.length; i++)
    {
      findViewById(frames[i]).setVisibility(android.view.View.GONE);
    }
    trans = null;
  }

  private void quitApp()
  {
    quitApp(true);
  }

  private void quitApp(boolean brutal)
  {
    this.finish();
    if( brutal )
      System.exit(0);
  }

  public ServerCallback getResponseCallback()
  {
    return callback;
  }

  public class ConnectTask extends AsyncTask<Void, Void, Boolean> {
    @Override
    protected Boolean doInBackground(Void... params)
    {
      return server.connect();
    }

    @Override
    protected void onPostExecute(final Boolean result)
    {
      MainActivity.this.runOnUiThread(new Runnable() {
        @Override
        public void run()
        {
          onServerConnect(result);
        }
      });
    }
  }
}
