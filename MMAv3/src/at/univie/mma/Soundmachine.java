package at.univie.mma;

import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;

public class Soundmachine implements OnInitListener, OnPreparedListener {
  
  public Activity main= null;
  // private Handler send_Handler= null;
  private TextToSpeech mTts;
  private ToneGenerator snd;
  private boolean speech_output_available= false;
  private String pattern_target_under;
  private String pattern_target_over;
  
  private static Soundmachine instance;
  
  public static synchronized Soundmachine getInstance(Activity main)
  {
    if( instance != null )
    {
      if( instance.main != main )
      {
        instance.close(false);
        instance.main= main;
        instance.init();
      }
      
      return instance;
    }
    else
    {
      instance= new Soundmachine(main);
      return instance;
    }
  }
  
  protected Soundmachine(Activity main)
  {
    this.main= main;
    
    init();
  }
  
  private void init()
  {
    mTts= new TextToSpeech(this.main, this);
    snd= new ToneGenerator(ToneGenerator.TONE_DTMF_5, 100);
  }
  
  public void beep()
  {
    snd.startTone(ToneGenerator.TONE_PROP_BEEP);
  }
  
  @SuppressWarnings("deprecation")
  @SuppressLint("NewApi")
  public void say_it(String tx)
  {
    if( speech_output_available )
    {
      if( Build.VERSION.SDK_INT >= 21 )
        mTts.speak(tx, TextToSpeech.QUEUE_ADD, null, tx);
      else
        mTts.speak(tx, TextToSpeech.QUEUE_ADD, null);
    }
  }
  
  @SuppressWarnings("deprecation")
  @SuppressLint("NewApi")
  public void say_it_interrupt(String tx)
  {
    if( speech_output_available )
    {
      if( Build.VERSION.SDK_INT >= 21 )
        mTts.speak(tx, TextToSpeech.QUEUE_FLUSH, null, tx);
      else
        mTts.speak(tx, TextToSpeech.QUEUE_FLUSH, null);
    }
  }
  
  /**
   * Clear TTS queue but finish speaking which is happening right now (if any)
   */
  public void stop_speaking()
  {
    stop_speaking(false);
  }
  
  /**
   * Clear TTS queue. If interrupt is true it will abort any speaking. If false it will wait for the
   * current output to finish and then kill any other output. Implemented as a non-blocking call.
   *
   * @param interrupt
   */
  public void stop_speaking(boolean interrupt)
  {
    if( interrupt )
    {
      mTts.stop();
    }
    else
    {
      new Thread(){
        public void run()
        {
          blockingWaitForSpeaking();
          mTts.stop();
        }
      }.start();
    }
  }
  
  public static int isNumeric(String value)
  {
    try
    {
      int number= Integer.parseInt(value);
      return number;
    }
    catch( NumberFormatException e )
    {
      return -1;
    }
  }
  
  /*public void sound(String message, int say_it)
  {
    int x= 0;
    Message m= send_Handler.obtainMessage();
    
    if( (x= isNumeric(message)) > 0 )
    {
      m.arg1= x;
    }
    else
    {
      m.arg1= 0;
    }
    m.arg2= say_it;
    m.obj= message;
    send_Handler.sendMessage(m);
  }*/
  
  public void onPrepared(MediaPlayer player)
  {
    player.start();
  }
  
  @Override
  public void onInit(int status)
  {
    if( status == TextToSpeech.SUCCESS )
    {
      int result = mTts.setLanguage(new Locale(LocaleManager.getLanguage(this.main)));
      if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
      {
        Log.e("NO", "NO SPEAKER!");
      }
      else
      {
        speech_output_available = true;
      }
    }
    else
    {
      Log.e("NO", "No SPEAKER");
    }
  }
  
  public void close(boolean finish_speaking)
  {
    if( finish_speaking )
    {
      blockingWaitForSpeaking();
    }
    
    mTts.shutdown();
    snd.release();
  }
  
  public void close()
  {
    this.close(false);
  }
  
  public void blockingWaitForSpeaking()
  {
    while( mTts.isSpeaking() )
    {
      try
      {
        Thread.sleep(500);
      }
      catch( InterruptedException e )
      {
        e.printStackTrace();
      }
    }
  }
  
  public void signal_target_under()
  {
    // TODO Auto-generated method stub
    say_it("Mehr Power");
  }
  
  public void signal_target_over()
  {
    // TODO Auto-generated method stub
    say_it("Weniger Power");
  }
  
  /**
   * Signal that a target needs to be increased by `deviation`. Disclaimer: This
   * is currently a workaround method.
   * 
   * @param deviation
   */
  public void signal_target_under(double deviation)
  {
    say_it(String.format(pattern_target_under, deviation));
  }
  
  /**
   * Signal that a target needs to be decreased by `deviation`
   * 
   * @param deviation
   */
  public void signal_target_over(double deviation)
  {
    say_it(String.format(pattern_target_over, deviation));
  }
  
  public void setPatterns(String pattern_under, String pattern_over)
  {
    this.pattern_target_under= pattern_under;
    this.pattern_target_over= pattern_over;
  }
}
