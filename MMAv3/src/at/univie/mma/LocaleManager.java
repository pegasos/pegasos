package at.univie.mma;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import at.pegasos.client.*;
import at.univie.mma.R;

import java.util.Locale;

public class LocaleManager {
  
  public static Context setLocale(Context c)
  {
    return updateResources(c, getLanguage(c));
  }
  
  public static Context setNewLocale(Context c, String language)
  {
    persistLanguage(c, language);
    return updateResources(c, language);
  }
  
  /**
   * Check whether the setting is overwriting the default language ie whether the app has specified
   * a language.
   * 
   * @return true if the language has been set. False otherwise
   */
  public static boolean isLanguageFixed(Context c)
  {
    return !PreferenceManager.getDefaultSharedPreferences(c).getString(Preferences.pref_key_app_language, "").equals("");
  }
  
  /**
   * Check whether the setting is overwriting the default language ie whether the app has specified
   * a language.
   * 
   * @return true if the language has been set. False otherwise
   */
  public static boolean isLanguageFixed(SharedPreferences prefs)
  {
    return !prefs.getString(Preferences.pref_key_app_language, "").equals("");
  }
  
  @SuppressWarnings("deprecation")
  @SuppressLint("NewApi")
  public static String getLanguage(Context c)
  {
    SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(c);
    String ret= prefs.getString(Preferences.pref_key_app_language, c.getResources().getString(R.string.settings_default_language));
    if( ret.equals("") )
    {
      Locale locale;
      if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N )
        locale= Resources.getSystem().getConfiguration().getLocales().get(0);
      else
        locale= Resources.getSystem().getConfiguration().locale;
      return locale.getLanguage();
    }
    else
      return ret;
  }
  
  private static void persistLanguage(Context c, String language)
  {
    SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(c);
    // use commit() instead of apply(), because sometimes we kill the application process
    // immediately which will prevent apply() to finish
    if( language.equals("") )
      prefs.edit().remove(Preferences.pref_key_app_language).commit();
    else
      prefs.edit().putString(Preferences.pref_key_app_language, language).commit();
  }
  
  @SuppressLint("NewApi")
  @SuppressWarnings("deprecation")
  private static Context updateResources(Context context, String language)
  {
    Locale locale= new Locale(language);
    Locale.setDefault(locale);
    
    Resources res= context.getResources();
    Configuration config= new Configuration(res.getConfiguration());
    if( Build.VERSION.SDK_INT >= 17 )
    {
      config.setLocale(locale);
      context= context.createConfigurationContext(config);
    }
    else
    {
      config.locale= locale;
      res.updateConfiguration(config, res.getDisplayMetrics());
    }
    return context;
  }
  
  @SuppressLint("NewApi")
  @SuppressWarnings("deprecation")
  public static Locale getLocale(Resources res)
  {
    Configuration config= res.getConfiguration();
    return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
  }
}