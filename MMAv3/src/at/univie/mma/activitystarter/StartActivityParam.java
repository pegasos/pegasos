package at.univie.mma.activitystarter;

import android.content.Context;
import android.content.Intent;
import at.univie.mma.ui.ActivityStarterFragment;
import at.univie.mma.ui.TrainingLandingScreen;

public class StartActivityParam implements ActivityStarterFragment.ActivityStarterCallback {
  private Context ctx;
  private int activity_c;
  private String sport;
  private Runnable command;
  private String param;

  public StartActivityParam(final Context ctx, final int activity_c, final String sport, final int param)
  {
    this.ctx= ctx;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= null;
    this.param= "" + param;
  }

  public StartActivityParam(final Context ctx, final int activity_c, final String sport, final String param)
  {
    this.ctx= ctx;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= null;
    this.param= param;
  }

  public StartActivityParam(final Context ctx, final int activity_c, final String sport, final Runnable additionalCommand,
      final int param)
  {
    this.ctx= ctx;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= additionalCommand;
    this.param= "" + param;
  }
  
  public void onActivityClicked(ActivityStarterFragment frament)
  {
    Intent i= new Intent(ctx, TrainingLandingScreen.class);
    
    i.putExtra(TrainingLandingScreen.EXTRA_ACTIVITY, activity_c);
    i.putExtra(TrainingLandingScreen.EXTRA_SPORT, sport);
    i.putExtra(TrainingLandingScreen.EXTRA_ARGUMENTS, param);
    
    if( command != null )
    {
      command.run();
    }
    
    ActivityStarterFragment.setParameters(frament, i);
    
    ctx.startActivity(i);
  }
}
