package at.univie.mma.controller.interval;

import at.univie.mma.ui.widget.RemainingTimeFragment;
import at.pegasos.client.values.Value;

public class TargetIntervalTimer extends Interval {
  public static final int TYPE_INT = 1;
  public static final int TYPE_DOUBLE = 2;
  
  final boolean val_type_int;
  
  Value val;
  Double dmin;
  Double dmax;
  Double dtarget;
  Integer imin;
  Integer imax;
  Integer itarget;
  Value target;
 
  private String timer;
  
  public TargetIntervalTimer(int val_type, String timer)
  {
    this.val_type_int = val_type == TYPE_INT ? true : false;

    this.timer= timer;
  }
  
  public void setValue(Value val, Double min, Double max, Double target)
  {
    this.val= val;
    dmin= min;
    dmax= max;
    dtarget= target;
  }
  
  public void setValue(Value val, Integer min, Integer max, Integer target)
  {
    this.val= val;
    imin= min;
    imax= max;
    itarget= target;
  }

  public void setValue(Value val, Double min, Double max, Double target, Value targetvalue)
  {
    this.val= val;
    dmin= min;
    dmax= max;
    dtarget= target;
    this.target= targetvalue;
  }

  public void setValue(Value val, Integer min, Integer max, Integer target, Value targetvalue)
  {
    this.val= val;
    imin= min;
    imax= max;
    itarget= target;
    this.target= targetvalue;
  }
  
  @Override
  public void onStart()
  {
    RemainingTimeFragment.StartCountdown(timer, duration_ms);
    if( val_type_int )
    {
      target.setIntValue(itarget);
    }
    else
    {
      target.setDoubleValue(dtarget);
    }
  }
  
  @Override
  public boolean cycle()
  {
    return true;
  }
  
  public double getTarget()
  {
    if( val_type_int )
      return itarget;
    else
      return dtarget;
  }
}
