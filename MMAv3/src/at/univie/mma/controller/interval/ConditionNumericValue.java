package at.univie.mma.controller.interval;

import at.pegasos.client.values.Value;
import at.pegasos.client.values.ValueStore;

/**
 * Check a numeric threshold
 */
public class ConditionNumericValue extends Condition {
  private final Value val;
  private final Value threshold;
  
  public ConditionNumericValue(Type type, Value val, Value threshold)
  {
    super(type);
    this.val= val;
    this.threshold= threshold;
  }
  
  public ConditionNumericValue(Type type, String val, String threshold)
  {
    super(type);
    this.val = ValueStore.getInstance().getCreateNumberValue(val);
    this.threshold = ValueStore.getInstance().getCreateNumberValue(threshold);
  }
  
  public boolean check()
  {
    switch( type )
    {
      case LESS:
        if( val.getDoubleValue() < threshold.getDoubleValue() )
        {
          fired= true;
          return true;
        }
        break;
      case GREATER:
        if( val.getDoubleValue() > threshold.getDoubleValue() )
        {
          fired= true;
          return true;
        }
    }
    return  false;
  }
  
  public String toString()
  {
    return "ConditionNumericValue: " + val.getName() + "/" + val.getDoubleValue() + " " + type + " " + threshold.getName() + "/" + threshold.getDoubleValue() + " " + fired;
  }
}
