package at.univie.mma.controller.interval;

import at.pegasos.client.values.Value;

public class Interval {
  public static final int TYPE_SINGLE= 1;
  public static final int TYPE_LOOP= 2;
  
  public int duration_ms;
  public Interval set;
  public Interval next;
  public Value param;
  public String txt;
  
  public int type;
  public IntervalCondition loop_set;
  
  /**
   * This method is called when the interval comes up. It is invoked after the onNext-Callback
   */
  public void onStart()
  {
    // Do nothing
  }
  
  /**
   * 
   * @return true if interval should continue, false if next interval should be done
   */
  public boolean cycle()
  {
    return true;
  }
  
  public boolean end()
  {
    return false;
  }
  
}
