package at.univie.mma.controller.interval;

public abstract class Condition {
  protected final Type type;
  protected boolean fired;
  
  /**
   * Enum for specifying when the threshold was reached. e.g. less is reached when the actual value
   * is less then the threshold
   */
  public enum Type {
    LESS, GREATER
  }
  
  public Condition(Type type)
  {
    this.type= type;
  }
  
  /**
   * Check whether the threshold was reached
   * 
   * @return true if threshold was reached
   */
  public abstract boolean check();
  
  public void reset()
  {
    fired= false;
  }
  
  public boolean hasFired()
  {
    return fired;
  }
}
