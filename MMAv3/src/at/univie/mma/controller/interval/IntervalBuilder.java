package at.univie.mma.controller.interval;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import at.univie.mma.Soundmachine;
import at.univie.mma.VibratorService;
import at.pegasos.client.values.DoubleValue;
import at.pegasos.client.values.ValueStore;

/**
 * Mutable Builder class for building an Interval which can then be passed on to the IntervalController
 *
 * Mutable means that any action you apply to this builder will affect future calls to this object
 */
public class IntervalBuilder {
  private static final String TAG= IntervalBuilder.class.getCanonicalName();
  
  private Interval start;
  private Interval prev;
  private String timer;
  private VibratorService vibrator;
  private Soundmachine sound;
  
  private boolean pattern_set= false;
  
  private List<Condition> conditions;
  
  public Interval build()
  {
    return start;
  }
  
  /**
   * Sets the fragment for the countdowntimer for subsequent intervals for this builder. Any
   * subsequent call for adding an interval will use this timer. To unset pass null
   * 
   * @param timer name of the timer
   */
  public IntervalBuilder setRemainingTime(String timer)
  {
    this.timer= timer;
    return this;
  }
  
  /**
   * Sets the VibratorService for subsequent intervals for this builder. Any subsequent call for
   * adding an interval will use this VibratorService. To unset pass null or use <code>unsetVibrator()</code>
   *
   * @param vibrator vibrator which should be used
   */
  public IntervalBuilder setVibrator(VibratorService vibrator)
  {
    this.vibrator= vibrator;
    return this;
  }
  
  /**
   * Do not use a vibrator for subsequent actions
   */
  public IntervalBuilder unsetVibrator()
  {
    this.vibrator= null;
    return this;
  }
  
  /**
   * Sets the soundmachine for subsequent intervals for this builder. Any subsequent call for adding
   * an interval will use this soundmachine. To unset pass null
   * 
   * @param sound soundmachine to be used for this builder
   */
  public IntervalBuilder setSound(Soundmachine sound)
  {
    this.sound= sound;
    return this;
  }
  
  public IntervalBuilder setPatternsDeviation(String pattern_under, String pattern_over)
  {
    this.sound.setPatterns(pattern_under, pattern_over);
    this.pattern_set= true;
    
    return this;
  }
  
  public IntervalBuilder addCondition(final Condition.Type type, final String value_name, double threshold)
  {
    return this.addCondition(new ConditionNumeric(type, ValueStore.getInstance().getCreateNumberValue(value_name), threshold));
  }
  
  public IntervalBuilder addCondition(final Condition condition)
  {
    if( this.conditions == null )
    {
      this.conditions= new ArrayList<Condition>();
    }
    this.conditions.add(condition);
    
    return this;
  }
  
  /**
   * Tell builder to not use "reach cutoff" for future intervals
   */
  public void deleteConditions()
  {
    this.conditions= null;
  }
  
  /**
   * Waits duration_sec seconds. Wait is ended once the value defined by value_name has exceeded
   * value. When not using an IntegerValue this method will cause a crash it is recommended to use
   * addWaitReach instead
   * 
   * @param duration_sec maximal duration of this interval
   * @param value_name name of the variable which should be checked
   * @param value threshold value
   * @return builder
   */
  public IntervalBuilder addWaitReachInteger(double duration_sec, final String value_name, int value)
  {
    WaitReachInterval i = new WaitReachInterval(WaitReachInterval.TYPE_INT);
    i.type = Interval.TYPE_SINGLE;
    i.duration_ms = (int) duration_sec * 1000;
    i.setValue(ValueStore.getInstance().getCreateValueInt(value_name), value);
    
    addInterval(i);
    
    return this;
  }
  
  /**
   * Waits duration_sec seconds. Wait is ended once the value defined by value_name has exceeded
   * value. When not using a DoubleValue this method will cause a crash it is recommended to use
   * addWaitReach instead
   *
   * @param duration_sec maximal duration of this interval
   * @param value_name name of the variable which should be checked
   * @param value threshold value
   * @return builder
   */
  public IntervalBuilder addWaitReachDouble(double duration_sec, final String value_name, double value)
  {
    WaitReachInterval i= new WaitReachInterval(WaitReachInterval.TYPE_DOUBLE);
    i.type= Interval.TYPE_SINGLE;
    i.duration_ms= (int) duration_sec * 1000;
    i.setValue((DoubleValue) ValueStore.getInstance().getCreateValueDouble(value_name), value);
    
    addInterval(i);
    
    return this;
  }
  
  /**
   * Waits duration_sec seconds. Wait is ended once the value defined by value_name has exceeded
   * value
   *
   * @param duration_sec maximal duration of this interval
   * @param value_name name of the variable which should be checked
   * @param value threshold value
   * @return builder
   */
  public IntervalBuilder addWaitReach(final double duration_sec, final String value_name, final double value)
  {
    WaitReachInterval i= new WaitReachInterval(WaitReachInterval.TYPE_DOUBLE);
    i.type= Interval.TYPE_SINGLE;
    i.duration_ms= (int) duration_sec * 1000;
    i.setValue(ValueStore.getInstance().getCreateNumberValue(value_name), value);
    
    addInterval(i);
    
    return this;
  }
  
  /**
   * Add a Interval with a Target. The target is an integer
   * 
   * @param duration_sec
   *          duration of the interval [s]. Use 0,5 for 500ms
   * @param value_name
   *          name of the value which should be controlled
   * @param target_value_name
   *          name of the value containing the target value
   * @param target_value
   *          the value
   * @param range
   *          +/- tolerance
   * @return builder
   */
  public IntervalBuilder addTarget(double duration_sec, final String value_name, final String target_value_name, int target_value, int range)
  {
    return this.addTarget(TargetInterval.TYPE_INT, duration_sec, value_name, target_value, range, vibrator != null, this.sound != null,
        this.timer != null, pattern_set, target_value_name);
  }

  /**
   * Add a Interval with a Target. The target is an integer
   *
   * @param duration_sec
   *          duration of the interval [s]. Use 0,5 for 500ms
   * @param value_name
   *          name of the value which should be the target
   * @param target_value
   *          the value
   * @param range
   *          +/- tolerance
   * @return builder
   */
  public IntervalBuilder addTarget(double duration_sec, final String value_name, int target_value, int range)
  {
    return this.addTarget(TargetInterval.TYPE_INT, duration_sec, value_name, target_value, range, vibrator != null, this.sound != null,
            this.timer != null, pattern_set, null);
  }
  
  /**
   * Add a Interval with a Target. The target is a double
   * 
   * @param duration_sec
   *          duration of the interval [s]. Use 0,5 for 500ms
   * @param value_name
   *          name of the value which should be the target
   * @param target_value
   *          the value
   * @param range
   *          +/- tolerance
   * @return builder
   */
  public IntervalBuilder addTarget(double duration_sec, final String value_name, double target_value, double range)
  {
    return this.addTarget(TargetInterval.TYPE_DOUBLE, duration_sec, value_name, target_value, range, vibrator != null, this.sound != null,
        this.timer != null, pattern_set, null);
  }
  
  /**
   * Add a Interval with a Target.
   * 
   * @param duration_sec
   *          duration of the interval [s]. Use 0,5 for 500ms
   * @param value_name
   *          name of the value which should be the target
   * @param target_value
   *          the value
   * @param range
   *          +/- tolerance
   * @param vibrate
   *          if true signal_target_under or signal_target_over will be invoked every time the value
   *          is not within the specified range
   * @param sound
   *          if true signal_target_under or signal_target_over will be invoked every time the value
   *          is not within the specified range
   * @param display
   *          if true a count down will be displayed
   * @param target_value_name
   *          name of the value containing the target value
   */
  public IntervalBuilder addTarget(int type, double duration_sec, final String value_name, double target_value, double range,
      boolean vibrate, boolean sound, boolean display, boolean custom, String target_value_name)
  {
    Interval i= null;
    
    int choice= 0;
    if( vibrate )
      choice+= 1;
    if( sound )
      choice+= 10;
    if( display )
      choice+= 100;
    if( custom )
      choice+= 1000;
    if( conditions != null )
      choice+= 10000;
    
    Log.d(TAG, "Adding Target " + choice);
    
    switch( choice )
    {
      case 0:
        i= new TargetInterval(type);
        if( type == TargetInterval.TYPE_INT )
          ((TargetInterval) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name), (int) (target_value - range),
              (int) (target_value + range), (int) target_value);
        else
          ((TargetInterval) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name), target_value - range,
              target_value + range, target_value);
        break;
        
      /*
       * case 1: i= new TargetInterval(TargetInterval.TYPE_INT); ((TargetInterval)
       * i).setValue((IntegerValue) ValueStore.getInstance().getCreateValueInt(value_name),
       * target_value - range, target_value + range, target_value); break;
       */
      
      /*
       * case 10: i= new TargetInterval(TargetInterval.TYPE_INT); ((TargetInterval)
       * i).setValue((IntegerValue) ValueStore.getInstance().getCreateValueInt(value_name),
       * target_value - range, target_value + range, target_value); break;
       */
      
      /*
       * case 11: i= new TargetInterval(TargetInterval.TYPE_INT); ((TargetInterval)
       * i).setValue((IntegerValue) ValueStore.getInstance().getCreateValueInt(value_name),
       * target_value - range, target_value + range, target_value); break;
       */
      
      case 100:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        i= new TargetIntervalTimer(type, timer);
        if( target_value_name == null )
        {
          if( type == TargetInterval.TYPE_INT )
            ((TargetIntervalTimer) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name), (int) (target_value - range),
                (int) (target_value + range), (int) target_value);
          else
            ((TargetIntervalTimer) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name), target_value - range,
                target_value + range, target_value);
        }
        else
        {
          if( type == TargetInterval.TYPE_INT )
            ((TargetIntervalTimer) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name), (int) (target_value - range),
                    (int) (target_value + range), (int) target_value, ValueStore.getInstance().getCreateValueInt(target_value_name));
          else
            ((TargetIntervalTimer) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name), target_value - range,
                    target_value + range, target_value, ValueStore.getInstance().getCreateValueDouble(value_name));
        }
        break;

      case 101:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        if( vibrator == null )
          throw new IllegalStateException("Tried to use a vibrator without setting it first");
        i= new TargetIntervalTimerVibrator(type, vibrator, timer);
        if( type == TargetInterval.TYPE_INT )
          ((TargetIntervalTimerVibrator) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name),
              (int) (target_value - range), (int) (target_value + range), (int) target_value);
        else
          ((TargetIntervalTimerVibrator) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name), target_value - range,
              target_value + range, target_value);
        break;
        
      case 110:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        if( this.sound == null )
          throw new IllegalStateException("Tried to use sound without setting it first");
        i= new TargetIntervalTimerSound(type, this.sound, timer);
        if( type == TargetInterval.TYPE_INT )
          ((TargetIntervalTimerSound) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name), (int) (target_value - range),
              (int) (target_value + range), (int) target_value);
        else
          ((TargetIntervalTimerSound) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name), target_value - range,
              target_value + range, target_value);
        break;
        
      case 111:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        if( vibrator == null )
          throw new IllegalStateException("Tried to use a vibrator without setting it first");
        if( this.sound == null )
          throw new IllegalStateException("Tried to use sound without setting it first");
        i= new TargetIntervalTimerVibratorSound(type, this.sound, vibrator, timer);
        if( type == TargetInterval.TYPE_INT )
          ((TargetIntervalTimerVibratorSound) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name),
              (int) (target_value - range), (int) (target_value + range), (int) target_value);
        else
          ((TargetIntervalTimerVibratorSound) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name),
              target_value - range, target_value + range, target_value);
        break;
        
      case 1111:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        if( vibrator == null )
          throw new IllegalStateException("Tried to use a vibrator without setting it first");
        if( this.sound == null )
          throw new IllegalStateException("Tried to use sound without setting it first");
        i= new TargetIntervalTimerVibratorSoundCustom(type, this.sound, vibrator, timer);
        if( type == TargetInterval.TYPE_INT )
          ((TargetIntervalTimerVibratorSoundCustom) i).setValue(ValueStore.getInstance().getCreateValueInt(value_name),
              (int) (target_value - range), (int) (target_value + range), (int) target_value);
        else
          ((TargetIntervalTimerVibratorSoundCustom) i).setValue(ValueStore.getInstance().getCreateValueDouble(value_name),
              target_value - range, target_value + range, target_value);
        break;
  
      case 11111:
        if( timer == null )
          throw new IllegalStateException("Tried to use a timer without setting it first");
        if( vibrator == null )
          throw new IllegalStateException("Tried to use a vibrator without setting it first");
        if( this.sound == null )
          throw new IllegalStateException("Tried to use sound without setting it first");
        if( this.conditions == null )
          throw new IllegalStateException("Tried to use reach cutoff without setting it first");
        
        i= new TargetReachIntervalTimerVibratorSoundCustom(this.sound, vibrator, timer, conditions.toArray(new Condition[0]));
        ((TargetReachIntervalTimerVibratorSoundCustom) i).setValue(ValueStore.getInstance().getCreateNumberValue(value_name),
                  target_value - range, target_value + range, target_value);
        break;
  
      case 10000:
      case 10001:
      case 10010:
      case 10011:
      case 10100:
      case 10101:
      case 10111:
      case 11000:
      case 11001:
      case 11010:
      case 11011:
      case 11100:
      case 11101:
      case 11110:
        i= new TargetReachIntervalTimerVibratorSoundCustom(this.sound, vibrator, timer, conditions.toArray(new Condition[0]));
        ((TargetReachIntervalTimerVibratorSoundCustom) i).setValue(ValueStore.getInstance().getCreateNumberValue(value_name),
                target_value - range, target_value + range, target_value);
        break;
        
      default:
        throw new IllegalStateException("The option you have specified is not implemented yet"); // TODO:
                                                                                                 // implement
                                                                                                 // all
                                                                                                 // combinations
    }
    
    i.type= Interval.TYPE_SINGLE;
    i.duration_ms= (int) duration_sec * 1000;
    
    addInterval(i);
    
    return this;
  }
  
  /**
   * Just waits for the given time.
   *
   * @param duration_sec
   *          duration of the interval [s]. Use 0,5 for 500ms
   * @return builder
   */
  public IntervalBuilder addFree(double duration_sec)
  {
    WaitInterval i= new WaitInterval();
    if( timer != null )
      i.setTimer(timer);
    i.type= Interval.TYPE_SINGLE;
    i.duration_ms= (int) duration_sec * 1000;
    
    addInterval(i);
    
    return this;
  }
  
  public IntervalBuilder addLoop(int times, Interval set)
  {
    // make start of loop and start storing set
    Interval i= new Interval();
    i.type= Interval.TYPE_LOOP;
    i.loop_set= new CounterCondition(times);
    i.set= set;
    
    addInterval(i);
    
    return this;
  }
  
  private void addInterval(Interval i)
  {
    if( start == null )
      start= i;
      
    if( prev != null )
      prev.next= i;
      
    prev= i;
  }
  
  public void clearIntervals()
  {
    this.start= null;
    this.prev= null;
  }
}
