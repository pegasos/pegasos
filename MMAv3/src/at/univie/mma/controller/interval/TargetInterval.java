package at.univie.mma.controller.interval;

import at.pegasos.client.values.Value;

public class TargetInterval extends Interval {
  public static final int TYPE_INT= 1;
  public static final int TYPE_DOUBLE= 2;
  
  final boolean val_type_int;
  
  boolean reached;
  
  Value val;
  Double dmin;
  Double dmax;
  Double dtarget;
  Integer imin;
  Integer imax;
  Integer itarget;
  
  int every_cur;
  
  public TargetInterval(int val_type)
  {
    this.val_type_int= val_type == TYPE_INT ? true : false;
  }
  
  public void setValue(Value val, Double min, Double max, Double target)
  {
    this.val= val;
    dmin= min;
    dmax= max;
    dtarget= target;
  }
  
  public void setValue(Value val, Integer min, Integer max, Integer target)
  {
    this.val= val;
    imin= min;
    imax= max;
    itarget= target;
  }
  
  @Override
  public boolean cycle()
  {
    return true;
  }
  
  public double getTarget()
  {
    if( val_type_int )
      return itarget;
    else
      return dtarget;
  }
}
