package at.univie.mma.controller.interval;

import at.univie.mma.VibratorService;
import at.univie.mma.Soundmachine;
import at.univie.mma.ui.widget.RemainingTimeFragment;
import at.pegasos.client.values.Value;

public class TargetIntervalTimerVibratorSound extends Interval {
  public static final int TYPE_INT = 1;
  public static final int TYPE_DOUBLE = 2;
  
  final boolean val_type_int;
  final int every;
  
  boolean reached;
  
  Value val;
  Double dmin;
  Double dmax;
  Double dtarget;
  Integer imin;
  Integer imax;
  Integer itarget;
  
  int every_cur;
  private final String timer;
  private final Soundmachine sound;
  private final VibratorService vibrator;
  
  public TargetIntervalTimerVibratorSound(int val_type, Soundmachine sound, VibratorService vibrator, String timer)
  {
    this.val_type_int = val_type == TYPE_INT ? true : false;
    
    // only check every 5 seconds whether target is ok
    this.every= (int) (5000 / IntervalController.INTERVAL_CYCLE);
    this.every_cur= this.every;
    
    this.timer= timer;
    this.sound= sound;
    this.vibrator= vibrator;
  }
  
  public void setValue(Value val, Double min, Double max, Double target)
  {
    this.val= val;
    dmin= min;
    dmax= max;
    dtarget= target;
  }
  
  public void setValue(Value val, Integer min, Integer max, Integer target)
  {
    this.val= val;
    imin= min;
    imax= max;
    itarget= target;
  }
  
  @Override
  public void onStart()
  {
    RemainingTimeFragment.StartCountdown(timer, duration_ms);
  }
  
  @Override
  public boolean cycle()
  {
    every_cur--;
    if( every_cur == 0 )
    {
      every_cur= every;
      if( val_type_int )
      {
        if( val.getIntValue() < imin ) 
        {
          sound.signal_target_under();
          vibrator.signal_target_under();
        }
        else if( val.getIntValue() > imax )
        {
          sound.signal_target_over();
          vibrator.signal_target_over();
        }
      }
      else
      {
        if( val.getDoubleValue() < dmin ) 
        {
          sound.signal_target_under();
          vibrator.signal_target_under();
        }
        else if( val.getDoubleValue() > dmax )
        {
          sound.signal_target_over();
          vibrator.signal_target_over();
        }
      }
    }
    
    return true;
  }
  
  public double getTarget()
  {
    if( val_type_int )
      return itarget;
    else
      return dtarget;
  }
}