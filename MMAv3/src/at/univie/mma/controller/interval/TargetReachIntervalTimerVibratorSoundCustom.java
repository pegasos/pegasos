package at.univie.mma.controller.interval;

import java.util.Arrays;

import at.univie.mma.Soundmachine;
import at.univie.mma.VibratorService;
import at.univie.mma.ui.widget.RemainingTimeFragment;
import at.pegasos.client.values.Value;

public class TargetReachIntervalTimerVibratorSoundCustom extends Interval {
  final int every;
  
  /**
   * Thresholds to be checked every cycle
   */
  private final Condition[] conditions;
  private final int nt;
  
  private Value val_target;
  private Double dmin;
  private Double dmax;
  private Double dtarget;
  
  private int every_cur;
  
  private final String timer;
  private final Soundmachine sound;
  private final VibratorService vibrator;
  
  public TargetReachIntervalTimerVibratorSoundCustom(Soundmachine sound, VibratorService vibrator, String timer,
      final Condition[] conditions)
  {
    // only check every 5 seconds whether target is ok
    this.every= (int) (5000 / IntervalController.INTERVAL_CYCLE);
    this.every_cur= this.every;
    
    this.timer= timer;
    this.sound= sound;
    this.vibrator= vibrator;
    
    this.conditions= conditions;
    nt= this.conditions.length;
  }
  
  public void setValue(Value val_target, Double min, Double max, Double target)
  {
    this.val_target= val_target;
    dmin= min;
    dmax= max;
    dtarget= target;
  }
  
  @Override
  public void onStart()
  {
    // sound.say_it(String.format("Start %.1f %.1f %.1f", dtarget, dmin, dmax));
    RemainingTimeFragment.StartCountdown(timer, duration_ms);
  }
  
  @Override
  public boolean cycle()
  {
    every_cur--;
    if( every_cur == 0 )
    {
      every_cur= every;
      if( val_target.getDoubleValue() < dmin )
      {
        if( sound != null )
          sound.signal_target_under(dtarget - val_target.getDoubleValue());
        if( vibrator != null )
          vibrator.signal_target_under();
      }
      else if( val_target.getDoubleValue() > dmax )
      {
        if( sound != null )
          sound.signal_target_over(val_target.getDoubleValue() - dtarget);
        if( vibrator != null )
          vibrator.signal_target_over();
      }
    }
    
    for(int i= 0; i < nt; i++)
    {
      if( conditions[i].check() )
      {
        fired= conditions[i];
        // Log.d("TRI", "end " + conditions[i]);
        return false;
      }
    }
    return true;
  }
  
  public double getTarget()
  {
    return dtarget;
  }
  
  private Condition fired;
  
  public Condition getFired()
  {
    return fired;
  }
  
  @Override
  public String toString()
  {
    return "TRI [t: " + dtarget + " " + dmin + "-" + dmax + " <" + Arrays.toString(conditions) + ">]";
  }
}
