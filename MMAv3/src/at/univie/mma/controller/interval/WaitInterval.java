package at.univie.mma.controller.interval;

import at.univie.mma.ui.widget.RemainingTimeFragment;

public class WaitInterval extends Interval {
  boolean slept_before;
  private String timer;
  
  public WaitInterval()
  {
    
  }
  
  @Override
  public void onStart()
  {
    if( timer != null )
    {
      RemainingTimeFragment.StartCountdown(timer, duration_ms);
    }
  }
  
  @Override
  public boolean cycle()
  {
    if( !slept_before && duration_ms > 1000 )
    {
      try
      {
        Thread.sleep(duration_ms - 1000);
        slept_before= true;
      }
      catch (InterruptedException e)
      {
        e.printStackTrace();
      }
    }
    
    return true;
  }
  
  public void setTimer(String timer)
  {
    this.timer=timer;
  }
}
