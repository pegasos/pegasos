package at.univie.mma.controller.interval;

import at.pegasos.client.values.NumberValue;

/**
 * Check a numeric threshold
 */
public class ConditionNumeric extends Condition {
  private final NumberValue val;
  private final double threshold;
  
  public ConditionNumeric(Type type, NumberValue val, double threshold)
  {
    super(type);
    this.val= val;
    this.threshold= threshold;
  }
  
  public boolean check()
  {
    // Log.d("ConditionNumeric", type + " " + val.getDoubleValue() + " " + threshold);
    switch( type )
    {
      case LESS:
        if( val.getDoubleValue() < threshold )
        {
          fired= true;
          return true;
        }
        break;
      case GREATER:
        if( val.getDoubleValue() > threshold )
        {
          fired= true;
          return true;
        }
    }
    return  false;
  }
  
  public String toString()
  {
    return "ConditionNumeric: " + val.getName() + " " + type + " " + threshold;
  }
}
