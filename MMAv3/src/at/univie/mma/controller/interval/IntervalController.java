package at.univie.mma.controller.interval;

import android.util.Log;

import at.univie.mma.Soundmachine;
import at.pegasos.client.ui.TrainingActivity;

public class IntervalController {
  public static final String TAG = IntervalController.class.getCanonicalName();
  
  static final long INTERVAL_CYCLE = 1000;

  // private TrainingActivity training;
  Soundmachine sound;
  
  Interval start;

  private Runnable onnext;
  private Runnable onfinish;

  private Interval running_int;
  
  private IntChecker c;
  
  public IntervalController(TrainingActivity training)
  {
    // this.training= training;
    this.sound= training.getSoundmachine();
  }
  
  public IntervalController(TrainingActivity training, Interval start)
  {
    this.sound= training.getSoundmachine();
    
    this.start= start;
    this.onnext= null;
  }
  
  public IntervalController(TrainingActivity training, Interval start, Runnable onnext)
  {
    // this.training= training;
    this.sound= training.getSoundmachine();
    
    this.start= start;
    
    this.onnext= onnext;
  }
  
  public void setStart(Interval start)
  {
    this.start= start;
  }
  
  public void start()
  {
    c= new IntChecker(start);
    c.start();
  }
  
  public void stop()
  {
    if( c != null )
      c.end();    
  }
  
  public void setOnNext(Runnable onnext)
  {
    this.onnext= onnext;
  }
  
  public void setOnFinish(Runnable runnable)
  {
    this.onfinish= runnable;
  }
  
  public Interval getRunning()
  {
    return this.running_int;
  }
  
  public void debug()
  {
    Log.d(TAG, this + " " + itos(start));
  }
  
  private String itosL(Interval set, IntervalCondition loop_set)
  {
    return " Loop(Condition: " + loop_set + ", " + itos(set) + ")";
  }
  
  private String itos(Interval i)
  {
    String ret= i.toString();
    
    if(i.type == Interval.TYPE_LOOP )
    {
      ret+= itosL(i.set, i.loop_set);
    }
    
    if( i.next != null )
    {
      ret+= itos(i.next);
    }
    
    return  ret;
  }
  
  private class IntChecker extends Thread {
    Interval start;
    private long tcur;
    private long tend;
    private volatile boolean run;
    
    public IntChecker(Interval start)
    {
      super("IntChecker");
      this.start= start;
    }
    
    public void end()
    {
      run= false;
      this.interrupt();
    }
    
    public void runIntervall(Interval i)
    {
      Log.d(TAG, "Start: text:" + (i.txt != null ? i.txt : "(null)") + " " + i + " " + i.next);
      if( i.txt != null )
        sound.say_it(i.txt);
      
      IntervalController.this.running_int= i;
      if( onnext != null ) onnext.run();
      i.onStart();
      
      if( run && i.type == Interval.TYPE_LOOP )
      {
        runIntervallLoop(i.set, i.loop_set);
      }
      else
      {
        tcur= System.currentTimeMillis();
        tend= tcur + i.duration_ms;
        
        while( run && tcur < tend )
        {
          try
          {
            Thread.sleep(INTERVAL_CYCLE);
            
            if( !i.cycle() )
              break;
          }
          catch (InterruptedException e)
          {
            /*e.printStackTrace();
            Log.e(TAG, e.getMessage());*/
          }
          tcur= System.currentTimeMillis();
        }
      }
      
      if( i.next != null && run )
      {
        // if( !i.end() ) TODO: make this work. An interval could have the option to kill the whole session. 
        {
          runIntervall(i.next);
        }
      }
    }
    
    public void runIntervallLoop(Interval i, IntervalCondition condition)
    {
      condition.init();
      while( run && condition.check() )
      {
        // Log.d(TAG, "Loop");
        runIntervall(i);
        condition.update();
      }
      // Log.d(TAG, "Loop end");
    }
    
    @Override
    public void run()
    {
      Log.d(TAG, "Starting interval");
      
      run= true;
      runIntervall(start);
      
      Log.d(TAG, "Interval finished");
      
      if( onfinish != null )
        onfinish.run();
    }
    
    /*
    public void run()
    {
      Intervall parent= null;
      Intervall cur= start;
      
      long tcur, tend;
      
      while(true)
      {
        // TODO: momentan nur einfach geschachtelte Intervalle moeglich
        if( cur.type == Intervall.TYPE_LOOP )
        {
          parent= cur;
          cur= parent.set;
        }
        
        Log.d(TAG, "Start: " + (cur.txt != null ? cur.txt : "(null)"));
        if( cur.txt != null )
          sound.say_it(cur.txt);
        
        tcur= System.currentTimeMillis();
        tend= tcur + cur.duration_ms;
        
        while(tcur < tend )
        {
          try
          {
            Thread.sleep(INTERVAL_CYCLE);
            
            if( !cur.cycle() )
              break;
          }
          catch (InterruptedException e)
          {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
          }
          tcur= System.currentTimeMillis();
        }
        
        if( cur.next == null )
        {
          break;
        }
        
        if( cur.end() )
          break;
        cur= cur.next;
      }
      
      Log.d(TAG, "Interval finished");
      
      if( onfinish != null )
        onfinish.run();
    }*/
  }
}
