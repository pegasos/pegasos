package at.univie.mma.controller.interval;



public class CounterCondition extends IntervalCondition {
  private int number;
  private int counter;
  
  public CounterCondition(int number)
  {
    this.number= number;
  }
  
  @Override
  public void init()
  {
    counter= 0;
  }
  
  @Override
  public boolean check()
  {
    return counter < number;
  }
  
  @Override
  public void update()
  {
    counter++;
  }
}