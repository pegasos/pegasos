package at.univie.mma.controller.interval;

import at.pegasos.client.values.Value;

public class WaitReachInterval extends Interval {
  public static final int TYPE_INT= 1;
  public static final int TYPE_DOUBLE= 2;
  
  final boolean val_type_int;
  boolean reached;
  
  Value val;
  Double dmin;
  Integer imin;
  
  public WaitReachInterval(int val_type)
  {
    this.val_type_int= val_type == TYPE_INT ? true : false;
  }
  
  public void setValue(Value val, Double min)
  {
    this.val= val;
    dmin= min;
  }
  
  public void setValue(Value val, Integer min)
  {
    this.val= val;
    imin= min;
  }
  
  @Override
  public boolean cycle()
  {
    if( val_type_int )
    {
      if( val.getIntValue() > imin )
      {
        reached= true;
        return false;
      }
      return true;
    }
    else
    {
      if( val.getDoubleValue() > dmin )
      {
        reached= true;
        return false;
      }
      return true;
    }
  }
}