package at.univie.mma.controller.interval;

public class IntervalCondition {

  /**
   * This method is called before the actual interval starts. (Can be used for resetting/initialising a counter). 
   */
  public void init()
  {
  }

  /**
   * This method is called before starting the intervals in the set.
   * @return true when set should be started
   */
  public boolean check()
  {
    return false;
  }

  /**
   * After an iteration of the intervals in the set, this method is called.  
   */
  public void update()
  {
  }
}