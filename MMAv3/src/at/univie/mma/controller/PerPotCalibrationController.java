package at.univie.mma.controller;

import at.pegasos.client.controller.BaseTrainingController;
import at.univie.mma.Soundmachine;
import at.univie.mma.serverinterface.Activities;
import at.pegasos.serverinterface.response.ServerResponse;
import at.pegasos.client.ui.screen.Screen;
import at.pegasos.client.ui.TrainingActivity;

public class PerPotCalibrationController extends BaseTrainingController
{
  Soundmachine soundmachine;
  
  @Override
  public void initUI(TrainingActivity activity)
  {
    soundmachine= activity.getSoundmachine();
  }
  
  @Override
  public void setActivity(int activity_c, String argument)
  {
    this.activity= activity_c;
    this.param2= Integer.parseInt(argument);;
  }
  
  @Override
  public Screen[] getAdditionalScreens()
  {
    return no_screens;
  }
  
  @Override
  public void onSessionStarted(boolean offline)
  {
    //TODO: decide about what to do when started offline
    switch( controller_id )
    {
      case Activities.RUNNING_PERPOT_EASY_C:
        soundmachine.say_it("Lauf wird automatisch bei Erreichen von 6km/h gestartet. Steigerung nach jeweils 3 Minuten um 1km/h.");
        break;
      case Activities.RUNNING_PERPOT_MEDIUM_C:
        soundmachine.say_it("Lauf wird automatisch bei Erreichen von 7km/h gestartet. Steigerung nach jeweils 3 Minuten um 1.5km/h.");
        break;
      case Activities.RUNNING_PERPOT_HARD_C:
        soundmachine.say_it("Lauf wird automatisch bei Erreichen von 8km/h gestartet. Steigerung nach jeweils 3 Minuten um 2km/h.");
        break;
    }
  }

  @Override
  public boolean onMessage(ServerResponse r)
  {
    return false;
  }
  
  @Override
  public boolean disableSportScreens() {
    return false;
  }

  @Override
  public void onSessionStopped()
  {
    // Nothing to do here
  }
}
