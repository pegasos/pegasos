package at.univie.mma.controller;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import at.pegasos.client.controller.BaseTrainingController;
import at.pegasos.client.Preferences;
import at.pegasos.client.sensors.*;
import at.pegasos.serverinterface.response.ServerResponse;
import at.pegasos.client.ui.TrainingActivity;
import at.pegasos.client.ui.screen.Screen;
import at.pegasos.client.ui.screen.ThreeSegments;
import at.univie.mma.ui.widget.EmptyFragment;
import at.univie.mma.ui.widget.StopButton;
import at.pegasos.client.values.ValueStore;

public class FootpodCalibration extends BaseTrainingController {
  
  private TrainingActivity training;
  private Screen[] extra_screens;
  
  @Override
  public void initUI(TrainingActivity activity)
  {
    this.training= activity;
    
    ThreeSegments test_screen= new ThreeSegments(activity, new EmptyFragment(), new StopButton(), new EmptyFragment());
    extra_screens= new Screen[1];
    extra_screens[0]= test_screen;
  }
  
  @Override
  public Screen[] getAdditionalScreens()
  {
    return extra_screens;
  }
  
  @Override
  public void onSessionStarted(boolean offline)
  {
    SensorControllerService.getInstance().resetMetrics();
  }
  
  @Override
  public void onSessionStopped()
  {
    long fp_distance= ValueStore.getInstance().getDistance();
    long distance= param2;
    
    // only calibrate the new factor if we can assume this was intentional
    if( fp_distance > 10 )
    {
      int footpod_calib= (int) Math.round((distance / ((double) fp_distance)) * 1000.00);
      
      SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(training);
      Editor editor= preferences.edit();
      
      editor.putInt(Preferences.footpod_int, footpod_calib);
      ValueStore.getInstance().setFpCalibInt(footpod_calib);
      
      editor.commit();
    }
  }
  
  @Override
  public boolean onMessage(ServerResponse r)
  {
    return false;
  }
  
}
