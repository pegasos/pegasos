package at.univie.mma.controller;

import android.util.Log;
import at.pegasos.client.controller.BaseTrainingController;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.Soundmachine;
import at.univie.mma.serverinterface.response.FeedbackMessage;
import at.pegasos.serverinterface.response.ServerResponse;
import at.pegasos.client.ui.TrainingActivity;
import at.univie.mma.ui.extrascreen.PerPotZeitprognose;
import at.pegasos.client.ui.screen.Screen;
import at.pegasos.client.ui.screen.ThreeSegments;
import at.univie.mma.ui.widget.target.SimpleTargetInt;
import at.univie.mma.ui.widget.target.SimpleTargetPoint;
import at.pegasos.client.values.Value;
import at.pegasos.client.values.ValueStore;

public class PerPotController extends BaseTrainingController
{
  Soundmachine soundmachine;
  
  PerPotZeitprognose fr1;
  SimpleTargetPoint speed_fragment;
  SimpleTargetInt hr_fragment;
  
  Screen[] extra_screens;
  Screen   perpot_screen1;
  
  private long   zeitprognose= -1;
  private double ians_puls= -1;
  private double opt_geschw_kmh= -1;

  private Value opt_geschw_km_value;
  private Value ians_puls_value;
  
  @Override
  public void initUI(TrainingActivity activity)
  {
    soundmachine= activity.getSoundmachine();
    
    opt_geschw_km_value= ValueStore.getInstance().getCreateValueDouble("TARGET_GESCHW", opt_geschw_kmh);
    ians_puls_value= ValueStore.getInstance().getCreateValueDouble("TARGET_HR", ians_puls);
    
    fr1= new PerPotZeitprognose();
    hr_fragment= new SimpleTargetInt();
    hr_fragment.setArguments(SimpleTargetInt.createArguments("TARGET_HR", "HR", R.drawable.heart));
    speed_fragment= new SimpleTargetPoint();
    speed_fragment.setArguments(SimpleTargetPoint.createArguments("TARGET_GESCHW", "SPEED_KMH", R.drawable.speed, R.drawable.kmh));
    
    if( zeitprognose != -1 )
      fr1.setTimeEstimate(zeitprognose);
    
    // if( ians_puls != -1 )
    //  hr_fragment.setTarget(ians_puls);
    
    // if( opt_geschw_kmh != -1 )
    //  speed_fragment.setTarget(opt_geschw_kmh);
    
    extra_screens= new Screen[1];
    perpot_screen1= new ThreeSegments(activity, fr1, speed_fragment, hr_fragment);
    extra_screens[0]= perpot_screen1;
  }
  
  @Override
  public Screen[] getAdditionalScreens()
  {
    return extra_screens;
  }
  
  @Override
  public void onSessionStarted(boolean offline)
  {
    if( offline ) throw new IllegalStateException("Cannot start a PerPot-Run when offline");
    
    soundmachine.say_it("PerPot Lauf aktiviert. Bitte beachten Sie die Ansagen des Systems.");
  }
  
  @Override
  public boolean onMessage(ServerResponse r)
  {
    if(r instanceof FeedbackMessage)
    {
      FeedbackMessage f= (FeedbackMessage) r;
      if( f.getMessage().equals("110") )
      {
        parsePerPotData(f.getExtra());
        return true;
      }
      else
        return false;
    }
    else
    {
      return false;
    }
  }
  
  private void parsePerPotData(String[] data)
  {
    for(String d : data)
    {
      // Log.d("PerPot", d);
      String[] dd= d.split(":");
      if( dd[0].equals("1") )
      { //Zeit
        // Log.e("PerPot", "Zeit: " + dd[1]);
        zeitprognose= Long.parseLong(dd[1]);
        fr1.setTimeEstimate(zeitprognose);
      }
      else if( dd[0].equals("2") )
      { //Puls
        // Log.e("PerPot", "Puls: " + dd[1]);
        ians_puls= Double.parseDouble(dd[1]);
        // hr_fragment.setTarget(ians_puls);
        ians_puls_value.setDoubleValue(ians_puls);
      }
      else
      { //Geschwindigkeit
        assert(dd[0].equals("3"));
        // Log.e("PerPot", "Geschwindigkeit: " + dd[1]);
        opt_geschw_kmh= Double.parseDouble(dd[1]);
        opt_geschw_km_value.setDoubleValue(opt_geschw_kmh);
      }
    }
    
    if( Config.LOGGING ) Log.d("PerPot", "Zeit: " + zeitprognose + " Puls:" + ians_puls + " Geschw: " + opt_geschw_kmh);
  }
  
  @Override
  public boolean disableSportScreens() {
    return false;
  }

  @Override
  public void onSessionStopped()
  {
    // Nothing to do here
  }
}
