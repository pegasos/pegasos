package at.univie.mma;

import android.content.Context;
import android.os.Vibrator;

public class VibratorService {
  private Vibrator v;
  private Context ctx;
  private static VibratorService instance;
  
  // Start without a delay
  //                                                       s    b    l    b    s    b    l
  private final static long[] DEFAULT_PATTERN_UNDER= { 0, 100, 100, 200, 100, 100, 100, 200 };
  //                                                       l    b    s    b    l    b    s
  private final static long[] DEFAULT_PATTERN_OVER=  { 0, 200, 100, 100, 100, 200, 100, 100 };
  
  private long[] pattern_under;
  private long[] pattern_over;
  
  public VibratorService(Context ctx)
  {
    this.ctx= ctx;
    
    this.pattern_under= DEFAULT_PATTERN_UNDER;
    this.pattern_over= DEFAULT_PATTERN_OVER;
    
    init();
  }
  
  private void init()
  {
    v= (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
  }
  
  public void signal_activity_start()
  {
    // Start without a delay
    long[] pattern = { 0, 100, 500, 100 };
    
    v.vibrate(pattern, -1);
  }
  
  public void signal_activity_stopp()
  {
    // Start without a delay
    long[] pattern = { 0, 500, 100, 500 };
    
    v.vibrate(pattern, -1);
  }
  
  public void signal_target_under()
  {
    v.vibrate(pattern_under, -1);
  }
  
  public void signal_target_over()
  {
    v.vibrate(pattern_over, -1);
  }

  public void signal_confirmed()
  {
    v.vibrate(400);
  }
  
  public void setPatternUnder(long[] pattern_under)
  {
    this.pattern_under=pattern_under;
  }
  
  public void setPatternOver(long[] pattern_over)
  {
    this.pattern_over=pattern_over;
  }
}
