package at.univie.mma;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import at.pegasos.client.ui.TrainingActivity;
import at.univie.mma.R;

/**
 * Service started to keep TrainingActivity as a foreground process.  
 */
public class TrainingService extends Service {

  private static final String NOTIFICATION_CHANNEL_ID= "at.pegasos.TrainingService";
  private static final String NOTIFICATION_CHANNEL_NAME= "Pegasos TrainingService";

  @Override
  public void onCreate()
  {
    super.onCreate();
  }
  
  @Override
  public void onDestroy()
  {
    super.onDestroy();
    stopForeground(true);
  }
  
  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    startForeground(1, buildNotification());
    return START_STICKY;
  }
  
  /**
   * Builds a persistent notification to inform the user about a running process.
   * 
   * @return The notification object
   */
  private Notification buildNotification()
  {
    Intent intent= new Intent(this, TrainingActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    PendingIntent pendingIntent= PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O )
    {
      NotificationManager nm= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

      NotificationChannel c= new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
      c.setDescription(NOTIFICATION_CHANNEL_NAME);

      nm.createNotificationChannel(c);
    }

    return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setContentTitle(getString(R.string.training_in_progress))
            .setTicker(getString(R.string.training_in_progress))
            .setContentText(getString(R.string.training_notification))
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentIntent(pendingIntent)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setAutoCancel(true)
            .build();
  }
  
  @Nullable
  @Override
  public IBinder onBind(Intent intent)
  {
    return null;
  }
}
