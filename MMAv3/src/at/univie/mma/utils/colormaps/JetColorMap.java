package at.univie.mma.utils.colormaps;

import android.graphics.Color;

public class JetColorMap extends MMAColorMap
{
  public static final String Name= "Jet";
  
  @Override
  public int getColor(float normValue)
  {
    return sGetColor(normValue);
  }
  
  @Override
  public int getColorPos(float normValuePos)
  {
    return sGetColorPos(normValuePos);
  }
  
  /**
   * 
   * @param normValue
   *          normalized value between -1 and 1
   * @return android.graphics.Color value as int
   */
  public static int sGetColor(float normValue)
  {
    return Color.rgb((int) (red(-normValue) * 255), (int) (green(-normValue) * 255), (int) (blue(-normValue) * 255));
  }
  
  /**
   * 
   * @param normValuePos
   *          normalized value between 0 and 1
   * @return android.graphics.Color value as int
   */
  public static int sGetColorPos(float normValuePos)
  {
    return sGetColor(normValuePos * 2 - 1);
  }
  
  private static float interpolate(float val, float y0, float x0, float y1, float x1)
  {
    return (val - x0) * (y1 - y0) / (x1 - x0) + y0;
  }
  
  private static float base(float val)
  {
    if( val <= -0.75 )
      return 0;
    else if( val <= -0.25 )
      return interpolate(val, 0.0f, -0.75f, 1.0f, -0.25f);
    else if( val <= 0.25 )
      return 1.0f;
    else if( val <= 0.75 )
      return interpolate(val, 1.0f, 0.25f, 0.0f, 0.75f);
    else
      return 0;
  }
  
  private static float red(float gray)
  {
    return base(gray - 0.5f);
  }
  
  private static float green(float gray)
  {
    return base(gray);
  }
  
  private static float blue(float gray)
  {
    return base(gray + 0.5f);
  }
}
