package at.univie.mma.utils.colormaps;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Color;

public abstract class MMAColorMap
{
  
  public static Map<String, MMAColorMap> colorMaps;
  static
  {
    colorMaps= new HashMap<String, MMAColorMap>();
    colorMaps.put(RYGYRColorMap.Name, new RYGYRColorMap());
    colorMaps.put(JetColorMap.Name, new JetColorMap());
    colorMaps.put("RED", new SingleColorMap(Color.rgb(255, 0, 0)));
  }
  
  /**
   * 
   * @param normValue
   *          normalized value between -1 and 1
   * @return android.graphics.Color value as int
   */
  public abstract int getColor(float normValue);
  
  /**
   * 
   * @param normValuePos
   *          normalized value between 0 and 1
   * @return android.graphics.Color value as int
   */
  public abstract int getColorPos(float normValuePos);
  
  /**
   * Retrieve ColorMap specified by mapName
   * @param mapName
   * @return
   */
  public static MMAColorMap fromString(String mapName)
  {
    if( colorMaps.containsKey(mapName) )
      return colorMaps.get(mapName);
    else
      return new JetColorMap();
  }
  
}
