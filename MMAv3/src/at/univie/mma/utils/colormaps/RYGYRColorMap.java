package at.univie.mma.utils.colormaps;

import android.graphics.Color;

public class RYGYRColorMap extends MMAColorMap
{
  public static final String Name= "RYGYR";
  
  @Override
  public int getColor(float normValue)
  {
    return sGetColor(normValue);
  }
  
  @Override
  public int getColorPos(float normValuePos)
  {
    return sGetColorPos(normValuePos);
  }
  
  /**
   * 
   * @param normValue
   *          normalized value between -1 and 1
   * @return android.graphics.Color value as int
   */
  public static int sGetColor(float normValue)
  {
    return sGetColorPos((normValue + 1) / 2.0f);
  }
  
  /**
   * 
   * @param normValuePos
   *          normalized value between 0 and 1
   * @return android.graphics.Color value as int
   */
  public static int sGetColorPos(float normValuePos)
  {
    float R= 0, G= 0, B= 0;
    if( normValuePos <= 0.25 )
    {
      R= 1;
      G= normValuePos * 4;
    }
    else if( normValuePos <= 0.5 )
    {
      R= 2 - normValuePos * 4;
      G= 1;
    }
    else if( normValuePos <= 0.75 )
    {
      R= normValuePos * 4 - 2;
      G= 1;
    }
    else
    {
      R= 1;
      G= 4 - normValuePos * 4;
    }
    return Color.rgb((int) (255 * R), (int) (255 * G), (int) (255 * B));
  }
  
}
