package at.univie.mma.utils.colormaps;

public class SingleColorMap extends MMAColorMap
{
  private int color;
  
  public SingleColorMap(int color)
  {
    this.color= color;
  }
  
  @Override
  public int getColor(float normValue)
  {
    return color;
  }
  
  @Override
  public int getColorPos(float normValuePos)
  {
    return color;
  }
}
