package at.pegasos.util;

import org.acra.ACRA;

public class CrashReporter {

  public static void handleException(Exception e)
  {
    ACRA.getErrorReporter().handleException(e);
  }
}
