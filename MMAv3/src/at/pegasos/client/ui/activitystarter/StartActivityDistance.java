package at.pegasos.client.ui.activitystarter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

import at.univie.mma.R;
import at.univie.mma.ui.*;

public class StartActivityDistance implements ActivityStarterFragment.ActivityStarterCallback {
  private final Context ctx;
  private final int activity_c;
  private final String sport;
  private final Runnable command;
  private final int distance;

  public StartActivityDistance(final Context ctx, final int activity_c, final String sport)
  {
    this.ctx = ctx;
    this.activity_c = activity_c;
    this.sport = sport;
    this.command = null;
    this.distance = -1;
  }

  public StartActivityDistance(final Context ctx, final int activity_c, final String sport, final int distance)
  {
    this.ctx = ctx;
    this.activity_c = activity_c;
    this.sport = sport;
    this.command = null;
    this.distance = distance;
  }

  public StartActivityDistance(final Context ctx, final int activity_c, final String sport, final Runnable additionalCommand)
  {
    this.ctx = ctx;
    this.activity_c = activity_c;
    this.sport = sport;
    this.command = additionalCommand;
    this.distance = -1;
  }

  public StartActivityDistance(final Context ctx, final int activity_c, final String sport, final Runnable additionalCommand, final int distance)
  {
    this.ctx = ctx;
    this.activity_c = activity_c;
    this.sport = sport;
    this.command = additionalCommand;
    this.distance = distance;
  }

  public void onActivityClicked(ActivityStarterFragment fragment)
  {
    if( distance != -1 )
    {
      Intent i = new Intent(ctx, TrainingLandingScreen.class);
      i.putExtra(TrainingLandingScreen.EXTRA_ACTIVITY, activity_c);
      i.putExtra(TrainingLandingScreen.EXTRA_SPORT, sport);
      i.putExtra(TrainingLandingScreen.EXTRA_ARGUMENTS, "" + distance);
      if( StartActivityDistance.this.command != null )
      {
        StartActivityDistance.this.command.run();
      }
      ActivityStarterFragment.setParameters(fragment, i);
      ctx.startActivity(i);

      return;
    }

    // 1. Instantiate an AlertDialog.Builder with its constructor
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

    // 2. Chain together various setter methods to set the dialog characteristics
    builder.setMessage(R.string.enter_distance).setTitle(R.string.distance);

    final EditText input = new EditText(ctx);
    input.setInputType(InputType.TYPE_CLASS_NUMBER);
    input.setRawInputType(Configuration.KEYBOARD_12KEY);
    builder.setView(input);

    builder.setPositiveButton(R.string.start_with_entered_distance,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id)
              {
                // User clicked OK button
                Intent i = new Intent(ctx, TrainingLandingScreen.class);
                i.putExtra(TrainingLandingScreen.EXTRA_ACTIVITY, activity_c);
                i.putExtra(TrainingLandingScreen.EXTRA_SPORT, sport);
                try
                {
                  // Test if parsable integer
                  Integer.parseInt(input.getEditableText().toString());
                  i.putExtra(TrainingLandingScreen.EXTRA_ARGUMENTS,
                          input.getEditableText().toString());
                  if( StartActivityDistance.this.command != null )
                  {
                    StartActivityDistance.this.command.run();
                  }
                  ctx.startActivity(i);
                }
                catch( java.lang.NumberFormatException e )
                {
                  Toast.makeText(ctx,
                          "Eingabe '" + input.getEditableText() + "' ist ung\uc3bcltig",
                          Toast.LENGTH_LONG).show();
                }
              }
            });

    builder.setNegativeButton(R.string.cancel_distance_start,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id)
              {
                dialog.dismiss();
              }
            });

    // 3. Get the AlertDialog from create()
    AlertDialog dialog = builder.create();

    dialog.show();
  }
}
