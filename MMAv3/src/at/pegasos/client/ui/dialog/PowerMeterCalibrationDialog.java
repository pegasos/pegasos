package at.pegasos.client.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import at.univie.mma.R;

public class PowerMeterCalibrationDialog implements CalibrationDialog {
  public interface IPowerMeterCalibrationCallback {
    /**
     * Callback method invoked when the calibration dialog is opened
     * 
     * @param ref
     *          reference to the dialog
     */
    public void onCalibrationDialogOpened(PowerMeterCalibrationDialog ref);

    public void storeOffsetValue(double value);
    
    /**
     * Get the currently stored calibration value
     * 
     * @return
     */
    public double getStoredOffsetValue();
    
    /**
     * Send the request for calibration to the sensor
     * 
     * @return true if send was performed
     */
    public boolean requestCalibration();
    
    /**
     * Calibration ended and dialog is closed
     */
    public void dialogClosed();
  }
  
  public enum RequestFailedReason{
    ANT_SERVICE_FAILURE,
    CommunicationError,
    Unkown
  };
  
  private Dialog dialog;
  
  TextView value_stored;
  TextView value_current;
  
  private Button btnSave;
  
  private IPowerMeterCalibrationCallback callback;
  
  /**
   * Last Value as reported by the callback
   */
  private double last_value;
  
  private Context ctx;
  
  private boolean is_ctf;

  private String name;
  
  public PowerMeterCalibrationDialog(Context ctx, IPowerMeterCalibrationCallback callback, boolean is_ctf, String name)
  {
    this.is_ctf= is_ctf;
    this.name= name;
    this.ctx= ctx;
    this.callback= callback;
    
    init();
  }
  
  public PowerMeterCalibrationDialog(Context ctx, IPowerMeterCalibrationCallback callback, boolean is_ctf)
  {
    this.is_ctf= is_ctf;
    this.name= null;
    this.ctx= ctx;
    this.callback= callback;
    
    init();
  }
  
  private void init()
  {
 // custom dialog
    dialog= new Dialog(ctx, R.style.TrainingDialogTheme);
    dialog.setContentView(R.layout.dialog_pm_calibration);
    dialog.setTitle(R.string.dialog_pm_calibration_title);
    
    // set the custom dialog components - text, image and button
    value_stored= (TextView) dialog.findViewById(R.id.editText);
    value_current= (TextView) dialog.findViewById(R.id.textView);
    
    value_stored.setText("" + callback.getStoredOffsetValue());
    value_stored.setEnabled(false);
    
    Button dialogButton= (Button) dialog.findViewById(R.id.btnClose);
    dialogButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        tearDown();
      }
    });
    
    Button btnCalibrate= (Button) dialog.findViewById(R.id.btnCalibrate);
    btnCalibrate.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        callback.requestCalibration();
        // TODO: react to status
      }
    });
    
    btnSave= (Button) dialog.findViewById(R.id.btnSave);
    btnSave.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v)
      {
        callback.storeOffsetValue(last_value);
        tearDown();
      }
    });
    btnSave.setEnabled(false);

    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialogInterface) {
        // TODO: make this thread safe
        if( showing )
          tearDown();
      }
    });
  }
  
  public void show()
  {
    callback.onCalibrationDialogOpened(this);
    dialog.show();
    showing= true;
  }
  
  protected void tearDown()
  {
    showing= false;
    dialog.dismiss();
    callback.dialogClosed();
  }
  
  public void setCurrentOffset(final double offset)
  {
    // Update Value on UI
    ((Activity) ctx).runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        value_current.setText("" + offset);
        // enable Button
        btnSave.setEnabled(true);
        
        last_value= offset;
      }
    });
  }
  
  @Override
  public CharSequence getDisplayName()
  {
    if( is_ctf )
      return "PowerMeter (CTF)" + (name != null ? " " + name : "");
    else
      return "PowerMeter (PWR)" + (name != null ? " " + name : "");
  }
  
  protected boolean showing;
  
  @Override
  public boolean isShowing()
  {
    return showing;
  }

  /**
   * The requested calibration failed
   */
  public void calibrationRequestFailed(final RequestFailedReason reason)
  {
    ((Activity) ctx).runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        value_current.setText("" + reason);
      }
    });
  }
  
  /**
   * Set the calibration value
   * 
   * @param offset
   */
  public void setValue(double offset)
  {
    if( showing )
      setCurrentOffset(offset);
  }

  /**
   * Report a failed calibration
   * 
   * @param reason
   */
  public void calibrationFailed(RequestFailedReason reason)
  {
    if( showing )
      calibrationRequestFailed(reason);
  }
}
