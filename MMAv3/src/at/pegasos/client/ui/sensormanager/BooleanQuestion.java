package at.pegasos.client.ui.sensormanager;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

import at.pegasos.client.ui.ParameterQuestion;
// Need to specifically import R like this
import at.univie.mma.R;

public class BooleanQuestion extends ParameterQuestion {

  public static class Builder {
    private final BooleanQuestion question;

    public Builder(String name)
    {
      this.question= new BooleanQuestion(name);
    }

    public BooleanQuestion build()
    {
      return question;
    }

    public Builder setMessage(String message)
    {
      this.question.message= message;
      return this;
    }

    public Builder setMessage(int message_id)
    {
      this.question.message_id= message_id;
      return this;
    }

    public Builder setAnswers(String[] answers)
    {
      this.question.positiveString= answers[0];
      this.question.negativeString= answers[1];
      return this;
    }

    public Builder setAnswers(int answerStringId)
    {
      this.question.answerStringId=answerStringId;
      return this;
    }
  }

  private int answerStringId= -1;
  private int message_id;
  private String message;

  private int positiveId;

  String positiveString= null;
  String negativeString= null;

  private boolean trueSelected= false;
  private boolean falseSelected= false;

  /**
   *
   * @param name
   *          name of the parameter
   */
  public BooleanQuestion(String name)
  {
    super(name);
  }

  /**
   *
   * @param name
   *          name of the parameter
   * @param description
   *          message to be displayed
   */
  public BooleanQuestion(String name, String description)
  {
    super(name);
    this.message= description;
  }

  /**
   *
   * @param name
   *          name of the parameter
   * @param description_id
   *          id (usually a R.string.) of the text to be displayed
   */
  public BooleanQuestion(String name, int description_id)
  {
    super(name);
    this.message_id= description_id;
  }

  /**
   *
   * @param name
   *          name of the parameter
   * @param description
   *          message to be displayed
   * @param answers
   *          array of answers, [0] is positive/yes/true, [1] is
   *          negative/no/false. All other indices will be ignored
   */
  public BooleanQuestion(String name, String description, String[] answers)
  {
    super(name);
    this.message= description;
    this.positiveString= answers[0];
    this.negativeString= answers[1];
  }

  /**
   *
   * @param name
   *          name of the parameter
   * @param description_id
   *          id (usually a R.string.) of the text to be displayed
   * @param answers
   *          array of answers, [0] is positive/yes/true, [1] is
   *          negative/no/false. All other indices will be ignored
   */
  public BooleanQuestion(String name, int description_id, String[] answers)
  {
    super(name);
    this.message_id= description_id;
    this.positiveString= answers[0];
    this.negativeString= answers[1];
  }

  /**
   *
   * @param name
   *          name of the parameter
   * @param description_id
   *          id (usually a R.string.) of the text to be displayed
   * @param answerStringId
   *          id (usually R.array.) of the answer strings. [0] is
   *          positive/yes/true, [1] is * negative/no/false. All other entries
   *          will be ignored
   */
  public BooleanQuestion(String name, int description_id, int answerStringId)
  {
    super(name);
    this.message_id= description_id;
    this.answerStringId= answerStringId;
  }

  protected void question()
  {
    AlertDialog.Builder builder= new AlertDialog.Builder(ctx);

    if( answerStringId != -1 )
    {
      String[] stringArray= ctx.getResources().getStringArray(answerStringId);
      positiveString= stringArray[0];
      negativeString= stringArray[1];
    }
    else if( positiveString == null )
    {
      String[] stringArray= ctx.getResources().getStringArray(R.array.boolean_strings);
      positiveString= stringArray[0];
      negativeString= stringArray[1];
    }

    LinearLayout view= new LinearLayout(ctx);
    view.setOrientation(LinearLayout.VERTICAL);

    TextView message= new TextView(ctx);
    if( this.message == null )
    {
      message.setText(message_id);
    }
    else
    {
      message.setText(this.message);
    }
    view.addView(message);
    RadioGroup group= new RadioGroup(ctx);
    group.setOrientation(RadioGroup.VERTICAL);

    // We can use View.generateViewId here because we are in target mr2
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
    {
      positiveId= View.generateViewId();
    }
    else
    {
      positiveId= (int) (Math.random() * 100000);
    }

    RadioButton radioButton= new RadioButton(ctx);
    radioButton.setText(positiveString);
    radioButton.setId(positiveId);
    group.addView(radioButton);

    radioButton= new RadioButton(ctx);
    radioButton.setText(negativeString);
    group.addView(radioButton);

    //set listener to radio button group
    group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        int checkedRadioButtonId = group.getCheckedRadioButtonId();
        if( checkedRadioButtonId == positiveId )
        {
          trueSelected= true;
        }
        else
        {
          falseSelected= true;
        }
      }
    });
    view.addView(group);

    builder.setView(view);

    builder.setPositiveButton(R.string.sensor_param_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id)
      {
        if( trueSelected )
        {
          dialog.dismiss();
          onAnswered("true");
        }
        else if( falseSelected )
        {
          dialog.dismiss();
          onAnswered("false");
        }
      }
    });

    builder.setNegativeButton(R.string.sensor_param_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id)
      {
        dialog.dismiss();

        onCancel();
      }
    });

    AlertDialog dialog= builder.create();
    dialog.show();
  }
}
