package at.pegasos.client.ui;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH_ADMIN;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.pegasos.client.*;
import org.acra.ACRA;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import at.univie.mma.LocaleManager;
import at.univie.mma.PermissionChecker;
import at.univie.mma.R;

public abstract class PegasosClientView extends AppCompatActivity {
  @Override
  protected void attachBaseContext(final Context base)
  {
    super.attachBaseContext(LocaleManager.setLocale(base));
  }
  
  public void showToast(final String msg, final int duration)
  {
    runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        Toast.makeText(getApplicationContext(), msg, duration).show();
      }
    });
  }
  
  public void showToast(String msg)
  {
    showToast(msg, Toast.LENGTH_SHORT);
  }
  
  public void showToast(int resource)
  {
    showToast(getString(resource), Toast.LENGTH_SHORT);
  }

  /**
   * Utility method for showing a toast. Use this method when in doubt whether
   * context is an MMAActivity
   *
   * @param ctx
   *          context
   * @param msg
   *          message to be shown
   */
  public static void showToast(Context ctx, String msg)
  {
    if( ctx instanceof PegasosClientView )
    {
      ((PegasosClientView) ctx).showToast(msg);
    }
    else
    {
      Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }
  }
  
  public enum CapabilityState {
    /**
     * App is currently capable of doing this
     */
    Can,
    /**
     * App can currently NOT do this
     */
    Cannot,
    /**
     * There is no information about whether or not the app can do this
     */
    Undecided
  };
  
  public static class Capabilities {
    private Map<String, CapabilityState> caps= new HashMap<String, CapabilityState>();
    
    public void setCapability(String name, CapabilityState state)
    {
      caps.put(name, state);
    }
    
    public CapabilityState getCapability(String name)
    {
      CapabilityState s= caps.get(name);
      if( s == null )
        return CapabilityState.Undecided;
      else
        return s;
    }

    public boolean isCapable(String name)
    {
      return getCapability(name) == CapabilityState.Can;
    }
  }
  
  protected Capabilities capabilities= new Capabilities();
  
  /**
   * Return the permission this activity needs
   * @return
   */
  protected String[] getRequiredPermissions()
  {
    return new String[0];
  }
  
  protected void checkAndRequestPermissions()
  {
    String[] required= getRequiredPermissions();
    int count= required.length;
    List<String> permissionsToRequest= new ArrayList<String>(count);
    
    for(int i= 0; i < count; i++)
    {
      if( ContextCompat.checkSelfPermission(this.getApplicationContext(), required[i]) != PackageManager.PERMISSION_GRANTED )
      {
        // at the moment we cannot do this...
        capabilities.setCapability(required[i], CapabilityState.Cannot);
        // request this permission
        permissionsToRequest.add(required[i]);
      }
      else
      {
        capabilities.setCapability(required[i], CapabilityState.Can);
      }
      
    }
    
    // Before checking the capabilities propagate current ones
    onCapabilitiesChanged();
    
    PermissionChecker.checkAndRequestPermissions(permissionsToRequest.toArray(new String[0]), this);
  }
  
  /**
   * Called for permission request results.
   * 
   * @param requestCode
   *          Code for the permission(s) requested.
   * @param permissions
   *          A list of request permissions.
   * @param grantResults
   *          A list of results if a permission has been granted, index corresponding to permissions
   *          array.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
  {
    switch( requestCode )
    {
      case PermissionChecker.PERMISSION_REQUEST_MULTIPLE:
        if( grantResults.length > 0 )
        {
          // retrieve request results
          for(int i= 0; i < permissions.length; i++)
          {
            // result.put(permissions[i], grantResults[i]);
            if( grantResults[i] == PackageManager.PERMISSION_GRANTED )
            {
              capabilities.setCapability(permissions[i], CapabilityState.Can);
              break;
            }
            else
            {
              if( ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]) )
              {
                PermissionChecker.showRationale(this, getString(getPermissionRationale(permissions[i])), permissions[i]);
              }
              else
              {
                capabilities.setCapability(permissions[i], CapabilityState.Cannot);
                Toast.makeText(this, getString(R.string.permission_required_failreason), Toast.LENGTH_LONG).show();
                PermissionChecker.showSettingsDialog(this);
              }
            }
          }
        }
        break;
      default:
        ACRA.getErrorReporter().handleException(new Exception("Hier ist ein unbehandelter Fall: " + requestCode));
    }
    
    onCapabilitiesChanged();
  }
  
  /**
   * The capabilities of the app/activity have changed.
   */
  protected void onCapabilitiesChanged()
  {
    if( capabilities.getCapability(WRITE_EXTERNAL_STORAGE) == CapabilityState.Can )
    {
      createDirs();
    }
  }
  
  protected void createDirs()
  {
    try
    {
      PegasosClient.getDirConfig().mkdirs();
      PegasosClient.getDirData().mkdirs();
    }
    catch( Exception e )
    {
      ACRA.getErrorReporter().handleException(e);
    }
  }
  
  /**
   * Transform permission into a rationale which can be shown to the user as explanation why a permission is needed.  
   * 
   * @param permission permission for which the rationale should be shown
   * @return
   */
  protected int getPermissionRationale(String permission)
  {
    if( permission.equals(WRITE_EXTERNAL_STORAGE) )
    {
      return R.string.permission_write_external_required;
    }
    else if(permission.equals(BLUETOOTH_ADMIN) )
    {
      return R.string.permission_bluetooth_required;
    }
    else if(permission.equals(ACCESS_FINE_LOCATION) )
    {
      return R.string.permission_fine_location_required;
    }
    else
    {
      return R.string.permission_required_failreason;
    }
  }
  
  /*public MMA getMMAApplication()
  {
    return (MMA) this.getApplicationContext();
  }*/
}
