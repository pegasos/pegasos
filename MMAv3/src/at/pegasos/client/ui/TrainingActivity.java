package at.pegasos.client.ui;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.GestureDetector.*;
import android.view.View.*;

import at.pegasos.client.*;
import at.pegasos.client.sports.*;
import at.pegasos.mma.ui.training.*;
import at.univie.mma.Config;
import at.univie.mma.*;
import at.univie.mma.R;
import at.univie.mma.ui.TimeoutUtil;
import at.univie.mma.ui.*;
import at.univie.mma.ui.controller.*;

public class TrainingActivity extends PegasosClientView implements OnTouchListener {
  public static final String EXTRA_ACTIVITY= TrainingLandingScreen.EXTRA_ACTIVITY;
  public static final String EXTRA_SPORT= TrainingLandingScreen.EXTRA_SPORT;
  public static final String EXTRA_ARGUMENT= TrainingLandingScreen.EXTRA_ARGUMENTS;
  public static final String EXTRA_STOP_TRAINING= "STOP_TRAINING";
  public static final String EXTRA_SERVER_TIMEOUT="SESSION_TIMEOUT";

  public static final int REQUEST_CODE_TRAINING= 1;

  private Soundmachine soundmachine;
  private VibratorService vibrator;

  private GestureDetector gestureDetector;
  
  private Intent serviceIntent;
  
  private ScreenManager screenManager;
  
  private ViewGroup screen_container;
  private TimeoutUtil timeoutUtil;

  /**
   * Sensor status display
   */
  private SensorStatusDisplay statusdisp;
  
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_training);
    
    // Check if MainActivity is active. If not we might have been killed before so better restart
    if( !PegasosClient.getInstance().connectCalled() )
    {
      finish();
    }
    
    serviceIntent= new Intent(this, TrainingService.class);
    startService(serviceIntent);
    
    screen_container= (ViewGroup) findViewById(R.id.trainingscreen_container);
    screen_container.setKeepScreenOn(true);
    
    PegasosClient app= ((PegasosClient) getApplicationContext());
    app.stopHeartBeats();
    
    Intent intent= getIntent();
    int activity_c = intent.getIntExtra(EXTRA_ACTIVITY, 0);
    String cargument = intent.getStringExtra(EXTRA_ARGUMENT);
    
    soundmachine= Soundmachine.getInstance(this);
    vibrator= new VibratorService(this.getApplicationContext());
    
    gestureDetector= new GestureDetector(this.getApplicationContext(), new GestureListener());

    timeoutUtil = new TimeoutUtil(this, new at.univie.mma.ui.TimeoutUtil.TimeoutListener() {
      
      @Override
      public void onTimeout(TimeoutUtil util)
      {
        TrainingUIController.getInstance().onStartTimedOut();
        setResult(Activity.RESULT_CANCELED, new Intent().putExtra(EXTRA_SERVER_TIMEOUT, true));
        if( !Config.TRAINING_SESSIONSTART_TIMEOUT_SHOWDIALOG )
          TrainingActivity.this.finish();
      }

      @Override
      public void onTimeoutAcknowledged()
      {
        TrainingActivity.this.finish();
      }
    }, Config.TRAINING_SESSIONSTART_TIMEOUT, Config.TRAINING_SESSIONSTART_TIMEOUT_SHOWDIALOG, null, getString(R.string.wait_server), getString(R.string.wait_server_timout));

    // Setup controller for the UI
    TrainingUIController me = TrainingUIController.getInstance();
    me.setUi(this);
    me.setParams(activity_c, cargument, intent.getStringExtra(EXTRA_SPORT));
    me.setTimeoutUtil(timeoutUtil);
    me.setSoundmachine(soundmachine);
    boolean continued= me.onUiStart();

    findViewById(R.id.activity_training).setOnTouchListener(this);

    screenManager = new ScreenManager(this, continued);
    screenManager.setup(me.getController(), me.getSport());
    screenManager.startUpdating();

    me.start();

    statusdisp = new SensorStatusDisplay();
    statusdisp.setUp(this);
    statusdisp.setVisibilities();

    // TODO: maybe start a sensor checker to check for sensor changes
  }

  @Override
  protected void onPause()
  {
    super.onPause();
    screenManager.setInPause(true);
  }
  
  @Override
  protected void onResume()
  {
    super.onResume();
    screenManager.setInPause(false);
    TrainingUIController.getInstance().getController().onResume();
  }
  
  @Override
  public void onConfigurationChanged(Configuration newConfig)
  {
    super.onConfigurationChanged(newConfig);
    
    // Nothing to do here for now...
  }
  
  @Override
  public void onDestroy()
  {
    super.onDestroy();
    
    if( timeoutUtil != null )
      timeoutUtil.abort();
    
    screenManager.stopUpdating();
    
    soundmachine.close(true);
    stopService(serviceIntent);
  }

  public Soundmachine getSoundmachine()
  {
    return soundmachine;
  }

  public void onSessionStarted()
  {
    // TODO: start the time

    timeoutUtil.abort();

    TrainingUIController.getInstance().getController().onSessionStarted(false);
    TrainingUIController.getInstance().OnSessionIdReceived();
  }

  @Override
  public void onBackPressed()
  {
    setResult(Activity.RESULT_OK, new Intent().putExtra("STOP_TRAINING", false));
    
    super.onBackPressed();
  }
  
  @Override
  public boolean onTouch(View v, MotionEvent event)
  {
    v.performClick();
    return gestureDetector.onTouchEvent(event);
  }
  
  private final class GestureListener extends SimpleOnGestureListener {
    
    @Override
    public boolean onDown(MotionEvent e)
    {
      return true;
    }
    
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
      if( Config.TRAINING_BACK_ON_TAP )
      {
        onTap();
        return true;
      }
      else
        return false;
    }
    
    @Override
    public void onLongPress(MotionEvent e)
    {
      if( Config.TRAINING_BACK_ON_LONGPRESS )
        onTap();
    }
    
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
      boolean result= false;
      try
      {
        float diffY= e2.getY() - e1.getY();
        float diffX= e2.getX() - e1.getX();
        if( Math.abs(diffX) > Math.abs(diffY) )
        {
          if( Math.abs(diffX) > Config.SWIPE_THRESHOLD && Math.abs(velocityX) > Config.SWIPE_VELOCITY_THRESHOLD )
          {
            if( diffX > 0 )
            {
              onSwipeRight();
            }
            else
            {
              onSwipeLeft();
            }
          }
          result= true;
        }
        /*
         * else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) >
         * SWIPE_VELOCITY_THRESHOLD) { if (diffY > 0) onSwipeBottom(); else onSwipeTop(); }
         */
        result= true;
      }
      catch( Exception exception )
      {
        exception.printStackTrace();
      }
      return result;
    }
  }
  
  public void onSwipeRight()
  {
    Log.d("TrainingActivity", "onSwipeRight");
    screenManager.rotate(1);
  }
  
  public void onSwipeLeft()
  {
    Log.d("TrainingActivity", "onSwipeLeft");
    screenManager.rotate(-1);
  }
  
  public void onTap()
  {
    // Log.d("TrainingActivity", "onTap");
    setResult(Activity.RESULT_OK, new Intent().putExtra("STOP_TRAINING", false));
    finish();
  }
  
  public void OnStopTraining()
  {
    setResult(Activity.RESULT_OK, new Intent().putExtra(EXTRA_STOP_TRAINING, true));
    
    finish();
  }
  
  public void setSensorIconsVisibilityGuiThread()
  {
    this.runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        TrainingActivity.this.statusdisp.setVisibilities();
      }
    });
  }

  @Deprecated
  public static long getStartTime()
  {
    return TrainingUIController.getInstance().getStartTime();
  }
  
  public ScreenManager getScreenManager()
  {
    return this.screenManager;
  }
  
  public ViewGroup getScreenContainer()
  {
    return screen_container;
  }
  
  public VibratorService getVibrator()
  {
    return vibrator;
  }

  /**
   * Get sport used during this activity. Use TrainingController.getSport instead
   * @return sport
   */
  @Deprecated
  public Sport getSport()
  {
    return TrainingUIController.getInstance().getSport();
  }
}
