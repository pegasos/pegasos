package at.pegasos.client.ui.screen;

import android.app.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import android.widget.LinearLayout.*;
import at.pegasos.client.ui.*;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.ui.*;

public class ThreeSegments extends Screen {
  private final MeasurementFragment frag1;
  private final MeasurementFragment frag2;
  private final MeasurementFragment frag3;
  private final int count;
  private boolean good;

  public ThreeSegments(TrainingActivity activity, MeasurementFragment... fragments)
  {
    super(activity);

    if( fragments.length == 3 )
    {
      this.frag1 = fragments[0];
      this.frag2 = fragments[1];
      this.frag3 = fragments[2];
      count = 3;
    }
    else if( fragments.length == 2 )
    {
      this.frag1 = fragments[0];
      this.frag2 = fragments[1];
      this.frag3 = null;
      count = 2;
    }
    else if( fragments.length == 1 )
    {
      this.frag1 = fragments[0];
      this.frag2 = null;
      this.frag3 = null;
      count = 1;
    }
    else
      throw new IllegalArgumentException();
  }

  /**
   * Put the screen on the (visual, i.e. phone) screen i.e. place yourself as a child of R.id.trianingscreen_container.
   * Make sure to properly remove the old screen if necessary by calling old.cleanUI()
   *
   * @param old the old screen
   */
  public void putOnUI(Screen old)
  {
    if( Config.LOGGING )
      Log.d("ThreeSegmentScreen", "putOnUI: " + frag1 + " " + frag2 + " " + frag3 + " old:" + old + "t.count:" + this.count + (
          (old != null) && old.getClass() == this.getClass() ? " o.count:" + ((ThreeSegments) old).count : "") + "");

    if( old != null && old.getClass() != this.getClass() )
    {
      if( Config.LOGGING )
        Log.d("ThreeSegmentScreen", "Incompatible Screens. Removing old screen");
      old.cleanUI();
      old = null;
    }

    if( old == null )
    {
      FragmentTransaction trans = activity.getFragmentManager().beginTransaction();

      // Inflate 3-Screen Layout
      View child = activity.getLayoutInflater().inflate(R.layout.training_screen, (ViewGroup) activity.getScreenContainer(), false);

      ((RelativeLayout) activity.findViewById(R.id.trainingscreen_container)).addView(child,
          new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

      trans.add(R.id.frame1, (Fragment) frag1);
      if( frag2 != null )
        trans.add(R.id.frame2, (Fragment) frag2);
      if( frag3 != null )
        trans.add(R.id.frame3, (Fragment) frag3);

      if( count < 3 )
      {
        View view = activity.findViewById(R.id.frame3);
        view.setVisibility(android.view.View.INVISIBLE);
        LayoutParams p = (LinearLayout.LayoutParams) view.getLayoutParams();
        p.weight = 0;
        view.setLayoutParams(p);

        if( count < 2 )
        {
          view = activity.findViewById(R.id.frame2);
          view.setVisibility(android.view.View.INVISIBLE);
          view.setLayoutParams(p);
        }
      }

      trans.commit();
    }
    else
    {
      ThreeSegments old1 = (ThreeSegments) old;

      if( this.count == old1.count )
      {
        if( old1.frag1 != this.frag1 )
        {
          activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag1).commit();
          activity.getFragmentManager().executePendingTransactions();
          activity.getFragmentManager().beginTransaction().add(R.id.frame1, (Fragment) frag1).commit();
        }
        if( old1.frag2 != this.frag2 )
        {
          activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag2).commit();
          activity.getFragmentManager().executePendingTransactions();
          activity.getFragmentManager().beginTransaction().add(R.id.frame2, (Fragment) frag2).commit();
        }
        if( old1.frag3 != this.frag3 )
        {
          activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag3).commit();
          activity.getFragmentManager().executePendingTransactions();
          activity.getFragmentManager().beginTransaction().add(R.id.frame3, (Fragment) frag3).commit();
        }
      }
      else
      {
        if( this.count < old1.count )
        {// need to hide at least one fragment container
          if( old1.count > 2 && this.count >= 1 ) // old 2,3, new/this = 1,2 (3 cannot be) --> have to hide 3
          {
            View view = activity.findViewById(R.id.frame3);
            view.setVisibility(android.view.View.INVISIBLE);
            LayoutParams p = (LinearLayout.LayoutParams) view.getLayoutParams();
            p.weight = 0;
            view.setLayoutParams(p);
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag3).commit();
          }
          else if( old1.frag3 != this.frag3 )
          {
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag3).commit();
            activity.getFragmentManager().executePendingTransactions();
            activity.getFragmentManager().beginTransaction().add(R.id.frame3, (Fragment) frag3).commit();
          }

					// don't care about the others, these have to be done else where, but we know if this is 1 have to hide frame2
          if( old1.count > 1 && this.count == 1 )
          {
            View view = activity.findViewById(R.id.frame2);
            LayoutParams p = (LinearLayout.LayoutParams) view.getLayoutParams();
            p.weight = 0;
            view.setVisibility(android.view.View.INVISIBLE);
            view.setLayoutParams(p);
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag2).commit();
          }
          else if( old1.frag2 != this.frag2 )
          {
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag2).commit();
            activity.getFragmentManager().executePendingTransactions();
            activity.getFragmentManager().beginTransaction().add(R.id.frame2, (Fragment) frag2).commit();
          }

          if( old1.frag1 != this.frag1 )
          {
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag1).commit();
            activity.getFragmentManager().executePendingTransactions();
            activity.getFragmentManager().beginTransaction().add(R.id.frame1, (Fragment) frag1).commit();
          }
        }
        else // this.count > old.count
        { //new/this has more fragments than old container
          if( old1.frag1 != this.frag1 )
          {
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag1).commit();
            activity.getFragmentManager().executePendingTransactions();
            activity.getFragmentManager().beginTransaction().add(R.id.frame1, (Fragment) frag1).commit();
          }

          if( old1.count == 1 && this.count > 1 )
          { // old (spot frame2) was empty -> add new fragment
            // Log.d("Screen", "Add Frag2");
            activity.getFragmentManager().beginTransaction().add(R.id.frame2, (Fragment) frag2).commit();
            View view = activity.findViewById(R.id.frame2);
            LayoutParams p = (LinearLayout.LayoutParams) view.getLayoutParams();
            p.weight = 1;
            view.setVisibility(android.view.View.VISIBLE);
            view.setLayoutParams(p);

          }
          else if( old1.frag2 != this.frag2 )
          {
            activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag2).commit();
            activity.getFragmentManager().executePendingTransactions();
            activity.getFragmentManager().beginTransaction().add(R.id.frame2, (Fragment) frag2).commit();
          }

          if( this.count > 2 )
          { // old (spot frame3) was empty -> add new fragment
            activity.getFragmentManager().beginTransaction().add(R.id.frame3, (Fragment) frag3).commit();
            View view = activity.findViewById(R.id.frame3);
            LayoutParams p = (LinearLayout.LayoutParams) view.getLayoutParams();
            p.weight = 1;
            view.setVisibility(android.view.View.VISIBLE);
            view.setLayoutParams(p);
          }
        }
      }
    }
    good = true;
  }

  /**
   * Send an update signal to all elements on the screen
   */
  public void updateUI()
  {
    if( good )
    {
      if( frag1 != null )
        frag1.updateUI();
      if( frag2 != null )
        frag2.updateUI();
      if( frag3 != null )
        frag3.updateUI();
    }
  }

  /**
   * Safely remove this screen from the training activity.
   * This means removing all your content from R.id.trainingscreen_container
   */
  public void cleanUI()
  {
    good = false;

    FragmentTransaction trans = activity.getFragmentManager().beginTransaction();

    ((RelativeLayout) activity.findViewById(R.id.trainingscreen_container)).removeAllViews();

    if( frag1 != null )
      trans.remove((Fragment) frag1);
    if( frag2 != null )
      trans.remove((Fragment) frag2);
    if( frag3 != null )
      trans.remove((Fragment) frag3);

    trans.commit();
  }
}
