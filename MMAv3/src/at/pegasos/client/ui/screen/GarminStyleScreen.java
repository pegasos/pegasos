package at.pegasos.client.ui.screen;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.View;
import android.view.ViewGroup;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.ui.MeasurementFragment;
import at.pegasos.client.ui.TrainingActivity;
import at.univie.mma.ui.widget.ScalableMeasurementFragment;

public class GarminStyleScreen extends Screen {
  private MeasurementFragment frag1;
  private MeasurementFragment frag2;
  private MeasurementFragment frag3;
  private MeasurementFragment frag4;
  private MeasurementFragment frag5;
  private MeasurementFragment frag6;
  private MeasurementFragment frag7;
  private MeasurementFragment frag8;
  private int count;
  private boolean good;

  /**
   * Maximimum widht (in px) a fragment needs
   */
  private int maxWidth;
  private float availableWidth;
  private float availableHeight;

  float sizeLarge;
  float sizeSmall;
  float sizeLabel;

  boolean sizesSet = false;

  public GarminStyleScreen(TrainingActivity activity, MeasurementFragment... fragments)
  {
    super(activity);

    if( fragments.length == 6 )
    {
      this.frag1= fragments[0];
      this.frag2= fragments[1];
      this.frag3= fragments[2];
      this.frag4= fragments[3];
      this.frag5= fragments[4];
      this.frag6= fragments[5];
      this.frag7= null;
      this.frag8= null;
      count= 6;
    }
    else if( fragments.length == 8 )
    {
      this.frag1= fragments[0];
      this.frag2= fragments[1];
      this.frag3= fragments[2];
      this.frag4= fragments[3];
      this.frag5= fragments[4];
      this.frag6= fragments[5];
      this.frag7= fragments[6];
      this.frag8= fragments[7];
      count= 8;
    }
    else
      throw new IllegalArgumentException();

    sizeLarge = (int) (activity.getResources().getDimension(R.dimen.textsize_training_fragment));
    sizeSmall = (int) (activity.getResources().getDimension(R.dimen.textsize_training_fragment_small));
    sizeLabel = (int) (activity.getResources().getDimension(R.dimen.textsize_training_fragment_label));
  }

  /**
   * Put the screen on the (visual, i.e. phone) screen i.e. place yourself as a child of R.id.trianingscreen_container.
   * Make sure to properly remove the old screen if necessary by calling old.cleanUI()
   * @param old the old screen
   */
  public void putOnUI(Screen old)
  {
    if( Config.LOGGING ) Log.d("GarminScreen", "putOnUI: " + frag1 + " " + frag2 + " " + frag3 + " old:" + old + "t.count:" + this.count + ((old != null) && old.getClass() == this.getClass() ? " o.count:" + ((GarminStyleScreen)old).count:"") +  "");
    if( Config.LOGGING ) Log.d("GarminScreen", "putOnUI: " + frag4 + " " + frag5 + " " + frag6 + " old:" + old + "t.count:" + this.count + ((old != null) && old.getClass() == this.getClass() ? " o.count:" + ((GarminStyleScreen)old).count:"") +  "");

    if( old != null && ( old.getClass() != this.getClass() || ((GarminStyleScreen) old).count != count ) )
    {
      if( Config.LOGGING ) Log.d("GarminScreen", "Incompatible Screens. Removing old screen");
      old.cleanUI();
      old= null;
    }

    if( old == null )
    {
      int layout;
      if( count == 6)
        layout= R.layout.training_screen_garmin6s;
      else
        layout= R.layout.training_screen_garmin;

      // Inflate Screen Layout
      View child= activity.getLayoutInflater().inflate(layout, (ViewGroup) activity.getScreenContainer(), false);

      ((RelativeLayout) activity.findViewById(R.id.trainingscreen_container)).addView(child,
           new RelativeLayout.LayoutParams(
               RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));

      addToScreen();
    }
    else
    {
      // TODO: here could be a better implementation. 
      
      GarminStyleScreen old1= (GarminStyleScreen) old;
      removeOld(old1);
      addToScreen();
    }
    activity.getScreenContainer().addOnLayoutChangeListener(layoutListener);
    good = true;
    sizesSet = false;
  }

  private void addToScreen()
  {
    LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    lp1.weight = 1;
    LinearLayout.LayoutParams lp0= new LinearLayout.LayoutParams(0, 0);
    lp0.weight = 0;

    FragmentTransaction trans= activity.getFragmentManager().beginTransaction();

    if( frag1 != null )
    {
      trans.add(R.id.frame1, (Fragment) frag1);
      activity.findViewById(R.id.frame1).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame1).setLayoutParams(lp0);
    }
    if( frag2 != null )
    {
      trans.add(R.id.frame2, (Fragment) frag2);
      activity.findViewById(R.id.frame2).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame2).setLayoutParams(lp0);
    }
    if( frag3 != null )
    {
      trans.add(R.id.frame3, (Fragment) frag3);
      activity.findViewById(R.id.frame3).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame3).setLayoutParams(lp0);
    }
    if( frag4 != null )
    {
      trans.add(R.id.frame4, (Fragment) frag4);
      activity.findViewById(R.id.frame4).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame4).setLayoutParams(lp0);
    }
    if( frag5 != null )
    {
      trans.add(R.id.frame5, (Fragment) frag5);
      activity.findViewById(R.id.frame5).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame5).setLayoutParams(lp0);
    }
    if( frag6 != null )
    {
      trans.add(R.id.frame6, (Fragment) frag6);
      activity.findViewById(R.id.frame6).setLayoutParams(lp1);
    }
    else
    {
      activity.findViewById(R.id.frame6).setLayoutParams(lp0);
    }

    if( count == 8 )
    {
      if( frag7 != null )
      {
        trans.add(R.id.frame7, (Fragment) frag7);
        activity.findViewById(R.id.frame7).setLayoutParams(lp1);
      }
      else
      {
        activity.findViewById(R.id.frame7).setLayoutParams(lp0);
      }
      if( frag8 != null )
      {
        trans.add(R.id.frame8, (Fragment) frag8);
        activity.findViewById(R.id.frame8).setLayoutParams(lp1);
      }
      else
      {
        activity.findViewById(R.id.frame8).setLayoutParams(lp0);
      }
    }

    trans.commit();
  }
  
  private int getMaxWidth()
  {
  	int maxWidth = 0;
  	// TODO: respect fragments with no neighbor
  	if( frag1 != null && frag1 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag1).getRequiredWidth() > maxWidth )
    {
    	Log.d("GarminScreen", "uS f1 mw: " + ((ScalableMeasurementFragment) frag1).getRequiredWidth());
      maxWidth= ((ScalableMeasurementFragment) frag1).getRequiredWidth();
    }
    if( frag2 != null && frag2 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag2).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag2).getRequiredWidth();
    if( frag3 != null && frag3 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag3).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag3).getRequiredWidth();
    if( frag4 != null && frag4 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag4).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag4).getRequiredWidth();
    if( frag5 != null && frag5 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag5).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag5).getRequiredWidth();
    if( frag6 != null && frag6 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag6).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag6).getRequiredWidth();
    if( frag7 != null && frag7 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag7).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag7).getRequiredWidth();
    if( frag8 != null && frag8 instanceof ScalableMeasurementFragment && ((ScalableMeasurementFragment) frag8).getRequiredWidth() > maxWidth )
      maxWidth= ((ScalableMeasurementFragment) frag8).getRequiredWidth();

    return maxWidth;
  }

  private void setSizes()
  {
    float scale = 1;
    if( frag1 != null && frag1 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag1).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag2 != null && frag2 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag2).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag3 != null && frag3 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag3).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag4 != null && frag4 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag4).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag5 != null && frag5 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag5).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag6 != null && frag6 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag6).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag7 != null && frag7 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag7).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
    if( frag8 != null && frag8 instanceof ScalableMeasurementFragment)
      ((ScalableMeasurementFragment) frag8).setScaleWidth(scale, sizeLarge, sizeSmall, sizeLabel, availableWidth);
  }

  private void updateSize()
  {
  	Log.d("GarminScreen", "updateSizes");
    // get sizes
    maxWidth= getMaxWidth();
    
    if( maxWidth > 0 && availableWidth > 0 )
    {
      if( maxWidth > availableWidth )
      {
        do
        {
      	  /*
      	  if( sizeLarge > availableHeight * 0.9 )
          sizeLarge= (int) (availableHeight * 0.9);
          */
          
          float scale = 1;
          sizeLarge = Math.max(10, sizeLarge - 3);
          sizeSmall = Math.max(10, sizeSmall - 2);
          sizeLabel = Math.max(10, sizeLabel - 1);
          
      	  setSizes();
          
          Log.d("GarminScreen", "scale:"+ scale + " a" + availableWidth + " m:" + maxWidth);
          
          maxWidth= getMaxWidth();
        } while( maxWidth > availableWidth );
      }
      else
      	setSizes();
    }
    else
      setSizes();
  }

  private void removeOld(GarminStyleScreen old1)
  {
    if( old1.frag1 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag1).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.frag2 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag2).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.frag3 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag3).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.frag4 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag4).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.frag5 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag5).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.frag6 != null )
    {
      activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag6).commit();
      activity.getFragmentManager().executePendingTransactions();
    }
    if( old1.count == 8 )
    {
      if( old1.frag7 != null )
      {
        activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag7).commit();
        activity.getFragmentManager().executePendingTransactions();
      }
      if( old1.frag8 != null )
      {
        activity.getFragmentManager().beginTransaction().remove((Fragment) old1.frag8).commit();
        activity.getFragmentManager().executePendingTransactions();
      }
    }
  }

  /**
   * Send an update signal to all elements on the screen
   */
  public void updateUI()
  {
    if( good )
    {
      if( frag1 != null )
        frag1.updateUI();
      if( frag2 != null )
        frag2.updateUI();
      if( frag3 != null )
        frag3.updateUI();
      if( frag4 != null )
        frag4.updateUI();
      if( frag5 != null )
        frag5.updateUI();
      if( frag6 != null )
        frag6.updateUI();
      if( frag7 != null )
        frag7.updateUI();
      if( frag8 != null )
        frag8.updateUI();

      if( !sizesSet )
      {
        updateSize();
      }
    }
  }
  
  /**
   * Safely remove this screen from the training activity.
   * This means removing all your content from R.id.trainingscreen_container
   */
  public void cleanUI()
  {
    good= false;
    
    FragmentTransaction trans= activity.getFragmentManager().beginTransaction();
    
    ((RelativeLayout) activity.findViewById(R.id.trainingscreen_container))
        .removeAllViews();
    
    if( frag1 != null )
      trans.remove((Fragment) frag1);
    if( frag2 != null )
      trans.remove((Fragment) frag2);
    if( frag3 != null )
      trans.remove((Fragment) frag3);
    if( frag4 != null )
      trans.remove((Fragment) frag4);
    if( frag5 != null )
      trans.remove((Fragment) frag5);
    if( frag6 != null )
      trans.remove((Fragment) frag6);
    if( count == 8 )
    {
      if( frag7 != null )
        trans.remove((Fragment) frag7);
      if( frag8 != null )
        trans.remove((Fragment) frag8);
    }
    
    trans.commit();
    activity.getScreenContainer().removeOnLayoutChangeListener(layoutListener);
  }

  View.OnLayoutChangeListener layoutListener= new View.OnLayoutChangeListener() {
    @Override
    public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
      availableWidth= activity.getScreenContainer().getWidth() / 2.0f;
      availableHeight= activity.getScreenContainer().getHeight();
      if( count > 6 )
        availableHeight/= 4.0;
      else
        availableHeight/= 3.0;
    }
  };
}
