package at.pegasos.client.ui;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.text.InputType;
import android.widget.EditText;
import at.univie.mma.R;
import at.pegasos.client.sensors.SensorParam;

public abstract class ParameterQuestion {
  public interface ParameterQuestionCallback {
    public void onSensorPairedQuestionsAnswered(SensorParam param, ArrayList<Entry<String, String>> additional_params);
    
    public void onSensoirPairedFailed();
  }
  
  private String param_name;
  
  List<ParameterQuestion> remaining;
  ParameterQuestionCallback caller;
  protected Context ctx;
  protected ArrayList<Entry<String, String>> params;
  
  private SensorParam param;
  
  public ParameterQuestion(String param_name)
  {
    this.param_name= param_name;
  }
  
  public void startAsking(Context ctx, ParameterQuestionCallback caller, SensorParam param, List<ParameterQuestion> remaining)
  {
    this.ctx= ctx;
    this.remaining= remaining;
    this.caller= caller;
    this.param= param;
    params= new ArrayList<Entry<String, String>>(1+(remaining != null ? remaining.size() : 0));
    
    question();
  }
  
  public void asking(Context ctx, ParameterQuestionCallback caller, SensorParam param, List<ParameterQuestion> remaining,
      ArrayList<Entry<String, String>> additional_params)
  {
    this.ctx= ctx;
    this.remaining= remaining;
    this.caller= caller;
    this.param= param;
    this.params= additional_params;
    
    question();
  }
  
  protected void onAnswered(String value)
  {
    params.add(new AbstractMap.SimpleEntry<String, String>(param_name, value));
    
    if( remaining.size() == 0 )
    {
      caller.onSensorPairedQuestionsAnswered(param, params);
    }
    else
    {
      ParameterQuestion q= remaining.get(0);
      remaining.remove(0);
      q.asking(ctx, caller, param, remaining, params);
    }
  }
  
  protected void onCancel()
  {
    caller.onSensoirPairedFailed();
  }
  
  protected abstract void question();
  
  public static class StringQuestion extends ParameterQuestion {
    private String description;
    
    public StringQuestion(String name, String description)
    {
      super(name);
      this.description= description;
    }
    
    protected void question()
    {
      // 1. Instantiate an AlertDialog.Builder with its constructor
      AlertDialog.Builder builder= new AlertDialog.Builder(ctx);
      
      // 2. Chain together various setter methods to set the dialog characteristics
      builder.setMessage(description);
      
      final EditText input= new EditText(ctx);
      // input.setInputType(InputType.TYPE_CLASS_NUMBER);
      // input.setRawInputType(Configuration.KEYBOARD_12KEY);
      builder.setView(input);
      
      builder.setPositiveButton(R.string.sensor_param_ok, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id)
        {
          dialog.dismiss();
          
          onAnswered(input.getEditableText().toString());
        }
      });
      
      builder.setNegativeButton(R.string.sensor_param_cancel, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id)
        {
          dialog.dismiss();
          
          onCancel();
        }
      });
      
      // 3. Get the AlertDialog from create()
      AlertDialog dialog= builder.create();
      
      dialog.show();
    }
  }
  
  public static class IntQuestion extends ParameterQuestion {
    private String description;
    private int description_id;
    
    public IntQuestion(String name, String description)
    {
      super(name);
      this.description= description;
    }
    
    public IntQuestion(String name, int description_id)
    {
      super(name);
      this.description_id= description_id;
    }
    
    protected void question()
    {
      // 1. Instantiate an AlertDialog.Builder with its constructor
      AlertDialog.Builder builder= new AlertDialog.Builder(ctx);
      
      // 2. Chain together various setter methods to set the dialog characteristics
      if( description == null )
        builder.setMessage(ctx.getString(description_id));
      else
        builder.setMessage(description);
      
      final EditText input= new EditText(ctx);
      input.setInputType(InputType.TYPE_CLASS_NUMBER);
      input.setRawInputType(Configuration.KEYBOARD_12KEY);
      builder.setView(input);
      
      builder.setPositiveButton(R.string.sensor_param_ok, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id)
        {
          dialog.dismiss();
          
          onAnswered(input.getEditableText().toString());
        }
      });
      
      builder.setNegativeButton(R.string.sensor_param_cancel, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id)
        {
          dialog.dismiss();
          
          onCancel();
        }
      });
      
      // 3. Get the AlertDialog from create()
      AlertDialog dialog= builder.create();
      
      dialog.show();
    }
  }
}
