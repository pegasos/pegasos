package at.pegasos.client.ui;

import android.annotation.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.text.method.*;
import android.view.*;
import android.widget.*;

import java.io.*;
import java.security.*;

import at.pegasos.client.*;
import at.pegasos.client.ServerCallback;
import at.pegasos.client.ui.controller.*;
import at.pegasos.client.util.*;
import at.univie.mma.Config;
import at.univie.mma.MainActivity;
import at.univie.mma.R;
import at.univie.mma.serverinterface.ServerInterface;

public class LoginActivity extends PegasosClientView {

  public static final String TAG= LoginActivity.class.getSimpleName();

  public static final int REQUEST_LOGIN= 111;

  public static final String EXTRA_RESULT= "result";
  public static final String EXTRA_OTHER= "other";

  private Button btn_login;

  private static LoginUIController controller;

  final private String clientId = "pegasos-client-android";

  final private String redirectURI = "pegasos://login";

  /**
   * Whether the user has pressed/clicked the login button once
   */
  private boolean login_pressed;

  ServerInterface server = ServerInterface.getInstance();

  Button.OnClickListener loginBtnClicked= new Button.OnClickListener() {
    
    @Override
    public void onClick(View arg0)
    {
      LoginActivity.this.runOnUiThread(new Runnable() {
        @Override
        public void run()
        {
          btn_login.setEnabled(false);

          try
          {
            final String loginUrl = controller.createLoginURL(clientId, redirectURI);
            PegasosLog.e(TAG, loginUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(loginUrl));
            intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            startActivity(intent);
            controller.OnLoginClicked();
          }
          catch( LoginUIController.LoginUIException e )
          {
            e.printStackTrace();
          }
        }
      });
      
      login_pressed= true;
    }
  };

  @SuppressLint("NewApi")
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    if( controller == null )
      controller = new LoginUIController(this);

    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    login_pressed= false;

    boolean returnFromWeb = false;

    Intent intent = getIntent();
    if( intent != null )
    {
      PegasosLog.e(TAG, "onCreate Intent: " + intent);
      if( Intent.ACTION_VIEW.equals(intent.getAction()) )
      {
        Uri uri = intent.getData();
        String code = uri.getQueryParameter("code");
        String stayloggedin = uri.getQueryParameter("stayloggedin");
        String login_type = uri.getQueryParameter("login_type");
        if( code != null )
        {
          (new Thread () {
            @Override
            public void run() {
              if( !controller.codeResponse(code, stayloggedin, login_type) )
              {
                showToast(R.string.login_failed);
              }
            }
          }).start();
          returnFromWeb = true;
        }
      }
    }

    if( !returnFromWeb )
    {
      if( server.getMessageManager().getCount() == 0 || PegasosClient.getInstance().getMainActivity() == null )
        finish();

      // We want a clean start check if we are starting properly (i.e. from main)
      if( Build.VERSION.SDK_INT >= 17 && (PegasosClient.getInstance().getMainActivity() == null ||
              PegasosClient.getInstance().getMainActivity().isDestroyed()) )
        finish();

      if( PegasosClient.getInstance().getMainActivity() == null )
        finish();
      PegasosClient.getInstance().getMainActivity().getResponseCallback().addMessageListener(this.controller);
    }
    else
    {
      // We know that MainActivity has not been started, so we need to attach response callback ourselves
      ServerCallback callback = new ServerCallback(null);
      callback.addMessageListener(controller);
      ServerInterface.getInstance().addResponseCallback(callback);
    }

    if( Config.LOGIN_USERLIST )
      setContentView(R.layout.login_activity_userlist);
    else
      setContentView(R.layout.login_activity);

    // TODO: I guess this was used as a fullscreen workaround. Remove?
    // ActionBar actionBar = getActionBar();
    // actionBar.hide();
    
    btn_login= (Button) findViewById(R.id.btn_login);
    btn_login.setOnClickListener(loginBtnClicked);
    
    TextView regist= (TextView) findViewById(R.id.tv_register);
    regist.setMovementMethod(LinkMovementMethod.getInstance());
    
    if( Config.LOGIN_USERLIST )
    {
      updateUserListGUI();

      // if( users.length == 0 )
      //  btn_login.setEnabled(false);
    }
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    if( Config.LOGIN_USERLIST )
    {
      getMenuInflater().inflate(R.menu.login, menu);
      return true;
    }
    else
      return false;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id= item.getItemId();
    
    if( Config.LOGIN_USERLIST )
    {
      if( id == R.id.action_add_user )
      {
        addUser();
        return true;
      }
      else if( id == R.id.action_remove_current_user )
      {
        /*if( users.length > 0 )
          removeCurrentUser();
        else
        {
          // TODO: report mistake (cannot remove non-existing user)
        }*/
        return true;
      }
    }
    
    return super.onOptionsItemSelected(item);
  }

  public void onRegister()
  {

  }

  private void addUser()
  {

  }

  /**
   * Load the user list and display it on the GUI. Hence this method needs to be
   * run on the GUI thread
   */
  private void updateUserListGUI()
  {

  }
  
  @Override
  public void onBackPressed()
  {
    // Remove message listener
    if( PegasosClient.getInstance().getMainActivity() != null )
    {
      if( PegasosClient.getInstance().getMainActivity().getResponseCallback() != null )
      {
        PegasosClient.getInstance().getMainActivity().getResponseCallback().removeMessageListener(this.controller);
      }
    }

    setResult(RESULT_CANCELED);

    finish();
  }

  public void OnLoginOk()
  {
    // we have done or work --> tear down
    controller = null;
    PegasosLog.d(TAG, "Have finished logging in. going to finish " + isTaskRoot() + " " + PegasosClient.getInstance().getMainActivity());
    if( PegasosClient.getInstance().getMainActivity() != null )
    {
      setResult(LoginActivity.RESULT_OK);

      finish();
    }
    else
    {
      Intent intent = new Intent(this, MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(intent);
    }
  }

  @Override
  protected void onResume()
  {
    PegasosLog.e(TAG, "onResume " + controller);
    Intent intent = getIntent();
    if( intent != null ) {
      PegasosLog.e(TAG, "onResume Intent: " + intent + " " + intent.getAction() + " " + intent.getData());
      if (Intent.ACTION_VIEW.equals(intent.getAction())) {
        PegasosLog.e(TAG, "Can request token");
      }
    }
    super.onResume();
  }

  @Override
  protected void onActivityResult (int requestCode,
                                   int resultCode,
                                   Intent data)
  {
    PegasosLog.d(TAG, "onActivityResult " + resultCode + " " + resultCode + " " + data);
    super.onActivityResult(requestCode, resultCode, data);
  }

  public void OnLoginFailed()
  {
    showToast(R.string.login_failed);

    btn_login.setEnabled(true);
  }
}
