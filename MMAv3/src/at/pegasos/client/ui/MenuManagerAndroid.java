package at.pegasos.client.ui;

import android.content.*;

import at.pegasos.client.activitystarter.*;
import at.pegasos.data.*;
import at.univie.mma.*;
import at.univie.mma.ui.*;

public class MenuManagerAndroid extends MenuManager {

  public MenuManagerAndroid(MainActivity activity)
  {
    super(activity);
  }
  
  @Override
  public void goToLanding(IActivityStarterFragment iActivityStarterFragment, Pair<Object, Object>[] arguments)
  {
    Intent i= new Intent((PegasosClientView) getActivity(), TrainingLandingScreen.class);
    for( Pair<Object, Object> arg : arguments)
    {
      // TODO: this is really stupid!
      if( arg.second.getClass().isPrimitive() )
      {
        i.putExtra((String) arg.first, (int) arg.second);
      }
      else if ( arg.second instanceof String )
      {
        i.putExtra((String) arg.first, (String) arg.second);
      }
      else
        throw new IllegalArgumentException("Cannot start activity with param " + arg.getClass().getSimpleName());
    }

    ((PegasosClientView) getActivity()).startActivity(i);
  }
}
