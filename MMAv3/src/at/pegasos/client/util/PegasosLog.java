package at.pegasos.client.util;

import android.util.Log;

import at.univie.mma.Config;

/**
 * Logging utility. All logging should go trough this facility.
 * For now on the client this uses System.(out/err) as backend.
 * On Android it uses android.util.Log as backend
 */
public class PegasosLog {

  public static void d(String tag, String message)
  {
    if( Config.LOGGING )
      Log.d(tag, message);
  }

  public static void e(String tag, String message)
  {
    Log.e(tag, message);
  }

  public static void i(String tag, String message)
  {
    Log.i(tag, message);
  }
}
