package at.pegasos.client.util;

import android.annotation.*;

import java.lang.reflect.*;
import java.util.*;

public class Base64Util {
  public static String encodeNoPaddingUrl(byte[] data)
  {
    return new String(android.util.Base64.encode(data,
                android.util.Base64.NO_WRAP | android.util.Base64.URL_SAFE | android.util.Base64.NO_PADDING));
  }

  @SuppressLint("NewApi")
  public static String encode(byte[] data, boolean padding)
  {
    try
    {
      Class.forName("java.util.Base64");
      return padding ?
              Base64.getEncoder().encodeToString(data) :
              Base64.getEncoder().withoutPadding().encodeToString(data);
    }
    catch( ClassNotFoundException e )
    {
      // try
      {
        /*Class<?> conv = Class.forName("android.util.Base64");
        java.lang.reflect.Method m = conv.getMethod("encode", byte[].class, int.class);
        int a = (int) conv.getField("NO_WRAP").get(null);
        int b = (int) conv.getField("NO_PADDING").get(null);
        return new String((byte[]) m.invoke(null, data, padding ? a : a|b));*/
        return new String(android.util.Base64.encode(data, padding ? android.util.Base64.NO_WRAP :
                android.util.Base64.NO_WRAP | android.util.Base64.URL_SAFE | android.util.Base64.NO_PADDING));
      }
      /*catch( ClassNotFoundException classNotFoundException )
      {
        classNotFoundException.printStackTrace();
      }
      catch( NoSuchMethodException classNotFoundException )
      {
        classNotFoundException.printStackTrace();
      }
      catch( NoSuchFieldException classNotFoundException )
      {
        classNotFoundException.printStackTrace();
      }
      catch( IllegalAccessException classNotFoundException )
      {
        classNotFoundException.printStackTrace();
      }
      catch( InvocationTargetException classNotFoundException )
      {
        classNotFoundException.printStackTrace();
      }*/
    }
    // return null;
  }
}
