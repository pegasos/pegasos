package at.pegasos.client.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class PreferenceManager {

  private static PreferenceManager instance;
  private SharedPreferences preferences;

  private SharedPreferences.Editor editor;

  public static void setup(Context ctx)
  {
    instance= new PreferenceManager();

    instance.preferences= android.preference.PreferenceManager.getDefaultSharedPreferences(ctx);
  }

  public static PreferenceManager getInstance()
  {
    return instance;
  }

  public int getInt(String name, int defaultValue)
  {
    return preferences.getInt(name, defaultValue);
  }

  public void storeInt(String name, int value)
  {
    if( editor != null )
      editor.putInt(name, value);
    else
      preferences.edit().putInt(name, value).apply();
  }

  public String getString(String name, String defaultValue)
  {
    return preferences.getString(name, defaultValue);
  }

  public boolean getBoolean(String name, boolean defaultValue)
  {
    return preferences.getBoolean(name, defaultValue);
  }

  public double getDouble(String name, double defaultValue)
  {
    return preferences.getFloat(name, (float) defaultValue);
  }

  public void remove(String name)
  {
    preferences.edit().remove(name).apply();
  }

  public void storeBoolean(String name, boolean value)
  {
    if( editor != null )
      editor.putBoolean(name, value);
    else
      preferences.edit().putBoolean(name, value).apply();
  }

  public void startEdit()
  {
    editor= preferences.edit();
  }

  public void apply()
  {
    editor.apply();
    editor= null;
  }

  public void storeDouble(String name, double value)
  {
    if( editor != null )
      editor.putFloat(name, (float) value);
    else
      preferences.edit().putFloat(name, (float) value).apply();
  }

  public void storeString(String name, String value)
  {
    if( editor != null )
      editor.putString(name, value);
    else
      preferences.edit().putString(name, value).apply();
  }

  public void storeStringSet(String name, Set<String> value)
  {
    if( editor != null )
      editor.putStringSet(name, value);
    else
      preferences.edit().putStringSet(name, value).apply();
  }

  public Set<String> getStringSet(String name, Set<String> defaultValue)
  {
    return preferences.getStringSet(name, defaultValue);
  }

  public void storeLong(String name, long value)
  {
    if( editor != null )
      editor.putLong(name, value);
    else
      preferences.edit().putLong(name, value).apply();
  }
}
