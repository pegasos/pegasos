package at.pegasos.client;

import android.app.*;
import android.content.*;
import android.content.res.Configuration;
import android.net.*;
import android.os.*;
import android.util.*;
import android.widget.*;
import at.pegasos.client.*;
import at.pegasos.client.sports.*;
import at.pegasos.client.ui.*;
import at.pegasos.client.util.*;
import at.univie.mma.*;
import at.univie.mma.Config;
import at.univie.mma.R;
import at.univie.mma.serverinterface.*;
import at.univie.mma.ui.*;
import org.acra.*;
import org.acra.config.*;
import org.acra.data.*;
import org.acra.sender.*;

import java.io.*;
import java.util.*;

public class PegasosClient extends Application implements Application.ActivityLifecycleCallbacks {
  private static final int SERVER_SENDDELAY = 1000;
  private static PegasosClient instance;
  Timer heartbeattask = null;
  private boolean trainingUploadRequired = false;
  private boolean intraining;
  private boolean inlanding;
  private boolean connected = false;
  private Activity foregroundActivity;
  private MainActivity mainActivity;

  @Override public void onCreate()
  {
    super.onCreate();

    if( Config.LOGGING )
      Log.d("MMA", "Application onCreate");

    PreferenceManager.setup(this);

    // Register to be notified of activity state changes
    registerActivityLifecycleCallbacks(this);

    connectToServer();

    CoreConfigurationBuilder builder= new CoreConfigurationBuilder(this);
    builder.setBuildConfigClass(at.univie.mma.BuildConfig.class).setReportFormat(StringFormat.JSON);
    builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
            .setUri(Config.ACRA_URI)
            .setBasicAuthLogin(Config.ACRA_LOGIN)
            .setBasicAuthPassword(Config.ACRA_PASSWORD)
            .setHttpMethod(HttpSender.Method.PUT)
            .setEnabled(true);
    builder.getPluginConfigurationBuilder(ToastConfigurationBuilder.class)
            .setResText(R.string.crash_text)
            .setLength(Toast.LENGTH_LONG)
            .setEnabled(true);

    ACRA.init(this, builder);
    ACRA.getErrorReporter().putCustomData("build-time", Config.BUILD_TIME);
    ACRA.getErrorReporter().putCustomData("build-id", Config.BUILD_ID);

    instance = this;

    init();

    // run startup actions
    new StartupActions().run();
  }

  /**
   * Open connection to server
   */
  public void connectToServer()
  {
    ServerInterface.getInstance(Config.MMA_SERVER_IP, Config.MMA_SERVER_PORT, SERVER_SENDDELAY);
    if( Config.LOGGING )
      Log.d("MMA", "ServerInterface created");
    connected = true;
  }

  public void closeConnection()
  {
    if( ServerInterface.getInstance() != null )
    {
      ServerInterface.getInstance().stopSending(true);
      ServerInterface.getInstance().close();
    }
    connected = false;
  }

  /**
   * Check if ServerInterface was created
   * @return true if ServerInterface is usable
   */
  public boolean connectCalled()
  {
    return connected;
  }

  private void init()
  {
    Sports.fillinIds(this);
  }

  public void startHeartBeats()
  {
    if( heartbeattask == null )
    {
      heartbeattask= new Timer();
      heartbeattask.scheduleAtFixedRate(new TimerTask() {

        @Override
        public void run()
        {
          ServerInterface s= ServerInterface.getInstance();

          if( s != null )
            s.heartbeat();
          else
          {
            // this is most probably due to an "orphaned" app --> restart app
            restartApp(PegasosClient.this.getApplicationContext());
          }
        }
      }, 1000, 10000);
    }
  }

  public static void restartApp(Context ctx)
  {
    Intent intent= new Intent(ctx.getApplicationContext(), MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    int mPendingIntendId= 123456;
    PendingIntent mPendingIntent= PendingIntent.getActivity(ctx.getApplicationContext(), mPendingIntendId, intent,
            PendingIntent.FLAG_CANCEL_CURRENT);
    AlarmManager mgr= (AlarmManager) ctx.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);

    Runtime.getRuntime().exit(0);
  }

  public void stopHeartBeats()
  {
    if( heartbeattask != null )
    {
      heartbeattask.cancel();
      heartbeattask.purge();
      heartbeattask= null;
    }
  }

  public void setTrainingUploadRequired(boolean required)
  {
    this.trainingUploadRequired= required;
  }

  public boolean isTrainingUploadRequired()
  {
    return this.trainingUploadRequired;
  }

  @Override
  protected void attachBaseContext(Context base)
  {
    super.attachBaseContext(LocaleManager.setLocale(base));
    Log.d("MMA", "attachBaseContext");
  }

  // TODO: make this better. There should be a deprecation
  @SuppressWarnings("deprecation")
  @Override
  public void onConfigurationChanged(Configuration newConfig)
  {
    super.onConfigurationChanged(newConfig);
    LocaleManager.setLocale(this);
    Log.d("MMA", "onConfigurationChanged: " + newConfig.locale.getLanguage());
  }

  public boolean isNetworkAvailable()
  {
    ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo= connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
  }

  /**
   * Return whether or a TrainingActivity is currently visible
   * @return
   */
  public boolean inTraining()
  {
    return intraining;
  }

  /**
   * Return whether or not the LandingScreen is currently visible
   * @return
   */
  public boolean inLanding()
  {
    return inlanding;
  }

  /**
   * Return the activity which can be assumed to be in the foreground
   * @return
   */
  public Activity getForegroundActivity()
  {
    return foregroundActivity;
  }

  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState)
  {

  }

  @Override
  public void onActivityStarted(Activity activity)
  {

  }

  @Override
  public void onActivityResumed(Activity activity)
  {
    intraining= inlanding= false;
    if( activity instanceof TrainingActivity)
      intraining= true;
    else if( activity instanceof TrainingLandingScreen)
      inlanding= true;
    foregroundActivity= activity;
  }

  @Override
  public void onActivityPaused(Activity activity)
  {

  }

  @Override
  public void onActivityStopped(Activity activity)
  {
    if( activity instanceof TrainingActivity )
      intraining= false;
    else if( activity instanceof TrainingLandingScreen )
      inlanding= false;
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState)
  {

  }

  @Override
  public void onActivityDestroyed(Activity activity)
  {

  }

  public void showToast(final int id)
  {
    if( foregroundActivity != null )
    {
      foregroundActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Toast.makeText(foregroundActivity.getApplicationContext(), id, Toast.LENGTH_SHORT).show();
        }
      });
    }
  }

  public void showToast(final String string)
  {
    if( foregroundActivity != null)
    {
      foregroundActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Toast.makeText(foregroundActivity.getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        }
      });
    }
  }

  public void setMainActivity(MainActivity mainActivity)
  {
    this.mainActivity = mainActivity;
  }

  public MainActivity getMainActivity()
  {
    return mainActivity;
  }

  public static PegasosClient getInstance()
  {
    return instance;
  }

  public static File getDirConfig()
  {
    String rootDir= Environment.getExternalStorageDirectory().toString();
    File f;
    if( Config.USE_OWN_DIRECTORY )
      f= new File(rootDir + "/Pegasos-" + Config.APP_NAME.replaceAll(" ", "_").replaceAll("\'", "").trim() + "/config");
    else
      f= new File(rootDir + "/PegasosClient/config");
    return f;
  }

  public static File getDirData()
  {
    String rootDir= Environment.getExternalStorageDirectory().toString();
    File f;
    if( Config.USE_OWN_DIRECTORY )
      f= new File(rootDir + "/Pegasos-" + Config.APP_NAME.replaceAll(" ", "_").replaceAll("\'", "").trim() + "/data");
    else
      f= new File(rootDir + "/PegasosClient/data");
    return f;
  }
}
