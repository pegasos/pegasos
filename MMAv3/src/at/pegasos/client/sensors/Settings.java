package at.pegasos.client.sensors;

import android.app.*;
import android.util.*;
import android.widget.*;

import java.io.*;

import at.univie.mma.Config;
import at.univie.mma.BuildConfig;
import at.univie.mma.serverinterface.*;
import at.univie.mma.ui.controller.*;

public class Settings {
  private static Settings inst;
  private final at.univie.mma.serverinterface.sensors.Setting sen;
  private boolean startTimeSet = false;

  private Settings()
  {
    sen = new at.univie.mma.serverinterface.sensors.Setting(1, 1);
    ServerInterface.getInstance().safeSensorAttach(sen);
  }

  /**
   * Report a setting with its value to the server.
   * Value is reported as string in order to support different types.
   *
   * @param name  name of the setting
   * @param value value
   */
  public static void reportSetting(String name, String value)
  {
    getInstance().report(name, value, null);
  }

  private synchronized static Settings getInstance()
  {
    if( inst == null )
      inst = new Settings();

    inst.setTime();

    return inst;
  }

  private void setTime()
  {
    if( TrainingUIController.getInstance().getStartTime() != 0  )
    {
      // TODO: some caveats we should consider here
      // * what about cases where settings are reported after a previous session,
      //   but before the new one has been started? --> time would be wrong
      sen.setStartTime(TrainingUIController.getInstance().getStartTime());
      startTimeSet = true;
    }
    sen.start_sending();
  }

  public static void reportSetting(String name, int value)
  {
    getInstance().report(name, "" + value, null);
  }

  public static void reportSetting(String name, int value, File output)
  {
    getInstance().report(name, "" + value, output);
  }

  public static void reportSetting(final Activity ctx, final String name, int value)
  {
    ctx.runOnUiThread(() -> Toast.makeText(ctx, "Report Setting " + name, Toast.LENGTH_SHORT).show());

    getInstance().report(name, "" + value, null);
  }

  private void report(String name, String value, File output)
  {
    if( BuildConfig.DEBUG )
    {
      if( name.length() > 30 )
        throw new IllegalArgumentException("Name too long");
      if( value.length() > 60 )
        throw new IllegalArgumentException("Value too long");
    }

    if( !startTimeSet )
    {
      if( BuildConfig.DEBUG && TrainingUIController.getInstance().getStartTime() == 0 )
        throw new IllegalStateException("Setting set before a session has been started");
      sen.setStartTime(TrainingUIController.getInstance().getStartTime());
      startTimeSet = true;
    }

    if( Config.LOGGING ) Log.d("Settings", name + "=" + value);

    if( output != null )
    {
      if( !ServerInterface.getInstance().isSensorDataFileOpen() )
      {
        try
        {
          ServerInterface.getInstance().setSensorDataFile(new FileOutputStream(output, true));
          sen.setData(name, value);
          // Just to be on the safe side we finish the packet
          sen.finishPacket();
          ServerInterface.getInstance().singleSend();
          ServerInterface.getInstance().closeSensorDataFile();
        }
        catch( FileNotFoundException e )
        {
          e.printStackTrace();
        }
      }
      else
      {
        sen.setData(name, value);
      }
    }
    else
    {
      sen.setData(name, value);
    }
  }
}
