package at.pegasos.client.sensors;

import android.content.Context;
import android.util.Log;

import at.pegasos.client.sensors.*;
import at.pegasos.client.sensors.Sensor;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;

import at.univie.mma.Config;

public abstract class AntPccSensor extends ANTSensor {
  
  /**
   * Constant for sensor_id / serial indicating that no corresponding setting could be loaded. Note:
   * this value is != 0 so that the sensor will not pair randomly
   */
  private static final int NO_SENSOR_ID_SPECIFIED= -1;
  
  protected int sampling_rate= 1000;
  
  protected com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc internal_sensor;
  
  protected boolean killed= false;
  
  protected int sensor_id;

  protected final Context ctx;

  public AntPccSensor(Context ctx)
  {
    super(ctx);
    this.ctx= ctx;
  }

  @Override
  protected void connect()
  {
    Log.d(LOG_TAG, "connect");
    if( pairing )
      reset(0);
    else
    {
      SensorParam p= currentSensorParam();
      if( p != null )
      {
        loadParams(p);
        reset(sensor_id);
      }
      else
      {
        // TODO: report this behaviour
        // TODO: add switch to make this configurable
        Log.i(LOG_TAG, "No ANT-Power Sensor paired --> Party mode");
        // setPairing(true);
        reset(0);
      }
    }
  }
  
  protected void reset(int serial)
  {
    if( internal_sensor != null )
    {
      internal_sensor.releaseAccess();
      internal_sensor= null;
    }
  }
  
  /**
   * Load the configuration of the current SensorParam into this sensor. <BR/>
   * <BR/>
   * Overwrite this method to react to new configurations during `connect` events. BUT: do not
   * forget to call this method (i.e. call <code>super.loadParams(sparam)</code> in your method)
   * 
   * @param sparam
   *          new sensor param
   */
  protected void loadParams(SensorParam sparam)
  {
    String sserial= sparam.getSensorId();
    if( sserial.equals("") )
      sensor_id= NO_SENSOR_ID_SPECIFIED;
    else
      sensor_id= Integer.parseInt(sserial);
  }
  
  @Override
  public void stopSensor()
  {
    killed= true;
    destroy();
  }
  
  public void destroy()
  {
    if( internal_sensor != null )
    {
      internal_sensor.releaseAccess();
      internal_sensor= null;
    }
  }
  
  @Override
  protected void onConnectFailed(ConnectionFailedReason reason)
  {
    Log.d(LOG_TAG, "onConnectFailed " + pairing + " " + reason);
    if( reason == ConnectionFailedReason.ANT_CONNECTION_LOST )
    {
      if( internal_sensor != null )
        internal_sensor.releaseAccess();

      // By default try to reconnect once to the same sensor again. If sensor is really dead,
      // connection will timeout and we will attempt to connect to a different sensor
      connect();
    }
    else if( pairing && reason == ConnectionFailedReason.ANT_CHANNEL_NOT_AVAILABLE )
    {
      if( internal_sensor != null )
        internal_sensor.releaseAccess();
    }
    else
      super.onConnectFailed(reason);
  }

  protected void handleResultCodes(RequestAccessResult resultCode, DeviceState initialDeviceState)
  {
    switch( resultCode )
    {
      case SUCCESS:
        break;

      case SEARCH_TIMEOUT:
        status.state= Sensor.State.STATE_CONNECTING;
        status.number= 0;
        status.text= "--";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "SEARCH TIMEOUT");
        onConnectFailed(ConnectionFailedReason.SEARCH_TIMEOUT);

      case CHANNEL_NOT_AVAILABLE:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.e(LOG_TAG, "Channel Not Available");
        onConnectFailed(ConnectionFailedReason.ANT_CHANNEL_NOT_AVAILABLE );
        // reset_cad(0);
        break;

      case OTHER_FAILURE:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "RequestAccess failed. See logcat for details.");
        break;

      case DEPENDENCY_NOT_INSTALLED:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "ANT NOT INSTALLED");
        break;

      case USER_CANCELLED:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "Cancelled. Do Menu->Reset.");
        break;

      case UNRECOGNIZED:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "Failed: UNRECOGNIZED. Upgrade Required?");
        break;

      default:
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.number= 0;
        status.text= "";
        if( Config.LOGGING )
          Log.d(LOG_TAG, "Unrecognized result: " + resultCode);
        break;
    }
  }
  
  /**
   * Receiver for connection state changes
   */
  protected IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver= new IDeviceStateChangeReceiver() {
    @Override
    public void onDeviceStateChange(final DeviceState newDeviceState)
    {
      if( Config.LOGGING )
        Log.d(LOG_TAG, "onDeviceStateChange: " + newDeviceState);
      switch( newDeviceState )
      {
        case CLOSED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          break;
        
        case DEAD:
          // Toast.makeText(ctx.getApplicationContext(), LOG_TAG + " DEAD", Toast.LENGTH_SHORT).show();
          status.state= Sensor.State.STATE_CONNECTING;
          status.number= 0;
          internal_sensor= null;
          status.text= "";
          onDead();
          onConnectFailed(ConnectionFailedReason.ANT_CONNECTION_LOST);
          break;
        
        case PROCESSING_REQUEST:
          break;
        
        case SEARCHING:
          status.state= Sensor.State.STATE_CONNECTING;
          status.number= 0;
          status.text= "--";
          break;
        
        case TRACKING:
          if( Config.LOGGING )
            Log.d(LOG_TAG, " (from Base): Tracking --> sensor state: ok");
          status.state= Sensor.State.STATE_OK;
          status.number= 0;
          status.text= "OK";
          break;
        
        case UNRECOGNIZED:
          break;
      }
    }
  };
  
  /**
   * Perform actions necessary when sensor reports dead status
   */
  protected void onDead()
  {
    
  }
}
