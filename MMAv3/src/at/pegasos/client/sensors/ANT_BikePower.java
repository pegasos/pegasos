package at.pegasos.client.sensors;

import android.content.*;
import android.util.*;
import android.widget.*;
import at.pegasos.client.ui.*;
import at.pegasos.client.ui.dialog.*;
import at.pegasos.client.values.*;
import at.pegasos.serverinterface.*;
import at.univie.mma.Config;
import at.pegasos.client.values.ValueStore;

import com.dsi.ant.plugins.antplus.pcc.*;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.*;
import com.dsi.ant.plugins.antplus.pcc.defines.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.*;

import java.math.*;
import java.util.*;
import java.util.Map.*;

/**
 * Connects to Bike Power Plugin and display all the event data.
 */
public class ANT_BikePower extends AntPccSensor implements PowerMeterCalibrationDialog.IPowerMeterCalibrationCallback {

  private static final double OFFSET_NOT_INITIALISED= 0;
  
  private static final String PARAM_OFFSET= "offset";
  private static final String PARAM_HAS_CADENCE= "has_cadence";
  private static final String PARAM_USE_CADENCE= "use_cadence";
  private static final String PARAM_DISPLAY_CADENCE= "disp_cadence";
  private static final String PARAM_HAS_SPEED= "has_speed";
  private static final String PARAM_IS_CTF= "is_ctf";
  
  /**
   * This question will be asked when the Sensor is paired
   */
  private static final ParameterQuestion cadenceq= new ParameterQuestion.StringQuestion(PARAM_USE_CADENCE, "Do you want to use this sensor as cadence sensor?");
  
  int bike_device_id= 0;
  private boolean required_spd;
  private boolean subscribed_spd;
  private boolean subscribed_pwr;
  
  protected com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc internal_sensor_speed;
  
  protected at.univie.mma.serverinterface.sensors.BikeNrSS server_sensor;
  private final ValueStore store;
  
  private Value power;
  private Value cadence;
  
  short power_short;
  
  boolean is_running= false;
  
  private double offset= OFFSET_NOT_INITIALISED;
  
  private final Timer avgs;

  /**
   * Flag indicating if the power meter is a CTF (true) or PWR (false) device
   */
  protected boolean is_ctf;
  
  /**
   * Flag indicating whether this sensor should send supplied cadence values to the server
   */
  private boolean use_cad;
  
  private boolean disp_cad;
  
  /**
   * String to be appended to all variable names of this instance
   */
  private final String var_suffix;
  
  protected String name;

  private PowerMeterCalibrationDialog calibrationDialog;

  public ANT_BikePower(Object ctx)
  {
    super((Context) ctx);
    
    LOG_TAG= "AntBikePower";
    
    status= new Status();
    status.state= Sensor.State.STATE_NOT_CONNECTED;
    status.number= 0;
    status.text= "";
    
    store= ValueStore.getInstance();
    
    var_suffix= "";
    createVariables();
    
    avgs= new Timer();
  }
  
  public ANT_BikePower(Context ctx, String var_suffix)
  {
    super(ctx);
    
    LOG_TAG= "AntBikePower";
    
    status= new Status();
    status.state= Sensor.State.STATE_NOT_CONNECTED;
    status.number= 0;
    status.text= "";
    
    store= ValueStore.getInstance();
    
    this.var_suffix= var_suffix;
    createVariables();
    
    avgs= new Timer();
  }
  
  private void createVariables()
  {
    power= store.getCreateNumberValueListening("POWER" + var_suffix);
    cadence= store.getCreateValueInt("CADENCE" + var_suffix);
  }
  
  @Override
  public void initSensor()
  {
    server_sensor= new at.univie.mma.serverinterface.sensors.BikeNrSS(14, 8);
    
    status.state= Sensor.State.STATE_CONNECTING;
    status.number= 0;
    status.text= "--";
    
    connect();
  }
  
  @Override
  protected void loadParams(SensorParam param)
  {
    super.loadParams(param);
    String offs= param.getParam(PARAM_OFFSET);
    if( offs != null )
      offset= Double.parseDouble(offs);
    else
      offset= OFFSET_NOT_INITIALISED;
    
    // TODO: fully implement this
    is_ctf= param.getParam(PARAM_IS_CTF,"false").equals("true");
    use_cad= param.getParam(PARAM_USE_CADENCE, "false").equals("true");
    disp_cad= param.getParam(PARAM_DISPLAY_CADENCE, "false").equals("true");
  }
  
  @Override
  public void startSensor()
  {
    Log.e(LOG_TAG, "start sending");
    is_running= true;
    server_sensor.start_sending();
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_1s");
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_3s");
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_30s");
    avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_1s", 1.0), 0, 500);
    avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_3s", 3.0), 0, 500);
    avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_30s", 30.0), 0, 500);
    // avgs.schedule(new Average(store.getList("POWER"), "POWER_30s", 30.0), 0, 1000);
    
    Settings.reportSetting(name+".offset", (int) offset);
  }
  
  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    
    if( avgs != null )
      avgs.cancel();
    
    super.stopSensor();
  }
  
  @Override
  public void destroy()
  {
    super.destroy();
    mResultReceiver= null;
    // BikeManager.Metrics.stopPowerAverage();
  }
  
  @Override
  protected void reset(int bike_device_id)
  {
    super.reset(bike_device_id);
    
    // in case we are here but should connect to a new sensor
    if( killed )
      return;
    
    if( !pairing )
    {
      if( bike_device_id != 0 )
        this.bike_device_id= bike_device_id;
    
      // TODO: search proximity
      AntPlusBikePowerPcc.requestAccess(ctx, bike_device_id, 10, mResultReceiver, base_IDeviceStateChangeReceiver);
    }
    else
    {
      AntPlusBikePowerPcc.requestAccess(ctx, 0, 10, mResultReceiverPairing, pairingDeviceState);
    }
  }
  
  AntPlusBikePowerPcc pairing_device;
  Timer pairing_timer;
  
  private final IDeviceStateChangeReceiver pairingDeviceState= new IDeviceStateChangeReceiver() {
    @Override
    public void onDeviceStateChange(final DeviceState newDeviceState)
    {
      if( Config.LOGGING )
        Log.d(LOG_TAG, "onDeviceStateChange: " + newDeviceState);
      switch( newDeviceState )
      {
        case CLOSED:
          pairing_device= null;
          break;
        
        case DEAD:
          pairing_device= null;
          break;
        
        case PROCESSING_REQUEST:
          break;
        
        case SEARCHING:
          pairing_device= null;
          break;
        
        case TRACKING:
          break;
        
        case UNRECOGNIZED:
          break;
      }
    }
  };
  
  IPluginAccessResultReceiver<AntPlusBikePowerPcc> mResultReceiverPairing= new IPluginAccessResultReceiver<AntPlusBikePowerPcc>() {
    private boolean has_cadence= false;
    private boolean has_speed= false;
    private boolean is_ctf;
    
    class Pairer extends TimerTask
    {
      @Override
      public void run()
      {
        ArrayList<Entry<String, String>> params= new ArrayList<Entry<String, String>>(3);
        params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_CADENCE, "" + has_cadence));
        params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_SPEED, "" + has_speed));
        params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_IS_CTF, "" + is_ctf));
        if( has_cadence )
        {
          List<ParameterQuestion> questions= new ArrayList<ParameterQuestion>(1);
          questions.add(cadenceq);
          onSensorPaired(name != null ? name : "Power-Meter", "" + pairing_device.getAntDeviceNumber(), questions, params);
        }
        else
          onSensorPairedParams(name != null ? name : "Power-Meter", "" + pairing_device.getAntDeviceNumber(), params);
      }
    }
    
    @Override
    public void onResultReceived(AntPlusBikePowerPcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      switch( resultCode )
      {
        case SUCCESS:
          internal_sensor= result;
          if( Config.LOGGING )
            Log.d(LOG_TAG, (result.getAntDeviceNumber() + ": " + initialDeviceState));
          
          // Ok we are connected. Now try to find out what kind of device this is
          
          result.subscribeInstantaneousCadenceEvent(new IInstantaneousCadenceReceiver() {
            @Override
            public void onNewInstantaneousCadence(long arg0, EnumSet<EventFlag> arg1, DataSource arg2, int arg3)
            {
              has_cadence= true;
            }
          });
          result.subscribeCalculatedWheelDistanceEvent(new AntPlusBikePowerPcc.CalculatedWheelDistanceReceiver (new BigDecimal(2100)) {
            @Override
            public void onNewCalculatedWheelDistance(long arg0, EnumSet<EventFlag> arg1, DataSource arg2, BigDecimal arg3)
            {
              has_speed= true;
            }
          });
          ((AntPlusBikePowerPcc) internal_sensor).subscribeRawCtfDataEvent(new IRawCtfDataReceiver() {
            @Override
            public void onNewRawCtfData(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long ctfUpdateEventCount,
                final BigDecimal instantaneousSlope, final BigDecimal accumulatedTimeStamp, final long accumulatedTorqueTicksStamp)
            {
              is_ctf= true;
              has_cadence= true;
            }
          });
          
          pairing_device= result;
          if( pairing_timer == null )
            pairing_timer= new Timer();
          pairing_timer.schedule(new Pairer(), 2000);
          
          break;
        case SEARCH_TIMEOUT:
          status.state= Sensor.State.STATE_CONNECTING;
          status.number= 0;
          status.text= "--";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "SEARCH TIMEOUT");
          onConnectFailed(ConnectionFailedReason.SEARCH_TIMEOUT);
        case CHANNEL_NOT_AVAILABLE:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Channel Not Available");
          reset(0);
          break;
        case OTHER_FAILURE:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "RequestAccess failed. See logcat for details.");
          break;
        case DEPENDENCY_NOT_INSTALLED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "ANT NOT INSTALLED");
          break;
        case USER_CANCELLED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Cancelled. Do Menu->Reset.");
          break;
        case UNRECOGNIZED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Failed: UNRECOGNIZED. Upgrade Required?");
          break;
        default:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Unrecognized result: " + resultCode);
          break;
      }
    }
  };
  
  IPluginAccessResultReceiver<AntPlusBikePowerPcc> mResultReceiver= new IPluginAccessResultReceiver<AntPlusBikePowerPcc>() {
    @Override
    public void onResultReceived(AntPlusBikePowerPcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      switch( resultCode )
      {
        case SUCCESS:
          internal_sensor= result;
          if( Config.LOGGING )
            Log.d(LOG_TAG, (result.getAntDeviceNumber() + ": " + initialDeviceState));
          status.state= Sensor.State.STATE_OK;
          status.number= 0;
          status.text= "OK";
          
//           Toast.makeText(ctx.getApplicationContext(), "Subscribe pwr", Toast.LENGTH_SHORT).show();
          onConnect();
          subscribeToEvents();
          
          break;
        case SEARCH_TIMEOUT:
          boolean yield= status.state != Sensor.State.STATE_OK;
//          Toast.makeText(ctx.getApplicationContext(), "SEARCH_TIMEOUT " + yield, Toast.LENGTH_SHORT).show();
          status.state= Sensor.State.STATE_CONNECTING;
          status.number= 0;
          status.text= "--";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "SEARCH TIMEOUT");
          if( yield )
            onConnectFailed(ConnectionFailedReason.SEARCH_TIMEOUT);
        case CHANNEL_NOT_AVAILABLE:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Channel Not Available");
          reset(0);
          break;
        case OTHER_FAILURE:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "RequestAccess failed. See logcat for details.");
          break;
        case DEPENDENCY_NOT_INSTALLED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "ANT NOT INSTALLED");
          break;
        case USER_CANCELLED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Cancelled. Do Menu->Reset.");
          break;
        case UNRECOGNIZED:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Failed: UNRECOGNIZED. Upgrade Required?");
          break;
        default:
          status.state= Sensor.State.STATE_NOT_CONNECTED;
          status.number= 0;
          status.text= "";
          if( Config.LOGGING )
            Log.d(LOG_TAG, "Unrecognized result: " + resultCode);
          break;
      }
    }
  };
  
  private void subscribeToEvents()
  {
    if( bike_device_id == 1478 )
    {
      ((AntPlusBikePowerPcc) internal_sensor).subscribeCalculatedPowerEvent(new ICalculatedPowerReceiver() {
        @Override
        public void onNewCalculatedPower(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
            final BigDecimal calculatedPower)
        {
          power_short= calculatedPower.shortValue();
          server_sensor.setPower(power_short);
          power.setIntValue((int) power_short);

          if( Config.LOGGING )
            Log.d("BikePower", "New Power: " + calculatedPower.doubleValue());
          
          // Toast.makeText(ctx.getApplicationContext(), "calcPower " + power_short, Toast.LENGTH_SHORT).show();
        }
      });
      
      /*((AntPlusBikePowerPcc) internal_sensor).subscribeCalculatedTorqueEvent(new ICalculatedTorqueReceiver() {
        @Override
        public void onNewCalculatedTorque(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
            final BigDecimal calculatedTorque)
        {
          
        }
      });*/
      
      ((AntPlusBikePowerPcc) internal_sensor).subscribeInstantaneousCadenceEvent(new IInstantaneousCadenceReceiver() {
        
        @Override
        public void onNewInstantaneousCadence(long estTimestamp, java.util.EnumSet<EventFlag> eventFlags, AntPlusBikePowerPcc.DataSource dataSource, int instantaneousCadence)
        {
          if( use_cad )
            server_sensor.setCadence(instantaneousCadence);
          if( disp_cad )
            cadence.setIntValue(instantaneousCadence);
        }
      });
      /*
        ((AntPlusBikePowerPcc) internal_sensor).subscribeCalculatedCrankCadenceEvent(new ICalculatedCrankCadenceReceiver() {
          @Override
          public void onNewCalculatedCrankCadence(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
              final BigDecimal calculatedCrankCadence)
          {
            Toast.makeText(ctx.getApplicationContext(), "Cadence", Toast.LENGTH_SHORT).show();
            if( use_cad )
              server_sensor.setCadence(calculatedCrankCadence.intValue());
            if( disp_cad )
              cadence.setIntValue(calculatedCrankCadence.intValue());
            
            if( Config.LOGGING )
              Log.d("BikePower", "New Cadence: " + calculatedCrankCadence.intValue());
          }
        });
        */
    }
    
    ((AntPlusBikePowerPcc) internal_sensor).subscribeRawCtfDataEvent(new IRawCtfDataReceiver() {
      private long last_ctfUpdateEventCount;
      private BigDecimal lastTime= new BigDecimal(0);
      private long last_accumulatedTorqueTicksStamp;

      @Override
      public void onNewRawCtfData(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long ctfUpdateEventCount,
          final BigDecimal instantaneousSlope, final BigDecimal accumulatedTimeStamp, final long accumulatedTorqueTicksStamp)
      {
        double event_diff= ctfUpdateEventCount - last_ctfUpdateEventCount;
        double time_diff= accumulatedTimeStamp.subtract(lastTime).doubleValue();
        long ticks= accumulatedTorqueTicksStamp - last_accumulatedTorqueTicksStamp;
        double elapsed_time= (time_diff * 2000) * 0.0005;
        
        // Log.e(LOG_TAG, "Time Stamp: " + accumulatedTimeStamp + " last: " + lastTime + " d: " + time_diff + " elapsed: " + elapsed_time);
        // Log.e(LOG_TAG, "Event: " + ctfUpdateEventCount + " last: " + last_ctfUpdateEventCount + " d: " + event_diff);
        // Log.e(LOG_TAG, "ticks: " + accumulatedTorqueTicksStamp + " d:" + ticks);
        
        // showToast("onNewRawCtf: " + elapsed_time);
        
        if( event_diff > 0 )
        {
          double cadence_period= (elapsed_time / event_diff);
          double cad = (60 / cadence_period);
          
          cadence.setIntValue((int) cad);
          
          double tf;
          double torque;
          if( ticks > 0 )
            tf= (1 / (elapsed_time / ticks)) - offset;
          else
            tf= 0;
          
          torque= (tf / (instantaneousSlope.doubleValue()));
          
          short power= (short) (torque * cad * Math.PI / 30);
          ANT_BikePower.this.power.setIntValue((int) power);
          // server_sensor.setData(speed, (long) bike_distance, (int) cad, power);
          
          if( use_cad )
            server_sensor.setCadencePower((int) cad, power);
          else
            server_sensor.setPower(power);
          
          Log.d(LOG_TAG, "ctfPower " + power);
        }
        
        last_ctfUpdateEventCount= ctfUpdateEventCount;
        lastTime= accumulatedTimeStamp.abs();
        last_accumulatedTorqueTicksStamp= accumulatedTorqueTicksStamp;
      }
    });
    
    subscribed_pwr= true;
    // BikeManager.Metrics.startPowerAverage();
    checkStatus();
  }
  
  private void checkStatus()
  {
    Log.d(LOG_TAG, "checkStatus rs:" + required_spd + " ss:" + subscribed_spd + " sp:" + subscribed_pwr);
    
    // Are we a CTF sensor?
    if( is_ctf )
    {
      // we can only be ok if we are connected and the offset is set
      if( offset == OFFSET_NOT_INITIALISED )
      {
        status.text= "Calibrate!";
      }
      else
      {
        status.state= Sensor.State.STATE_OK;
        status.number= 0;
        status.text= "OK";
      }
    }
    else
    {
      if( !subscribed_pwr )
      {
        status.state= Sensor.State.STATE_NOT_CONNECTED;
        status.text= "Not Implemented!";
      }
      else
      {
        status.state= Sensor.State.STATE_OK;
        status.number= 0;
        status.text= "OK";
      }
    }
  }
  
  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }
  
  @Override
  public void resetMetrics()
  {
    // nothing to do
  }
  
  @Override
  public void storeOffsetValue(double value)
  {
    offset= value;
    currentSensorParam().addParam(PARAM_OFFSET, "" + value);
    updateConfig();
    checkStatus();
    // TODO: Settings.reportSetting(name+".offset", (int) offset);
  }
  
  @Override
  public double getStoredOffsetValue()
  {
    return offset;
  }
  
  @Override
  public CalibrationDialog[] getCalibrationDialog()
  {
    if( is_ctf )
      return new CalibrationDialog[] {new PowerMeterCalibrationDialog(ctx, this, true)};
    else
      return new CalibrationDialog[] {new PowerMeterCalibrationDialog(ctx, this, false)};
  }

  @Override
  public boolean requestCalibration()
  {
    ((AntPlusBikePowerPcc) internal_sensor).requestManualCalibration(new IRequestFinishedReceiver() {
      
      @Override
      public void onNewRequestFinished(RequestStatus requestStatus)
      {
        switch( requestStatus )
        {
          case SUCCESS:
            Toast.makeText(ctx.getApplicationContext(), "Request Successfully Sent", Toast.LENGTH_SHORT).show();
            break;
          case FAIL_PLUGINS_SERVICE_VERSION:
            if( calibrationDialog != null )
              calibrationDialog.calibrationRequestFailed(PowerMeterCalibrationDialog.RequestFailedReason.ANT_SERVICE_FAILURE);
            // Toast.makeText(ctx.getApplicationContext(), "Plugin Service Upgrade Required?", Toast.LENGTH_SHORT).show();
            break;
          default:
            if( calibrationDialog != null )
              calibrationDialog.calibrationRequestFailed(PowerMeterCalibrationDialog.RequestFailedReason.CommunicationError);
            // Toast.makeText(ctx.getApplicationContext(), "Request Failed to be Sent", Toast.LENGTH_SHORT).show();
            break;
        }
      }
    }, new ICalibrationMessageReceiver() {
      @Override
      public void onNewCalibrationMessage(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
          final CalibrationMessage calibrationMessage)
      {
        Log.d(LOG_TAG, "t: " + estTimestamp + " f:" + eventFlags + " c:" + calibrationMessage);

        switch( calibrationMessage.calibrationId )
        {
          case GENERAL_CALIBRATION_FAIL:
            break;
          case GENERAL_CALIBRATION_SUCCESS:
            if( calibrationDialog != null )
              calibrationDialog.setCurrentOffset(calibrationMessage.calibrationData);
            Toast.makeText(ctx.getApplicationContext(), "GENERAL_CALIBRATION_SUCCESS " + calibrationMessage.calibrationData + " " + calibrationDialog, Toast.LENGTH_SHORT).show();
            break;
          
          case CUSTOM_CALIBRATION_RESPONSE:
          case CUSTOM_CALIBRATION_UPDATE_SUCCESS:
            /*String bytes= "";
            for(byte manufacturerByte : calibrationMessage.manufacturerSpecificData)
              bytes+= "[" + manufacturerByte + "]";*/
            break;
          
          case CTF_ZERO_OFFSET:
            if( calibrationDialog != null )
              calibrationDialog.setCurrentOffset(calibrationMessage.ctfOffset);
            // Toast.makeText(ctx.getApplicationContext(), "CTF_ZERO_OFFSET " + calibrationMessage.ctfOffset, Toast.LENGTH_SHORT).show();
            break;
          case UNRECOGNIZED:
            // TODO:
            // Toast.makeText(ANT_BikePower.this.ctx, "Failed: UNRECOGNIZED. PluginLib Upgrade
            // Required?", Toast.LENGTH_SHORT).show();
          default:
            break;
        }
      }
    }, new AntPlusBikePowerPcc.IMeasurementOutputDataReceiver() {
      @Override
      public void onNewMeasurementOutputData(long estTimestamp, java.util.EnumSet<EventFlag> eventFlags, int numOfDataTypes, int dataType,
          java.math.BigDecimal timeStamp, java.math.BigDecimal measurementValue)
      {
        
      }
    });
    
    return false;
  }

  @Override
  public void dialogClosed()
  {
    // inCalibration= false;
    calibrationDialog= null;
  }

  @Override
  public void onCalibrationDialogOpened(PowerMeterCalibrationDialog ref)
  {
    calibrationDialog= ref;
  }
}
