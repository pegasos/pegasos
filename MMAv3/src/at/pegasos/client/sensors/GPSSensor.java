package at.pegasos.client.sensors;

import android.*;
import android.annotation.*;
import android.content.*;
import android.content.pm.*;
import android.location.*;
import android.location.GpsStatus.*;
import android.os.*;
import android.support.v4.content.*;
import android.util.*;

import at.pegasos.client.values.*;
import at.pegasos.serverinterface.ServerSensor;
import at.univie.mma.Config;
import at.univie.mma.serverinterface.sensors.*;

import java.util.*;

public class GPSSensor extends Sensor implements LocationListener {
  private final ValueStore store;
  private final Status status;
  private at.univie.mma.serverinterface.sensors.GpsSS server_sensor;

  /**
   * Whether to use or not to use the speed provided by the GPS signal.
   * Not-Using in this context means not even bothering reporting it to the @class{ValueStore}
   */
  private boolean use_gps_speed = false;
  private boolean is_running = false;
  private boolean is_not_stopped = true;

  private boolean can_init = true;

  private final LocationManager mgr;

  private Listener onGpsStatusChange;
  private GnssStatus.Callback mGnssStatusListener;

  /**
   * Whether or not we have received a 'first fix' since the start / last disconnect.
   */
  private boolean have_fix = false;

  @SuppressLint("WrongConstant") // we supress this warning because this Context.LOCATION_SERVICE is listed as allowed constant ...
  public GPSSensor(Object starter)
  {
    super(starter);

    if( ContextCompat.checkSelfPermission((Context) starter, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED )
    {
      status = new Status();
      status.state = Sensor.State.STATE_NOT_CONNECTED;
      status.number = 0;
      status.text = "Err";

      can_init = false;
    }
    else
    {
      status = new Status();
      status.state = Sensor.State.STATE_NOT_CONNECTED;
      status.number = 0;
      status.text = "0 %";
    }

    mgr = (LocationManager) ((Context) starter).getSystemService(Context.LOCATION_SERVICE);

    store = ValueStore.getInstance();
  }

  @SuppressLint("MissingPermission") // we can do this since we check for the permission
  @Override public void initSensor()
  {
    // If we are pairing then we do not have to do something
    if( pairing )
      return;

    if( can_init )
    {
      server_sensor = new GpsSS(3, Config.DATASETS_PERPACKET_GPS);

      mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, Config.GPS_MIN_UPDATE_TIME, 0, this);

      if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N )
      {
        if( Config.LOGGING )
          Log.d(LOG_TAG, "Starting new API");
        mGnssStatusListener = new GnssStatus.Callback() {
          @Override public void onStarted()
          {
            if( Config.LOGGING )
              Log.d(LOG_TAG, "EVENT STARTED");
            status.state = Sensor.State.STATE_CONNECTING;
            status.number = 0;
          }

          @Override public void onStopped()
          {
            if( Config.LOGGING )
              Log.d(LOG_TAG, "Event stop");
            status.state = Sensor.State.STATE_NOT_CONNECTED;
            status.number = 0;
          }

          @Override public void onFirstFix(int ttffMillis)
          {
            if( Config.LOGGING )
              Log.d(LOG_TAG, "First fix");
            // Toast.makeText(GPSSensor.this.ctx, "First fix", Toast.LENGTH_LONG).show();
          }

          @SuppressLint("NewApi") @Override public void onSatelliteStatusChanged(GnssStatus gnssStatus)
          {
            int satelliteCount = gnssStatus.getSatelliteCount();

            if( Config.LOGGING )
              Log.d(LOG_TAG, " count1: " + satelliteCount);

            if( GPSSensor.this.status.state == Sensor.State.STATE_OK )
            {
              if( satelliteCount < Config.MIN_REQUIRED_SATELLITES_GPS )
              {
                GPSSensor.this.status.number = satelliteCount;
                GPSSensor.this.status.state = Sensor.State.STATE_CONNECTING;
                if( GPSSensor.this.status.number == 0 )
                  status.text = "0 %";
                else
                {
                  int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                  status.text = nr + " %";
                }
              }
            }
            else if( GPSSensor.this.status.state == Sensor.State.STATE_CONNECTING )
            {
              GPSSensor.this.status.number = satelliteCount;
              if( satelliteCount >= Config.MIN_REQUIRED_SATELLITES_GPS )
              {
                GPSSensor.this.status.state = Sensor.State.STATE_OK;
                status.text = "OK";
              }
              else
              {
                if( GPSSensor.this.status.number == 0 )
                  status.text = "0 %";
                else
                {
                  int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                  status.text = nr + " %";
                }
              }
            }
            else
            {
              GPSSensor.this.status.state = Sensor.State.STATE_CONNECTING;
              GPSSensor.this.status.number = satelliteCount;
              if( GPSSensor.this.status.number == 0 )
                status.text = "0 %";
              else
              {
                int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                status.text = nr + " %";
              }
            }
          }
        };
        mgr.registerGnssStatusCallback(mGnssStatusListener);
      }
      else
      {
        if( Config.LOGGING )
          Log.d(LOG_TAG, "Starting old API");
        onGpsStatusChange = new GpsStatus.Listener() {
          public void onGpsStatusChanged(int event)
          {
            switch( event )
            {
              case GpsStatus.GPS_EVENT_STARTED:
                if( Config.LOGGING )
                  Log.d(LOG_TAG, "EVENT STARTED");
                status.state = Sensor.State.STATE_CONNECTING;
                status.number = 0;
                // Started...
                break;

              case GpsStatus.GPS_EVENT_FIRST_FIX:
                if( Config.LOGGING )
                  Log.d(LOG_TAG, "First fix");
                have_fix = true;
                //main.say_it(1,"GPS connected");
                //gps_text.setTextColor(Color.GREEN);
                break;

              case GpsStatus.GPS_EVENT_STOPPED:
                if( Config.LOGGING )
                  Log.d(LOG_TAG, "Event stop");
                status.state = Sensor.State.STATE_NOT_CONNECTED;
                status.number = 0;
                have_fix = false;
                //main.log_e("GPS","EVENT_STOPPED");
                // Stopped...
                break;

              case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                int count = 0, count1 = 0;
                GpsStatus xGpsStatus = mgr.getGpsStatus(null);
                Iterable<GpsSatellite> iSatellites = xGpsStatus.getSatellites();
                Iterator<GpsSatellite> it = iSatellites.iterator();
                //gps_signal=0;
                while( it.hasNext() )
                {
                  count++;
                  GpsSatellite oSat = (GpsSatellite) it.next();
                  //main.log_e("GPS","LocationActivity - onGpsStatusChange: Satellite "+oSat.getPrn()+": " +oSat.getSnr() ) ;
                  if( oSat.usedInFix() )
                  {
                    count1++;
                    //gps_signal= gps_signal+oSat.getSnr();
                  }
                }

                if( count1 == 0 )
                {
                  count1 = 1;
                }

                if( Config.LOGGING )
                  Log.d(LOG_TAG, "count: " + count + " count1: " + count1);

                if( status.state == Sensor.State.STATE_OK )
                {
                  if( count1 < Config.MIN_REQUIRED_SATELLITES_GPS )
                  {
                    status.number = count1;
                    status.state = Sensor.State.STATE_CONNECTING;
                    if( status.number == 0 )
                      status.text = "0 %";
                    else
                    {
                      int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                      status.text = nr + " %";
                    }
                  }
                }
                else if( status.state == Sensor.State.STATE_CONNECTING )
                {
                  status.number = count1;
                  if( count1 >= Config.MIN_REQUIRED_SATELLITES_GPS && have_fix )
                  {
                    status.state = Sensor.State.STATE_OK;
                    status.text = "OK";
                  }
                  else
                  {
                    if( status.number == 0 )
                      status.text = "0 %";
                    else
                    {
                      int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                      if( nr < 100 )
                        status.text = nr + " %";
                      else
                        status.text = "99 %"; // 99 since we do not have a fix.
                    }
                  }
                }
                else
                {
                  status.state = Sensor.State.STATE_CONNECTING;
                  status.number = count1;
                  if( status.number == 0 )
                    status.text = "0 %";
                  else
                  {
                    int nr = (int) ((status.number / (double) Config.MIN_REQUIRED_SATELLITES_GPS) * 100);
                    status.text = nr + " %";
                  }
                }
                break;
            }
          }
        };
        mgr.addGpsStatusListener(onGpsStatusChange);
        if( Config.LOGGING )
          Log.e(LOG_TAG, "STARTED");
      }

      status.state = Sensor.State.STATE_CONNECTING;
      status.number = 0;
    }
    else
    {
      if( ContextCompat.checkSelfPermission((Context) ctx, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED )
      {
        can_init = true;
        initSensor();
      }
    }
  }

  @Override
  public void startSensor()
  {
    Log.d("GPS", "Start Sensor");
    is_running = true;
    use_gps_speed = !SensorControllerService.getInstance().isUsingFootPod();
    server_sensor.start_sending();
  }

  @Override
  public void stopSensor()
  {
    Log.d("GPS", "Stop Sensor");
    mgr.removeUpdates(this);
    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N )
    {
      mgr.unregisterGnssStatusCallback(mGnssStatusListener);
    }
    else
    {
      mgr.removeGpsStatusListener(onGpsStatusChange);
    }
    is_running = false;
    is_not_stopped = false;
  }

  public void onLocationChanged(Location location)
  {
    // if( Config.LOGGING ) Log.d(LOG_TAG, "onLocationChanged " + location.getLatitude() + " " + location.getLongitude() + " " + location.getAccuracy());
    if( this.is_running )
    {
      // LAT=5 Byte,LON=5 Byte,ALT=2 Byte, SPEED=2 Byte, ACCUR=2 Byte
      short alt = (short) location.getAltitude();
      short speed = (short) (location.getSpeed() * 100);
      short accur = (short) (location.getAccuracy() * 100);
      short bearing = (short) location.getBearing();

      //set Location for value store
      store.setLocation(location.getLatitude(), location.getLongitude(), this);
      store.setBearing(bearing, this);
      store.setAltitude(alt, this);
      store.setAccuracy(accur, this);
      
      /*
      long time= System.currentTimeMillis();
      distance+= location.getSpeed() * (time - last_time);
      last_time= time;
      */

      if( this.use_gps_speed )
        store.setCurrentSpeed(location.getSpeed() * 1000, this);
      //add data packet
      server_sensor.setData(location.getLatitude(), location.getLongitude(), alt, speed, accur, bearing);
      // server_sensor.setData(alt, speed, accur, bearing, lat, lon);
    }
  }

  /**
   * Required interface method.
   * Sourcecode mainly written by @author martin tampier
   */
  @SuppressLint("MissingPermission")
  public void onProviderDisabled(String arg0)
  {
    if( is_not_stopped )
      mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, Config.GPS_MIN_UPDATE_TIME, 0, this);
  }

  /**
   * Required interface method.
   * Sourcecode mainly written by @author martin tampier
   */
  @SuppressLint("MissingPermission")
  public void onProviderEnabled(String arg0)
  {
    Log.d("GPS", "onProviderEnabled: " + arg0);
    mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, Config.GPS_MIN_UPDATE_TIME, 0, this);
  }

  /**
   * Required interface method.
   * Sourcecode mainly written by @author martin tampier
   */
  public void onStatusChanged(String arg0, int arg1, Bundle arg2)
  {
    Log.d(LOG_TAG, "onStatusChanged: " + arg0 + " " + arg1 + " " + arg2);
  }

  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }

  @Override
  public void resetMetrics()
  {
  }

  @Override
  protected void connect()
  {
  }
}
