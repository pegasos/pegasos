package at.pegasos.client.sensors;

import android.content.Context;
import android.util.Log;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.CalculatedAccumulatedDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;

import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;

import at.univie.mma.Config;
import at.univie.mma.R;
import at.pegasos.serverinterface.ServerSensor;
import at.univie.mma.serverinterface.sensors.BikeNrSS;
import at.pegasos.client.ui.ParameterQuestion;
import at.pegasos.client.values.Value;
import at.pegasos.client.values.ValueStore;

import static com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult.SUCCESS;

public class ANT_BikeSpeedCadence extends AntPccSensor {
  
  /**
   * This question will be asked when the Sensor is paired
   */
  private static final ParameterQuestion wheelsizeq= new ParameterQuestion.IntQuestion("wheelsize", R.string.wheelsize);
  private static final List<ParameterQuestion> paramql;
  
  static
  {
    paramql= new ArrayList<ParameterQuestion>(1);
    paramql.add(wheelsizeq);
  }

  private static final String PARAM_COMBINED= "combined";
  private static final String PARAM_CADONLY= "cadonly";
  private static final String PARAM_SPDONLY= "spdonly";

  private final static BigDecimal TAUSAND= new BigDecimal(1000);
  
  private at.univie.mma.serverinterface.sensors.BikeNrSS server_sensor;
  private ValueStore store;
  
  private int device_id;
  
  private long wheelSize;

  private boolean is_combined;
  private boolean is_spdonly;
  private boolean is_cadonly;

  private double api_speed_mm_s;
  private double api_distance_m;
  private long api_dist_offset= 0;

  protected Value cadence;
  
  public ANT_BikeSpeedCadence(Object ctx)
  {
    super((Context) ctx);
    
    LOG_TAG= "BikeSpeedOnly";
    
    // fp_buffer= new int[5];
    
    status= new Status();
    status.state= Sensor.State.STATE_NOT_CONNECTED;
    status.number= 0;
    status.text= "";
    
    store= ValueStore.getInstance();
  }

  @Override
  protected void loadParams(SensorParam param)
  {
    super.loadParams(param);

    is_combined= param.getParam(PARAM_COMBINED,"false").equals("true");
    is_spdonly= param.getParam(PARAM_SPDONLY,"false").equals("true");
    is_cadonly= param.getParam(PARAM_CADONLY,"false").equals("true");

    String ws= currentSensorParam().getParam("wheelsize");
    if( ws != null )
      wheelSize= Long.parseLong(ws);
  }
  
  @Override
  public void initSensor()
  {
    server_sensor= new BikeNrSS(3, 4);
    
    status.state= Sensor.State.STATE_CONNECTING;
    status.number= 0;
    status.text= "--";
    
    connect();
  }
  
  @Override
  public void startSensor()
  {
    Log.e(LOG_TAG, "start sending");
    // is_running= true;
    server_sensor.start_sending();
    
    resetMetrics();
  }
  
  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    super.stopSensor();
  }
  
  @Override
  public void destroy()
  {
    super.destroy();
    
    base_Spd= null;
  }

  @Override
  public void reset(final int serial)
  {
    super.reset(serial);
    
    if( !killed )
    {
      Log.d(LOG_TAG, "reset " + serial + " " + is_combined);
      if( pairing )
      {
        AntPlusBikeCadencePcc.requestAccess(ctx, serial, 0, false, base_Cad, base_IDeviceStateChangeReceiver);
        AntPlusBikeSpeedDistancePcc.requestAccess(ctx, serial, 0, false, base_Spd, base_IDeviceStateChangeReceiver);
      }
      else
      {
        /*
        this here is more complicated than necessary.
        However, I hope it keeps things more clear.
        If a sensor is is spd only it will pair using the AntPlusBikeSpeedDistancePcc access.
        For combined sensors it will report as AntPlusBikeCadencePcc with is_combined = true
        cadence only sensors will report as AntPlusBikeCadencePcc with is_combined = false
         */
        if( !is_combined )
        {
          if( is_spdonly )
          {
            AntPlusBikeSpeedDistancePcc.requestAccess(ctx, serial, 0, is_combined, base_Spd, base_IDeviceStateChangeReceiver);
          }
          if( is_cadonly )
          {
            cadence= store.getCreateNumberValueListening("CADENCE");
            AntPlusBikeCadencePcc.requestAccess(ctx, serial, 0, is_combined, base_Cad, base_IDeviceStateChangeReceiver);
          }
        }
        else
        {
          cadence= store.getCreateNumberValueListening("CADENCE");
          AntPlusBikeCadencePcc.requestAccess(ctx, serial, 0, is_combined, base_Cad, base_IDeviceStateChangeReceiver);
        }
      }
    }
  }
  
  public void subscribeToEventsSpd()
  {
    BigDecimal con= new BigDecimal(wheelSize);
    ((AntPlusBikeSpeedDistancePcc) internal_sensor).subscribeCalculatedSpeedEvent(new CalculatedSpeedReceiver(con) {
      
      @Override
      public void onNewCalculatedSpeed(long estTimestamp, EnumSet<EventFlag> eventFlags, BigDecimal calculatedSpeed)
      {
        api_speed_mm_s= calculatedSpeed.doubleValue();
        store.setCurrentSpeed(api_speed_mm_s, ANT_BikeSpeedCadence.this);
      }
    });
    
    ((AntPlusBikeSpeedDistancePcc) internal_sensor)
        .subscribeCalculatedAccumulatedDistanceEvent(new CalculatedAccumulatedDistanceReceiver(con) {
          
          @Override
          public void onNewCalculatedAccumulatedDistance(long estTimestamp, java.util.EnumSet<EventFlag> eventFlags,
              java.math.BigDecimal calculatedAccumulatedDistance)
          {
            api_distance_m= calculatedAccumulatedDistance.divide(TAUSAND).longValue() - api_dist_offset;
            server_sensor.setSpeed_mm_sDistance_m(api_speed_mm_s, (long) api_distance_m);
            store.setDistance((long) api_distance_m, ANT_BikeSpeedCadence.this);
          }
        });
  } // END SUBSCRIBE TO EVENTS
  
  protected IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> base_Spd= new IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
    @Override
    public void onResultReceived(AntPlusBikeSpeedDistancePcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      if( resultCode == SUCCESS )
      {
        internal_sensor= result;
        if( Config.LOGGING )
          Log.d(LOG_TAG, (result.getAntDeviceNumber() + ": " + initialDeviceState));
        // main.say_it(1,"FootPod connected");
        status.state= Sensor.State.STATE_OK;
        status.number= 0;
        status.text= "OK";
        // subscribeToEventsCad();
        // set_fp_device_id(result.getAntDeviceNumber());
        if( pairing )
        {
          // this needs to be done since onSensorPaired uses
          // currenSensorConfig() which uses bikesc_device_id
          device_id= result.getAntDeviceNumber();

          is_combined= result.isSpeedAndCadenceCombinedSensor();

          ArrayList<Entry<String, String>> params= new ArrayList<Entry<String, String>>(2);
          params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_COMBINED, "" + is_combined));
          params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_SPDONLY, "" + !is_combined));

          // Ask user whether pairing should be completed
          if( !is_combined )
          {
            onSensorPaired("Ant Bike Speed", "" + device_id, paramql, params);
          }
          else
          {
            onSensorPaired("Ant Bike Speed & Cadence", "" + device_id, paramql, params);
          }
        }
        else
        {
          subscribeToEventsSpd();
          onConnect();
        }
      }
      else
      {
        handleResultCodes(resultCode, initialDeviceState);
      }
    }
  };

  protected IPluginAccessResultReceiver<AntPlusBikeCadencePcc> base_Cad= new IPluginAccessResultReceiver<AntPlusBikeCadencePcc>() {
    @Override
    public void onResultReceived(AntPlusBikeCadencePcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      if( resultCode == SUCCESS )
      {
        internal_sensor= result;
        if( Config.LOGGING )
          Log.d(LOG_TAG, (result.getAntDeviceNumber() + ": " + initialDeviceState));

        if( pairing )
        {
          // this needs to be done since onSensorPaired uses
          // currenSensorConfig() which uses bikesc_device_id
          device_id= result.getAntDeviceNumber();

          is_combined= result.isSpeedAndCadenceCombinedSensor();

          ArrayList<Entry<String, String>> params= new ArrayList<Entry<String, String>>(2);
          params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_COMBINED, "" + is_combined));
          params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_CADONLY, "" + !is_combined));

          // Ask user whether pairing should be completed
          if( !is_combined )
          {
            onSensorPairedParams("Ant Bike Cadence", "" + device_id, params);
          }
          else
          {
            onSensorPaired("Ant Bike Speed & Cadence", "" + device_id, paramql, params);
          }
        }
        else
        {
          // subscribeToEventsSpd();
          ((AntPlusBikeCadencePcc) internal_sensor).subscribeCalculatedCadenceEvent(new AntPlusBikeCadencePcc.ICalculatedCadenceReceiver() {
            @Override
                    public void onNewCalculatedCadence(long estTimestamp,
                        java.util.EnumSet<EventFlag> eventFlags,
                        java.math.BigDecimal calculatedCadence)
            {
              // Log.d(LOG_TAG, "cad " + calculatedCadence.intValue());
              cadence.setIntValue(calculatedCadence.intValue());
              server_sensor.setCadence(calculatedCadence.intValue());
            }
          });

          if( is_combined )
          {
            AntPlusBikeSpeedDistancePcc.requestAccess(ctx, result.getAntDeviceNumber(), 0, is_combined, base_Spd, base_IDeviceStateChangeReceiver);
          }
          else
          {
            status.state= Sensor.State.STATE_OK;
            status.number= 0;
            status.text= "OK";

            onConnect();
          }
        }
      }
      else
      {
        handleResultCodes(resultCode, initialDeviceState);
      }
    }
  };


  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }
  
  @Override
  public void resetMetrics()
  {
    api_dist_offset= (long) api_distance_m;
  }
}
