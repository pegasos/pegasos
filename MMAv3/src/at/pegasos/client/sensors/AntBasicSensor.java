package at.pegasos.client.sensors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;

import com.dsi.ant.AntService;
import com.dsi.ant.channel.AntChannel;
import com.dsi.ant.channel.AntChannelProvider;
import com.dsi.ant.channel.AntCommandFailedException;
import com.dsi.ant.channel.ChannelNotAvailableException;
import com.dsi.ant.channel.IAntChannelEventHandler;
import com.dsi.ant.channel.PredefinedNetwork;
import com.dsi.ant.message.ChannelId;
import com.dsi.ant.message.ChannelType;
import com.dsi.ant.message.fromant.MessageFromAntType;
import com.dsi.ant.message.ipc.AntMessageParcel;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import at.pegasos.client.util.PegasosLog;
import at.pegasos.client.sensors.*;
import at.pegasos.client.ui.ParameterQuestion;

public abstract class AntBasicSensor extends ANTSensor implements ServiceConnection {
  
  protected int PERIOD= 0;
  
  private AntService mBoundService;
  protected AntChannel channel;
  protected boolean killed= false;
  
  private boolean registered= false;
  
  protected int serial;
  
  protected int channel_number= 0;
  
  protected boolean channel_id_requested= false;
  // protected boolean channel_id_recieved= false;
  
  protected String SENSOR_NAME;
  
  protected Set<Integer> allowed_device_types;
  
  protected int device_type;
  
  private List<Integer> excluded_devices= new ArrayList<Integer>(3);

  protected final Context ctx;

  /**
   * Whether or not we have reported that we have established a connection to the sensor
   */
  private boolean connect_reported= false;

  /**
   * Whether a channel acquire has failed before
   */
  private boolean channel_acquire_failed= false;

  /**
   * How many times we have timed out during pairing
   */
  private int timeout= 0;

  /**
   * Time when last time a data message was received.
   */
  private long last_data;

  private long start_time;
  
  public AntBasicSensor(Context ctx)
  {
    super(ctx);
    this.ctx= ctx;
  }

  @Override
  public void stopSensor()
  {
    killed= true;
    destroy();
  }
  
  public void destroy()
  {
    if( channel != null )
    {
      try
      {
        channel.close();
      }
      catch( RemoteException e )
      {
        e.printStackTrace();
      }
      catch( AntCommandFailedException e )
      {
        e.printStackTrace();
      }
    }
    
    if( registered )
    {
      ctx.unbindService(this);
      ctx.unregisterReceiver(receiver);
      registered= false;
    }
  }
  
  @Override
  public void initSensor()
  {
    status.state= Sensor.State.STATE_CONNECTING;
    status.number= 0;
    status.text= "--";
    
    killed= false;
    channel_id_requested= false;
    // channel_id_recieved= false;

    channel_acquire_failed= false;
    
    IntentFilter filter= new IntentFilter(AntChannelProvider.ACTION_CHANNEL_PROVIDER_STATE_CHANGED);
    ctx.registerReceiver(receiver, filter);
    
    // TODO: do not start service if !pairing and no sensor paired? ie do not start if not necessary
    AntService.bindService(ctx, this);
    
    start_time= System.currentTimeMillis();
  }
  
  @Override
  protected void connect()
  {
    if( !pairing )
    {
      SensorParam p= currentSensorParam();
      if( p != null )
      {
        loadParams(p);
        connect_reported= false;
        // Toast.makeText(ctx.getApplicationContext(), LOG_TAG + " connect to " + serial + " (no pairing)", Toast.LENGTH_SHORT).show();
      }
    }
    else
    {
      // Toast.makeText(ctx.getApplicationContext(), LOG_TAG + " pairing", Toast.LENGTH_SHORT).show();
      serial= 0;
      setPairing(true);
    }
  }
  
  /**
   * Load the configuration of the current SensorParam into this sensor. <BR/>
   * <BR/>
   * Overwrite this method to react to new configurations during `connect` events. BUT: do not
   * forget to call this method (i.e. call <code>super.loadParams(sparam)</code> in your method)
   * 
   * @param sparam
   *          new sensor param
   */
  protected void loadParams(SensorParam sparam)
  {
    String sserial= sparam.getSensorId();
    if( sserial.equals("") )
      // TODO:
      // serial= NO_SENSOR_ID_SPECIFIED;
      serial= -1;
    else
      serial= Integer.parseInt(sserial);
  }
  
  @Override
  public void onServiceConnected(ComponentName name, IBinder service)
  {
    // if( name.getClassName())
    if( name.getClassName().equals("com.dsi.ant.service.AntRadioService") )
    {
      Log.d(LOG_TAG, "Service bound killed:" + killed);
      mBoundService= new AntService(service);
      connect();
      registered= true;
    }
  }
  
  @Override
  public void onServiceDisconnected(ComponentName name)
  {
    Log.e(LOG_TAG, "Service Connection lost");
    mBoundService= null;
  }
  
  private BroadcastReceiver receiver= new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      // if( LOG_TAG != "AntBikePowerNew")
      //  Toast.makeText(ctx, LOG_TAG + ":On receive " + intent.getAction(), Toast.LENGTH_SHORT).show();
      
      if( intent.getAction().equals(AntChannelProvider.ACTION_CHANNEL_PROVIDER_STATE_CHANGED) )
      {
        if( channel == null )
        {
          boolean nc= intent.getBooleanExtra(AntChannelProvider.NEW_CHANNELS_AVAILABLE, false);
          int nu= intent.getIntExtra(AntChannelProvider.NUM_CHANNELS_AVAILABLE, -1);
          
          Log.d(LOG_TAG, "ChannelProvider State changed " + nu + " " + nc);
          // Toast.makeText(ctx, LOG_TAG + ":ChannelProvider State changed " + nu + " " + nc, Toast.LENGTH_SHORT).show();
          
          if( ( nu > 0 ) && !killed &&
               // keep out of getting a new channel when we had problems before and its only pairing
               !( pairing && channel_acquire_failed ) )
          {
            try
            {
              channel= mBoundService.getChannelProvider().acquireChannel(ctx, PredefinedNetwork.ANT_PLUS);
              
              onChannelAvailable();
            }
            catch( RemoteException e )
            {
              e.printStackTrace();
              channel= null;
              channel_acquire_failed= true;
            }
            catch( ChannelNotAvailableException e )
            {
              e.printStackTrace();
              channel= null;
              channel_acquire_failed= true;
              pairing_possible= false;
            }
          }
        }
      }
    }
  };
  
  protected boolean pairing_possible= false;
  
  private Timer pairing_timer;

  private boolean channel_action_running;

  protected ArrayList<Entry<String, String>> pairing_params= new ArrayList<Entry<String, String>>();

  protected List<ParameterQuestion> pairing_questions= new ArrayList<ParameterQuestion>();

  private class PairingTimeout extends TimerTask {
    
    @Override
    public void run()
    {
      if( !killed )
      {
        Log.d(LOG_TAG, "Pairing Timeout. Serial: " + serial);
        if( serial != 0 )
        {
          Log.d(LOG_TAG, "Exclude device: " + serial);
          // This is not the device we want --> exclude it
          excluded_devices.add(serial);
        }
        timeout++;
        nextPairingTry();
      }
    }
  }

  private void acquireChannel()
  {
    while( channel_action_running )
      Thread.yield();
  }
  
  private void onChannelAvailable()
  {
    Log.d(LOG_TAG, "onChannelAvailable pairing:"+pairing);
    try
    {
      if( pairing && timeout < 3 )
      {
        onPairingStart();

        // Toast.makeText(ctx, "On Channel Available", Toast.LENGTH_SHORT).show();
        // ((MMAActivity) ctx).showToast(LOG_TAG + " On Channel Available");
        
        acquireChannel();
        channel_action_running= true;
        channel.setChannelEventHandler(new IAntChannelEventHandler() {
          @Override
          public void onReceiveMessage(MessageFromAntType type, AntMessageParcel message)
          {
            Log.d(LOG_TAG, type + " " + message.getMessageContentString());
            switch( type )
            {
              case ACKNOWLEDGED_DATA:
              case BURST_TRANSFER_DATA:
                break;
              case BROADCAST_DATA:
                // Although it seems impossible that this happens. There are
                // cases where channel is null and a nullpointerexception
                // happens. In this cases we ignore it and wait for other data
                if( channel != null )
                {
                  broadcastDataPairing(message);
                  if( !channel_id_requested )
                  {
                    if( channel != null )
                    {
                      try
                      {
                        channel.requestChannelId();
                        channel_id_requested= true;
                      }
                      catch( RemoteException e )
                      {
                        e.printStackTrace();
                      }
                      catch( AntCommandFailedException e )
                      {
                        e.printStackTrace();
                      }
                    }
                  }
                }
                break;
              
              case ANT_VERSION:
                break;
              case CAPABILITIES:
                break;
              case CHANNEL_EVENT:
                Log.e(LOG_TAG, message.getMessageContentString() + " " +
                        (message.getMessageContent()[1] == 0x01) + " " + (message.getMessageContent()[2] == 0x02));
                if( message.getMessageContent()[1] == 0x01 ) // ???
                {
                  if( message.getMessageContent()[2] == 0x07 ) // ??? Dead?, Connecting?
                  {
                    status.state= State.STATE_NOT_CONNECTED;
                    try
                    {
                      if( channel != null )
                        channel.close();
                      channel= null;
                      onConnectFailed(ConnectionFailedReason.ANT_CONNECTION_LOST);
                    }
                    catch( RemoteException e )
                    {
                      e.printStackTrace();
                    }
                    catch( AntCommandFailedException e )
                    {
                      e.printStackTrace();
                    }
                  }
                  else if( message.getMessageContent()[2] == 0x02 || message.getMessageContent()[2] == 0x08 )
                  {
                    // ((MMAActivity) ctx).showToast(LOG_TAG + " timeout?");
                    timeout++;
                    try
                    {
                      channel.close();
                    }
                    catch( RemoteException e )
                    {
                      e.printStackTrace();
                    }
                    catch( AntCommandFailedException e )
                    {
                      e.printStackTrace();
                    }
                    finally
                    {
                      channel.release();
                      channel= null;
                    }
                  }
                }
                else
                {
                  /*((MMAActivity) ctx).showToast(LOG_TAG + " Channel Event " + String.format("%02X", message.getMessageContent()[1]) + " "
                          + String.format("%02X", message.getMessageContent()[2]));*/
                }
                break;
              case CHANNEL_ID:
                // We have received a channel ID notification
                // channel_number= message.getMessageContent()[0];
                
                serial= ((int) message.getMessageContent()[1] & 0xFF) | (((int) message.getMessageContent()[2] & 0xFF) << 8);
                device_type= (int) message.getMessageContent()[3] & 0xFF;
                
                Toast.makeText(ctx.getApplicationContext(), LOG_TAG + ": Channel ID " + serial, Toast.LENGTH_SHORT).show();
                
                /*if( serial == 113376 )
                {
                  excluded_devices.add(serial);
                  nextPairingTry();
                }*/
                
                break;
              
              case CHANNEL_RESPONSE:
                if( message.getMessageContent()[1] == 0x4B ) // Channel open
                {
                
                }
                break;
              case CHANNEL_STATUS:
                break;
              case OTHER:
                break;
              case SERIAL_NUMBER:
                break;
              default:
                break;
              
            }
            
            if( pairing_possible && serial != 0 )
            {
              Log.d(LOG_TAG, "Pairing possible serial: " + serial + " device_type:" + device_type + " allowed:" + allowed_device_types + (allowed_device_types != null ? allowed_device_types.toString() : "") + " excluded:" + excluded_devices.toString());
              if( allowed_device_types == null || allowed_device_types.contains(device_type) )
              {
                Log.d(LOG_TAG, "Pair: " + serial);
                // In case the user answers "No". Device will be excluded from search
                excluded_devices.add(serial);
                onSensorPaired(SENSOR_NAME + LOG_TAG, "" + serial, pairing_questions, pairing_params);
                // onSensorPaired(SENSOR_NAME + LOG_TAG, "" + serial);
              }
              else
              {
                Log.d(LOG_TAG, "Exclude device: " + serial);
                // This is not the device we want --> exclude it
                excluded_devices.add(serial);
              }
              
              nextPairingTry();
            }
          }
          
          @Override
          public void onChannelDeath()
          {
            Log.d(LOG_TAG, "onChannelDeath");
            if( !killed )
              nextPairingTry();
          }
        });
        
        channel.assign(ChannelType.BIDIRECTIONAL_SLAVE);
        channel.setChannelId(new ChannelId(0, ChannelId.ANY_DEVICE_TYPE, ChannelId.ANY_TRANSMISSION_TYPE, false));
        if( excluded_devices.size() > 0 )
        {
          Log.d(LOG_TAG, "Pairing again");
          channel.configureInclusionExclusionList(Math.min(3, excluded_devices.size()), true);
          for(int i= 0; i < Math.min(3, excluded_devices.size()); i++)
          {
            channel.addIdToInclusionExclusionList(
                new ChannelId(excluded_devices.get(i), ChannelId.ANY_DEVICE_TYPE, ChannelId.ANY_TRANSMISSION_TYPE, false), i);
          }
          
          /*
           * channel.configureInclusionExclusionList(excluded_devices.size(), true); for(int i= 0; i
           * < Math.min(3, excluded_devices.size()); i++) {
           * channel.addIdToInclusionExclusionList(new ChannelId(excluded_devices.get(i),
           * ChannelId.ANY_DEVICE_TYPE, ChannelId.ANY_TRANSMISSION_TYPE, false), i); }
           */
        }
        channel.setRfFrequency(57);
        channel.setPeriod(PERIOD);
        channel.open();

        Log.d(LOG_TAG, "Starting pairing timer");
        if( pairing_timer == null )
          pairing_timer= new Timer();
        pairing_timer.scheduleAtFixedRate(new PairingTimeout(), 10000, 10000);
        Log.d(LOG_TAG, "Starting pairing timer done ");

        channel_action_running= false;
      }
      else if( !pairing )
      {
        channel.setChannelEventHandler(new IAntChannelEventHandler() {
          @Override
          public void onReceiveMessage(MessageFromAntType type, AntMessageParcel message)
          {
            // TODO: remove this debug output
            Log.d(LOG_TAG, type + " " + message.getMessageContentString());
            switch( type )
            {
              case ACKNOWLEDGED_DATA:
                // Toast.makeText(ctx, "ACKNOWLEDGED_DATA", Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG, "ACKNOWLEDGED DATA");
                break;
              case ANT_VERSION:
                break;
              case BROADCAST_DATA:
                last_data= System.currentTimeMillis();
                if( !connect_reported )
                {
                  onConnect();
                  connect_reported= true;
                }
                broadcastData(message);
                break;
              case BURST_TRANSFER_DATA:
                Log.e(LOG_TAG, "Burst Transfer Data");
                // Toast.makeText(ctx, "BURST_TRANSFER_DATA", Toast.LENGTH_SHORT).show();
                break;
              case CAPABILITIES:
                break;
              case CHANNEL_EVENT:
                Log.e(LOG_TAG, message.getMessageContentString());
                if( message.getMessageContent()[2] == 0x01 ) // EVENT_RX_SEARCH_TIMEOUT
                {
                  status.state= State.STATE_NOT_CONNECTED;
                  status.text= "";
  
                  acquireChannel();
                  channel_action_running= true;
                  channel.release();
                  channel= null;
                  channel_action_running= false;
                  onConnectFailed(ConnectionFailedReason.SEARCH_TIMEOUT);
                }
                else if( message.getMessageContent()[2] == 0x02 ) // EVENT_RX_FAILED
                {
                  // if( System.currentTimeMillis() - start_time > 4000 )
                  //  Toast.makeText(ctx.getApplicationContext(), "Channel rx failed", Toast.LENGTH_SHORT).show();
                  // Don't report this directly on ui.
                  /*
                   * A receive channel missed a message which it was expecting. This would happen
                   * when a receiver is tracking a transmitter and is expecting a message at the set
                   * message rate.
                   */
                  /*
                   * status.state= State.STATE_NOT_CONNECTED; try { channel.close(); channel= null;
                   * onConnectFailed(ConnectionFailedReason.ANT_CONNECTION_LOST); } catch
                   * (RemoteException | AntCommandFailedException e) { e.printStackTrace(); }
                   */
                }
                else if(message.getMessageContent()[2] == 0x03 ) // EVENT_TX
                {
                  /*
                   * A Broadcast message has been transmitted successfully. This event should be
                   * used to send the next message for transmission to the ANT device if the node is
                   * setup as a transmitter.
                   */
                }
                else if(message.getMessageContent()[2] == 0x04 ) // event_transfer_rx_failed
                {
                  Toast.makeText(ctx.getApplicationContext(), "Channel transfer rx failed", Toast.LENGTH_SHORT).show();
                  /*
                  * A receive transfer has failed. This occurs when a Burst Transfer Message was incorrectly received.
                  */
                }
                else if(message.getMessageContent()[2] == 0x05 ) // event_transfer_tx_completed
                {
                  /*
                   * An Acknowledged Data message or a Burst Transfer sequence has been completed successfully. When transmitting Acknowledged Data or Burst Transfer, there is no EVENT_TX message.
                   */
                }
                else if(message.getMessageContent()[2] == 0x06 ) // event_transfer_tx_failed
                {
                  Toast.makeText(ctx.getApplicationContext(), "Channel transfer tx failed", Toast.LENGTH_SHORT).show();
                  /*
                   * An Acknowledged Data message or a Burst Transfer Message has been initiated and the transmission has failed to complete successfully
                   */
                }
                else if( message.getMessageContent()[2] == 0x07 && !killed ) // EVENT_CHANNEL_CLOSED
                {
                  /**
                   * The channel has been successfully closed. When the Host sends a message to
                   * close a channel, it first receives a RESPONSE_NO_ERROR to indicate that the
                   * message was successfully received by ANT. This event is the actual indication
                   * of the closure of the channel. So, the Host must use this event message instead
                   * of the RESPONSE_NO_ERROR message to let a channel state machine continue.
                   */
                  status.state= State.STATE_NOT_CONNECTED;
                  status.text= "Fail";
                  try
                  {
                    channel.close();
                  }
                  catch( RemoteException e )
                  {
                    e.printStackTrace();
                  }
                  catch( AntCommandFailedException e )
                  {
                    e.printStackTrace();
                  }
                  finally
                  {
                    Log.e(LOG_TAG, "Closing channel");
                    channel.release();
                    channel= null;
                    onConnectFailed(ConnectionFailedReason.ANT_CONNECTION_LOST);
                  }
                }
                else if( message.getMessageContent()[2] == 0x08 ) // event_rx_fail_go_to_search
                {
                  Toast.makeText(ctx.getApplicationContext(), LOG_TAG + "event_rx_fail_go_to_search? " + String.format("%02X", message.getMessageContent()[1]) + " "
                      + String.format("%02X", message.getMessageContent()[2]) + " " + message.getMessageContentString(), Toast.LENGTH_SHORT).show();
                  connect();
                }
                else if( message.getMessageContent()[2] == 0x09 ) // collision
                {
                  // Do nothing for now.
                  Toast.makeText(ctx.getApplicationContext(), LOG_TAG + " collision " + String.format("%02X", message.getMessageContent()[1]) + " "
                      + String.format("%02X", message.getMessageContent()[2]) + " " + message.getMessageContentString(), Toast.LENGTH_SHORT).show();
                }
                /*
                 * event_rx_search_timeout     0x40,channel,message_id,0x01 A receive channel has timed out on searching. The search is terminated, and the channel has been automatically closed. In order to restart the search the Open Channel message must be sent again.
* event_rx_fail_go_to_search  0x40,channel,message_id,0x08 The channel has dropped to search after missing too many messages.
* event_channel_collision     0x40,channel,message_id,0x09 Two channels have drifted into each other and overlapped in time on the device causing one channel to be blocked.
* event_transfer_tx_start   0x40,channel,message_id,0x0A A burst transfer has begun
# event_rx_broadcast          0x40,channel,message_id,0x0A ANT Library special event (Not in serial interface). This event is sent to denote a valid broadcast data message has been received by the ANT library, and the data is waiting in the appropriate channel receive buffer.
* event_rx_acknowledged       0x40,channel,message_id,0x0B ANT Library special event (Not in serial interface). This event is sent to denote that a valid acknowledged data message has been received by the ANT library, and the data is waiting in the appropriate channel receive buffer.
* event_rx_burst_packet       0x40,channel,message_id,0x0C ANT Library special event (Not in serial interface). It indicates the successful reception of a burst packet in a Burst Transfer sequence.
* channel_in_wrong_state      0x40,channel,message_id,0x15 Returned on attempt to perform an action on a channel that is not valid for the channel's state
* channel_not_opened          0x40,channel,message_id,0x16 Attempt to transmit data on an unopened channel
* channel_id_not_set          0x40,channel,message_id,0x18 Returned on attempt to open a channel before setting a valid ID
* transfer_in_progress        0x40,channel,message_id,31 Returned on an attempt to communicate on a channel with a transmit transfer in progress.
                 */
                /*else if( message.getMessageContent()[2] == 0x08 ) // ???
                {
                
                }*/
                else
                {
                  Log.d(LOG_TAG, "Reached Else: " + "Channel Event " + String.format("%02X", message.getMessageContent()[1]) + " "
                          + String.format("%02X", message.getMessageContent()[2]) + " " + message.getMessageContentString());
                  Toast.makeText(ctx.getApplicationContext(), "Channel Event " + String.format("%02X", message.getMessageContent()[1]) + " "
                          + String.format("%02X", message.getMessageContent()[2]) + " " + message.getMessageContentString(), Toast.LENGTH_SHORT).show();
                  // Toast.makeText(ctx.getApplicationContext(), "CHANNEL_EVENT " + message.getMessageContentString(), Toast.LENGTH_SHORT).show();
                }
                break;
              case CHANNEL_ID:
                // We have received a channel ID notification
                // device_number;
                // device_id;
                channel_number= message.getMessageContent()[0];
                serial= ((int) message.getMessageContent()[1] & 0xFF) | (((int) message.getMessageContent()[2] & 0xFF) << 8);
                // channel_id_recieved= true;
                break;
              case CHANNEL_RESPONSE:
                if( message.getMessageContent()[1] == 0x4B ) // Channel open
                {
                
                }
                else if( message.getMessageContent()[1] == 0x4C ) // Channel close
                {
                
                }
                else if( message.getMessageContent()[1] == 0x42 ) // Assign channel
                {
                
                }
                else if( message.getMessageContent()[1] == 0x43 ) // Channel period
                {
                
                }
                else if( message.getMessageContent()[1] == 0x45 ) // RF Frequency
                {
                
                }
                else if( message.getMessageContent()[1] == 0x51 ) // channel id
                {
                
                }
                else if( message.getMessageContent()[1] == 0x75 ) // Search Priority
                {
                
                }
                else
                {
                  Log.d(LOG_TAG, "Channel Response " + String.format("%02X", message.getMessageContent()[1]) + " " + message.getMessageContentString());
                  Toast.makeText(ctx.getApplicationContext(), "Channel Response " + String.format("%02X", message.getMessageContent()[1]) + " " + message.getMessageContentString(), Toast.LENGTH_SHORT).show();
                }
                
                break;
              case CHANNEL_STATUS:
                break;
              case OTHER:
                break;
              case SERIAL_NUMBER:
                break;
              default:
                break;
              
            }
          }
          
          @Override
          public void onChannelDeath()
          {
            Log.d(LOG_TAG, "onChannelDeath");
            // TODO Auto-generated method stub
          }
        });
        channel.assign(ChannelType.BIDIRECTIONAL_SLAVE);
        Log.d(LOG_TAG, "Connect to: " + serial + " " + PERIOD);
        channel.setChannelId(new ChannelId(serial, ChannelId.ANY_DEVICE_TYPE, ChannelId.ANY_TRANSMISSION_TYPE, true));
        channel.setRfFrequency(57);
        channel.setPeriod(PERIOD);
        channel.open();
      }
    }
    catch( RemoteException e )
    {
      e.printStackTrace();
    }
    catch( AntCommandFailedException e )
    {
      e.printStackTrace();
    }
  }
  
  protected void onPairingStart()
  {
    channel_id_requested= false;
    pairing_params= new ArrayList<Entry<String, String>>();
    pairing_questions= new ArrayList<ParameterQuestion>();
    pairing_possible= false;
  }

  protected void nextPairingTry()
  {
    PegasosLog.e(LOG_TAG, "nextPairingTry");
    if( pairing_timer != null )
      pairing_timer.cancel();
    pairing_timer= null;
    
    // Toast.makeText(ctx, "Next Pairing try", Toast.LENGTH_SHORT).show();
    
    acquireChannel();
    
    channel_action_running= true;
    if( channel != null )
    {
      try
      {
        channel.close();
      }
      catch( RemoteException e )
      {
        e.printStackTrace();
      }
      catch( AntCommandFailedException e )
      {
        e.printStackTrace();
      }
      channel= null;
    }
    channel_id_requested= false;
    serial= 0;
    channel_action_running= false;
  }
  
  protected abstract void broadcastData(AntMessageParcel message);
  
  protected abstract void broadcastDataPairing(AntMessageParcel message);
}
