package at.pegasos.client.sensors;

import android.content.*;
import android.util.*;

import at.pegasos.client.values.*;
import at.pegasos.serverinterface.*;
import at.univie.mma.Config;
import at.univie.mma.serverinterface.sensors.*;

import com.dsi.ant.plugins.antplus.pcc.*;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.*;
import com.dsi.ant.plugins.antplus.pcc.defines.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.*;

import java.math.*;
import java.util.*;

import static com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult.*;

public class ANT_HRSensor extends AntPccSensor {

  private final ValueStore store;
  int heart_beat;
  private HRSensor server_sensor;

  protected IPluginAccessResultReceiver<AntPlusHeartRatePcc> base_IPluginAccessResultReceiver = new IPluginAccessResultReceiver<AntPlusHeartRatePcc>() {
    // Handle the result, connecting to events on success or reporting
    // failure to user.
    @Override public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      if( resultCode == SUCCESS )
      {
        internal_sensor = result;
        if( Config.LOGGING )
          Log.d(LOG_TAG, result.getAntDeviceNumber() + ": " + initialDeviceState);
        status.state = Sensor.State.STATE_OK;
        status.number = 0;
        status.text = "OK";

        if( pairing )
        {
          // this needs to be done since onSensorPaired uses currenSensorConfig() which uses hr_device_id
          int hr_device_id = result.getAntDeviceNumber();

          // Ask user whether pairing should be completed
          onSensorPaired("HR-Monitor", "" + hr_device_id);
        }
        else
        {
          subscribeToHrEvents();
          onConnect();
        }
      }
      else
      {
        handleResultCodes(resultCode, initialDeviceState);
      }
    }
  };

  public ANT_HRSensor(Object ctx)
  {
    super((Context) ctx);

    sampling_rate = Config.ANT_HEARTRATE_SAMPLING_RATE;
    LOG_TAG = "HR ANT";

    store = ValueStore.getInstance();
  }

  @Override
  public void initSensor()
  {
    server_sensor = new HRSensor(2, Config.DATASETS_PERPACKET_HR);

    status.state = Sensor.State.STATE_CONNECTING;
    status.number = 0;
    status.text = "--";

    connect();
  }

  @Override
  public void startSensor()
  {
    // is_running= true;
    server_sensor.start_sending();
  }

  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    super.stopSensor();
  }

  @Override
  public void destroy()
  {
    super.destroy();
    base_IPluginAccessResultReceiver = null;
  }

  public void reset(int serial)
  {
    //Release the old access if it exists
    if( internal_sensor != null )
    {
      internal_sensor.releaseAccess();
      internal_sensor = null;
    }

    if( !killed )
    {
      AntPlusHeartRatePcc.requestAccess((Context) ctx, serial, 10, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
    }
  }  // END HANDLE RESET

  private void addRR(int calcInt)
  {
    server_sensor.addRR(calcInt);
  }

  public void subscribeToHrEvents()
  {
    // It can happen that a stop was requested and we reconnect before we are properly shut down
    if( killed )
      return;
    ((AntPlusHeartRatePcc) internal_sensor).subscribeHeartRateDataEvent(new IHeartRateDataReceiver() {
      @Override public void onNewHeartRateData(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int computedHeartRate,
          final long heartBeatCounter, final BigDecimal heartBeatEventTime, final DataState dataState)

      // public void onNewHeartRateData(final long estTimestamp,
      // EnumSet<EventFlag> eventFlags,
      // final int computedHeartRate, final long heartBeatCount,final
      // BigDecimal heartBeatEventTime, final DataState dataState)

      {
        heart_beat = computedHeartRate;
        status.state = Sensor.State.STATE_OK;
        status.number = heart_beat;
        status.text = "" + heart_beat;
        store.setHeartRateBPM(heart_beat);
        server_sensor.setCurrentHR(heart_beat);

      }
    });

    ((AntPlusHeartRatePcc) internal_sensor).subscribePage4AddtDataEvent(new IPage4AddtDataReceiver() {
      @Override public void onNewPage4AddtData(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
          final int manufacturerSpecificByte, final BigDecimal timestampOfPreviousToLastHeartBeatEvent)
      {
        /*
         * main.runOnUiThread(new Runnable() {
         *
         * @Override public void run() { //
         * tv_previousToLastHeartBeatEventTimeStamp
         * .setText(String.valueOf
         * (timestampOfPreviousToLastHeartBeatEvent)); } });
         */
      }
    });

    ((AntPlusHeartRatePcc) internal_sensor).subscribeManufacturerAndSerialEvent(new IManufacturerAndSerialReceiver() {
      @Override public void onNewManufacturerAndSerial(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
          final int manufacturerID, final int serialNumber)
      {
        /*
         * main.runOnUiThread(new Runnable() {
         *
         * @Override public void run() {
         * //tv_estTimestamp.setText(String.valueOf(estTimestamp));
         *
         * //tv_manufacturerID.setText(String.valueOf(manufacturerID));
         * //tv_serialNumber.setText(String.valueOf(serialNumber));
         *
         * } });
         */
      }
    });

    ((AntPlusHeartRatePcc) internal_sensor).subscribeCalculatedRrIntervalEvent(new ICalculatedRrIntervalReceiver() {

      @Override public void onNewCalculatedRrInterval(long estTimestamp, java.util.EnumSet<EventFlag> eventFlags,
          java.math.BigDecimal calculatedRrInterval, AntPlusHeartRatePcc.RrFlag rrFlag)
      {
        if( Config.LOGGING )
          Log.d("ANT_HR RR", "t: " + estTimestamp + " " + eventFlags + " calcInt " + calculatedRrInterval + " rrFlag: " + rrFlag);
        addRR((int) (calculatedRrInterval.doubleValue() / 1024.0 * 1000));
      }
    });

  }

  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }

  @Override
  public void resetMetrics()
  {
    // There is nothing to do
  }

  @Override
  protected void onDead()
  {
    store.setHeartRateBPM(-1);
  }
}
