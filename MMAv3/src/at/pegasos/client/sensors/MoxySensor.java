package at.pegasos.client.sensors;

import android.content.*;
import android.os.*;
import android.util.*;

import at.pegasos.client.values.*;
import at.pegasos.serverinterface.*;
import at.univie.mma.serverinterface.sensors.*;

import com.dsi.ant.channel.*;
import com.dsi.ant.message.ipc.*;

import java.util.*;

public class MoxySensor extends AntBasicSensor {

  private at.univie.mma.serverinterface.sensors.MoxySS server_sensor;

  private final Value smO2;
  private final Value thb;

  private byte last_event_count= 0;

  private boolean utc_required;

  /**
   * Message used to send time information to the device
   */
  private byte time_message[]= {0x10, /* Data Page Number */
                                0x00, /* Command ID */ 
                         (byte) 0xFF, /* Reserved */
                                0x00, /* Local Time offset */
                                0x00, 0x00, 0x00, 0x00}; 
  
  /**
   * Time offset from UTC. Used for sending time info to the device
   */
  private byte local_time_offset= 0x00;

  public MoxySensor(Object ctx)
  {
    super((Context) ctx);

    LOG_TAG= "Moxy";
    SENSOR_NAME= "Moxy";
    PERIOD= 8192;

    ValueStore store = ValueStore.getInstance();
    smO2= store.getCreateValueInt("SMO2");
    thb= store.getCreateValueDouble("THB");

    allowed_device_types= new HashSet<Integer>(1);
    // allowed_device_types.add(0x79); //Bike Speed & Cadence
    allowed_device_types.add(0x1F); // Muscle Oxygen
  }

  @Override
  public void initSensor()
  {
    server_sensor = new MoxySS(13, 1);
    
    TimeZone tz = TimeZone.getDefault();
    Date now = new Date();
    int offsetFromUtc = tz.getOffset(now.getTime()) / 1000 / 3600;
    local_time_offset = (byte) (offsetFromUtc * 15);

    super.initSensor();
  }

  @Override
  public void startSensor()
  {
    Log.e(LOG_TAG, "start sending");
    // is_running= true;
    server_sensor.start_sending();
  }

  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    super.stopSensor();
  }

  @Override
  protected void broadcastData(AntMessageParcel message)
  {
    status.state = Sensor.State.STATE_OK;
    status.text = "OK";
    if( /*device_type == 0x1F && */ message.getMessageContent()[1] == 0x01 ) // Moxy data
    {
      byte event_count = message.getMessageContent()[2];
      if( event_count != last_event_count)
      {
        utc_required = ((message.getMessageContent()[3] & 0x01) == 1);
        short interval = (short) ((message.getMessageContent()[4] & 0x0E) >> 1);
        double tHB = 0.01 * (double) ((message.getMessageContent()[5] & 0xFF) | ((message.getMessageContent()[6] & 0x0F) << 8));
        double pSO = 0.1 * (double) (((message.getMessageContent()[6] & 0xF0) >> 4) | ((message.getMessageContent()[7] & 0x3F) << 4));
        double cSO = 0.1 * (double) (((message.getMessageContent()[7] & 0xC0) >> 6) | ((message.getMessageContent()[8] & 0xFF) << 2));

        thb.setDoubleValue(tHB);
        smO2.setIntValue((int) (cSO));

        server_sensor.setData((int) cSO, (int) (tHB * 100));

        Log.d(LOG_TAG, "Data recieved: " + event_count + " " + tHB + " " + pSO + " " + cSO + " " + utc_required + " " + interval);

        last_event_count = event_count;
      }
    
      Log.d(LOG_TAG, "utc_required:" +utc_required + " local_time_offset:" + local_time_offset);
      if( utc_required )
      {
        time_message[1] = 0x00; /* set time */
        time_message[3] = local_time_offset;
        
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(1989, 12, 31, 0, 0);
        int h = (int) (( new Date().getTime() - cal.getTimeInMillis() ) / 1000);
        time_message[4] = (byte) (h & 0x000000FF);
        time_message[5] = (byte) ((h & 0x0000FF00)>>8);
        time_message[6] = (byte) ((h & 0x00FF0000)>>16);
        time_message[7] = (byte) ((h & 0xFF000000)>>32);

        try
        {
          channel.startSendAcknowledgedData(time_message);
        }
        catch (RemoteException e )
        {}
        catch(AntCommandFailedException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    /*else if( message.getMessageContent()[0] == 0x00 ) // S&C data
    {
      int ctime= ((message.getMessageContent()[1]&0xFF)) | ((message.getMessageContent()[2]&0xFF)<<8);
      double time = ctime - last_cad_time;
      int crevs= ((message.getMessageContent()[3]&0xFF)) | ((message.getMessageContent()[4]&0xFF)<<8);
      int revs = crevs - last_rev_cad;
      cSO= (revs / ((time/1024.0)*1000));
      
      last_rev_cad= crevs;
      last_cad_time= ctime;
      
      ctime= ((message.getMessageContent()[8]&0xFF)) | ((message.getMessageContent()[9]&0xFF)<<8);
      time = ctime - last_speed_time;
      crevs= ((message.getMessageContent()[10]&0xFF)) | ((message.getMessageContent()[11]&0xFF)<<8);
      revs = crevs - last_rev;
      tHB= (revs / ((time/1024.0)*1000) ) * (1000) / 2070;
      
      last_rev= crevs;
      last_speed_time= ctime;
      
      thb.setDoubleValue(tHB);
      smO2.setIntValue((int) (cSO));
      
      server_sensor.setData((int) cSO, (int) (tHB * 100));
      
      Log.d(LOG_TAG, "Data recieved: " + time + " " + tHB + " " + pSO + " " + cSO + " " + utc_required + " " + interval);
    }*/
  }
  
  /*@Override
  public int getSensorType() {
    return ServerSensor.TYPE_FOOTPOD;
  }

  @Override
  public int getDataSetsPerPacket() {
    return Config.DATASETS_PERPACKET_FOOTPOD;
  }*/

  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }

  @Override
  public void resetMetrics()
  {
    smO2.setIntValue(null);
    thb.setDoubleValue(null);
  }

  @Override
  protected void broadcastDataPairing(AntMessageParcel message)
  {
    pairing_possible= true;
  }
}
