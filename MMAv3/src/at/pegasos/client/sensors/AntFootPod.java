package at.pegasos.client.sensors;

import android.content.Context;
import android.util.Log;

import com.dsi.ant.message.ipc.AntMessageParcel;

import java.util.HashSet;
import java.util.Timer;

import at.pegasos.client.values.*;
import at.pegasos.concurrent.TimedList;
import at.univie.mma.Config;
import at.pegasos.serverinterface.ServerSensor;
import at.univie.mma.serverinterface.sensors.FootPodSS;
import at.pegasos.client.values.NumberValueListening;
import at.pegasos.client.values.ValueStore;

public class AntFootPod extends AntBasicSensor {
  
  protected FootPodSS server_sensor;
  private ValueStore store;
  /**
   * Footpod Calibration factor
   */
  private double factor;
  
  private double distance_m= 0;
  private double previous_distance= 0;
  private long previous_strides= 0;
  private long strides= 0;
  
  private static AntFootPod instance;
  private NumberValueListening speedVal;
  private Timer avgs;

  public static AntFootPod getInstance()
  {
    return instance;
  }

  public AntFootPod(Object ctx)
  {
    super((Context) ctx);
    
    setup();
  }
  
  private void setup()
  {
    LOG_TAG= "AntFootPod";
    SENSOR_NAME= "AntFootPodName";
    PERIOD= 8134;
    
    status= new Status();
    status.state= State.STATE_NOT_CONNECTED;
    status.number= 0;
    status.text= "";
    
    store= ValueStore.getInstance();

    speedVal= store.getCreateNumberValueListening("SPEED_MMS", (double) 0);
    
    allowed_device_types= new HashSet<Integer>(1);
    allowed_device_types.add(0x7C); //

    instance= this;
  }
  
  @Override
  public void initSensor()
  {
    server_sensor= new FootPodSS(1, Config.DATASETS_PERPACKET_FOOTPOD);
    
    super.initSensor();
  }
  
  @Override
  protected void loadParams(SensorParam param)
  {
    super.loadParams(param);
    
    // has_cadence= param.getParam(PARAM_HAS_CADENCE, "false").equals("true");
    
    factor= store.getFpCalibInt() / 1000.0;
  }
  
  @Override
  public void startSensor() {
    Log.e(LOG_TAG, "start sending");
    // is_running= true;
    server_sensor.start_sending();

    Settings.reportSetting("FPCalib", store.getFpCalibInt());
    // setAverage(5);
  }
  
  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();

    if( avgs != null )
    {
      avgs.cancel();
      avgs= null;
    }
    
    super.stopSensor();
  }
  
  @Override
  protected void broadcastData(AntMessageParcel message)
  {
    status.state= State.STATE_OK;
    status.text= "OK";
  
    if( message.getMessageContent()[1] == 0x01 ) // Main Data
    {
      int mstrides= message.getMessageContent()[7] & 0xFF;
  
      double time= (message.getMessageContent()[3] & 0xFF) + (message.getMessageContent()[2] & 0xFF) * 1/200.0;
      double distance= (message.getMessageContent()[4] & 0xFF) + ((message.getMessageContent()[5] & 0xF0)>> 4) * (1/16.0);
      double instant_speed= (message.getMessageContent()[5] & 0x0F) + (message.getMessageContent()[6] & 0xFF) * (1/256.0);
      // Log.d(LOG_TAG, "time: " + time + " distance: " + distance + " speed: " + instant_speed);
      
      if( previous_distance > distance )
      {
        distance_m+= (256 + distance - previous_distance) * factor;
      }
      else
      {
        distance_m+= (distance - previous_distance) * factor;
      }
      
      strides+= mstrides - previous_strides;
      if( previous_strides > mstrides )
      {
        strides+= 256;
      }
      previous_strides= mstrides;
      
      double hs= instant_speed * 1000.0 * factor;
      Log.d(LOG_TAG, "speed: " + hs + " dist:" + distance_m + " " + distance + " " + previous_distance + " strides:" + strides);
      previous_distance= distance;
      server_sensor.setData(hs, distance_m, strides, 0);
      store.setDistance((long) distance_m, this);
      speedVal.setDoubleValue(hs);
      // store.setCurrentSpeed(hs, this);
    }
    /*else if( message.getMessageContent()[1] == 0x02 ) // SDM Data
    {
      int cadence= message.getMessageContent()[4] & 0xFF;
      int cadence_fric= (message.getMessageContent()[5] & 0xF0) >> 4;
      int instant_speed= message.getMessageContent()[5] & 0x0F;
      int instant_speed_frac= message.getMessageContent()[6] & 0xFF;
      int status= message.getMessageContent()[8] & 0xFF;
    
      Log.d(LOG_TAG, "cad: "+ cadence + " cadfric"+ cadence_fric + " ispeed:"+instant_speed + " isf:" + instant_speed_frac + " s:" + status);
    }*/
  }
  
  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }
  
  @Override
  public void resetMetrics()
  {
    distance_m= 0;
    strides= 0;
  }
  
  // private boolean has_cadence= false;
  private int msg_count;
  
  @Override
  protected void onPairingStart()
  {
    super.onPairingStart();
    // has_cadence= false;
    msg_count= 0;
    Log.w(LOG_TAG, "onPairingStart");
  }
  
  // int zero_cad_detected= 0;

  @Override
  protected void broadcastDataPairing(AntMessageParcel message)
  {
    if( message.getMessageContent()[1] == 0x01 ) // Main Data
    {
      int time_frac= message.getMessageContent()[2] & 0xFF;
      int time= message.getMessageContent()[3] & 0xFF;
      int distance= message.getMessageContent()[4] & 0xFF;
      int distancef= (message.getMessageContent()[5] & 0xF0) >> 4;
      int instant_speed= message.getMessageContent()[5] & 0x0F;
      int instant_speed_frac= message.getMessageContent()[6] & 0xFF;
      int strides= message.getMessageContent()[7] & 0xFF;
      int timeu= message.getMessageContent()[8] & 0xFF;
  
      Log.d(LOG_TAG, "timef: "+ time_frac + " time"+ time + " distance:"+distance + " distf:" + distancef + "is: " + instant_speed + " isf:" + instant_speed_frac + " strides:" + strides + " t:" + timeu);
  
      msg_count++;
    }
    else if( message.getMessageContent()[1] == 0x02 ) // SDM Data
    {
      int cadence= message.getMessageContent()[4] & 0xFF;
      int cadence_fric= (message.getMessageContent()[5] & 0xF0) >> 4;
      int instant_speed= message.getMessageContent()[5] & 0x0F;
      int instant_speed_frac= message.getMessageContent()[6] & 0xFF;
      int status= message.getMessageContent()[8] & 0xFF;
      
      Log.d(LOG_TAG, "cad: "+ cadence + " cadfric"+ cadence_fric + " ispeed:"+instant_speed + " isf:" + instant_speed_frac + " s:" + status);
      
      /*if( cadence == 0 )
      {
        zero_cad_detected++;
        if( zero_cad_detected > 2 )
        {
          requestCapabilities();
        }
      }
      else
      {
        has_cadence= true;
      }*/
    }
    else if( message.getMessageContent()[1] == 0x03 ) // Calories
    {
  
    }
    else if( message.getMessageContent()[1] == 0x16 ) // Capabilities
    {
      int cap= (message.getMessageContent()[2] & 0xFF);
      Log.d(LOG_TAG, "Capabilities: " + cap);
    }
    
    if( msg_count > 4 && pairing_params != null )
    {
      // pairing_params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_CADENCE, "" + has_cadence));
      pairing_possible= true;
    }
  }

  private class FpAverage extends Average {
    public FpAverage(TimedList<Double> list, String outname, double sec_past)
    {
      super(list, outname, sec_past);
    }
    
    @Override
    public void run()
    {
      super.run();
      store.setCurrentSpeed(last_value, AntFootPod.this);
    }
  }
  
  public void setAverage(int time_s)
  {
    if( avgs != null )
    {
      avgs.cancel();
      avgs= new Timer();
    }
    avgs= new Timer();
    avgs.schedule(
        new FpAverage(store.getList("SPEED_MMS"), "SPEED_MMS_T", time_s), 500,
        500);
  }
  
  //private byte calibration_message[]= {0x01, /* Data Page Number */
  //        (byte) 0xAA, /* Calibration ID */
  //        0x00, 0x00, 0x00, 0x00, 0x00, 0x00 /* Reserved */
  //};
  /*
  private void requestCapabilities()
  {
    try
    {
      channel.startSendAcknowledgedData(calibration_message);
    }
    catch( RemoteException e )
    {
      e.printStackTrace();
    }
    catch( AntCommandFailedException e )
    {
      e.printStackTrace();
    }
  }*/
}
