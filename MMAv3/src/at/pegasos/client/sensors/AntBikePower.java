package at.pegasos.client.sensors;

import java.util.*;

import at.pegasos.client.values.Value;
import at.pegasos.client.values.ValueStore;
import org.acra.ACRA;

import com.dsi.ant.channel.AntCommandFailedException;
import com.dsi.ant.message.ipc.AntMessageParcel;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import at.pegasos.client.ui.*;
import at.pegasos.client.ui.dialog.*;
import at.pegasos.client.ui.sensormanager.*;
import at.pegasos.client.values.*;
import at.pegasos.client.ui.dialog.PowerMeterCalibrationDialog;
import at.pegasos.client.ui.dialog.PowerMeterCalibrationDialog.IPowerMeterCalibrationCallback;
import at.pegasos.client.ui.dialog.PowerMeterCalibrationDialog.RequestFailedReason;
import at.pegasos.serverinterface.ServerSensor;
import at.univie.mma.serverinterface.sensors.BikeNrSS;

public class AntBikePower extends AntBasicSensor implements IPowerMeterCalibrationCallback {

  private static final double OFFSET_NOT_INITIALISED= 100;

  private static final String PARAM_OFFSET= "offset";
  private static final String PARAM_HAS_CADENCE= "has_cadence";
  private static final String PARAM_USE_CADENCE= "use_cadence";
  private static final String PARAM_DISPLAY_CADENCE= "disp_cadence";
  private static final String PARAM_HAS_SPEED= "has_speed";
  private static final String PARAM_IS_CTF= "is_ctf";
  
  /**
   * This question will be asked when the Sensor is paired
   */
  private final ParameterQuestion cadenceq= new BooleanQuestion(PARAM_USE_CADENCE, "Do you want to use this sensor as cadence sensor?");
  private final ParameterQuestion cadenceDisplay= new BooleanQuestion.Builder(PARAM_USE_CADENCE)
          .setMessage( "Do you want to display the cadence of this sensor?")
          .build();

  /**
   * Constant used during the calculation of power from CTF message
   */
  private static final double PI30 = Math.PI / 30.0;

  protected BikeNrSS server_sensor;
  private ValueStore store;
  
  protected Value power;
  protected Value power_balance;
  protected Value cadence;
  protected Value torque;
  
  private double offset= OFFSET_NOT_INITIALISED;
  
  /**
   * Flag indicating if the power meter is a CTF (true) or PWR (false) device
   */
  protected boolean is_ctf;
  
  /**
   * Flag indicating whether this sensor should send supplied cadence values to the server
   */
  protected boolean use_cad;
 
  /**
   * Flag indicating whether this sensor should 'display' i.e. write values to the CADENCE value.
   */
  protected boolean disp_cad;

  /**
   * Message used to send time information to the device
   */
  private final byte[] calibration_message = {0x01, /* Data Page Number */
      (byte) 0xAA, /* Calibration ID */
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00 /* Reserved */
  };

  private byte last_ctfUpdateEventCount;

  private byte eventcount_unchanged_count= 0;

  private int ticks;

  private int last_ticks;

  private int last_time;

  protected Timer avgs;

  protected Sum sum;

  /**
   * String to be appended to all variable names of this instance
   */
  protected final String var_suffix;
  
  protected String name;

  private PowerMeterCalibrationDialog calibrationDialog;

  public AntBikePower(Object ctx)
  {
    super((Context) ctx);
    
    var_suffix= "";
    setup();
    createVariables();
  }
  
  public AntBikePower(Context ctx, String suffix)
  {
    super(ctx);
    
    var_suffix= suffix;
    setup();
    createVariables();
  }
  
  private void setup()
  {
    LOG_TAG= "BikePowerNew";
    SENSOR_NAME= "BikePower";
    PERIOD= 8182;
    
    store= ValueStore.getInstance();
    power= store.getCreateNumberValueListening("POWER");
    cadence= store.getCreateNumberValueListening("CADENCE");
    
    allowed_device_types= new HashSet<Integer>(1);
    // allowed_device_types.add(0x79); //Bike Speed & Cadence
    allowed_device_types.add(0x0B); // Bike Power
    
    avgs= new Timer();
  }
  
  private void createVariables()
  {
    power= store.getCreateNumberValueListening("POWER" + var_suffix);
    cadence= store.getCreateValueInt("CADENCE" + var_suffix);
    torque = store.getCreateValueDouble("TORQUE" + var_suffix);
  }
  
  @Override
  public void initSensor()
  {
    server_sensor= new BikeNrSS(0, 4);
    
    super.initSensor();
  }
  
  @Override
  protected void loadParams(SensorParam param)
  {
    super.loadParams(param);
    String offs= param.getParam(PARAM_OFFSET);
    if( offs != null )
      offset= Double.parseDouble(offs);
    else
      offset= OFFSET_NOT_INITIALISED;

    // TODO: fully implement this
    is_ctf= param.getParam(PARAM_IS_CTF,"false").equals("true");
    use_cad= param.getParam(PARAM_USE_CADENCE, "false").equals("true");
    disp_cad= param.getParam(PARAM_DISPLAY_CADENCE, "false").equals("true");
    
    // Toast.makeText(ctx.getApplicationContext(), LOG_TAG + " loadParams " + offset, Toast.LENGTH_SHORT).show();
  }
  
  @Override
  public void startSensor() {
    Log.e(LOG_TAG, "start sending");
    // is_running= true;
    server_sensor.start_sending();
    
    // TODO: make this better
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_1s");
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_3s");
    ValueStore.getInstance().getCreateNumberValue("POWER" + var_suffix + "_30s");
    // avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_1s", 1.0), 0, 500);
    // avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_3s", 3.0), 0, 500);
    // avgs.schedule(new Average(store.getList("POWER" + var_suffix), "POWER" + var_suffix + "_30s", 30.0), 0, 500);
    Average.scheduleSimple(avgs, "POWER" + var_suffix, "POWER" + var_suffix + "_1s", 1.0);
    Average.scheduleSimple(avgs, "POWER" + var_suffix, "POWER" + var_suffix + "_3s", 3.0);
    Average.scheduleSimple(avgs, "POWER" + var_suffix, "POWER" + var_suffix + "_30s", 30.0);
    ValueStore.getInstance().getCreateNumberValue("SUM_POWER" + var_suffix);
    sum= Sum.schedule(avgs, store.getList("POWER" + var_suffix), "SUM_POWER" + var_suffix, 60);
  }

  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    
    if( avgs != null )
      avgs.cancel();
    
    super.stopSensor();
  }
  
  @Override
  protected void broadcastData(AntMessageParcel message)
  {
    status.state= Sensor.State.STATE_OK;
    status.text= "OK";
    
    int pageType= message.getMessageContent()[1] & 0xFF;

    if( pageType == 0x01 ) // Calibration Message
    {
      // String msg= "Calibration message1 " + message.getMessageContentString();
      int calibration_type= message.getMessageContent()[2] & 0xFF;
      Log.e(LOG_TAG, "Calibration " + String.format("%02X", calibration_type) + " " + (calibration_type == 0x10) + " " + (calibration_type == 0xAC) + " " + (calibration_type == 0xAF));
      switch( calibration_type )
      {
        case 0x10: // CTF-defined message ie Zero Offset
          int offset= ((((int) message.getMessageContent()[7]) & 0xFF) << 8);
          offset+= ((int) message.getMessageContent()[8]) & 0xFF;
          if( calibrationDialog != null )
            calibrationDialog.setValue(offset);
          // Toast.makeText(ctx.getApplicationContext(), "Offset: " + offset, Toast.LENGTH_SHORT).show();
          break;
          
        case 0xAF:
          Toast.makeText(ctx.getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
          if( calibrationDialog != null )
            calibrationDialog.calibrationFailed(RequestFailedReason.Unkown);
          break;
        case 0xAC: // Success!
          Toast.makeText(ctx.getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
          // 04-09 14:46:11.024: D/BikePowerNew(16851): BROADCAST_DATA [00][01][AC][FF][FF][FF][FF][D1][01][40][20][CA][80]
          offset= ((((int) message.getMessageContent()[8]) & 0xFF) << 8);
          offset+= ((int) message.getMessageContent()[7]) & 0xFF;
          if( calibrationDialog != null )
            calibrationDialog.setValue(offset);
          break;
      }
      boolean calibration_success= (message.getMessageContent()[2]&0xFF) == 0xAC;
      boolean auto_zero_on= message.getMessageContent()[3] == 0x00;
      boolean auto_zero_not_supported= (message.getMessageContent()[2]&0xFF) == 0xFF;
      // Toast.makeText(ctx.getApplicationContext(), msg + "\nSuccess: " + calibration_success + "\nAutozero " + auto_zero_on + "/" + auto_zero_not_supported, Toast.LENGTH_SHORT).show();
    }
    else if( pageType == 0x02 ) // Get/Set Parameters
    {
      
    }
    else if( pageType == 0x03 ) // Calibration Message
    {
      Toast.makeText(ctx.getApplicationContext(), "Calibration message3 " + message.getMessageIdString(), Toast.LENGTH_SHORT).show();
    }
    else if( pageType == 0x10 ) // Power Only
    {
      byte eventCount= message.getMessageContent()[2];
      double event_diff;

      if( eventCount >= last_ctfUpdateEventCount )
        event_diff= eventCount- last_ctfUpdateEventCount;
      else
        event_diff= 256 + eventCount - last_ctfUpdateEventCount;

      int pedalPowerContribution;
      if( (message.getMessageContent()[3]&0xFF) != 0xFF )
        pedalPowerContribution= message.getMessageContent()[3] & 0x80;
      else
        pedalPowerContribution= 0;
      int pedalPower= (message.getMessageContent()[3] & 0x7F); // right pedalPower % - stored in bit
                                                               // 0-6
      byte instantCadence= message.getMessageContent()[4];

      int sumPower= (message.getMessageContent()[5] & 0xFF) + ((int) (message.getMessageContent()[6] & 0xFF) << 8);
      int diffPower;
      if( sumPower < last_ticks )
        diffPower= 65536 + (sumPower - last_ticks);
      else
        diffPower= sumPower - last_ticks;

      int instantPower= (message.getMessageContent()[7] & 0xFF ) + (((int) message.getMessageContent()[8] & 0xFF) << 8);

      status.text= "" + sumPower;

      boolean has_pedal_contribution= (message.getMessageContent()[3] & 0x40) == 0x40 && (message.getMessageContent()[3] != (byte) 0xFF);
      if( has_pedal_contribution )
      {
        short contribution= (short) (message.getMessageContent()[3] & 0x3F);
      }

      short cadence= message.getMessageContent()[4];

      double averagePower= -1.0;
      if( event_diff != 0 )
      {
        averagePower= diffPower / event_diff;

        this.power.setDoubleValue(averagePower);

        if( use_cad )
          server_sensor.setCadencePower(cadence, (short) averagePower);
        else
          server_sensor.setPower((short) averagePower);

        if( disp_cad )
          this.cadence.setIntValue((int) cadence);
      }

      Log.d(LOG_TAG, String.format("%drpm %dW %dW %.2f %.0f", cadence, instantPower, sumPower, averagePower, event_diff));

      last_ctfUpdateEventCount= eventCount;
      last_ticks= sumPower;
    }
    else if( pageType == 0x11 ) // Torque At Wheel
    {
      
    }
    else if( pageType == 0x12 ) // Torque At Crank
    {
      
    }
    else if( pageType == 0x13 ) // Torque Effectiveness & Pedal Smoothness
    {
      
    }
    else if( pageType == 0x20 ) // CTF Message
    {
      double event_diff;
      double ticks_diff;
      double time_diff= 0;

      byte ctfUpdateEventCount = message.getMessageContent()[2];
      
      if( ctfUpdateEventCount >= last_ctfUpdateEventCount )
        event_diff= ctfUpdateEventCount - last_ctfUpdateEventCount;
      else
        event_diff= 256 + ctfUpdateEventCount - last_ctfUpdateEventCount;
      
      double slope= (((int)message.getMessageContent()[3]&0xFF) << 8);
      slope+= ((int) message.getMessageContent()[4]&0xFF);
      slope/= 10.0;

      int time= (((int) message.getMessageContent()[5] & 0xFF) << 8);
      time+= ((int) message.getMessageContent()[6]) & 0xFF;
      
      ticks= (((int) message.getMessageContent()[7] & 0xFF) << 8);
      ticks+= ((int) message.getMessageContent()[8]) & 0xFF;
      
      Double dpower = null;
      if( event_diff > 0 )
      {
        // Reset check for stopped pedaling
        eventcount_unchanged_count= 0;

        if( time >= last_time )
          time_diff= time - last_time;
        else
          time_diff= 65536 + time - last_time;
        
        double elapsed_time= time_diff * 0.0005;
        
//        Toast.makeText(ctx.getApplicationContext(), "time_diff:" + time_diff + " " + elapsed_time + " " + time + " " + last_time, Toast.LENGTH_SHORT).show();
//        Toast.makeText(ctx.getApplicationContext(), "time_diff:" + time_diff + " " + elapsed_time, Toast.LENGTH_SHORT).show();
        
        if( ticks >= last_ticks )
          ticks_diff= ticks - last_ticks;
        else
          ticks_diff= 65536 + ticks - last_ticks;
        
        double cadence_period= (elapsed_time / event_diff);
        double cad= (60 / cadence_period);
        
        cadence.setIntValue((int) cad);
        
        double tf;
        double torque;
        if( ticks > 0 )
          tf= (1 / (elapsed_time / ticks_diff)) - offset;
        else
          tf= 0;

        torque= (tf / slope);
        this.torque.setDoubleValue(torque);

        dpower= (torque * cad * PI30);
        power.setDoubleValue(dpower);

        Log.d(LOG_TAG, String.format("Torque: %.2fnm %.1frpm %.2fW",torque, cad, dpower));

        if( use_cad )
        {
          server_sensor.setCadencePower((int) cad, dpower.shortValue());

          if( disp_cad )
            cadence.setIntValue((int) cad);
        }
        else
          server_sensor.setPower(dpower.shortValue());
      }
      else
      {
      	/*
         * When the user stops pedaling, the update event count field in broadcast messages does not
         * increment. After receiving 12 messages with the same update event count (approximately 3
         * seconds), the receiving device should change the cadence and power displays to zero.
         */
        eventcount_unchanged_count++;
        if( eventcount_unchanged_count >= 12 )
        {
          power.setDoubleValue(dpower);
          if( disp_cad )
            cadence.setIntValue(0);

          // Not only do we want to display zero power but also we want to record it
          if( use_cad )
          {
            server_sensor.setCadencePower(0, (short) 0);
            Log.d(LOG_TAG, "Report zero: 0rpm 0W");
          }
          else
          {
            server_sensor.setPower((short) 0);
            Log.d(LOG_TAG, "Report zero: 0W");
          }
        }
      }
      
      /*double power1= -1.0, power2= -1.0;
      {
        int period = time - last_time;
        int torque1 = ticks - last_ticks;
        float time = (float)period / (float)2000.00;
        // TODO: 
        /*
         * When the user stops pedaling, the update event count field in broadcast messages does not
         * increment. After receiving 12 messages with the same update event count (approximately 3
         * seconds), the receiving device should change the cadence and power displays to zero.
         */
        /*
        if( time != 0 && slope != 0 && period != 0 )
        {
          double torque_freq = torque1 / time - offset;
          double nm_torque = 10.0 * torque_freq / slope;
          double cadence = 2000.0 * 60 * (ctfUpdateEventCount - last_ctfUpdateEventCount) / period;
          power1 = 3.14159 * nm_torque * cadence / 30;
          power2 = Math.PI * nm_torque * cadence / 30.0;
//          Toast.makeText(ctx.getApplicationContext(), "Ctf power:" + power1 + " " + cadence, Toast.LENGTH_SHORT).show();
          
          if( use_cad )
            server_sensor.setCadencePower((short) cadence, (short) power2);
          if( disp_cad )
            this.cadence.setIntValue((int) cadence);
        }
      }*/
      
      // Log.d(LOG_TAG, String.format("slope: %.2f time %d ticks %d %.10f %.10f %.10f", slope,time,ticks, power1, power2, dpower));
      
      // Store last values
      last_ctfUpdateEventCount= ctfUpdateEventCount;
      last_time= time;
      last_ticks= ticks;
    }
    else if( message.getMessageContent()[1] == 0x00 )
    {
      Toast.makeText(ctx.getApplicationContext(), "Calibration? " + String.format("%02X", message.getMessageContent()[2]) + " " + cadence, Toast.LENGTH_SHORT).show();
    }
  }
  
  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }
  
  @Override
  public void resetMetrics()
  {
    cadence.setIntValue(null);
    power.setDoubleValue(null);
    sum.reset();
  }
  
  /*class Pairer extends TimerTask
  {
    @Override
    public void run()
    {
      ArrayList<Entry<String, String>> params= new ArrayList<Entry<String, String>>(3);
      params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_CADENCE, "" + has_cadence));
      params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_SPEED, "" + has_speed));
      params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_IS_CTF, "" + is_ctf));
      if( has_cadence )
      {
        List<ParameterQuestion> questions= new ArrayList<ParameterQuestion>(1);
        questions.add(cadenceq);
        onSensorPaired(name != null ? name : "Power-Meter", "" + pairing_device.getAntDeviceNumber(), questions, params);
      }
      else
        onSensorPaired(name != null ? name : "Power-Meter", "" + pairing_device.getAntDeviceNumber(), params);
    }
  }*/
  
  private boolean has_cadence= false;
  private boolean has_speed= false;
  private int msg_count;

  private boolean calibration_request_running;
  
  @Override
  protected void onPairingStart()
  {
    super.onPairingStart();
    is_ctf= false;
    has_cadence= false;
    has_speed= false;
    msg_count= 0;
  }

  @Override
  protected void broadcastDataPairing(AntMessageParcel message)
  {
    byte type= (byte) (message.getMessageContent()[1] & 0xFF);
    
    String msg= " ";
    if( type == 0x10 ) // Power Only
    {
      // TODO: jetzt das hier ausgeben um zu sehen, was fuer ein typ wir eigentlich sind
      // muss feststellen ob es auch cadence hat usw. 
      
      boolean has_pedal_contribution= (message.getMessageContent()[3] & 0x40) == 0x40 && (message.getMessageContent()[3] != (byte) 0xFF);
      
      has_cadence= message.getMessageContent()[4] != (byte) 0xFF;
      msg_count++;
    }
    if( message.getMessageContent()[1] == 0x11 ) // Torque At Wheel
    {
      
    }
    if( message.getMessageContent()[1] == 0x12 ) // Torque At Crank
    {
      
    }
    if( message.getMessageContent()[1] == 0x13 ) // Torque Effectiveness & Pedal Smoothness
    {
      
    }
    if( message.getMessageContent()[1] == 0x20 ) // CTF
    {
      has_cadence= true;
      is_ctf= true;
      msg_count++;
    }
    
//    Toast.makeText(ctx, "Data " + String.format("%02X", type) + "/" + type + " " + serial + msg, Toast.LENGTH_SHORT).show();
    
    if( msg_count > 4 && pairing_params != null )
    {
      if( is_ctf || has_cadence || has_speed )
      {
        try
        {
          // pairing_params.add(new AbstractMap.SimpleEntry<String,String>("a", "b"));
          pairing_params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_CADENCE, "" + has_cadence));
          pairing_params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_HAS_SPEED, "" + has_speed));
          pairing_params.add(new AbstractMap.SimpleEntry<String,String>(PARAM_IS_CTF, "" + is_ctf));
          Toast.makeText(ctx, "new sensor " + serial + " " + has_cadence + " " + has_speed + " " + is_ctf, Toast.LENGTH_SHORT).show();
          if( has_cadence )
          {
            pairing_questions.add(cadenceq);
            pairing_questions.add(cadenceDisplay);
          }
        }
        catch(Exception e)
        {
          ACRA.getErrorReporter().handleException(e);
        }
        
        pairing_possible= true;
      }
    }
  }
  
  @Override
  public CalibrationDialog[] getCalibrationDialog()
  {
    // Only show a dialog when we are ok. TODO: connected as well?
    if( status.state == Sensor.State.STATE_OK )
    {
      if( is_ctf )
        return new CalibrationDialog[] {new PowerMeterCalibrationDialog(ctx, this, true,name)};
      else
        return new CalibrationDialog[] {new PowerMeterCalibrationDialog(ctx, this, false)};
    }
    else
      return new CalibrationDialog[0];
  }

  @Override
  public void onCalibrationDialogOpened(PowerMeterCalibrationDialog ref)
  {
    calibrationDialog= ref;
  }

  @Override
  public void storeOffsetValue(double value)
  {
    Toast.makeText(ctx.getApplicationContext(), "Store offset "+ value, Toast.LENGTH_SHORT).show();
    offset= value;
    currentSensorParam().addParam(PARAM_OFFSET, "" + value);
    updateConfig();
  }

  @Override
  public double getStoredOffsetValue()
  {
    return offset;
  }

  @Override
  public boolean requestCalibration()
  {
    try
    {
      channel.startSendAcknowledgedData(calibration_message);
      calibration_request_running= true;
      // inCalibration= true;
      return true;
    }
    catch( RemoteException e )
    {
      e.printStackTrace();
    }
    catch( AntCommandFailedException e )
    {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public void dialogClosed()
  {
    // inCalibration= false;
    calibrationDialog= null;
  }

}
