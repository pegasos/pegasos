package at.pegasos.client.sensors;

import android.content.Context;
import android.hardware.*;
import android.util.Log;

import at.pegasos.serverinterface.ServerSensor;
import at.univie.mma.Config;
import at.univie.mma.sensors.*;
import at.univie.mma.serverinterface.sensors.*;

import static android.hardware.Sensor.TYPE_ACCELEROMETER;
import static android.hardware.Sensor.TYPE_GYROSCOPE;
import static android.hardware.Sensor.TYPE_MAGNETIC_FIELD;

public class IMU extends at.pegasos.client.sensors.Sensor implements SensorEventListener {
  public static final String[] types = {SensorTypeNames.Inertial};
  private static final int SAMPLE_DIFF = 10;

  private final android.hardware.Sensor gyroscope;
  private final android.hardware.Sensor accelerometer;
  private final android.hardware.Sensor magnetometer;

  private final SensorManager sm;

  private Inertial server_sensor;

  private float acc_x;
  private float acc_y;
  private float acc_z;
  private float gyr_x;
  private float gyr_y;
  private float gyr_z;
  private float mag_x;
  private float mag_y;
  private float mag_z;

  public IMU(Object starter)
  {
    super((Context) starter);
    status.state = State.STATE_OK;
    status.number = 0;
    status.text = "";

    sm = (SensorManager) ((Context) starter).getSystemService(Context.SENSOR_SERVICE);
    accelerometer = sm.getDefaultSensor(TYPE_ACCELEROMETER);
    gyroscope = sm.getDefaultSensor(TYPE_GYROSCOPE);
    magnetometer = sm.getDefaultSensor(TYPE_MAGNETIC_FIELD);
  }

  public IMU(Context starter)
  {
    super(starter);
    status.state = State.STATE_OK;
    status.number = 0;
    status.text = "";

    sm = (SensorManager) starter.getSystemService(Context.SENSOR_SERVICE);
    accelerometer = sm.getDefaultSensor(TYPE_ACCELEROMETER);
    gyroscope = sm.getDefaultSensor(TYPE_GYROSCOPE);
    magnetometer = sm.getDefaultSensor(TYPE_MAGNETIC_FIELD);
  }

  @Override
  public Status getStatus()
  {
    return status;
  }

	@Override
  public void initSensor()
  {
    server_sensor = new Inertial(1, 1000 / SAMPLE_DIFF, SAMPLE_DIFF);

    sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    sm.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_FASTEST);
    sm.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_FASTEST);

    if( Config.LOGGING ) Log.d("AccelerationSensor", "STARTED");
    status.state = State.STATE_OK;
    status.number = 0;
  }

  @Override
  public void startSensor()
  {
    if( Config.LOGGING ) Log.d("AccelerationSensor", "Start Sensor");
    server_sensor.start_sending();
  }

  @Override
  public void stopSensor()
  {
    sm.unregisterListener(this);
    server_sensor.stop_sending();
  }

  /**
   * Required Interface method. @author martin tampier
   */
  @Override
  public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy)
  {
    // TODO Auto-generated method stub

  }

  /**
   * Required Interface method. @author martin tampier
   */
  @Override
  public void onSensorChanged(SensorEvent sensorEvent)
  {
    status.state = Sensor.State.STATE_OK;
    status.text = "OK";

    if( sensorEvent.sensor.getType() == TYPE_ACCELEROMETER )
    {
      acc_x = sensorEvent.values[0];
      acc_y = sensorEvent.values[1];
      acc_z = sensorEvent.values[2];
      // accTim = sensorEvent.timestamp;
    }

    if( sensorEvent.sensor.getType() == TYPE_GYROSCOPE )
    {
      gyr_x = sensorEvent.values[0];
      gyr_y = sensorEvent.values[1];
      gyr_z = sensorEvent.values[2];
      // gyroTim = sensorEvent.timestamp;
    }

    if( sensorEvent.sensor.getType() == TYPE_MAGNETIC_FIELD )
    {
      mag_x = sensorEvent.values[0];
      mag_y = sensorEvent.values[1];
      mag_z = sensorEvent.values[2];
      // magnTim = sensorEvent.timestamp;
    }

    server_sensor.setData((short) (acc_x * 1000),
            (short) (acc_y * 1000),
            (short) (acc_z * 1000),
            (short) (gyr_x * 1000),
            (short) (gyr_y * 1000),
            (short) (gyr_z * 1000),
            (short) (mag_x * 1000),
            (short) (mag_y * 1000),
            (short) (mag_z * 1000));
  }
	
  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }

  @Override
  public void resetMetrics()
  {
    // TODO Auto-generated method stub
  }

  @Override
  protected void connect()
  {

  }
}
