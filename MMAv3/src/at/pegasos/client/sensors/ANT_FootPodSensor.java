package at.pegasos.client.sensors;

import android.content.*;
import android.util.*;

import at.pegasos.client.values.*;
import at.pegasos.serverinterface.*;
import at.univie.mma.Config;
import at.univie.mma.serverinterface.sensors.*;

import com.dsi.ant.plugins.antplus.pcc.*;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.*;
import com.dsi.ant.plugins.antplus.pcc.defines.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.*;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.*;

import java.math.*;
import java.util.*;

import static com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult.*;

public class ANT_FootPodSensor extends AntPccSensor {
  private final ValueStore store;
  private final int[] fp_buffer;

  private FootPodSensor server_sensor;
  private int fp_calib_int = 1000, fp_device_id = 0;
  private double fp_speed, fp_distance, fp_last_distance = 0;
  private long fp_strides, fp_last_strides = 0;
  private long fp_calories = 0;
  private double fp_calib_double;
  private int fp_counter = 0;

  protected IPluginAccessResultReceiver<AntPlusStrideSdmPcc> base_IPluginAccessResultReceiver = new IPluginAccessResultReceiver<AntPlusStrideSdmPcc>() {
    @Override public void onResultReceived(AntPlusStrideSdmPcc result, RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      if( resultCode == SUCCESS )
      {
        internal_sensor = result;
        if( Config.LOGGING )
          Log.d("FP ANT", (result.getAntDeviceNumber() + ": " + initialDeviceState));
        // main.say_it(1,"FootPod connected");
        status.state = Sensor.State.STATE_OK;
        status.number = 0;
        status.text = "OK";
        if( pairing )
        {
          // this needs to be done since onSensorPaired uses currenSensorConfig() which uses fp_device_id
          fp_device_id = result.getAntDeviceNumber();

          // Ask user whether pairing should be completed
          onSensorPaired("FootPod", "" + fp_device_id);
        }
        else
        {
          subscribeToEvents();
          onConnect();
        }
      }
      else
      {
        handleResultCodes(resultCode, initialDeviceState);
      }
    }
  };

  public ANT_FootPodSensor(Object ctx)
  {
    super((Context) ctx);

    sampling_rate = Config.ANT_FOOTPOD_SAMPLING_RATE;
    LOG_TAG = "FP ANT";

    fp_buffer = new int[5];

    store = ValueStore.getInstance();

    fp_calib_int = store.getFpCalibInt();
    fp_calib_double = ((double) fp_calib_int) / 1000.0;
  }

  @Override
  public void initSensor()
  {
    server_sensor = new FootPodSensor(1, Config.DATASETS_PERPACKET_FOOTPOD);

    status.state = Sensor.State.STATE_CONNECTING;
    status.number = 0;
    status.text = "--";

    connect();
  }

  @Override
  public void startSensor()
  {
    server_sensor.start_sending();

    fp_distance = 0.00;
    fp_strides = 0;
  }

  @Override
  public void stopSensor()
  {
    server_sensor.stop_sending();
    super.stopSensor();
  }

  @Override
  public void destroy()
  {
    super.destroy();
    base_IPluginAccessResultReceiver = null;
  }

  private double get_fp_speed(double speed)
  {
    double ret = 0;
    // fp_buffer[fp_counter]=(int)speed;
    // int tmp=(int) Math.round(speed*3.6); // Speed in km/h
    // if (tmp<7) {tmp=7;}
    // fp_buffer[fp_counter]=(int) (speed*1000*(main.fp_calibration_factor[tmp-7]/1000.00));
    fp_buffer[fp_counter] = (int) (speed * 1000 * (fp_calib_double));
    fp_counter++;
    if( fp_counter == 5 )
    {
      fp_counter = 0;
    }
    for(int x = 0; x < 5; x++)
    {
      ret += fp_buffer[x];
    }

    // Log.d(LOG_TAG, "get_fp_speed: speed=" + speed + " fp_calib_int: " + fp_calib_int + " ret: " + ret);

    return ret / 5;
  }

  public void reset(final int serial)
  {
    if( internal_sensor != null )
    {
      internal_sensor.releaseAccess();
      internal_sensor = null;
    }

    fp_device_id = serial;

    if( !killed )
      AntPlusStrideSdmPcc.requestAccess(ctx, serial, 10, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
  }

  // GET ALREADY CALIBRATED FP_SPEED IN mm/second
  public void subscribeToEvents()
  {
    // It can happen that a stop was requested and we reconnect before we are properly shut down
    if( killed )
      return;

    ((AntPlusStrideSdmPcc) internal_sensor).subscribeInstantaneousSpeedEvent(new IInstantaneousSpeedReceiver() {
      @Override public void onNewInstantaneousSpeed(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
          final BigDecimal instantaneousSpeed)
      {
        fp_speed = get_fp_speed(instantaneousSpeed.doubleValue());
        store.setCurrentSpeed(fp_speed, ANT_FootPodSensor.this);
        status.text = String.format("%2.1f", store.getCurrentSpeed_kmh());

        server_sensor.setCurrentSpeed(fp_speed);
      }
    });

    // GET ALREADY CALIBRATED FP_DISTANCE IN m
    ((AntPlusStrideSdmPcc) internal_sensor).subscribeDistanceEvent(new IDistanceReceiver() {
      double cumDist;
      double dist;

      @Override public void onNewDistance(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final BigDecimal cumulativeDistance)
      {
        cumDist = cumulativeDistance.doubleValue();
        if( cumDist > fp_last_distance )
        {
          dist = cumDist - fp_last_distance;
          fp_last_distance = cumDist;
        }
        else
        {
          if( cumDist != 0 )
          {
            if( fp_last_distance - cumDist < 0.1 )
            {
              dist = 0;
            }
            else
            {
              dist = cumDist; // TODO: check if this is ok
              fp_last_distance = dist;
            }
          }
          else
          {
            fp_last_distance = dist = 0;
          }
        }

        fp_distance += dist * fp_calib_double;

        store.setDistance((long) fp_distance, ANT_FootPodSensor.this);
        server_sensor.setDistance(fp_distance);
      }
    });

    // GET FP_STRIDE_COUNT
    ((AntPlusStrideSdmPcc) internal_sensor).subscribeStrideCountEvent(new IStrideCountReceiver() {
      long strides;

      @Override public void onNewStrideCount(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long cumulativeStrides)
      {
        if( cumulativeStrides >= fp_last_strides )
        {
          strides = cumulativeStrides - fp_last_strides;
          fp_last_strides = cumulativeStrides;
        }
        else
        {
          if( cumulativeStrides != 0 )
          {
            strides = cumulativeStrides; // TODO: check if this is ok
            fp_last_strides = strides;
          }
          else
          {
            fp_last_strides = strides = 0;
          }
        }

        fp_strides += strides;

        server_sensor.setStrideCount(fp_strides);
      }
    });

    ((AntPlusStrideSdmPcc) internal_sensor).subscribeCalorieDataEvent(new ICalorieDataReceiver() {
      @Override public void onNewCalorieData(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long cumulativeCalories)
      {
        fp_calories = cumulativeCalories;
        server_sensor.setCalories(fp_calories);
      }
    });

    // GET FP SERIAL_ID
    ((AntPlusStrideSdmPcc) internal_sensor).subscribeProductInformationEvent(new IProductInformationReceiver() {
      @Override public void onNewProductInformation(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
          final int mainSoftwareRevision, final int supplementalSoftwareRevision, final long serialNumber)
      {
        if( Config.LOGGING )
          Log.d("ANT_FP",
              estTimestamp + " " + eventFlags + " " + mainSoftwareRevision + " " + supplementalSoftwareRevision + " " + serialNumber);
        // main.set_fp_device_id((int) serialNumber);
      }
    });

    // GET FP_STATUS
    ((AntPlusStrideSdmPcc) internal_sensor).subscribeSensorStatusEvent(new ISensorStatusReceiver() {
      @Override public void onNewSensorStatus(final long estTimestamp, EnumSet<EventFlag> eventFlags, final SensorLocation sensorLocation,
          final BatteryStatus batteryStatus, final SensorHealth sensorHealth, final SensorUseState useState)
      {

      }
    });

  } // END SUBSCRIBE TO EVENTS

  @Override
  public ServerSensor getServerSensor()
  {
    return server_sensor;
  }

  @Override
  public void resetMetrics()
  {
    fp_distance = 0;
    fp_strides = 0;
  }
}
