package at.pegasos.client.sensors;

import android.content.*;
import android.hardware.*;
import android.util.*;
import at.pegasos.serverinterface.*;
import at.univie.mma.Config;
import at.univie.mma.serverinterface.sensors.Acc3dSS;

public class AccelerationSensor extends Sensor implements SensorEventListener {
  private Acc3dSS server_sensor;

  android.hardware.Sensor accelerometer;
  SensorManager sm;

  public AccelerationSensor(Object starter)
  {
    super((Context) starter);

    sm = (SensorManager) ((Context) starter).getSystemService(Context.SENSOR_SERVICE);
    accelerometer = sm.getDefaultSensor(android.hardware.Sensor.TYPE_ACCELEROMETER); // ACCELEROMETER
  }

  @Override
  public Status getStatus()
  {
    return status;
  }

  @Override
  public void initSensor()
  {
    server_sensor= new Acc3dSS(4, 1000);
 
    sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);

    if( Config.LOGGING ) Log.d("AccelerationSensor", "STARTED");
    status.state= Sensor.State.STATE_CONNECTING;
    status.number= 0;
  }

  @Override
  public void startSensor() {
    if( Config.LOGGING ) Log.d("AccelerationSensor", "Start Sensor");
    server_sensor.start_sending();
  }

  @Override
  public void stopSensor() {
    sm.unregisterListener(this);
    server_sensor.stop_sending();
  }

  /**
   * Required Interface method. @author martin tampier
   */
  @Override
  public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {
    // TODO Auto-generated method stub
  }

  /**
   * Required Interface method. @author martin tampier
   */
  @Override
  public void onSensorChanged(SensorEvent event) {
    status.state= Sensor.State.STATE_OK;
    status.text= "OK";

    short x=(short) (event.values[0]*1000);
    short y=(short) (event.values[1]*1000);
    short z=(short) (event.values[2]*1000);

    server_sensor.setData(x, y, z);
  } // END METHOD

  @Override
  public ServerSensor getServerSensor() {
    return server_sensor;
  }

  @Override
  public void resetMetrics() {
    // TODO Auto-generated method stub
  }

  @Override
  protected void connect()
  {
    
  }
}
