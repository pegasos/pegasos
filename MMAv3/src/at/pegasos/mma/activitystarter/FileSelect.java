package at.pegasos.mma.activitystarter;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import at.pegasos.client.*;
import at.pegasos.mma.ui.dialog.ListDialog;
import at.univie.mma.ui.ActivityStarterFragment;

public class FileSelect extends ActivityStarterList<FileSelect> {

  private String directory;
  private String pattern;

  // Standard constructor is required
  public FileSelect()
  {

  }

  private FileSelect(FileSelect other) {
    super(other);
    this.directory= other.directory;
    this.pattern= other.pattern;
  }

  @Override
  public FileSelect copy() {
    return new FileSelect(this);
  }

  /**
   *
   * @param directory path relative to MMA base dir
   * @return copy
   */
  public FileSelect setDirectory(String directory)
  {
    FileSelect ret= new FileSelect(this);
    ret.directory= directory;
    return ret;
  }

  /**
   *
   * @param pattern inclusion pattern
   * @return copy
   */
  public FileSelect setPattern(String pattern)
  {
    FileSelect ret= new FileSelect(this);
    ret.pattern= pattern;
    return ret;
  }

  @Override
  public void onActivityClicked(final ActivityStarterFragment fragment)
  {
    List<String> filenames= new ArrayList<String>();
    final String basedir= new File(PegasosClient.getDirData().getParentFile().getAbsolutePath(),directory).getAbsolutePath();

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
    {
      Path dir= Paths.get(PegasosClient.getDirData().getParentFile().getAbsolutePath()).resolve(directory);
      try( DirectoryStream<Path> directoryStream= Files.newDirectoryStream(dir) )
      {
        for(Path path : directoryStream)
        {
          if( path.toFile().isFile() && path.getFileName().toString().matches(pattern) )
          {
            filenames.add(path.getFileName().toString());
          }
        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    else
    {
      File dir= new File(PegasosClient.getDirData().getParentFile(), directory);

      File[] filesList = dir.listFiles();
      for(File f : filesList)
      {
        if( f.isFile() && f.getName().matches(pattern) )
        {
          // System.out.println(f.getName());
          filenames.add(f.getName());
        }
      }
    }

    createDlg(filenames.toArray(new String[0]), new ListDialog.ListDialogOnYes() {
      @Override
      public void OnClick(String item) {
        addArgumentAndProceed(basedir + File.separator + item, ctx, fragment);
      }
    });
  }
}
