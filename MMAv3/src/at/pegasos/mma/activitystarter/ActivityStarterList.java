package at.pegasos.mma.activitystarter;

import android.app.Dialog;

import at.pegasos.mma.ui.dialog.ListDialog;

public abstract class ActivityStarterList<T extends ActivityStarterList<T>> extends StartActivityBase<T> {

  protected String title;
  protected int ititle;
  protected String label;
  protected int ilabel;

  public ActivityStarterList()
  {

  }

  public ActivityStarterList(ActivityStarterList other)
  {
    super(other);
    this.label= other.label;
    this.ilabel= other.ilabel;
    this.title= other.title;
    this.ititle= other.ititle;
  }

  public T setLabel(String label)
  {
    T ret= copy();
    ret.label= label;
    return ret;
  }

  public T setLabel(int label)
  {
    T ret= copy();
    ret.ilabel= label;
    return ret;
  }

  public T setTitle(String title)
  {
    T ret= copy();
    ret.title= title;
    return ret;
  }

  public T setTitle(int title)
  {
    T ret= copy();
    ret.ititle= title;
    return ret;
  }

  protected void createDlg(final String[] items, ListDialog.ListDialogOnYes onYes)
  {
    Dialog dialog= null;

    if( ititle != 0 && ilabel != 0)
      dialog= ListDialog.createListDialog(ctx, ititle, ilabel, items, onYes);
    if( ititle != 0 && ilabel == 0)
      dialog= ListDialog.createListDialog(ctx, ititle, label, items, onYes);
    if( ititle == 0 && ilabel != 0)
      dialog= ListDialog.createListDialog(ctx, title, ilabel, items, onYes);
    if( ititle == 0 && ilabel == 0)
      dialog= ListDialog.createListDialog(ctx, title, label, items, onYes);

    dialog.show();
  }
}
