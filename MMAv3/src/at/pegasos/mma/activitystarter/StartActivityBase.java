package at.pegasos.mma.activitystarter;

import android.content.Context;
import android.content.Intent;

import at.univie.mma.ui.ActivityStarterFragment;
import at.univie.mma.ui.SportSelectionActivity;
import at.pegasos.client.ui.TrainingActivity;
import at.univie.mma.ui.TrainingLandingScreen;

public abstract class StartActivityBase<T extends StartActivityBase<T>> implements ActivityStarterFragment.ActivityStarterCallback {

  protected Context ctx;
  protected int activity_c;
  protected String sport;
  protected Runnable command;
  protected String argument;
  /**
   * Name for the current parameter. If null this parameter has no name
   */
  protected String name;

  /**
   * Next action in the chain
   */
  protected StartActivityBase next;

  /**
   * Previous action in the chain
   */
  private StartActivityBase prev;

  public StartActivityBase()
  {

  }

  public abstract T copy();

  public StartActivityBase(StartActivityBase other)
  {
    this.ctx= other.ctx;
    this.activity_c= other.activity_c;
    this.command= other.command;
    this.sport= other.sport;
    this.argument= other.argument;
    this.name= other.name;
    this.next= other.next;
  }

  public T setContext(Context ctx)
  {
    T ret= copy();
    ret.ctx= ctx;
    return ret;
  }

  public T setController(int activity_c)
  {
    T ret= copy();
    ret.activity_c= activity_c;
    return ret;
  }

  public T setCommand(Runnable command)
  {
    T ret= copy();
    ret.command= command;
    return ret;
  }

  public T setSport(String sport)
  {
    T ret= copy();
    ret.sport= sport;
    return ret;
  }

  public T setArgument(String argument)
  {
    T ret= copy();
    ret.argument= argument;
    return ret;
  }

  public T setName(String name)
  {
    T ret= copy();
    ret.name= name;
    return ret;
  }

  private Intent createIntent(ActivityStarterFragment fragment)
  {
    Class cls;

    if( this.sport == null )
      cls= SportSelectionActivity.class;
    else
      cls= TrainingLandingScreen.class;

    Intent intent= new Intent(ctx, cls);

    intent.putExtra(TrainingLandingScreen.EXTRA_ACTIVITY, activity_c);
    if( this.sport != null )
      intent.putExtra(TrainingLandingScreen.EXTRA_SPORT, sport);

    if( this.argument != null )
      intent.putExtra(TrainingActivity.EXTRA_ARGUMENT, argument);

    ActivityStarterFragment.setParameters(fragment, intent);

    return intent;
  }

  protected void addArgumentAndProceed(final String parameter, final Context ctx, final ActivityStarterFragment fragment)
  {
    String oldargs= (argument != null ? argument + ";" : "");
    String newarg= (name != null ? name + "=" : "") + parameter;

    // Log.d("StartActivityBase", "addArgumentAndProceed " + this + " next:" + next + " prev:" + prev);
    if( next == null )
    {
      StartActivityBase base= this;
      // go to the first starter
      while( base.prev != null )
      {
        base= base.prev;
      }
      // Log.d("StartActivityBase", "addAAP createIntent from:" + base);
      Intent i= base.createIntent(fragment);

      // Log.d("StartActivityBase", "addAAP arguments:" + oldargs + newarg);
      i.putExtra(TrainingLandingScreen.EXTRA_ARGUMENTS, oldargs + newarg);

      if( command != null )
      {
        command.run();
      }

      ctx.startActivity(i);
    }
    else
    {
      if( next.argument == null )
      {
        next.argument= oldargs + newarg;
      }
      else
      {
        next.argument+= ";" + oldargs + newarg;
      }

      next.prev= this;
      next.onActivityClicked(fragment);
    }
  }

  public T addNext(StartActivityBase next)
  {
    T ret= copy();
    ret.next= next;
    next.setPrevious(this);
    return ret;
  }

  /**
   * It is important to implement this method correctly.
   * Copy all necessary objects here which should mainly be focused on the context ...
   * @param previous
   */
  protected void setPrevious(StartActivityBase previous)
  {
    this.ctx= previous.ctx;
    this.prev= previous;
  }
}
