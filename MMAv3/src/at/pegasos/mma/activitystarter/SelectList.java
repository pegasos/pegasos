package at.pegasos.mma.activitystarter;

import android.util.Log;

import at.pegasos.mma.ui.dialog.ListDialog;
import at.univie.mma.ui.ActivityStarterFragment;

public class SelectList extends ActivityStarterList<SelectList> {
  private String list;

  public SelectList()
  {

  }

  public String toString()
  {
    return "[label:" + label + ",list:" + list + ",activity_c:" + activity_c + ",sport:" + sport + ",command:" + command + ",argument:" + argument +"]";
  }

  @Override
  public SelectList copy()
  {
    Log.d("SelectList", "copy" + this);
    return new SelectList(this);
  }

  public SelectList(SelectList other)
  {
    super(other);
    this.list= other.list;
  }

  public SelectList setList(String list)
  {
    SelectList ret= new SelectList(this);
    Log.d("SelectList", "setList ret:" + ret);
    ret.list= list;
    Log.d("SelectList", "setList ret-ret:" + ret);
    return ret;
  }
  
  @Override
  public void onActivityClicked(final ActivityStarterFragment fragment)
  {
    createDlg(this.list.split(";"), new ListDialog.ListDialogOnYes() {
      @Override
      public void OnClick(String item) {
        addArgumentAndProceed(item, ctx, fragment);
      }
    });
  }
}
