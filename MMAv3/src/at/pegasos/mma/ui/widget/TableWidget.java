package at.pegasos.mma.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import at.univie.mma.ui.MeasurementFragment;

public abstract class TableWidget extends MeasurementFragment {

  /**
   * Whether or not to add a line between table rows. Default true.
   * Values ('true', 'false')
   */
  public static final String ARGUMENT_ADD_SEPARATOR= "ARGUMENT_ADD_SEPARATOR";

  /**
   * Default chronological order to be ascending (i.e. newest elements at the bottom).
   * Default true. Values ('true', 'false')
   */
  public static final String ARGUMENT_CHRON_DESC= "ARGUMENT_CHRON_DESC";

  /**
   * Whether or not to add a line between table rows
   */
  private boolean addSeparator= true;

  /**
   * Default chronological order to be ascending (i.e. newest elements at the bottom
   */
  private boolean desc= true;

  protected boolean headerAdded= false;

  int leftRowMargin=0;
  int topRowMargin=0;
  int rightRowMargin=0;
  int bottomRowMargin = 0;

  protected Context ctx;

  /**
   * View containing all the data
   */
  private TableLayout layout;

  /**
   * view containing the table
   */
  private ScrollView scroll;

  private int rows;

  private GestureDetector gestureDetector;

  @SuppressLint("ClickableViewAccessibility")
  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
  {
    scroll= new ScrollView(inflater.getContext());
    layout= new TableLayout(inflater.getContext());
    ctx= inflater.getContext();

    layout.setOrientation(LinearLayout.VERTICAL);
    layout.setStretchAllColumns(true);

    scroll.addView(layout);

    scroll.setOnTouchListener(new SwipeGestureDetector(inflater.getContext()));

    initialise(true);

    return scroll;
  }

  @Override
  protected void initialise(boolean set_init)
  {
    layout.removeAllViews();

    Bundle args= getArguments();
    if( args != null )
    {
      String p = args.getString(ARGUMENT_ADD_SEPARATOR);
      if( p != null && p.toLowerCase().equals("false") )
      {
        addSeparator= false;
      }
      p = args.getString(ARGUMENT_CHRON_DESC);
      if( p != null && p.toLowerCase().equals("false") )
      {
        desc= false;
      }
    }

    rows= 0;

    addHeader();
    headerAdded= true;
    populateTable();

    super.initialise(set_init);
  }

  /**
   * Populate the table with all data that is currently present. This method is
   * invoked when the view is put on the screen. It should display all data
   * currently present.
   */
  protected abstract void populateTable();

  protected abstract TextView[] getHeader();

  private void addHeader()
  {
    TextView[] header= getHeader();

    addRow(header);
    rows++;
  }

  /**
   * Utility method for adding items to the table
   * @param items string data to be displayed
   */
  protected void addRow(String[] items)
  {
    TextView row[]= new TextView[items.length];
    int i= 0;
    for(String text : items)
    {
      TextView t= new TextView(ctx);
      t.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
      // d.setGravity(Gravity.LEFT);
      t.setText(text);
      t.setBackgroundColor(Color.WHITE);
      t.setTextColor(Color.BLACK);
      t.setTextSize(30);

      row[i++]= t;
    }

    // now really add it
    addRow(row);
  }
  
  /**
   * Add a row to the table
   * 
   * @param items
   *          text views to be added
   */
  protected void addRow(TextView[] items)
  {
    // add table row
    final TableRow tr= new TableRow(ctx);
    // tr.setId(i + 1);
    TableLayout.LayoutParams trParams= new TableLayout.LayoutParams(
        TableLayout.LayoutParams.MATCH_PARENT,
        TableLayout.LayoutParams.WRAP_CONTENT);
    trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
    tr.setPadding(0, 0, 0, 0);
    tr.setLayoutParams(trParams);

    for(TextView t : items)
    {
      tr.addView(t);
    }

    if( desc || rows < 2 )
    {
      layout.addView(tr, trParams);
    }
    else
    {
      layout.addView(tr, 1, trParams);
    }

    if( addSeparator )
    {
      // add separator row
      final TableRow trSep= new TableRow(ctx);
      TableLayout.LayoutParams trParamsSep= new TableLayout.LayoutParams(
          TableLayout.LayoutParams.MATCH_PARENT,
          TableLayout.LayoutParams.WRAP_CONTENT);
      trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
      
      trSep.setLayoutParams(trParamsSep);
      TextView tvSep= new TextView(ctx);
      TableRow.LayoutParams tvSepLay= new TableRow.LayoutParams(
          TableRow.LayoutParams.MATCH_PARENT,
          TableRow.LayoutParams.WRAP_CONTENT);
      tvSepLay.span= 4;
      tvSep.setLayoutParams(tvSepLay);
      tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"));
      tvSep.setHeight(1);
      
      trSep.addView(tvSep);
      if( desc || rows < 2 )
      {
        layout.addView(trSep, trParamsSep);
      }
      else
      {
        layout.addView(trSep, 2, trParamsSep);
      }
    }
    rows++;
  }
}
