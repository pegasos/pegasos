package at.pegasos.mma.ui.widget;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import at.univie.mma.Config;
import at.univie.mma.ui.ScreenManager;

public class SwipeGestureDetector implements View.OnTouchListener {

  private final GestureDetector gestureDetector;

  public SwipeGestureDetector(Context ctx)
  {
    gestureDetector= new GestureDetector(ctx, new SwipeGestureListener());
  }

  @Override
  public boolean onTouch(View v, MotionEvent event)
  {
    if( event.getAction() == MotionEvent.ACTION_UP )
    {
      // For this particular app we want the main work to happen
      // on ACTION_UP rather than ACTION_DOWN. So this is where
      // we will call performClick().
      v.performClick();
    }

    return gestureDetector.onTouchEvent(event);
  }

  private final class SwipeGestureListener extends GestureDetector.SimpleOnGestureListener {

    @Override
    public boolean onDown(MotionEvent e)
    {
      return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
      boolean result= false;
      try
      {
        float diffY= e2.getY() - e1.getY();
        float diffX= e2.getX() - e1.getX();
        if( Math.abs(diffX) > Math.abs(diffY) )
        {
          if( Math.abs(diffX) > Config.SWIPE_THRESHOLD && Math.abs(velocityX) > Config.SWIPE_VELOCITY_THRESHOLD )
          {
            if( diffX > 0 )
            {
              ScreenManager.getInstance().rotate(1);
              result= true;
            }
            else
            {
              ScreenManager.getInstance().rotate(-1);
              result= true;
            }
          }
        }
      }
      catch(Exception exception)
      {
        exception.printStackTrace();
      }
      return result;
    }
  }
}
