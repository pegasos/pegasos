package at.pegasos.mma.ui.training;

import android.graphics.Bitmap;
import android.graphics.drawable.*;
import android.support.v4.content.res.ResourcesCompat;
import android.util.TypedValue;
import android.widget.*;

import java.util.*;

import at.univie.mma.R;
import at.pegasos.client.sensors.SensorControllerService;
import at.pegasos.client.sensors.*;
import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.client.ui.TrainingActivity;

public class SensorStatusDisplay {
  private TrainingActivity training;

  /**
   * Dimension applied to icons when adding them to the display
   */
  private int dimensionInDp;

  /**
   * Layout Param used for adding new icons
   */
  private LinearLayout.LayoutParams p;

  /**
   * Container for status display
   */
  private LinearLayout container;

  private Map<Sensor, LayerDrawable> sensor_icon;

  /**
   * Drawable for displaying the network status
   */
  private LayerDrawable network_icon;

  public SensorStatusDisplay()
  {
    this.sensor_icon= new HashMap<Sensor, LayerDrawable>();
  }

  public void setUp(TrainingActivity training)
  {
    this.training= training;
    
    dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, training.getResources().getDimension(R.dimen.img_size_training), training.getResources().getDisplayMetrics());
    p= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    
    container= (LinearLayout) training.findViewById(R.id.sensor_status);

    addNetwork();
    addSensors();
  }

  public void setVisibilities()
  {
    if( network_icon != null )
    {
      network_icon.findDrawableByLayerId(R.id.line).setAlpha(ServerInterface.getInstance().isConnected() ? 0 : 255);
    }

    for(Sensor sensor : sensor_icon.keySet())
    {
      setVisibility(sensor);
    }
  }

  private void setVisibility(Sensor sensor)
  {
    boolean ok= sensor.getStatus().state == Sensor.State.STATE_OK;

    sensor_icon.get(sensor).findDrawableByLayerId(R.id.line).setAlpha(ok ? 0 : 255);
  }
  
  private void addSensors()
  {
    Sensor[] sensors= SensorControllerService.getInstance().getUsedSensors();
    
    for(Sensor sensor : sensors)
    {
      addSensor(sensor);
    }
  }

  private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight)
  {
    if( maxHeight > 0 && maxWidth > 0 )
    {
      int width = image.getWidth();
      int height = image.getHeight();
      float ratioBitmap = (float) width / (float) height;
      float ratioMax = (float) maxWidth / (float) maxHeight;

      int finalWidth = maxWidth;
      int finalHeight = maxHeight;
      if( ratioMax > ratioBitmap )
      {
        finalWidth = (int) ((float)maxHeight * ratioBitmap);
      }
      else
      {
        finalHeight = (int) ((float)maxWidth / ratioBitmap);
      }
      image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
      return image;
    }
    else
    {
      return image;
    }
  }

  /**
   * Add an image with the specified ID to the container
   * @param id ID of image to add
   * @return layerDrawable containing the image
   */
  private LayerDrawable addImage(int id)
  {
    LayerDrawable layerDrawable = (LayerDrawable) ResourcesCompat.getDrawable(training.getResources(), R.drawable.sensorstatus, null);
    layerDrawable.mutate();

    // Load image
    BitmapDrawable newImage = (BitmapDrawable) training.getResources().getDrawable(id);
    layerDrawable.setDrawableByLayerId(R.id.image,
      new BitmapDrawable(training.getResources(), resize(newImage.getBitmap(), dimensionInDp, dimensionInDp)));

    // create the image which will be added to the screen
    ImageView img= new ImageView(training);
    img.setImageDrawable(layerDrawable);

    // add it
    container.addView(img, p);

    // img.requestLayout();
    img.getLayoutParams().width= dimensionInDp;
    img.getLayoutParams().height= dimensionInDp;
    img.requestLayout();

    return layerDrawable;
  }
  
  private void addSensor(Sensor sensor)
  {
    int id= Sensors.getIcon(sensor);

    LayerDrawable layerDrawable = addImage(id);

    sensor_icon.put(sensor, layerDrawable);

    setVisibility(sensor);
  }

  private void addNetwork()
  {
    network_icon= addImage(R.drawable.icon_connected);
    network_icon.findDrawableByLayerId(R.id.line).setAlpha(ServerInterface.getInstance().isConnected() ? 0 : 255);
  }
}
