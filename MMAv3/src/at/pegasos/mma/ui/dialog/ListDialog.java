package at.pegasos.mma.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import at.univie.mma.R;

public class ListDialog {
  public interface ListDialogOnYes {
    /**
     * User has clicked on yes / positive button
     * @param item selected item
     */
    void OnClick(String item);
  }

  public interface ListDialogOnCancel {
    /**
     * User has clicked on no / negative button
     */
    void OnClick();
  }

  public static Dialog createListDialog(final Context ctx, int title, int label, final String[] items, final ListDialogOnYes onYes, final ListDialogOnCancel onCancel)
  {
    String t= null;
    if( title != 0 )
    {
      t= ctx.getResources().getString(title);
    }

    String l= null;
    if( label != 0 )
    {
      l= ctx.getResources().getString(label);
    }

    return createListDialog(ctx, t, l, items, onYes, onCancel);
  }

  public static Dialog createListDialog(final Context ctx, int title, int label, final String[] items, final ListDialogOnYes onYes)
  {
    String t= null;
    if( title != 0 )
    {
      t= ctx.getResources().getString(title);
    }

    String l= null;
    if( label != 0 )
    {
      l= ctx.getResources().getString(label);
    }

    return createListDialog(ctx, t, l, items, onYes, null);
  }

  public static Dialog createListDialog(final Context ctx, String title, int label, final String[] items, final ListDialogOnYes onYes, final ListDialogOnCancel onCancel)
  {
    String l= null;
    if( label != 0 )
    {
      l= ctx.getResources().getString(label);
    }

    return createListDialog(ctx, title, l, items, onYes, onCancel);
  }

  public static Dialog createListDialog(final Context ctx, int title, String label, final String[] items, final ListDialogOnYes onYes, final ListDialogOnCancel onCancel)
  {
    String t= null;
    if( title != 0 )
    {
      t= ctx.getResources().getString(title);
    }

    return createListDialog(ctx, t, label, items, onYes, onCancel);
  }

  public static Dialog createListDialog(final Context ctx, int title, String label, final String[] items, final ListDialogOnYes onYes)
  {
    String t= null;
    if( title != 0 )
    {
      t= ctx.getResources().getString(title);
    }

    return createListDialog(ctx, t, label, items, onYes, null);
  }

  public static Dialog createListDialog(final Context ctx, String title, String label, final String[] items, final ListDialogOnYes onYes)
  {
    return createListDialog(ctx, title, label, items, onYes, null);
  }

  public static Dialog createListDialog(final Context ctx, String title, int label, final String[] items, final ListDialogOnYes onYes)
  {
    return createListDialog(ctx, title, label, items, onYes, null);
  }

  public static Dialog createListDialog(final Context ctx, String title, String label, final String[] items, final ListDialogOnYes onYes, final ListDialogOnCancel onCancel)
  {
    AlertDialog.Builder builder= new AlertDialog.Builder(ctx);

    if( title != null )
      builder.setTitle(title);

    LinearLayout layout = new LinearLayout(ctx);
    layout.setOrientation(LinearLayout.VERTICAL);

    if( label != null )
    {
      TextView txtLbl= new TextView(ctx);
      txtLbl.setText(label);
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
      {
        txtLbl.setTextAppearance(ctx, android.R.style.TextAppearance_Medium);
      }
      else
      {
        txtLbl.setTextAppearance(android.R.style.TextAppearance_Medium);
      }
      layout.addView(txtLbl);
    }

    ArrayAdapter<String> adapter= new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, items);
    final ListView listView= new ListView(ctx);
    listView.setAdapter(adapter);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    listView.setSelector(android.R.color.darker_gray);
    listView.setSelection(0);

    layout.addView(listView);

    builder.setView(layout);

    builder.setPositiveButton(R.string.start_with_entered_distance,
            new DialogInterface.OnClickListener() {

              public void onClick(DialogInterface dialog, int id)
              {
                // selected ...
                int idx= listView.getCheckedItemPosition();
                if( idx == ListView.INVALID_POSITION )
                {
                  idx= 0;
                }
                String item= items[idx];
                dialog.dismiss();
                onYes.OnClick(item);
              }
            });

    builder.setNegativeButton(R.string.cancel_distance_start,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id)
              {
                dialog.dismiss();
                if( onCancel != null )
                  onCancel.OnClick();
              }
            });

    return builder.create();
  }
}
