package at.pegasos.mma.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class PictureHelper {
  public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
  {
    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options= new BitmapFactory.Options();
    options.inJustDecodeBounds= true;
    BitmapFactory.decodeFile(path, options);
    
    // Calculate inSampleSize
    options.inSampleSize= calculateInSampleSize(options, reqWidth, reqHeight);
    
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds= false;
    return BitmapFactory.decodeFile(path, options);
  }

  public static Bitmap decodeSampledBitmapFromFile(String path, int reqSize)
  {
    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options= new BitmapFactory.Options();
    options.inJustDecodeBounds= true;
    BitmapFactory.decodeFile(path, options);

    // Calculate inSampleSize
    options.inSampleSize= estimateInSampleSize(options, reqSize);

    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds= false;
    return BitmapFactory.decodeFile(path, options);
  }

  public static int estimateInSampleSize(BitmapFactory.Options options, int reqSize)
  {
    final int height= options.outHeight;
    final int width= options.outWidth;
    int inSampleSize= 1;

    if( height > reqSize || width > reqSize )
    {
      int size= width;
      if( height > width )
      {
        size= height;
      }
      // inSampleSize= Math.pow(Math.ceil(Math.l))
      inSampleSize= (int)Math.pow(2, (int) Math.ceil(Math.log(size / reqSize) / Math.log(2)));
    }

    return inSampleSize;
  }
  
  public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
  {
    // Raw height and width of image
    final int height= options.outHeight;
    final int width= options.outWidth;
    int inSampleSize= 1;
    
    if( height > reqHeight || width > reqWidth )
    {
      final int halfHeight= height / 2;
      final int halfWidth= width / 2;
      
      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // height and width larger than the requested height and width.
      while( (halfHeight / inSampleSize) > reqHeight
          && (halfWidth / inSampleSize) > reqWidth )
      {
        inSampleSize*= 2;
      }

      Log.d("PH", halfHeight + "/" + halfWidth + " " + inSampleSize + " " + reqHeight + "/" + reqWidth);
    }
    
    return inSampleSize;
  }
}
