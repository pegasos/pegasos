package at.univie.mma;

public class Config
{
    /** Whether or not to include logging statements in the application. */
    public final static boolean LOGGING = @config.logging@;
    
    public final static String BUILD_TIME="@time.stamp@";
    
    public final static String MENUCLASS= "@menu.class@";
    
    public final static String APP_NAME= "@app.name@";
    
    /** Where to find the MMA server **/
    public final static String MMA_SERVER_IP = "@config.server_ip@"; 
    public final static int    MMA_SERVER_PORT = @config.server_port@;
    
    /** Sampling rates for ANT devices **/
    public final static int ANT_FOOTPOD_SAMPLING_RATE= @config.ant_footpod_samplingrate@;
    public final static int ANT_HEARTRATE_SAMPLING_RATE= @config.ant_heartrate_samplingrate@;
    
    /** Datasets per Packet **/
    public final static int DATASETS_PERPACKET_HR= @config.datasets_perpacket_hr@;
    public final static int DATASETS_PERPACKET_FOOTPOD= @config.datasets_perpacket_footpod@;
    public final static int DATASETS_PERPACKET_GPS= @config.datasets_perpacket_gps@;
    
    /** UI Configuration **/
    public final static int LOGIN_TIMEOUT=@config.ui_login_timeout@;
    public final static boolean LOGIN_TIMEOUT_SHOWDIALOG=@config.ui_login_timeout_dialog@;
    
    /** GPS-Sensor Configuration **/
    public final static int MIN_REQUIRED_SATELLITES_GPS=@config.min_required_satellites_gps@;
}

