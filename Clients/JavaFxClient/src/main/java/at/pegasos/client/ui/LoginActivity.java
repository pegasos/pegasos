package at.pegasos.client.ui;

import android.content.*;
import android.os.*;
import at.pegasos.client.ui.controller.*;
import at.univie.mma.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.web.*;
import javafx.stage.*;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.security.*;

public class LoginActivity extends PegasosClientView {

  private Stage browserWindow;

  class Browser extends StackPane {
    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();

    final String clientId = "pegasos-client";
    final String redirectURI = "http://jafx-pegaos-client/login";

    public Browser() throws NoSuchAlgorithmException, UnsupportedEncodingException {
      //add the web view to the scene
      getChildren().add(browser);

      final String loginURL = controller.createLoginURL(clientId, redirectURI);

      System.out.println(loginURL);

      // load the web page
      webEngine.load(loginURL);

      webEngine.locationProperty().addListener((observable, oldValue, newValue) -> {
        if (controller.codeResponse(newValue)) {
          browserWindow.close();
        }
      });
    }

    @Override protected void layoutChildren() {
      double w = getWidth();
      double h = getHeight();
      layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }

    @Override protected double computePrefWidth(double height) {
      return 750;
    }

    @Override protected double computePrefHeight(double width) {
      return 850;
    }
  }

  public static final String TAG= LoginActivity.class.getSimpleName();

  public static final int REQUEST_LOGIN= 111;

  /**
   * Button to perform the login action
   */
  @FXML private Button btnLogin;
  
  @FXML private ComboBox<String> personSelect;
  
  private LoginUIController controller;
  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    setContentView("LoginActivity.fxml");
    this.controller= new LoginUIController(this);
    
    this.mainApp.getMainActivity().getResponseCallback().addMessageListener(controller);

    // TODO:
    // TextView regist= (TextView) findViewById(R.id.tv_register);
    // regist.setMovementMethod(LinkMovementMethod.getInstance());

    personSelect.valueProperty().addListener((observable, oldValue, newValue) -> {
      controller.setUserFromList(newValue);
    });
    
    if( Config.LOGIN_USERLIST )
    {
      updateUserListGUI();
    }
    
    if( Config.LOGIN_USERLIST )
    {
      if( controller.getUserList() != null && controller.getUserList().length == 0 )
      {
        btnLogin.setDisable(true);
      }
    }
    else
    {
      personSelect.setVisible(false);
      personSelect.setManaged(false);
    }
  }
  
  /*
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    if( Config.LOGIN_USERLIST )
    {
      getMenuInflater().inflate(R.menu.login, menu);
      return true;
    }
    else
      return false;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id= item.getItemId();
    
    if( Config.LOGIN_USERLIST )
    {
      if( id == R.id.action_add_user )
      {
        addUser();
        return true;
      }
      else if( id == R.id.action_remove_current_user )
      {
        if( users.length > 0 )
          removeCurrentUser();
        else
        {
          // TODO: report mistake (cannot remove non-existing user)
        }
        return true;
      }
    }
    
    return super.onOptionsItemSelected(item);
  }*/
  
  @FXML
  private void onLoginClicked(ActionEvent event)
  {
    LoginActivity.this.runOnUiThread(new Runnable() {
      @Override
      public void run()
      {
        btnLogin.setDisable(true);
      }
    });

    Browser b = null;
    try {
      b = new Browser();

      browserWindow = new Stage();
      browserWindow.setTitle("My New Stage Title");

      browserWindow.setScene(new Scene(b, 450, 450));
      browserWindow.show();

      // TODO: disable current view / us?
      controller.OnLoginClicked();
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  @FXML
  private void onRegisterClicked(ActionEvent event) {
    (new Thread() {
      public void run() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        System.out.println("Register clicked " + desktop);
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
          try {
            // desktop.browse(new URI("https://mma2.schmelz.univie.ac.at/register"));
            desktop.browse(new URI(resources.getString("registration_link")));
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }).start();
  }
  
  private void removeCurrentUser()
  {
    /*
    TODO
    int i, j;
    int k= Sp_usr.getSelectedItemPosition();
    String[] user_list2= new String[users.length - 1];
    String[] pwd_list2= new String[users.length - 1];
    
    for(i= 0, j= 0; i < users.length; i++)
    {
      if( i != k )
      {
        user_list2[j]= users[i];
        pwd_list2[j]= pwd_list[i];
        j++;
      }
    }
    
    users= user_list2;
    pwd_list= pwd_list2;
    
    storeUserList();
    
    updateUserListGUI();
    */
  }
  
  /*
  @SuppressLint("InflateParams") // As this was copied from
                                 // https://developer.android.com/guide/topics/ui/dialog.html
  private void addUser()
  {
    // 1. Instantiate an AlertDialog.Builder with its constructor
    AlertDialog.Builder builder= new AlertDialog.Builder(this);
    
    LayoutInflater inflater= getLayoutInflater();
    
    View dialog= inflater.inflate(R.layout.dialog_add_user, null);
    final EditText user= (EditText) dialog.findViewById(R.id.username);
    final EditText pwd= (EditText) dialog.findViewById(R.id.pwd);
    builder.setPositiveButton(R.string.add, null);
    builder.setNegativeButton(R.string.cancel_distance_start, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id)
      {
        dialog.dismiss();
      }
    });
    builder.setView(dialog);
    
    final AlertDialog dlg= builder.create();
    dlg.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface dialog)
      {
        Button addBtn= dlg.getButton(DialogInterface.BUTTON_POSITIVE);
        addBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v)
          {
            String[] user_list2= new String[users.length + 1];
            String[] pwd_list2= new String[users.length + 1];
            boolean validInput;
  
            for(int i= 0; i < users.length; i++)
            {
              user_list2[i]= users[i];
              pwd_list2[i]= pwd_list[i];
            }
  
            String userString= user.getText().toString();
            String pwdString= pwd.getText().toString();
  
            if( userString.isEmpty() || !userString.contains("@") || pwdString.isEmpty() )
            {
              validInput= false;
              Toast.makeText(LoginActivity.this, getString(R.string.invalid_email_pwd), Toast.LENGTH_SHORT).show();
            }
            else
            {
              validInput= true;
              user_list2[users.length] = userString;
              pwd_list2[users.length] = pwdString;
            }
  
            if( validInput )
            {
              users= user_list2;
              pwd_list= pwd_list2;
    
              storeUserList();
              dlg.dismiss();
              updateUserListGUI();
            }
          }
        });
      }
    });
    
    dlg.show();
  }*/
  
  /**
   * Load the user list and display it on the GUI. Hence this method needs to be
   * run on the GUI thread
   */
  private void updateUserListGUI()
  {
    controller.loadUserList();
    
    personSelect.getItems().setAll(controller.getUserList());
    
    if( controller.getUserList().length == 0 )
    {
      btnLogin.setDisable(true);
    }
    else
    {
      // TODO: Do this only when we are online
      btnLogin.setDisable(false);
    }
  }
  
  @Override
  public void onBackPressed()
  {
    // Remove message listener
    this.mainApp.getMainActivity().getResponseCallback().removeMessageListener(controller);
    
    setResult(RESULT_CANCELED, null);
    
    finish();
  }

  public void OnLoginOk()
  {
    browserWindow.close();
    setResult(LoginActivity.RESULT_OK, null);
    finish();
  }

  public void OnLoginFailed()
  {
    showToastText("Login fehlgeschlagen");
    
    // TODO: show a better notification / translation
    runOnUiThread(new Runnable() {
      
      @Override
      public void run()
      {
        btnLogin.setDisable(false);              
      }
    });
  }
}
