package at.pegasos.client.util;

import at.pegasos.client.*;

import java.io.*;
import java.util.*;

public class PreferenceManager {
  
  private final static String FILE= PegasosClient.getDirConfig() + "/.prefs";
  
  private static PreferenceManager instance;
  
  private Properties props;

  public static synchronized PreferenceManager getInstance()
  {
    if( instance == null )
      instance= new PreferenceManager();
    return instance;
  }
  
  protected PreferenceManager()
  {
    load();
  }
  
  private void load()
  {
    Properties inProps = new Properties();
    InputStream inputStream;
    try
    {
      File in= new File(PegasosClient.getDirConfig() + "/.prefs");
      if( !in.exists() )
      {
        return;
      }

      inputStream = new FileInputStream(in);
      inProps.load(inputStream);
      props = new Properties();
      inProps.forEach((key,value) -> {
        if( ((String)key).startsWith("_") )
        {
          props.setProperty(((String)key).substring(1), (String) value);
        }
        else
        {
          props.put(((String)key).substring(1), new HashSet<String>(Arrays.asList(((String) value).split("###"))));
        }
      });
    }
    catch(IOException e)
    {
      // TODO:
    }
  }
  
  private void store()
  {
    File f = new File(FILE);
    OutputStream out;
    try
    {
      out = new FileOutputStream( f );
      System.out.println(props);
      Properties toWrite = new Properties();
      props.forEach((key, value) -> {
        if( value instanceof Set )
        {
          toWrite.put("-" + key, String.join("###", (Set<String>)value));
        }
        else
        {
          toWrite.put("_" + key, value);
        }
      });
      toWrite.store(out, "This is an optional header comment string");
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
      // ACRA.getErrorReporter().handleException(e);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      // ACRA.getErrorReporter().handleException(e);
      // TODO
    }
  }

  public int getInt(String name, int defaultValue)
  {
    if( !props.containsKey(name) )
      return defaultValue;
    return Integer.parseInt(props.getProperty(name));
  }

  public void storeInt(String name, int value)
  {
    props.put(name, "" + value);
    if( !editing )
      store();
  }

  public String getString(String name, String defaultValue)
  {
    return props.getProperty(name, defaultValue);
  }
  
  public boolean getBoolean(String name, boolean defaultValue)
  {
    if( !props.containsKey(name) )
      return defaultValue;
    return Boolean.parseBoolean(props.getProperty(name));
  }
  
  public void remove(String name)
  {
    props.remove(name);
  }

  public void storeBoolean(String name, boolean value)
  {
    props.put(name, "" + value);
    if( !editing )
      store();
  }

  // We ignore this hoping that the value has been a set before
  @SuppressWarnings("unchecked")
  public Set<String> getStringSet(String name, Set<String> defaultValue)
  {
    return (Set<String>) props.getOrDefault(name, defaultValue);
  }
  
  public void storeStringSet(String name, Set<String> value)
  {
    props.put(name, value);
    if( !editing )
      store();
  }

  public void storeDouble(String name, double value)
  {
    props.put(name, "" + value);
    if( !editing )
      store();
  }
  public double getDouble(String name, double defaultValue)
  {
    if( !props.containsKey(name) )
      return defaultValue;
    return Double.parseDouble(props.getProperty(name));
  }

  public void storeLong(String name, long value)
  {
    props.put(name, "" + value);
    if( !editing )
      store();
  }
  public double getLong(String name, long defaultValue)
  {
    if( !props.containsKey(name) )
      return defaultValue;
    return Long.parseLong(props.getProperty(name));
  }
  
  private boolean editing= false;
  
  public void startEdit()
  {
    if( editing )
      throw new IllegalStateException("Already editing");
    editing= true;
  }
  
  public void apply()
  {
    store();
    editing= false;
  }

  public void storeString(String name, String value)
  {
    props.put(name, value);
    if( !editing )
      store();
  }
}
