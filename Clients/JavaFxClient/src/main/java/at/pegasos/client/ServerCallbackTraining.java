package at.pegasos.client;

import at.pegasos.client.controller.*;
import at.pegasos.client.ui.*;
import at.pegasos.client.ui.controller.*;
import at.pegasos.client.util.*;
import at.pegasos.serverinterface.response.*;
import at.univie.mma.*;
import at.univie.mma.serverinterface.*;
import at.univie.mma.serverinterface.response.*;

public class ServerCallbackTraining implements ResponseCallback {
	
	private final TrainingActivity activity;
	private final Soundmachine soundmachine;
	private ITrainingController controller;
  
	public ServerCallbackTraining(TrainingActivity activity) {
		this.activity= activity;
		this.soundmachine= activity.getSoundmachine();
	}
	
	private boolean disconnect_reported= false;
	private long disconnect_reported_time;
  private int login_required_counts= 0;
  private int session_required_counts= 0;
  private boolean restart_required= false;
  
  public void setController(ITrainingController controller)
  {
    this.controller= controller;
  }
	
  @Override
  public void messageRecieved(ServerResponse r)
  {
    if( r != null )
    {
      PegasosLog.d("Message recieved", (r.getRaw() != null ? r.getRaw() : "null"));
			
			if( controller != null && controller.onMessage(r) )
			  return;
			
			if(r instanceof FeedbackMessage )
			{
				PegasosLog.e("Mesaage", ((FeedbackMessage) r).getMessage());
				readMessage(((FeedbackMessage) r).getMessage());
			}
			else if( r instanceof StartSessionMessage )
			{
			  PegasosLog.d("Message1", "Session started");
				session_required_counts= 0;
				activity.onSessionStarted();
			}
      else if( r instanceof LoginMessage)
      {
        login_required_counts= 0;
        
        if( restart_required )
        {
          ServerInterface.getInstance().restartSession();
        }
        
        activity.showToastText("Login Message recieved");
      }
			else if( r instanceof ActivityStoppedMessage )
			{
				//TODO: Handle this
				activity.showToastText("Activity Stopped");
				readMessage("Activity ended");
			}
			else if( r instanceof ServerDisconnectedMessage ) {
			  PegasosLog.d("Training", "Server disconnected recieved");
				
				if( disconnect_reported )
				{
					long delta= System.currentTimeMillis() - disconnect_reported_time;
					if( delta > Config.SILENT_SERVER_DISCONNECTED_RETELL_INTERVALL )
					{
						disconnect_reported_time= System.currentTimeMillis();
						if( !Config.SILENT_SERVER_DISCONNECTED_RETELL ) readMessage(activity.getString("server_connection_lost_retell"));
					}
				}
				else
				{
					disconnect_reported= true;
					disconnect_reported_time= System.currentTimeMillis();
					if( !Config.SILENT_SERVER_DISCONNECTED ) readMessage(activity.getString("server_connection_lost"));
					// ServerInterface.getInstance().stopSending(false);
					ServerInterface.getInstance().connect();
					
					activity.setSensorIconsVisibilityGuiThread();
				}
			}
			else if( r instanceof ServerReconnected ) {
				disconnect_reported = false;

				if( !Config.SILENT_SERVER_RECONNECTED ) readMessage(activity.getString("server_connection_reestablished"));
        LoginData l = LoginData.getLoginData();
        if( l == null )
          CrashReporter.handleException(new Exception("Das sollte nicht passieren. LoginData ist null"));
        else
          Tasks.login(l);

				restart_required = true;

				activity.setSensorIconsVisibilityGuiThread();
			}
			else if( r instanceof RestartSessionMessage ) {
				if( restart_required )
				{
					restart_required= false;
					ServerInterface.getInstance().startSending();
				}
				session_required_counts= 0;
			}
      else if( r instanceof ControlCommand )
      {
        ControlCommand command= (ControlCommand) r;
        if( command.getType() == ControlCommand.TYPE_LOGIN_REQUIRED )
        {
          login_required_counts++;
        }
        else if( command.getType() == ControlCommand.TYPE_SESSION_REQUIRED )
        {
          session_required_counts++;
          // if the server sends this only once we can assume that it handled automatically
          if( session_required_counts > 2 )
          {
            Tasks.restartSession();
            if( session_required_counts > 3 )
            {
              // readMessage("Mehr als drei Mal session required. Sollte nicht sein. Bitte Fehler melden");
              CrashReporter.handleException(new IllegalStateException("Mehr als drei Mal session required. Sollte nicht sein."));
            }
          }
        }
        else
        {
          PegasosLog.d("ServerCallbackTraining", "Kontrollkommando: " + ((ControlCommand) r).getMessage());
          readMessage(((ControlCommand) r).getMessage());
          TrainingUIController.getInstance().stopTraining();
        }
      }
      else
      {
        // if( Config.DEBUG ) readMessage("Unbekannte Rueckmeldung: " + r.getRaw());
        PegasosLog.e("ServerCallbackTraining", "Message: " + (r.getRaw() != null ? r.getRaw() : "null"));
      }
    }
    else
    {
      PegasosLog.e("ServerCallbackTraining", "Message recieved empty");
    }
  }
	
	public boolean isConnected() {
	  // TODO: this is actually incorrect. When never connected this does not work. 
	  //   when no data is sent -> does not work
	  return !disconnect_reported;
	}
	
	private void readMessage(String text) {
		// soundmachine.sound(message, say_it);
		soundmachine.say_it(text);
	}
};
