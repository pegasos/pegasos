package at.pegasos.client;

import at.pegasos.client.util.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class AndroidXMLResourceBundleControl extends ResourceBundle.Control {
  @Override
  public List<String> getFormats(String baseName)
  {
    if( baseName == null )
    {
      throw new NullPointerException();
    }
    return List.of("xml");
  }
  
  @Override
  public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
      throws IllegalAccessException, InstantiationException, IOException
  {
    PegasosLog.d("AndroidXMLResourceBundleControl", baseName + " locale: '" + locale + "' format:'" + format + "' loader: '" + loader + "' reload:'" + reload + "'");
    if( baseName == null || locale == null || format == null || loader == null )
    {
      throw new NullPointerException();
    }

    AndroidXMLResourceBundle bundle= null;
    if( format.equals("xml") )
    {
      String bundleName= toBundleName(baseName, locale);
      // String resourceName = toResourceName(bundleName, format);
      
      // System.out.println(bundleName + " " + resourceName + " " + locale);

      // File[] files = getMMA(baseName, locale);
      // System.out.println(Arrays.toString(files));
      
      for(File file : getResourceFiles(bundleName))
      {
        // System.out.println(file);
        AndroidXMLResourceBundle bundle2= new AndroidXMLResourceBundle(file);
        if( bundle != null )
        {
          bundle.merge(bundle2);
        }
        else
        {
          bundle= bundle2;
        }
        System.out.println(file + " done");
      }
    }
    
    return bundle;
  }

  private List<File> getResourceFiles(String path) throws IOException {
    List<File> filenames = new ArrayList<>();

    try (
        InputStream in = getResourceAsStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
        String resource;

        while ((resource = br.readLine()) != null) {
          if( resource.endsWith(".xml") )
          {
            ClassLoader classLoader = getClass().getClassLoader();
            URL url = classLoader.getResource(path + "/" + resource);
            if (url == null) {
              throw new IllegalArgumentException("file not found! " + resource);
            } else
            {
              filenames.add(new File(url.toURI()));
            }
          }
        }
    }
    catch( URISyntaxException e )
    {
      e.printStackTrace();
    }

    return filenames;
  }

  private InputStream getResourceAsStream(String resource) {
    final InputStream in
        = getContextClassLoader().getResourceAsStream(resource);

    return in == null ? getClass().getResourceAsStream(resource) : in;
  }

  private ClassLoader getContextClassLoader() {
    return Thread.currentThread().getContextClassLoader();
  }

  private File[] getMMA(String baseName, Locale locale)
  {
    String fn= "/home/martin/pegasos.Test/MMAv3/res/" + baseName;
    if( !locale.getLanguage().equals("" ) )
      fn+= "-" + locale.getLanguage();
    if( !locale.getCountry().equals("") )
      fn+= "-" + locale.getCountry();
    File dir= new File(fn);
    System.out.println(fn + " " + dir.exists());

    return dir.listFiles(new FileFilter() {

      @Override
      public boolean accept(File pathname)
      {
        return pathname.getName().endsWith(".xml");
      }
    });
  }

  private static class AndroidXMLResourceBundle extends ResourceBundle {
    private final Properties props;
    
    AndroidXMLResourceBundle(File f) throws IOException
    {
      System.err.println("Load " + f);
      props= new Properties();
      
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder;
      Node s= null;
      
      String key= null;
      String value= "";
      
      try
      {
        dBuilder= dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(f);
        doc.getDocumentElement().normalize();
        
        NodeList pList = doc.getElementsByTagName("string");
        
        for(int p= 0; p < pList.getLength(); p++)
        {
          s= pList.item(p);
          
          key= ((Element) s).getAttribute("name");
          Node tmp= pList.item(p).getChildNodes().item(0);
          if( tmp == null )
          {
            value= "";
          }
          else if( tmp.getNodeType() == Node.TEXT_NODE )
          {
            value= tmp.getNodeValue();
          }
          else
          {
            value= getNodeString(tmp);
          }
          props.setProperty(key, value);
        }
      }
      catch( /* ParserConfigurationException | SAXException |*/ Exception e )
      {
        System.out.println(s + " " + key + " " + value);
        System.err.println(e);
        e.printStackTrace();
      }
      System.out.println("Load " + f + " done");
    }
    
    private static String getNodeString(Node node)
    {
      try
      {
        StringWriter writer= new StringWriter();
        Transformer transformer= TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        String output= writer.toString();
        return output.substring(output.indexOf("?>") + 2);// remove <?xml version="1.0"
                                                          // encoding="UTF-8"?>
      }
      catch( TransformerException e )
      {
        e.printStackTrace();
      }
      return node.getTextContent();
    }
    
    public void merge(AndroidXMLResourceBundle other)
    {
      for(Object key : other.props.keySet())
      {
        this.props.put(key, other.props.get(key));
      }
    }

    protected Object handleGetObject(String key)
    {
      if( key == null )
      {
        throw new NullPointerException();
      }
      return props.get(key);
    }
    
    public Enumeration<String> getKeys()
    {
      return new Vector<String>(props.stringPropertyNames()).elements();
    }
  }
}
