package at.pegasos.client.ui.screen;

import at.pegasos.client.ui.*;
import at.pegasos.client.ui.widget.*;
import at.pegasos.client.util.*;
import at.univie.mma.*;
import javafx.scene.layout.*;

public class ThreeSegments extends Screen {
  private MeasurementFragment frag1;
  private MeasurementFragment frag2;
  private MeasurementFragment frag3;
  private int count;
  private boolean good;

  private GridPane layout;

  public ThreeSegments(TrainingActivity activity, MeasurementFragment... fragments)
  {
    super(activity);

    if( fragments.length == 3 )
    {
      this.frag1= fragments[0];
      this.frag2= fragments[1];
      this.frag3= fragments[2];
      count= 3;
    }
    else if( fragments.length == 2 )
    {
      this.frag1= fragments[0];
      this.frag2= fragments[1];
      this.frag3= null;
      count= 2;
    }
    else if( fragments.length == 1 )
    {
      this.frag1= fragments[0];
      this.frag2= null;
      this.frag3= null;
      count= 1;
    }
    else
      throw new IllegalArgumentException();
  }

  /**
   * Put the screen on the (visual, i.e. phone) screen i.e. place yourself as a child of R.id.trianingscreen_container.
   * Make sure to properly remove the old screen if necessary by calling old.cleanUI()
   * @param old the old screen
   */
  public void putOnUI(Screen old)
  {
    if( Config.LOGGING ) PegasosLog.d("ThreeSegmentScreen", "putOnUI: " + frag1 + " " + frag2 + " " + frag3 + " old:" + old + "t.count:" + this.count + ((old != null) && old.getClass() == this.getClass() ? " o.count:" + ((ThreeSegments)old).count:"") +  "");

    if( old != null )
    {
      if( Config.LOGGING ) PegasosLog.d("ThreeSegmentScreen", "Removing old screen");
      old.cleanUI();
      old = null;
    }

    System.out.println(activity.getScreenWidth() + " " + activity.getScreenHeight());

    layout = new GridPane();

    String cssLayout = "-fx-border-color: red;\n" +
        "-fx-border-insets: 5;\n" +
        "-fx-border-width: 3;\n" +
        "-fx-border-style: dashed inside;\n";

    int idx = 0;

    if( frag1 != null )
    {
      // layout.add(frag1, 0, idx++);
      // frag1.setStyle(cssLayout);
      AnchorPane p= new AnchorPane();
      p.getChildren().add(frag1);
      AnchorPane.setBottomAnchor(frag1, 0.0);
      AnchorPane.setTopAnchor(frag1, 0.0);
      AnchorPane.setLeftAnchor(frag1, 0.0);
      AnchorPane.setRightAnchor(frag1, 0.0);
      layout.add(p, 0, idx++);
      p.setStyle(cssLayout);
    }
    if( frag2 != null )
    {
      AnchorPane p= new AnchorPane();
      p.getChildren().add(frag2);
      AnchorPane.setBottomAnchor(frag2, 0.0);
      AnchorPane.setTopAnchor(frag2, 0.0);
      AnchorPane.setLeftAnchor(frag2, 0.0);
      AnchorPane.setRightAnchor(frag2, 0.0);
      layout.add(p, 0, idx++);
      p.setStyle(cssLayout);

      // layout.add(frag2, 0, idx++);
      // frag2.setStyle(cssLayout);
    }
    if( frag3 != null )
    {
      // layout.add(frag3, 0, idx++);
      // VBox.setVgrow(frag3, Priority.ALWAYS);

      AnchorPane p= new AnchorPane();
      p.getChildren().add(frag3);
      AnchorPane.setBottomAnchor(frag3, 0.0);
      AnchorPane.setTopAnchor(frag3, 0.0);
      AnchorPane.setLeftAnchor(frag3, 0.0);
      AnchorPane.setRightAnchor(frag3, 0.0);
      layout.add(p, 0, idx++);
      p.setStyle(cssLayout);
    }

    ColumnConstraints cc = new ColumnConstraints();
    cc.setPercentWidth(100.0);
    cc.setHgrow(Priority.ALWAYS);
    layout.getColumnConstraints().add(cc);
    for (int i = 0 ; i < count ; i++)
    {
      RowConstraints rc = new RowConstraints();
      rc.setVgrow(Priority.ALWAYS);
      rc.setPercentHeight(100 / (float) count);
      layout.getRowConstraints().add(rc);
    }

    frag1.heightProperty().addListener((observable, oldValue, newValue) -> {
      System.out.println("F1 Height change: " + observable + " " + oldValue + " " + newValue);
    });
    frag2.heightProperty().addListener((observable, oldValue, newValue) -> {
      System.out.println("F2 Height change: " + observable + " " + oldValue + " " + newValue);
    });
    frag3.heightProperty().addListener((observable, oldValue, newValue) -> {
      System.out.println("F3 Height change: " + observable + " " + oldValue + " " + newValue);
    });

    activity.setScreencontainerView(layout);

    System.out.println("Height: " + layout.getHeight() + " " + frag1.getHeight());

    good= true;
  }

  /**
   * Send an update signal to all elements on the screen
   */
  public void updateUI()
  {
    if( good )
    {
      if( frag1 != null )
        frag1.updateUI();
      if( frag2 != null )
        frag2.updateUI();
      if( frag3 != null )
        frag3.updateUI();
    }
  }

  /**
   * Safely remove this screen from the training activity.
   * This means removing all your content from R.id.trainingscreen_container
   */
  public void cleanUI()
  {
    good= false;

    activity.removeFromScreenContainer(layout);
  }
}
