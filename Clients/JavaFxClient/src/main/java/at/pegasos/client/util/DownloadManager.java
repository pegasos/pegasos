package at.pegasos.client.util;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;

public class DownloadManager {
  private final ArrayList<Download> downloads;

  private DownloadFileFromURL task= null;

  private ProgresListener progressListener;

  private EndListener endListenter;

  public void setProgressListener(ProgresListener listener)
  {
    this.progressListener= listener;
  }

  public void setEndCallback(EndListener listener)
  {
    this.endListenter= listener;
  }

  public class Download {
    /**
     * The URL to download
     */
    public String source;
    /**
     * where to store it
     */
    public String target;

    /**
     * Construct new download
     * @param source source (URL)
     * @param target target (where to store)
     */
    public Download(String source, String target)
    {
      this.source= source;
      this.target= target;
    }
  }

  /**
   * Background Async Task to download file
   * */
  class DownloadFileFromURL extends Thread {

    boolean success;
    
    private Download[] downloads;

    private boolean stop= false;
    
    public DownloadFileFromURL(Download... downloads)
    {
      this.downloads= downloads;
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    public void run()
    {
      if( progressListener != null )
        progressListener.onProgress(0);
      
      int count;
      long dt= 0;
      try
      {
        SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
        sslcontext.init(null, null, null);
        SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());

        HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);

        URL[] urls= new URL[downloads.length];
        URLConnection[] connections= new URLConnection[downloads.length];
        Response[] responses= new Response[downloads.length];

        OkHttpClient client = getNewHttpClient();
        Request.Builder requestBuilder = new Request.Builder();
        long length= 0;

        for(int i= 0; i < downloads.length; i++)
        {
          urls[i]= new URL(downloads[i].source);
          PegasosLog.i("DM", i + "/" + downloads.length + " " + urls[i].toString());
          // connections[i]= urls[i].openConnection();
          // connections[i].connect();

          // this will be useful so that you can show a tipical 0-100%
          // progress bar
          // length+= connections[i].getContentLength();

          Request request = requestBuilder.url(urls[i]).build();
          Call call = client.newCall(request);
          responses[i]= call.execute();
          // this will be useful so that you can show a tipical 0-100% progress bar
          length+= responses[i].body().contentLength();

          File f= new File(downloads[i].target);
          if( !f.getParentFile().exists() )
          {
            f.getParentFile().mkdirs();
          }
          
          if( stop )
          {
            return;
          }
        }

        for(int i= 0; i < downloads.length; i++)
        {
          PegasosLog.i("DM", "Downloading: "+ urls[i].toString() + " to " + downloads[i].target);
          // download the file
          // InputStream input = new BufferedInputStream(urls[i].openStream(), 8192);
          InputStream input = new BufferedInputStream(responses[i].body().byteStream(), 8192);

          // Output stream
          OutputStream output = new FileOutputStream(downloads[i].target);

          byte[] data = new byte[1024];

          long total = 0;

          while ((count = input.read(data)) != -1)
          {
            total += count;
            // publishing the progress....
            // After this onProgressUpdate will be called
            if( progressListener != null )
            {
              progressListener.onProgress(total / (double) length);
            }

            // writing data to file
            output.write(data, 0, count);
            
            if( stop )
            {
              output.close();
              return;
            }
          }

          // flushing output
          output.flush();

          // closing streams
          output.close();
          input.close();

          dt+= total;
          
          if( stop )
          {
            return;
          }
        }

        success= true;
      }
      catch (Exception e)
      {
        e.printStackTrace();
        PegasosLog.e("Error: ", e.toString());
        CrashReporter.handleException(e);
      }

      task= null;
      if( endListenter != null )
        endListenter.onFinished(success);
    }

    public void setStop()
    {
      this.stop= true;
    }
  }

  public DownloadManager()
  {
    downloads= new ArrayList<Download>();
  }

  public void addDownload(String url, String target)
  {
    if( task != null )
      throw new IllegalStateException("Download already started");
    downloads.add(new Download(url,target));
  }

  public void addDownload(Download download)
  {
    if( task != null )
      throw new IllegalStateException("Download already started");
    downloads.add(download);
  }

  public void start()
  {
    if( task != null )
      throw new IllegalStateException("Download already started");
    PegasosLog.d("DownloadManager", "start " + downloads.size());
    task= new DownloadFileFromURL(downloads.toArray(new Download[0]));
    task.start();
    downloads.clear();
  }

  public void cancel()
  {
    task.setStop();
    task.interrupt();
  }

  public interface ProgresListener {
    void onProgress(double percent);
  }

  public interface EndListener {
    void onFinished(boolean success);
  }

  public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
    /*if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
      try {
        PegasosLog.d("DM", "Enabling TLSv1.2");
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, null, null);
        client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

        ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2).build();

        List<ConnectionSpec> specs = new ArrayList<>();
        specs.add(cs);
        specs.add(ConnectionSpec.COMPATIBLE_TLS);
        specs.add(ConnectionSpec.CLEARTEXT);

        client.connectionSpecs(specs);
      } catch (Exception exc) {
        PegasosLog.e("OkHttpTLSCompat", "Error while setting TLS 1.2" + exc);
      }
    }*/

    return client;
  }

  public OkHttpClient getNewHttpClient() {
    OkHttpClient.Builder client = new OkHttpClient.Builder().followRedirects(true).followSslRedirects(true)
            .retryOnConnectionFailure(true).cache(null).connectTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS).readTimeout(5, TimeUnit.SECONDS);

    return enableTls12OnPreLollipop(client).build();
  }
}
