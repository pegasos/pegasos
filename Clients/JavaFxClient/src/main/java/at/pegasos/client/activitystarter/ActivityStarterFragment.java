package at.pegasos.client.activitystarter;

import android.content.*;
import at.pegasos.client.ui.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;

import java.io.*;

public class ActivityStarterFragment extends VBox implements IActivityStarterFragment {
  
  /**
   * Callback interface for activity starter fragments
   */
  public interface ActivityStarterCallback {
    /**
     * Invoked when the fragment was clicked by the user i.e. when the activity should be started
     * 
     * @param fragment
     *          fragment which was clicked
     */
    public void onActivityClicked(ActivityStarterFragment fragment);
  }
  
  @FXML
  private Label text;
  
  @FXML
  private ImageView img;
  
  private boolean auto_start;
  
  public ActivityStarterFragment()
  {
    FXMLLoader fxmlLoader= new FXMLLoader();
    fxmlLoader.setLocation(getClass().getResource("activitystarter.fxml"));
    fxmlLoader.setController(this);
    fxmlLoader.setRoot(this);
    
    try
    {
      fxmlLoader.load();
    }
    catch( IOException exception )
    {
      throw new RuntimeException(exception);
    }
  }
  
  public void setText(String value)
  {
    text.setText(value);
  }

  public void setPicture(String string)
  {
    // System.out.println("res/drawable/" + string);
    img.setImage(new Image("drawable/" + string));
  }

  @SuppressWarnings("unused")
  public void setText(int text)
  {
    // TODO:
    throw new UnsupportedOperationException("Cannot set text from int");
  }

  public void setAutoStart(boolean start)
  {
    this.auto_start= start;
  }
  
  public boolean getAutoStart()
  {
    return auto_start;
  }

  public void setCallback(final IActivityStarterCallback activityStarterCallback)
  {
    this.setOnMouseClicked(event -> activityStarterCallback.onActivityClicked(ActivityStarterFragment.this));
  }

  @SuppressWarnings("unused")
  public static void setParameters(ActivityStarterFragment frag, Intent i)
  {
    i.putExtra(TrainingLandingScreen.EXTRA_AUTOSTART, frag.auto_start);
  }
}
