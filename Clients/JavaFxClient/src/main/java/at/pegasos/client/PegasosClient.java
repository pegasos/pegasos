package at.pegasos.client;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import at.pegasos.client.controller.*;
import at.pegasos.client.java.util.Toast;
import at.pegasos.client.util.PegasosLog;
import at.univie.mma.Config;
import at.univie.mma.StartupActions;
import at.univie.mma.serverinterface.ServerInterface;
import at.univie.mma.sports.Sports;
import at.pegasos.client.ui.PegasosClientView;
import at.univie.mma.MainActivity;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.*;
import javafx.stage.Stage;

public class PegasosClient extends Application /*implements Application.ActivityLifecycleCallbacks*/ {
  private Stage primaryStage;
  
  Timer heartbeattask= null;
  private boolean trainingUploadRequired= false;
  private ITrainingController trainingcontroller;
  private boolean intraining;
  private boolean inlanding;
  // private MMAActivity foregroundActivity;

  private PegasosClientView activity;
  
  private static final int SERVER_SENDDELAY= 1000;

  private static PegasosClient instance;
  
  private ActivityStack activities;

  private int resultCode;

  private Intent resultData;

  private MainActivity mainActivity;

  private ResourceBundle bundle;

  @Override
  public void start(Stage primaryStage)
  {
    PegasosLog.d("PegasosClient", "Application onCreate");
    
    this.activities= new ActivityStack();
    
    this.primaryStage = primaryStage;
    
    // Locale locale= new Locale("en", "UK");
    // bundle= ResourceBundle.getBundle("strings", locale);
    bundle= ResourceBundle.getBundle("values", Locale.getDefault(), new AndroidXMLResourceBundleControl());
    
    // Register to be notified of activity state changes
    // registerActivityLifecycleCallbacks(this);
    
    connectToServer();
    
    /*
    ACRAConfiguration config= null;

    config= new ConfigurationBuilder(this)
        .setFormUri(Config.ACRA_URI)
        .setFormUriBasicAuthLogin(Config.ACRA_LOGIN)
        .setFormUriBasicAuthPassword(Config.ACRA_PASSWORD)
        .setHttpMethod(Method.PUT)
        .setReportType(Type.JSON).
        build();

    ACRA.init(this, config);
    ACRA.getErrorReporter().putCustomData("build-time", Config.BUILD_TIME);
    ACRA.getErrorReporter().putCustomData("build-id", Config.BUILD_ID);
    */
    instance= this;

    createDirs();

    initUI();
    
    initStuff();
    
    // run startup actions
    new StartupActions().run();
    
    startMainActivity();

    primaryStage.show();
  }
  
  @Override
  public void stop()
  {
    PegasosLog.d("PegasosClient", "stop");
    if( this.activity != null )
      activity.onStop();

    ServerInterface server = ServerInterface.getInstance();
    if( server != null )
    {
      server.close();
    }
    PegasosLog.d("PegasosClient", "stopped");
  }

  private void initUI()
  {
    primaryStage.setTitle(getString("app_name"));
    primaryStage.getIcons().add(new Image("drawable/ic_launcher.png"));
  }

  public static PegasosClient getInstance()
  {
    return instance;
  }
  
  /**
   * Open connection to server
   */
  public void connectToServer()
  {
    ServerInterface.getInstance(Config.MMA_SERVER_IP, Config.MMA_SERVER_PORT, SERVER_SENDDELAY);
    PegasosLog.d("PegasosClient", "ServerInterface created");
  }

  private void initStuff()
  {
    Sports.fillinIds();
  }

  public void startHeartBeats()
  {
    if( heartbeattask == null )
    {
      heartbeattask= new Timer("Heartbeat Timer");
      heartbeattask.scheduleAtFixedRate(new TimerTask() {
        
        @Override
        public void run()
        {
          ServerInterface s= ServerInterface.getInstance();
          
          if( s != null )
            s.heartbeat();
          else
          {
            // this is most probably due to an "orphaned" app --> restart app
            PegasosClient.restartApp(/*MMA.this.getApplicationContext()*/);
          }
        }
      }, 1000, 10000);
    }
  }

  public static void restartApp(/*Context ctx*/)
  {
    /*
    final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
    final File currentJar = new File(MyClassInTheJar.class.getProtectionDomain().getCodeSource().getLocation().toURI());

    // TODO: when it is not a jar we still want to start ...
    // is it a jar file?
    if(!currentJar.getName().endsWith(".jar"))
      return;

    // Build command: java -jar application.jar
    final ArrayList<String> command = new ArrayList<String>();
    command.add(javaBin);
    command.add("-jar");
    command.add(currentJar.getPath());

    final ProcessBuilder builder = new ProcessBuilder(command);
    builder.start();
    System.exit(0);
    */
  }
  
  public void stopHeartBeats()
  {
    if( heartbeattask != null )
    {
      heartbeattask.cancel();
      heartbeattask.purge();
      heartbeattask= null;
    }
  }
  
  public void setTrainingUploadRequired(boolean required)
  {
    this.trainingUploadRequired= required;
  }
  
  public boolean isTrainingUploadRequired()
  {
    return this.trainingUploadRequired;
  }
  
  public ITrainingController getController()
  {
    return this.trainingcontroller;
  }
  
  public void setTrainingController(ITrainingController controller)
  {
    this.trainingcontroller= controller;
  }
  
  public static File getDirConfig()
  {
    // TODO:
    String rootDir= ".";
    File f;
    if( Config.USE_OWN_DIRECTORY )
      f= new File(rootDir + "/Pegasos-" + Config.APP_NAME.replaceAll(" ", "_").replaceAll("\'", "").trim() + "/config");
    else
      f= new File(rootDir + "/PegasosClient/config");
    return f;
  }
  
  public static File getDirData()
  {
    // TODO:
    String rootDir= ".";
    File f;
    if( Config.USE_OWN_DIRECTORY )
      f= new File(rootDir + "/Pegasos-" + Config.APP_NAME.replaceAll(" ", "_").replaceAll("\'", "").trim() + "/data");
    else
      f= new File(rootDir + "/PegasosClient/data");
    return f;
  }
  
  public boolean isNetworkAvailable()
  {
    // TODO:
    /*ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo= connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnected();*/
    return true;
  }
  
  /**
   * Return whether or a TrainingActivity is currently visible
   * @return
   */
  public boolean inTraining()
  {
    return intraining;
  }
  
  /**
   * Return whether or not the LandingScreen is currently visible
   * @return
   */
  public boolean inLanding()
  {
    return inlanding;
  }
  
  /**
   * Return the activity which can be assumed to be in the foreground
   * @return
   
  public Activity getForegroundActivity()
  {
    return foregroundActivity;
  }*/
  
  /**
   * Returns the main stage.
   * @return
   */
  public Stage getPrimaryStage() {
      return primaryStage;
  }
  
  public static void main(String[] args)
  {
    launch(args);
  }
  
  private void startMainActivity()
  {
    MainActivity a= new MainActivity();
    
    startActivity(a);
  }
  
  private void startActivity(PegasosClientView a)
  {
    activities.put(a);
    a.setMainApp(this);
    a.onCreate(null);
    activity= a;
  }

  public void startActivity(Intent i)
  {
    PegasosClientView a;
    try
    {
      a= i.create();
      activities.put(a);
      resultCode= 0;
      resultData= null;
      a.setIntent(i);
      a.setMainApp(this);
      a.onCreate(null);
      activity= a;
    }
    catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void startActivityForResult(Intent i, int requestCode)
  {
    PegasosClientView a;
    try
    {
      a= i.create();
      activities.put(a, requestCode);
      resultCode= 0;
      resultData= null;
      a.setIntent(i);
      a.setMainApp(this);
      a.onCreate(null);
      activity= a;
    }
    catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void finishActivity(PegasosClientView activity)
  {
    PegasosLog.d("PegasosClient", "finishActivity");
    activities.debug();
    
    activities.current().onStop();
    
    if( activities.resultRequested() )
    {
      int code= activities.getRequestCode();
      activities.pop(activity);
      
      activities.current().onActivityResult(code, resultCode, resultData);
    }
    else
    {
      activities.pop(activity);
    }
    
    PegasosLog.d("PegasosClient", "popped " + (activities.current() != null ? activities.current() : "null"));
    activities.debug();
    if( activities.current() != null )
    {
      Platform.runLater(new Runnable() {
        
        @Override
        public void run()
        {
          activities.current().onCreate(null);
        }
      });
    }
    // TODO: quit the app
  }

  public void setResult(int resultCode, Intent data)
  {
    this.resultCode= resultCode;
    this.resultData= data;
  }

  public void showToast(String text)
  {
    PegasosLog.d("PegasosClient", "Toast: " + text);
    Platform.runLater(new Runnable() {
      
      @Override
      public void run()
      {
        Toast.makeText(getPrimaryStage(), getString(text), 3000, 250, 250);
      }
    });
  }
  
  public void showToastText(String text)
  {
    PegasosLog.d("PegasosClient", "Toast: " + text);
    Platform.runLater(new Runnable() {
      
      @Override
      public void run()
      {
        Toast.makeText(getPrimaryStage(), text, 3000, 250, 250);
      }
    });
  }

  public PegasosClient getResources()
  {
    return this;
  }
  
  public ResourceBundle getResourceBundle()
  {
    return bundle;
  }

  public String getString(String string)
  {
    try
    {
      return bundle.getString(string);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    // if( string.equals("wmt_files_url") )
    //  return "https://ucloud.univie.ac.at/index.php/s/IxuZqnpzZfqKUKV/download";
    return string;
  }

  public void setMainActivity(MainActivity mainActivity)
  {
    this.mainActivity= mainActivity;
  }
  
  public MainActivity getMainActivity()
  {
    return mainActivity;
  }

  public boolean createDirs()
  {
    boolean ret= true;
    try
    {
      getDirConfig().mkdirs();
    }
    catch(Exception e)
    {
      ret= false;
    }
    try
    {
      getDirData().mkdirs();
    }
    catch(Exception e)
    {
      ret= false;
    }
    return ret;
  }
}
