package at.pegasos.client;

import java.util.List;
import java.util.Stack;

import at.pegasos.client.util.PegasosLog;
import at.pegasos.client.ui.PegasosClientView;

public class ActivityStack {
  /*public enum Type {
    Normal,
    Result
  };*/
  
  private class StackElement {
    
    PegasosClientView activity;
    // Type type;
    int request_code;
    
    @Override
    public String toString()
    {
      return "StackElement [activity=" + activity + ", request_code=" + request_code + "]";
    }
  }
  
  private List<StackElement> stack;
  private StackElement current;
  
  public ActivityStack()
  {
    this.stack= new Stack<StackElement>();
  }
  
  public void put(PegasosClientView act)
  {
    StackElement e= new StackElement();
    e.activity= act;
    stack.add(e);
    
    current= e;
  }
  
  public void put(PegasosClientView act, int requestCode)
  {
    StackElement e= new StackElement();
    e.activity= act;
    e.request_code= requestCode;
    stack.add(e);
    
    current= e;
  }
  
  /**
   * Remove current activity from the stack
   * @param activity 
   */
  public void pop(PegasosClientView activity)
  {
    int r= stack.size() - 1;
    for(; r >= 0; r--)
    {
      if( stack.get(r).activity == activity )
      {
        stack.remove(r);
        break;
      }
    }
    if( r == -1 )
      throw new IllegalStateException("Cannot remove activity " + activity);
    
    if( stack.size() > 0 )
      current= stack.get(stack.size()-1);
    else
      current= null;
  }
  
  /**
   * Return the current activity from the stack
   * 
   * @return current activity
   */
  public PegasosClientView current()
  {
    if( current != null )
      return current.activity;
    else
      return null;
  }
  
  public boolean resultRequested()
  {
    return current.request_code > 0;
  }
  
  public int getRequestCode()
  {
    return current.request_code;
  }
  
  public void debug()
  {
    String s= "[";
    for(StackElement x :stack)
    {
      s+= x;
    }
    s+= "]";
    PegasosLog.d("ActivityStack", s);
  }
}