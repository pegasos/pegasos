package at.pegasos.client.util;

import java.util.*;

public class Base64Util {
  public static String encodeNoPaddingUrl(byte[] code)
  {
    return Base64.getUrlEncoder().withoutPadding().encodeToString(code);
  }
}
