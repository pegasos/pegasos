package at.pegasos.client.ui.widget;

import at.pegasos.client.values.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;

public class HeartrateFragment extends MeasurementFragment /*implements ScalableMeasurementFragment*/ {
  ValueStore vals;

  Label txtBmp;
  ImageView hrLogo;
  ImageView hrLogoSmall;

  private int requiredWidth= 0;

  public HeartrateFragment()
  {
    super();

    // mainApp.getPrimaryStage().getScene().getStylesheets().add(String.valueOf(getClass().getResource(themename)));

    /* HBox layout = new HBox();

    // txtBmp = new Text();
    txtBmp = new Label();
    txtBmp.setText("TEST");
    txtBmp.setStyle("-fx-font-size: 60;");
    txtBmp.setPrefWidth(200);
    txtBmp.setPrefHeight(100);
    // hrLogo= view.findViewById(R.id.hrlogo);
    hrLogo = new ImageView();
    hrLogo.setImage(new Image("drawable/heart_icon.png"));
    hrLogo.setVisible(true);
    hrLogo.setFitHeight(100);
    hrLogo.setFitWidth(100);
    // hrLogoSmall= view.findViewById(R.id.hrlogosmall);

    layout.getChildren().add(txtBmp);
    layout.getChildren().add(hrLogo);

    this.getChildren().add(layout); */

    txtBmp = new Label();
    txtBmp.setText("TEST");
    // txtBmp.setPrefWidth(200);
    // txtBmp.setPrefHeight(100);

    hrLogo = new ImageView();
    hrLogo.setImage(new Image("drawable/heart_icon.png"));
    hrLogo.setVisible(true);
    hrLogo.setPreserveRatio(true);
    hrLogo.setFitHeight(60);
    hrLogo.setFitWidth(60);
    // hrLogo.setY(0);

    // txtBmp.layoutXProperty().bind(widthProperty().subtract(txtBmp.widthProperty()).divide(2));
    // txtBmp.layoutYProperty().bind(heightProperty().subtract(txtBmp.heightProperty()).divide(2));

    /*BorderPane layout = new BorderPane();
    layout.setCenter(txtBmp);
    layout.setLeft(hrLogo);
    this.getChildren().add(layout);*/

    /*HBox root = new HBox();
    HBox.setHgrow(root, Priority.ALWAYS);
    root.getChildren().add(txtBmp);
    root.getChildren().add(hrLogo);*/

    /*StackPane root = new StackPane();
    root.getChildren().addAll(txtBmp, hrLogo);
    StackPane.setAlignment(txtBmp, Pos.CENTER);
    StackPane.setAlignment(hrLogo, Pos.CENTER_LEFT);

    root.minHeight(Long.MIN_VALUE);*/
    // root.setMinWidth(Long.MAX_VALUE);
    // txtBmp.minHeight(USE_COMPUTED_SIZE);

    /*root.parentProperty().addListener((observable, oldValue, newValue) -> {
      root.minHeight(((Node) newValue).property)
    });*/

    /*
    AnchorPane root = new AnchorPane();
    root.getChildren().addAll(txtBmp, hrLogo);
    AnchorPane.setLeftAnchor(txtBmp, 0.0);
    AnchorPane.setRightAnchor(txtBmp, 0.0);
    AnchorPane.setBottomAnchor(root, 0.0);
     */

    StackPane root = new StackPane();
    AnchorPane p = new AnchorPane();
    AnchorPane.setBottomAnchor(txtBmp, 0.0);
    AnchorPane.setTopAnchor(txtBmp, 0.0);
    AnchorPane.setLeftAnchor(txtBmp, 0.0);
    AnchorPane.setRightAnchor(txtBmp, 0.0);
    p.getChildren().add(txtBmp);
    AnchorPane.setBottomAnchor(hrLogo, 0.0);
    AnchorPane.setTopAnchor(hrLogo, 0.0);
    AnchorPane.setLeftAnchor(hrLogo, 0.0);
    p.getChildren().add(hrLogo);
    root.getChildren().add(p);

    /*GridPane root = new GridPane();

    AnchorPane p = new AnchorPane();
    AnchorPane.setBottomAnchor(txtBmp, 0.0);
    AnchorPane.setTopAnchor(txtBmp, 0.0);
    AnchorPane.setLeftAnchor(txtBmp, 0.0);
    AnchorPane.setRightAnchor(txtBmp, 0.0);
    p.getChildren().add(txtBmp);

    root.add(p, 0, 0);

    ColumnConstraints cc = new ColumnConstraints();
    cc.setPercentWidth(100.0);
    cc.setHgrow(Priority.ALWAYS);
    root.getColumnConstraints().add(cc);
    RowConstraints rc = new RowConstraints();
    rc.setVgrow(Priority.ALWAYS);
    rc.setPercentHeight(100);
    root.getRowConstraints().add(rc);*/

    parentProperty().addListener((observable, oldValue, newValue) -> {
      if( newValue != null ) {
        root.prefHeightProperty().bind(((Region) newValue).heightProperty());
        root.prefWidthProperty().bind(((Region) newValue).widthProperty());

        AnchorPane.setBottomAnchor(hrLogo, 0.0);
        AnchorPane.setTopAnchor(hrLogo, 0.0);

        initialise(true);

        /*AnchorPane.setBottomAnchor(txtBmp, 0.0);
        AnchorPane.setTopAnchor(txtBmp, 0.0);
        AnchorPane.setLeftAnchor(txtBmp, 0.0);
        AnchorPane.setRightAnchor(txtBmp, 0.0);*/
      }
    });

    String cssLayout = "-fx-border-color: yellow;\n" +
        "-fx-border-insets: 5;\n" +
        "-fx-border-width: 10;\n" +
        "-fx-border-style: dashed;\n";
    setStyle(cssLayout);

    cssLayout = "-fx-border-color: white;\n" +
        "-fx-border-insets: 5;\n" +
        "-fx-border-width: 3;\n" +
        "-fx-border-style: dashed;\n";
    p.setStyle(cssLayout);

    hrLogo.setStyle("-fx-alignment: CENTER;");
    txtBmp.getStyleClass().add("primary-value");

    this.getChildren().add(root);

    vals = ValueStore.getInstance();
  }

  public void updateUI()
  {
    if( !init )
      return;

    int value= vals.getHeartRateBPM();
    if( value == -1 )
      txtBmp.setText("--");
    else
      txtBmp.setText(String.valueOf(value));
  }

  /*@Override
  public int getRequiredWidth()
  {
    if( isAdded() )
    {
      requiredWidth= ScalableMeasurementFragmentHelper.requiredWidthLarge("000");
      return requiredWidth;
    }
    else
      return 0;
  }

  @Override
  public void setScaleWidth(float scale, float sizeLarge, float sizeSmall, float sizeLabel, float availableWidth)
  {
    txtBmp.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeLarge);
    // txtBmp.setScaleX(scale);
    // txtBmp.setScaleY(scale);
    // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
    // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
    txtBmp.requestLayout();

    if( requiredWidth + 1.75 * sizeLarge > availableWidth )
    {
      hrLogo.setVisibility(View.GONE);
      hrLogoSmall.setVisibility(View.VISIBLE);

      hrLogoSmall.getLayoutParams().height= (int) (sizeSmall * 1.2);
      hrLogoSmall.getLayoutParams().width= (int) (sizeSmall * 1.2);
    }
    else
    {
      hrLogo.setVisibility(View.VISIBLE);
      hrLogoSmall.setVisibility(View.GONE);

      hrLogo.getLayoutParams().height= (int) (sizeLarge * 0.8);
      hrLogo.getLayoutParams().width= (int) (sizeLarge * 0.8);
      // hrLogo.setScaleX(scale);
      // hrLogo.setScaleY(scale);
      // hrLogo.setMaxHeight((int) (sizeLarge * 0.9) );
      // hrLogo.setMaxHeight((int) sizeLarge);
      // ((RelativeLayout.LayoutParams) hrLogo.getLayoutParams()).addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
      // ((RelativeLayout.LayoutParams) txtBmp.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM, txtBmp.getId());
      hrLogo.requestLayout();
    }
  }*/
}

