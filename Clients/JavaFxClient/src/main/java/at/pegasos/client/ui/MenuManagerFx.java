package at.pegasos.client.ui;

import android.content.Intent;
import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.pegasos.data.Pair;
import at.univie.mma.MainActivity;

public class MenuManagerFx extends MenuManager {
  public MenuManagerFx(MainActivity mainActivity)
  {
    super(mainActivity);
  }

  @Override public void goToLanding(IActivityStarterFragment fragment,
      Pair<Object, Object>[] arguments)
  {
    Intent i= new Intent((PegasosClientView) getActivity(), TrainingLandingScreen.class);
    for( Pair<Object, Object> arg : arguments)
    {
      i.putExtra(arg.first, arg.second);
    }

    ((PegasosClientView) getActivity()).startActivity(i);
  }
}
