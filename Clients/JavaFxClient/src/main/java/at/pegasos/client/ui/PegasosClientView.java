package at.pegasos.client.ui;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import android.content.Intent;
import android.os.Bundle;
import at.pegasos.client.PegasosClient;
import at.pegasos.client.util.PegasosLog;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;

public abstract class PegasosClientView implements Initializable {
  
  public static final int RESULT_OK= 1;
  public static final int RESULT_CANCELED= -1;
  
  protected static final String WRITE_EXTERNAL_STORAGE= "write_data";

  protected PegasosClient mainApp;

  private Intent intent;

  @FXML
  protected ResourceBundle resources;

  public void showToast(String text)
  {
    mainApp.showToast(text);
  }
  
  public void showToastText(String text)
  {
    mainApp.showToastText(text);
  }

  public void onBackPressed()
  {
    PegasosClient.getInstance().finishActivity(this);
  }
  
  protected void setContentView(String layoutname, String themename)
  {
    try
    {
      // Load FXML
      FXMLLoader loader= new FXMLLoader();
      
      loader.setLocation(getClass().getResource(layoutname));
      // loader.setResources(ResourceBundle.getBundle("vcpx.database.lang.DatabasePane", Settings.getInstance().getLocale()));
      if( loader.getLocation() == null )
        PegasosLog.e("PegasosClientView", "Setting loader location failed. Layout not in classpath?");
      // System.out.println("'" + loader.getLocation() + "' " + layoutname + " " + getClass());
      loader.setController(this);
      loader.setResources(mainApp.getResourceBundle());

      Scene s = new Scene(loader.load());
      // System.out.println(s + " " + mainApp.getPrimaryStage() + " " + mainApp.getPrimaryStage().getScene());
      mainApp.getPrimaryStage().setScene(s);
      // mainApp.getPrimaryStage().getScene().getStylesheets().add(getClass().getResource(themename).toExternalForm());
      // System.out.println(getClass().getResource(themename));
      mainApp.getPrimaryStage().getScene().getStylesheets().add(String.valueOf(getClass().getResource(themename)));
      addKeyListener();
      mainApp.getPrimaryStage().show();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }
  
  protected void setContentView(String layoutname)
  {
    try
    {
      // Load FXML
      FXMLLoader loader= new FXMLLoader();
      
      loader.setLocation(getClass().getResource(layoutname));
      if( loader.getLocation() == null )
        PegasosLog.e("PegasosClientView", "Setting loader location failed. Layout not in classpath?");
      // System.out.println("'" + loader.getLocation() + "' " + layoutname + " " + getClass());
      loader.setController(this);
      loader.setResources(mainApp.getResourceBundle());

      Scene s = new Scene(loader.load());
      System.out.println(s + " " + mainApp.getPrimaryStage() + " " + mainApp.getPrimaryStage().getScene());
      mainApp.getPrimaryStage().setScene(s);
      addKeyListener();
      mainApp.getPrimaryStage().hide();
      mainApp.getPrimaryStage().show();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }
  
  protected void setContentView(Parent view)
  {
    if( !Platform.isFxApplicationThread() )
    {
      Platform.runLater(new Runnable() {
        
        @Override
        public void run()
        {
          mainApp.getPrimaryStage().setScene(new Scene(view));
          addKeyListener();
          mainApp.getPrimaryStage().show();
        }
      });
    }
    else
    {
      mainApp.getPrimaryStage().setScene(new Scene(view));
      addKeyListener();
      mainApp.getPrimaryStage().show();
    }
  }

  private void addKeyListener()
  {
    mainApp.getPrimaryStage().getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event)
      {
        PegasosLog.d("MMA", "onKeyPressed " + event.getCode());
        switch( event.getCode() )
        {
          case BACK_SPACE:
            onBackPressed();
            break;
          default:
            break;
        }
      }
    });
  }

  /**
   * Is called by the main application to give a reference back to itself.
   * 
   * @param mainApp
   */
  public void setMainApp(PegasosClient mainApp)
  {
    this.mainApp= mainApp;
  }

  public abstract void onCreate(Bundle savedInstanceState);

  public void startActivity(Intent i)
  {
    PegasosLog.d("MMAActivity", "startActivity " + i.getClass());
    this.mainApp.startActivity(i);
  }
  
  public void startActivityForResult(Intent i, int requestCode)
  {
    if( !Platform.isFxApplicationThread() )
    {
      Platform.runLater(new Runnable() {

        @Override
        public void run()
        {
          PegasosClientView.this.mainApp.startActivityForResult(i, requestCode);
        }
      });
    }
    else
    {
      this.mainApp.startActivityForResult(i, requestCode);
    }
  }
  
  /**
   * Stop/finish this activity
   */
  protected void finish()
  {
    this.mainApp.finishActivity(this);
  }
  
  protected void setResult(int resultCode, Intent data)
  {
    this.mainApp.setResult(resultCode, data);
  }
  
  protected Intent getIntent()
  {
    return intent;
  }
  
  public void setIntent(Intent i)
  {
    this.intent= i;
  }
  
  public enum CapabilityState {
    /**
     * App is currently capable of doing this
     */
    Can,
    /**
     * App can currently NOT do this
     */
    Cannot,
    /**
     * There is no information about whether or not the app can do this
     */
    Undecided
  };
  
  public static class Capabilities {
    private Map<String, CapabilityState> caps= new HashMap<String, CapabilityState>();
    
    public void setCapability(String name, CapabilityState state)
    {
      caps.put(name, state);
    }
    
    public CapabilityState getCapability(String name)
    {
      CapabilityState s= caps.get(name);
      if( s == null )
        return CapabilityState.Undecided;
      else
        return s;
    }

    public boolean isCapable(String name)
    {
      return getCapability(name) == CapabilityState.Can;
    }
  }
  
  protected Capabilities capabilities= new Capabilities();
  
  /**
   * Return the permission this activity needs
   * @return
   */
  protected String[] getRequiredPermissions()
  {
    return new String[0];
  }
  
  protected void checkAndRequestPermissions()
  {
    capabilities.setCapability(WRITE_EXTERNAL_STORAGE, CapabilityState.Can);
    
    /*String[] required= getRequiredPermissions();
    int count= required.length;
    List<String> permissionsToRequest= new ArrayList<String>(count);
    
    for(int i= 0; i < count; i++)
    {
      if( ContextCompat.checkSelfPermission(this.getApplicationContext(), required[i]) != PackageManager.PERMISSION_GRANTED )
      {
        // at the moment we cannot do this...
        capabilities.setCapability(required[i], CapabilityState.Cannot);
        // request this permission
        permissionsToRequest.add(required[i]);
      }
      else
      {
        capabilities.setCapability(required[i], CapabilityState.Can);
      }
      
    }
    */
    // Before checking the capabilities propagate current ones
    onCapabilitiesChanged();
    /*
    PermissionChecker.checkAndRequestPermissions(permissionsToRequest.toArray(new String[0]), this);*/
  }
  
  /**
   * The capabilities of the app/activity have changed.
   */
  protected void onCapabilitiesChanged()
  {
    if( capabilities.getCapability(WRITE_EXTERNAL_STORAGE) == CapabilityState.Can )
    {
      createDirs();
    }
  }
  
  protected void createDirs()
  {
    try
    {
      PegasosClient.getInstance().createDirs();
    }
    catch( Exception e )
    {
      // TODO
      // CrashReporter.handleException(e);
    }
  }

  public void onStop()
  {
    // TODO Auto-generated method stub
    
  }
  
  public double getScreenHeight()
  {
    return this.mainApp.getPrimaryStage().getHeight();
  }

  public double getScreenWidth()
  {
    return this.mainApp.getPrimaryStage().getWidth();
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    // TODO Auto-generated method stub
    
  }
  
  public void runOnUiThread(Runnable runnable)
  {
    Platform.runLater(runnable);
  }
  
  public String getString(String name)
  {
    return name;
  }
  
  public String getString(String name, Object...args)
  {
    return String.format(getString(name), args);
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    this.resources= resources;
  }
}
