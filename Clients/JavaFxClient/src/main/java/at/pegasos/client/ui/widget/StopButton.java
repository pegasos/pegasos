package at.pegasos.client.ui.widget;

import at.pegasos.client.*;
import javafx.scene.control.Button;

import at.pegasos.client.ui.controller.*;
import javafx.scene.layout.*;

public class StopButton extends MeasurementFragment {
  
  public StopButton()
  {
    Button b= new Button();
    b.setText(PegasosClient.getInstance().getResources().getString("StopTraining"));
    b.setOnMouseClicked(event -> {
      /*
      Activity a= getActivity();
      if( a instanceof TrainingActivity )
      {
        ((TrainingActivity) a).getVibrator().signal_confirmed();
      }
      else
        throw new IllegalStateException("Tried to stop training from activity != TrainingActivity");
      */

      TrainingUIController.getInstance().stopTraining();
    });
    
    /*
    String cssLayout = "-fx-border-color: yellow;\n" +
        "-fx-border-insets: 5;\n" +
        "-fx-border-width: 10;\n" +
        "-fx-border-style: dashed;\n";
    setStyle(cssLayout);
     */

    b.getStyleClass().add("primary-value");

    parentProperty().addListener((observable, oldValue, newValue) -> {
      if( newValue != null ) {
        b.prefHeightProperty().bind(((Region) newValue).heightProperty());
        b.prefWidthProperty().bind(((Region) newValue).widthProperty());

        initialise(true);
      }
    });

    getChildren().add(b);
  }
  
  public void updateUI()
  {
    
  }
}
