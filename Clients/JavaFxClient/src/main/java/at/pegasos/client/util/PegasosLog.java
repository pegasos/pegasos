package at.pegasos.client.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import at.univie.mma.Config;

/**
 * Logging utility. All logging should go trough this facility.
 * For now on the client this uses System.(out/err) as backend.
 * On Android it uses android.util.Log as backend
 */
public class PegasosLog {
  
  // 05-09 07:51:16.741
  private final static SimpleDateFormat sdf= new SimpleDateFormat("MM-dd HH:mm:ss SSS ");
  
  public static void d(String tag, String message)
  {
    if( Config.LOGGING )
      System.out.println("D/" + sdf.format(new Date(System.currentTimeMillis())) + " [" + tag + "] " + message);
  }
  
  public static void e(String tag, String message)
  {
    if( Config.LOGGING )
      System.err.println("E/" + sdf.format(new Date(System.currentTimeMillis())) + " [" + tag + "] " + message);
  }

  public static void e(String tag, String message, Exception exception)
  {
    if( Config.LOGGING )
    {
      System.err.println("E/" + sdf.format(new Date(System.currentTimeMillis())) + " [" + tag + "] " + message);
      exception.printStackTrace();
    }
  }
  
  public static void i(String tag, String message)
  {
    if( Config.LOGGING )
      System.err.println("I/" + sdf.format(new Date(System.currentTimeMillis())) + " [" + tag + "] " + message);
  }

  public static void w(String tag, String message)
  {
    if( Config.LOGGING )
      System.err.println("W/" + sdf.format(new Date(System.currentTimeMillis())) + " [" + tag + "] " + message);
  }
}
