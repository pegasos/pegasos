package at.univie.mma.ui;

import at.pegasos.client.ui.*;

public class TimeoutUtilFx extends TimeoutUtil {
  
  // private Dialog progressDialog;
  
  public TimeoutUtilFx(PegasosClientView activity, TimeoutListener timeoutListener, int time, boolean showDialog, String waitTitle,
                       String waitMessage, String failMessage)
  {
    super(activity, timeoutListener, time, showDialog, waitTitle, waitMessage, failMessage);
  }
  
  /**
   * Starts the timer and shows the respective dialogs/toasts.
   *
   */
  public void start()
  {
    // Create the dialog
    /*activity.runOnUiThread(new Runnable() {
      @Override
      public void run()
      {
        if( istraining )
          progressDialog= new Dialog(activity, R.style.TrainingDialogTheme);
        else
          progressDialog= new Dialog(activity);
        progressDialog.setContentView(R.layout.dialog_progress);
        if( waitTitle != null )
          progressDialog.setTitle(waitTitle);
        if( waitMessage != null )
          ((TextView) progressDialog.findViewById(R.id.txtMessage)).setText(waitMessage);
        progressDialog.show();
      }
    });
    
    // Start the timer
    timer= new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run()
      {
        timeoutListener.onTimeout(TimeoutUtil.this);
        activity.runOnUiThread(new Runnable() {
          @Override
          public void run()
          {
            progressDialog.dismiss();
            if( showDialog )
            {
              AlertDialog.Builder builder;
              if( istraining )
                builder= new AlertDialog.Builder(activity, R.style.TrainingDialogTheme);
              else
                builder= new AlertDialog.Builder(activity);
              builder.setMessage(failMessage);
              builder.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                  timeoutListener.onTimeoutAcknowledged();
                }
              });
              builder.create().show();
            }
            else
            {
              activity.showToast(failMessage);
              // timeoutListener.onTimeout();
            }
            open= false;
          }
        });
      }
    }, time);
    open= true;*/
  }
  
  /**
   * Called to abort the timer and show an optional message.
   *
   * @param message
   *          Optional toast message, set to null if unused.
   */
  public void abort(final String message)
  {
    /*if( open )
    {
      timer.cancel();
      progressDialog.dismiss();
      if( message != null )
      {
        activity.showToast(message);
      }
    }*/
  }
}