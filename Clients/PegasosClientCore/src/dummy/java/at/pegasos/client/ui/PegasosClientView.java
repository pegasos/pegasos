package at.pegasos.client.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class PegasosClientView {
  public void showToast(final String msg, final int duration)
  {

  }
  
  public void showToast(String msg)
  {

  }
  
  public void showToast(int resource)
  {

  }
  
  public enum CapabilityState {
    /**
     * App is currently capable of doing this
     */
    Can,
    /**
     * App can currently NOT do this
     */
    Cannot,
    /**
     * There is no information about whether or not the app can do this
     */
    Undecided
  };
  
  public static class Capabilities {
    private Map<String, CapabilityState> caps= new HashMap<String, CapabilityState>();
    
    public void setCapability(String name, CapabilityState state)
    {
      caps.put(name, state);
    }
    
    public CapabilityState getCapability(String name)
    {
      CapabilityState s= caps.get(name);
      if( s == null )
        return CapabilityState.Undecided;
      else
        return s;
    }

    public boolean isCapable(String name)
    {
      return getCapability(name) == CapabilityState.Can;
    }
  }
  
  protected Capabilities capabilities= new Capabilities();
  
  /**
   * Return the permission this activity needs
   * @return
   */
  protected String[] getRequiredPermissions()
  {
    return new String[0];
  }
  
  /**
   * The capabilities of the app/activity have changed.
   */
  protected void onCapabilitiesChanged()
  {

  }
}
