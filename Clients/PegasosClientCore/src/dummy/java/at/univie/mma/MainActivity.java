package at.univie.mma;

import at.pegasos.client.ServerCallback;
import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.pegasos.client.ui.PegasosClientView;
import at.pegasos.serverinterface.response.LoginMessage;

public class MainActivity extends PegasosClientView {
  public void clearFrames() {}

  public void addStarter(int i, IActivityStarterFragment menu) {}

  public void add() {}

  public void onLoginMessageReceived(LoginMessage m) {}

  public void setLoggedIn(boolean b) {}

  public ServerCallback getResponseCallback()
  {
    return null;
  }
}