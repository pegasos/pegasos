package at.univie.mma;

public class Config {

  public static final boolean CAN_START_OFFLINE= false;
  public static final long SILENT_SERVER_DISCONNECTED_RETELL_INTERVALL= 0;
  public static final boolean SILENT_SERVER_DISCONNECTED_RETELL= false;
  public static final boolean SILENT_SERVER_DISCONNECTED= false;
  public static final boolean SILENT_SERVER_RECONNECTED= false;
  public static final boolean SHOW_NOTIFICATION_HEARTBEAT= false;
  public static final boolean DEBUG= true;
  public static final String MENUCLASS= null;
  public static final int SENSORCHECKER_INTERVAL_LANDING= 1500;
  public static final boolean HIDE_ANT_BUTTON= false;
  public static final boolean ANT_ALWAYS_ON= false;
  public static final boolean HIDE_BLE_BUTTON= false;
  public static final boolean BLE_ALWAYS_ON= false;
  public static final boolean LOGIN_USERLIST= false;
  public static final boolean CLUB_ONLY= false;
  public static final String BACKENDBASEURL = null;
  public static final boolean REMEMBER_LOGIN_ALWAYS = false;
}
