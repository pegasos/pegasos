package at.pegasos.concurrent;

public class TFineGrainedList<T> implements TimedList<T> {
  public class TIterator implements TimedList.TIterator<T> {
    private Node<T> pos;
    
    private TIterator(Node<T> pos)
    {
      this.pos= pos;
    }
    
    public TIterator copy()
    {
    	return new TIterator(pos);
    }
    
    @Override
    public boolean hasNext()
    {
      return pos.next != null;
    }

    @Override
    public boolean moveForward()
    {
      pos= pos.next;
      if( pos != null )
        return pos.item != null;
      else //TODO: why is this check necessary? Maybe due to not having locked the list somebody moved forward?
        return false;
    }

    @Override
    public T getItem()
    {
      pos.lock();
      try
      {
        return pos.item;
      }
      finally
      {
        pos.unlock();
      }
    }

    @Override
    public long getTimestamp()
    {
      pos.lock();
      try
      {
        return pos.timestamp;
      }
      finally
      {
        pos.unlock();
      }
    }
  }
  
  private Node<T> head;
  
  public TFineGrainedList()
  {
    head= new Node<T>(Long.MAX_VALUE);
    head.next= new Node<T>(Long.MIN_VALUE);
  }
  
  @Override
  public TimedList.TIterator<T> iterator()
  {
	  return new TIterator(head);
  }

  @Override
  public boolean add(long timestamp, T item)
  {
    head.lock();
    
    Node<T> pred= head;
    try
    {
      Node<T> curr= pred.next;
      curr.lock();
      try
      {
        while(curr.timestamp > timestamp)
        {
          pred.unlock();
          pred= curr;
          curr= curr.next;
          curr.lock();
        }
        
        if( curr.timestamp == timestamp )
        {
          curr.item= item;
          return false;
        }
        
        Node<T> n= new Node<T>(timestamp, item);
        n.next= curr;
        pred.next= n;
        
        return true;
      }
      finally
      {
        curr.unlock();
      }
    }
    finally
    {
      pred.unlock();
    }
  }

  @Override
  public void removeOlder(long timestamp)
  {
    // TODO: this method would greately benefit from a dedicated tail.
    // ie remove first locks node before removal ie curr in 139 and then make it point to tail.
    // then we can remove all nodes
    head.lock();

    Node<T> pred= head;
    Node<T> remove;
    try
    {
      Node<T> curr= pred.next;
      curr.lock();
      try
      {
        while(curr.timestamp > timestamp)
        {
          pred.unlock();
          pred= curr;
          curr= curr.next;
          curr.lock();
        }

        while(curr.next != null)
        {
          pred.next= curr.next;
          remove= curr;
          curr= curr.next;
          curr.lock();
          remove.unlock();
        }
      }
      finally
      {
        curr.unlock();
      }
    }
    finally
    {
      pred.unlock();
    }
  }
}
