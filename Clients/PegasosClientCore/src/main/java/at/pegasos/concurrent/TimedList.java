package at.pegasos.concurrent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public interface TimedList<T> {
  public static class Node<T> {
    /**
     * Timestamp for the item
     */
    long timestamp;
    
    /**
     * The item
     */
    T item;
    
    Node<T> next;
    
    Lock lock;
    
    /**
     * Construct a new node using timestamp and item
     * @param timestamp
     * @param item
     */
    public Node(long timestamp, T item)
    {
      this.item= item;
      this.timestamp= timestamp;
      this.lock= new ReentrantLock();
    }
    
    /**
     * Construct a new node without an item
     * @param timestamp
     */
    public Node(long timestamp)
    {
      this.item= null;
      this.timestamp= timestamp;
      this.lock= new ReentrantLock();
    }
    
    /**
     * Construct a new node using the item and currentTimeMillis
     * @param item
     */
    public Node(T item)
    {
      this.item= item;
      this.timestamp= System.currentTimeMillis();
      this.lock= new ReentrantLock();
    }
    
    public void lock()
    {
      lock.lock();
    }
    
    public void unlock()
    {
      lock.unlock();
    }
  }
  
  public static interface TIterator<T> {
    /**
     * @return true if a next element exists
     */
    public boolean hasNext();
    
    /**
     * Move the cursor forward and return true if an element exists on this position
     * @return
     */
    public boolean moveForward();
    
    /**
     * Return a copy of this iterator
     * @return
     */
    public TIterator<T> copy();
    
    public T getItem();
    
    public long getTimestamp();
  }

  /**
   * Add a new element to the list
   * @param timestamp
   * @param item
   * @return true if new iteam was added, false if an item with the same timestamp existed
   */
  public boolean add(long timestamp, T item);
 
  /**
   * Remove all elements older than the time stamp
   * @param timestamp
   */
  public void removeOlder(long timestamp);

	public TIterator<T> iterator();
}
