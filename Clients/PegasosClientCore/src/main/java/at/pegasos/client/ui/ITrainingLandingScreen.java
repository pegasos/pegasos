package at.pegasos.client.ui;

import at.pegasos.client.sensors.Sensor;
import at.pegasos.client.ui.sensorswitch.*;

public interface ITrainingLandingScreen {
  public static final String EXTRA_ACTIVITY= "ACTIVITY";
  public static final String EXTRA_SPORT= "SPORT";
  public static final String EXTRA_ARGUMENTS= "DISTANCE";
  public static final String EXTRA_AUTOSTART= "AUTOSTART";

    /**
     * Set the state of the start button
     *
     * @param required_ok
     *          whether all required sensors are ok
     * @param visible
     *          whether the start button should be visible
     */
  void setBtnState(boolean required_ok, boolean visible);

  /**
   * Callback method (from uicontroller) for setting the displayed sensor status
   *
   * @param i
   * @param text
   *          Text to be displayed
   * @param state
   *          State to be displayed
   */
  void setSensorStatus(final int i, final String text, final Sensor.State state);

  void setVisibilities(boolean gps, boolean hr, boolean fp, boolean extras[]);

  SensorSwitchManager getSwitchManager();

  boolean isBleCapable();

  /**
   * Start the training
   * @param continued whether or not the training was resumed
   */
  void startTraining(boolean continued);
}
