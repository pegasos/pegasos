package at.pegasos.client.activitystarter;

/**
 * Callback interface for activity starter fragments
 */
public interface IActivityStarterCallback {
  /**
   * Invoked when the fragment was clicked by the user i.e. when the activity should be started
   *
   * @param fragment fragment which was clicked
   */
  void onActivityClicked(IActivityStarterFragment fragment);
}
