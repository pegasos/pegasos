package at.pegasos.client.ui;

import java.util.*;

import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.univie.mma.Config;

public class PegasosClientMenu {
  public static class PegasosClientSubMenu {
    public IActivityStarterFragment[] menu;
    public String name;

    @Override public String toString()
    {
      return "PegasosClientSubMenu [menu=" + Arrays.toString(menu) + ", name=" + name + "]";
    }
  }

  private final List<PegasosClientSubMenu> menus;
  protected PegasosClientSubMenu main_menu;

  public PegasosClientMenu()
  {
    menus = new ArrayList<PegasosClientSubMenu>();
  }

  public PegasosClientSubMenu getMainMenu()
  {
    return main_menu;
  }

  public PegasosClientSubMenu getMenu(int number)
  {
    if( Config.DEBUG && !(number >= 0 && number < menus.size()) )
      throw new AssertionError("Tried to request a non existing menu?");

    return menus.get(number);
  }

  protected void addMenu(PegasosClientSubMenu menu)
  {
    menus.add(menu);
  }

  public void setupMenus()
  {

  }
}
