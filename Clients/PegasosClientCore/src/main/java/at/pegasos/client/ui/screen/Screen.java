package at.pegasos.client.ui.screen;

import at.pegasos.client.ui.TrainingActivity;

public abstract class Screen {
	protected TrainingActivity activity;

	public Screen(TrainingActivity activity)
	{
		this.activity= activity;
	}

	/**
	 * Put the screen on the (visual, i.e. phone) screen i.e. place yourself as a child of R.id.trianingscreen_container.
	 * Make sure to properly remove the old screen if necessary by calling old.cleanUI()
	 * @param old the old screen
	 */
	public abstract void putOnUI(Screen old);

	/**
	 * Send an update signal to all elements on the screen
	 */
	public abstract void updateUI();

	/**
	 * Safely remove this screen from the training activity.
	 * This means removing all your content from R.id.trainingscreen_container
	 */
	public abstract void cleanUI();
}
