package at.pegasos.client;

public class Preferences {
  public final static String ant_sensors_enabled= "ant_sensors_enabled";
  public final static String ble_sensors_enabled= "ble_sensors_enabled";
  public static final String auto_login= "auto_login";
  public static final String username= "username";
  public static final String password= "password";
  public static final String club_usage= "club_usage";
  public static final String language= "language";
  public static final String sport_selection_last_sport= "sport_selection_last_sport";
  public static String footpod_int = "footpod_int";
  public static final String max_hr = "max_hr";
  public static String user_list = "user_list";
  public static final String pref_key_app_language= "pref_key_app_language";
  public static String token = "token";
}
