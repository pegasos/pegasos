package at.pegasos.client;

import java.util.*;

import at.pegasos.client.util.PreferenceManager;
import at.pegasos.client.values.*;
import at.pegasos.serverinterface.response.LoginMessage;

public class UserValues {

  private final static UserValues instance;
  private final Collection<String> definedValues;

  static
  {
    instance = new UserValues();
  }

  public static UserValues getInstance()
  {
    return instance;
  }

  private UserValues()
  {
    definedValues = new ArrayList<String>(30);
  }

  public final Collection<String> getDefinedValues()
  {
    return definedValues;
  }

  public void restoreFromLast()
  {
    definedValues.clear();

    int footpod_calib = PreferenceManager.getInstance().getInt(Preferences.footpod_int, 1000);
    ValueStore.getInstance().setFpCalibInt(footpod_calib);

    int max_hr = PreferenceManager.getInstance().getInt(Preferences.max_hr, -1);
    ValueStore.getInstance().getCreateValueInt("MAX_HR").setIntValue(max_hr);
    definedValues.add("MAX_HR");

    Set<String> vals = PreferenceManager.getInstance().getStringSet("USERVALUES_LIST", new HashSet<String>(0));
    for(String valr : vals)
    {
      String[] h = valr.split("#");
      String name = h[0];
      String type = h[1];
      definedValues.add(name);

      if( type.equals("DEC") )
      {
        ValueStore.getInstance().getCreateValueDouble(name).setDoubleValue(
                PreferenceManager.getInstance().getDouble("USERVALUE_" + name, -1));
      }
      if( type.equals("INT") )
      {
        ValueStore.getInstance().getCreateValueInt(name).setIntValue(
                PreferenceManager.getInstance().getInt("USERVALUE_" + name, -1));
      }
      if( type.equals("TEXT") )
      {
        ValueStore.getInstance().getCreateTextValue(name).setStringValue(
                PreferenceManager.getInstance().getString("USERVALUE_" + name, ""));
      }
    }
  }

  public void setFromLoginMessage(LoginMessage message)
  {
    definedValues.clear();

    int footpod_calib = message.getFootpod_calib();

    PreferenceManager.getInstance().startEdit();

    PreferenceManager.getInstance().storeInt(Preferences.footpod_int, footpod_calib);
    ValueStore.getInstance().setFpCalibInt(footpod_calib);

    int max_hr = message.getMaxHr();
    PreferenceManager.getInstance().storeInt(Preferences.max_hr, max_hr);
    ValueStore.getInstance().getCreateValueInt("MAX_HR").setIntValue(max_hr);
    definedValues.add("MAX_HR");

    Set<String> vals = new HashSet<String>();
    for(LoginMessage.UserData data : message.getUserData())
    {
      definedValues.add(data.name);

      switch(data.type)
      {
        case DEC:
          ValueStore.getInstance().getCreateValueDouble(data.name).setDoubleValue((Double) data.value);
          PreferenceManager.getInstance().storeDouble("USERVALUE_" + data.name, (Double) data.value);
          break;
        case INT:
          ValueStore.getInstance().getCreateValueInt(data.name).setIntValue((Integer) data.value);
          PreferenceManager.getInstance().storeInt("USERVALUE_" + data.name, (Integer) data.value);
          break;
        case TEXT:
          ValueStore.getInstance().getCreateTextValue(data.name).setStringValue((String) data.value);
          PreferenceManager.getInstance().storeString("USERVALUE_" + data.name, (String) data.value);
          break;
        default:
          break;
      }
      vals.add(data.name + "#" + data.type);
    }
    PreferenceManager.getInstance().storeStringSet("USERVALUES_LIST", vals);

    PreferenceManager.getInstance().apply();
  }

  public void remove()
  {
    definedValues.clear();

    PreferenceManager.getInstance().startEdit();

    PreferenceManager.getInstance().remove(Preferences.footpod_int);

    PreferenceManager.getInstance().remove(Preferences.max_hr);
    ValueStore.getInstance().removeValue("MAX_HR");

    Set<String> vals = PreferenceManager.getInstance().getStringSet("USERVALUES_LIST", new HashSet<String>());
    for(String valueName : vals)
    {
      ValueStore.getInstance().removeValue(valueName);
      PreferenceManager.getInstance().remove(valueName);
    }
    PreferenceManager.getInstance().remove("USERVALUES_LIST");

    PreferenceManager.getInstance().apply();
  }
}
