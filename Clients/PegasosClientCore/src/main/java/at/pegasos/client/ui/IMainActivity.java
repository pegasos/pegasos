package at.pegasos.client.ui;

import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.pegasos.serverinterface.response.LoginMessage;

public interface IMainActivity {
  void clearFrames();

  void addStarter(int i, IActivityStarterFragment menu);

  void add();

  void onLoginMessageReceived(LoginMessage m);

  void showToast(String s);

  void setLoggedIn(boolean b);
}
