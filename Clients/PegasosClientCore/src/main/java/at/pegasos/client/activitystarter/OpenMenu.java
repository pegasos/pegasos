package at.pegasos.client.activitystarter;

import at.univie.mma.Config;
import at.pegasos.client.ui.MenuManager;

public class OpenMenu implements IActivityStarterCallback {
  private MenuManager manager;
  private int nr;
  
  public OpenMenu(MenuManager manager, int nr)
  {
    this.manager= manager;
    this.nr= nr;
  }
  
  @Override
  public void onActivityClicked(IActivityStarterFragment fragment)
  {
    // No need for setting intent parameters here. However, just some safety check
    if( Config.DEBUG && fragment.getAutoStart() )
      throw new AssertionError();
    
    manager.openMenu(nr);
  }
}
