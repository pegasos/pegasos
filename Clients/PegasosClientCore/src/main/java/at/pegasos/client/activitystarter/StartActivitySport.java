package at.pegasos.client.activitystarter;

import at.pegasos.client.ui.ITrainingLandingScreen;
import at.pegasos.data.Pair;
import at.pegasos.client.ui.MenuManager;

/**
 * Start an activity directly with a sport i.e. go directly to TrainingLandingScreen
 */
public class StartActivitySport implements IActivityStarterCallback {
  
  private final MenuManager manager;
  private final int controller_id;
  private final String sportname;

  /**
   * Start an activity with a sport
   * 
   * @param manager
   * @param controller_id
   *          ID of the controller
   * @param sportname
   *          name of the sport
   */
  public StartActivitySport(MenuManager manager, int controller_id, String sportname)
  {
    this.manager= manager;
    this.controller_id= controller_id;
    this.sportname= sportname;
  }

  @Override
  public void onActivityClicked(IActivityStarterFragment fragment)
  {
    Pair<Object, Object> extras[]= new Pair[] {
      new Pair<>(ITrainingLandingScreen.EXTRA_ACTIVITY, controller_id),
      new Pair<>(ITrainingLandingScreen.EXTRA_SPORT, sportname)
    };

    manager.goToLanding(fragment, extras);
  }
}
