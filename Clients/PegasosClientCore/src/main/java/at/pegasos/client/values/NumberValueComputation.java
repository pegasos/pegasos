package at.pegasos.client.values;

import java.util.*;

public class NumberValueComputation extends NumberValue {
  private final List<ComputationValue> computations;

  public NumberValueComputation(String name)
  {
    super(name);
    this.computations = new ArrayList<>(5);
  }

  public NumberValueComputation(String name, Double initial)
  {
    super(name, initial);
    this.computations = new ArrayList<>(5);
  }

  public void attachComputation(ComputationValue computationValue)
  {
    this.computations.add(computationValue);
  }

  private void update(long time)
  {
    for(ComputationValue comp : computations)
      comp.update(internal, time);
  }

  @Override
  public void setIntValue(Integer value)
  {
    long time = System.currentTimeMillis();
    internal= value.doubleValue();
    update(time);
  }

  @Override
  public void setDoubleValue(Double value)
  {
    long time = System.currentTimeMillis();
    internal= value;
    update(time);
  }

  @Override
  public void setStringValue(String value)
  {
    long time = System.currentTimeMillis();
    internal= Double.parseDouble(value);
    update(time);
  }
}
