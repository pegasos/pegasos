package at.pegasos.client.values;

import java.util.*;

import at.pegasos.client.util.*;

public class Max extends TimerTask /*implements ComputedValueList*/ {
  private final static String LOG_TAG = "Max";

  protected final String inname;
  private final double sec_past;
  protected Value outval;
  /**
   * timestamp of the youngest/oldest event which has been displayed already
   */
  protected long lastEvent = Long.MIN_VALUE;
  ValueStore.TimedDoubleValue[] buffer;

  public Max(String inname, String outname, double sec_past)
  {
    this.inname = inname;
    this.outval = ValueStore.getInstance().getCreateNumberValue(outname);
    this.buffer = new ValueStore.TimedDoubleValue[20 * (int) sec_past];
    this.sec_past = sec_past;
  }

  // @Override
  public static Max schedule(Timer timer, String inname, String outname, int sec)
  {
    Max ret = new Max(inname, outname, sec * 1000);
    timer.scheduleAtFixedRate(ret, sec * 1000L, sec * 1000L);
    return ret;
  }

  public static Max scheduleList(Timer timer, String inname, String outname, int sec, int interv)
  {
    Max ret = new MaxList(inname, outname, sec * 1000);
    timer.scheduleAtFixedRate(ret, sec * 1000L, interv * 1000L);
    return ret;
  }

  public void reset()
  {
    lastEvent = System.currentTimeMillis();
    outval.setDoubleValue(null);
    PegasosLog.d(LOG_TAG, "Reset. lastEvent: " + lastEvent);
  }

  public void compute()
  {
  	/*
    // +1 in order to avoid adding the same value twice
    int elements= ValueStore.getInstance().fetchTimedDoubleListValues(list, lastEvent+1, buffer);
    
    double v = outval.getDoubleValue();

    // add all new elements to the sum
    for(int i= elements - 1; i >= 0; i--)
    {
      v = Math.max((Double) buffer[i].value, v);

      lastEvent= buffer[i].timestamp;
    }

    // PegasosLog.d(LOG_TAG, String.format("AUC end. sum: %.1f, lastEvent: %d", sum, lastEvent));
    outval.setDoubleValue(v);
    */
    Double v = outval.getDoubleValue();
    if( v != null )
    {
      v = Math.max(ValueStore.getInstance().getValue(inname).getDoubleValue(), v);
      outval.setDoubleValue(v);
    }
  }

  @Override
  public void run()
  {
    compute();
  }

  public Value getValue()
  {
    return outval;
  }

  public static class MaxList extends Max {
    public MaxList(String inname, String outname, double sec_past)
    {
      super(inname, outname, sec_past);
    }

    public void compute()
    {
      // +1 in order to avoid adding the same value twice
      int elements = ValueStore.getInstance().fetchTimedDoubleListValues(ValueStore.getInstance().getList(inname), lastEvent + 1, buffer);

      Double v = outval.getDoubleValue();

      boolean wasNull = false;
      if( v == null )
      {
        wasNull = true;
        v = Double.MIN_VALUE;
      }

      // check all new elements
      for(int i = elements - 1; i >= 0; i--)
      {
        v = Math.max((Double) buffer[i].value, v);

        lastEvent = buffer[i].timestamp;
      }

      // PegasosLog.d(LOG_TAG, String.format("AUC end. sum: %.1f, lastEvent: %d", sum, lastEvent));
      if( wasNull )
      {
        if( !v.equals(Double.MIN_VALUE) )
          outval.setDoubleValue(v);
      }
      else
        outval.setDoubleValue(v);
    }
  }
}
