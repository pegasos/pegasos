package at.pegasos.client.sensors;

import at.pegasos.client.PegasosClient;
import at.pegasos.client.PegasosErrorReporter;
import at.pegasos.client.util.PegasosLog;

import java.io.*;
import java.util.*;

/**
 * Handles CRUD methods for Sensors and SensorParams.
 */
public class SensorConfigService {

  /**
   * Loads the provided SensorParams of the provided sensor.
   * @param clazz The class of the sensor the parameters are loaded from.
   * @return An array of the loaded SensorParams.
   */
  public static SensorParam[] loadParams(String clazz)
  {
    Properties props=new Properties();
    InputStream inputStream;
    try
    {
      File in= new File(PegasosClient.getDirConfig() + "/" + clazz + ".sensors");
      if( !in.exists() )
      {
        // No configuration for this sensor exists...
        return null;
      }

      inputStream = new FileInputStream(in);
      props.load(inputStream);

      int count= Integer.parseInt(props.getProperty("count"));

      if( count == 0 )
        return null;

      SensorParam[] params= new SensorParam[count];
      for(int i= 0; i < count; i++)
      {
        params[i]= new SensorParam();
        params[i].sensor_id= new ArrayList<Map.Entry<String,String>>();
        params[i].param= new ArrayList<Map.Entry<String,String>>();
      }

      for(Map.Entry<Object, Object> e : props.entrySet())
      {
        if( e.getKey().toString().equals("count") )
          continue;

        String[] h= e.getKey().toString().split("[.]");

        int nr= Integer.parseInt(h[0]);

        if( h[1].equals("id") )
        {
          if( h.length > 2 )
          {
            int hh= e.getKey().toString().indexOf(".");
            hh= e.getKey().toString().indexOf(".", hh+1);
            String k= ((String) e.getKey()).substring(hh+1);
            params[nr].sensor_id.add(new AbstractMap.SimpleEntry<String,String>(k, (String) e.getValue()));
          }
          else
            params[nr].sensor_id.add(new AbstractMap.SimpleEntry<String,String>(null, (String) e.getValue()));
        }
        else
        {
          if( h.length >= 2 )
          {
            int hh= ((String) e.getKey()).indexOf(".");
            hh= ((String) e.getKey()).indexOf(".", hh+1);
            String k= ((String) e.getKey()).substring(hh+1);
            params[nr].param.add(new AbstractMap.SimpleEntry<String,String>(k, (String) e.getValue()));
          }
          else
            params[nr].param.add(new AbstractMap.SimpleEntry<String,String>(null, (String) e.getValue()));
        }
      }

      return params;
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      PegasosErrorReporter.get().silentException(e);
    }
    catch (IOException e) {
      e.printStackTrace();
      PegasosErrorReporter.get().silentException(e);
    }

    return null;
  }

  /**
   * Writes the provided SensorParams to the provided sensor.
   * @param clazz The class of the sensor the parameters are written to.
   * @param params An array of SensorParams to be written to a sensor.
   */
  private static void writeParams(String clazz, SensorParam[] params)
  {
    Properties props = new Properties();

    int i= 0;
    for(SensorParam p : params)
    {
      for(Map.Entry<String,String> id : p.sensor_id)
      {
        if( id.getKey() != null)
          props.setProperty(i + ".id." + id.getKey(), id.getValue());
        else
          props.setProperty(i + ".id", id.getValue());
      }
      for(Map.Entry<String,String> param : p.param)
      {
        if( param.getKey() != null)
          props.setProperty(i + ".param." + param.getKey(), param.getValue());
        else
          props.setProperty(i + ".param", param.getValue());
      }

      i++;
    }

    props.setProperty("count", "" + params.length);

    File f = new File(PegasosClient.getDirConfig() + "/" + clazz + ".sensors");
    OutputStream out;
    try
    {
      out = new FileOutputStream( f );
      props.store(out, "This is an optional header comment string");
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
      PegasosErrorReporter.get().silentException(e);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      PegasosErrorReporter.get().silentException(e);
    }
  }

  /**
   * Adds a new sensor instance to the app.
   * @param clazz The class of the new sensor to add.
   * @param param The parameters of the new sensor to add.
   */
  public static void addSensorInstance(String clazz, SensorParam param)
  {
    if( param == null )
      return;

    SensorParam[] params_old= loadParams(clazz);
    SensorParam[] params;

    if( params_old == null ) // no params before
    {
      params= new SensorParam[1];
      params[0]= param;
    }
    else
    {
      params= new SensorParam[params_old.length + 1];
      for(int i= 0; i < params_old.length; i++)
        params[i]= params_old[i];
      params[params_old.length]= param;
    }

    writeParams(clazz, params);
  }

  /**
   * Removes a sensor instance from the app.
   * @param clazz The class of the sensor to remove.
   * @param param The parameters of the sensor to remove.
   */
  public static void removeSensorInstance(String clazz, SensorParam param)
  {
    PegasosLog.d("SensorService", "removeSensorInstance: " + clazz + " " + param);
    SensorParam[] params= loadParams(clazz);
    PegasosLog.d("SensorService", Arrays.toString(params) + " " + params.length);
    List<SensorParam> out= new ArrayList<SensorParam>(params.length - 1);
    int length= params.length;
    for(int i= 0; i < length; i++)
    {
      if( !params[i].equals(param) )
      {
        out.add(params[i]);
      }
    }

    SensorParam[] write= new SensorParam[out.size()];
    write= out.toArray(write);

    PegasosLog.d("SensorService", "Writing" + Arrays.toString(write));
    writeParams(clazz, write);
  }

  /**
   * Replaces a sensor instance with another sensor instance.
   * @param clazz The class of the sensor being replaced.
   * @param idx The SensorParam array position indicating the sensor to replace.
   * @param param The parameters of the new sensor.
   */
  public static void replaceSensorInstance(String clazz, int idx, SensorParam param)
  {
    SensorParam[] params= loadParams(clazz);
    params[idx]= param;
    writeParams(clazz, params);
  }
}
