package at.pegasos.client.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

import at.pegasos.client.activitystarter.IActivityStarterFragment;
import at.pegasos.data.Pair;
import at.pegasos.client.util.PegasosLog;
import at.univie.mma.*;
import at.pegasos.client.ui.PegasosClientMenu.PegasosClientSubMenu;

public abstract class MenuManager {
  private final MainActivity activity;
  
  private PegasosClientMenu menu;
  private int menu_depth; 
  private PegasosClientSubMenu onscreen;
  private final Stack<PegasosClientSubMenu> screen_stack;
  
  public MenuManager(MainActivity activity)
  {
    this.activity= activity;
    
    try
    {
      menu= (PegasosClientMenu) Class.forName(Config.MENUCLASS).getConstructor(MenuManager.class).newInstance(this);
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
    }
    catch (IllegalArgumentException e)
    {
      e.printStackTrace();
    }
    catch (InvocationTargetException e)
    {
      e.printStackTrace();
    }
    catch (NoSuchMethodException e)
    {
      e.printStackTrace();
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
    }
    
    menu_depth= 0;
    screen_stack= new Stack<PegasosClientSubMenu>();
  }
  
  public void createMainMenu()
  {
    menu.setupMenus();
    putMenuOnScreen(menu.getMainMenu());
  }
  
  /**
   * Called from MainActivity when a BackPress is reported
   * @return true when the BackPress is by the menu structure
   */
  public boolean onBackPress()
  {
    PegasosLog.d("MenuManager", "onBackPress " + menu_depth + " " + screen_stack.size());
    if( menu_depth > 1 )
    {
      screen_stack.pop();
      // Remove the screen from the stack as it will be added by putMenuOnScreen again
      onscreen= screen_stack.pop();
      menu_depth-= 2;
      
      putMenuOnScreen(onscreen);
      
      PegasosLog.d("MenuManager", "onBackPress done " + menu_depth + " " + screen_stack.size() + " " + screen_stack);
      
      return true;
    }
    else
      return false;
  }
  
  private void putMenuOnScreen(PegasosClientSubMenu menu)
  {
    PegasosLog.d("MenuManager", "putMenuOnScreen " + menu_depth + " " + screen_stack.size());
    activity.clearFrames();
    int i;
    for(i= 0; i < menu.menu.length; i++)
    {
      activity.addStarter(i, menu.menu[i]);
    }
    activity.add();
    
    screen_stack.add(menu);
    onscreen= menu;
    menu_depth++;
    PegasosLog.d("MenuManager", "putMenuOnScreen done " + menu_depth + " " + screen_stack.size());
  }
  
  public MainActivity getActivity()
  {
    return activity;
  }
  
  public void openMenu(int i)
  {
    PegasosLog.d("MenuManager", "openMenu " + menu_depth + " " + screen_stack.size());
    PegasosClientSubMenu men= menu.getMenu(i);
    putMenuOnScreen(men);
    PegasosLog.d("MenuManager", "openMenu done " + menu_depth + " " + screen_stack.size());
  }

  public abstract void goToLanding(IActivityStarterFragment fragment, Pair<Object, Object>[] arguments);
  /*{
    Intent i= null;
    try
    {
      i= new Intent(getActivity(), Class.forName("at.univie.mma.ui.TrainingLandingScreen"));
      for( Pair<Object, Object> arg : arguments)
      {
        i.putExtra(arg.first, arg.second);
      }
      ActivityStarterFragment.setParameters(fragment, i);

      getActivity().startActivity(i);
    }
    catch( ClassNotFoundException e )
    {
      e.printStackTrace();
      throw new IllegalStateException();
    }
  }*/

  /*public MMA getApp()
  {
    return activity.getApp();
  }*/
}
