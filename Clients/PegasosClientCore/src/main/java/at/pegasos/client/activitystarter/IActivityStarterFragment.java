package at.pegasos.client.activitystarter;

public interface IActivityStarterFragment {
  boolean getAutoStart();
}
