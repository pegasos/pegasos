package at.pegasos.client.values;

public class IntegerValue extends Value {
  protected Integer internal;

  public IntegerValue(String name)
  {
    super(name);
  }

  public Integer getIntValue()
  {
    return internal;
  }

  public void setIntValue(Integer value)
  {
    internal = value;
  }

  public Double getDoubleValue()
  {
    if( internal != null )
      return internal.doubleValue();
    else
      return null;
  }

  public void setDoubleValue(Double value)
  {
    internal = value.intValue();
  }

  public String getStringValue()
  {
    if( internal != null )
      return internal.toString();
    else
      return null;
  }

  public void setStringValue(String value)
  {
    if( value != null )
      internal = Integer.parseInt(value);
    else
      internal = null;
  }
}
