package at.pegasos.client.values;

import java.util.*;

import at.pegasos.client.util.*;
import at.pegasos.concurrent.*;
import at.pegasos.concurrent.TimedList.*;

public class Average extends TimerTask {
  private String LOG_TAG= "Average";

  private final TimedList<Double> list;
  
  // protected Value outval;
  protected final String outname;
  protected final ValueStore store;
  // private Value debug;

  private final double sec_past;
  private final double msec_past;

  ValueStore.TimedDoubleValue[] buffer;

  private final long MAX_DIST;

  /**
   * timestamp of the youngest/oldest event which has been displayed already
   */
  // private long lastEvent= Long.MIN_VALUE;

  protected Double last_value;
  
  public static Average scheduleSimple(Timer timer, String inname, String outname, double sec)
  {
    Average ret= new SimpleAverage(ValueStore.getInstance().getList(inname), outname, sec);
    timer.schedule(ret, (long) (Math.random() * 10), 500);
    return ret;
  }

  public static Average scheduleWeighted(Timer timer, String inname, String outname, double sec)
  {
    Average ret= new WeightedAverage(ValueStore.getInstance().getList(inname), outname, sec);
    timer.schedule(ret, (long) (Math.random() * 10), 500);
    return ret;
  }

  public Average(TimedList<Double> list, String outname, double sec_past)
  {
    this.store = ValueStore.getInstance();

    this.list= list;

    this.outname = outname;
    this.buffer= new ValueStore.TimedDoubleValue[20 * (int) sec_past + 40];
    if( store.getValue(outname) == null )
      store.getCreateNumberValue(outname);
    // this.debug= ValueStore.getInstance().getCreateTextValue("DEBUG", "??");
    this.sec_past= sec_past;
    this.msec_past = sec_past * 1000;
    MAX_DIST= 2000;
    LOG_TAG += outname;
  }
 /* 
  public double getWeightedAverage()
  {
    long to= System.currentTimeMillis();
    long from= to - (long) (sec_past * 1000);
    
    // int n= 0;
    // String h="";
    
    // we start at the head
    TIterator<Double> lit= list.iterator();
    
    // check if there are value
    if( lit.getTimestamp() >= to )
    {
      long lastTime= 0;
      long currTime;
      
      // go to our starting position
      while( (currTime= lit.getTimestamp()) >= to )
      {
        // h+= "mv " + currTime + "\n";
        lit.moveForward();
      }
      
      // h+= "first2: " + lit.getTimestamp() + "/" + to + "\n";
      
      // first sample counts only up to the begin of the window
      lastTime= to;
      
      double sum= 0;
      boolean next= false;
      
      do
      {
        if( lit.getItem() != null )
        {
          currTime= lit.getTimestamp();
          sum+= lit.getItem() * (lastTime - currTime);
          // h+= lit.getItem() + " /" + lastTime + "-" + currTime + " \n ";
          // h+= lit.getItem() + " " + lit.getTimestamp() + "\n";
          // n+= 1;
          lastTime= currTime;
        }
        // else
        //  h+= "eliminated sample?\n";
        
        next= lit.moveForward();
      } while( next && (currTime= lit.getTimestamp()) >= from );
      
      // debug.setStringValue(n + " \n " + h);
      // outval.setDoubleValue(sum / ((double) (to - currTime)));
      
      return sum / ((double) (to - currTime));
    }
    else
    {
      // outval.setStringValue("0 :(");
      return 0;
    }
  }*/
  
  public Double getSimpleWeightedAverage()
  {
    long curT = System.currentTimeMillis();
    long past = (long) (curT - msec_past);
    int elements= ValueStore.getInstance().fetchTimedDoubleListValues(list, past, buffer);

    double sum = 0;

    long lastEvent = curT;

    // add all new elements to the sum
    for(int i= 0; i < elements; i++)
    {
      double dist = Math.max(Math.min(lastEvent - buffer[i].timestamp, MAX_DIST), 0);
      sum += ((Double) buffer[i].value) * dist;

      /*PegasosLog.d(LOG_TAG, String.format("v: %.1f, t: %d, sum: %.1f, lastEvent: %d, dist: %.1f",
              (Double) buffer[i].value, buffer[i].timestamp, sum, lastEvent, dist));*/

      lastEvent= buffer[i].timestamp;
    }

    if( elements > 0 )
    {
	  // PegasosLog.d(LOG_TAG, sum + " " + past + " " + buffer[elements-1].timestamp + " " + ( curT - buffer[elements-1].timestamp ) + " " + msec_past);
      // are we computing for a full time window?
      if( past < buffer[elements-1].timestamp )
      {
      	return sum / ( curT - buffer[elements-1].timestamp );
      }
      else
      {
	    return sum / msec_past;
      }
    }
    else
      return null;
  }
  
  public Double getSimpleAverage()
  {
    long to= System.currentTimeMillis();
    long from= to - (long) (sec_past * 1000);
    
    int n= 0;
    // String h="";
    
    // we start at the head
    TIterator<Double> lit= list.iterator();
    
    // check if there are values
    if( lit.getTimestamp() >= to )
    {
      // go to our starting position
      while( lit.getTimestamp() >= to )
      {
        lit.moveForward();
      }
      
      double sum= 0;
      boolean next= false;
      
      do
      {
        if( lit.getItem() != null )
        {
          sum+= lit.getItem();
          n+= 1;
        }
        
        next= lit.moveForward();
      } while( next && lit.getTimestamp() >= from );
      
      return sum / ((double) n);
    }
    else
    {
      return null;
    }
  }
  
  @Override
  public void run()
  {
    // getWeightedAverage();
    // outval.setDoubleValue(getWeightedAverage());
    // outval.setDoubleValue(getSimpleAverage());
    last_value= getSimpleAverage();
    PegasosLog.d(LOG_TAG, "output: " + last_value);
    store.getValue(outname).setDoubleValue(last_value);
  }

  public static class WeightedAverage extends Average {
    public WeightedAverage(TimedList<Double> list, String outname, double sec_past)
    {
      super(list, outname, sec_past);
    }

    public void run()
    {
      last_value= getSimpleWeightedAverage();
      // PegasosLog.d(LOG_TAG, "output: " + last_value);
      store.getValue(outname).setDoubleValue(last_value);
    }
  }

  public static class SimpleAverage extends Average {
    public SimpleAverage(TimedList<Double> list, String outname, double sec_past)
    {
      super(list, outname, sec_past);
    }

    public void run()
    {
      last_value= getSimpleAverage();
      // PegasosLog.d(LOG_TAG, "output: " + last_value);
      store.getValue(outname).setDoubleValue(last_value);
    }
  }
}
