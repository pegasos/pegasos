package at.pegasos.client.sensors;

import java.util.List;
import java.util.Map.Entry;

import at.pegasos.client.ui.ParameterQuestion;

public interface SensorCallbackListener {
  /**
   * This method is called when a sensor has been detected.
   * 
   * @param sensor
   *          The detected sensor.
   * @param types
   *          The supported types of the detected sensor.
   * @param id
   *          The ID of the detected sensor.
   * @param questions
   *          An optional list of questions.
   * @param params
   *          An optional list of parameters of this sensor
   */
  void onSensorDetected(Sensor sensor, String[] types, String id, final List<ParameterQuestion> questions,
      List<Entry<String, String>> params);
}