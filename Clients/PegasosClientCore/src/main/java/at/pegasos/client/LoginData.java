package at.pegasos.client;

import java.io.Serializable;

import at.pegasos.client.util.*;
import at.pegasos.serverinterface.response.*;
import at.univie.mma.serverinterface.ServerInterface;
//import at.pegasos.serverinterface.response.LoginMessage;

public class LoginData implements Serializable {
  private static final long serialVersionUID= -4323493352614442939L;

  public String username;
  public String token;
  public boolean clubsession;
  public int language;

  private static LoginData instance= new LoginData();

  public static LoginData createFromPreferences()
  {
    PreferenceManager preferences = PreferenceManager.getInstance();
    boolean club = preferences.getBoolean(Preferences.club_usage, true);
    int lang = preferences.getInt(Preferences.language, ServerInterface.LANGUAGE_DE);

    instance = new LoginData();
    instance.username = preferences.getString(Preferences.username, "");
    instance.token = preferences.getString(Preferences.token, "");
    instance.clubsession = club;
    instance.language = lang;

    return instance;
  }

  public static LoginData getLoginData()
  {
    return instance;
  }

  /**
   * If store is true: Store the login information as preference and set autologin to true
   */
  public static LoginData createFromData(LoginMessage m, String token, boolean club_usage, int language, boolean store)
  {
    PegasosLog.d("LoginData", "createFromData store?:" + store);
    if( store )
    {
      PreferenceManager editor = PreferenceManager.getInstance();
      editor.startEdit();
      editor.storeString(Preferences.username, m.getUsername());
      editor.storeString(Preferences.token, token);
      editor.storeBoolean(Preferences.club_usage, club_usage);
      editor.storeInt(Preferences.language, language);
      editor.storeBoolean(Preferences.auto_login, true);
      editor.apply();
    }

    instance = new LoginData();
    instance.username = m.getUsername();
    instance.token = token;
    instance.clubsession = club_usage;
    instance.language = language;
    return instance;
  }

  public static void removePreferences()
  {
    PreferenceManager editor= PreferenceManager.getInstance();
    editor.startEdit();
    editor.remove(Preferences.username);
    editor.remove(Preferences.token);
    editor.remove(Preferences.club_usage);
    editor.remove(Preferences.language);
    editor.storeBoolean(Preferences.auto_login, false);
    editor.apply();
  }
}
