package at.pegasos.client.sensors;

import java.util.*;
import java.util.Map.Entry;

public class SensorParam {
  public ArrayList<Entry<String, String>> sensor_id;
  public ArrayList<Entry<String, String>> param;
  
  @Override
  public boolean equals(Object o)
  {
    if( !this.getClass().equals(o.getClass()) )
      return false;
    
    SensorParam other= (SensorParam) o;
    
    if( sensor_id.size() != other.sensor_id.size() || param.size() != other.param.size() )
      return false;
    
    for(Entry<String, String> id : other.sensor_id)
    {
      boolean found= false;
      for(Entry<String, String> tid : this.sensor_id)
      {
        if( tid.getKey() == null && id.getKey() == null && tid.getValue().equals(id.getValue())
            || tid.getKey() != null && id.getKey() != null && tid.getKey().equals(id.getKey()) && tid.getValue().equals(id.getValue()) )
        {
          found= true;
          break;
        }
      }
      
      if( !found )
        return false;
    }
    
    for(Entry<String, String> param : other.param)
    {
      boolean found= false;
      for(Entry<String, String> tparam : this.param)
      {
        if( tparam.getKey().equals(param.getKey()) && tparam.getValue().equals(param.getValue()) )
        {
          found= true;
          break;
        }
      }
      if( !found )
        return false;
    }
    
    return true;
  }
  
  public SensorParam()
  {
    this.sensor_id= null;
    this.param= null;
  }
  
  public SensorParam(ArrayList<Entry<String, String>> sensor_id, ArrayList<Entry<String, String>> param)
  {
    this.sensor_id= sensor_id;
    this.param= param;
  }
  
  public String toString()
  {
    String ret= "Param <ids:[";
    boolean comma;
    
    comma= false;
    if( sensor_id != null )
      for(Entry<String, String> x : sensor_id)
      {
        if( comma )
          ret+= ",";
        ret+= x.getKey() + ":" + x.getValue();
        comma= true;
      }
    else
      ret+= "null";
    ret+= "]";
    
    ret+= " values:[";
    comma= false;
    if( param != null )
      for(Entry<String, String> x : param)
      {
        if( comma )
          ret+= ",";
        ret+= x.getKey() + ":" + x.getValue();
        comma= true;
      }
    else
      ret+= "null";
    ret+= "]";
    
    ret+= ">";
    
    return ret;
  }
  
  public String getSensorId(String name)
  {
    for(Entry<String, String> e : sensor_id)
    {
      if( e.getKey() == null && name == null || e.getKey() != null && e.getKey().equals(name) )
        return e.getValue();
    }
    
    return "";
  }
  
  public String getSensorId()
  {
    return getSensorId(null);
  }
  
  public void addSensorId(String key, String value)
  {
    if( this.sensor_id == null )
    {
      this.sensor_id= new ArrayList<Entry<String, String>>(1);
      
      sensor_id.add(new AbstractMap.SimpleEntry<String, String>(key, value));
    }
    else
    {
      boolean found= false;
      for(Entry<String, String> oldid : sensor_id)
      {
        if( oldid.getKey().equals(key) )
        {
          found= true;
          // Update value
          oldid.setValue(value);
          break;
        }
        
        if( !found )
          sensor_id.add(new AbstractMap.SimpleEntry<String, String>(key, value));
      }
    }
  }
  
  /**
   * Return the parameter with the requested name. Null if not found
   * 
   * @param name
   * @return the parameter-value or null if no such parameter exists.
   */
  public String getParam(String name)
  {
    for(Entry<String, String> e : param)
    {
      if( e.getKey() == null && name == null || e.getKey() != null && e.getKey().equals(name) )
        return e.getValue();
    }
    
    return null;
  }
  
  /**
   * Return the parameter with the requested name. If the value does not exist `default_value` is returned
   * 
   * @param name
   * @param default_value value to return if `name` is not specified
   * @return the parameter-value or default-value
   */
  public String getParam(String name, String default_value)
  {
    String val= getParam(name);
    
    if( val != null )
      return val;
    else
      return default_value;
  }

  public ArrayList<Entry<String, String>> getAllParams()
  {
    return param;
  }
  
  public void addParams(ArrayList<Entry<String, String>> params)
  {
    if( this.param == null )
    {
      this.param= new ArrayList<Entry<String, String>>(params.size());
      
      for(Entry<String, String> x : params)
        param.add(x);
    }
    else
    {
      for(Entry<String, String> newparam : params)
      {
        boolean found= false;
        for(Entry<String, String> oldparam : param)
        {
          if( oldparam.getKey().equals(newparam.getKey()) )
          {
            found= true;
            // Update value
            oldparam.setValue(newparam.getValue());
            break;
          }
        }
        
        if( !found )
          param.add(newparam);
      }
    }
  }
  
  public void addParam(String key, String value)
  {
    if( this.param == null )
    {
      this.param= new ArrayList<Entry<String, String>>(1);
      
      param.add(new AbstractMap.SimpleEntry<String, String>(key, value));
    }
    else
    {
      boolean found= false;
      for(Entry<String, String> oldparam : param)
      {
        if( oldparam.getKey().equals(key) )
        {
          found= true;
          // Update value
          oldparam.setValue(value);
          break;
        }
      }
      if( !found )
        param.add(new AbstractMap.SimpleEntry<String, String>(key, value));
    }
  }
}
