package at.pegasos.client.values;

import java.util.*;

public class Computer {
  public interface Computation {
    void compute();
  }

  private final Collection<Computation> veryhigh;

  private final Collection<Computation> high;

  private final Collection<Computation> low;

  private Timer veryhighTimer;
  private Timer highTimer;
  private Timer lowTimer;

  private boolean running;

  public Computer()
  {
    veryhighTimer = new Timer();
    highTimer = new Timer();
    lowTimer = new Timer();

    veryhigh = new ArrayList<Computation>(20);
    high = new ArrayList<Computation>(20);
    low = new ArrayList<Computation>(20);
  }

  public void start()
  {
    if( veryhigh.size() > 0 )
      veryhighTimer.schedule(new ComputationTask(veryhigh), 1, 500);
    if( high.size() > 0 )
      highTimer.schedule(new ComputationTask(high), 5, 1000);
    if( low.size() > 0 )
      lowTimer.schedule(new ComputationTask(low), 5, 10000);
    running = true;
  }

  public void stop()
  {
    veryhighTimer.cancel();
    highTimer.cancel();
    lowTimer.cancel();
    running = false;
  }

  public void pause()
  {
    stop();

    veryhighTimer = new Timer();
    highTimer = new Timer();
    lowTimer = new Timer();
  }

  public void addComputationVeryHigh(Computation c)
  {
    if( running && veryhigh.size() > 0 )
      highTimer.schedule(new ComputationTask(veryhigh), 1, 500);
    veryhigh.add(c);
  }

  public void addComputationHigh(Computation c)
  {
    if( running && high.size() > 0 )
      highTimer.schedule(new ComputationTask(high), 5, 1000);
    high.add(c);
  }

  public void addComputationLow(Computation c)
  {
    if( running && low.size() > 0 )
      lowTimer.schedule(new ComputationTask(low), 5, 10000);
    low.add(c);
  }

  private static class ComputationTask extends TimerTask {
    private final Collection<Computation> computations;

    public ComputationTask(Collection<Computation> computations)
    {
      this.computations = computations;
    }

    @Override
    public void run()
    {
      for(Computation c : computations)
      {
        c.compute();
      }
    }
  }
}
