package at.pegasos.client.values;

public abstract class ComputationValue extends Value {
  public ComputationValue(String name)
  {
    super(name);
  }

  public abstract void update(Double internal, long time);
}
