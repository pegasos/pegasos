package at.pegasos.client.sensors;

public enum ConnectionFailedReason {
  ANT_CHANNEL_NOT_AVAILABLE,
  ANT_CONNECTION_LOST,
  ANT_OTHER_FAILURE,
  SEARCH_TIMEOUT
}
