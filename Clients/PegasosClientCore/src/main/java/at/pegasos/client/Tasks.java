package at.pegasos.client;

import at.pegasos.client.util.PegasosLog;
import at.univie.mma.serverinterface.ServerInterface;

public class Tasks {
  private static final String TAG = "BackgroundTasks";

  public static class SessionRestartTask extends Thread {

    @Override public void run()
    {
      PegasosLog.d(TAG, "Restarting Session");
      ServerInterface.getInstance().restartSession();
    }
  }

  public static void restartSession()
  {
    new SessionRestartTask().start();
  }

  private static class LoginTask extends Thread {

    private final String token;
    private final boolean clubusage;
    private final Long sessionId;

    public LoginTask(String token, boolean clubusage)
    {
      this.token = token;
      this.clubusage = clubusage;
      this.sessionId = null;
    }
    public LoginTask(String token, boolean clubusage, long sessionId)
    {
      this.token = token;
      this.clubusage = clubusage;
      this.sessionId = sessionId;
    }

    @Override public void run()
    {
      PegasosLog.d(TAG, "Logging in");
      if( sessionId != null )
        ServerInterface.getInstance().login(token, clubusage, sessionId);
      else
        ServerInterface.getInstance().login(token, clubusage);
    }
  }

  /**
   * Create a login task and start it immediately
   * @param loginData loginData to use
   */
  public static void login(LoginData loginData)
  {
    new LoginTask(loginData.token, loginData.clubsession).start();
  }

  /**
   * Create a login task and start it immediately
   * @param loginData loginData to use
   * @param sessionId ID of the session which was lost
   */
  public static void login(LoginData loginData, long sessionId)
  {
    new LoginTask(loginData.token, loginData.clubsession, sessionId).start();
  }
}
