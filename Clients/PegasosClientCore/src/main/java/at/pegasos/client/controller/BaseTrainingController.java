package at.pegasos.client.controller;

import at.pegasos.client.ui.screen.Screen;

public abstract class BaseTrainingController implements ITrainingController {
  protected int controller_id;
  protected int activity;
  protected int param1;
  protected int param2;
  
  @Override
  public void setActivity(int activity_c)
  {
    this.controller_id= activity_c;
    
    activity= (activity_c / 100) % 10;
    param1= (activity_c / 10) % 10;
    param2= activity_c % 10;
  }
  
  @Override
  public void setActivity(int activity_c, String argument)
  {
    this.controller_id= activity_c;
    
    int param2_hook= Integer.parseInt(argument);
    
    param1= 0;
    param2= 0;
    
    activity= (activity_c / 100) % 10;
    param1= (activity_c / 10) % 10;
    param2= param2_hook;
  }
  
  @Override
  public int getActivity()
  {
    return activity;
  }
  
  @Override
  public int getParam1()
  {
    return param1;
  }
  
  @Override
  public int getParam2()
  {
    return param2;
  }
  
  @Override
  public Screen[] getAdditionalScreens()
  {
    return no_screens;
  }
  
  @Override
  public boolean disableSportScreens()
  {
    return false;
  }
  
  @Override
  public void onResume()
  {
    
  }
}
