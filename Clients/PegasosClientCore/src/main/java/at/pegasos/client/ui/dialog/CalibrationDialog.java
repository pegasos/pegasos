package at.pegasos.client.ui.dialog;

public interface CalibrationDialog
{
  public CharSequence getDisplayName();
  
  // public CalibrationDialog createDialog(Context ctx);
  public void show();
  
  public boolean isShowing();
}