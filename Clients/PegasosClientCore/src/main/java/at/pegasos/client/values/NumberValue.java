package at.pegasos.client.values;

public class NumberValue extends Value {
  protected Double internal;

  public NumberValue(String name)
  {
    super(name);
  }

  public NumberValue(String name, int value)
  {
    super(name);
    internal = (double) value;
  }

  public NumberValue(String name, double value)
  {
    super(name);
    internal = value;
  }

  @Override
  public Integer getIntValue()
  {
    if( internal != null )
      return internal.intValue();
    else
      return null;
  }

  @Override
  public void setIntValue(Integer value)
  {
    if( value != null )
      internal = value.doubleValue();
    else
      internal = null;
  }

  @Override
  public Double getDoubleValue()
  {
    return internal;
  }

  @Override
  public void setDoubleValue(Double value)
  {
    internal = value;
  }

  @Override
  public String getStringValue()
  {
    return internal.toString();
  }

  @Override
  public void setStringValue(String value)
  {
    if( value != null )
      internal = Double.parseDouble(value);
    else
      internal = null;
  }
}
