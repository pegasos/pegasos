package at.pegasos.client.values;

import java.util.*;

import at.pegasos.concurrent.*;

/**
 * This is our very own garbage collector. Once an activity is started it needs to run and remove
 * some of the ListValue entries in order to keep the system running
 */
public class GarbageCollector {
  private static final int MINUTE = 60 * 1000;
  /**
   * Time before the GC will first start checking
   */
  private static final long FIRST = 15 * MINUTE + MINUTE;
  /**
   * How often the GC should check start deleting
   */
  private static final long PERIOD = 15 * MINUTE;
  /**
   * how old items in the lists can be
   */
  private static final long TIME = 15 * MINUTE;

  private static GarbageCollector instance;

  private final Timer timer;
  private Set<String> lists;

  private GarbageCollector()
  {
    timer = new Timer();
  }

  public static synchronized void start()
  {
    if( instance == null )
    {
      instance = new GarbageCollector();
      instance.addLists();
    }
    else
    {
      instance.refresh();
    }
  }

  public static void stop()
  {
    if( instance != null )
    {
      instance.timer.cancel();
      instance = null;
    }
  }

  private void addLists()
  {
    ValueStore store = ValueStore.getInstance();
    Set<String> lists_to_add = store.getLists();
    lists = new HashSet<String>(lists_to_add.size());
    for(String name : lists_to_add)
    {
      addList(name, store.getList(name));
    }
  }

  private void refresh()
  {
    ValueStore store = ValueStore.getInstance();
    Set<String> lists_to_add = store.getLists();
    for(String name : lists_to_add)
    {
      if( !lists.contains(name) )
      {
        addList(name, store.getList(name));
      }
    }
  }

  private void addList(String name, TimedList<Double> list)
  {
    lists.add(name);
    timer.scheduleAtFixedRate(new DeleteTask(list), FIRST, PERIOD);
  }

  private static class DeleteTask extends TimerTask {
    private final TimedList<Double> list;

    public DeleteTask(TimedList<Double> list)
    {
      super();
      this.list = list;
    }

    @Override
    public void run()
    {
      list.removeOlder(System.currentTimeMillis() - TIME);
    }

  }
}
