package at.pegasos.client.values;

public class Value {
  private final String name;
  protected Object internal;

  public Value(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public Integer getIntValue()
  {
    return (Integer) internal;
  }

  public void setIntValue(Integer value)
  {
    internal = value;
  }

  public Double getDoubleValue()
  {
    return (Double) internal;
  }

  public void setDoubleValue(Double value)
  {
    internal = value;
  }

  public String getStringValue()
  {
    return (String) internal;
  }

  public void setStringValue(String value)
  {
    internal = value;
  }
}
