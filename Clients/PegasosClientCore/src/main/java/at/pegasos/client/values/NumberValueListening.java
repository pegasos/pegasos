package at.pegasos.client.values;

import at.pegasos.concurrent.*;

public class NumberValueListening extends NumberValue {

  public TimedList<Double> values;

  public NumberValueListening(String name)
  {
    super(name);
    values = new TFineGrainedList<Double>();
  }

  public NumberValueListening(String name, int value)
  {
    super(name, value);
    internal = (double) value;
    values = new TFineGrainedList<Double>();
    values.add(System.currentTimeMillis(), Double.valueOf(value));
  }

  public NumberValueListening(String name, double value)
  {
    super(name, value);
    internal = value;
    values = new TFineGrainedList<Double>();
    values.add(System.currentTimeMillis(), value);
  }

  public TimedList<Double> getValueList()
  {
    return values;
  }

  /**
   * Set the current value but do not add it to the list
   *
   * @param value
   */
  public void setNoAdd(Integer value)
  {
    internal = value.doubleValue();
  }

  /**
   * Set the current value but do not add it to the list
   *
   * @param value
   */
  public void setNoAdd(Double value)
  {
    internal = value;
  }

  @Override
  public void setIntValue(Integer value)
  {
    if( value != null )
    {
      internal = value.doubleValue();
      values.add(System.currentTimeMillis(), value.doubleValue());
    }
    else
    {
      internal = null;
      values.add(System.currentTimeMillis(), null);
    }
  }

  /**
   * Set the current value with a specified timestamp
   *
   * @param value
   * @param ts
   */
  public void setIntValue(Integer value, long ts)
  {
    if( value != null )
    {
      internal = value.doubleValue();
      values.add(ts, value.doubleValue());
    }
    else
    {
      internal = null;
      values.add(ts, null);
    }
  }

  @Override
  public void setDoubleValue(Double value)
  {
    internal = value;
    values.add(System.currentTimeMillis(), value);
  }

  /**
   * Set the current value with a specified timestamp
   *
   * @param value
   * @param ts
   */
  public void setDoubleValue(Double value, long ts)
  {
    internal = value;
    values.add(ts, value);
  }
}
