package at.pegasos.client.sensors;

import java.util.*;
import java.util.Map.Entry;

import at.pegasos.client.ui.ParameterQuestion;

/**
 * Singleton class to handle callbacks from any Sensor instances to any registered listener.
 */
public class SensorCallback {
  private static SensorCallback instance;
  private static List<SensorCallbackListener> listeners;
  
  private SensorCallback()
  {
    listeners= new ArrayList<SensorCallbackListener>();
  }
  
  public static SensorCallback getInstance()
  {
    if( instance == null )
    {
      instance= new SensorCallback();
    }
    return instance;
  }
  
  public void addListener(SensorCallbackListener listener)
  {
    listeners.add(listener);
  }
  
  public void removeListener(SensorCallbackListener listener)
  {
    listeners.remove(listener);
  }
  
  /**
   * Notify all listeners of a detected sensor.
   *
   * @param sensor
   *          The detected sensor.
   * @param types
   *          The supported types of the detected sensor.
   * @param id
   *          The ID of the detected sensor.
   * @param questions
   *          A list of requested parameters.
   * @param params
   *          List of params to be added to the detected sensor
   */
	public void notifyListeners(Sensor sensor, String[] types, String id, final List<ParameterQuestion> questions,
      List<Entry<String, String>> params)
  {
    for(SensorCallbackListener listener : listeners)
    {
      listener.onSensorDetected(sensor, types, id, questions, params);
    }
  }
}
