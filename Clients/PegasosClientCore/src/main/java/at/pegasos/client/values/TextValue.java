package at.pegasos.client.values;

public class TextValue extends Value {

  protected String internal = "";

  public TextValue(String name)
  {
    super(name);
  }

  public TextValue(String name, String value)
  {
    super(name);
    internal = value;
  }

  @Override
  public Integer getIntValue()
  {
    return Integer.parseInt(internal);
  }

  @Override
  public void setIntValue(Integer value)
  {
    internal = value.toString();
  }

  @Override
  public Double getDoubleValue()
  {
    return Double.parseDouble(internal);
  }

  @Override
  public void setDoubleValue(Double value)
  {
    internal = value.toString();
  }

  @Override
  public String getStringValue()
  {
    return internal;
  }

  @Override
  public void setStringValue(String value)
  {
    internal = value;
  }
}
