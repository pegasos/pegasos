package at.pegasos.client.values;

public class DoubleValue extends Value {
  protected Double internal;

  public DoubleValue(String name)
  {
    super(name);
  }

  public Integer getIntValue()
  {
    if( internal != null )
      return internal.intValue();
    else
      return null;
  }

  public void setIntValue(Integer value)
  {
    if( value != null )
      internal = value.doubleValue();
    else
      internal = null;
  }

  public Double getDoubleValue()
  {
    return internal;
  }

  public void setDoubleValue(Double value)
  {
    internal = value;
  }

  public String getStringValue()
  {
    return internal.toString();
  }

  public void setStringValue(String value)
  {
    if( value != null )
      internal = Double.parseDouble(value);
    else
      internal = null;
  }
}
