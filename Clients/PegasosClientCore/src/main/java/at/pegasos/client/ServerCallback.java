package at.pegasos.client;

import at.pegasos.client.util.*;
import at.pegasos.serverinterface.response.*;
import at.pegasos.serverinterface.response.ServerResponse;
import at.univie.mma.*;
import at.univie.mma.serverinterface.*;
import at.univie.mma.serverinterface.response.*;
import at.pegasos.serverinterface.response.*;

public class ServerCallback implements ResponseCallback {
  private final MainActivity mainActivity;
  int session_required_counts = 0;
  boolean restart_required = false;
  private final MessageListeners listeners;
  private boolean disconnect_reported = false;
  private int login_required_counts = 0;

  public ServerCallback(MainActivity mainActivity)
  {
    this.mainActivity = mainActivity;
    this.listeners = new MessageListeners();
  }

  @Override
  public void messageRecieved(ServerResponse r)
  {
    if( r != null )
    {
      PegasosLog.d("Message recieved", (r.getRaw() != null ? r.getRaw() : "null"));

      if( listeners.messageReceived(r) )
      {
        PegasosLog.d("Message recieved", "Consumed by listener");
        return;
      }

      if( r instanceof LoginMessage )
      {
        if( r.getResultCode() == ServerResponse.RESULT_OK )
        {
          this.mainActivity.setLoggedIn(true);
          login_required_counts = 0;

          mainActivity.onLoginMessageReceived((LoginMessage) r);

          if( PegasosClient.getInstance().isTrainingUploadRequired() && restart_required )
          {
            ServerInterface.getInstance().restartSession();
          }
        }
        else
        {
          mainActivity.onLoginMessageReceived((LoginMessage) r);
        }
      }
      else if( r instanceof ActivityStoppedMessage )
      {
        this.mainActivity.showToast("Activity stopped (from main)");
        if( ServerInterface.getInstance().queueSize() < 3 ) // TODO: check whether this Threshold is ok
        {
          PegasosClient.getInstance().setTrainingUploadRequired(false);
          this.mainActivity.showToast("Finished uploading training");
        }
      }
      else if( r instanceof ServerDisconnectedMessage )
      {
        // TODO: handle this
        // this means checking whether login is in progress or if have been logged in before (if
        // true auto-relogin + timeout)

        // app can be null when the app is just starting.
        if( PegasosClient.getInstance() != null && PegasosClient.getInstance().isTrainingUploadRequired() )
        {
          if( disconnect_reported )
          {
            // showToast("Server disconnected");
          }
          else
          {
            this.mainActivity.showToast("Server disconnected. Wiederverbinden");
            disconnect_reported = true;
            ServerInterface.getInstance().connect();
          }
        }
      }
      else if( r instanceof ServerReconnected )
      {
        disconnect_reported = false;

        if( this.mainActivity != null )
        {
          this.mainActivity.showToast("Serververbindung wieder hergestellt");

          LoginData logindata = LoginData.getLoginData();
          Tasks.login(logindata);
        }

        if( PegasosClient.getInstance().isTrainingUploadRequired() )
          restart_required = true;
      }
      else if( r instanceof HeartBeatMessage )
      {
        final HeartBeatMessage m = (HeartBeatMessage) r;
        if( Config.SHOW_NOTIFICATION_HEARTBEAT )
          this.mainActivity.showToast("Heartbeat: " + m.getMessage());
      }
      else if( r instanceof ControlCommand )
      {
        final ControlCommand c = (ControlCommand) r;

        if( c.getType() == ControlCommand.TYPE_LOGIN_REQUIRED )
        {
          this.mainActivity.setLoggedIn(false);
          if( login_required_counts == 0 )
          {
            PegasosLog.d("ServerCallback", "Sending a new login message");

            LoginData logindata = LoginData.getLoginData();
            Tasks.login(logindata);
          }
          login_required_counts++;
        }
        else if( c.getType() == ControlCommand.TYPE_SESSION_REQUIRED )
        {
          // TODO: check whether it is necessary to restart session when this happens
          session_required_counts++;
          if( session_required_counts > 2 )
          {
            this.mainActivity.showToast("Mehr als zwei Mal session required. Sollte nicht sein. Bitte Fehler melden");
          }
        }
        else
        {
          this.mainActivity.showToast("Kontrollkommando: " + c.getMessage());
        }
      }
      else if( r instanceof RestartSessionMessage )
      {
        if( restart_required )
        {
          restart_required = false;
          ServerInterface.getInstance().startSending();
        }
        session_required_counts = 0;
      }
      else
      {
        PegasosLog.d("MainActivity", "Message unhandled: " + r);
      }
    }
    else
    {
      PegasosLog.e("Message recieved", "empty");
    }
  }

  public void addMessageListener(MessageListeners.IMessageListener listener)
  {
    this.listeners.addListener(listener);
  }

  public void removeMessageListener(MessageListeners.IMessageListener listener)
  {
    this.listeners.removeListener(listener);
  }
}
