package at.pegasos.client.sports;

import at.pegasos.client.ui.screen.*;
import at.pegasos.client.ui.TrainingActivity;

public abstract class Sport {
  protected final static Screen[] no_screens = new Screen[0];

  public abstract String[] getRequiredSensors();

  public abstract String[] getOptionalSensors();

  public abstract void setupAdditionalScreens(TrainingActivity activity);

  public abstract Screen[] getAdditionalScreens();

  public String[] getAllSensorsNames()
  {
    int countOfReqSen = getRequiredSensors().length;
    int countOfOptSen = getOptionalSensors().length;

    String[] allSensors = new String[countOfReqSen + countOfOptSen];

    System.arraycopy(getRequiredSensors(), 0, allSensors, 0, countOfReqSen);
    System.arraycopy(getOptionalSensors(), 0, allSensors, countOfReqSen, countOfOptSen);

    return allSensors;
  }
}
