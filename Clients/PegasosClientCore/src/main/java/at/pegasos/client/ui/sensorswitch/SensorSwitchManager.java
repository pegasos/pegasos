package at.pegasos.client.ui.sensorswitch;


import at.pegasos.client.Preferences;
import at.pegasos.client.util.PreferenceManager;
import at.univie.mma.Config;
import at.pegasos.client.ui.PegasosClientView;

public abstract class SensorSwitchManager {
  
  public interface SensorSwitchManagerCallback
  {
    public void setBleEnabled(boolean status);
    public void setAntEnabled(boolean status);
  }
  
  SensorSwitchManagerCallback callback;

  private boolean ant_enabled;
  private boolean ble_enabled;
  
  public void destroy()
  {
    this.callback= null;
  }
  
  public void setAntEnabled(boolean enabled)
  {
    this.ant_enabled= enabled;
  }
  
  public void setBleEnabled(boolean enabled)
  {
    this.ble_enabled= enabled;
  }
  
  // This can be done since there can be dead code due to the configuration
  @SuppressWarnings("unused")
  public void setup(PegasosClientView a, SensorSwitchManagerCallback callback)
  {
    this.callback= callback;
    
    PreferenceManager preferences= PreferenceManager.getInstance();
    
    ant_enabled= preferences.getBoolean(Preferences.ant_sensors_enabled, true);
    if( Config.HIDE_ANT_BUTTON && Config.ANT_ALWAYS_ON )
      ant_enabled= true;
    
    ble_enabled= preferences.getBoolean(Preferences.ble_sensors_enabled, true);
    if( Config.HIDE_BLE_BUTTON && Config.BLE_ALWAYS_ON )
      ble_enabled= true;
  }
  
  @SuppressWarnings("unused")
  public void update(PegasosClientView a, boolean can_ble, boolean can_ant)
  {
    // Only set BLE checked if ble is enabled and the device is able to use ble
    setSwitchBLEChecked(ble_enabled && can_ble);
    
    setSwitchAntChecked(ant_enabled && can_ant);
    
    setBleVisible(Config.HIDE_BLE_BUTTON);
    setAntVisible(Config.HIDE_ANT_BUTTON);
    
    if( Config.HIDE_ANT_BUTTON && Config.HIDE_BLE_BUTTON )
    {
      removeSwitches();
      
      if( Config.ANT_ALWAYS_ON )
        ant_enabled= true;
      if( Config.BLE_ALWAYS_ON )
        ble_enabled= true;
      
      // todo callback?
    }
    else
    {
      if( Config.HIDE_ANT_BUTTON )
      {
        if( Config.ANT_ALWAYS_ON )
          ant_enabled= true;
      }
      else
        ant_enabled= PreferenceManager.getInstance().getBoolean(Preferences.ant_sensors_enabled, true);
      
      if( Config.HIDE_BLE_BUTTON )
      {
        if( Config.BLE_ALWAYS_ON )
          ble_enabled= true;
      }
      else
        ble_enabled= PreferenceManager.getInstance().getBoolean(Preferences.ble_sensors_enabled, true);
    }
  }

  protected void OnBleSwitchToggled(boolean isChecked)
  {
    if( isChecked )
      ble_enabled= true;
    else
      ble_enabled= false;
    
    PreferenceManager.getInstance().storeBoolean(Preferences.ble_sensors_enabled, ble_enabled);
    
    if( callback != null )
      callback.setBleEnabled(ble_enabled);
  }
  
  protected void OnAntSwitchToggled(boolean isChecked)
  {
    if( isChecked )
    {
      ant_enabled= true;
    }
    else
    {
      ant_enabled= false;
    }
    
    PreferenceManager.getInstance().storeBoolean(Preferences.ant_sensors_enabled, ble_enabled);
    
    if( callback != null )
      callback.setAntEnabled(ant_enabled);
  }
  
  public boolean getAntEnabled()
  {
    return ant_enabled;
  }
  
  public boolean getBleEnabled()
  {
    return ble_enabled;
  }
  
  protected abstract void setBleVisible(boolean hideBleButton);
  
  protected abstract void setSwitchBLEChecked(boolean checked);
  
  protected abstract void setAntVisible(boolean hideAntButton);
  
  protected abstract void setSwitchAntChecked(boolean checked);
  
  protected abstract void removeSwitches();
}
