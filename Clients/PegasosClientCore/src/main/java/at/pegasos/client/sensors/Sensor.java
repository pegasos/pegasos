package at.pegasos.client.sensors;

import java.util.List;
import java.util.Map.Entry;

import at.pegasos.client.ui.ParameterQuestion;
import at.pegasos.client.ui.dialog.CalibrationDialog;
import at.pegasos.client.util.PegasosLog;

import at.pegasos.client.util.PreferenceManager;
import at.pegasos.serverinterface.ServerSensor;

public abstract class Sensor
{
  public enum State {
    STATE_OK(4), STATE_NOT_CONNECTED(0), STATE_CONNECTING(2), STATE_CONNECTED(3), STATE_SEARCHING(1);

    private int intValue;

    private State(int value)
    {
      this.intValue= value;
    }

    public boolean greaterThan(State other)
    {
      return this.intValue > other.intValue;
    }

    public boolean greaterequalThan(State other)
    {
      return this.intValue >= other.intValue;
    }
  };

  public static class Status {
    public State state;
    public int number;
    public String text= "";

    @Override
    public boolean equals(Object o)
    {
      if( !(o instanceof Status) )
        return false;

      Status s2= (Status) o;
      if( s2.state != state )
        return false;
      if( s2.number != number )
        return false;
      if( !s2.text.equals(text) )
        return false;

      return true;
    }

    public Status copy()
    {
      Status ret= new Status();

      ret.state= this.state;
      ret.number= this.number;
      ret.text= this.text;

      return ret;
    }

    public String toString()
    {
      return state + " " + number + " " + text;
    }
  };

	protected Status status;

	public Status getStatus()
	{
		return status;
	}

	/**
	 * Startup the sensor and establish the first connection attempt
	 */
	public abstract void initSensor();

	/**
	 * Start data acquisition
	 */
	public abstract void startSensor();

	/**
	 * Stop the sensor ie close all open channels for this sensor
	 */
	public abstract void stopSensor();

	/**
	 * This method is used to rest the metrics of the sensor. For example it
	 * resets traveled distance to 0.
	 */
	public abstract void resetMetrics();

	public abstract ServerSensor getServerSensor();

	protected SensorParam[] params;
	protected int param_idx;

	protected Object ctx;

	protected boolean pairing = false;

	protected String LOG_TAG;

  public Sensor(Object ctx)
  {
    this.ctx= ctx;
    status= new Status();
    status.state= Sensor.State.STATE_NOT_CONNECTED;
    status.number= 0;
    status.text= "";
  }

  /**
   * Callback method performed when a new connect is performed. This method
   * should not be invoked directly. Its invoked when a connect has failed or
   * when the first connection is established. The method should load the
   * current SensorParam based on param_idx
   */
  abstract protected void connect();

  public void setParams(SensorParam[] params)
  {
    PegasosLog.d(LOG_TAG, "setting Sensorparams " + params + " " + (params != null ? params.length : 0));
    this.params= params;
    if( params != null )
    {
      PreferenceManager preferences= PreferenceManager.getInstance();
      param_idx= preferences.getInt(this.getClass().getCanonicalName() + "_last_connected", 0);
      if( param_idx > params.length || param_idx < 0 )
        param_idx= 0;
    }
    else
      param_idx= 0;
  }

  /**
   * Load stored sensor configuration
   */
  public void loadSensorConfig()
  {
    // Try to load the configuration from file
    this.params= SensorConfigService.loadParams(getClass().getCanonicalName());
    PegasosLog.d(LOG_TAG, "loading Sensorparams " + params + " " + (params != null ? params.length : 0));
    if( params != null )
    {
      // Load last connected sensor
      PreferenceManager preferences= PreferenceManager.getInstance();
      param_idx= preferences.getInt(this.getClass().getCanonicalName() + "_last_connected", 0);

      // check whether we have a valid index
      if( param_idx > params.length || param_idx < 0 )
        param_idx= 0;
    }
    else
      param_idx= 0;
  }

  /**
   * Report a successful connect to a sensor
   */
	protected void onConnect()
	{
    PreferenceManager preferences= PreferenceManager.getInstance();
    preferences.startEdit();
    preferences.storeInt(this.getClass().getCanonicalName() + "_last_connected", param_idx);
    preferences.apply();
	}

	/**
	 * Report a failed connection attempt or a lost connection
	 * @param reason
	 */
	protected void onConnectFailed(ConnectionFailedReason reason)
	{
	  // TODO: set sensor status
		param_idx++;
		if (params != null && param_idx >= params.length)
			param_idx = 0;
		connect();
	}

	public void setPairing(boolean pairing)
	{
		this.pairing = pairing;
	}

  protected SensorParam currentSensorParam()
  {
    if( params != null )
    {
      // Check again whether we are inbounds
      if( param_idx >= params.length || param_idx < 0 )
        param_idx= 0;

      return params[param_idx];
    }
    else
      return null;
  }

  /**
   * Replaces the stored config with the current config. Use this method only when you know what you
   * are doing and when a sensor is actually loaded either by the load method or by onConnect
   */
  protected void updateConfig()
  {
    SensorConfigService.replaceSensorInstance(getClass().getCanonicalName(), param_idx, params[param_idx]);
  }

  /**
   * This method should be called when a new sensor is detected
   */
  protected void onSensorPaired(String name, String sensorid)
  {
    PegasosLog.d(LOG_TAG, "onSensorPaired no questions");
    onSensorPaired(name, sensorid, null, null);
  }

  /**
   * This method should be called when a new sensor is detected
   */
  protected void onSensorPairedQuestions(String name, String sensorid, final List<ParameterQuestion> questions)
  {
    onSensorPaired(name, sensorid, questions, null);
  }

  /**
   * This method should be called when a new sensor is detected
   */
  protected void onSensorPairedParams(String name, String sensorid, List<Entry<String, String>> params)
  {
    onSensorPaired(name, sensorid, null, params);
  }

  /**
   * This method should be called when a new sensor is detected
   */
  protected void onSensorPaired(String name, String sensorid, final List<ParameterQuestion> questions, List<Entry<String, String>> params)
  {
    String[] types = SensorControllerService.getInstance().getSensorType(Sensor.this);
    SensorCallback.getInstance().notifyListeners(Sensor.this, types, sensorid, questions, params);
  }

  /**
   * Return the dialogs for calibrating this sensor. If no calibration is available for this sensor it has to return null 
   * @return
   */
  public CalibrationDialog[] getCalibrationDialog()
  {
    return null;
  }
}
