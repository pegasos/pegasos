package at.pegasos.client.activitystarter;

import at.pegasos.client.ui.ITrainingLandingScreen;
import at.pegasos.data.Pair;
import at.pegasos.client.ui.MenuManager;

public class StartActivityParam implements IActivityStarterCallback {
  private final MenuManager manager;
  private final int activity_c;
  private final String sport;
  private final Runnable command;
  private final String param;

  @SuppressWarnings("unused")
  public StartActivityParam(final MenuManager manager, final int activity_c, final String sport, final int param)
  {
    this.manager= manager;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= null;
    this.param= "" + param;
  }

  public StartActivityParam(final MenuManager manager, final int activity_c, final String sport, final String param)
  {
    this.manager= manager;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= null;
    this.param= param;
  }

  @SuppressWarnings("unused")
  public StartActivityParam(final MenuManager manager, final int activity_c, final String sport, final Runnable additionalCommand,
      final int param)
  {
    this.manager= manager;
    this.activity_c= activity_c;
    this.sport= sport;
    this.command= additionalCommand;
    this.param= "" + param;
  }

  public void onActivityClicked(IActivityStarterFragment fragment)
  {
    @SuppressWarnings("unchecked")
    Pair<Object, Object>[] extras= new Pair[] {
        new Pair<Object, Object>(ITrainingLandingScreen.EXTRA_ACTIVITY, activity_c),
        new Pair<Object, Object>(ITrainingLandingScreen.EXTRA_SPORT, sport),
        new Pair<Object, Object>(ITrainingLandingScreen.EXTRA_ARGUMENTS, param)
    };

    if( command != null )
    {
      command.run();
    }

    manager.goToLanding(fragment, extras);
  }
}
