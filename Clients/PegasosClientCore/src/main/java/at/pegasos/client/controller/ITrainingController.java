package at.pegasos.client.controller;

import at.pegasos.serverinterface.response.ServerResponse;
import at.pegasos.client.ui.screen.Screen;
import at.pegasos.client.ui.TrainingActivity;

public interface ITrainingController {
  final static Screen[] no_screens= new Screen[0];
  
  /**
   * Initialise the controller's UI. This is where the extra screens can/should
   * be set up. This method can be called multiple times during a session. For
   * example, it will be called during every transition between
   * TrainingLandingScreen and TrainingActivity.
   * 
   * @param activity
   */
  public void initUI(TrainingActivity activity);
  
  public void setActivity(int activity_c);
  
  public void setActivity(int activity_c, String parameter);
  
  public Screen[] getAdditionalScreens();
  
  /**
   * This method is called once the activity is running. When the activity was
   * started offline i.e. no connection to the server exists then this method is
   * called immediately after after the `init` method with *offline=true*.
   * Otherwise, the method is called once the server responds to the activity
   * start request. Use it for starting all your client side processes.
   * 
   * @param offline
   *          true if the activity was started without a connection to the
   *          server
   */
  public void onSessionStarted(boolean offline);
  
  /**
   * This method is called when <BR/>
   * * the user hits the stop button on the TrainingLandingScreen or<br/>
   * * the server sends a stop signal or<br/>
   * * the controller itself requested this to happen
   */
  public void onSessionStopped();
  
  /**
   * 
   * @param r
   * @return true if message is consumed by the controller
   */
  boolean onMessage(ServerResponse r);
  
  public int getActivity();
  
  public int getParam1();
  
  public int getParam2();
  
  /**
   * This method can be used in order to not show any sport specific screens.
   * 
   * @return whether sport-specific screens should be disabled
   */
  public boolean disableSportScreens();
  
  /**
   * Called when the controller's onResume() is called, e.g. for updating the
   * UI.
   */
  public void onResume();
}
