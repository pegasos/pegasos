package at.pegasos.client.ui.controller;

import at.pegasos.client.*;
import at.pegasos.client.MessageListeners;
import at.pegasos.client.ServerCallback;
import at.pegasos.client.ui.*;
import at.pegasos.client.util.*;
import at.pegasos.serverinterface.response.*;
import at.univie.mma.*;
import at.univie.mma.serverinterface.*;
import com.google.gson.*;
import com.google.gson.annotations.*;

import java.io.*;
import java.net.*;
import java.security.*;
import java.util.*;

public class LoginUIController implements MessageListeners.IMessageListener {

  public static class LoginUIException extends Exception {

    public LoginUIException(Exception e) {
      super(e);
    }
  }

  private static class TokenResponse {
    @SerializedName("expires_in")
    public long expires_in;
    @SerializedName("access_token")
    public String access_token;

    @Override
    public String toString() {
      return "TokenResponse{" +
              "expires_in=" + expires_in +
              ", access_token='" + access_token + '\'' +
              '}';
    }
  }

  private String redirectURI;
  private String clientId;

  private String codeVerifier;

  // Data for login.
  private String username;
  private String token;

  private boolean club_usage = true;
  private final int language = ServerInterface.LANGUAGE_DE;

  /**
   * Indication whether the user wants the login information to be persisted
   */
  private boolean auto_login = false;

  /**
   * List of passwords for users. null if Config.LOGIN_USERLIST is false
   */
  private String[] tokenList;

  /**
   * List of the names to display on the UI. null if Config.LOGIN_USERLIST is false
   */
  private String[] userDisplayNameList;
  
  /**
   * Whether the user has pressed/clicked the login button once
   */
  private boolean login_pressed = false;

  /**
   * Reference to the UI
   */
  private final LoginActivity activity;
  private String accessToken;

  @Override
  public boolean messageRecieved(ServerResponse r)
  {
    if( r != null )
    {
      if( r instanceof LoginMessage )
      {
        PegasosLog.e(LoginActivity.TAG, "messageRecieved " + r.getResultCode() + " " + login_pressed);
        LoginMessage loginMessage = (LoginMessage) r;
        // Only react to a login message if we have requested this message
        if( loginMessage.getResultCode() == ServerResponse.RESULT_OK && login_pressed )
        {
          PegasosLog.d(LoginActivity.TAG, "Logged in");

          LoginData.createFromData((LoginMessage) r, accessToken, club_usage, language, auto_login);
          UserValues.getInstance().setFromLoginMessage(loginMessage);

          // unregister message listener before closing
          if( PegasosClient.getInstance().getMainActivity() == null )
          {
            // we have not been through main activity (i.e. we are most likely on android)
            ServerInterface.getInstance().removeResponseCallback(ServerCallback.class);
          }
          else
            PegasosClient.getInstance().getMainActivity().getResponseCallback().removeMessageListener(this);
          activity.OnLoginOk();
        }
        else
        {
          PegasosLog.e(LoginActivity.TAG, "Login failed " + loginMessage.getError());

          activity.OnLoginFailed();
        }
        return true;
      } // TODO: React to ServerDisconnected
      else
        return false;
    }
    return false;
  }

  public LoginUIController(LoginActivity activity)
  {
    this.activity= activity;
  }

  /**
   * User has clicked on the login button
   */
  public void OnLoginClicked()
  {
    if( Config.LOGIN_USERLIST )
    {
      if( !(userDisplayNameList.length > 0) )
      {
        PegasosLog.e("LoginActivity", "Tried to login. Shouldn't be possible");
      }
    }

    if( Config.CLUB_ONLY )
      club_usage = true;

    removeLogin();

    login_pressed = true;

    // When we are showing a list of logins this method is called when the user has clicked on a login
    if( Config.LOGIN_USERLIST )
    {
      if( !ServerInterface.getInstance().isConnected() )
        ServerInterface.getInstance().connect();
      ServerInterface.getInstance().login(token, club_usage);
    }
  }

  /**
   * Remove the login information from the preferences and set autologin to false
   */
  private void removeLogin()
  {
    LoginData.removePreferences();
  }

  public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException
  {
    Map<String, String> query_pairs = new LinkedHashMap<String, String>();
    String[] pairs = query.split("\\?")[1].split("&");
    for (String pair : pairs)
    {
      int idx = pair.indexOf("=");
      if( idx != - 1)
      {
        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
      }
      else
        query_pairs.put(pair, null);
    }
    return query_pairs;
  }

  /**
   * Handle the response from the oauth backend
   * @param requestUrl redirect URL from the server
   */
  public boolean codeResponse(String requestUrl)
  {
    Map<String, String> params;
    try
    {
      params = splitQuery(requestUrl);
    }
    catch (UnsupportedEncodingException e)
    {
      throw new RuntimeException(e);
    }

    if( params.containsKey("code") )
    {
      return codeResponse(params.get("code"), params.get("stayloggedin"), params.get("login_type"));
    }
    return false;
  }

  /**
   * Handle the response from the oauth backend
   * @param code code response from the server
   * @param stayloggedin value of the stayloggedin parameter or null if none has been transmitted
   * @param login_type value of the login_type parameter or null if none has been transmitted
   */
  public boolean codeResponse(String code, String stayloggedin, String login_type)
  {
    PegasosLog.d(LoginActivity.TAG, "codeResponse code: '" + code + "' stayloggedin:" +
            stayloggedin + " login_type:" + login_type);
    if( stayloggedin != null && stayloggedin.equals("on") )
    {
      auto_login = true;
    }
    if( login_type != null && login_type.equals("club") )
    {
      club_usage = true;
    }

    // Actually if the config tells us to, we ignore what we might have received from the server
    if( Config.CLUB_ONLY )
      club_usage = true;
    if( Config.REMEMBER_LOGIN_ALWAYS )
      auto_login = true;

    try
    {
      this.requestToken(code);
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    return true;
  }

  public String createLoginURL(String clientId, String redirectURI) throws LoginUIException
  {
    this.clientId = clientId;
    this.redirectURI = redirectURI;

    SecureRandom sr = new SecureRandom();
    byte[] code = new byte[32];
    sr.nextBytes(code);

    this.codeVerifier = Base64Util.encodeNoPaddingUrl(code);

    try
    {
      // generate the code_challenge
      byte[] bytes = codeVerifier.getBytes("UTF-8");
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      md.update(bytes);
      final String codeChallenge = Base64Util.encodeNoPaddingUrl(md.digest());

      StringBuilder url = new StringBuilder(Config.BACKENDBASEURL)
              .append("/oauth2/authorize?response_type=code&client_id=")
              .append(clientId)
              .append("&scope=openid&code_challenge=")
              .append(codeChallenge)
              .append("&code_challenge_method=S256&state=foo&redirect_uri=")
              .append(redirectURI);

      if( !Config.CLUB_ONLY )
        url.append("&show-logintype");
      if( !Config.REMEMBER_LOGIN_ALWAYS )
        url.append("&show-stayloggedin");

      return url.toString();
    }
    catch(UnsupportedEncodingException e)
    {
      throw new LoginUIException(e);
    }
    catch(NoSuchAlgorithmException e)
    {
      throw new LoginUIException(e);
    }
  }

  private void requestToken(String code) throws IOException {
    // Create a neat value object to hold the URL
    URL url = new URL(Config.BACKENDBASEURL + "/oauth2/token");

    // Open a connection
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

    // Set Headers
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    
    // Set Headers
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

    String tmp = "grant_type=" + URLEncoder.encode("authorization_code") + "&" +
            "client_id=" + clientId + "&" +
            "redirect_uri=" + URLEncoder.encode(redirectURI, "UTF-8") + "&" +
            "code=" + URLEncoder.encode(code, "UTF-8") + "&" +
            "code_verifier=" + URLEncoder.encode(this.codeVerifier, "UTF-8");
    PegasosLog.d(LoginActivity.TAG, tmp);
    byte[] out = tmp.getBytes("UTF-8");
    int length = out.length;

    // We can then attach our form contents to the http request with proper headers and send it.

    connection.setFixedLengthStreamingMode(length);
    connection.setDoOutput(true);
    connection.connect();
    try(OutputStream os = connection.getOutputStream()) {
      os.write(out);
    }

    if( connection.getErrorStream() != null )
    {
      PegasosLog.e(LoginActivity.TAG, "An error occurred");
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();

      int nRead;
      byte[] data = new byte[16384];

      while ((nRead = connection.getErrorStream().read(data, 0, data.length)) != -1)
      {
        buffer.write(data, 0, nRead);
      }
      PegasosLog.e(LoginActivity.TAG,buffer.toString());
      // TODO: react to error and terminate here
    }
    else
    {
      InputStream responseStream = connection.getInputStream();

      ByteArrayOutputStream buffer = new ByteArrayOutputStream();

      int nRead;
      byte[] data = new byte[16384];

      while ((nRead = responseStream.read(data, 0, data.length)) != -1)
      {
        buffer.write(data, 0, nRead);
      }
      String response = buffer.toString();

      Gson g = new Gson();
      TokenResponse map = g.fromJson(response, TokenResponse.class);
      PegasosLog.d(LoginActivity.TAG, "Response: " + response + " " + map + " " + g);

      onTokenReceived(map);
    }
  }
  
  public void setUserFromList(String username)
  {
    for(int i = 0; i < userDisplayNameList.length; i++)
    {
      if( userDisplayNameList[i].equals(username) )
      {
        username = userDisplayNameList[i];
        token = tokenList[i];
        return;
      }
    }
  }

  public void loadUserList()
  {
    Set<String> raw_list= PreferenceManager.getInstance().getStringSet(Preferences.user_list, new HashSet<String>());
    userDisplayNameList = new String[raw_list.size()];
    tokenList = new String[raw_list.size()];
    
    int i= 0;
    for(String raw : raw_list)
    {
      String[] d= raw.split("#\\;#");
      userDisplayNameList[i]= d[0];
      tokenList[i]= d[1];
      i++;
    }
  }
  
  public void storeUserList()
  {
    final PreferenceManager prefs= PreferenceManager.getInstance();
    Set<String> raw_list= new HashSet<String>();
    
    int i= 0;
    for(i= 0; i < userDisplayNameList.length; i++)
    {
      String raw= userDisplayNameList[i] + "#;#" + tokenList[i];
      raw_list.add(raw);
    }
    
    prefs.startEdit();
    prefs.storeStringSet(Preferences.user_list, raw_list);
    prefs.apply();
  }

  public void setUser(String value)
  {
    this.username= value;
  }

  public String[] getUserList()
  {
    return userDisplayNameList;
  }

  /**
   * Callback when token has been received from the oauth backend. Login to backend (as test)
   *
   * @param tokenData Token response sent from the server
   */
  private void onTokenReceived(TokenResponse tokenData)
  {
    if( auto_login )
    {
      final long expireDate = new Date().getTime() + (1000 * tokenData.expires_in);
      PreferenceManager.getInstance().startEdit();
      PreferenceManager.getInstance().storeLong("ACCESS_TOKEN_EXPIRY", expireDate);
      PreferenceManager.getInstance().apply();
    }

    PegasosLog.d(LoginActivity.TAG, "Obtained Access token " + tokenData.access_token);
    this.accessToken = tokenData.access_token;

    if( !ServerInterface.getInstance().isConnected() )
      ServerInterface.getInstance().connect();
    ServerInterface.getInstance().login(tokenData.access_token, club_usage);
  }
}
