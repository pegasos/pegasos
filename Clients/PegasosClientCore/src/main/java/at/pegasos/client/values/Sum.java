package at.pegasos.client.values;

import java.util.*;

import at.pegasos.client.util.*;
import at.pegasos.concurrent.*;

public class Sum extends TimerTask /*implements ComputedValueList*/ {
  private final static String LOG_TAG= "Sum";

  public static Sum instance;

  private final TimedList<Double> list;

  protected Value outval;

  private double sec_past;

  ValueStore.TimedDoubleValue[] buffer;

  /**
   * timestamp of the youngest/oldest event which has been displayed already
   */
  private long lastEvent= Long.MIN_VALUE;

  private double sum;

  private final long MAX_DIST;

  // @Override
  public static Sum schedule(Timer timer, TimedList<Double> list, String outname, int sec)
  {
    Sum ret= new Sum(list, outname, sec * 1000);
    timer.scheduleAtFixedRate(ret, sec * 1000, sec * 1000);
    return ret;
  }

  public Sum(TimedList<Double> list, String outname, double sec_past)
  {
    this.list= list;
    this.outval= ValueStore.getInstance().getCreateNumberValue(outname);
    this.buffer= new ValueStore.TimedDoubleValue[20 * (int) sec_past];
    this.sec_past= sec_past;
    MAX_DIST= 2000;
  }

  public void reset()
  {
    lastEvent= System.currentTimeMillis();
    sum= 0;
    PegasosLog.d(LOG_TAG, "Reset. lastEvent: " + lastEvent);
  }

  protected void compute()
  {
    // +1 in order to avoid adding the same value twice
    int elements= ValueStore.getInstance().fetchTimedDoubleListValues(list, lastEvent+1, buffer);

    // add all new elements to the sum
    for(int i= elements - 1; i >= 0; i--)
    {
      double dist= Math.max(Math.min(buffer[i].timestamp - lastEvent, MAX_DIST), 0);
      sum+= ((Double) buffer[i].value) * dist / 1000.0 / 1000.0;

      lastEvent= buffer[i].timestamp;
    }
  }
  
  @Override
  public void run()
  {
    compute();
    outval.setDoubleValue(sum);
  }
}
