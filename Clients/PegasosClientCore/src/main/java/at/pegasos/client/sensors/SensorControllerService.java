package at.pegasos.client.sensors;

import at.pegasos.client.sensors.Sensor.*;
import at.pegasos.client.sports.*;
import at.pegasos.client.ui.*;
import at.pegasos.client.util.*;
import at.pegasos.util.*;
import at.univie.mma.*;
import at.univie.mma.sensors.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.Map.*;

public class SensorControllerService {
  private final static Status KilledStatus;
  private final static Set<String> first_class_sensors;
  private static SensorControllerService inst;

  static
  {
    KilledStatus = new Status();
    KilledStatus.state = State.STATE_NOT_CONNECTED;
    KilledStatus.number = 0;
    KilledStatus.text = "";
  }

  static
  {
    first_class_sensors = new HashSet<String>();
    first_class_sensors.add(SensorTypeNames.HeartRate);
    first_class_sensors.add(SensorTypeNames.FootPod);
    first_class_sensors.add(SensorTypeNames.GPS);
  }

  private final boolean using_hr;
  private final boolean using_fp;
  private final boolean using_gps;
  /**
   * All types used (names as defined in SensorTypeNames)
   */
  private final String[] using;
  /**
   * Whether the instance should start in pairing mode
   */
  private final boolean pairing;
  private final Map<String, Boolean> avail;
  private final PegasosClientView starter;
  List<Class<ANTSensor>> classes_ant_sensors = new ArrayList<Class<ANTSensor>>();
  List<Class<Sensor>> classes_ble_sensors = new ArrayList<Class<Sensor>>();
  List<Class<Sensor>> classes_other_sensors = new ArrayList<Class<Sensor>>();
  List<Class<Sensor>> classes_hr_sensors = new ArrayList<Class<Sensor>>();
  List<Class<Sensor>> classes_footpod_sensors = new ArrayList<Class<Sensor>>();
  List<Class<Sensor>> classes_gps_sensors = new ArrayList<Class<Sensor>>();
  boolean required_hr = false;
  boolean required_fp = false;
  boolean required_gps = false;
  /**
   * Contains the SensorTypeNames of all sensors which are required and not first-class sensors.
   */
  List<String> required;
  boolean sens_changed = true;
  boolean all_required_sensors_ok = false;
  private Map<Class<Sensor>, Set<String>> classSensorTypes;
  /**
   * All Ant+ sensors which are started/available for starting
   */
  private ANTSensor[] sensors_ant;
  private Sensor[] sensors_ble;
  private Sensor[] sensors_other;
  private List<Sensor> sensors_hr;
  private List<Sensor> sensors_fp;
  private List<Sensor> sensors_gps;
  /**
   * key: SensorTypeName, value: list of sensor instances for this type
   */
  private Map<String, List<Sensor>> sensors;
  private boolean avail_gps = false;
  private boolean avail_hr = false;
  private boolean avail_fp = false;
  private Sensor.Status sensor_state_gps;
  private Sensor.Status sensor_state_hr;
  private Sensor.Status sensor_state_fp;
  private Map<String, Sensor.Status> sensor_state;
  private Timer checker;
  private boolean ant_enabled;
  private boolean ant_sensors_started;
  private boolean ble_enabled;
  private boolean ble_sensors_started;
  private boolean other_sensors_started;
  private SensorControllerService(PegasosClientView starter, Map<String, Boolean> sensor_types_used, String[] required_sensors,
      boolean pairing)
  {
    this.starter = starter;
    this.pairing = pairing;

    // Initialising usage of first-class sensors
    if( sensor_types_used.containsKey(SensorTypeNames.HeartRate) && sensor_types_used.get(SensorTypeNames.HeartRate) )
      using_hr = true;
    else
      using_hr = false;
    if( sensor_types_used.containsKey(SensorTypeNames.FootPod) && sensor_types_used.get(SensorTypeNames.FootPod) )
      using_fp = true;
    else
      using_fp = false;
    if( sensor_types_used.containsKey(SensorTypeNames.GPS) && sensor_types_used.get(SensorTypeNames.GPS) )
      using_gps = true;
    else
      using_gps = false;

    // Achtung: wenn hier unten etwas geaendert wird. In beiden zeilen aendern
    int c = 0;
    for(Entry<String, Boolean> e : sensor_types_used.entrySet())
    {
      if( e.getValue() && !first_class_sensors.contains(e.getKey()) )
      {
        c++;
      }
    }

    using = new String[c];
    avail = new HashMap<String, Boolean>();

    c = 0;
    for(Entry<String, Boolean> e : sensor_types_used.entrySet())
    {
      if( e.getValue() && !first_class_sensors.contains(e.getKey()) )
      {
        using[c++] = e.getKey();
        avail.put(e.getKey(), false);
      }
    }
    // ENDE

    required = new ArrayList<String>(required_sensors.length);
    for(String sensor : required_sensors)
    {
      // First class sensors
      if( sensor.equals(SensorTypeNames.HeartRate) )
        required_hr = true;
      else if( sensor.equals(SensorTypeNames.FootPod) )
        required_fp = true;
      else if( sensor.equals(SensorTypeNames.GPS) )
        required_gps = true;
      else // Non first-class sensors
        required.add(sensor);
    }

    ant_enabled = false;
    ant_sensors_started = false;
    ble_enabled = false;
    ble_sensors_started = false;
    other_sensors_started = false;
  }

  public static SensorControllerService getInstance()
  {
    return inst;
  }

  @SuppressWarnings("unchecked") public static SensorControllerService createService(PegasosClientView starter, Sport sport,
      boolean pairing)
  {
    assert (inst == null);

    Map<String, Boolean> sensor_types_used = new HashMap<String, Boolean>();
    List<Class<ANTSensor>> classes_ant_sensors = new ArrayList<Class<ANTSensor>>();
    List<Class<Sensor>> classes_ble_sensors = new ArrayList<Class<Sensor>>();
    List<Class<Sensor>> classes_other_sensors = new ArrayList<Class<Sensor>>();
    List<Class<Sensor>> classes_hr_sensors = new ArrayList<Class<Sensor>>();
    List<Class<Sensor>> classes_footpod_sensors = new ArrayList<Class<Sensor>>();
    List<Class<Sensor>> classes_gps_sensors = new ArrayList<Class<Sensor>>();

    String[] sensors_to_add;
    if( !pairing )
    {
      sensors_to_add = new String[sport.getRequiredSensors().length + sport.getOptionalSensors().length];
      int i = 0;
      for(String sensor_type : sport.getRequiredSensors())
        sensors_to_add[i++] = sensor_type;
      for(String sensor_type : sport.getOptionalSensors())
        sensors_to_add[i++] = sensor_type;
    }
    else
      sensors_to_add = SensorTypeNames.ALL;

    Map<Class<Sensor>, Set<String>> classSensorTypes = new HashMap<>();

    // Add all types
    for(String sensor_type : sensors_to_add)
    {
      try
      {
        @SuppressWarnings("rawtypes") Class[] sensors = (Class[]) Sensors.class.getDeclaredField("Sensors_" + sensor_type).get(null);
        // Load the classes / implementations of this sensor type and add them to ant/ble/other
        for(@SuppressWarnings("rawtypes") Class sensor : sensors)
        {
          // Which type of sensor
          if( Sensors.ant_sensors.contains(sensor) )
          {
            classes_ant_sensors.add(sensor);
          }
          else if( Sensors.ble_sensors.contains(sensor) )
          {
            classes_ble_sensors.add(sensor);
          }
          else
          {
            classes_other_sensors.add(sensor);
          }
          if( !classSensorTypes.containsKey(sensor) )
            classSensorTypes.put(sensor, new HashSet<String>(2));
          classSensorTypes.get(sensor).add(sensor_type);
        }

        // Now add them to the concrete sensor category (e.g. HR)
        if( sensor_type.equals(SensorTypeNames.HeartRate) )
        {
          for(@SuppressWarnings("rawtypes") Class sensor : sensors)
            classes_hr_sensors.add(sensor);
          sensor_types_used.put(SensorTypeNames.HeartRate, true);
        }
        else if( sensor_type.equals(SensorTypeNames.FootPod) )
        {
          for(@SuppressWarnings("rawtypes") Class sensor : sensors)
            classes_footpod_sensors.add(sensor);
          sensor_types_used.put(SensorTypeNames.FootPod, true);
        }
        else if( sensor_type.equals(SensorTypeNames.GPS) )
        {
          for(@SuppressWarnings("rawtypes") Class sensor : sensors)
            classes_gps_sensors.add(sensor);
          sensor_types_used.put(SensorTypeNames.GPS, true);
        }
        /* else if( sensor_type.equals(SensorTypeNames.Accelerometer) )
        {
          classes_accelerometer_sensors.addAll(sensors);
        } */
        // TODO: add more first class sensors
        else
        {
          sensor_types_used.put(sensor_type, true);
        }
      }
      catch( NoSuchFieldException e )
      {
        // TODO Auto-generated catch block
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalAccessException e )
      {
        // TODO Auto-generated catch block
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalArgumentException e )
      {
        // TODO Auto-generated catch block
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
    }

    inst = new SensorControllerService(starter, sensor_types_used, pairing ? new String[0] : sport.getRequiredSensors(), pairing);

    inst.classes_ant_sensors = classes_ant_sensors;
    inst.classes_ble_sensors = classes_ble_sensors;
    inst.classes_other_sensors = classes_other_sensors;
    inst.classes_hr_sensors = classes_hr_sensors;
    inst.classes_footpod_sensors = classes_footpod_sensors;
    inst.classes_gps_sensors = classes_gps_sensors;
    inst.classSensorTypes = classSensorTypes;

    inst.initService();
    inst.createStartChecker();

    PegasosLog.d("SensorService required",
        Arrays.toString(inst.required.toArray()) + " " + (inst.required_hr ? "hr" : "") + " " + (inst.required_fp ? "fp" : "") + " "
            + (inst.required_gps ? "gps" : ""));
    PegasosLog.d("SensorService using",
        Arrays.toString(inst.using) + " " + (inst.using_hr ? "hr" : "") + " " + (inst.using_fp ? "fp" : "") + " " + (inst.using_gps ?
            "gps" :
            ""));

    return inst;
  }

  /**
   * Get all Classes of a type
   *
   * @param sensorname Name of sensor type (as declared in SensorTypeNames)
   * @return array of Classes
   */
  public static String[] getSensorClassNamesForType(String sensorname)
  {
    try
    {
      @SuppressWarnings("rawtypes") Class[] sensors = (Class[]) Sensors.class.getDeclaredField("Sensors_" + sensorname).get(null);
      String[] ret = new String[sensors.length];
      for(int i = 0; i < sensors.length; i++)
      {
        ret[i] = sensors[i].getCanonicalName();
      }
      return ret;
    }
    catch( Exception e )
    {
      e.printStackTrace();
      CrashReporter.handleException(e);
    }

    return null;
  }

  private void initService()
  {
    sensors_ant = new ANTSensor[classes_ant_sensors.size()];
    sensors_ble = new Sensor[classes_ble_sensors.size()];
    sensors_other = new Sensor[classes_other_sensors.size()];

    sensors_hr = new ArrayList<Sensor>(classes_hr_sensors.size());
    sensors_fp = new ArrayList<Sensor>(classes_hr_sensors.size());
    sensors_gps = new ArrayList<Sensor>(classes_gps_sensors.size());

    sensors = new HashMap<String, List<Sensor>>();

    int i = 0, j = 0, k = 0;
    for(Class<ANTSensor> c : classes_ant_sensors)
    {
      try
      {
        ANTSensor s = (ANTSensor) c.getConstructor(Object.class).newInstance(starter);
        sensors_ant[i++] = s;
        if( classes_hr_sensors.contains(c) )
          sensors_hr.add(s);
        else if( classes_footpod_sensors.contains(c) )
          sensors_fp.add(s);
        else if( classes_gps_sensors.contains(c) )
          sensors_gps.add(s);
        else
        {
          nonFirstClass(s);
        }
      }
      catch( InstantiationException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalAccessException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalArgumentException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( InvocationTargetException e )
      {
        if( e.getMessage() != null )
          PegasosLog.e("SensorService", e.getMessage());
        else
          PegasosLog.e("SensorService", "InvocationTargetException");
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( NoSuchMethodException e )
      {
        PegasosLog.e("SensorService", c.toString() + " " + e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
    }
    // Did we have a problem constructing one sensor?
    if( i != sensors_ant.length )
    {
      ANTSensor tmp[] = new ANTSensor[i];
      System.arraycopy(sensors_ant, 0, tmp, 0, i);
      sensors_ant = tmp;
    }

    for(Class<Sensor> c : classes_ble_sensors)
    {
      try
      {
        Sensor s = (Sensor) c.getConstructor(Object.class).newInstance(starter);
        sensors_ble[j++] = s;
        if( classes_hr_sensors.contains(c) )
          sensors_hr.add(s);
        else if( classes_footpod_sensors.contains(c) )
          sensors_fp.add(s);
        else if( classes_gps_sensors.contains(c) )
          sensors_gps.add(s);
        else
        {
          nonFirstClass(s);
        }
      }
      catch( InstantiationException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalAccessException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalArgumentException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( InvocationTargetException e )
      {
        if( e.getMessage() != null )
          PegasosLog.e("SensorService", e.getMessage());
        else
          PegasosLog.e("SensorService", "InvocationTargetException");
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( NoSuchMethodException e )
      {
        PegasosLog.e("SensorService", c.toString() + " " + e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
    }
    // Did we have a problem constructing one sensor?
    if( j != sensors_ble.length )
    {
      Sensor[] tmp = new Sensor[j];
      System.arraycopy(sensors_ble, 0, tmp, 0, j);
      sensors_ble = tmp;
    }

    for(Class<Sensor> c : classes_other_sensors)
    {
      try
      {
        Sensor s = (Sensor) c.getConstructor(Object.class).newInstance(starter);
        sensors_other[k++] = s;
        if( classes_hr_sensors.contains(c) )
          sensors_hr.add(s);
        else if( classes_footpod_sensors.contains(c) )
          sensors_fp.add(s);
        else if( classes_gps_sensors.contains(c) )
          sensors_gps.add(s);
        else
        {
          nonFirstClass(s);
        }
      }
      catch( InstantiationException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalAccessException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( IllegalArgumentException e )
      {
        PegasosLog.e("SensorService", e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( InvocationTargetException e )
      {
        if( e.getMessage() != null )
          PegasosLog.e("SensorService", e.getMessage());
        else
          PegasosLog.e("SensorService", "InvocationTargetException");
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
      catch( NoSuchMethodException e )
      {
        PegasosLog.e("SensorService", c.toString() + " " + e.getMessage());
        e.printStackTrace();
        CrashReporter.handleException(e);
      }
    }
    // Did we have a problem constructing one sensor?
    if( k != sensors_other.length )
    {
      Sensor[] tmp = new Sensor[k];
      System.arraycopy(sensors_other, 0, tmp, 0, k);
      sensors_other = tmp;
    }
  }

  private void nonFirstClass(Sensor s)
  {
    Set<String> types = classSensorTypes.get(s.getClass());
    assert types != null;

    // Loop over all types
    for(String type : types)
    {
      if( first_class_sensors.contains(type) )
        continue;

      if( !sensors.containsKey(type) )
        sensors.put(type, new ArrayList<Sensor>());
      sensors.get(type).add(s);
    }
  }

  private void createStartChecker()
  {
    sensor_state_gps = KilledStatus;
    sensor_state_hr = KilledStatus;
    sensor_state_fp = KilledStatus;

    sensor_state = new HashMap<String, Sensor.Status>();
    for(String sensor : using)
    {
      sensor_state.put(sensor, KilledStatus);
    }

    checker = new Timer();
  }

  public void stopService()
  {

    stopCheckingSensorState();

    if( ant_sensors_started )
      stopAntPlusSensors();

    if( ble_sensors_started )
      stopBleSensors();

    if( other_sensors_started )
      stopOtherSensors();

    inst = null;
  }

  public boolean sensorsChanged()
  {
    PegasosLog.d("SensorService", "sensChanged: " + sens_changed);
    if( sens_changed )
    {
      sens_changed = false;
      return true;
    }
    return false;
  }

  public synchronized void setAntPlusStatus(boolean enabled)
  {
    ant_enabled = enabled;
    //TODO: if sensor search started and enabled=true --> start sensors

    if( ant_sensors_started && !ant_enabled )
      stopAntPlusSensors();
  }

  private void stopAntPlusSensors()
  {
    PegasosLog.d("SensorService", "stopAntPlusSensors");

    for(ANTSensor s : sensors_ant)
    {
      PegasosLog.d("SensorService", "Stop: " + s.getClass().getName());
      s.stopSensor();
    }
    ant_sensors_started = false;
  }

  /**
   * Start all Ant+ sensors. During the start the parameters/configuration of the sensors are/is
   * loaded from the respective files.
   */
  private void startAntPlusSensors()
  {
    PegasosLog.d("SensorService", "startAntPlusSensors pairing:" + pairing);

    for(ANTSensor s : sensors_ant)
    {
      s.loadSensorConfig();
      s.setPairing(pairing);
      s.initSensor();
    }

    ant_sensors_started = true;
  }

  private void stopOtherSensors()
  {
    PegasosLog.d("SensorService", "stopOtherSensors");

    for(Sensor s : sensors_other)
      s.stopSensor();

    other_sensors_started = false;
  }

  private void startOtherSensors()
  {
    PegasosLog.d("SensorService", "startOtherSensors");

    for(Sensor s : sensors_other)
    {
      PegasosLog.d("SensorService", "starting " + s.getClass().getCanonicalName());
      s.loadSensorConfig();
      s.setPairing(pairing);
      s.initSensor();
    }

    other_sensors_started = true;
  }

  public synchronized void setBLEStatus(boolean enabled)
  {
	  /*if( ble_enabled == false )
	    startBleSensors();
	  else
	    stopBleSensors();*/

    //TODO: if sensor search started and enabled=true --> start sensors
    ble_enabled = enabled;
  }

  private void stopBleSensors()
  {
    PegasosLog.d("SensorService", "stopBleSensors");

    for(Sensor s : sensors_ble)
    {
      PegasosLog.d("SensorService", "Stop: " + s.getClass().getName());
      s.stopSensor();
    }
    ble_sensors_started = false;
  }

  private void startBleSensors()
  {
    PegasosLog.d("SensorService", "startBleSensors " + Arrays.toString(sensors_ble));

    for(Sensor s : sensors_ble)
    {
      PegasosLog.d("SensorService", "starting " + s.getClass().getCanonicalName());
      s.loadSensorConfig();
      s.setPairing(pairing);
      s.initSensor();
    }

    ble_sensors_started = true;
  }

  public void startSensorsType(final String type)
  {
    if( type.equals(SensorTypeNames.GPS) )
    {
      for(Sensor s : sensors_gps)
      {
        // If sensor is ok. No need to do something
        if( s.getStatus().state == Sensor.State.STATE_OK )
          continue;
        else
        {
          if( ble_sensors_started )
          {
            // TODO: this loop here is really stupid ...
            //  the loop is used in order to determine whether the sensor is
            //  actually a BLE sensor. Same below for ANT and 'other'
            for(Sensor s2 : sensors_ble)
            {
              if( s2 == s )
                s.initSensor();
            }
          }
          if( ant_sensors_started )
          {
            // TODO: this loop here is really stupid ...
            for(Sensor s2 : sensors_ant)
            {
              if( s2 == s )
                s.initSensor();
            }
          }
          if( other_sensors_started )
          {
            // TODO: this loop here is really stupid ...
            for(Sensor s2 : sensors_other)
            {
              if( s2 == s )
                s.initSensor();
            }
          }
        }
      }
    }
  }

  public boolean isUsingGps()
  {
    return using_gps;
  }

  public boolean isUsingHr()
  {
    return using_hr;
  }

  public boolean isUsingFootPod()
  {
    return using_fp;
  }

  public Sensor.Status getStatusGPS()
  {
    return sensor_state_gps;
  }

  public Sensor.Status getStatusHR()
  {
    return sensor_state_hr;
  }

  public Sensor.Status getStatusFootPod()
  {
    return sensor_state_fp;
  }

  public Sensor.Status getStatus(String sensor)
  {
    return sensor_state.get(sensor);
  }

  /**
   * Returns a string array of all non first-class(!) sensors which can be used
   */
  public String[] getExtraSensorTypeNames()
  {
    return sensors.keySet().toArray(new String[required.size()]);
  }

  /**
   * Returns whether all required sensors have status ok
   *
   * @return true if all required types have a sensor with state ok
   */
  public boolean allRequiredSensorsOk()
  {
    return all_required_sensors_ok;
  }

  /**
   * Start searching for new sensors. If sensors have been started they will be stopped and
   * restarted according to the settings (ant/ble enabled)
   */
  public void startSearchingSensors()
  {
    PegasosLog.d("SensorService", "StartSearchingSensors " + ant_enabled + " " + ble_enabled);

    if( ant_sensors_started )
    {
      stopAntPlusSensors();
    }

    if( ant_enabled )
    {
      startAntPlusSensors();
    }

    if( ble_sensors_started )
    {
      stopBleSensors();
    }

    if( ble_enabled )
    {
      startBleSensors();
    }

    if( other_sensors_started )
    {
      stopOtherSensors();
    }

    startOtherSensors();
  }

  /**
   * Stop attempting to connect to sensors if they are not already connected. Nicht mehr versuchen
   * sensoren zu verbinden, sollten diese nicht verbunden sein.
   */
  public void stopSearchingNewSensors()
  {
    if( ant_sensors_started )
    {
      for(Sensor s : sensors_ant)
      {
        if( s.getStatus().state != Sensor.State.STATE_OK )
        {
          PegasosLog.d("SensorService", "Kill " + s.getClass().getCanonicalName());
          s.stopSensor();
        }
      }
    }
    if( ble_sensors_started )
    {
      for(Sensor s : sensors_ble)
      {
        if( s.getStatus().state != Sensor.State.STATE_OK )
        {
          PegasosLog.d("SensorService", "Kill " + s.getClass().getCanonicalName());
          s.stopSensor();
        }
      }
    }
    if( other_sensors_started )
    {
      for(Sensor s : sensors_other)
      {
        if( s.getStatus().state != Sensor.State.STATE_OK )
        {
          PegasosLog.d("SensorService", "Kill " + s.getClass().getCanonicalName());
          s.stopSensor();
        }
      }
    }
  }

  /**
   * Stop checking the sensors periodically for their sensor state.
   */
  public void stopCheckingSensorState()
  {
    if( checker != null )
    {
      checker.cancel();
      checker.purge();
      checker = null;
    }
  }

  public void checkSensorsIntervall(int intervall)
  {
    if( checker != null )
      checker.cancel();
    checker = new Timer();
    checker.scheduleAtFixedRate(new Checker(), 0, intervall);
  }

  /**
   * Get a handle to all OK/running sensors
   *
   * @return array of sensor instances
   */
  public final Sensor[] getSensors()
  {
    ArrayList<Sensor> sens_ret = new ArrayList<Sensor>(sensors_ant.length + sensors_ble.length + sensors_other.length);

    if( other_sensors_started )
    {
      for(Sensor s : sensors_other)
      {
        if( s.getStatus().state == Sensor.State.STATE_OK )
          sens_ret.add(s);
      }
    }
    if( ble_sensors_started )
    {
      for(Sensor s : sensors_ble)
      {
        if( s.getStatus().state == Sensor.State.STATE_OK )
          sens_ret.add(s);
      }
    }
    if( ant_sensors_started )
    {
      for(Sensor s : sensors_ant)
      {
        if( s.getStatus().state == Sensor.State.STATE_OK )
          sens_ret.add(s);
      }
    }

    return sens_ret.toArray(new Sensor[0]);
  }

  /**
   * Get an array of all sensors which are connected (or OK)
   *
   * @return array of sensor instances
   */
  public final Sensor[] getConnectedSensors()
  {
    ArrayList<Sensor> sens_ret = new ArrayList<Sensor>(sensors_ant.length + sensors_ble.length + sensors_other.length);

    if( other_sensors_started )
    {
      for(Sensor s : sensors_other)
      {
        if( s.getStatus().state == State.STATE_OK || s.getStatus().state == State.STATE_CONNECTED )
          sens_ret.add(s);
      }
    }
    if( ble_sensors_started )
    {
      for(Sensor s : sensors_ble)
      {
        if( s.getStatus().state == State.STATE_OK || s.getStatus().state == State.STATE_CONNECTED )
          sens_ret.add(s);
      }
    }
    if( ant_sensors_started )
    {
      for(Sensor s : sensors_ant)
      {
        if( s.getStatus().state == State.STATE_OK || s.getStatus().state == State.STATE_CONNECTED )
          sens_ret.add(s);
      }
    }

    return sens_ret.toArray(new Sensor[0]);
  }

  /**
   * Get a list of all sensors which have been started.
   *
   * @return array of all sensors that have been started
   */
  public final Sensor[] getUsedSensors()
  {
    ArrayList<Sensor> sens_ret = new ArrayList<Sensor>(sensors_ant.length + sensors_ble.length + sensors_other.length);
    sens_ret.addAll(Arrays.asList(sensors_other));

    sens_ret.addAll(Arrays.asList(sensors_ble));

    sens_ret.addAll(Arrays.asList(sensors_ant));

    return sens_ret.toArray(new Sensor[0]);
  }

  /**
   * Return whether the service uses BLE-Sensors
   *
   * @return true if a ble sensor is used
   */
  @SuppressWarnings("unused")
  public boolean isUsingBleSensors()
  {
    return sensors_ble.length > 0;
  }

  /**
   * Reset the metrics of all Sensors (which are running/ok)
   * (Keep in mind that what this operation actually does is up to the sensor implementation)
   */
  public void resetMetrics()
  {
    for(Sensor s : getSensors())
      s.resetMetrics();
  }

  public String[] getSensorType(Sensor s)
  {
    Set<String> types = classSensorTypes.get(s.getClass());
    return types.toArray(new String[0]);
  }

  public String getStatusTextGPS()
  {
    return getStatusGPS().text;
  }

  public String getStatusTextHR()
  {
    return getStatusHR().text;
  }

  public String getStatusTextFootPod()
  {
    return getStatusFootPod().text;
  }

  public String getStatusText(String sensor)
  {
    return getStatus(sensor).text;
  }

  private class Checker extends TimerTask {
    /**
     * Check if the status of a type of sensors has changed.
     *
     * @param last_state last/previous state of this sensor type
     * @param sensors    list of sensors of this type
     * @param sens_avail previous avail value
     * @return state
     */
    private Help checkChange(Sensor.Status last_state, List<Sensor> sensors, boolean sens_avail)
    {
      Help ret = new Help();
      Status state = KilledStatus;

      // In the case that nothing has change report the old state
      ret.state = last_state;

      if( last_state.equals(KilledStatus) )
      {
        boolean unchanged = true;
        for(Sensor sens : sensors)
        {
          state = sens.getStatus();
          if( unchanged && !state.equals(last_state) )
          {
            sens_changed = true;
            unchanged = false;
            ret.state = state.copy();
          }
        }

        if( ret.state.state == Sensor.State.STATE_OK )
        {
          ret.sens_avail = true;
        }
      }
      else
      {
        if( sens_avail )
        {
          boolean working = false;
          for(Sensor s : sensors)
          {
            state = s.getStatus();
            if( state.state == Sensor.State.STATE_OK )
            {
              working = true;
              ret.sens_avail = true;
              ret.state = state.copy();
              break;
            }
          }

          if( !working )
          {
            sens_changed = true;
            // ret.state= KilledStatus;
            // Instead of reverting to killed let's use the last seen status
            ret.state = state;
            ret.sens_avail = false;
          }
        }
        else
        {
          Status cur = KilledStatus;
          for(Sensor sens : sensors)
          {
            state = sens.getStatus();
            if( !state.equals(cur) && (state.state.greaterThan(cur.state) || state.state.greaterequalThan(cur.state) && (
                state.number > cur.number || !state.text.equals(cur.text))) )
            {
              cur = state.copy();
            }
          }

          if( !last_state.equals(cur) )
            sens_changed = true;
          ret.state = cur;
          if( cur.state == Sensor.State.STATE_OK )
            ret.sens_avail = true;
        }
      }

      return ret;
    }

    private void requiredCheck()
    {
      all_required_sensors_ok = false;

      if( required_hr && sensor_state_hr.state != Sensor.State.STATE_OK )
        return;
      if( required_fp && sensor_state_fp.state != Sensor.State.STATE_OK )
        return;
      if( required_gps && sensor_state_gps.state != Sensor.State.STATE_OK )
        return;

      for(String sensor : required)
        if( sensor_state.get(sensor).state != Sensor.State.STATE_OK )
          return;

      all_required_sensors_ok = true;
    }

    @Override public void run()
    {
      // PegasosLog.d("SensorService", "HR: " + using_hr + " " + avail_hr + " " + sensor_state_hr + " " + sensor_state_hr_text);
      if( using_hr )
      {
        Help h = checkChange(sensor_state_hr, sensors_hr, avail_hr);
        sensor_state_hr = h.state;
        avail_hr = h.sens_avail;
      }
      if( using_fp )
      {
        Help h = checkChange(sensor_state_fp, sensors_fp, avail_fp);
        sensor_state_fp = h.state;
        avail_fp = h.sens_avail;
      }
      if( using_gps )
      {
        Help h = checkChange(sensor_state_gps, sensors_gps, avail_gps);
        sensor_state_gps = h.state;
        avail_gps = h.sens_avail;
      }

      for(String sensor : using)
      {
        PegasosLog.d("SensorService", "check change for " + sensor);
        List<Sensor> sensors_h = sensors.get(sensor);
        if( sensors_h != null ) // As this should in no way changed. I assume its safe to not change any sensor_state here ...
        {
          Help h = checkChange(sensor_state.get(sensor), sensors_h, avail.get(sensor));
          sensor_state.put(sensor, h.state);
          avail.put(sensor, h.sens_avail);
        }
        PegasosLog.d("SensorService", "check change for " + sensor + " " + sensor_state.get(sensor).toString());
        // avail_gps= h.sens_avail;
      }

      requiredCheck();

      PegasosLog.d("SensorService", "g:" + avail_gps + " h:" + avail_hr + " f:" + avail_fp + " c:" + sens_changed);
    }

    private class Help {
      Status state;
      boolean sens_avail = false;
    }
  }
}
