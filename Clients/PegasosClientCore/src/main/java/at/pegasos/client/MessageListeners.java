package at.pegasos.client;

import at.pegasos.serverinterface.response.ServerResponse;

public class MessageListeners {
  public interface IMessageListener {
    public boolean messageRecieved(ServerResponse r);
  }

  private IMessageListener[] listeners;
  
  public MessageListeners()
  {
    listeners= new IMessageListener[0];
  }
  
  public void addListener(IMessageListener listener)
  {
    IMessageListener[] tmp= new IMessageListener[listeners.length + 1];
    System.arraycopy(tmp, 0, listeners, 0, listeners.length);
    tmp[listeners.length]= listener;
    listeners= tmp;
  }
  
  public void removeListener(IMessageListener listener)
  {
    IMessageListener[] tmp= new IMessageListener[listeners.length - 1];
    int pos= 0;
    for(IMessageListener list : listeners)
    {
      if( list == listener )
        break;
      pos++;
    }
    
    System.arraycopy(listeners, 0, tmp, 0, pos);
    System.arraycopy(listeners, pos + 1, tmp, pos, listeners.length - pos - 1);
    
    listeners= tmp;
  }
  
  public boolean messageReceived(ServerResponse r)
  {
    for(IMessageListener listener : listeners)
    {
      if( listener.messageRecieved(r) )
        return true;
    }
    return false;
  }
}
