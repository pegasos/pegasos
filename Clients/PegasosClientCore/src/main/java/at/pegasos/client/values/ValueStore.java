package at.pegasos.client.values;

import java.util.*;

import at.pegasos.client.sensors.*;
import at.pegasos.client.util.*;
import at.pegasos.concurrent.*;
import at.pegasos.concurrent.TimedList.*;

public class ValueStore {

  protected static ValueStore inst;
  protected final Map<String, Value> values;
  private final Map<String, TimedList<Double>> value_lists;
  private final Value hr;
  private int hr_bpm;
  private int fp_calib_int;
  private double speed_ms;
  private double speed_kmh;
  private double speed_minkm;
  private long speed_minkm_min;
  private long speed_minkm_sec;
  // private long distance_m;
  private double lat;
  private double lon;
  private short accuracy;
  private short alt;

  protected ValueStore()
  {
    values = new HashMap<String, Value>();
    value_lists = new HashMap<String, TimedList<Double>>();

    hr = getCreateNumberValue("HR", null);
    getCreateNumberValue("DISTANCE_M", null);
    getCreateNumberValue("SPEED_KMH", null);
    getCreateNumberValue("ALTITUDE_M", null);

    resetMetrics();
  }

  public static ValueStore getInstance()
  {
    if( inst == null )
      inst = new ValueStore();

    return inst;
  }

  /**
   * Set current speed / instant speed in [m/sec]
   */
  public void setCurrentSpeed(double speed, Sensor sender)
  {
    this.speed_ms = speed;
    this.speed_kmh = speed_ms * 3.6 / 1000D;
    this.speed_minkm = 60 / this.speed_kmh;
    this.speed_minkm_min = (long) speed_minkm;
    this.speed_minkm_sec = (long) ((speed_minkm - speed_minkm_min) * 60);

    getValue("SPEED_KMH").setDoubleValue(this.speed_kmh);
  }

  public double getCurrentSpeed_kmh()
  {
    return speed_kmh;
  }

  public long getCurrentSpeed_minkm_min()
  {
    return speed_minkm_min;
  }

  public long getCurrentSpeed_minkm_sec()
  {
    return speed_minkm_sec;
  }

  /**
   * Set the accuracy. current convention is to set it in [cm]
   *
   * @param accur
   * @param sender
   */
  public void setAccuracy(short accur, Sensor sender)
  {
    this.accuracy = accur;
  }

  /**
   * Get the accuracy in [cm]
   *
   * @return
   */
  public short getAccuracy()
  {
    return accuracy;
  }

  /**
   * Set the altitude. Convention is to set it in [m]
   *
   * @param alt    Altitude in metres
   * @param sender who is setting the altitude
   */
  public void setAltitude(short alt, Sensor sender)
  {
    this.alt = alt;
    getValue("ALTITUDE_M").setIntValue((int) alt);
  }

  /**
   * Get the altitude in [m]
   *
   * @return
   */
  public short getAltitude()
  {
    return this.alt;
  }

  public void setLocation(double lat, double lon, Sensor sender)
  {
    this.lat = lat;
    this.lon = lon;
  }

  public double getLatitude()
  {
    return lat;
  }

  public double getLongitude()
  {
    return lon;
  }

  public void setBearing(short bearing, Sensor gpsSensor)
  {
    // TODO Auto-generated method stub

  }

  public int getHeartRateBPM()
  {
    return hr_bpm;
  }

  public synchronized void setHeartRateBPM(int heart_beat)
  {
    this.hr_bpm = heart_beat;
    hr.setIntValue(heart_beat);
  }

  public int getFpCalibInt()
  {
    return fp_calib_int;
  }

  public void setFpCalibInt(int calib_int)
  {
    this.fp_calib_int = calib_int;
  }

  /*
   * Distance traveled in [m]
   */
  public void setDistance(long distance, Sensor sender)
  {
    // this.distance_m= distance;
    getValue("DISTANCE_M").setIntValue((int) distance);
  }

  public long getDistance()
  {
    Double distance_m = getValue("DISTANCE_M").getDoubleValue();
    if( distance_m != null )
      return distance_m.longValue();
    else
      return -1;
  }

  public void resetMetrics()
  {
    hr_bpm = -1;
    speed_kmh = speed_minkm = speed_minkm_min = speed_minkm_sec = -1;
    // distance_m= -1;
    lat = lon = -1;

    /*TODO: for(Value value : values.values())
    {
      if( value instanceof NumberValue || value instanceof IntegerValue )
        value.setIntValue(null);
      else if( value instanceof DoubleValue )
        value.setDoubleValue(null);
    }*/
    hr.setIntValue(-1);
    PegasosLog.d("ValueStore", "Internal: " + values);
    getValue("DISTANCE_M").setIntValue(null);
    getValue("SPEED_KMH").setDoubleValue(null);
    getValue("ALTITUDE_M").setDoubleValue(null);
  }

  /**
   * Get a Value from the store
   *
   * @param name Name
   * @return Value or null
   */
  public Value getValue(String name)
  {
    return values.get(name);
  }

  /**
   * Create a NumberValue. If this value already exists this value will be returned. In case the
   * existing value was not a NumberValue an UnsopportedOperationException will be thrown
   *
   * @param name    Name of the variable
   * @param initial initial value for the variable. Ignored if the value already exists
   * @return the value
   */
  public NumberValue getCreateNumberValue(String name, Double initial)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      if( val instanceof NumberValue )
        return (NumberValue) val;
      assert val != null;
      throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to NumberValue");
    }
    NumberValue val;
    if( initial != null )
    {
      val = new NumberValue(name, initial);
    }
    else
    {
      val = new NumberValue(name);
    }
    values.put(name, val);
    return val;
  }

  public NumberValue getCreateNumberValue(String name)
  {
    return getCreateNumberValue(name, null);
  }

/*
  public NumberValue getCreateNumberValue(String name, double initial)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      if( val instanceof NumberValue )
        return (NumberValue) val;
      assert val != null;
      throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to NumberValue");
    }
    NumberValue val;
    val = new NumberValue(name, initial);
    values.put(name, val);
    return val;
  }
*/

  public NumberValueListening getCreateNumberValueListening(String name)
  {
    return getCreateNumberValueListening(name, null);
  }

  public NumberValueListening getCreateNumberValueListening(String name, Double initial)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      if( val instanceof NumberValueListening )
        return (NumberValueListening) val;
      assert val != null;
      throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to NumberValueListening");
    }
    NumberValueListening val = initial != null ? new NumberValueListening(name, initial) : new NumberValueListening(name);
    values.put(name, val);
    value_lists.put(name, val.getValueList());
    return val;
  }

  public NumberValueListening convertToListingNumber(String name, boolean create)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      assert val != null;
      if( val instanceof NumberValueListening )
        return (NumberValueListening) val;
      else
      {
        NumberValueListening valList;
        if( val.getDoubleValue() != null )
          valList = new NumberValueListening(name, val.getDoubleValue());
        else
          valList = new NumberValueListening(name);
        values.put(name, valList);
        value_lists.put(name, valList.getValueList());
        return valList;
      }
    }
    else if( create )
    {
      NumberValueListening valList = new NumberValueListening(name);
      values.put(name, valList);
      value_lists.put(name, valList.getValueList());
      return valList;
    }
    else
      throw new UnsupportedOperationException("Cannot convert variable " + name + " to NumberValueListening --> Does not exist");
  }

  /**
   * Create a DoubleValue. Use this type with care!
   *
   * @param name    Name of the variable
   * @return the value
   */
  public Value getCreateValueDouble(String name)
  {
    return getCreateValueDouble(name, null);
  }

  /**
   * Create a DoubleValue. Use this type with care!
   *
   * @param name    Name of the variable
   * @param initial initial value for the variable. Ignored if the value already exists
   * @return the value
   */
  public Value getCreateValueDouble(String name, Double initial)
  {
    if( values.containsKey(name) )
      return values.get(name);
    else
    {
      Value val = new DoubleValue(name);
      val.setDoubleValue(initial);
      values.put(name, val);
      return val;
    }
  }

  public Value getCreateValueInt(String name)
  {
    return getCreateValueInt(name, null);
  }

  /**
   * Create a Integer-Value. Use this type with care!
   *
   * @param name    Name of the variable
   * @param initial initial value for the variable. Ignored if the value already exists
   * @return the value
   */
  public Value getCreateValueInt(String name, Integer initial)
  {
    if( values.containsKey(name) )
      return values.get(name);
    else
    {
      Value val = new IntegerValue(name);
      if( initial != null )
        val.setIntValue(initial);
      values.put(name, val);
      return val;
    }
  }

  public TextValue getCreateTextValue(String name)
  {
    return getCreateTextValue(name, null);
  }

  public TextValue getCreateTextValue(String name, String initial)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      assert val != null;
      if( val instanceof TextValue )
        return (TextValue) val;
      throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to TextValue");
    }
    TextValue val = new TextValue(name, initial);
    values.put(name, val);
    return val;
  }

  public TimedList<Double> getList(String name)
  {
    return value_lists.get(name);
  }

  /**
   * Fetch all values from a list which are older than from
   *
   * @param list   the list
   * @param from   time stamp for the oldest value
   * @param buffer buffer where the values will be stored
   * @return number of elements stored in the buffer
   */
  public int fetchTimedListValues(TimedList<Object> list, long from, TimedValue[] buffer)
  {
    //    TIterator<Object> it= list.iterator();
    //
    //    long[] h= new long[500];
    //    int i= 0;
    //
    //    while( it.moveForward() )
    //    {
    //      Object o= it.getItem();
    //      if( o != null )
    //      {
    //        h[i++]= (int) o;
    //        h[i++]= it.getTimestamp();
    //      }
    //    }
    //    Log.d("ASDF", Arrays.toString(h));

    TIterator<Object> lit = list.iterator();

    int idx = 0;
    boolean next;
    int length = buffer.length; //TODO: think of a speed up for this (no buffer overflow checks)

    // Log.d("VS.fetchTimedListValues", "it.t:" + lit.getTimestamp() + " " + from);
    if( lit.getTimestamp() >= from )
    {
      do
      {
        //Log.d("VS.fetchTimedListValues", "it.t:" + lit.getTimestamp() + " " + from + " item:" + lit.getItem());
        if( lit.getItem() != null )
          buffer[idx++] = new TimedValue(lit.getTimestamp(), lit.getItem());
        next = lit.moveForward();
      } while( next && lit.getTimestamp() >= from && idx < length );

      return idx;
    }
    else
      return 0;
  }

  /**
   * Fetch all values from a list which are older than from
   *
   * @param name   name of the list
   * @param from   time stamp for the oldest value
   * @param buffer buffer where the values will be stored
   * @return number of elements stored in the buffer
   */
  public int fetchTimedDoubleListValues(String name, long from, TimedDoubleValue[] buffer)
  {
    return fetchTimedDoubleListValues(value_lists.get(name), from, buffer);
  }

  /**
   * Fetch all values from a list which are older than from. Values will be
   * stored in the buffer from newest to oldest.
   *
   * @param list   the list
   * @param from   time stamp for the oldest value
   * @param buffer buffer where the values will be stored
   * @return number of elements stored in the buffer
   */
  public int fetchTimedDoubleListValues(TimedList<Double> list, long from, TimedDoubleValue[] buffer)
  {
    TIterator<Double> lit = list.iterator();

    int idx = 0;
    boolean next;
    int length = buffer.length; //TODO: think of a speed up for this (no buffer overflow checks)

    if( lit.getTimestamp() >= from )
    {
      do
      {
        if( lit.getItem() != null )
          buffer[idx++] = new TimedDoubleValue(lit.getTimestamp(), lit.getItem());
        next = lit.moveForward();
      } while( next && lit.getTimestamp() >= from && idx < length );

      return idx;
    }
    else
      return 0;
  }

  /**
   * Get the names of all Lists
   *
   * @return all names
   */
  public Set<String> getLists()
  {
    return value_lists.keySet();
  }

  public NumberValueComputation getCreateComputerNumberValue(String name)
  {
    return getCreateComputerNumberValue(name, null);
  }

  public NumberValueComputation getCreateComputerNumberValue(String name, Double initial)
  {
    if( values.containsKey(name) )
    {
      Value val = values.get(name);
      if( val instanceof NumberValueComputation )
        return (NumberValueComputation) val;
      throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to NumberValue");
    }
    NumberValueComputation val;
    if( initial != null )
    {
      val = new NumberValueComputation(name, initial);
    }
    else
    {
      val = new NumberValueComputation(name);
    }
    values.put(name, val);
    return val;
  }

  public void addValue(String name, Value value)
  {
    values.put(name, value);
  }

  public void removeValue(String name)
  {
    this.values.remove(name);
    this.value_lists.remove(name);
  }

  public static class TimedValue {
    public long timestamp;
    public Object value;

    public TimedValue(long ts, Object v)
    {
      this.timestamp = ts;
      this.value = v;
    }
  }

  public static class TimedDoubleValue extends TimedValue {
    public TimedDoubleValue(long ts, Double v)
    {
      super(ts, v);
    }
  }
}
