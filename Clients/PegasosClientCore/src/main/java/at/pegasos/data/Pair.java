package at.pegasos.data;

/**
 * Some versions of the Android API actually do not contain such a class. That's
 * why this class exists
 * 
 * @param <T1>
 * @param <T2>
 */
public class Pair<T1,T2> {

  public final T1 first;
  public final T2 second;

  public Pair(T1 key, T2 val)
  {
    this.first= key;
    this.second= val;
  }

  public T1 getKey()
  {
    return first;
  }

  public T2 getValue()
  {
    return second;
  }
}
