#!/bin/bash

source ./environment

echo "Compiling project"
ant compile

## We do not need to remove previous builds any more as we have the --clean flag now ...
# rm -rf /tmp/MMA/*
# rm -rf /tmp/Server/*

## Uncomment the following line to clean build information
##  alternatively --apps-always does the same job (except for servers)
# echo "" > lastbuilt.txt

echo "\n\nNow running Pegasos\n\n"
echo "$@"
java -cp "src${JAVA_PATH_SEP}bin${JAVA_PATH_SEP}lib/gradle-tooling-api-7.3.1.jar${JAVA_PATH_SEP}lib/org.eclipse.jgit-4.3.1.201605051710-r.jar${JAVA_PATH_SEP}lib/slf4j-api-1.7.30.jar${JAVA_PATH_SEP}lib/jcommander-1.81.jar${JAVA_PATH_SEP}lib/log4j-core-2.13.3.jar${JAVA_PATH_SEP}lib/log4j-api-2.13.3.jar${JAVA_PATH_SEP}lib/log4j-slf4j-impl-2.13.3.jar${JAVA_PATH_SEP}lib/reflections-0.9.11.jar${JAVA_PATH_SEP}lib/guava-28.1-jre.jar${JAVA_PATH_SEP}lib/javassist-3.26.0-GA.jar:lib/snakeyaml-1.25.jar:lib/jsch-0.1.53.jar:lib/ant-1.10.12.jar:lib/ant-launcher-1.10.12.jar" at.univie.mma.MMAGenerator "$@"

