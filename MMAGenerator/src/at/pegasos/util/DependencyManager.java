package at.pegasos.util;

import java.util.*;

import at.pegasos.util.DAG.ColoredNode;
import at.pegasos.util.DAG.Node;
import lombok.extern.slf4j.*;

@Slf4j
public class DependencyManager {
  
  public static class DependencyObject {
    protected String s;
    
    public DependencyObject(String s)
    {
      this.s= s;
    }
    
    public int hashCode()
    {
      return s.hashCode();
    }
    
    public String toFile()
    {
      throw new IllegalAccessError();
    }
    
    public String toString()
    {
      return getClass() + " " + s;
    }
  }
  
  public static class Cls extends DependencyObject {
    
    public Cls(String s)
    {
      super(s);
    }
    
    @Override
    public String toFile()
    {
      return "src/" + s.replaceAll("\\.", "/") + ".java";
    }
  }
  
  public static class Folder extends DependencyObject {
    
    public Folder(String s)
    {
      super(s);
    }
    
    @Override
    public String toFile()
    {
      return s;
    }
  }
  
  public static class Library extends DependencyObject {
    
    public Library(String s)
    {
      super(s);
    }
    
    @Override
    public String toFile()
    {
      return s;
    }
  }
  
  private final DAG<DependencyObject> objects;
  
  public DependencyManager()
  {
    objects= new DAG<DependencyObject>();
  }
  
  /**
   * Add a dependency of object to 'dependency'
   * 
   * @param object
   *          the object
   * @param dependency
   *          the object on which it depends
   */
  public void addDependency(DependencyObject object, DependencyObject dependency)
  {
    objects.addEdgeNC(object, dependency);
  }
  
  /**
   * Add a dependency of object to 'dependency'
   * 
   * @param object
   *          the object
   * @param dependency
   *          the object on which it depends
   */
  public void addDependency(String object, String dependency)
  {
    objects.addEdgeNC(new DependencyObject(object), new DependencyObject(dependency));
  }
  
  /**
   * Add a dependency of object to 'dependency'
   * 
   * @param object
   *          the object
   * @param dependency
   *          the object on which it depends
   */
  public void addDependencyClsO(String object, String dependency)
  {
    objects.addEdgeNC(new DependencyObject(object), new Cls(dependency));
  }
  
  public DependencyObject addObject(DependencyObject object)
  {
    // return objects.addNode(object).getID();
    Node<DependencyObject> o= objects.getNode(object);
    if( o == null )
    {
      return objects.addNode(object).getID();
    }
    else
    {
      return o.getID();
    }
  }
  
  /**
   * Add an abstract node to the tree
   * 
   * @param obj
   *          Name of the node
   * @return 
   */
  public DependencyObject addObject(String obj)
  {
    DependencyObject x= new DependencyObject(obj);
    Node<DependencyObject> o= objects.getNode(x);
    if( o == null )
    {
      return this.addObject(x);
    }
    else
    {
      return x;
    }
  }
  
  /**
   * Return a list of all object which are independent of this object
   * 
   * @param from
   * @return
   */
  public List<String> getIndependentFiles(DependencyObject from)
  {
    List<ColoredNode<DependencyObject>> list= objects.color(from, true);
    List<String> ret= new ArrayList<String>();
    
    // all nodes reachable from from are colored with 1
    for(ColoredNode<DependencyObject> obj : list)
    {
      if( obj.getColor() != 1 )
      {
        if( !obj.getID().getClass().equals(DependencyObject.class) )
        {
          ret.add(obj.getID().toFile());
        }
      }
    }
    
    return ret;
  }
  
  public List<String> getIndependentFiles(String from)
  {
    // return getIndependentFiles(objects.getNodeHash(from.hashCode()).getID());
    return getIndependentFiles(new DependencyObject(from));
  }
  
  public static void main(String[] args)
  {
    DependencyManager manager= new DependencyManager();
    
    DependencyObject sensors= new DependencyObject("sensors");
    manager.addObject(sensors);
    final String[] BUILTIN_CLASSES= {"at.univie.mma.sensors.ANT_HRSensor", "at.univie.mma.sensors.ANT_FootPodSensor",
        "at.univie.mma.sensors.GPSSensor", "at.univie.mma.sensors.AccelerationSensor"};
    for(String s : BUILTIN_CLASSES)
      manager.addObject(new Cls(s));
    
    manager.addDependency("sensors", BUILTIN_CLASSES[0]);
    manager.addDependency("sensors", BUILTIN_CLASSES[2]);
    
    // System.out.println(Arrays.toString(manager.getIndependentFiles(sensors).toArray(new String[0])));
  }

  public DependencyObject addClass(String name)
  {
    return addObject(new Cls(name));
  }
  
  public void debug()
  {
    for(DependencyObject x : objects.getNodes())
    {
      // System.out.println(x.getClass() + " " + x.s + " " + x.hashCode());
      log.debug("{} {} {}", x.getClass(),x.s, x.hashCode());
    }
  }

  public DependencyObject addLibrary(String name)
  {
    return addObject(new Library(name));
  }
  
  public DependencyObject addFolder(String path)
  {
    return addObject(new Folder(path));
  }

  /**
   * Check whether a node exists in the tree
   * 
   * @param dependency
   *          node which to be checked
   * @return true if the node exists
   */
  public boolean hasDependency(String dependency)
  {
    return objects.getNode(new DependencyObject(dependency)) != null;
  }
}
