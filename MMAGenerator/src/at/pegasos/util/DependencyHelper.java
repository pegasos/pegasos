package at.pegasos.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DependencyHelper {
  private final static Logger log= LoggerFactory.getLogger(DependencyHelper.class);
  
  /**
   * Add an implementation to the dependency tree. This method will first check whether the
   * implementation actually exists. if so it will try to infer all further dependencies.
   * 
   * @param implementation
   *          class to be added
   * @param searchpath
   *          where to search for the implementations
   * @param app
   *          dependency manager
   * @param extras
   *          extra dependencies
   * @param top
   *          Node/Depenceny under which the implementation will be added
   * @return true if a the file exists
   */
  public static boolean addImplementationAndInfer(String implementation, Path searchpath, DependencyManager app, Map<String, String> extras,
      String top)
  {
    String fn= implementation.replaceAll("\\.", "/") + ".java";
    
    Path p= searchpath.resolve("src").resolve(fn);
    
    if( !Files.exists(p) )
    {
      System.out.println(p.toString() + " does not exists");
      return false;
    }
    else
    {
      inferFromFile(app, implementation, extras, top, p);
      
      return true;
    }
  }
  
  /**
   * Add an implementation to the dependency tree. This method will first check whether the
   * implementation actually exists. if so it will try to infer all further dependencies.
   * 
   * @param implementation
   *          class to be added
   * @param searchpaths
   *          where to search for the implementations
   * @param app
   *          dependency manager
   * @param extras
   *          extra dependencies. Use them for example to add dependencies to libraries. Individual
   *          imports can be shadowed by this command. For example all imports from
   *          'com.dsi.ant.plugins.antplus' and sub packages can be used to add a dependency to
   *          'antpluginlib' by adding a key/value "com.dsi.ant.plugins.antplus"/"antpluginlib"
   * @param top
   *          Node/Depenceny under which the implementation will be added
   * @return
   */
  public static boolean addImplementationAndInfer(String implementation, List<Path> searchpaths, DependencyManager app, Map<String, String> extras,
      String top)
  {
    final String fn= implementation.replaceAll("\\.", "/") + ".java";
    
    boolean found= false;
    Path p= null;
    for(int i= 0; i < searchpaths.size(); i++)
    {
      p= searchpaths.get(i).resolve(fn);
      if( !Files.exists(p) )
      {
        log.debug(p.toString() + " does not exists");
      }
      else
      {
        found= true;
        break;
      }
    }
    
    if( found )
    {
      inferFromFile(app, implementation, extras, top, p);
      
      return true;
    }
    else
      return false;
  }
  
  private static void inferFromFile(DependencyManager app, String implementation, Map<String, String> extras, String top, Path p)
  {
    app.addClass(implementation);
    
    // package of the class
    String pkg= implementation.substring(0, implementation.lastIndexOf('.'));
    
    Map<String, String> imported= new HashMap<String, String>();
    
    try
    {
      boolean in_extends_implements= false;
      List<String> fileContent= Files.readAllLines(p);
      for(String line : fileContent)
      {
        if( !in_extends_implements )
        {
          if( line.startsWith("import") )
          {
            String importP= line.substring(7, line.length() - 1);
            boolean extra= false;
            for(String h : extras.keySet())
            {
              if( importP.startsWith(h) )
              {
                log.debug("Add dependency " + implementation + " to " + extras.get(h));
                app.addDependency(implementation, extras.get(h));
                extra= true;
              }
            }
            
            // we have not found some extra dependecy so just add it directly
            if( !extra )
            {
              if( app.hasDependency(importP) )
                app.addDependency(implementation, importP);
            }
            
            String pkgI= importP.substring(0, importP.lastIndexOf('.'));
            String clsI= importP.substring(importP.lastIndexOf('.')+1);
            imported.put(pkgI, clsI);
          }
          else if( line.contains("extends") || line.contains("implements") )
          {
            in_extends_implements= inferExtImp(in_extends_implements, line, pkg, implementation, app, imported);
          }
        }
        else
        {
          
        }
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
    
    app.addDependency(top, implementation);
  }
  
  private static boolean inferExtImp(boolean in_extends_implements, String line, String pkg, String implementation, DependencyManager app, Map<String, String> imported)
  {
    // check what comes first
    int beginE= line.indexOf("extends");
    int beginI= line.indexOf("implements");
    
    int end= 0;
    
    if( beginE > -1 )
    {
      if( beginE < beginI )
      {
        // line contains both 'implements' and 'extends'
        end= beginI;
      }
      else
      {
        // no 'implements' in this line
        if( line.contains("{") )
        {
          end= line.indexOf('{');
          in_extends_implements= false;
        }
        else
        {
          in_extends_implements= true;
          end= line.length();
        }
      }
      
      addExtImp(line, beginE, end, pkg, implementation, app, imported);
    }
    else if( !in_extends_implements )
    {
      // line does not contain 'extends' but contains 'implements'
      if( line.contains("{") )
      {
        end= line.indexOf('{');
        in_extends_implements= true;
      }
      else
      {
        in_extends_implements= true;
        end= line.length();
      }
      
      addExtImp(line, beginE, end, pkg, implementation, app, imported);
    }
    
    return in_extends_implements;
  }
  
  private static void addExtImp(String line, int begin, int end, String pkg, String implementation, DependencyManager app, Map<String, String> imported)
  {
    String ext= line.substring(begin + 8, end);
    String h[]= ext.split(",");
    
    for(String e : h)
    {
      e= e.trim();
      if( app.hasDependency(e) )
      {
        log.debug("Dependency from " + implementation + " to " + e + " added");
        app.addDependency(implementation, e);
      }
      else if( app.hasDependency(pkg + '.' + e) )
      {
        log.debug("Dependency from " + implementation + " to " + e + " added");
        app.addDependency(implementation, pkg + '.' + e);
      }
      else if( imported.containsKey(e) && app.hasDependency(imported.get(e) + '.' + e) )
      {
        String d= imported.get(e) + '.' + e;
        log.debug("Dependency from " + implementation + " to " + d + " added");
        app.addDependency(implementation, d);
      }
    }
  }
  
  /**
   * Load dependencies from a file into the dependencymanager
   * 
   * @param file file with the depencencies
   * @param app the dependency manager
   * @param extras map to be filled with `convenience` inferals
   * @throws IOException 
   */
  public static void loadDependencies(Path file, DependencyManager app, Map<String, String> extras) throws IOException
  {
    if( !Files.exists(file) )
    {
      log.debug(file + " does not exsists. Not loading dependencies");
      return;
    }
    
    Map<String, DepObj> map;
    
    log.debug("Reading dependencies from " + file);
    
    map= new HashMap<String, DepObj>();
    
    List<String> fileContent= Files.readAllLines(file);
    for(String line : fileContent)
    {
      DepObj obj= new DepObj();
      String[] parts= line.split(":");
      
      // System.out.println("'" + line + "' " + Arrays.toString(parts) + " " + parts.length);
      
      if( parts.length < 2 || line.charAt(0) == '#' )
        continue;
      
      int of= 0;
      boolean add= true;
      if( parts[0].equals("lib") )
      {
        obj.type= DepObj.Type.Library;
        of= 1;
      }
      else if( parts[0].equals("cls") )
      {
        obj.type= DepObj.Type.Class;
        of= 1;
      }
      else if( parts[0].equals("folder") )
      {
        obj.type= DepObj.Type.Folder;
        of= 1;
      }
      else if( parts[0].equals("extra") )
      {
        extras.put(parts[1], parts[2].substring(1));
        add= false;
      }
      
      if( add )
      {
        // System.out.println(Arrays.toString(parts));
        if( parts.length > of+1 )
        {
          String[] h= parts[of+1].trim().split(" ");
          ArrayList<String> deps= new ArrayList<String>(h.length);
          for(String dep : h)
          {
            // System.out.println("'" + dep + "'");
            if( dep.equals("") )
              continue;
            deps.add(dep);
          }
          obj.dependencies= deps.toArray(new String[0]);
        }
        else
          obj.dependencies= new String[0];
        
        map.put(parts[of], obj);
      }
    }
    
    for(Entry<String, DepObj> x : map.entrySet())
    {
      switch(x.getValue().type)
      {
        case Class:
          app.addClass(x.getKey());
          break;
        case Library:
          app.addLibrary(x.getKey());
          break;
        case Object:
          app.addObject(x.getKey());
          break;
        case Folder:
          app.addFolder(x.getKey());
          break;
        default:
          break;
      }
    }
    
    for(Entry<String, DepObj> x : map.entrySet())
    {
      for(String dep : x.getValue().dependencies)
      {
        // System.out.println("Add '" + x.getKey() + "' " + dep);
        log.debug("Adding dependency from " + x.getKey() + " to " + dep);
        app.addDependency(x.getKey(), dep);
      }
    }
    
    log.debug("Reading dependencies from " + file + " done");
  }
  
  static class DepObj {
    static enum Type {Object, Class, Library, Folder};
    Type type;
    String[] dependencies;
    
    public DepObj()
    {
      type= Type.Object;
    }
  }
  
  public static void main(String[] args) throws IOException
  {
    DependencyManager app= new DependencyManager();
    HashMap<String, String> extras= new HashMap<String, String>();
    loadDependencies(Paths.get("/home/martin/schmelz/MMA/MMAv3/dependencies"), app, extras);
    
    app.debug();
    
    System.out.println(extras);
  }
}
