package at.pegasos.util;

import java.util.*;
import java.util.function.*;

/**
 * simple directed Graph.
 * 
 * Might be low in performance but does the job.
 *
 * @param <T>
 *          type of node content
 */
public class DAG<T> {
  private final Map<Integer, Node<T>> nodes;
  
  public DAG()
  {
    nodes= new HashMap<Integer, Node<T>>();
  }
  
  /**
   * Add node to graph
   * 
   * @return created node
   */
  public Node<T> addNode(T id)
  {
    if( nodes.containsKey(id.hashCode()) )
    {
      return nodes.get(id.hashCode());
    }
    else
    {
      Node<T> n= new Node<T>(id);
      nodes.put(id.hashCode(), n);
      return n;
    }
  }
  
  /**
   * Add an edge from a to b without checking acyclicity constraint
   * 
   * @param a node id
   * @param b node id
   */
  public void addEdgeNC(T a, T b)
  {
    Node<T> n= nodes.get(b.hashCode());
    if( n == null )
      throw new IllegalArgumentException("could not find " + b);
    
    // Check whether there is no edge a -> b
    Node<T> na= nodes.get(a.hashCode());
    if( !na.successors.contains(n) )
    {
      // Add new edge
      na.successors.add(n);
    }
  }

  /**
   * Traverse tree and remove node from tree. This will disconnect all child nodes but leave them in the tree.
   *
   * @param root root from which search is started
   * @param node node to remove
   */
  public boolean removeNode(Node<T> root, Node<T> node)
  {
    for(Node<T> successor : root.successors)
    {
      if( successor == node )
      {
        root.successors.remove(node);
        nodes.remove(node.hashCode());
        return true;
      }
      else
      {
        // Traverse deeper
        if( removeNode(successor, node) )
        {
          // we are done
          return true;
        }
      }
    }
    return false;
  }

  public Node<T> getNodeById(int id)
  {
    return nodes.get(id);
  }

  public static class Node<T> {
    public Node(T id)
    {
      this.id= id;
      this.successors= new ArrayList<Node<T>>();
    }
    
    private final T id;
    protected List<Node<T>> successors;
    
    public final T getID()
    {
      return id;
    }
    
    /**
     * Returns all children and their children
     * @return flat list with all elements
     */
    public List<Node<T>> getAllChildren()
    {
      // System.out.println("Get children for " + this + " " + this.id);
      List<Node<T>> ret= new ArrayList<Node<T>>();
      
      for(Node<T> child : successors)
      {
        // System.out.println("Child: " + child);
        ret.add(child);
        ret.addAll(child.getAllChildren());
      }
      
      return ret;
    }
    
    /**
     * Returns all children.
     * @return a list with all children (not flattened)
     */
    public List<T> getChildren()
    {
      List<T> ret= new ArrayList<T>(successors.size());
      
      for(Node<T> child : successors)
      {
        ret.add(child.getID());
      }
      
      return ret;
    }
    
    /**
     * Returns all children
     * @return a list with all child nodes (not flattened)
     */
    public List<Node<T>> getChildNodes()
    {
      List<Node<T>> ret= new ArrayList<Node<T>>(successors.size());

      ret.addAll(successors);
      
      return ret;
    }

    /**
     * Traverse the tree in Pre-Order.
     * 
     * @param run
     *          action to be performed when a node is visited
     * @param post
     *          action to be performed when a node and all its children have been visited i.e. the
     *          processing of this node is done
     */
    public void traverse(NodeVisitor<T> run, NodeVisitor<T> post)
    {
      traverse(run, post, null);
    }

    private void traverse(NodeVisitor<T> run, NodeVisitor<T> post, Node<T> parent)
    {
      run.id= this.id;
      run.node= this;
      run.parent = parent;
      run.run();

      for(Node<T> child : successors)
      {
        child.traverse(run, post, this);
      }

      post.id= this.id;
      post.node= this;
      post.parent = parent;
      post.run();
    }
    
    /**
     * Merge all children from other into this node
     *
     * @param tree       tree into which the nodes are merged into
     * @param other      node to be merged into this
     */
    public void mergeNode(DAG<T> tree, Node<T> other, NodeMerger<T> merger)
    {
      if( merger != null )
      {
        merger.a= this;
        merger.b= other;
        merger.run();
      }

      if( this.successors.size() == 0 )
      {
        // at this point we know:
        // * in our tree no children exist for this node
        // * this also means that all of other.children can't exist

        for(Node<T> child : other.getChildNodes())
        {
          shallowCopy(child, this, tree);
        }
      }
      else
      {
        // at this point we know:
        // * this node has some children
        // * node to be merged might have some children
        // * these children might conflict with children in our node

        for(Node<T> otherchild : other.successors)
        {
          boolean haveChild = false;
          Node<T> mergern = null;
          for(Node<T> mychild : this.successors)
          {
            if( mychild.id.hashCode() == otherchild.id.hashCode() )
            {
              haveChild = true;
              mergern = mychild;
              break;
            }
          }

          if( haveChild )
          {
            mergern.mergeNode(tree, otherchild, merger);
          }
          else
          {
            if( tree.nodes.get(otherchild.id.hashCode()) != null )
              System.err.println("Node " + otherchild.id + " already in tree2a " + this.id);

            shallowCopy(otherchild, this, tree);
          }
        }
      }
    }

    private void shallowCopy(Node<T> from, Node<T> to, DAG<T> tree)
    {
      Node<T> fromCopy = new Node<T>(from.id);
      tree.nodes.put(fromCopy.id.hashCode(), fromCopy);

      // merge all the other nodes into the tree as well
      for(Node<T> child : from.getChildNodes())
      {
        shallowCopy(child, fromCopy, tree);
      }

      to.successors.add(fromCopy);
    }

    public void ordered(Comparator<Node<T>> nodeComparator)
    {
      this.successors.sort(nodeComparator);
      for(Node<T> child : successors)
      {
        child.ordered(nodeComparator);
      }
    }

    public void forEach(Consumer<? super T> consumer)
    {
      consumer.accept(this.id);
      successors.forEach((node) -> node.forEach(consumer));
    }
  }

  public static class ColoredNode<T> extends Node<T> {
    public ColoredNode(Node<T> n, int color)
    {
      super(n.id);
      this.successors= n.successors;
      this.color= color;
    }
    
    private final int color;
    
    public int getColor()
    {
      return color;
    }
  }
  
  public static abstract class NodeVisitor<T> implements Runnable {
    protected T id;
    protected Node<T> node;
    protected Node<T> parent;
  }
  
  public static abstract class NodeMerger<T> implements Runnable {
    /**
     * Node which is the output of this merge operation
     */
    public Node<T> a;
    /**
     * Node to be merged into a
     */
    public Node<T> b;
  }
  
  /**
   * Get node with specified id
   * 
   * @param id node to lookup
   * @return node or null if it does not exist
   */
  public Node<T> getNode(T id)
  {
    return nodes.get(id.hashCode());
  }
  
  /**
   * Color the tree
   * 
   * @param start
   *          node which will be used for starting color 1
   * @param reachable
   *          if true only nodes directly reachable from the starting node will get the color. If false all nodes in the same tree will get the same color
   * @return
   */
  public List<ColoredNode<T>> color(T start, boolean reachable)
  {
    int color= 1;
    List<ColoredNode<T>> ret= new ArrayList<ColoredNode<T>>(nodes.size());
    boolean first= true;
    
    Collection<Node<T>> remain= this.nodes.values();
    while( !remain.isEmpty() )
    {
      // System.out.println(remain.size());
      
      Node<T> n= null;
      if( first )
      {
        boolean found= false;
        for(Node<T> node : remain)
        {
          if( node.id.hashCode() == start.hashCode() )
          {
            found= true;
            n= node;
            break;
          }
        }
        
        if( !found )
          throw new IllegalArgumentException("could not find starting node " + start + " " + start.getClass() + " " + start.hashCode());
        
        first= false;
      }
      else
      {
        n= remain.iterator().next();
      }
      
      if( reachable )
        colorChildrenD(n, remain, ret, color++);
      else
        colorChildren(n, remain, ret, color++);
    }

/*
    System.out.println("<---");
    for(ColoredNode<T> n : ret)
    {
      System.out.println(n.color + " " + n.getID());
    }
    System.out.println("--->");
 */
    
    return ret;
  }
  
  private void colorChildrenD(Node<T> node, Collection<Node<T>> remaining, List<ColoredNode<T>> colored, int color)
  {
    List<Node<T>> children= node.getAllChildren();
    
    boolean found;
    
    ColoredNode<T> s= new ColoredNode<T>(node, color);
    colored.add(s);
    remaining.remove(node);
    for(Node<T> child : children)
    {
      found= false;
      for(ColoredNode<T> other : colored)
      {
        if( other.getID().hashCode() == child.getID().hashCode() )
        {
          found= true;
          break;
        }
      }
      
      if( !found )
      {
        s= new ColoredNode<T>(child, color);
        colored.add(s);
        remaining.remove(child);
      }
    }
  }
  
  private void colorChildren(Node<T> node, Collection<Node<T>> remaining, List<ColoredNode<T>> colored, int color)
  {
    List<Node<T>> children= node.getAllChildren();
    List<Node<T>> toColor= new ArrayList<Node<T>>(children.size());
    
    boolean found;
    
    for(Node<T> child : children)
    {
      System.out.print("Child " + child.id);
      // Is this node already colored?
      found= false;
      for(ColoredNode<T> other : colored)
      {
        if( other.getID().hashCode() == child.getID().hashCode() )
        {
          color= other.color;
          System.out.print(" won't be colored");
          found= true;
          break;
        }
      }
      
      if( !found )
      {
        toColor.add(child);
        System.out.print(" will be colored");
      }
      System.out.println();
    }
    
    ColoredNode<T> s= new ColoredNode<T>(node, color);
    colored.add(s);
    remaining.remove(node);
    for(Node<T> child : toColor)
    {
      s= new ColoredNode<T>(child, color);
      colored.add(s);
      remaining.remove(child);
    }
  }
  
  public List<T> getNodes()
  {
    List<T> ret= new ArrayList<T>(nodes.size());
    for(Node<T> n : nodes.values())
    {
      ret.add(n.id);
    }
    return ret;
  }
}