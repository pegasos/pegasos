package at.pegasos.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class LastModification {
  private static class LastMod extends SimpleFileVisitor<Path> {
    private long max= 0;
    @Override
    public FileVisitResult visitFile(Path file,
                                   BasicFileAttributes attr) {
      if( attr.lastModifiedTime().toMillis() > max )
        max= attr.lastModifiedTime().toMillis();
      
      return java.nio.file.FileVisitResult.CONTINUE;
    }
  };
  
  public static long lastModified(Path dir) throws IOException
  {
    LastMod pf = new LastMod();
    Files.walkFileTree(dir, pf);
    return pf.max;
  }
}
