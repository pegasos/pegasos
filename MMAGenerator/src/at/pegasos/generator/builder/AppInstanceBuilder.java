package at.pegasos.generator.builder;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardOpenOption.APPEND;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import at.pegasos.generator.generator.client.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.univie.mma.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.util.*;
import at.pegasos.generator.parser.token.ServerInstance;
import at.pegasos.generator.util.GitHelper;
import at.univie.mma.pegasos.util.*;

public class AppInstanceBuilder {
  private final static Logger log= LoggerFactory.getLogger(AppInstanceBuilder.class);
  @SuppressWarnings("SimpleDateFormat")
  private final static SimpleDateFormat build_time_stamp_formatter= new SimpleDateFormat("yyyy.MM.dd'.'HH:mm:ss");

  private Config app_conf;
  private final Application inst;
  private final Path dir;
  private final ServerInstanceParser server_instances_parser;
  private final Path builds;
  private final Path gen_base_dir;
  private final GitHelper git;
  private final ToolsBuilder toolsBuilder;
  private final MMAGenerator mainGenerator;

  private String package_name;

  /**
   * DependencyManager
   */
  private DependencyManager app;

  /**
   * Extra map for the 'convenience' inferal of dependencies
   */
  HashMap<String, String> dependencies_extra= new HashMap<String, String>();

  private final Map<String, Application> libraries;

  public AppInstanceBuilder(Application inst, Path dir, ServerInstanceParser server_instances_parser,
      Path builds, GitHelper git,
      final Map<String, Application> libraries, MMAGenerator mainGenerator, final ToolsBuilder toolsBuilder)
  {
    this.inst= inst;
    this.dir= dir;
    this.server_instances_parser= server_instances_parser;
    this.builds= builds;
    this.git= git;
    this.mainGenerator = mainGenerator;
    this.toolsBuilder = toolsBuilder;

    this.libraries= libraries;

    gen_base_dir= dir.resolve("MMAv3").resolve("gen");
  }

  private void generateCode() throws GeneratorException
  {
    ClientCodeGenerator codeGenerator= new ClientCodeGenerator(inst, gen_base_dir, !Files.exists(dir.resolve("Clients").resolve("PegasosClientCore")));
    codeGenerator.generate();

    List<CodeGenerator> generators= new ArrayList<CodeGenerator>();

    generators.add(new SportsFactory(inst.getSports()));
    generators.add(new SportsGenerator(inst.getSports()));
    generators.add(new ActionGenerator("at.pegasos.client", "StartupActions", inst.startup_actions));

    boolean legacy= !Files.exists(dir.resolve("MMAv3").resolve("src").resolve("at").resolve("univie").resolve("mma").resolve("ui")
        .resolve("MeasurementFragment.java"));
    boolean legacy2= !Files.exists(dir.resolve("MMAv3").resolve("src").resolve("at").resolve("pegasos").resolve("client").resolve("ui")
        .resolve("TrainingActivity.java"));
    log.debug("Generating sports {} {}", legacy, legacy2);
    for(Sport sport : inst.getSports())
      generators.add(new SportGenerator(sport, legacy, legacy2));

    Sensors sensors= inst.getSensors();
    generators.add(new SensorTypeNamesGenerator(sensors));

    try
    {
      log.info("Generating config");
      // TODO: move config

      ConfigGenerator confgen= new ConfigGenerator("at.univie.mma", app_conf);
      confgen.setOutputToPrintStream(gen_base_dir);
      confgen.generate();
      confgen.closeOutput();

      for(CodeGenerator gen : generators)
      {
        gen.setOutputToPrintStream(gen_base_dir);
        gen.generate();
        gen.closeOutput();
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException("Could not write all Code");
    }
  }

  /**
   * This method is intended to remove some code which is not necessary in the app For example it
   * deletes unused sensor code
   * 
   * @throws GeneratorException when something fails during the generation
   */
  private void cleanSrcDir() throws GeneratorException
  {
    app.debug();
    
    for(String clasz : app.getIndependentFiles(new DependencyManager.DependencyObject("App")))
    {
      Path file= dir.resolve("MMAv3").resolve(clasz);
      log.debug("Removing object " + clasz + ": " + file);
      try
      {
        if( Files.isDirectory(file) )
        {
          FileHelper.deleteDirectory(file);
        }
        else
        {
          Files.delete(file);
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new GeneratorException(e.getMessage());
      }
    }
  }

  public void build() throws IOException, InterruptedException, GeneratorException, ParserException
  {
    String menu_name= inst.name.replaceAll(" ", "_");
    menu_name= menu_name.replaceAll("'", "");
    menu_name= menu_name.replaceAll("[.]", "");
    menu_name= menu_name.trim() + "Menu";
    MenuGenerator menugen= new MenuGenerator(inst, menu_name);
    Path menu = dir.resolve("MMAv3").resolve("gen").resolve("at").resolve("pegasos").resolve("client").resolve("ui");
    menu= menu.resolve(menu_name + ".java");

    boolean notfirst= false;

    // Go over all servers and build the instance-server combinations
    for(String server_name : inst.servers)
    {
      // First of all restore directory to original state
      try
      {
        git.cleanUp();
      }
      catch( NoWorkTreeException | GitAPIException e )
      {
        e.printStackTrace();
        throw new GeneratorException("Could not restore Repo to original state");
      }

      // Copy libraries
      FileHelper.copyDirectory(dir.resolve("lib"), dir.resolve("MMAv3").resolve("libs"));

      ServerInstance server= server_instances_parser.getInstance(server_name);

      Builder builder = new GradleBuilder(dir.resolve("MMAv3"), inst.name + "-android-app");

      // Now copy all the files to the directory
      copyFiles(server_name);

      builder.prepareBuild();

      if( notfirst )
      {
        builder.buildTargets("clean");
        builder.prepareBuild();
      }

      app_conf= inst.getConfig();
      app_conf.setValueString("menuclass", "at.pegasos.client.ui." + menu_name);
      app_conf.setValueString("app.name", inst.name);
      app_conf.setValueString("mma.server.ip", server.IP);
      app_conf.setValue("mma.server.port", "" + server.port);
      long timestamp= System.currentTimeMillis();
      app_conf.setValueString("build.id",
          server.name + "-" + inst.name + "-" + inst.getGitBranch(mainGenerator.getParameters()) + "-" + build_time_stamp_formatter.format(timestamp));
      app_conf.setValueString("build.time", build_time_stamp_formatter.format(timestamp));

      String baseURL = Web.getBackendBaseUrl(server.getWeb().config);
      app_conf.setValueString("backendBaseURL", baseURL);

      if( Files.notExists(menu.getParent().toAbsolutePath()) )
        Files.createDirectories(menu.getParent().toAbsolutePath());

      log.info("Generating menu: {}", menu.toAbsolutePath());
      menugen.setOutput(new PrintStream(Files.newOutputStream(menu)));
      menugen.generate();
      menugen.closeOutput();

      setupDependencyManager();

      generateCode();
      addContributions();

      inferDependencies();

      cleanSrcDir();

      addManifestContributions();
      changeReferences();

      addAndroidLibraries();

      setVersion();

      log.debug("Uses tools? {}", app_conf.hasValue("use_tools") && app_conf.getValueBool("use_tools") || app_conf.hasValue("use.tools") && app_conf.getValueBool("use.tools"));
      if( app_conf.hasValue("use_tools") && app_conf.getValueBool("use_tools") ||
          app_conf.hasValue("use.tools") && app_conf.getValueBool("use.tools") )
      {
        Files.copy(toolsBuilder.getToolsJar(server_name), dir.resolve("MMAv3").resolve("libs").resolve("tools.jar"),
            java.nio.file.StandardCopyOption.REPLACE_EXISTING);
      }

      buildCore(server_name);

      builder.buildTargets("assembleDebug");
      if( Files.exists(dir.resolve("MMAv3").resolve("build").resolve("outputs").resolve("apk").resolve("debug").resolve("MMAv3-debug.apk")) )
      {
        Files.copy(dir.resolve("MMAv3").resolve("build").resolve("outputs").resolve("apk").resolve("debug").resolve("MMAv3-debug.apk"),
            builds.resolve(inst.name + "-" + server_name + ".apk"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
      }
      else
      {
        if( Files.exists(dir.resolve("MMAv3").resolve("build").resolve("outputs").resolve("apk").resolve("MMAv3-debug.apk")) )
        {
          Files.copy(dir.resolve("MMAv3").resolve("build").resolve("outputs").resolve("apk").resolve("MMAv3-debug.apk"),
              builds.resolve(inst.name + "-" + server_name + ".apk"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
        }
        else
          throw new GeneratorException("Could not find build");
      }

      notfirst= true;
    }
  }

  private void buildCore(String server_name) throws IOException, InterruptedException
  {
    log.info("Building Pegasos Client Core");

    AntBuilder coreBuilder = new AntBuilder(dir.resolve("Clients").resolve("PegasosClientCore"), "core");
    coreBuilder.setProperty("server.interface", mainGenerator.getServerInterfaceHandle(server_name));
    coreBuilder.buildTargets("CreateJar");

    Files.copy(dir.resolve("Clients").resolve("PegasosClientCore").resolve("pegasos-client-core.jar"),
        dir.resolve("MMAv3").resolve("libs").resolve("pegasos-client-core.jar"),
        java.nio.file.StandardCopyOption.REPLACE_EXISTING);
  }

  /**
   * copy dependency files such as local.properties and the serverinterface into their corresponding directories
   *
   * @param server_name name of the server for which the build will happen (necessary for selecting the correct server interface)
   * @throws IOException when a file copy operation fails
   */
  private void copyFiles(String server_name) throws IOException
  {
    Path conf= Paths.get("local.properties").toAbsolutePath().normalize();
    Path build;

    if( !Files.exists(conf) )
    {
      log.warn("'local.properties' does not exist in current directory. Checking for packaged file");
      conf= Paths.get(getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParent().resolve("local.properties");

      if( !Files.exists(conf) )
      {
        log.warn("No 'local.properties' present. Reverting to default generated 'sdk.dir=/opt/android'");

        conf= Files.createTempFile("local", "properties");

        BufferedWriter writer= Files.newBufferedWriter(conf);
        writer.write("## This is a file generated by Pegasos");
        writer.write("# Location of the SDK. This is only used by Gradle");
        writer.write("sdk.dir=/opt/android");
        writer.close();
      }
      else
      {
        log.debug("Using packaged 'local.properties'");
      }
    }

    build= dir.resolve("local.properties");
    log.debug("Copy {} to {}", conf, build);
    Files.copy(conf, build, REPLACE_EXISTING);

    Path si = Paths.get(mainGenerator.getServerInterfaceHandle(server_name));
    Files.copy(si , dir.resolve("MMAv3").resolve("libs").resolve("ServerInterface.jar"),
        java.nio.file.StandardCopyOption.REPLACE_EXISTING);
  }

  /**
   * Add Android libraries to the main project dir
   * 
   * @throws IOException updating of files failed
   */
  private void addAndroidLibraries() throws IOException
  {
    log.info("Adding Android Libraries to build files");

    BufferedWriter out= Files.newBufferedWriter(dir.resolve("settings.gradle"), Charset.defaultCharset(), APPEND);
    List<String> dependencies= new ArrayList<String>(inst.android_projects.size() + inst.android_dependencies.size());
    for(String lib : inst.android_projects)
    {
      Path p= Paths.get(lib);
      if( !p.isAbsolute() )
        p= inst.directory.resolve(lib).toAbsolutePath();

      // TODO: check if path exists and raise ex otw. We will fail during compile anyway ...

      String name= p.getFileName().toString();

      out.write("include ':" + name + "'\n");
      out.write("project(':" + name + "').projectDir = new File('" + p + "')\n");

      dependencies.add("\t\timplementation project(':" + name + "')");
    }
    out.close();

    for(String lib : inst.android_dependencies )
    {
      dependencies.add("implementation '" + lib + "'");
    }

    Path build_gradle= dir.resolve("MMAv3").resolve("build.gradle");
    List<String> fileContent= Files.readAllLines(build_gradle, Charset.defaultCharset());
    final String Begin= "start of generated dependencies";
    final String End= "end of generated dependencies";

    int i= 0;
    while( i < fileContent.size() )
    {
      // Find begin of create Table
      if( fileContent.get(i++).trim().contains(Begin) )
      {
        // Remove everything until /* createTable - end */
        int j= i;
        while( j < fileContent.size() && !fileContent.get(j).trim().contains(End) )
          fileContent.remove(j);

        // Add the new table and exit loop
        fileContent.addAll(i, dependencies);
        System.out.println("Adding all dependencies");
        break;
      }
    }
    Files.write(build_gradle, fileContent, Charset.defaultCharset());

    build_gradle= dir.resolve("build.gradle");
    fileContent= Files.readAllLines(build_gradle, Charset.defaultCharset());
    final String BeginRepo= "start of generated repositories";
    final String EndRepo= "end of generated repositories";
    i= 0;
    while( i < fileContent.size() )
    {
      // Find begin of create Table
      if( fileContent.get(i++).trim().contains(BeginRepo) )
      {
        // Remove everything until /* createTable - end */
        int j= i;
        while( j < fileContent.size() && !fileContent.get(j).trim().contains(EndRepo) )
          fileContent.remove(j);

        // Add the new table and exit loop
        fileContent.addAll(i, inst.android_repositories);
        System.out.println("Adding all repositories");
        break;
      }
    }
    Files.write(build_gradle, fileContent, Charset.defaultCharset());
  }

  private void addExtra(Path directory, String name) throws IOException
  {
    Path extra_files= directory.resolve("app").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra source files for app present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("MMAv3").resolve("src"));
    }
    else
      log.debug("No extra source files for app present in " + name);

    extra_files= directory.resolve("app").resolve("res");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra rersources for app present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("MMAv3").resolve("res"));
    }
    else
      log.debug("No extra resource files for app present in " + name);

    extra_files= directory.resolve("app").resolve("libs");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra libraries for app present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("MMAv3").resolve("libs"));
    }
    else
      log.debug("No extra libraries files for app present in " + inst.name);

    extra_files= directory.resolve("common").resolve("src");
    if( Files.exists(extra_files) ) 
    {
      log.debug("Extra common files present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("MMAv3").resolve("src"));
    }
    else
      log.debug("No extra common files present in " + name);
  }

  private void addContributions() throws IOException
  {
    addExtra(inst.directory, inst.name);

    for(String name : inst.getLibraries() )
    {
      Application lib= libraries.get(name);
      // lib cannot be null here since existence was checked during parsing
      addExtra(lib.directory.toAbsolutePath(), name);
      
      DependencyHelper.loadDependencies(lib.directory.toAbsolutePath().resolve("dependencies"), app, dependencies_extra);
    }

    Path extra_files= inst.directory.resolve("app").resolve("build.gradle");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra build.grade for app present in " + inst.name);
      Files.copy(extra_files, dir.resolve("MMAv3").resolve("build.gradle"),
              java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }
    extra_files = inst.directory.resolve("app").resolve("proguard-rules.pro");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra proguard-rules.pro for app present in " + inst.name);
      Files.copy(extra_files, dir.resolve("MMAv3").resolve("proguard-rules.pro"),
              java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }
    extra_files = inst.directory.resolve("build.gradle");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra build.grade present in " + inst.name);
      Files.copy(extra_files, dir.resolve("build.gradle"),
          java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }
    extra_files = inst.directory.resolve("gradle.properties");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra gradle.properties present in " + inst.name);
      Files.copy(extra_files, dir.resolve("gradle.properties"),
              java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    /*extra_files= inst.directory.resolve("common").resolve("src");
    if( Files.exists(extra_files) ) 
    {
      log.debug("Extra common files present in " + inst.name);
      FileHelper.copyDirectory(extra_files, dir.resolve("MMAv3").resolve("src"));
    }
    else
      log.debug("No extra common files present in " + inst.name);*/
  }

  private void changeReferences() throws IOException
  {
    package_name= "at.pegasos." + inst.name.replaceAll(" ", "_").replaceAll("\'", "").trim();

    List<String> fileContent= Files.readAllLines(dir.resolve("MMAv3").resolve("AndroidManifest.xml"), StandardCharsets.ISO_8859_1);

    int i= 0;
    while( i < fileContent.size() )
    {
      if( fileContent.get(i).contains("package=\"at.univie.mma\"") )
      {
        fileContent.set(i, fileContent.get(i).replaceAll("package=\"at.univie.mma\"", "package=\"" + package_name + "\""));
        break;
      }
      i++;
    }
    Files.write(dir.resolve("MMAv3").resolve("AndroidManifest.xml"), fileContent, StandardCharsets.ISO_8859_1);

    Path[] dirs= new Path[] {dir.resolve("MMAv3").resolve("src"), dir.resolve("MMAv3").resolve("gen")};
    for(Path dir : dirs)
    {
      examineDir(dir);
    }
  }

  private void addManifestContributions() throws GeneratorException
  {
    DocumentBuilderFactory dbf= DocumentBuilderFactory.newInstance();
    DocumentBuilder db;
    Document doc;
    
    try
    {
      db= dbf.newDocumentBuilder();
      doc= db.parse(dir.resolve("MMAv3").resolve("AndroidManifest.xml").toFile());

      NodeList application= doc.getElementsByTagName("application");
      NodeList manifest= doc.getElementsByTagName("manifest");

      for(Node n : inst.manifest.application)
      {
        Node nodeArea= doc.importNode(n, true);
        application.item(0).appendChild(nodeArea);
      }
      for(Node n : inst.manifest.root)
      {
        Node nodeArea= doc.importNode(n, true);
        manifest.item(0).appendChild(nodeArea);
      }

      TransformerFactory tFactory= TransformerFactory.newInstance();
      Transformer transformer= tFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");

      DOMSource source= new DOMSource(doc);
      StreamResult result= new StreamResult(new StringWriter());
      transformer.transform(source, result);

      String xmlOutput= result.getWriter().toString();
      PrintStream out= new PrintStream(Files.newOutputStream(dir.resolve("MMAv3").resolve("AndroidManifest.xml")));
      out.print(xmlOutput);
      out.close();
    }
    catch( ParserConfigurationException | SAXException | IOException | TransformerException e )
    {
      e.printStackTrace();
      throw new GeneratorException("Error modifying manifest");
    }
  }

  private void setVersion() throws GeneratorException
  {
    if( inst.version == null )
      return;

    try
    {
      final Path manifest= dir.resolve("MMAv3").resolve("AndroidManifest.xml");
      final String vc= "android:versionCode=\"1\"";
      final String vn= "android:versionName=\"1.0\"";
      String code= "android:versionCode=\"" + inst.version.code + "\"";
      String name= "android:versionName=\"" + inst.version.name + "\"";

      boolean changed= false;
      List<String> fileContent= Files.readAllLines(manifest, StandardCharsets.ISO_8859_1);

      int i= 0;
      while( i < fileContent.size() )
      {
        if( fileContent.get(i).contains(vc) )
        {
          fileContent.set(i, fileContent.get(i).replaceAll(vc, code));
          changed= true;
        }
        if( fileContent.get(i).contains(vn) )
        {
          fileContent.set(i, fileContent.get(i).replaceAll(vn, name));
          changed= true;
        }
        i++;
      }
      
      if( changed )
      {
        log.info("Updating version numbers in Manifest");
        Files.write(manifest, fileContent, StandardCharsets.ISO_8859_1);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }

  private void examineDir(Path dir) throws IOException
  {
    log.debug("Travsering " + dir);
    try( DirectoryStream<Path> directoryStream= Files.newDirectoryStream(dir) )
    {
      for(Path path : directoryStream)
      {
        // Check whether it is a directory
        if( path.toFile().isDirectory() )
        {
          examineDir(path);
        }
        else
        {
          if( path.toFile().isFile() && path.getFileName().toString().endsWith("java") )
          {
            boolean changed= false;
            List<String> fileContent= Files.readAllLines(path, StandardCharsets.ISO_8859_1);

            int i= 0;
            while( i < fileContent.size() )
            {
              if( fileContent.get(i).contains("at.univie.mma.R") )
              {
                fileContent.set(i, fileContent.get(i).replaceAll("at.univie.mma.R", package_name + ".R"));
                changed= true;
              }
              if( fileContent.get(i).contains("at.univie.mma.BuildConfig") )
              {
                fileContent.set(i, fileContent.get(i).replaceAll("at.univie.mma.BuildConfig", package_name + ".BuildConfig"));
                changed= true;
              }
              i++;
            }

            if( changed )
            {
              log.info("Updating resource references in " + path);
              Files.write(path, fileContent, StandardCharsets.ISO_8859_1);
            }
          }
        }
      }
    }
  }

  private void setupDependencyManager()
  {
    app= new DependencyManager();

    app.addObject("App");
    app.addObject("sensors");

    app.addDependency("App", "sensors");

    try
    {
      DependencyHelper.loadDependencies(dir.resolve("MMAv3").resolve("dependencies"), app, dependencies_extra);
      
      if( inst.config.getValueBool("use.sensormanager") )
      {
        app.addDependency("App", "SensorManager");
      }
    }
    catch( ParserException | IOException e )
    {
      e.printStackTrace();
      log.error("Something is really wrong here ...");
    }
  }

  private void inferDependencies()
  {
    inst.getSensors().setDependencies(app, dir.resolve("MMAv3"));
  }
}
