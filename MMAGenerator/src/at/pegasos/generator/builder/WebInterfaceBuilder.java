package at.pegasos.generator.builder;

import java.io.*;
import java.nio.file.*;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.token.*;
import at.univie.mma.pegasos.util.*;
import lombok.extern.slf4j.*;

@Slf4j
public class WebInterfaceBuilder {

  private final Builder web_backend_builder;
  private final Builder web_frontend_builder;

  private final ServerInstance instance;

  private final Path dir;

  public WebInterfaceBuilder(Path dir, ServerInstance instance) throws IOException
  {
    this.instance = instance;
    this.dir = dir;

    web_backend_builder = new GradleBuilder(dir.resolve("server").resolve("web").resolve("backend"),
            instance.name + "-web-backend");
    web_backend_builder.prepareBuild();

    web_frontend_builder = new AntBuilder(dir.resolve("server").resolve("web").resolve("frontend"),
            instance.name + "-frontend");
    web_frontend_builder.prepareBuild();
  }

  public void build() throws IOException, InterruptedException
  {
    web_backend_builder.setProperty("instname", this.instance.name);
    if( !Parameters.get().generateOnly )
    web_backend_builder.buildTargets("bootJar");

    web_backend_builder.prepareBuild();
    web_backend_builder.buildTargets("generateTypeScript");
    Files.copy(dir.resolve("server").resolve("web").resolve("backend").resolve("build").resolve("sample.ts"),
            dir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app").resolve("shared").resolve("sample.ts"));

    web_frontend_builder.setProperty("name", this.instance.name);
    if( !Parameters.get().generateOnly )
    web_frontend_builder.buildTargets("compile", "export");
  }
}
