package at.pegasos.generator.builder;

import at.pegasos.generator.*;
import at.pegasos.generator.generator.server.*;
import at.pegasos.generator.generator.serverinterface.ServerInterfaceGenerator;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.util.*;
import org.eclipse.jgit.api.errors.*;
import org.gradle.tooling.*;
import org.slf4j.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.backend.*;
import at.pegasos.generator.parser.backend.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.servergenerator.*;
import at.pegasos.util.*;
import at.univie.mma.pegasos.util.*;
import at.univie.mma.pegasos.webgenerator.*;

/**
 * Helper facility for building the server instance. After the builder is created. The build needs to be prepared using 'prepareBuild()'.
 * Afterwards instance of Apps can be added using 'addAppInstance'. 
 */
public class ServerInstanceBuilder {
  private final ServerInstance instance;
  /**
   * Working directory. Location of the directory where the files will be placed
   */
  private final Path dir;

  private final List<Instance> apps;

  private Builder server_builder;
  private Builder ai_builder;
  private Builder si_builder;

  private WebInterfaceBuilder web_builder;

  private Commands commands;

  private final GitHelper git;

  private boolean build_required= true;

  /**
   * Location where the builds will be stored
   */
  private final Path builds;
  private DependencyManager dependencies;
  private Map<String, String> dependencies_extra;
  private DependencyManager dependenciessi;
  private HashMap<String, String> dependencies_extrasi;

  private boolean legacybuild= true;

  private final Map<String, Application> libraries;

  /**
   * If true web interface of this instance is not built
   */
  private boolean omitweb= false;

  /**
   * Parameters for the application (passed to main)
   */
  private final Parameters parameters;

  private final static Logger log= LoggerFactory.getLogger(ServerInstanceBuilder.class);

  private boolean oldUnivie;

  private String dockerUserCreateFile;

  /**
   *
   * @param instance the instance to be built
   * @param dir where the build should take place
   * @param builds where the finished builds should be stored
   * @param libraries available libraries
   * @param parameters Parameters for the Generator (passed to the main application)
   * @throws IOException when the temp directory cannot be created
   */
  public ServerInstanceBuilder(ServerInstance instance, Path dir, Path builds, Map<String, Application> libraries, Parameters parameters) throws IOException
  {
    this.instance= instance;
    this.dir= Files.createTempDirectory(dir, instance.name);
    this.apps= new ArrayList<Instance>();
    this.builds= builds;
    this.parameters= parameters;

    this.libraries= libraries;

    git= new GitHelper(instance, this.dir, parameters);
  }

  public String getServername()
  {
    return instance.name;
  }

  public boolean buildRequired(List<Long> times_instances, long lastbuilt_server) throws IOException, GitAPIException
  {
    log.debug("Is build for server {} required?", instance.name);
    // No copy of Server exists
    // TODO: make this check better as directory could exist but be empty
    if( !Files.exists(builds.resolve("server-" + instance.name)) )
    {
      log.debug("Binaries don't exist -> building");
      return true;
    }
    else
    {
      long last_modified= LastModification.lastModified(instance.directory);
      if( last_modified > lastbuilt_server )
      {
        log.debug("Changes in sourcecode/config since last build -> building");
        return true;
      }
      else
      {
        last_modified= ((long) git.lastChange(instance.getGitBranch(parameters))) * 1000L;
        // Source-Code has changed since last build
        if( last_modified > lastbuilt_server )
        {
          log.debug("Changes in repository-sourcecode since last build -> building");

          return true;
        }
        else
        {
          for(long time : times_instances)
	  		{
	  			// at least one instance has been modified since last build
	  			if( time > lastbuilt_server )
	  			{
	  				log.debug("An instance has been updated -> building");
	  				return true;
	  			}
	  		}

          build_required = false;
          return false;
        }
      }
    }
  }

  public void setBuildRequired(boolean required)
  {
    build_required= required;
  }

  public void prepareBuild() throws GeneratorException
  {
    try
    {
      log.info("Preparing build for {}. {}", instance.name, instance.git_root);
      Path bd = dir.resolve("server").resolve("web").resolve("backend");

      git.initAsClone();
      git.prepareBranch(instance.getGitBranch(parameters));

      this.oldUnivie = Files.exists(dir.resolve("server").resolve("mma_server"));

      server_builder = new AntBuilder(dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server"), instance.name + "-server");
      ai_builder = new GradleBuilder(dir.resolve("server").resolve("ai"), instance.name + "-ai");
      si_builder = new AntBuilder(dir.resolve("ServerInterface"), instance.name + "-si");

      Files.createDirectories(dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("bin"));
      server_builder.prepareBuild();

      Files.createDirectories(dir.resolve("server").resolve("ai").resolve("bin"));
      ai_builder.prepareBuild();

      Files.createDirectories(dir.resolve("ServerInterface").resolve("bin"));
      si_builder.prepareBuild();

      if( Files.exists(bd.resolve("src")) )
      {
        legacybuild = false;
        if( !omitweb )
        {
          web_builder = new WebInterfaceBuilder(dir, instance);
        }
      }
      Files.createDirectories(bd.resolve("gen").resolve("java"));

      // Add my own customisations / modifications
      log.info("Adding customisations from " + instance.directory.toAbsolutePath());
      addExtra(instance.directory.toAbsolutePath(), instance.name);
    }
    catch(IOException | GitAPIException e )
    {
      throw new GeneratorException(e);
    }
  }

  public void addAppInstance(Application inst, final Map<String, Application> libraries) throws IOException, ParserException
  {
    if( !build_required )
    {
      log.debug("Building of " + instance.name + " not required -> not adding " + inst.name);
      return;
    }

    log.info("Adding {} to {}", inst.name, this.instance.name);
    log.trace(instance.toString());
    log.debug(inst.toString());
    instance.addApp(inst);

    apps.add(inst);

    addExtra(inst.directory, inst.name);

    for(String name : inst.getLibraries())
    {
      Application lib = libraries.get(name);
      // lib cannot be null here since existence was checked during parsing
      addExtra(lib.directory.toAbsolutePath(), name);
    }
  }

  private void addExtra(Path directory, String name) throws IOException
  {
    log.info("Adding customisations / contributions from {} ({})", name, directory);

    Path extra_files = directory.resolve("server").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for server present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("src"));
    }
    else
      log.debug("No extra files for server present in {}", name);

    extra_files = directory.resolve("server").resolve("lib");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra libraries for server present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("lib"));
    }
    else
      log.debug("No extra libraries for server present in {}", name);

    // Extra AI modules
    extra_files= directory.resolve("ai").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for AI present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
    }
    else
      log.debug("No extra files for ai present in " + name);
    extra_files = directory.resolve("ai").resolve("lib");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra libraries for AI present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("lib"));
    }
    else
      log.debug("No extra libraries for ai present in {}", name);
    extra_files= directory.resolve("ai").resolve("assets");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra asset files for AI present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("assets"));
    }
    else
      log.debug("No extra asset for ai present in {}", name);

    // extensions to the ServerInterface
    extra_files = directory.resolve("serverinterface").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for ServerInterface present in " + name);
      FileHelper.copyDirectory(extra_files, dir.resolve("ServerInterface").resolve("src"));
    }
    else
      log.debug("No extra files for ServerInterface present in {}", name);

    boolean extracommon = false;
    extra_files = directory.resolve("common").resolve("src");
    if( Files.exists(extra_files) )
    {
      extracommon = true;
      log.debug("Extra common files present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("backend").resolve("src").resolve("main").resolve("java"));
    }
    extra_files = directory.resolve("common-servers").resolve("src");
    if( Files.exists(extra_files) )
    {
      extracommon = true;
      log.debug("Extra common files present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("backend").resolve("src").resolve("main").resolve("java"));
    }
    extra_files = directory.resolve("common-server-webbackend").resolve("src");
    if( Files.exists(extra_files) )
    {
      extracommon = true;
      log.debug("Extra common files present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("backend").resolve("src").resolve("main").resolve("java"));
    }
    extra_files = directory.resolve("common-ai-webbackend").resolve("src");
    if( Files.exists(extra_files) )
    {
      extracommon = true;
      log.debug("Extra common files present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("backend").resolve("src").resolve("main").resolve("java"));
    }
    extra_files = directory.resolve("common-backend").resolve("src");
    if( Files.exists(extra_files) )
    {
      extracommon = true;
      log.debug("Extra common files present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("backend_server").resolve("src"));
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
    }
    if( !extracommon )
      log.debug("No extra common files present in {}", name);

    extra_files = directory.resolve("web").resolve("backend");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for web-backend present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("backend"));
    }
    else
      log.debug("No extra files for web-backend present in {}", name);

    extra_files = directory.resolve("web").resolve("frontend").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for web-frontend present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("frontend").resolve("src"));
    }
    else
      log.debug("No extra files for web-frontend present in {}", name);

    extra_files = directory.resolve("web").resolve("frontend").resolve("lang");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra lang files for web-frontend present in {}", name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("web").resolve("frontend").resolve("lang"));
    }
    else
      log.debug("No extra files for web-backend present in {}", name);
  }

  /**
   * Build the server, ai, serverinterface and web.
   *
   * @return true if built, false if built was not required
   * @throws IOException
   * @throws InterruptedException
   * @throws ParserException
   * @throws GeneratorException
   */
  public boolean build() throws IOException, InterruptedException, ParserException, GeneratorException, BuildException
  {
    if( !build_required )
      return false;

    setupDependencyManager();

    parse();

    generate();

    inferDependencies();

    Path extra_files = instance.directory.resolve("ai").resolve("src");
    if( Files.exists(extra_files) )
    {
      log.debug("Extra files for AI present in " + instance.name);
      FileHelper.copyDirectory(extra_files, dir.resolve("server").resolve("ai").resolve("src"));
    }
    else
      log.debug("No extra files for ai present in " + instance.name);

    cleanDir();

    server_builder.setProperty("name", this.instance.name);
    if( !Parameters.get().generateOnly )
    server_builder.buildTargets("create_run_jar");

    ai_builder.setProperty("instname", this.instance.name);
    if( !Parameters.get().generateOnly )
    ai_builder.buildTargets("customFatJar");

    if( !Parameters.get().generateOnly )
    si_builder.buildTargets("CreateJar");

    if( !legacybuild && !omitweb )
    {
      log.info("Build web");
      web_builder.build();
    }

    return true;
  }

  /**
   * Remove unused files from server and ServerInterface using DependencyManager
   *
   * @throws GeneratorException if removing fails
   */
  private void cleanDir() throws GeneratorException
  {
    dependencies.debug();
    dependenciessi.debug();

    for(String clasz : dependencies.getIndependentFiles(new DependencyManager.DependencyObject("Server")))
    {
      Path file= dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve(clasz);
      log.info("Removing object " + clasz + ": " + file);
      try
      {
        if( Files.isDirectory(file) )
        {
          FileHelper.deleteDirectory(file);
        }
        else
        {
          Files.delete(file);
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new GeneratorException(e.getMessage());
      }
    }

    for(String clasz : dependenciessi.getIndependentFiles(new DependencyManager.DependencyObject("ServerInterface")))
    {
      Path file= dir.resolve("ServerInterface").resolve(clasz);
      log.info("Removing object " + clasz + ": " + file);
      try
      {
        if( Files.isDirectory(file) )
        {
          FileHelper.deleteDirectory(file);
        }
        else
        {
          Files.delete(file);
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new GeneratorException(e.getMessage());
      }
    }
  }

  /**
   * Save binaries created during build
   *
   * @throws IOException e.g. if files do not exist
   */
  public void saveBuilds() throws IOException
  {
    if( !build_required || Parameters.get().generateOnly )
      return;

    final Path out = builds.resolve("server-" + instance.name);
    Files.createDirectories(out);

    String fn = "server_" + instance.name + ".jar";
    Files.copy(dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve(fn),
            out.resolve(fn), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

    Files.createDirectories(out.resolve("database"));
    fn = "model.sql";
    Files.copy(dir.resolve("server").resolve(fn), out.resolve("database").resolve(fn), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

    fn = "ai_" + instance.name + ".jar";
    Files.copy(dir.resolve("server").resolve("ai").resolve("build").resolve("libs").resolve(fn),
            out.resolve(fn), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    fn = "ServerInterface.jar";
    Files.copy(dir.resolve("ServerInterface").resolve(fn),
            out.resolve(fn), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

    if( !legacybuild && !omitweb )
    {
      fn = "frontend_" + instance.name + ".zip";
      Files.copy(dir.resolve("server").resolve("web").resolve("frontend").resolve(fn), out.resolve(fn),
          java.nio.file.StandardCopyOption.REPLACE_EXISTING);

      fn = instance.name + "-web-backend.jar";
      Files.copy(dir.resolve("server").resolve("web").resolve("backend").resolve("build").resolve("libs").resolve(fn), out.resolve(fn),
          java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    if( instance.generateDocker() )
    {
      Files.copy(dir.resolve("server").resolve("docker").resolve(".dockerignore"),
              out.resolve(".dockerignore"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

      // Database model is copied above
      Files.copy(dir.resolve("server").resolve("docker").resolve("database").resolve("database-mariadb.yml"),
              out.resolve("database").resolve("database-mariadb.yml"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
      fn = dockerUserCreateFile;
      Files.copy(dir.resolve("server").resolve(fn), out.resolve("database").resolve(fn), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

      // Files.createDirectories(out.resolve("frontend"));
      FileHelper.copyDirectory(dir.resolve("server").resolve("docker").resolve("frontend"),
              out.resolve("frontend"));
      Files.copy(dir.resolve("server").resolve("docker").resolve("nginx.conf"),
              out.resolve("nginx.conf"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

      Files.copy(dir.resolve("server").resolve("docker").resolve("server-backend.yml"),
              out.resolve("server-backend.yml"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

      Files.copy(dir.resolve("server").resolve("docker").resolve("docker-compose.yml"),
              out.resolve("docker-compose.yml"), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    // create a file containing the name of the server
    BufferedWriter writer = Files.newBufferedWriter(out.resolve("name"));
    writer.write(instance.name);
    writer.close();
  }

  private void generate() throws IOException, ParserException, GeneratorException
  {
    // Set the path to the AI file (used by backend server and web-backend)
    instance.config.setValueString("ai_file", "ai_" + instance.name + ".jar");

    if( instance.generateDocker() )
    {
      DockerCompose gen = new DockerCompose(instance);
      // gen.setOutput(System.out);
      // gen.generate();
      gen.setOutputToPrintStream(dir.resolve("server").resolve("docker").resolve("docker-compose.yml"));
      gen.generate();
      gen.closeOutput();

      this.dockerUserCreateFile = "z-user.sql";
      Path user = dir.resolve("server").resolve(dockerUserCreateFile);
      if( !Files.exists(user) )
      {
        try(BufferedWriter writter = Files.newBufferedWriter(user))
        {
          writter.write("CREATE USER '" + instance.config.getValue("db.user") +
                  "'@'%' IDENTIFIED BY '" + instance.config.getValue("db.password") + "';\n");
          writter.write("GRANT SELECT, INSERT, UPDATE, DELETE ON " + instance.config.getValue("db.name") + ".* TO '" +
                  instance.config.getValue("db.user") + "'@'%';\n");
          writter.write("FLUSH PRIVILEGES;\n");
        }
      }
      else
        log.warn("File for creating database user ({}) exists.", user);
    }

    ServerGenerator gen_server= new ServerGenerator(dir, instance, commands, apps);
    gen_server.run();

    ServerInterfaceGenerator gen_si= new ServerInterfaceGenerator(dir, instance, git, parameters);
    gen_si.run();

    log.debug("Need to run web generator: legacybuild: {} omitweb: {}", legacybuild, omitweb);
    if( !legacybuild && !omitweb )
    {
      WebGenerator gen_web= new WebGenerator(dir, instance);
      gen_web.run();
    }

    KeyGenerator g= new KeyGenerator(instance.directory);
    // check if this instance has existing keys
    if( !Files.exists(instance.directory.resolve("public.key"))
        || !Files.exists(instance.directory.resolve("private.key")) )
    {
      log.info("Generating keys for " + instance.name);
      // keys do not exist. Generate them
      g.generateKeys();
      g.saveKeys();
    }
    Files.copy(g.getPrivateKeyFile(), dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("private.key"),
        java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    Files.copy(g.getPublicKeyFile(), dir.resolve("ServerInterface").resolve("public.key"),
        java.nio.file.StandardCopyOption.REPLACE_EXISTING);
  }

  private void parse() throws ParserException, GeneratorException
  {
    Set<String> importedlibs= new HashSet<String>();
    commands= new Commands(oldUnivie);
    
    CommunicationParser cp = new CommunicationParser(instance.directory.resolve("communication.xml"));
    cp.parse();
    commands.addCommands(cp.getCommands());
    log.debug("Instance " + this.instance.name + " defines commands: " + commands + " #apps:" + apps.size());
    
    for(Instance app : apps)
    {
      log.debug("App " + app.name + " for " + instance.name);
      cp = new CommunicationParser(app.directory.resolve("communication.xml"));
      cp.parse();
      Commands help= new Commands(oldUnivie);
      help.addCommands(cp.getCommands());
      log.debug("App adds commands: " + help);
      commands.addCommands(cp.getCommands());

      for(String lib : ((Application) app).getLibraries())
      {
        // check if we have imported this library already
        if( importedlibs.contains(lib) )
          continue;
        
        cp = new CommunicationParser(libraries.get(lib).directory.resolve("communication.xml"));
        cp.parse();
        help= new Commands(oldUnivie);
        help.addCommands(cp.getCommands());
        log.debug("App adds commands: " + help);
        commands.addCommands(cp.getCommands());

        importedlibs.add(lib);
      }

      instance.feedbackmodules.merge(app.feedbackmodules);
      instance.postprocessingmodules.merge(app.postprocessingmodules);

      // TODO: load dependencies
    }

    // Parse the instance itself
    new InstanceParser(instance, dir).parse();

    log.debug("Instance " + this.instance.name + " has commands: " + commands);
    instance.commands= commands;
  }
  
  private void setupDependencyManager()
  {
    dependencies= new DependencyManager();
    dependencies_extra= new HashMap<String,String>();
    dependenciessi= new DependencyManager();
    dependencies_extrasi= new HashMap<String,String>();
    
    dependencies.addObject("Server");
    dependencies.addObject("commands");
    
    dependenciessi.addObject("ServerInterface");
    dependenciessi.addObject("command");
    dependenciessi.addObject("response");
    
    dependencies.addDependency("Server", "commands");
    dependenciessi.addDependency("ServerInterface", "command");
    dependenciessi.addDependency("ServerInterface", "response");
    
    try
    {
      DependencyHelper.loadDependencies(dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("dependencies"), dependencies, dependencies_extra);
      DependencyHelper.loadDependencies(dir.resolve("ServerInterface").resolve("dependencies"), dependenciessi, dependencies_extrasi);
      
      /*if( inst.config.getValueBool("use.sensormanager") )
      {
        app.addDependency("App", "SensorManager");
      }*/
    }
    catch( /*ParserException |*/ IOException e )
    {
      e.printStackTrace();
      log.error("Something is really wrong here ...");
    }
  }
  
  private void inferDependencies()
  {
    dependencies.debug();
    dependenciessi.debug();
    
    Path src= dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("src");
    Path gen= dir.resolve("server").resolve(oldUnivie ? "mma_server" : "backend_server").resolve("gen");
    Path sisrc= dir.resolve("ServerInterface").resolve("src");
    Path sigen= dir.resolve("ServerInterface").resolve("gen");
    List<Path> search= new ArrayList<Path>(2);
    List<Path> sisearch= new ArrayList<Path>(2);
    search.add(src);
    search.add(gen);
    sisearch.add(sisrc);
    sisearch.add(sigen);
    
    
    // Add depencencies
    // this.commands.getCommands().get(0).server_class
    for(Command command : this.commands.getAllCommands())
    {
      if( DependencyHelper.addImplementationAndInfer(command.server_class, search, dependencies, dependencies_extra, "commands") )
      {
        log.debug("Depencencies from server commands to " + command.server_class + " added");
      }
      
      if( command.client_command_class != null )
      {
        if( DependencyHelper.addImplementationAndInfer(command.client_command_class, sisearch, dependenciessi, dependencies_extrasi, "command") )
        {
          log.debug("Depencencies from SI command to " + command.client_command_class + " added");
        }
      }
      if( command.response_class != null )
      {
        if( DependencyHelper.addImplementationAndInfer(command.response_class, sisearch, dependenciessi, dependencies_extrasi, "response") )
        {
          log.debug("Depencencies from SI response to " + command.response_class + " added");
        }
      }
    }
  }

  public Path getServerInterface()
  {
    return builds.resolve("server-" + instance.name).resolve("ServerInterface.jar");
  }

  /**
   * Get the directory where the temporary / build files are located
   * @return directory
   */
  public Path getBuildDir()
  {
    return dir;
  }

  public void setOmitWeb(boolean omitweb)
  {
    this.omitweb= omitweb;
  }

  public IInstance getInstance()
  {
    return this.instance;
  }
}
