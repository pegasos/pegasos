package at.pegasos.generator.builder;

import at.pegasos.generator.*;
import at.pegasos.generator.generator.*;
import at.univie.mma.*;
import at.univie.mma.pegasos.util.*;
import lombok.extern.slf4j.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

@Slf4j
public class ToolsBuilder {
  /**
   * Directory where the sources (root) is located
   */
  private final Path dir;
  private final Map<String, Boolean> ready;
  private final MMAGenerator mainGenerator;

  public ToolsBuilder(Path dir, MMAGenerator gen)
  {
    this.dir = dir;
    this.mainGenerator = gen;

    this.ready = new HashMap<>();
  }

  /**
   * get the 'tools.jar' for a specific server. If it has not been build before, it will be build
   *
   * @param serverName name of the server
   * @return Path to toos.jar
   * @throws GeneratorException if the compilation of the tools failed
   */
  public Path getToolsJar(String serverName) throws GeneratorException
  {
    if( ready.get(serverName) == null || !ready.get(serverName) )
    {
      build(serverName);
    }
    return mainGenerator.getBuildsDir().resolve("server-" + serverName).resolve("tools.jar");
  }

  public void build(String serverName) throws GeneratorException
  {
    Builder builder = new AntBuilder(dir.resolve("Tools"), "tools");

    builder.setProperty("server.interface", mainGenerator.getServerInterfaceHandle(serverName));

    try
    {
      builder.buildTargets("create_jar");

      log.debug("Copy from {} to {}", dir.resolve("Tools").resolve("tools.jar"),
          mainGenerator.getBuildsDir().resolve("server-" + serverName).resolve("tools.jar"));

      Files.copy(dir.resolve("Tools").resolve("tools.jar"),
          mainGenerator.getBuildsDir().resolve("server-" + serverName).resolve("tools.jar"),
          java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }
    catch( IOException | InterruptedException e )
    {
      throw new GeneratorException(e);
    }

    ready.put(serverName, true);
  }
}
