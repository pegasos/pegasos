package at.pegasos.generator.servergenerator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import at.pegasos.generator.generator.GeneratorException;

public class KeyGenerator {
  private PublicKey publicKey;
  private PrivateKey privateKey;
  
  private final Path dir;
  
  /**
   * 
   * @param workingdir
   *          directory where operations will take place ie. dir where keys will be stored
   */
  public KeyGenerator(Path workingdir)
  {
    this.dir= workingdir;
  }
  
  /**
   * Generate a public/private key pair
   * 
   * @throws GeneratorException
   */
  public void generateKeys() throws GeneratorException
  {
    KeyPairGenerator kpg;
    try
    {
      kpg= KeyPairGenerator.getInstance("RSA");
      kpg.initialize(2048);
      KeyPair kp= kpg.genKeyPair();
      publicKey= kp.getPublic();
      privateKey= kp.getPrivate();
    }
    catch( NoSuchAlgorithmException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new GeneratorException(e);
    }
  }
  
  public void saveKeys() throws IOException
  {
    // save public key
    X509EncodedKeySpec x509EncodedKeySpec= new X509EncodedKeySpec(publicKey.getEncoded());
    FileOutputStream fos= new FileOutputStream(dir.resolve("public.key").toFile());
    fos.write(x509EncodedKeySpec.getEncoded());
    fos.close();
    
    // save private Key
    PKCS8EncodedKeySpec pkcs8EncodedKeySpec= new PKCS8EncodedKeySpec(privateKey.getEncoded());
    fos= new FileOutputStream(dir.resolve("private.key").toFile());
    
    fos.write(pkcs8EncodedKeySpec.getEncoded());
    fos.close();
  }

  public Path getPublicKeyFile()
  {
    return dir.resolve("public.key");
  }

  public Path getPrivateKeyFile()
  {
    return dir.resolve("private.key");
  }

  public static void main(String[] args) throws GeneratorException, IOException, NoSuchAlgorithmException, InvalidKeySpecException,
      NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
  {
    KeyGenerator g= new KeyGenerator(Paths.get("."));
    g.generateKeys();
    g.saveKeys();
    
    String msg= "This is a Test";
    
    // now read the actual key from the file
    ByteArrayOutputStream buffer= new ByteArrayOutputStream(2048);
    FileInputStream in= new FileInputStream(new File("public.key"));
    
    // Just to be safe we read it in loop
    int nRead;
    byte[] data= new byte[2048];
    
    while( (nRead= in.read(data, 0, data.length)) != -1 )
    {
      buffer.write(data, 0, nRead);
    }
    in.close();
    
    byte[] keyBytes= buffer.toByteArray();
    
    X509EncodedKeySpec keySpec= new X509EncodedKeySpec(keyBytes);
    KeyFactory fact= KeyFactory.getInstance("RSA");
    PublicKey pubKey= fact.generatePublic(keySpec);
    
    Cipher cipherEncrypt= Cipher.getInstance("RSA");
    cipherEncrypt.init(Cipher.ENCRYPT_MODE, pubKey);
    
    byte[] cipherData= cipherEncrypt.doFinal(msg.getBytes(StandardCharsets.UTF_8));
    
    buffer= new ByteArrayOutputStream(2048);
    
    // Just to be safe we read it in loop
    data= new byte[2048];
    
    in= new FileInputStream(new File("private.key"));
    while( (nRead= in.read(data, 0, data.length)) != -1 )
    {
      buffer.write(data, 0, nRead);
    }
    in.close();
    
    keyBytes= buffer.toByteArray();
    
    PKCS8EncodedKeySpec keySpec2= new PKCS8EncodedKeySpec(keyBytes);
    fact= KeyFactory.getInstance("RSA");
    PrivateKey privKey= fact.generatePrivate(keySpec2);
    
    Cipher cipherDecrypt= Cipher.getInstance("RSA");
    cipherDecrypt.init(Cipher.DECRYPT_MODE, privKey);
    
    byte[] cipherData2= cipherDecrypt.doFinal(cipherData);
    
    String msg2= new String(cipherData2);
    System.out.println(msg);
    System.out.println(msg2);
    System.out.println(msg.equals(msg2));
  }
}
