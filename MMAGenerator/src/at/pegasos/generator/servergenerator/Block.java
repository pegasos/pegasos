package at.pegasos.generator.servergenerator;

import java.util.ArrayList;

public class Block {
  int startline;
  int endline;
  int length;
  String[] lines;
  
  public Block(int start, int end, ArrayList<String> lines)
  {
    this.startline= start;
    this.endline= end;
    this.length= end - start;
    
    this.lines= new String[length];
    this.lines= lines.toArray(this.lines);
  }
  
  public String toString()
  {
    String ret= "Block start: " + startline + "{";
    for(String line : lines)
      ret+= line + ",";
    ret+= "}";
    return ret;
  }
}
