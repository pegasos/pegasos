package at.pegasos.generator.servergenerator;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.generator.server.*;
import at.pegasos.generator.generator.server.ai.*;
import at.pegasos.generator.generator.server.backend.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.backend.token.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.slf4j.*;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

public class ServerGenerator {
  private final Path outdir;
  private final Path srv_gen_dir;
  private final String name;
  private final Commands commands;

  private final ServerInstance server_instance;

  private final boolean oldUnivie;

  /**
   * Apps which are associated with this server
   */
  private final List<Instance> apps;

  private final static Logger log = LoggerFactory.getLogger(ServerGenerator.class);

  public ServerGenerator(Path outdir, ServerInstance instance, Commands commands, List<Instance> apps)
  {
    this.outdir = outdir;

    this.server_instance = instance;

    this.commands = commands;

    this.name = instance.name;

    this.apps = apps;

    if( Files.exists(outdir.resolve("server").resolve("mma_server")) )
    {
      this.oldUnivie= true;
      this.srv_gen_dir= outdir.resolve("server").resolve("mma_server").resolve("gen").toAbsolutePath();
    }
    else
    {
      this.oldUnivie= false;
      this.srv_gen_dir= outdir.resolve("server").resolve("backend_server").resolve("gen").toAbsolutePath();
    }
  }

  public void run() throws ParserException, GeneratorException
  {
    try
    {
      generateSensorDataParsers();
      generateDatabase();

      generateSensorParser();

      generateServerCommandParser();

      generateServerConfig();

      new AiGenerator(outdir, server_instance).run();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e);
    }
  }

  private void generateDatabase() throws IOException, GeneratorException
  {
    Path outmodel = outdir.resolve("server").resolve("model.sql");

    // Generate all sensor statements
    PrintStream out = new PrintStream(Files.newOutputStream(outmodel, StandardOpenOption.APPEND));
    for(Sensor sensor : server_instance.getSensors())
    {
      log.debug("Creating db for {}", sensor);
      Database gen_db= new Database(sensor);
      gen_db.setOutput(out);
      gen_db.generate();
    }
    out.close();

    // Import all other model definitions into our model
    for(Instance app : apps)
    {
      log.debug("Examining " + app.directory + " for model.sql" + app.directory.resolve("model.sql"));
      Path model= app.directory.resolve("model.sql");
      if( Files.exists(model) )
      {
        log.info("Adding model contributions from " + app.name);

        // Load content
        List<String> lines= Files.readAllLines(model, StandardCharsets.ISO_8859_1);

        // Put a frame around the contribution
        lines.add(0, "-- begin contributions from " + app.name);
        lines.add("-- end contributions from " + app.name);
        lines.add("");

        // Append
        Files.write(outmodel, lines, StandardCharsets.ISO_8859_1, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
      }
    }
  }

  private void generateSensorDataParsers() throws IOException, GeneratorException
  {
    for(Sensor sensor : server_instance.getSensors())
    {
      if( sensor.type == Sensor.TYPE_GENERATE )
      {
        ServerParser gen= new ServerParser(sensor, sensor.db_table, sensor.name, oldUnivie);

        gen.setOutputToPrintStream(srv_gen_dir);
        gen.generate();
        gen.getOutputStream().close();
      }
    }
  }
  
  private void generateSensorParser() throws GeneratorException
  {
    boolean legacy= false;
    if( oldUnivie )
      legacy= !outdir.resolve("server").resolve("mma_server").resolve("src").resolve("at").resolve("univie").resolve("mma").resolve("server")
          .resolve("parser").resolve("SensorDataParser.java").toFile().exists();

    if( legacy)
      throw new GeneratorException("Cannot generate code for this old version ...");

    CodeGenerator gen= new SensorDataParserTableGenerator(server_instance.getSensors(), oldUnivie);
    try
    {
      gen.setOutputToPrintStream(srv_gen_dir);
      gen.generate();
      gen.closeOutput();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException("Cannot write SensorDataParser");
    }
  }
  
  private void generateServerCommandParser() throws GeneratorException, IOException
  {
    boolean newbuild;
    if( oldUnivie )
      newbuild= outdir.resolve("server").resolve("mma_server").resolve("src").resolve("at").resolve("univie").resolve("mma").resolve("server")
        .resolve("parser").resolve("SensorDataParser.java").toFile().exists();
    else
      newbuild= outdir.resolve("server").resolve("backend_server").resolve("src").resolve("at").resolve("pegasos").resolve("server")
          .resolve("parser").resolve("SensorDataParser.java").toFile().exists();
    
    if( newbuild )
    {
      log.info("Generating ControlCommand - Parser");

      CodeGenerator gen= new ControlCommandGenerator(commands, oldUnivie);
      gen.setOutputToPrintStream(srv_gen_dir);
      gen.generate();
      gen.closeOutput();
    }
    else
      generateServerCommandParserOld();
  }

  private void generateServerCommandParserOld() throws GeneratorException
  {
    Path p;
    if( oldUnivie )
      p= outdir.resolve("server").resolve("mma_server").resolve("src").resolve("at").resolve("univie").resolve("mma").resolve("server")
        .resolve("parser").resolve("ControlCommand.java").toAbsolutePath();
    else
      p= outdir.resolve("server").resolve("backend_server").resolve("src").resolve("at").resolve("pegasos").resolve("server")
          .resolve("parser").resolve("ControlCommand.java").toAbsolutePath();

    try
    {
      // System.out.println(p.toString() + " " + Files.exists(p));
      log.info("Generating ControlCommand - Parser");
      List<String> fileContent = new ArrayList<String>(Files.readAllLines(p, StandardCharsets.ISO_8859_1));
      ControlCommandGenerator gen = new ControlCommandGenerator(commands, oldUnivie);

      int i= 0;
      while( i < fileContent.size() )
      {
        // Find begin of create Table
        if( fileContent.get(i++).trim().contains("/* createTable - begin */") )
        {
          // Remove everything until /* createTable - end */
          //noinspection UnnecessaryLocalVariable because it gives us flexibility in case we would like to move j
          int j = i;
          while( j < fileContent.size() && !fileContent.get(j).trim().contains("/* createTable - end */") )
            fileContent.remove(j);

          // Add the new table and exit loop
          fileContent.addAll(i, gen.generateCreateTable());
          break;
        }
      }

      i = 0;
      while( i < fileContent.size() )
      {
        // Find begin of create Table
        if( fileContent.get(i++).trim().contains("/* testTable - begin */") )
        {
          // Remove everything until /* testTable - end */
          //noinspection UnnecessaryLocalVariable
          int j = i;
          while( j < fileContent.size() && !fileContent.get(j).trim().contains("/* testTable - end */") )
            fileContent.remove(j);

          // Add the new table and exit loop
          fileContent.addAll(i, gen.generateCreateTestTable());

          break;
        }
      }

      Files.write(p, fileContent, StandardCharsets.ISO_8859_1);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }

  private void generateServerConfig() throws GeneratorException, IOException, ParserException
  {
    server_instance.config.setValue("port", server_instance.port);
    server_instance.config.setValueString("name", name);

    CodeGenerator gen= new ConfigGenerator(oldUnivie ? "at.univie.mma.server" : "at.pegasos.server", server_instance.config);
    gen.setOutputToPrintStream(srv_gen_dir);
    gen.generate();
    gen.closeOutput();

    gen= new CommandsGenerator(oldUnivie ? "at.univie.mma.server" : "at.pegasos.server", true, server_instance);
    gen.setOutputToPrintStream(srv_gen_dir);
    gen.generate();
    gen.closeOutput();
  }
}
