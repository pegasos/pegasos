package at.pegasos.generator.servergenerator;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import org.slf4j.*;

import java.io.*;
import java.util.*;

public class SensorConfigParser {
  private final static String BLOCK_SEPARATOR = "----------------";

  private final static Logger log = LoggerFactory.getLogger(ServerGenerator.class);
  private final ArrayList<Field> fields;
  private final Sensor sensor;
  BufferedReader reader;
  private int line_no;
  private String lastline_unconsumed;
  private Options options;

  public SensorConfigParser(String filename, Sensor sensor) throws FileNotFoundException
  {
    FileReader inputStream = new FileReader(filename);
    reader = new BufferedReader(inputStream);

    this.sensor = sensor;

    fields = new ArrayList<Field>();
  }

  public SensorConfigParser(BufferedReader reader, Sensor sensor)
  {
    this.reader = reader;

    this.sensor = sensor;

    fields = new ArrayList<Field>();
  }

  public void parse() throws IOException, ParserException
  {
    line_no = 1;

    parseOptions();

    if( !options.isFixedLength() && !options.isOptionalLength() )
    {
      log.info(options.debug());
    }

    parseFixed();
    if( options.isOptionalLength() )
    {
      String x = options.getOption(Options.OPTIONAL_FIELDS);
      if( x == null )
        throw new ParserException(
            "When specifing length to be optional it is required to specify the optional fields using '" + Options.OPTIONAL_FIELDS + "'");

      // System.out.println(Arrays.toString(x.split(",")));
      for(String idx : x.split(","))
      {
        int i = Integer.parseInt(idx.trim());
        fields.get(i).getOptions().setOptional(true);
        // System.out.println(idx + " " + i + " " + fields.get(i).name + " is optional");
      }
    }

    parseSessionInsert();

    parseExtraCode();

    parseSessionDataValues();

    parseSessionDataSession();

    Field[] ret = new Field[this.fields.size()];
    ret = fields.toArray(ret);
    this.sensor.setFields(ret);

    this.sensor.setOptions(options);
  }

  private void parseOptions() throws IOException, ParserException
  {
    ArrayList<String> opts = new ArrayList<String>();
    String line;
    int start = line_no;

    while( (line = reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      opts.add(line);
      line_no++;
    }

    this.options = new Options(start, opts);
  }

  /**
   * Parse fields of this sensor for fixed and optional length
   * TODO: refactor this. As it is used for optional length as well
   *
   * @throws IOException
   * @throws ParserException
   */
  private void parseFixed() throws IOException, ParserException
  {
    Block b = parseMandatoryBlock("fields:", "Fields definition");
    log.debug(b.toString());

    int blno = b.startline; //block line number
    int no = 0;
    for(String line : b.lines)
    {
      String[] def = line.split("\t");
      if( def.length < 3 )
        throw new ParserException("Error in line " + blno + ": Expected <name> <dbtype> <rawtype> <options>");
      Field f = new Field();
      f.number = no++;
      f.name = def[0];

      f.db_type = Type.fromString(def[1]);
      f.raw_type = Type.fromString(def[2]);

      if( def.length >= 4 )
      {
        f.options = new Field.Options();
        String[] opts = def[3].split(",");
        for(String opt : opts)
        {
          f.options.addOption(opt);
        }
      }

      fields.add(f);
    }

    Field[] ret = new Field[this.fields.size()];
    ret = fields.toArray(ret);
    this.sensor.setFields(ret);
  }

  private Block parseMandatoryBlock(String start_token, String name) throws IOException, ParserException
  {
    String line;
    ArrayList<String> lines = new ArrayList<String>();
    int start, end;

    if( lastline_unconsumed != null )
    {
      line = lastline_unconsumed;
      lastline_unconsumed = null;
    }
    else
    {
      while( (line = reader.readLine()) != null && !line.equals(start_token) )
      {
        line_no++;
      }
    }

    if( line == null || !line.equals(start_token) )
      throw new ParserException("Could not find mandatory block " + name);

    start = line_no;

    while( (line = reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      lines.add(line);
      line_no++;
    }

    end = line_no;

    return new Block(start, end, lines);
  }

  private Block parseOptionalBlock(String start_token, String name) throws IOException
  {
    String line;
    ArrayList<String> lines = new ArrayList<String>();
    int start, end;

    if( lastline_unconsumed != null )
    {
      if( lastline_unconsumed.equals(start_token) )
      {
        lastline_unconsumed = null;
      }
      else
        return null;
    }
    else
    {
      if( (line = reader.readLine()) == null || !line.equals(start_token) )
      {
        lastline_unconsumed = line;
        return null;
      }

      line_no++;
    }

    start = line_no;

    while( (line = reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      lines.add(line);
      line_no++;
    }

    end = line_no;

    return new Block(start, end, lines);
  }

  private void parseSessionInsert() throws IOException
  {
    Block b = parseOptionalBlock("session insert:", "code executed when session insert is =1");
    if( b != null )
    {
      BlockSessionInsert sb = new BlockSessionInsert();
      sb.lines = b.lines;
      this.sensor.setSessionInsertBlock(sb);
    }
  }

  private void parseExtraCode() throws IOException
  {
    Block b = parseOptionalBlock("extra code:", "extra code to be added");
    if( b != null )
    {
      BlockSessionInsert sb = new BlockSessionInsert();
      sb.lines = b.lines;
      this.sensor.setExtraCode(sb);
    }
  }

  private void parseSessionDataValues() throws IOException
  {
    Block b = parseOptionalBlock("session data database:", "Setting session values using transmitted values");
    if( b != null )
    {
      FieldDb[] fields = new FieldDb[b.lines.length];
      int i = 0;
      for(String line : b.lines)
      {
        String[] def = line.split("\t");
        FieldDb f = new FieldDb();
        f.idx = Integer.parseInt(def[0]);
        f.number = lookupDbField(def[1]);
        fields[i] = f;
        i++;
      }
      this.sensor.setFieldsDb(fields);
    }
    else
    {
      this.sensor.setFieldsDb(new FieldDb[0]);
    }
  }

  private void parseSessionDataSession() throws IOException
  {
    Block b = parseOptionalBlock("session data code:", "Setting session values using code");
    if( b != null )
    {
      FieldSession[] fields = new FieldSession[b.lines.length];
      int i = 0;
      for(String line : b.lines)
      {
        String[] def = line.split("\t");
        FieldSession f = new FieldSession();
        f.idx = Integer.parseInt(def[0]);
        f.text = def[1];
        fields[i] = f;
        i++;
      }
      this.sensor.setSessionFieldsCode(fields);
    }
    else
    {
      this.sensor.setSessionFieldsCode(new FieldSession[0]);
    }
  }

  private int lookupDbField(String name)
  {
    int i = 0;

    for(Field f : fields)
    {
      if( f.name.equals(name) )
        return i;
      i++;
    }

    return -1;
  }
}
