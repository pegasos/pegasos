package at.pegasos.generator.configurator.util;

import at.pegasos.generator.configurator.button.*;
import javafx.collections.*;
import javafx.scene.control.*;

public class RemoveColumn<T> extends TableColumn<T, Object> {
  public RemoveColumn(ObservableList<T> entries)
  {
    super();
    setCellFactory(column -> {
      ButtonTableCell<T, Object> cell = new RemoveButtonTableCell<>();
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        entries.remove(index);
      });
      return cell;
    });
  }
}
