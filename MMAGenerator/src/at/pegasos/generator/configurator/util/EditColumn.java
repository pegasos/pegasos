package at.pegasos.generator.configurator.util;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.parser.appparser.token.menu.*;
import at.pegasos.generator.parser.appparser.token.menu.MenuItem;
import javafx.collections.*;
import javafx.scene.control.*;

import java.util.*;
import java.util.function.*;

public class EditColumn<T> extends TableColumn<T, Object> {
  @FunctionalInterface
  public interface Callback<T> {
    void clicked(ObservableList<T> entries, int index, T entry);
  }
  public EditColumn(ObservableList<T> entries, Callback<T> callback)
  {
    super();
    setCellFactory(column -> {
    ButtonTableCell<T, Object> cell= new EditButtonTableCell<T, Object>();

    cell.getButton().setOnAction(event -> {
      int index = cell.getIndex();
      T entry= entries.get(index);
      callback.clicked(entries, index, entry);
    });

    return cell;
  });
  }
}
