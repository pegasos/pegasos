package at.pegasos.generator.configurator.writer;

import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Activity;

public class ActivitiesWriter extends Writer {

  private Activity[] activities;

  public ActivitiesWriter(Document document, Element root, Activity[] activities)
  {
    super(document, root);
    this.activities= activities;
  }

  public void write()
  {
    Element cfg= document.createElement("activities");

    if( activities != null )
    {
      for(Activity value : activities)
      {
        Element param= document.createElement("activity");

        param.setAttribute("id", value.id);
        param.setAttribute("class", value.clasz);

        cfg.appendChild(param);
      }
    }

    root.appendChild(cfg);
  }
}
