package at.pegasos.generator.configurator.writer;

import java.util.List;

import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Sensor;
import at.pegasos.generator.parser.appparser.token.Sensor.Clasz;

public class SensorWriter extends Writer {

  private List<Sensor> sensors;

  public SensorWriter(Document document, Element root, List<Sensor> sensors)
  {
    super(document, root);
    this.sensors= sensors;
  }

  public void write()
  {
    Element sensorsTag= document.createElement("sensors");
    for(Sensor sens : sensors)
    {
      Element sensor= document.createElement("sensor");
      
      if( sens.isBuiltin() )
      {
        sensor.setAttribute("builtin", "true");
        sensor.setAttribute("name", sens.name);
      }
      else
      {
        Element elem= document.createElement("name");
        elem.appendChild(document.createTextNode(sens.name));
        sensor.appendChild(elem);

        if( sens.icon != null )
        {
          elem= document.createElement("icon");
          elem.appendChild(document.createTextNode(sens.icon));
          sensor.appendChild(elem);
        }

        if( sens.classes != null )
        {
          for(Clasz clasz : sens.classes)
          {
            elem= document.createElement("class");
            elem.appendChild(document.createTextNode(clasz.clasz));

            switch( clasz.type )
            {
              case Ant:
                elem.setAttribute("type", "ant");
                break;
              case Ble:
                elem.setAttribute("type", "ble");
                break;
              case Other:
                elem.setAttribute("type", "other");
                break;
              default:
                break;
            }

            sensor.appendChild(elem);
          }
        }
      }
      
      sensorsTag.appendChild(sensor);
    }
    
    root.appendChild(sensorsTag);
  }
}
