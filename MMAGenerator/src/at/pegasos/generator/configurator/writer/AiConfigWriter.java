package at.pegasos.generator.configurator.writer;

import java.io.IOException;
import java.nio.file.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import at.pegasos.generator.parser.aiparser.token.*;

public class AiConfigWriter extends Writer {
  
  private Path path;
  private FeedbackModules feedbackModules;
  private FeedbackModules postProcessingModules;
  
  public AiConfigWriter(Path path, FeedbackModules feedbackModlues, FeedbackModules postProcessingModules)
  {
    super(null, null);
    this.path= path;

    this.feedbackModules= feedbackModlues;
    this.postProcessingModules= postProcessingModules;
  }

  public void write() throws WriterException
  {
    /*
    <?xml version="1.0" encoding="UTF-8"?>
-<ai>
-<feedbackmodules>
-</feedbackmodules>
-
-<postprocessing>
-       <!-- <module param0="1" param1="1">at.univie.mma.ai.postprocessing.TcxExport</module>
-       <module param0="1" param1="5">at.univie.mma.ai.postprocessing.TcxExport</module>
-       <module param0="1" param1="7">at.univie.mma.ai.postprocessing.TcxExport</module> -->
-       
-       <module param0="9" param1="5" param2="1">at.univie.mma.ai.postprocessing.HRVParams</module>
-       
-  <module param0="1">at.fovex.ai.post.MetricsComputer</module>
-  <module param0="1">at.fovex.ai.post.StravaUpload</module>
-  <module param0="9" param1="6">at.fovex.ai.post.LSCTMetrics</module>
-       
-       <!-- <module param0="1" param1="3">at.dobiasch.fovex.ai.postprocessing.PerpotCalibration</module> -->
-</postprocessing>
-</ai>

     */

    if( feedbackModules.modules.size() == 0 && postProcessingModules.modules.size() == 0 )
      return;

    try
    {
      start();
      
      if( feedbackModules.modules.size() > 0 )
      {
        Element elem= document.createElement("feedbackmodules");
        for(FeedbackModule module : feedbackModules.modules)
        {
          Element e= document.createElement("module");
          if( module.param0 != -1 )
            e.setAttribute("param0", "" + module.param0);
          if( module.param1 != -1 )
            e.setAttribute("param1", "" + module.param1);
          if( module.param2 != -1 )
            e.setAttribute("param2", "" + module.param2);
          e.appendChild(document.createTextNode(module.clasz));
          elem.appendChild(e);
        }
        root.appendChild(elem);
      }
      
      if( postProcessingModules.modules.size() > 0 )
      {
        Element elem= document.createElement("postprocessing");
        for(FeedbackModule module : postProcessingModules.modules)
        {
          Element e= document.createElement("module");
          if( module.param0 != -1 )
            e.setAttribute("param0", "" + module.param0);
          if( module.param1 != -1 )
            e.setAttribute("param1", "" + module.param1);
          if( module.param2 != -1 )
            e.setAttribute("param2", "" + module.param2);
          e.appendChild(document.createTextNode(module.clasz));
          elem.appendChild(e);
        }
        root.appendChild(elem);
      }

      finish();
    }
    catch( ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
  
  private void start() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
    document= documentBuilder.newDocument();
    
    // root element
    root= document.createElement("ai");
    document.appendChild(root);
  }
  
  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);

      Path p= path.resolve("ai.xml");
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult= new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException| IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
}
