package at.pegasos.generator.configurator.writer;

import java.nio.file.Path;
import java.util.Collection;

import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Screen;

public class ScreensWriter extends Writer {

  private Collection<Screen> screens;
  private Path path;

  public ScreensWriter(Document document, Element root, Collection<Screen> screens, Path path)
  {
    super(document, root);
    this.screens= screens;
    this.path= path;
  }

  public void write() throws WriterException
  {
    if( screens.size() > 0 )
    {
      Element cfg= document.createElement("Screens");

      for(Screen value : screens)
      {
        ScreenWriter writer= new ScreenWriter(document, cfg, path, value);
        writer.write();
      }

      root.appendChild(cfg);
    }
  }
}
