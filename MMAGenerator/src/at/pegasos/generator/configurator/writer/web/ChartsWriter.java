package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.util.*;

public class ChartsWriter extends Writer {

  private final Collection<Charts.Chart> charts;

  public ChartsWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.charts = instance.getWeb().charts.getCharts();
  }

  public void write()
  {
    if( charts.size() == 0 )
      return;

    Element eCharts = document.createElement("charts");

    for(Charts.Chart chart : charts)
    {
      Element e = document.createElement("chart");
      chart.write(document, e);
      if( !chart.default_avail )
      {
        Element node = document.createElement("default");
        node.appendChild(document.createTextNode("false"));
        e.appendChild(node);
      }
      eCharts.appendChild(e);
    }

    root.appendChild(eCharts);
  }
}
