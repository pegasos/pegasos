package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.util.*;
import java.util.stream.*;

public class ActivityNamesWriter extends Writer {

  private List<ActivityNames.Activity> names;

  public ActivityNamesWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.names = instance.getWeb().activitynames.getAllActivities().stream()
        .filter(a -> !a.dummy).collect(Collectors.toList());
  }

  public void write()
  {
    boolean added = false;
    Element mods= document.createElement("names");

    for(ActivityNames.Activity name : names)
    {
      Element mod= document.createElement("name");

      if( name.param0 != -1 )
        mod.setAttribute("param0", String.valueOf(name.param0));
      if( name.param1 != -1 )
        mod.setAttribute("param1", String.valueOf(name.param1));
      if( name.param2 != -1 )
        mod.setAttribute("param2", String.valueOf(name.param2));
      mod.appendChild(document.createTextNode(name.name));

      mods.appendChild(mod);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
