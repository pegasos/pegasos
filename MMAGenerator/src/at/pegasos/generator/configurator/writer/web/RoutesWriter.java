package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
public class RoutesWriter extends Writer {

  private final List<Route> routes;

  public RoutesWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.routes = instance.getWeb().routes;
  }

  public void write()
  {
    boolean added = false;
    Element mods = document.createElement("routes");

    for(Route entry : routes)
    {
      log.info("Write: {} {}", entry, entry.getClass().getCanonicalName());
      Element e = document.createElement("route");

      e.setAttribute("path", entry.path);
      if( entry instanceof ComponentRoute )
      {
        e.setAttribute("component", ((ComponentRoute) entry).component);
      }
      else if( entry instanceof ModuleRoute )
      {
        e.setAttribute("module", ((ModuleRoute) entry).module);
      }

      if( entry.guards.size() > 0 )
      {
        e.setAttribute("canActivate", String.join(",", entry.guards));
      }

      mods.appendChild(e);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
