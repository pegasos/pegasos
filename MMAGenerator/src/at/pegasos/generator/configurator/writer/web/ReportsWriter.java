package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.webparser.token.reports.*;

public class ReportsWriter extends Writer {
  private final Collection<Report> reports;

  @SuppressWarnings("unchecked")
  public ReportsWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    if( instance.getWeb().get("reports") == null )
    {
      reports = Collections.emptyList();
    }
    else
    {
      reports = (Collection<Report>) instance.getWeb().get("reports");
    }
  }

  public void write()
  {
    boolean added = false;
    Element mods = document.createElement("reports");

    for(Report entry : reports)
    {
      Element e = document.createElement("report");

      entry.write(document, e);

      mods.appendChild(e);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
