package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.webparser.token.*;

public class MenuWriter extends Writer {

  private final Collection<MenuEntry> menuEntries;

  public MenuWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.menuEntries = instance.getWeb().menu;
  }

  public void write()
  {
    boolean added = false;
    Element mods = document.createElement("menu");

    for(MenuEntry entry : menuEntries)
    {
      Element e = document.createElement("entry");

      e.setAttribute("name", entry.name);
      e.setAttribute("action", entry.action);
      if( entry.order != MenuEntry.DEFAULT_ORDER )
        e.setAttribute("order", String.valueOf(entry.order));
      if( entry.isPublic != null )
        e.setAttribute("public", String.valueOf(entry.isPublic));
      e.setAttribute("requiredRoles", String.join(",", entry.requiredRoles));

      mods.appendChild(e);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
