package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.webparser.token.*;

public class ModulesWriter extends Writer {

  private final Modules modules;

  public ModulesWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.modules = instance.getWeb().modules;
  }

  public void write()
  {
    boolean added = false;
    Element mods= document.createElement("modules");

    for(Modules.Module module : modules.entries())
    {
      if( !module.isDefault() )
      {
        Element mod= document.createElement("module");

        mod.setAttribute("name", module.name);
        mod.setAttribute("path", module.location.toString());
        mod.setAttribute("file", module.file);

        mods.appendChild(mod);

        added = true;
      }
    }

    if( added )
      root.appendChild(mods);
  }
}
