package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.util.*;

public class ExportsWriter extends Writer {

  private final Exports exports;

  public ExportsWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    exports = instance.getWeb().exports;
  }

  public void write()
  {
    boolean added = false;
    Element mods = document.createElement("exports");

    for(Exports.ExportType type : exports.getTypes())
    {
      Element mod = document.createElement("type");

      mod.setAttribute("name", type.typeName);
      mod.setAttribute("class", type.className);

      mods.appendChild(mod);

      added = true;
    }

    /*.stream().filter(a -> !a.dummy).collect(Collectors.toList())*/
    List<Exports.ExportActivity> exps = exports.getAllExports();
    for(Exports.ExportActivity export : exps)
    {
      Element activity = document.createElement("name");

      if( export.param0 != -1 )
        activity.setAttribute("param0", String.valueOf(export.param0));
      if( export.param1 != -1 )
        activity.setAttribute("param1", String.valueOf(export.param1));
      if( export.param2 != -1 )
        activity.setAttribute("param2", String.valueOf(export.param2));

      for(Exports.Export exp : export.exports)
      {
        Element node = document.createElement("export");
        node.setAttribute("type", exp.typeName);
        activity.appendChild(node);
      }

      mods.appendChild(activity);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
