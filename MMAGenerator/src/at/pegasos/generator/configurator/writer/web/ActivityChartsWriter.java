package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.Activity;
import at.pegasos.util.*;
import org.w3c.dom.*;

import java.util.*;

public class ActivityChartsWriter extends Writer {

  private final List<DAG.Node<Activity>> names;

  public ActivityChartsWriter(Document document, Element root, IInstance instance)
  {
    super(document, root);
    this.names = instance.getWeb().charts.getRoot().getAllChildren();
  }

  public void write()
  {
    boolean added = false;
    Element mods= document.createElement("activitycharts");

    for(DAG.Node<Activity> activityNode : names)
    {
      Activity activity = activityNode.getID();

      if( activity.chartsPre.size() == 0 && activity.chartsPost.size() == 0 )
        continue;

      Element mod= document.createElement("activity");

      if( activity.param0 != -1 )
        mod.setAttribute("param0", String.valueOf(activity.param0));
      if( activity.param1 != -1 )
        mod.setAttribute("param1", String.valueOf(activity.param1));
      if( activity.param2 != -1 )
        mod.setAttribute("param2", String.valueOf(activity.param2));

      if( activity.adddefault )
        mod.setAttribute("default", "true");
      if( activity.overwrite != null && activity.overwrite )
        mod.setAttribute("overwrite", "true");


      for(Activity.Chart chart : activity.chartsPre)
      {
        Element c = document.createElement("chart");
        if( chart.hasOrder )
          c.setAttribute("order", String.valueOf(chart.order));
        c.appendChild(document.createTextNode(chart.name));
        mod.appendChild(c);
      }

      for(Activity.Chart chart : activity.chartsPost)
      {
        Element c = document.createElement("chart");
        c.setAttribute("order", String.valueOf(chart.order));
        c.setAttribute("post", "true");
        c.appendChild(document.createTextNode(chart.name));
        mod.appendChild(c);
      }
      mods.appendChild(mod);

      added = true;
    }

    if( added )
      root.appendChild(mods);
  }
}
