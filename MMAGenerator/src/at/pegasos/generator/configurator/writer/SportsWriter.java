package at.pegasos.generator.configurator.writer;

import java.nio.file.Path;
import java.util.List;

import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Sport;

public class SportsWriter extends Writer {

  private List<Sport> sports;
  private Path path;

  public SportsWriter(Document document, Element root, List<Sport> sports, Path path)
  {
    super(document, root);
    this.sports= sports;
    this.path= path;
  }

  public void write() throws WriterException
  {
    Element cfg= document.createElement("sports");

    if( sports != null )
    {
      for(Sport value : sports)
      {
        Element param= document.createElement("sport");

        param.setAttribute("name", value.name);
        param.setAttribute("file", value.fileName);

        SportWriter writer= new SportWriter(path, value);
        writer.write();

        cfg.appendChild(param);
      }
    }

    root.appendChild(cfg);
  }
}
