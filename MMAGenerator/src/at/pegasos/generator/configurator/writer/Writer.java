package at.pegasos.generator.configurator.writer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class Writer {
  protected Element root;
  protected Document document;
  
  public Writer(Document document, Element root)
  {
    this.document= document;
    this.root= root;
  }
  
  public abstract void write() throws WriterException;
}
