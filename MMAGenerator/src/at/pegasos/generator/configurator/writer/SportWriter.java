package at.pegasos.generator.configurator.writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import at.pegasos.generator.parser.appparser.token.Sport;

public class SportWriter extends Writer {
  
  private Path path;
  private Sport sport;
  
  public SportWriter(Path path, Sport sport)
  {
    super(null, null);
    this.path= path;
    this.sport= sport;
  }

  public void write() throws WriterException
  {
    try
    {
      start();
      
      Element elem= document.createElement("RequiredSensors");
      for(String name : sport.required_names)
      {
        Element e= document.createElement("sensor");
        e.setAttribute("name", name);
        elem.appendChild(e);
      }
      root.appendChild(elem);

      elem= document.createElement("OptionalSensors");
      for(String name : sport.optional_names)
      {
        Element e= document.createElement("sensor");
        e.setAttribute("name", name);
        elem.appendChild(e);
      }
      root.appendChild(elem);

      if( sport.icon != null )
      {
        elem= document.createElement("icon");
        elem.setAttribute("id", sport.icon.isId() ? "true" : "false");
        elem.appendChild(document.createTextNode(sport.icon.toString()));
        root.appendChild(elem);
      }

      ScreensWriter screens= new ScreensWriter(document, root, sport.screens, path);
      screens.write();

      finish();
    }
    catch( ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
  
  private void start() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
    document= documentBuilder.newDocument();
    
    // root element
    root= document.createElement("Sport");
    document.appendChild(root);
  }
  
  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);

      Path p= path.resolve(sport.fileName);
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult= new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException| IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
}
