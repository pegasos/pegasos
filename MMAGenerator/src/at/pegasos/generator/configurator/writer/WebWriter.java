package at.pegasos.generator.configurator.writer;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.webparser.*;

public class WebWriter {

  private static final String fn = "web.xml";
  private static final String elem = "web";

  /**
   * Instance of which the web will be written
   */
  private final IInstance instance;

  /**
   * Path where the instance configuration is located
   */
  private final Path path;

  private Element root;
  private Document document;

  public WebWriter(IInstance inst, Path path)
  {
    this.instance = inst;
    this.path = path;
  }

  public void write() throws WriterException
  {
    start();

    writeConfig();
    writeDynamicModules();

    finish();
  }

  private void writeDynamicModules()
  {
    // Load all parser classes
    List<Class<? extends PegasosParser>> parsersClasses = DynamicModulesLoader.fetchParsers(PegasosWebXMLParserInterface.class, PegasosWebParser.class,
            WebParser.class.getPackage().getName());

    parsersClasses.stream()
            // Filter for parsers having a configurator
            .filter(c -> c.getAnnotationsByType(PegasosWebParser.class)[0].writer() != PegasosWebParser.DEFAULT_WRITER.class)
            // Sort the writers according to their order --> ensures that xml is the same when no changes happened (if order is unique)
            .sorted((a, b) -> {
              PegasosWebParser an = a.getAnnotationsByType(PegasosWebParser.class)[0];
              PegasosWebParser bn = b.getAnnotationsByType(PegasosWebParser.class)[0];
              return an.order() - bn.order();
            })
            // write
            .forEach(c -> {
              PegasosWebParser an = c.getAnnotationsByType(PegasosWebParser.class)[0];
              try
              {
                Writer writer = an.writer().getConstructor(Document.class, Element.class, IInstance.class).newInstance(document, root, instance);
                writer.write();
              } catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | WriterException e )
              {
                e.printStackTrace();
              }
            });
  }

  private void start() throws WriterException
  {
    try
    {
      DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
      document = documentBuilder.newDocument();

      // root element
      root = document.createElement(elem);
      document.appendChild(root);
    } catch( ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }

  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource = new DOMSource(document);

      Path p = path.resolve(fn);
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult = new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    } catch( TransformerException | IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }

  private void writeConfig()
  {
    Config config = instance instanceof ServerInstance ? ((ServerInstance) instance).config : ((Application) instance).getWeb().config;
    if( config.hasValues() )
    {
      ConfigWriter configWriter = new ConfigWriter(document, root, config);
      configWriter.write();
    }
  }
}
