package at.pegasos.generator.configurator.writer;

import at.pegasos.generator.parser.*;
import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Config;

public class ConfigWriter extends Writer {

  private final Config config;

  public ConfigWriter(Document document, Element root, Config config)
  {
    super(document, root);
    this.config= config;
  }

  public void write()
  {
    Element cfg= document.createElement("config");
    
    for(String value : config.getValueNames())
    {
      Element param= document.createElement("param");
      
      param.setAttribute("name", value);
      
      if( config.isBool(value) )
        param.setAttribute("type", "bool");
      else if( config.isString(value) )
        param.setAttribute("type", "string");
      else
        param.setAttribute("type", "int");
      
      try
      {
        param.setAttribute("value", config.getValue(value));
      }
      catch( DOMException | ParserException e )
      {
        e.printStackTrace();
      }
      
      cfg.appendChild(param);
    }
    
    root.appendChild(cfg);
  }
}
