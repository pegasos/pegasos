package at.pegasos.generator.configurator.writer;

import java.util.HashMap;
import java.util.Map.Entry;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.appparser.token.Menu;
import at.pegasos.generator.parser.appparser.token.menu.*;
import at.pegasos.generator.parser.token.EntryString;

public class MenuWriter extends Writer {
  
  private Application app;
  
  public MenuWriter(Document document, Element root, Application app)
  {
    super(document, root);
    this.app= app;
  }
  
  public void write() throws WriterException
  {
    if( app.main != null && app.main != Menu.EmptyMenu )
      writeMenu(app.main);
    for(Menu menu : app.getMenus().values())
    {
      writeMenu(menu);
    }
  }

  private void writeMenu(Menu menu)
  {
    Element men= document.createElement("menu");
    
    if( menu == app.main )
      men.setAttribute("main", "true");
    else
      men.setAttribute("name", menu.name);
    
    for( MenuItem entry : menu.items )
    {
      Element en= document.createElement("entry");
      Element e;
      
      if( entry.icon != null && entry.icon.raw() != null )
      {
        e= document.createElement("icon");
        if( entry.icon.isId() )
          e.setAttribute("id", "true");
        e.appendChild(document.createTextNode(entry.icon.raw()));
        en.appendChild(e);
      }

      if( entry.text != null && !entry.text.raw().equals("") )
      {
        e= document.createElement("text");
        if( entry.text.isId() )
          e.setAttribute("id", "true");
        e.appendChild(document.createTextNode(entry.text.raw()));
        en.appendChild(e);
      }

      if( entry instanceof MenuItemActivity )
      {
        en.setAttribute("type", "Activity");
        MenuItemActivity a= (MenuItemActivity) entry;
        
        e= document.createElement("activity");
        e.appendChild(document.createTextNode(a.activity.toString()));
        en.appendChild(e);
        
        if( a.sport != null )
        {
          e= document.createElement("sport");
          e.appendChild(document.createTextNode(a.sport));
          en.appendChild(e);
        }
        
        if( a.auto_start )
        {
          e= document.createElement("autostart");
          e.appendChild(document.createTextNode("true"));
          en.appendChild(e);
        }
        
        if( a.distance != null )
        {
          e= document.createElement("distance");
          if( a.fixed_distance )
          {
            e.setAttribute("fixed", "true");
            e.appendChild(document.createTextNode(a.distance.toString()));
          }
          en.appendChild(e);
        }
        
        if( a.callback != null && !a.callback.equals("") )
        {
          e= document.createElement("callback");
          if( a.callback_class )
          {
            e.setAttribute("class", "true");
          }
          e.appendChild(document.createTextNode(a.callback));
          en.appendChild(e);
          /*
        if( e.getElementsByTagName("callback").getLength() != 0 )
        {
          ((MenuItemActivity) m).callback= e.getElementsByTagName("callback").item(0).getChildNodes().item(0).getNodeValue();
          Element ee= (Element) e.getElementsByTagName("callback").item(0);
          if( !ee.getAttribute("class").equals("") && ee.getAttribute("class").toLowerCase().equals("true") )
          {
            ((MenuItemActivity) m).callback_class= true;
          }
        }
           */
        }
        
        if( a.parameters != null )
        {
          e= document.createElement("parameters");
          e.appendChild(document.createTextNode(a.parameters));
          en.appendChild(e);
        }
        
        if( a.qvalues.size() > 0 )
        {
          e= en;
          if( a.qvalues.size() > 1 )
          {
            e= document.createElement("queries");
            en.appendChild(e);
          }
          
          for(HashMap<String, EntryString> query : a.qvalues)
          {
            Element q= document.createElement("query");

            String type= query.get("_Type").raw();
            q.setAttribute("type", type);
            
            Element p= document.createElement("name");
            p.appendChild(document.createTextNode(query.get("Name").raw()));
            q.appendChild(p);
            
            EntryString title= query.get("Title");
            p= document.createElement("title");
            if( title.isId() )
              p.setAttribute("id", "true");
            p.appendChild(document.createTextNode(title.raw()));
            q.appendChild(p);
            
            for(Entry<String, EntryString> params : query.entrySet())
            {
              if( params.getKey().equals("_Callback") || params.getKey().equals("_Type") || params.getKey().equals("Name")|| params.getKey().equals("Title") )
                continue;
              
              String key= params.getKey();
              if( type.toLowerCase().equals("list") )
              {
                if( key.equals("List") )
                {
                  key= "list";
                }
                else if( key.equals("Label") )
                {
                  key= "label";
                }
              }
              else if( type.toLowerCase().equals("filelist") )
              {
                if( key.equals("Label") )
                {
                  key= "label";
                }
                else if( key.equals("Directory") )
                {
                  key= "directory";
                }
                else if( key.equals("Pattern") )
                {
                  key= "pattern";
                }
              }
              
              p= document.createElement(key);
              if( params.getValue().isId() )
                p.setAttribute("id", "true");
              p.appendChild(document.createTextNode(params.getValue().raw()));
              q.appendChild(p);
            }

            e.appendChild(q);
          }
        }
      }
      else if( entry instanceof MenuItemIntent )
      {
        MenuItemIntent intent= (MenuItemIntent) entry;
        en.setAttribute("type", "Intent");

        e= document.createElement("intent");
        e.appendChild(document.createTextNode(intent.intent));
        en.appendChild(e);
        
        if( intent.params.size() > 0 )
        {
          for(Entry<String, String> param : intent.params.entrySet())
          {
            Element p= document.createElement("param");
            p.setAttribute("name", param.getKey());
            p.appendChild(document.createTextNode(param.getValue()));
            en.appendChild(p);
          }
        }
      }
      else if( entry instanceof MenuItemMenu )
      {
        en.setAttribute("type", "Menu");

        e= document.createElement("name");
        e.appendChild(document.createTextNode(((MenuItemMenu) entry).menu));
        en.appendChild(e);
      }
      men.appendChild(en);
    }
    
    root.appendChild(men);
  }
}
