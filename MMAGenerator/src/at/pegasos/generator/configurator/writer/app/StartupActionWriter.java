package at.pegasos.generator.configurator.writer.app;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

public class StartupActionWriter extends Writer {
  private final Application inst;

  public StartupActionWriter(Document document, Element root, Application instance)
  {
    super(document, root);
    this.inst = instance;
  }

  @Override public void write() throws WriterException
  {
    if( inst.startup_actions.size() > 0 )
    {
      Element libs= document.createElement("startup");
      int written = 0;
      for(OwnedString lib : inst.startup_actions )
      {
        if( lib.owner != inst )
          continue;

        Element elem= document.createElement("action");
        elem.appendChild(document.createTextNode(lib.string));
        libs.appendChild(elem);
        written++;
      }
      if( written > 0 )
        root.appendChild(libs);
    }
  }
}
