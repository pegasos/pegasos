package at.pegasos.generator.configurator.writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import at.pegasos.generator.parser.appparser.token.Application;

public class ManifestWriter extends Writer {
  
  private Path path;
  private Application app;
  
  public ManifestWriter(Path path, Application app)
  {
    super(null, null);
    this.path= path;
    this.app= app;
  }

  public void write() throws WriterException
  {
    if( app.manifest.application.size() == 0 && app.manifest.root.size() == 0 )
      return;

    try
    {
      start();
      
      if( app.manifest.application.size() > 0 )
      {
        Element application= document.createElement("application");

        for(Node n : app.manifest.application )
        {
          Node x= document.importNode(n, true);
          // System.out.println("Node: " + x.getNodeName() + " " + x.getNodeType() + " '" + x.getNodeValue() + "'");
          if( x.getNodeType() != Node.TEXT_NODE )
            application.appendChild(x);
        }

        root.appendChild(application);
      }
      
      for(Node n : app.manifest.root )
      {
        Node x= document.importNode(n, true);
        if( x.getNodeType() != Node.TEXT_NODE )
          root.appendChild(x);
      }

      finish();
    }
    catch( ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
  
  private void start() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
    document= documentBuilder.newDocument();
    
    // root element
    root= document.createElement("manifest");
    root.setAttribute("xmlns:android", "http://schemas.android.com/apk/res/android");
    document.appendChild(root);
  }
  
  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      // transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);

      Path p= path.resolve("AndroidManifest.xml");
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult= new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException| IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
}
