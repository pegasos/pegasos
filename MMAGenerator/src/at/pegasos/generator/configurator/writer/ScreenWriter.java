package at.pegasos.generator.configurator.writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map.Entry;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

import at.pegasos.generator.parser.appparser.token.Screen;
import at.pegasos.generator.parser.appparser.token.screen.*;

public class ScreenWriter extends Writer {
  
  private final Path path;
  private final Screen screen;
  
  public ScreenWriter(Document document, Element root, Path path, Screen screen)
  {
    super(document, root);
    this.path= path;
    this.screen= screen;
  }
  
  public ScreenWriter(Path path, Screen screen)
  {
    super(null, null);
    this.path= path;
    this.screen= screen;
  }
  
  public void write() throws WriterException
  {
    boolean writeFile= false;
    Element scr;
    
    if( document == null )
    {
      try
      {
        start();
        writeFile= true;
      }
      catch( ParserConfigurationException e )
      {
        e.printStackTrace();
        throw new WriterException(e);
      }
      
      scr= root;
    }
    else
    {
      scr= document.createElement("Screen");
    }
    
    if( screen.name != null && !screen.name.equals("") )
    {
      scr.setAttribute("name", screen.name);
    }
    
    if( !screen.referenced && (writeFile || screen.fileName == null ) )
    {
      if( screen.clasz != null && !screen.clasz.equals("") )
      {
        scr.setAttribute("type", screen.clasz);
      }
      
      for(Fragment fragment : screen.fragments)
      {
        Element frag;
        if( fragment instanceof ValueFragment )
        {
          frag= document.createElement("Generate");
          ValueFragment vf= (ValueFragment) fragment;
          Element t= document.createElement("Value");
          t.setAttribute("name", vf.value);
          t.setAttribute("type", vf.value_type);
          frag.appendChild(t);
          
          if( vf.left_img != null )
          {
            t= document.createElement("Left");
            t.setAttribute("img", vf.left_img);
            frag.appendChild(t);
          }
          else
          {
            t= document.createElement("Left");
            t.setAttribute("txt", vf.left_txt);
            frag.appendChild(t);
          }
          if( vf.right_img != null )
          {
            t= document.createElement("Right");
            t.setAttribute("img", vf.right_img);
            frag.appendChild(t);
          }
          else
          {
            t= document.createElement("Right");
            t.setAttribute("txt", vf.right_txt);
            frag.appendChild(t);
          }

          writeParameters(fragment, frag);
        }
        else if( fragment == null )
        {
          frag = document.createElement("stretch");
        }
        else
        {
          frag= document.createElement("Java");
          if( fragment.clasz != null && !fragment.clasz.equals("") )
            frag.setAttribute("class", fragment.clasz);
          if( fragment.method != null && !fragment.method.equals("") )
            frag.setAttribute("method", fragment.method);
          
          writeParameters(fragment, frag);
        }
        scr.appendChild(frag);
      }
    }
    else if( screen.fileName != null && !writeFile )
    {
      scr.setAttribute("file", screen.fileName);
      
      ScreenWriter writer= new ScreenWriter(path, screen);
      writer.write();
    }

    if( writeFile )
    {
      finish();
    }
    else
    {
      root.appendChild(scr);
    }
  }

  private void writeParameters(Fragment fragment, Element frag)
  {
    for( Entry<String, String> param : fragment.args.entrySet() )
    {
      // <parameter name="chron.desc">false</parameter>
      Element p= document.createElement("parameter");
      p.setAttribute("name", param.getKey());
      p.appendChild(document.createTextNode(param.getValue()));
      frag.appendChild(p);
    }
  }

  private void start() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
    document= documentBuilder.newDocument();
    
    // root element
    root= document.createElement("Screen");
    document.appendChild(root);
  }

  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);
      
      Path p= path.resolve(screen.fileName);
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }
    
      StreamResult streamResult= new StreamResult(p.toFile());
    
      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException| IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
}
