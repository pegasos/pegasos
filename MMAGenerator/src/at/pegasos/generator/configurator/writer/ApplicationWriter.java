package at.pegasos.generator.configurator.writer;

import at.pegasos.generator.configurator.writer.app.*;
import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.io.*;
import java.nio.file.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import at.pegasos.generator.parser.appparser.token.*;

public class ApplicationWriter {
  
  /**
   * Application to be written
   */
  private final Application app;

  /**
   * Path where the app should be written to
   */
  private final Path path;
  
  private final String fn;
  
  private final String elem;
  
  private Element root;
  private Document document;

  public ApplicationWriter(Application app, boolean library, Path path)
  {
    this.app= app;
//    this.library= library;
    this.path= path;
    if( library )
    {
      fn= "library.xml";
      elem= "library";
    }
    else
    {
      fn= "application.xml";
      elem= "application";
    }
  }
  
  public void write() throws WriterException
  {
    // logger.debug("Parsing " + (library ? "Library" : "Application") + " " + app.name);
    
    start();
    
    writeBasicInfo();
    writeServers();
    writeLibraries();
    writeAndroidDependencies();
    writeConfig();
    StartupActionWriter writer = new StartupActionWriter(document, root, app);
    writer.write();
    writeMenus();
    writeSensors();
    
    ScreensWriter screens= new ScreensWriter(document, root, app.getScreens(), path);
    screens.write();
    
    writeSports();
    writeActivities();
    writeManifest();
    
    writeVersion();
    
    writeAiConfig();
    
    finish();

    writeWeb();
  }
  
  private void start() throws WriterException
  {
    try
    {
      DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
      DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
      document= documentBuilder.newDocument();
      
      // root element
      root= document.createElement(elem);
      document.appendChild(root);
    }
    catch( ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
  
  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);

      Path p= path.resolve(fn);
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult= new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException| IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
  
  private void writeBasicInfo()
  {
    Element name= document.createElement("name");
    name.appendChild(document.createTextNode(app.name));
    root.appendChild(name);

    if( (app.getGitRootValue() != null && !app.getGitRootValue().equals("")) || 
        (app.getGitBranchValue() != null && !app.getGitBranchValue().equals("" ) ) )
    {
      Element git= document.createElement("git");
      if( app.getGitRootValue() != null && !app.getGitRootValue().equals("") )
      {
        Element elem= document.createElement("repository");
        elem.appendChild(document.createTextNode(app.getGitRootValue()));
        git.appendChild(elem);
      }
      if( app.getGitBranchValue() != null && !app.getGitBranchValue().equals("") )
      {
        Element elem= document.createElement("branch");
        elem.appendChild(document.createTextNode(app.getGitBranchValue()));
        git.appendChild(elem);
      }
      root.appendChild(git);
    }
  }
  
  private void writeServers()
  {
    Element servers= document.createElement("servers");
    for(String server : app.servers )
    {
      Element elem= document.createElement("server");
      elem.appendChild(document.createTextNode(server));
      servers.appendChild(elem);
    }
    root.appendChild(servers);
  }
  
  private void writeLibraries()
  {
    if( app.getLibraries().size() > 0 )
    {
      Element libs= document.createElement("libraries");
      for(String lib : app.getLibraries() )
      {
        Element elem= document.createElement("library");
        elem.appendChild(document.createTextNode(lib));
        libs.appendChild(elem);
      }
      root.appendChild(libs);
    }
  }
  
  private void writeAndroidDependencies()
  {
    if( app.android_dependencies.size() > 0 || app.android_projects.size() > 0 || app.android_repositories.size() > 0 )
    {
      Element deps= document.createElement("dependencies");
      
      for(String proj : app.android_projects )
      {
        Element elem= document.createElement("project");
        elem.appendChild(document.createTextNode(proj));
        deps.appendChild(elem);
      }
      
      for(String dep : app.android_dependencies )
      {
        Element elem= document.createElement("dependency");
        elem.appendChild(document.createTextNode(dep));
        deps.appendChild(elem);
      }
      
      for(String rep : app.android_repositories )
      {
        Element elem= document.createElement("repository");
        elem.appendChild(document.createTextNode(rep));
        deps.appendChild(elem);
      }
      
      root.appendChild(deps);
    }
  }
  
  private void writeMenus() throws WriterException
  {
    MenuWriter writer= new MenuWriter(document, root, app);
    writer.write();
  }
  
  private void writeConfig()
  {
    ConfigWriter configWriter= new ConfigWriter(document, root, app.config);
    configWriter.write();
  }
  
  private void writeSensors()
  {
    SensorWriter writer= new SensorWriter(document, root, app.getSensors().getSensors());

    writer.write();
  }

  private void writeSports() throws WriterException
  {
    SportsWriter writer= new SportsWriter(document, root, app.getSports(), path);
    writer.write();
  }
  
  private void writeActivities()
  {
    ActivitiesWriter writer= new ActivitiesWriter(document, root, app.activities);
    writer.write();
  }
  
  private void writeManifest() throws WriterException
  {
    ManifestWriter writer= new ManifestWriter(path, app);
    writer.write();
  }

  private void writeWeb() throws WriterException
  {
    WebWriter writer = new WebWriter(app, path);
    writer.write();
  }

  private void writeVersion() throws WriterException
  {
    if( app.version != null )
    {
      try
      {
        BufferedWriter writer= Files.newBufferedWriter(path.resolve("version"));
        writer.write(app.version.code + "\n");
        writer.write(app.version.name + "\n");
        writer.close();
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new WriterException(e);
      }
    }
  }

  private void writeAiConfig() throws WriterException
  {
    AiConfigWriter writer= new AiConfigWriter(path, app.feedbackmodules, app.postprocessingmodules);
    writer.write();
  }
}
