package at.pegasos.generator.configurator.writer;

public class WriterException extends Exception {

  private static final long serialVersionUID= 6123449951835907590L;

  public WriterException(Exception e)
  {
    super(e);
  }
  
}
