package at.pegasos.generator.configurator;

import java.io.Serializable;
import java.nio.charset.Charset;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.layout.PatternLayout;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

public class Logoutput extends VBox {
  
  private class TextAreaAppender extends AbstractAppender {
    
    public TextAreaAppender(String name, Layout<? extends Serializable> layout)
    {
      super(name, null, layout, false, null);
    }
    
    /**
     * Format and then append the loggingEvent to the stored TextArea.
     *
     * @param loggingEvent
     */
    @Override
    public void append(final LogEvent loggingEvent)
    {
      // this.getLayout().encode(arg0, arg1);
      // final String message= this.layout.format(loggingEvent);
      // String message= loggingEvent.getMessage().getFormattedMessage();
      String message= new String(getLayout().toByteArray(loggingEvent));
//      System.out.println(loggingEvent.getLevel().intLevel() + " " + message);
      
      // Append formatted message to text area using the Thread.
      try
      {
        Platform.runLater(new Runnable() {
          @Override
          public void run()
          {
            // loggingEvent.getLevel();
            
            try
            {
              Text text= new Text(message + System.lineSeparator());
              out.getChildren().add(text);
              out.layout();
              scroll.setVvalue(1.0);
            }
            catch( final Throwable t )
            {
              System.out.println("Unable to append log to text area: " + t.getMessage());
            }
          }
        });
      }
      catch( final IllegalStateException e )
      {
        // ignore case when the platform hasn't yet been initialised
      }
    }
  }
  
  private TextFlow out;
  
  private TextAreaAppender appender;
  
  private ScrollPane scroll;
  
  public Logoutput()
  {
    Label lblOutput= new Label("Output:");
    getChildren().add(lblOutput);
    
//    this.setMaxHeight(100);
    
    /*
     * output= new TextArea(); output.setEditable(false); output.setWrapText(true);
     */
    
    out= new TextFlow();
    out.setTextAlignment(TextAlignment.LEFT);
    
    // Set Hgrow for TextField
    // HBox.setHgrow(output, Priority.ALWAYS);
    HBox.setHgrow(out, Priority.ALWAYS);
    // getChildren().add(output);
    scroll= new ScrollPane();
    HBox.setHgrow(scroll, Priority.ALWAYS);
    VBox.setVgrow(scroll, Priority.ALWAYS);
    scroll.setContent(out);
    scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
    getChildren().add(scroll);
    
    widthProperty().addListener((obs, oldVal, newVal) -> {
      // Do whatever you want
      out.setPrefWidth(newVal.doubleValue() - 10);
    });
    
    final LoggerContext context= LoggerContext.getContext(false);
    final Configuration config= context.getConfiguration();
    // final PatternLayout layout= PatternLayout.createDefaultLayout(config);
    /*ConfigurationBuilder<BuiltConfiguration> builder= ConfigurationBuilderFactory.newConfigurationBuilder();
    LayoutComponentBuilder standard= builder.newLayout("PatternLayout");
    standard.addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable");*/
    String pattern= "%date{HH:mm:ss.SSS} %-5level %logger{1} - %msg";
    final PatternLayout layout= PatternLayout.createLayout(pattern, null, null, null, Charset.defaultCharset(),false,false,null,null);
    appender= new TextAreaAppender("Test", layout);
    appender.start();
    config.addAppender(appender);
    final Level level= null;
    final Filter filter= null;
    /*for(final LoggerConfig loggerConfig : config.getLoggers().values())
    {
      loggerConfig.addAppender(appender, level, filter);
    }*/
    config.getRootLogger().addAppender(appender, level, filter);
  }
}
