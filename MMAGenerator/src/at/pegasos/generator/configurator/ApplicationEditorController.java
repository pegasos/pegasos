package at.pegasos.generator.configurator;

import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.*;
import at.pegasos.generator.parser.appparser.token.Application;
import javafx.application.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.control.ButtonBar.*;
import javafx.scene.control.TabPane.*;
import javafx.stage.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.prefs.*;

public class ApplicationEditorController implements Initializable {
  
  private final static String LAST_APP= "LAST_APP";
  private final ConfiguratorController configurationController;

  @FXML
  private TabPane tabs;
  @FXML
  private MenuItem saveMenu;
  
  private final Stage primaryStage;
  
  // private Path dirInput;
  
  private boolean modified;
  
  private Application app;

  private Tab root;
  
  public ApplicationEditorController(Stage primaryStage, ConfiguratorController configurationController)
  {
    this.primaryStage= primaryStage;
    this.configurationController = configurationController;
    
    // this.dirInput= Paths.get(".");
  }

  public void initialize(URL location, ResourceBundle res)
  {
    tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
  }

  /*
   * public void onSetDirectory() { DirectoryChooser chooser= new DirectoryChooser();
   * chooser.setTitle("JavaFX Projects"); // File defaultDirectory = new File("c:/dev/javafx"); //
   * chooser.setInitialDirectory(defaultDirectory); dirInput=
   * Paths.get(chooser.showDialog(primaryStage).getAbsolutePath()); }
   */
  
  public void onOpenApplication()
  {
    /*
     * FileChooser chooser= new FileChooser(); chooser.setTitle("Open Application"); // File
     * defaultDirectory = new File("c:/dev/javafx"); //
     * chooser.setInitialDirectory(defaultDirectory);
     * 
     * chooser.getExtensionFilters().addAll( new FileChooser.
     * 
     * File f= chooser.showOpenDialog(primaryStage);
     */

    if( modified )
    {
      if( !askSave() )
        return;
    }
    
    DirectoryChooser chooser= new DirectoryChooser();
    chooser.setTitle("Open Application");
    
    Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
    String dir= pref.get(LAST_APP, ".");
    
    chooser.setInitialDirectory(new File(dir));
    
    File f= chooser.showDialog(primaryStage);
    if( f != null )
    {
      openApp(f);
    }
  }

  /**
   * Ask the user if he/she wants to save the application
   * 
   * @return false if operation was aborted
   */
  public boolean askSave()
  {
    Alert alert= new Alert(AlertType.CONFIRMATION);
    alert.setTitle("Modified");
    alert.setHeaderText("The current application has been modified");
    alert.setContentText("Do you want to save?");
    
    ButtonType buttonTypeSave= new ButtonType("Save");
    ButtonType buttonTypeNo= new ButtonType("No");
    ButtonType buttonTypeCancel= new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
    
    alert.getButtonTypes().setAll(buttonTypeSave, buttonTypeNo, buttonTypeCancel);
    
    Optional<ButtonType> result= alert.showAndWait();
    if( result.get() == buttonTypeSave )
    {
      // ... user chose "Save"
      if( !onSaveApplication() )
        return false;
    }
    else if( result.get() == buttonTypeNo )
    {
      // ... user chose "No"
      return true;
    }
    else
    {
      // ... user chose CANCEL or closed the dialog
      return false;
    }
    
    return true;
  }
  
  public void setModified()
  {
    this.modified= true;
  }
  
  private void openApp(File f)
  {
    modified= false;
    
    Path path= Paths.get(f.getAbsolutePath());
    
    try
    {
      ApplicationParser parser= new ApplicationParser(path, false, false);
      parser.setLibraries(configurationController.getLibraries());
      parser.parse();
      app= parser.getApplication();
      
      displayApp();
    }
    catch( ParserException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Opening Application failed").showAndWait();
    }
    
    Preferences preferences= Preferences.userRoot().node(getClass().getCanonicalName());
    preferences.put(LAST_APP, f.toString());
  }
  
  public void setApplication(Tab root, Application app)
  {
    root.setOnCloseRequest(this::closingEvent);
    this.root= root;
    this.app= app;
    displayApp();
  }
  
  private void displayApp()
  {
    Platform.runLater(new Runnable() {
      
      @Override
      public void run()
      {
        while( tabs.getTabs().size() > 0 )
          tabs.getTabs().remove(0);
        
        tabs.getTabs().add(new BasicTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new VersionControllTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new ConfigurationTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new MenuTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new SensorsTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new SportsTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new ControllersTab(app, ApplicationEditorController.this));
        tabs.getTabs().add(new WebTab(app, ApplicationEditorController.this));
      }
    });
  }
  
  public boolean onSaveApplication()
  {
    if( app.directory == null )
    {
      DirectoryChooser chooser= new DirectoryChooser();
      chooser.setTitle("Open Application");
      
      Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
      String dir= pref.get(LAST_APP, ".");
      
      chooser.setInitialDirectory(new File(dir));
      
      File f= chooser.showDialog(primaryStage);
      if( f != null )
      {
//        configurationController.getProject().path.relativize(f)
        app.directory= Paths.get(f.getAbsolutePath());
        configurationController.getProject().open_apps.add(configurationController.getProject().path.getParent().relativize(f.toPath()).toString());
        configurationController.getProject().setModified();
      }
      else
        return false;
    }
    
    ApplicationWriter writer= new ApplicationWriter(app, false, app.directory);
    try
    {
      writer.write();

      modified = false;

      return true;
    }
    catch( Exception e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Saving failed").showAndWait();
    }
    
    return false;
  }

  private void closingEvent(Event event)
  {
    if( modified )
    {
      if( !askSave() )
      {
        event.consume();
        this.configurationController.getProject().closeApp(this.app);
      }
    }
    else
    {
      this.configurationController.getProject().closeApp(this.app);
    }
  }

  public boolean isModified()
  {
    return modified;
  }

  public void setName()
  {
    if( app.name != null && !app.name.isEmpty() )
    {
      root.setText(app.name);
    }
    else
      root.setText("<unnamed>");
  }
}