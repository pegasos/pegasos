package at.pegasos.generator.configurator;

import java.nio.file.*;
import java.util.prefs.Preferences;

import com.beust.jcommander.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.image.*;
import javafx.stage.*;

public class Configurator extends Application {

  public static class Arguments {
    public static Arguments inst;

    public Arguments()
    {
      inst = this;
    }

    @Parameter(description = "Input (what to import)", required = false)
    public String input;
  }

  private static final String WINDOW_POSITION_X= "Window_Position_X";
  private static final String WINDOW_POSITION_Y= "Window_Position_Y";
  private static final String WINDOW_WIDTH= "Window_Width";
  private static final String WINDOW_HEIGHT= "Window_Height";
  private static final double DEFAULT_X= 10;
  private static final double DEFAULT_Y= 10;
  private static final double DEFAULT_WIDTH= 800;
  private static final double DEFAULT_HEIGHT= 600;
  private static Stage primaryStage;

  @Override
  public void start(Stage primaryStage) throws Exception
  {
    Configurator.primaryStage= primaryStage;

    // Parent root= FXMLLoader.load(getClass().getResource("Configurator.fxml"));
    ConfiguratorController controller = new ConfiguratorController(primaryStage);

    FXMLLoader loader= new FXMLLoader();
    loader.setLocation(this.getClass().getResource("Configurator.fxml"));
    loader.setController(controller);

    Parent root= loader.load();
    primaryStage.setTitle("Pegasos Configurator");
    Scene secene= new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    primaryStage.setScene(secene);
    primaryStage.show();
    secene.getStylesheets().add("at/pegasos/generator/configurator/res/css/Main.css");

    Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
    double x= pref.getDouble(WINDOW_POSITION_X, DEFAULT_X);
    double y= pref.getDouble(WINDOW_POSITION_Y, DEFAULT_Y);
    double width= pref.getDouble(WINDOW_WIDTH, DEFAULT_WIDTH);
    double height= pref.getDouble(WINDOW_HEIGHT, DEFAULT_HEIGHT);
    primaryStage.setX(x);
    primaryStage.setY(y);
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);

    primaryStage.setTitle("Pegasos Configurator");
    String img = this.getClass().getResource("res/gfx/Configurator_scaled.png").getFile();
    img = Path.of(img).toUri().toString();
    System.out.println("Image: " + img);
    primaryStage.getIcons().add(new Image(img));

    primaryStage.setOnCloseRequest((final WindowEvent event) -> {
      Preferences preferences= Preferences.userRoot().node(getClass().getCanonicalName());
      preferences.putDouble(WINDOW_POSITION_X, primaryStage.getX());
      preferences.putDouble(WINDOW_POSITION_Y, primaryStage.getY());
      preferences.putDouble(WINDOW_WIDTH, primaryStage.getWidth());
      preferences.putDouble(WINDOW_HEIGHT, primaryStage.getHeight());
    });
  }

  public static Stage getPrimaryStage()
  {
    return primaryStage;
  }

  public static void main(String[] args)
  {
    at.pegasos.generator.Parameters parameters = new at.pegasos.generator.Parameters();
    at.pegasos.generator.Parameters.set(parameters);

    Arguments arguments = new Arguments();

    JCommander.newBuilder()
        .addObject(parameters)
        .addObject(arguments)
        .build()
        .parse(args);

    launch(args);
  }
}
