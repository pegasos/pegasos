package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.ResourceBundle;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;
import javafx.beans.value.*;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

public class VersionControllTabController extends EditTabController {
  
  @FXML
  CheckBox useDefaults;
  
  @FXML
  TextField repository;
  
  @FXML
  TextField branch;
  
  @FXML
  Region pane;
  
  public VersionControllTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);
  }
  
  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    boolean hasGit= (app.getGitRootValue() != null && !app.getGitRootValue().equals(""))
        || (app.getGitBranchValue() != null && !app.getGitBranchValue().equals(""));
    // System.out.println("hasGit: " + hasGit + " " + app.getGitRoot() + " " + app.getGitBranch());
    
    useDefaults.setSelected(!hasGit);
    if( hasGit )
    {
      if( app.getGitRootValue() != null )
        repository.setText(app.getGitRootValue());
      
      if( app.getGitBranchValue() != null )
        branch.setText(app.getGitBranchValue());
    }
    
    useDefaults.selectedProperty().addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
      {
        pane.setDisable(newValue);
        if( newValue )
        {
          app.setGitRoot(null);
          app.setGitBranch(null);
        }
        else
        {
          app.setGitRoot(repository.getText());
          app.setGitBranch(branch.getText());
        }
        mainController.setModified();
      }
    });
    
    repository.textProperty().addListener((obs, oldT, newT) -> {
      if( newT.equals("") )
        app.setGitRoot(null);
      else
        app.setGitRoot(newT);
      mainController.setModified();
    });
    
    branch.textProperty().addListener((obs, oldT, newT) -> {
      if( newT.equals("") )
        app.setGitBranch(null);
      else
        app.setGitBranch(newT);
      mainController.setModified();
    });
  }
}
