package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.ResourceBundle;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.appparser.token.Application;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.TextFieldTableCell;

public class BasicTabController extends EditTabController {
  
  @FXML
  TextField name;

  @FXML
  Button addServer;

  @FXML
  TableView<String> tblServers;

  @FXML
  TableColumn<String, String> nameCol;

  @FXML
  TableColumn<String, Object> removeCol;

  private final ObservableList<String> entries;

  public BasicTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    entries= FXCollections.observableList(this.app.servers);
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    name.setText(app.name);

 // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    addServer.getStyleClass().add("icon-add");
    addServer.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    addServer.setTooltip(tooltip);

    addServer.setOnAction(event -> {
      String e= "";
      entries.add(e);
      tblServers.scrollTo(e);
      mainController.setModified();
    });

    name.textProperty().addListener((obs, oldValue, newValue) -> {
      app.name= newValue;
      mainController.setName();
      mainController.setModified();
    });

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue()));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    nameCol.setOnEditCommit((CellEditEvent<String, String> event) -> {
      TablePosition<String, String> pos= event.getTablePosition();

      int row= pos.getRow();
      event.getTableView().getItems().set(row, event.getNewValue());
      this.app.servers= entries;
      mainController.setModified();
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<String, Object> cell= new RemoveButtonTableCell<String, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        entries.remove(index);
        this.app.servers= entries;
        mainController.setModified();
      });

      return cell;
    });

    tblServers.setItems(entries);
    tblServers.setEditable(true);
  }
}
