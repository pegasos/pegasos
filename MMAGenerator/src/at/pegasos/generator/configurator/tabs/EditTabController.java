package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;
import javafx.fxml.Initializable;

public abstract class EditTabController implements Initializable {
  
  protected Application app;
  protected ApplicationEditorController mainController;

  public EditTabController(Application app, ApplicationEditorController controller)
  {
    this.app= app;
    this.mainController= controller;
  }

  public Application getApp()
  {
    return app;
  }
}
