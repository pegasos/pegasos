package at.pegasos.generator.configurator.tabs;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.parser.appparser.token.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;

public class EditTab extends Tab {

  protected Application app;

  protected EditTabController controller;

  public EditTab(Application app, ApplicationEditorController controller, String name)
  {
    super(name);
    
    this.app= app;

    try
    {
      // setContent(FXMLLoader.load(this.getClass().getResource(getClass().getSimpleName() + ".fxml")));
      
      // Load FXML
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(this.getClass().getResource(getClass().getSimpleName() + ".fxml"));
      // loader.setResources(ResourceBundle.getBundle("vcpx.analysis.lang.AnalysisPane", Settings.getInstance().getLocale()));
      loader.setRoot(this);

      this.controller = (EditTabController) Class.forName(getClass().getName() + "Controller")
          .getConstructor(Application.class, ApplicationEditorController.class).newInstance(app, controller);
      loader.setController(this.controller);
  
      // this.setPrefWidth(Globals.getInstance().getDefaultDatabaseWidth());
      // this.setPrefHeight(Globals.getInstance().getDefaultDatabaseHeight());

      loader.load();
    }
    catch( IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Error creating tab");
    }
  }
}
