package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.*;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.*;
import javafx.scene.control.cell.*;

public class ConfigurationTabController extends EditTabController {

  private static class ConfigEntry {
    public String name;
    public String type;
    public String value;
  }

  @FXML
  Button add;

  @FXML
  TableView<ConfigEntry> tbl;

  @FXML
  TableColumn<ConfigEntry, String> nameCol;

  @FXML
  TableColumn<ConfigEntry, String> typeCol;

  @FXML
  TableColumn<ConfigEntry, String> valueCol;

  @FXML
  TableColumn<ConfigEntry, Object> removeCol;

  private final ObservableList<ConfigEntry> entries;

  public ConfigurationTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    List<ConfigEntry> entries= new ArrayList<ConfigEntry>();
    if( true )
    {
      for(String valName : app.getConfig().getValueNames())
      {
        ConfigEntry e= new ConfigEntry();
        if( app.getConfig().isBool(valName) )
        {
          e.type= "boolean";
        }
        else if( app.getConfig().isString(valName) )
        {
          e.type= "string";
        }
        else
        {
          e.type= "int";
        }
        try
        {
          e.name= valName;
          e.value= app.getConfig().getValue(valName);
        }
        catch( ParserException e1 )
        {
          e1.printStackTrace();
        }
        entries.add(e);
      }
    }
    this.entries= FXCollections.observableArrayList(entries);
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
 // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      ConfigEntry e= new ConfigEntry();
      entries.add(e);
      tbl.scrollTo(e);
    });

    ObservableList<String> type= FXCollections.observableArrayList(Arrays.asList("string", "int", "boolean"));

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    typeCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().type));
    typeCol.setCellFactory(ComboBoxTableCell.forTableColumn(type));

    valueCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().value));
    valueCol.setCellFactory(TextFieldTableCell.forTableColumn());

    nameCol.setOnEditCommit((CellEditEvent<ConfigEntry, String> event) -> {
      TablePosition<ConfigEntry, String> pos= event.getTablePosition();

      int row= pos.getRow();
      ConfigEntry entry= event.getTableView().getItems().get(row);
      entry.name= event.getNewValue();

      checkEntry(entry);
    });

    typeCol.setOnEditCommit((CellEditEvent<ConfigEntry, String> event) -> {
      TablePosition<ConfigEntry, String> pos= event.getTablePosition();

      int row= pos.getRow();
      ConfigEntry entry= event.getTableView().getItems().get(row);
      entry.type= event.getNewValue();

      checkEntry(entry);
    });

    valueCol.setOnEditCommit((CellEditEvent<ConfigEntry, String> event) -> {
      TablePosition<ConfigEntry, String> pos= event.getTablePosition();

      int row= pos.getRow();
      ConfigEntry entry= event.getTableView().getItems().get(row);
      entry.value= event.getNewValue();

      checkEntry(entry);
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<ConfigEntry, Object> cell= new RemoveButtonTableCell<ConfigEntry, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        ConfigEntry e= entries.remove(index);
        app.config.remove(e.name);
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }

  private void checkEntry(ConfigEntry entry)
  {
    if( entry.name != null && !entry.name.equals("") && entry.type != null && !entry.type.equals("") && entry.value != null
        && !entry.value.equals("") )
    {
      if( entry.type.equals("string") )
      {
        try
        {
          app.config.setValueString(entry.name, entry.value);
        }
        catch( ParserException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      else if( entry.type.equals("boolean") )
      {
        try
        {
          app.config.setValueBool(entry.name, entry.value);
        }
        catch( ParserException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      else
      {
        try
        {
          app.config.setValue(entry.name, entry.value);
        }
        catch( ParserException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }
}
