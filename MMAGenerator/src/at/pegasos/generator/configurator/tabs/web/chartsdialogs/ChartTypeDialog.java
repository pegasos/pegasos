package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class ChartTypeDialog extends Dialog<String> {
  private final static String header = "Chart Type";
  private final static String header2 = "Please select a type for the chart and click OK to proceed";

  private final ObservableList<String> types;
  private String selectedType;

  public ChartTypeDialog(Stage primaryStage, ObservableList<String> types)
  {
    super();
    initOwner(primaryStage);

    this.types = types;

    setupUi();
  }

  private void setupUi()
  {
    BorderPane root = new BorderPane();
    root.setTop(new Label(header2));

    ComboBox<String> dropDown = new ComboBox<String>(types);
    dropDown.setPrefWidth(200.0d);
    dropDown.valueProperty().addListener((observable, oldValue, newValue) -> selectedType = newValue);

    root.setCenter(dropDown);

    getDialogPane().setContent(root);
    getDialogPane().setHeaderText(header);

    ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    getDialogPane().getButtonTypes().addAll(okBtnType, closeBtnType);

    setResultConverter((btnType) -> {
      if( btnType == okBtnType )
        return selectedType;
      else
        return null;
    });

    this.setResizable(false);
  }
}
