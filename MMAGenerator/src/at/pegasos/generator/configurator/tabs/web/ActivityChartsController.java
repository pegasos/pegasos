package at.pegasos.generator.configurator.tabs.web;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.configurator.tabs.web.chartsdialogs.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import at.pegasos.util.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.extern.slf4j.*;

import java.util.*;
import java.util.stream.*;

@Slf4j
@WebTab(name = "Activity Charts", order = 32)
public class ActivityChartsController extends VBox {

  private final static class ActivityTable<T extends at.pegasos.generator.parser.token.Activity> extends TreeView<T> {
    public interface OnClicked<T> {
      void onClicked(T item);
    }

    private final DAG.Node<T> root;
    private OnClicked<T> onClicked;

    public ActivityTable(DAG.Node<T> root)
    {
      super();
      this.root = root;
      updateTree();

      setOnMouseClicked(mouseEvent -> {
        if(mouseEvent.getClickCount() == 2)
        {
          TreeItem<T> item = getSelectionModel().getSelectedItem();
          // System.out.println("Selected Text : " + item.getValue());
          if( onClicked != null && item != null )
            onClicked.onClicked(item.getValue());
        }
      });

      /*setCellFactory(param -> new TreeCell<T>() {
      @Override
      protected void updateItem(T employee, boolean empty) {
        super.updateItem(employee, empty);
        if (employee == null || empty) {
          setGraphic(null);
        } else {
          EmployeeControl employeeControl = new EmployeeControl(employee);
          employeeControl.setOnMouseClicked(mouseEvent -> label.setText(employee.getName()));
          setGraphic(employeeControl);
        }
      }
    });*/
    }

    public void updateTree()
    {
      TreeItem<T> _root = new TreeItem<T>(root.getID());
      setRoot(_root);
      constructElements(_root, root);
    }

    private void constructElements(TreeItem<T> root, DAG.Node<T> node)
    {
      for(DAG.Node<T> child : node.getChildNodes())
      {
        TreeItem<T> childItem = new TreeItem<T>(child.getID());
        root.getChildren().add(childItem);
        constructElements(childItem, child);
      }
    }

    public void setOnClicked(OnClicked<T> onClicked)
    {
      this.onClicked = onClicked;
    }
  }

  private final ActivityTable<Activity> tbl;

  public ActivityChartsController(TitledPane ignoredPane, WebTabController webTabController)
  {
    ObservableList<String> chartList = FXCollections.observableArrayList(
        webTabController.getApp().getWeb().charts.getCharts().stream().map(chart -> chart.name).collect(Collectors.toList()));

    tbl = new ActivityTable<>(webTabController.getApp().getWeb().charts.getRoot());

    getChildren().add(new AddButton(event -> {
      Activity activity = null;
      if( tbl.getSelectionModel().getSelectedItems() != null && tbl.getSelectionModel().getSelectedItems().size() > 0 )
      {
        Activity parent = tbl.getSelectionModel().getSelectedItems().get(0).getValue();
        activity = new Activity();
        activity.param0 = parent.param0;
        activity.param1 = parent.param1;
        activity.param2 = parent.param2;
      }
      ActivityChartsDialog dlg = new ActivityChartsDialog(Configurator.getPrimaryStage(), activity, chartList);
      Optional<Activity> ret = dlg.showAndWait();
      if( ret.isPresent() )
      {
        try
        {
          webTabController.getApp().getWeb().charts.getAvailNode(ret.get()).replace(ret.get());
          webTabController.getMainController().setModified();
          tbl.updateTree();
        }
        catch( ParserException e )
        {
          ExceptionDialog.showAndWait(e, "Adding activity chart failed");
          e.printStackTrace();
        }
      }
    }));

    tbl.setOnClicked(item -> {
      ActivityChartsDialog dlg = new ActivityChartsDialog(Configurator.getPrimaryStage(), item.copy(), chartList);
      Optional<Activity> ret = dlg.showAndWait();
      if( ret.isPresent() )
      {
        item.replace(ret.get());
        webTabController.getMainController().setModified();
        tbl.updateTree();
      }
    });

    getChildren().add(tbl);
  }
}
