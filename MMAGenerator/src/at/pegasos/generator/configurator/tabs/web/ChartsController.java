package at.pegasos.generator.configurator.tabs.web;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.configurator.tabs.web.chartsdialogs.*;
import at.pegasos.generator.configurator.util.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import lombok.extern.slf4j.*;

import java.lang.reflect.*;
import java.util.*;

@Slf4j
@WebTab(name = "Charts", order = 31)
public class ChartsController extends VBox {

  public static final List<Class<? extends Charts.Chart>> types = List.of(SingleLineChart.class,
      MultiLineChart.class, DBTableChart.class, GeneratedChartType.class, MapChart.class,
      Poincare.class, Tachogram.class);

  private final WebTabController webTabController;
  private final ObservableList<Charts.Chart> entries;
  @FXML private final TableView<Charts.Chart> tbl;
  @FXML private final TableColumn<Charts.Chart, String> typeCol;
  @FXML private final TableColumn<Charts.Chart, String> nameCol;
  @FXML private final TableColumn<Charts.Chart, Boolean> defaultCol;
  @FXML private final TableColumn<Charts.Chart, Object> removeCol;

  private final EditColumn.Callback<Charts.Chart> onChartEdit = (entries, index, entry) -> {
    Charts.Chart chart = openChartDialog(entry.copy());
    if( chart != null && !chart.equals(entry) )
    {
      ChartsController.this.entries.set(index, chart);
      ChartsController.this.webTabController.getApp().getWeb().charts.replaceChart(entry, chart);
      ChartsController.this.webTabController.getMainController().setModified();
    }
  };

  public ChartsController(TitledPane ignoredPane, WebTabController webTabController)
  {
    this.webTabController = webTabController;
    this.entries = FXCollections.observableArrayList(webTabController.getApp().getWeb().charts.getCharts());

    ObservableList<String> typesList = buildTypesList();

    getChildren().add(new AddButton(event -> {
      ChartTypeDialog dlg = new ChartTypeDialog(Configurator.getPrimaryStage(), typesList);
      Optional<String> ret = dlg.showAndWait();
      if( ret.isPresent() )
      {
        Charts.Chart chart = openChartDialog(ret.get());
        if( chart != null )
        {
          this.entries.add(chart);
          System.out.println(chart);
          try
          {
            this.webTabController.getApp().getWeb().charts.addChart(chart);
            this.webTabController.getMainController().setModified();
          }
          catch( ParserException e )
          {
            e.printStackTrace();
          }
        }
      }
    }));

    tbl = new TableView<>();
    getChildren().add(tbl);

    typeCol = new TableColumn<>("Type");
    tbl.getColumns().add(typeCol);
    nameCol = new TableColumn<>("Name");
    tbl.getColumns().add(nameCol);
    defaultCol = new TableColumn<>("Default");
    tbl.getColumns().add(defaultCol);
    tbl.getColumns().add(new EditColumn<>(entries, onChartEdit));
    removeCol = new TableColumn<Charts.Chart, Object>();
    tbl.getColumns().add(removeCol);

    typeCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().getTypeName()));
    typeCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    defaultCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().default_avail));
    defaultCol.setCellFactory(CheckBoxTableCell.forTableColumn(defaultCol));

    nameCol.setOnEditCommit((TableColumn.CellEditEvent<Charts.Chart, String> event) -> {
      TablePosition<Charts.Chart, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Charts.Chart entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        entry.name = event.getNewValue();
      }
      webTabController.getMainController().setModified();
    });

    defaultCol.setOnEditCommit((TableColumn.CellEditEvent<Charts.Chart, Boolean> event) -> {
      TablePosition<Charts.Chart, Boolean> pos = event.getTablePosition();

      int row = pos.getRow();
      Charts.Chart entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        entry.default_avail = event.getNewValue();
      }
      webTabController.getMainController().setModified();
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Charts.Chart, Object> cell= new RemoveButtonTableCell<Charts.Chart, Object>();

      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        Charts.Chart e = entries.remove(index);

        webTabController.getApp().getWeb().charts.getCharts().removeIf(r -> r.equals(e));
        webTabController.getMainController().setModified();
      });

      return cell;
    });

    tbl.setRowFactory( tv -> {
    TableRow<Charts.Chart> row = new TableRow<>();
    row.setOnMouseClicked(event -> {
      if( event.getClickCount() == 2 && (! row.isEmpty()) )
      {
        System.out.println("Type: " + typeCol.getCellObservableValue(row.getIndex()).getValue());
        // System.out.println(types.get(typeCol.getCellObservableValue(row.getIndex()).getValue()));
        Charts.Chart rowData = row.getItem();
        onChartEdit.clicked(entries, row.getIndex(), rowData);
      }
    });
    return row ;
  });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }

  private static ObservableList<String> buildTypesList()
  {
    List<String> list = new ArrayList<>();
    for(Class<? extends Charts.Chart> clasz : ChartsController.types)
    {
      try
      {
        if( clasz != GeneratedChartType.class )
          list.add(clasz.getConstructor(String.class).newInstance("").getTypeName());
      }
      catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e )
      {
        e.printStackTrace();
      }
    }
    list.add("Generated");
    return FXCollections.observableArrayList(list);
  }

  private Charts.Chart openChartDialog(Charts.Chart chart)
  {
    ChartDialog dialog;
    if( chart instanceof SingleLineChart )
    {
      dialog = new SingleLineChartDialog(Configurator.getPrimaryStage(), (SingleLineChart) chart);
    }
    else if( chart instanceof MultiLineChart )
    {
      dialog = new MultiLineChartDialog(Configurator.getPrimaryStage(), (MultiLineChart) chart);
    }
    else if( chart instanceof MapChart )
    {
      dialog = new MapChartDialog(Configurator.getPrimaryStage(), (MapChart) chart);
    }
    else if( chart instanceof Poincare )
    {
      dialog = new PoincareChartDialog(Configurator.getPrimaryStage(), (Poincare) chart);
    }
    else if( chart instanceof Tachogram )
    {
      dialog = new TachogramChartDialog(Configurator.getPrimaryStage(), (Tachogram) chart);
    }
    else if( chart instanceof DBTableChart )
    {
      dialog = new BarsChartDialog(Configurator.getPrimaryStage(), (DBTableChart) chart);
    }
    else if( chart instanceof GeneratedChartType )
    {
      dialog = new GeneratedChartDialog(Configurator.getPrimaryStage(), (GeneratedChartType) chart);
    }
    else
    {
      throw new IllegalStateException();
    }

    Optional<Charts.Chart> ret = dialog.showAndWait();
    return ret.orElse(null);
  }
  private Charts.Chart openChartDialog(String chart)
  {
    ChartDialog dialog;
    switch( chart )
    {
      case SingleLineChart.TypeName:
        dialog = new SingleLineChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case MultiLineChart.TypeName:
        dialog = new MultiLineChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case MapChart.TypeName:
        dialog = new MapChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case Poincare.TypeName:
        dialog = new PoincareChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case Tachogram.TypeName:
        dialog = new TachogramChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case DBTableChart.TypeName:
        dialog = new BarsChartDialog(Configurator.getPrimaryStage(), null);
        break;
      case "Generated":
      case "generated":
        dialog = new GeneratedChartDialog(Configurator.getPrimaryStage(), null);
        break;
      default:
        throw new IllegalStateException();
    }

    Optional<Charts.Chart> ret = dialog.showAndWait();
    return ret.orElse(null);
  }
}
