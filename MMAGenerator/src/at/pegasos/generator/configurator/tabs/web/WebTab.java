package at.pegasos.generator.configurator.tabs.web;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface WebTab {
  String name() default "";

  int order() default 99;
}
