package at.pegasos.generator.configurator.tabs.web;

import java.util.*;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import lombok.extern.slf4j.*;

@Slf4j
@WebTab(name = "Routes", order = 26)
public class RoutesController extends VBox {

  @FXML
  TableView<Route> tbl;

  @FXML
  TableColumn<Route, String> typeCol;

  @FXML
  TableColumn<Route, String> pathCol;

  @FXML
  TableColumn<Route, String> compmodCol;

  @FXML
  TableColumn<Route, String> guardsCol;

  @FXML
  TableColumn<Route, Object> removeCol;

  private final ObservableList<Route> entries;

  private final WebTabController webTabController;

  public RoutesController(TitledPane pane, WebTabController webTabController)
  {
    this.webTabController = webTabController;

    this.entries= FXCollections.observableArrayList(webTabController.getApp().getWeb().routes);

    Button add = new Button();

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip("add");
    add.setTooltip(tooltip);

    /*entries.addListener((ListChangeListener<Route>) (change) -> {
      change.
    });*/

    add.setOnAction(event -> {
      boolean ok = true;
      for(Route e : webTabController.getApp().getWeb().routes )
      {
        if( e.path == null || e.path.equals("") )
        {
          ok = false;

          Alert alert = new Alert(Alert.AlertType.INFORMATION);
          alert.setTitle("Error");
          alert.setHeaderText("Adding rout not possible");
          alert.setContentText("You cannot have two routes with an empty path. Please configure the route first");
          alert.showAndWait();

          break;
        }
      }

      if( ok )
      {
        Route e = new ComponentRoute();
        entries.add(e);
        tbl.scrollTo(e);

        webTabController.getApp().getWeb().routes.add(e);
        webTabController.getMainController().setModified();
        log.info(webTabController.getApp().getWeb().routes.toString());
      }
    });
    getChildren().add(add);

    tbl = new TableView<Route>();
    getChildren().add(tbl);

    typeCol = new TableColumn<Route, String>("Type");
    tbl.getColumns().add(typeCol);
    pathCol = new TableColumn<Route, String>("Path");
    tbl.getColumns().add(pathCol);
    compmodCol = new TableColumn<Route, String>("Component/Module");
    tbl.getColumns().add(compmodCol);
    guardsCol = new TableColumn<Route, String>("Guards");
    tbl.getColumns().add(guardsCol);
    removeCol = new TableColumn<Route, Object>();
    tbl.getColumns().add(removeCol);

    typeCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue() instanceof ComponentRoute ? "Component" : "Module"));
    ObservableList<String> type= FXCollections.observableArrayList(Arrays.asList("Component", "Module"));
    typeCol.setCellFactory(ComboBoxTableCell.forTableColumn(type));

    pathCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().path));
    pathCol.setCellFactory(TextFieldTableCell.forTableColumn());
    pathCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    compmodCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(
            param.getValue() instanceof ComponentRoute ? ((ComponentRoute) param.getValue()).component : ((ModuleRoute) param.getValue()).module));
    compmodCol.setCellFactory(TextFieldTableCell.forTableColumn());
    compmodCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    guardsCol.setCellValueFactory(param -> {
      String s = param.getValue().guards.toString();
      s = s.substring(1, s.length() - 1);
      return new SimpleObjectProperty<String>(s);
    });
    guardsCol.setCellFactory(TextFieldTableCell.forTableColumn());
    guardsCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    typeCol.setOnEditCommit((TableColumn.CellEditEvent<Route, String> event) -> {
      TablePosition<Route, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Route entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        switch( event.getNewValue() )
        {
          case "Component":
          {
            ComponentRoute newRoute = new ComponentRoute();
            newRoute.path = entry.path;
            newRoute.guards = entry.guards;
            if( entry instanceof ModuleRoute )
              newRoute.component = ((ModuleRoute) entry).module;

            webTabController.getApp().getWeb().routes.remove(entry);
            webTabController.getApp().getWeb().routes.add(newRoute);
            break;
          }
          case "Module":
          {
            ModuleRoute newRoute = new ModuleRoute();
            newRoute.path = entry.path;
            newRoute.guards = entry.guards;
            if( entry instanceof ComponentRoute )
              newRoute.module = ((ComponentRoute) entry).component;

            webTabController.getApp().getWeb().routes.remove(entry);
            webTabController.getApp().getWeb().routes.add(newRoute);
            break;
          }
        }
      }
      webTabController.getMainController().setModified();
      log.info(webTabController.getApp().getWeb().routes.toString());
    });

    pathCol.setOnEditCommit((TableColumn.CellEditEvent<Route, String> event) -> {
      TablePosition<Route, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Route entry = event.getTableView().getItems().get(row);
      Route route = webTabController.getApp().getWeb().routes.stream().filter(r -> r.path == null && entry.path == null || r.path!= null && r.path.equals(entry.path)).findFirst().get();
      final String newPath = event.getNewValue();

      boolean ok = true;
      for(Route e : webTabController.getApp().getWeb().routes )
      {
        if( e.path != null && e.path.equals(newPath) )
        {
          ok = false;

          Alert alert = new Alert(Alert.AlertType.INFORMATION);
          alert.setTitle("Error");
          alert.setHeaderText("Changing path not possible");
          alert.setContentText("You already have a route defined with path '" + newPath + "'.");
          alert.showAndWait();

          break;
        }
      }
      if( ok )
      {
        entry.path = newPath;
        route.path = newPath;
        webTabController.getMainController().setModified();
      }
      log.info(webTabController.getApp().getWeb().routes.toString());
    });

    compmodCol.setOnEditCommit((TableColumn.CellEditEvent<Route, String> event) -> {
      TablePosition<Route, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Route entry = event.getTableView().getItems().get(row);
      Route route = webTabController.getApp().getWeb().routes.stream().filter(r -> r.path == null && entry.path == null || r.path!= null && r.path.equals(entry.path)).findFirst().get();
      if( route instanceof ComponentRoute )
      {
        ((ComponentRoute) route).component = event.getNewValue();
      }
      else if( route instanceof ModuleRoute )
      {
        ((ModuleRoute) route).module = event.getNewValue();
      }
      else
        log.error("Route is neither Module nor Component?");
      webTabController.getMainController().setModified();
      log.info(webTabController.getApp().getWeb().routes.toString());
    });

    guardsCol.setOnEditCommit((TableColumn.CellEditEvent<Route, String> event) -> {
      TablePosition<Route, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Route entry = event.getTableView().getItems().get(row);
      Route route = webTabController.getApp().getWeb().routes.stream().filter(r -> r.path == null && entry.path == null || r.path!= null && r.path.equals(entry.path)).findFirst().get();
      route.guards = List.of(event.getNewValue().split(","));
      webTabController.getMainController().setModified();
      log.info(webTabController.getApp().getWeb().routes.toString());
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Route, Object> cell= new RemoveButtonTableCell<Route, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        Route e = entries.remove(index);

        webTabController.getApp().getWeb().routes.removeIf(r -> r.path.equals(e.path));
        webTabController.getMainController().setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }
}
