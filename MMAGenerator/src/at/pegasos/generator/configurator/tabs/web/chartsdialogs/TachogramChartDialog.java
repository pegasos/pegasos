package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.stage.*;

public class TachogramChartDialog extends ChartDialog {
  private final static String header = "Configure a tachogram";

  public TachogramChartDialog(Stage primaryStage, Tachogram chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new Tachogram("");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();
    root.setTop(new Label(header));
  }

  @Override protected void onOk()
  {
    // Nothing to do here
  }
}
