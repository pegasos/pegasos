package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.stage.*;

public class MapChartDialog extends ChartDialog {
  private final static String header = "Configure a map chart";

  public MapChartDialog(Stage primaryStage, MapChart chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new MapChart("");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();

    root.setTop(new Label(header));

    TextField tableField = new TextField();
    if( chart != null )
      tableField.setText(((MapChart) chart).tbl);
    tableField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MapChart) chart).tbl = newValue;
      }
    });
    gridPane.add(new Label("Table: "), 0, gridRow);
    gridPane.add(tableField, 1, gridRow++);

    TextField colField = new TextField();
    if( chart != null )
      colField.setText(((MapChart) chart).collat);
    colField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MapChart) chart).collat = newValue;
      }
    });
    gridPane.add(new Label("Latitude: "), 0, gridRow);
    gridPane.add(colField, 1, gridRow++);

    TextField lonField = new TextField();
    if( chart != null )
      lonField.setText(((MapChart) chart).collon);
    lonField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MapChart) chart).collon = newValue;
      }
    });
    gridPane.add(new Label("Longitude: "), 0, gridRow);
    gridPane.add(lonField, 1, gridRow++);
  }

  @Override protected void onOk()
  {
    // nothing to do here
  }
}
