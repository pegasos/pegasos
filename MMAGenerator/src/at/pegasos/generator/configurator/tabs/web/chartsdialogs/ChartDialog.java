package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import javax.annotation.*;

public abstract class ChartDialog extends Dialog<Charts.Chart> {

  private final static String header = "Configure chart";

  protected Charts.Chart chart;
  protected VBox vbox;
  protected BorderPane root;
  protected int gridRow;
  protected GridPane gridPane;
  protected TextField name;

  public ChartDialog(Stage primaryStage, @Nullable Charts.Chart chart)
  {
    super();
    initOwner(primaryStage);

    this.chart = chart;
  }

  protected void setupUi()
  {
    getDialogPane().setHeaderText(header);
    root = new BorderPane();
    vbox = new VBox();
    gridPane = new GridPane();

    name = new TextField();
    if( chart != null )
      name.setText(chart.name);
    name.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( !oldValue.equals(newValue) && this.chart != null )
      {
        chart.name = newValue;
      }
    });
    gridPane.add(new Label("Name: "), 0, 0);
    gridPane.add(name, 1, 0);
    gridRow = 1;

    vbox.getChildren().add(gridPane);
    root.setCenter(vbox);

    getDialogPane().setContent(root);

    ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    getDialogPane().getButtonTypes().addAll(okBtnType, closeBtnType);

    setResultConverter((btnType) -> {
      if( btnType == okBtnType )
      {
        onOk();
        return chart;
      }
      else
        return null;
    });

    this.setResizable(false);
  }

  protected abstract void onOk();
}
