package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.stage.*;

import java.util.*;

public class MultiLineChartDialog extends ChartWithConfigDialog {
  private final static String header = "Configure a chart showing a (possibly) multiple lines";

  public MultiLineChartDialog(Stage primaryStage, MultiLineChart chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new MultiLineChart("");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();
    root.setTop(new Label(header));

    TextField tableField = new TextField();
    if( chart != null )
      tableField.setText(((MultiLineChart) chart).tbl);
    tableField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MultiLineChart) chart).tbl = newValue;
      }
    });
    gridPane.add(new Label("Table: "), 0, gridRow);
    gridPane.add(tableField, 1, gridRow++);

    TextField colField = new TextField();
    if( chart != null )
    {
      String type = ((MultiLineChart) chart).type;
      if( type != null && type.startsWith("LineType.") )
        type = type.substring(9).toLowerCase(Locale.ROOT);
      colField.setText(type);
    }
    colField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MultiLineChart) chart).type = newValue;
      }
    });
    gridPane.add(new Label("Type: "), 0, gridRow);
    gridPane.add(colField, 1, gridRow++);

    TextField linesField = new TextField();
    if( chart != null )
    {
      linesField.setText(((MultiLineChart) chart).getLines());
    }
    linesField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MultiLineChart) chart).setLines(newValue);
      }
    });
    gridPane.add(new Label("Type: "), 0, gridRow);
    gridPane.add(linesField, 1, gridRow++);

    TextField lineColors = new TextField();
    if( chart != null )
    {
      lineColors.setText(((MultiLineChart) chart).line_colors);
    }
    lineColors.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((MultiLineChart) chart).line_colors = newValue;
      }
    });
    gridPane.add(new Label("Line Colors: "), 0, gridRow);
    gridPane.add(lineColors, 1, gridRow++);

    addConfigUi();
  }

  @Override protected void onOk()
  {
    updateConfig();
  }
}
