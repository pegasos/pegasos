package at.pegasos.generator.configurator.tabs.web;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.parser.webparser.token.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;

@WebTab(name = "Menus", order = 25)
public class MenuController extends VBox {

  @FXML
  TableView<MenuEntry> tbl;

  @FXML
  TableColumn<MenuEntry, String> nameCol;

  @FXML
  TableColumn<MenuEntry, String> actionCol;

  @FXML
  TableColumn<MenuEntry, Integer> orderCol;

  @FXML
  TableColumn<MenuEntry, Boolean> publicCol;

  @FXML
  TableColumn<MenuEntry, String> rolesCol;

  @FXML
  TableColumn<MenuEntry, Object> removeCol;

  private final ObservableList<MenuEntry> entries;

  private final WebTabController webTabController;

  public MenuController(TitledPane pane, WebTabController webTabController)
  {
    // pane.setText("Menu");

    this.webTabController = webTabController;

    this.entries= FXCollections.observableArrayList(webTabController.getApp().getWeb().menu);

    Button add = new Button();

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip("add");
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      MenuEntry e= new MenuEntry("", "");
      entries.add(e);
      tbl.scrollTo(e);

      webTabController.getApp().getWeb().addMenuEntry(e);
      webTabController.getMainController().setModified();
    });
    getChildren().add(add);

    tbl = new TableView<MenuEntry>();
    getChildren().add(tbl);

    nameCol = new TableColumn<MenuEntry, String>("Name");
    tbl.getColumns().add(nameCol);
    actionCol = new TableColumn<MenuEntry, String>("Action");
    tbl.getColumns().add(actionCol);
    orderCol = new TableColumn<MenuEntry, Integer>("Order");
    tbl.getColumns().add(orderCol);
    publicCol = new TableColumn<MenuEntry, Boolean>("Public");
    tbl.getColumns().add(publicCol);
    rolesCol = new TableColumn<MenuEntry, String>("Roles");
    tbl.getColumns().add(rolesCol);
    removeCol = new TableColumn<MenuEntry, Object>();
    tbl.getColumns().add(removeCol);

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    actionCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().action));
    actionCol.setCellFactory(TextFieldTableCell.forTableColumn());
    actionCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    orderCol.setCellValueFactory(param -> new SimpleObjectProperty<Integer>(param.getValue().order));
    // orderCol.setCellValueFactory(cellData -> cellData.getValue().order);
    orderCol.setCellFactory(col -> new IntegerEditingCell<MenuEntry>());
    orderCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    publicCol.setCellValueFactory(param -> new SimpleObjectProperty<Boolean>(param.getValue().isPublic));
    publicCol.setCellFactory(CheckBoxTableCell.forTableColumn(publicCol));

    rolesCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().requiredRoles.toString()));
    rolesCol.setCellFactory(TextFieldTableCell.forTableColumn());
    rolesCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    nameCol.setOnEditCommit((TableColumn.CellEditEvent<MenuEntry, String> event) -> {
      final String oldName = event.getOldValue();

      for(MenuEntry e : webTabController.getApp().getWeb().menu )
      {
        if( e.name.equals(oldName) )
        {
          e.name = event.getNewValue();
        }
      }
    });
    actionCol.setOnEditCommit((TableColumn.CellEditEvent<MenuEntry, String> event) -> {
      TablePosition<MenuEntry, String> pos = event.getTablePosition();

      int row = pos.getRow();
      MenuEntry entry = event.getTableView().getItems().get(row);

      for(MenuEntry e : webTabController.getApp().getWeb().menu )
      {
        if( e.name.equals(entry.name) )
        {
          e.action = event.getNewValue();
        }
      }
    });
    orderCol.setOnEditCommit((TableColumn.CellEditEvent<MenuEntry, Integer> event) -> {
      TablePosition<MenuEntry, Integer> pos = event.getTablePosition();

      int row = pos.getRow();
      MenuEntry entry = event.getTableView().getItems().get(row);

      for(MenuEntry e : webTabController.getApp().getWeb().menu )
      {
        if( e.name.equals(entry.name) )
        {
          e.order = event.getNewValue();
        }
      }
    });
    publicCol.setOnEditCommit((TableColumn.CellEditEvent<MenuEntry, Boolean> event) -> {
      TablePosition<MenuEntry, Boolean> pos = event.getTablePosition();

      int row = pos.getRow();
      MenuEntry entry = event.getTableView().getItems().get(row);

      for(MenuEntry e : webTabController.getApp().getWeb().menu )
      {
        if( e.name.equals(entry.name) )
        {
          e.isPublic = event.getNewValue();
        }
      }
    });
    // rolesCol.setOnEditCommit((TableColumn.CellEditEvent<Modules.Module, String> event) -> save(event, ColAttr.LOCATION));

    removeCol.setCellFactory(column -> {
      ButtonTableCell<MenuEntry, Object> cell= new RemoveButtonTableCell<MenuEntry, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        MenuEntry entry = entries.remove(index);

        webTabController.getApp().getWeb().menu.removeIf(e -> e.name.equals(entry.name));
        webTabController.getMainController().setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }

}
