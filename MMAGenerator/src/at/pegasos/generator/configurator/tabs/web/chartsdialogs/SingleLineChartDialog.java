package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.stage.*;

public class SingleLineChartDialog extends ChartWithConfigDialog {
  private final static String header = "Configure a chart showing a single line";

  public SingleLineChartDialog(Stage primaryStage, SingleLineChart chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new SingleLineChart("");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();
    root.setTop(new Label(header));

    TextField tableField = new TextField();
    if( chart != null )
      tableField.setText(((SingleLineChart) chart).tbl);
    tableField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((SingleLineChart) chart).tbl = newValue;
      }
    });
    gridPane.add(new Label("Table: "), 0, gridRow);
    gridPane.add(tableField, 1, gridRow++);

    TextField colField = new TextField();
    if( chart != null )
      colField.setText(((SingleLineChart) chart).col);
    colField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((SingleLineChart) chart).col = newValue;
      }
    });
    gridPane.add(new Label("Column: "), 0, gridRow);
    gridPane.add(colField, 1, gridRow++);

    addConfigUi();
  }

  @Override protected void onOk()
  {
    updateConfig();
  }
}
