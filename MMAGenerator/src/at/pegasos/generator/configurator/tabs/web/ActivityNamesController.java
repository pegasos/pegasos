package at.pegasos.generator.configurator.tabs.web;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import lombok.extern.slf4j.*;

import java.util.stream.*;

@Slf4j
@WebTab(name = "Names", order = 30)
public class ActivityNamesController extends VBox {

  @FXML
  TableView<ActivityNames.Activity> tbl;

  @FXML
  TableColumn<ActivityNames.Activity, String> param0Col;

  @FXML
  TableColumn<ActivityNames.Activity, String> param1Col;

  @FXML
  TableColumn<ActivityNames.Activity, String> param2Col;

  @FXML
  TableColumn<ActivityNames.Activity, String> nameCol;

  @FXML
  TableColumn<ActivityNames.Activity, Object> removeCol;

  private final ObservableList<ActivityNames.Activity> entries;

  public ActivityNamesController(TitledPane pane, WebTabController webTabController)
  {
    this.entries= FXCollections.observableArrayList(webTabController.getApp().getWeb().activitynames.getAllActivities().stream()
        .filter(a -> !a.dummy).collect(Collectors.toList()));

    Button add = new Button();

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip("add");
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      ActivityNames.Activity e= new ActivityNames.Activity();
      entries.add(e);
      tbl.scrollTo(e);

      // webTabController.getApp().getWeb().addMenuEntry(e);
      webTabController.getMainController().setModified();
    });

    getChildren().add(add);

    tbl = new TableView<>();
    getChildren().add(tbl);

    param0Col = new TableColumn<>("Param0");
    tbl.getColumns().add(param0Col);
    param1Col = new TableColumn<>("Param1");
    tbl.getColumns().add(param1Col);
    param2Col = new TableColumn<>("Param2");
    tbl.getColumns().add(param2Col);
    nameCol = new TableColumn<>("Name");
    tbl.getColumns().add(nameCol);
    removeCol = new TableColumn<>();
    tbl.getColumns().add(removeCol);

    param0Col.setCellValueFactory(param ->
        new SimpleObjectProperty<String>(param.getValue().param0 == -1 ? "" : String.valueOf(param.getValue().param0)));
    param0Col.setCellFactory(TextFieldTableCell.forTableColumn());
    param0Col.setStyle( "-fx-alignment: CENTER-LEFT;");

    param1Col.setCellValueFactory(param ->
        new SimpleObjectProperty<String>(param.getValue().param1 == -1 ? "" : String.valueOf(param.getValue().param1)));
    param1Col.setCellFactory(TextFieldTableCell.forTableColumn());
    param1Col.setStyle( "-fx-alignment: CENTER-LEFT;");

    param2Col.setCellValueFactory(param ->
        new SimpleObjectProperty<String>(param.getValue().param2 == -1 ? "" : String.valueOf(param.getValue().param2)));
    param2Col.setCellFactory(TextFieldTableCell.forTableColumn());
    param2Col.setStyle( "-fx-alignment: CENTER-LEFT;");

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    param0Col.setOnEditCommit((TableColumn.CellEditEvent<ActivityNames.Activity, String> event) -> {
      TablePosition<ActivityNames.Activity, String> pos = event.getTablePosition();

      int row = pos.getRow();
      ActivityNames.Activity entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        webTabController.getApp().getWeb().activitynames.remove(entry);
        if( event.getNewValue().matches(" ") || event.getNewValue().equals("") )
          entry.param0 = -1;
        else
          entry.param0 = Integer.parseInt(event.getNewValue());
        try
        {
          entries.set(row, webTabController.getApp().getWeb().activitynames.addName(entry));
        }
        catch (ParserException e)
        {
          // TODO: better handling of duplicate entry
          throw new RuntimeException(e);
        }
      }
      webTabController.getMainController().setModified();
    });

    param1Col.setOnEditCommit((TableColumn.CellEditEvent<ActivityNames.Activity, String> event) -> {
      TablePosition<ActivityNames.Activity, String> pos = event.getTablePosition();

      int row = pos.getRow();
      ActivityNames.Activity entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        webTabController.getApp().getWeb().activitynames.remove(entry);
        if( event.getNewValue().matches(" ") || event.getNewValue().equals("") )
          entry.param1 = -1;
        else
          entry.param1 = Integer.parseInt(event.getNewValue());
        try {
          entries.set(row, webTabController.getApp().getWeb().activitynames.addName(entry));
        } catch (ParserException e) {
          // TODO: better handling of duplicate entry
          throw new RuntimeException(e);
        }
      }
      webTabController.getMainController().setModified();
    });

    param2Col.setOnEditCommit((TableColumn.CellEditEvent<ActivityNames.Activity, String> event) -> {
      TablePosition<ActivityNames.Activity, String> pos = event.getTablePosition();

      int row = pos.getRow();
      ActivityNames.Activity entry = event.getTableView().getItems().get(row);
      if( event.getOldValue() == null || !event.getOldValue().equals(event.getNewValue()) )
      {
        webTabController.getApp().getWeb().activitynames.remove(entry);
        if( event.getNewValue().matches(" ") || event.getNewValue().equals("") )
          entry.param2 = -1;
        else
          entry.param2 = Integer.parseInt(event.getNewValue());
        try {
          entries.set(row, webTabController.getApp().getWeb().activitynames.addName(entry));
        } catch (ParserException e) {
          // TODO: better handling of duplicate entry
          throw new RuntimeException(e);
        }
      }
      webTabController.getMainController().setModified();
    });

    nameCol.setOnEditCommit((TableColumn.CellEditEvent<ActivityNames.Activity, String> event) -> {
      TablePosition<ActivityNames.Activity, String> pos = event.getTablePosition();

      int row = pos.getRow();
      ActivityNames.Activity entry = event.getTableView().getItems().get(row);
      entry.name = event.getNewValue();
      webTabController.getMainController().setModified();
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<ActivityNames.Activity, Object> cell= new RemoveButtonTableCell<>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        ActivityNames.Activity e = entries.remove(index);

        webTabController.getApp().getWeb().activitynames.remove(e);
        webTabController.getMainController().setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }
}
