package at.pegasos.generator.configurator.tabs.web;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.tabs.*;
import at.pegasos.generator.parser.webparser.token.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;

@WebTab(name = "Modules", order = 1)
public class ModuleController extends VBox {

  @FXML
  TableView<Modules.Module> tbl;

  @FXML
  TableColumn<Modules.Module, String> nameCol;

  @FXML
  TableColumn<Modules.Module, String> fileCol;

  @FXML
  TableColumn<Modules.Module, String> locationCol;

  @FXML
  TableColumn<Modules.Module, Object> removeCol;

  private final ObservableList<Modules.Module> entries;

  private final WebTabController webTabController;

  public ModuleController(TitledPane pane, WebTabController webTabController)
  {
    pane.setText("Modules");

    this.webTabController = webTabController;

    this.entries= FXCollections.observableArrayList(webTabController.getApp().getWeb().modules.entries());

    Button add = new Button();

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip("add");
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      Modules.Module e= new Modules.Module("","","", false);
      entries.add(e);
      tbl.scrollTo(e);

      webTabController.getApp().getWeb().modules.addModule(e);
      webTabController.getMainController().setModified();
    });
    getChildren().add(add);

    tbl = new TableView<Modules.Module>();
    getChildren().add(tbl);

    nameCol = new TableColumn<Modules.Module, String>("Name");
    tbl.getColumns().add(nameCol);
    fileCol = new TableColumn<Modules.Module, String>("File");
    tbl.getColumns().add(fileCol);
    locationCol = new TableColumn<Modules.Module, String>("Location");
    tbl.getColumns().add(locationCol);
    removeCol = new TableColumn<Modules.Module, Object>();
    tbl.getColumns().add(removeCol);

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    fileCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().file));
    fileCol.setCellFactory(TextFieldTableCell.forTableColumn());
    fileCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    locationCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().location.toString()));
    locationCol.setCellFactory(TextFieldTableCell.forTableColumn());
    locationCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    nameCol.setOnEditCommit((TableColumn.CellEditEvent<Modules.Module, String> event) -> save(event, ColAttr.NAME));
    fileCol.setOnEditCommit((TableColumn.CellEditEvent<Modules.Module, String> event) -> save(event, ColAttr.FILE));
    locationCol.setOnEditCommit((TableColumn.CellEditEvent<Modules.Module, String> event) -> save(event, ColAttr.LOCATION));

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Modules.Module, Object> cell= new RemoveButtonTableCell<Modules.Module, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        Modules.Module e= entries.remove(index);

        webTabController.getApp().getWeb().modules.removeModule(e);
        webTabController.getMainController().setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }

  private enum ColAttr {
    NAME,
    FILE,
    LOCATION
  }

  private void save(TableColumn.CellEditEvent<Modules.Module, String> event, ColAttr t)
  {
    TablePosition<Modules.Module, String> pos = event.getTablePosition();

    int row = pos.getRow();
    Modules.Module entry = event.getTableView().getItems().get(row);
    Modules.Module newEntry = null;
    switch( t)
    {
      case NAME:
        newEntry = new Modules.Module(event.getNewValue(), entry.file, entry.location.toString(), entry.isDefault());
        break;
      case FILE:
        newEntry = new Modules.Module(entry.name, event.getNewValue(), entry.location.toString(), entry.isDefault());
        break;
      case LOCATION:
        newEntry = new Modules.Module(entry.name, entry.file, event.getNewValue(), entry.isDefault());
        break;
    }

    event.getTableView().getItems().set(row, newEntry);
    if( t == ColAttr.NAME )
    {
      webTabController.getApp().getWeb().modules.removeModule(entry);
      webTabController.getApp().getWeb().modules.addModule(newEntry);
    }
    else
    {
      webTabController.getApp().getWeb().modules.updateModule(newEntry);
    }
    webTabController.getMainController().setModified();
  }
}
