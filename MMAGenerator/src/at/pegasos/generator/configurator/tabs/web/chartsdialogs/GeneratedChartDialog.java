package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import java.util.*;

public class GeneratedChartDialog extends ChartDialog {
  private final static String header = "Configure a generic chart";

  private String oldCTypeVal;

  public GeneratedChartDialog(Stage primaryStage, GeneratedChartType chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new GeneratedChartType("Generated", "");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();
    root.setTop(new Label(header));

    TextField ctype = new TextField();
    if( chart != null )
      ctype.setText(chart.name);
    ctype.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( !oldValue.equals(newValue) && this.chart != null )
      {
        ((GeneratedChartType) chart).ctype = newValue;
      }
    });
    gridPane.add(new Label("Chart Type: "), 0, gridRow);
    gridPane.add(ctype, 1, gridRow++);

    super.name.focusedProperty().addListener((whatEver, oldValue, newValue) -> {
      if( !newValue && chart != null && ( ctype.getText().isEmpty() || ctype.getText().equals(oldCTypeVal) ) )
      {
        String val;
        if( name.getText().toLowerCase(Locale.ROOT).startsWith("chart_") )
          val = name.getText().substring(6);
        else
          val = name.getText();
        oldCTypeVal = val;
        ctype.textProperty().setValue(val);
      }
    });

    // CheckBox alwaysAvailableCheck = new CheckBox();
    GridPane availClassPane = new GridPane();
    GridPane databaseAvailPane = new GridPane();

    // create radiobuttons
    RadioButton r1 = new RadioButton("Always Available");
    RadioButton r2 = new RadioButton("Availability Class");
    RadioButton r3 = new RadioButton("Database Table");
    ToggleGroup group = new ToggleGroup();
    r1.setToggleGroup(group);
    r2.setToggleGroup(group);
    r3.setToggleGroup(group);
    VBox availabilityBoxes = new VBox();
    availabilityBoxes.getChildren().addAll(r1, r2, r3);

    if( chart != null )
    {
      if( ((GeneratedChartType) chart).alwaysavailable )
      {
        r1.setSelected(true);
        availClassPane.setDisable(true);
        databaseAvailPane.setDisable(true);
      }
      else if( ((GeneratedChartType) chart).hasAvailable )
      {
        r2.setSelected(true);
        availClassPane.setDisable(false);
        databaseAvailPane.setDisable(true);
      }
      else
      {
        r3.setSelected(true);
        availClassPane.setDisable(true);
        databaseAvailPane.setDisable(false);
      }
    }

    r1.selectedProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setAlwaysAvailable(newValue);
        availClassPane.setDisable(newValue);
        databaseAvailPane.setDisable(newValue);
      }
    });
    r2.selectedProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).hasAvailable = newValue;
        ((GeneratedChartType) chart).setAlwaysAvailable(!newValue);
        availClassPane.setDisable(!newValue);
        databaseAvailPane.setDisable(newValue);
      }
    });
    r3.selectedProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).hasAvailable = !newValue;
        ((GeneratedChartType) chart).setAlwaysAvailable(!newValue);
        availClassPane.setDisable(newValue);
        databaseAvailPane.setDisable(!newValue);
      }
    });

    gridPane.add(new Label("Availability: "), 0, gridRow);
    gridPane.add(availabilityBoxes, 1, gridRow++);

    TextField clsField = new TextField();
    TextField argsField = new TextField();
    if( chart != null )
    {
      clsField.setText(((GeneratedChartType) chart).getCls());
    }
    clsField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setClsArgs(newValue, argsField.textProperty().getValue());
      }
    });
    availClassPane.add(new Label("Class: "), 0, 0);
    availClassPane.add(clsField, 1, 0);
    if( chart != null )
    {
      argsField.setText(((GeneratedChartType) chart).getArgs());
    }
    argsField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setClsArgs(clsField.textProperty().getValue(), newValue);
      }
    });
    availClassPane.add(new Label("Constructor arguments: "), 0, 1);
    availClassPane.add(argsField, 1, 1);
    gridPane.add(new Label("Availability class: "), 0, gridRow);
    gridPane.add(availClassPane, 1, gridRow++);

    TextField tblField = new TextField();
    TextField colField = new TextField();
    if( chart != null )
    {
      tblField.setText(((GeneratedChartType) chart).getTbl());
    }
    tblField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setTblCol(newValue, colField.textProperty().getValue());
      }
    });
    databaseAvailPane.add(new Label("Table: "), 0, 0);
    databaseAvailPane.add(tblField, 1, 0);
    if( chart != null )
    {
      colField.setText(((GeneratedChartType) chart).getCol());
    }
    colField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setTblCol(tblField.textProperty().getValue(), newValue);
      }
    });
    databaseAvailPane.add(new Label("Column: "), 0, 1);
    databaseAvailPane.add(colField, 1, 1);
    gridPane.add(new Label("Availability (database): "), 0, gridRow);
    gridPane.add(databaseAvailPane, 1, gridRow++);


    GridPane typePane = new GridPane();
    TextField chartClass = new TextField();
    TextField chartFile = new TextField();
    if( chart != null && ((GeneratedChartType) chart).getType() != null )
    {
      Type type = ((GeneratedChartType) chart).getType();
      chartClass.setText(type.clas);
      chartFile.setText(type.file);
    }
    chartClass.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setType(Type.create(ctype.getText(), newValue, chartFile.getText()));
      }
    });
    typePane.add(new Label("Class: "), 0, 0);
    typePane.add(chartClass, 1, 0);
    chartFile.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((GeneratedChartType) chart).setType(Type.create(ctype.getText(), chartClass.getText(), newValue));
      }
    });
    typePane.add(new Label("File: "), 0, 1);
    typePane.add(chartFile, 1, 1);
    gridPane.add(new Label("Frontend (class): "), 0, gridRow);
    gridPane.add(typePane, 1, gridRow++);
  }

  @Override protected void onOk()
  {
  }
}
