package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.configurator.util.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.util.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.stage.*;

import java.util.*;
import java.util.stream.*;

public abstract class ChartWithConfigDialog extends ChartDialog {
  private final ObservableList<Pair<String, String>> entries;
  private final Stage primaryStage;

  public ChartWithConfigDialog(Stage primaryStage, Charts.ChartWithConfig chart)
  {
    super(primaryStage, chart);
    initOwner(primaryStage);
    this.primaryStage = primaryStage;

    if( chart != null )
      entries = FXCollections.observableArrayList(
        chart.getConfigs().entrySet().stream().map(e -> new Pair<>(e.getKey(), e.getValue())).collect(Collectors.toList()));
    else
      entries = FXCollections.observableArrayList();
  }

  protected void setupUi()
  {
    super.setupUi();
  }

  protected void addConfigUi()
  {
    // Possible config variables
    ObservableList<String> type = FXCollections.observableArrayList(((Charts.ChartWithConfig) chart).getAllowedValues());

    gridPane.add(new AddButton((event) -> {
      ConfigValueDialog dlg = new ConfigValueDialog(primaryStage, null, type);
      Optional<Pair<String, String>> res = dlg.showAndWait();
      if( res.isPresent() )
      {
        Pair<String, String> val = res.get();
        entries.add(val);
        System.out.println(Arrays.toString(entries.toArray()));
      }
    }), 1, gridRow++);

    gridPane.add(new Label("Config: "), 0, gridRow);

    TableView<Pair<String, String>> tbl;
    TableColumn<Pair<String, String>, String> variableCol = new TableColumn<>("Variable");
    TableColumn<Pair<String, String>, String> valueCol = new TableColumn<>("Value");
    TableColumn<Pair<String, String>, Object> editCol = new EditColumn<>(entries, (entries, index, entry) -> {
      ConfigValueDialog dlg = new ConfigValueDialog(primaryStage, entry, type);
      Optional<Pair<String, String>> res = dlg.showAndWait();
      if( res.isPresent() )
      {
        Pair<String, String> val = res.get();
        if( !val.equals(entry) )
          entries.set(index, val);
      }
    });
    TableColumn<Pair<String, String>, Object> removeCol = new RemoveColumn<>(entries);

    variableCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().getA()));
    variableCol.setCellFactory(ComboBoxTableCell.forTableColumn(type));
    variableCol.setOnEditCommit(event -> entries.get(event.getTablePosition().getRow()).setA(event.getNewValue()));

    valueCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().getB()));
    valueCol.setOnEditCommit(event -> entries.get(event.getTablePosition().getRow()).setB(event.getNewValue()));

    tbl = new TableView<Pair<String, String>>(entries);
    tbl.getColumns().addAll(Arrays.asList(variableCol, valueCol, editCol, removeCol));
    tbl.setEditable(true);

    tbl.setMinHeight(150);
    tbl.setMinWidth(380);

    gridPane.add(tbl, 1, gridRow++);
  }

  protected void updateConfig()
  {
    Map<String, String> map = new HashMap<>();
    entries.forEach(p -> map.put(p.getA(), p.getB()));
    ((Charts.ChartWithConfig) this.chart).setConfigs(map);
  }
}
