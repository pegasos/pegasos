package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.util.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.util.converter.*;

import java.util.*;

public class ActivityChartsDialog extends Dialog<Activity> {
  private final static String header = "Activity Chart";
  private final static String header2 = "Configure charts for an activity";
  private final Activity activity;

  public static class ChartsTable extends VBox {

    public ChartsTable(List<Activity.Chart> valueList, ObservableList<String> chartList)
    {
      ObservableList<Activity.Chart> entries = FXCollections.observableList(valueList);

      getChildren().add(new AddButton(event -> entries.add(new Activity.Chart("", 99, false))));

      TableColumn<Activity.Chart, String> valCol = new TableColumn<Activity.Chart, String>("Chart");
      valCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
      valCol.setCellFactory(param -> {
        ComboBoxTableCell<Activity.Chart, String> ct= new ComboBoxTableCell<>(chartList);
        ct.setComboBoxEditable(true);
        return ct;
      });
      valCol.setOnEditCommit((TableColumn.CellEditEvent<Activity.Chart, String> event) -> {
        TablePosition<Activity.Chart, String> pos= event.getTablePosition();

        int row= pos.getRow();
        Activity.Chart entry= event.getTableView().getItems().get(row);
        entry.name= event.getNewValue();
      });
      valCol.setStyle( "-fx-alignment: CENTER-LEFT;");

      TableColumn<Activity.Chart, Integer> orderCol = new TableColumn<Activity.Chart, Integer>("Order");
      orderCol.setCellValueFactory(param -> new SimpleObjectProperty<Integer>(param.getValue().hasOrder ? param.getValue().order : null));
      orderCol.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
      orderCol.setOnEditCommit((TableColumn.CellEditEvent<Activity.Chart, Integer> event) -> {
        TablePosition<Activity.Chart, Integer> pos= event.getTablePosition();

        int row= pos.getRow();
        Activity.Chart entry = event.getTableView().getItems().get(row);
        System.out.println(event.getNewValue());
        if( event.getNewValue() == null )
        {
          entry.hasOrder = false;
        }
        else
        {
          entry.hasOrder = true;
          entry.order = event.getNewValue();
        }
      });
      orderCol.setStyle( "-fx-alignment: CENTER-RIGHT;");

      TableView<Activity.Chart> tbl = new TableView<>(entries);
      tbl.getColumns().add(valCol);
      tbl.getColumns().add(orderCol);
      tbl.getColumns().add(new RemoveColumn<>(entries));

      // tbl.setMinHeight(2);
      tbl.setMinHeight(150);
      tbl.setMinWidth(380);
      tbl.setEditable(true);

      getChildren().add(tbl);
    }
  }

  public ActivityChartsDialog(Stage primaryStage, Activity activity, ObservableList<String> chartList)
  {
    super();
    initOwner(primaryStage);

    if( activity == null )
      this.activity = new Activity();
    else
      this.activity = activity;

    BorderPane root = new BorderPane();
    root.setTop(new Label(header2));

    GridPane activityPane = new GridPane();

    IntegerTextField p0 = new IntegerTextField(-1, 999, activity != null ? activity.param0 : -1);
    IntegerTextField p1 = new IntegerTextField(-1, 999, activity != null ? activity.param1 : -1);
    IntegerTextField p2 = new IntegerTextField(-1, 999, activity != null ? activity.param2 : -1);

    p0.valueProperty().addListener((ignored, oldValue, newValue) -> this.activity.param0 = newValue.intValue());
    p1.valueProperty().addListener((ignored, oldValue, newValue) -> this.activity.param1 = newValue.intValue());
    p2.valueProperty().addListener((ignored, oldValue, newValue) -> this.activity.param2 = newValue.intValue());

    activityPane.add(new Label("Param0: "), 0, 0);
    activityPane.add(p0, 1, 0);
    activityPane.add(new Label("Param1: "), 0, 1);
    activityPane.add(p1, 1, 1);
    activityPane.add(new Label("Param2: "), 0, 2);
    activityPane.add(p2, 1, 2);

    activityPane.add(new Label("Charts Pre: "), 0, 3);
    activityPane.add(new ChartsTable(this.activity.chartsPre, chartList), 1, 3);

    activityPane.add(new Label("Charts Post: "), 0, 4);
    activityPane.add(new ChartsTable(this.activity.chartsPost, chartList), 1, 4);

    CheckBox chkDefault = new CheckBox();
    chkDefault.setSelected(activity != null && activity.adddefault);
    chkDefault.selectedProperty().addListener((ignored, oldValue, newValue) -> this.activity.overwrite = newValue);
    activityPane.add(new Label("Add default charts: "), 0, 5);
    activityPane.add(chkDefault, 1, 5);

    CheckBox chkOverwrite = new CheckBox();
    chkOverwrite.setSelected(activity != null && activity.overwrite == Boolean.TRUE);
    chkOverwrite.selectedProperty().addListener((ignored, oldValue, newValue) -> this.activity.overwrite = newValue);
    activityPane.add(new Label("Overwrite: "), 0, 6);
    activityPane.add(chkOverwrite, 1, 6);

    root.setCenter(activityPane);

    getDialogPane().setContent(root);
    getDialogPane().setHeaderText(header);

    ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    getDialogPane().getButtonTypes().addAll(okBtnType, closeBtnType);

    setResultConverter((btnType) -> {
      if( btnType == okBtnType )
        return this.activity;
      else
        return null;
    });

    this.setResizable(false);
  }
}
