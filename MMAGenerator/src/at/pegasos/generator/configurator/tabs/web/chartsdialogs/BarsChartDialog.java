package at.pegasos.generator.configurator.tabs.web.chartsdialogs;

import at.pegasos.generator.parser.webparser.token.charts.*;
import javafx.scene.control.*;
import javafx.stage.*;

public class BarsChartDialog extends ChartWithConfigDialog {
  private final static String header = "Configure a chart showing bars";

  public BarsChartDialog(Stage primaryStage, DBTableChart chart)
  {
    super(primaryStage, chart);

    if( chart == null )
      this.chart = new DBTableChart("");

    setupUi();
  }

  protected void setupUi()
  {
    super.setupUi();
    root.setTop(new Label(header));

    // A bit hacky. But for now we just set the type to bars
    /*TextField typeField = new TextField();
    if( chart != null )
      typeField.setText(((DBTableChart) chart).type);
    typeField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).type = newValue;
      }
    });
    gridPane.add(new Label("Type: "), 0, gridRow);
    gridPane.add(typeField, 1, gridRow++);*/
    ((DBTableChart) chart).type = "Type.Bars";

    TextField tableName = new TextField();
    if( chart != null )
      tableName.setText(((DBTableChart) chart).table_name);
    tableName.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).table_name = newValue;
      }
    });
    gridPane.add(new Label("Table: "), 0, gridRow);
    gridPane.add(tableName, 1, gridRow++);

    TextField discriminatorField = new TextField();
    if( chart != null )
      discriminatorField.setText(((DBTableChart) chart).discriminator);
    discriminatorField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).discriminator = newValue;
      }
    });
    gridPane.add(new Label("Discriminator: "), 0, gridRow);
    gridPane.add(discriminatorField, 1, gridRow++);

    TextField valueField = new TextField();
    if( chart != null )
      valueField.setText(((DBTableChart) chart).value_field);
    valueField.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).value_field = newValue;
      }
    });
    gridPane.add(new Label("Value: "), 0, gridRow);
    gridPane.add(valueField, 1, gridRow++);

    TextField whereClause = new TextField();
    if( chart != null )
      whereClause.setText(((DBTableChart) chart).where_clause);
    whereClause.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).where_clause = newValue;
      }
    });
    gridPane.add(new Label("Where: "), 0, gridRow);
    gridPane.add(whereClause, 1, gridRow++);

    TextField lineColors = new TextField();
    if( chart != null )
    {
      lineColors.setText(((DBTableChart) chart).colors);
    }
    lineColors.textProperty().addListener((whatEver, oldValue, newValue) -> {
      if( this.chart != null )
      {
        ((DBTableChart) chart).colors = newValue;
      }
    });
    gridPane.add(new Label("Colors: "), 0, gridRow);
    gridPane.add(lineColors, 1, gridRow++);

    addConfigUi();
  }

  @Override protected void onOk()
  {
    DBTableChart c = (DBTableChart) chart;
    if( c.discriminator != null && c.discriminator.equals("") )
      c.discriminator = null;
    if( c.value_field != null && c.value_field.equals("") )
      c.value_field = null;
    if( c.where_clause != null && c.where_clause.equals("") )
      c.where_clause = null;
    if( c.colors != null && c.colors.equals("") )
      c.colors = null;
    updateConfig();
  }
}
