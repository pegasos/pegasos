package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;

public class SportsTab extends EditTab {

  public SportsTab(Application app, ApplicationEditorController controller)
  {
    super(app, controller, "Sports");
  }
}
