package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;

public class ControllersTab extends EditTab {
  public ControllersTab(Application app, ApplicationEditorController controller)
  {
    super(app, controller, "Activities");
  }
}
