package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.parser.appparser.token.*;

public class WebTab extends EditTab {

  public WebTab(Application app, ApplicationEditorController controller)
  {
    super(app, controller, "Web");
  }
}
