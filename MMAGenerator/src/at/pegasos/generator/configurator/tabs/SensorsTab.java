package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;

public class SensorsTab extends EditTab {

  public SensorsTab(Application app, ApplicationEditorController controller)
  {
    super(app, controller, "Sensors");
  }
}
