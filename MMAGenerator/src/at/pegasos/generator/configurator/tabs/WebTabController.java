package at.pegasos.generator.configurator.tabs;

import org.reflections.*;
import org.reflections.scanners.*;
import org.reflections.util.*;

import java.lang.reflect.*;
import java.net.*;
import java.util.*;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.tabs.web.WebTab;
import at.pegasos.generator.parser.appparser.token.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.extern.slf4j.*;

@Slf4j
public class WebTabController extends EditTabController {

  @FXML
  VBox container;

  public WebTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    // Load all parser classes
    List<Class<? extends Node>> parsersClasses = fetchParsers(this.getClass().getPackage().getName());

    parsersClasses.stream()
            .sorted((a, b) -> {
              WebTab an = a.getAnnotationsByType(WebTab.class)[0];
              WebTab bn = b.getAnnotationsByType(WebTab.class)[0];
              return an.order() - bn.order();
            })
            .forEach(c -> {
              WebTab an = c.getAnnotationsByType(WebTab.class)[0];
              TitledPane pane = new TitledPane();
              pane.setText(an.name());
              try
              {
                Node x = c.getConstructor(TitledPane.class, WebTabController.class).newInstance(pane, WebTabController.this);
                pane.setContent(x);
                pane.expandedProperty().setValue(false);
                container.getChildren().add(pane);
              }
              catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e )
              {
                e.printStackTrace();
              }
            });
  }

  /**
   * @param packageName   package to be searched
   * @return
   */
  @SuppressWarnings("unchecked") // this is safe as we type check before adding to the list
  private List<Class<? extends Node>> fetchParsers(String packageName)
  {
    List<Class<? extends Node>> ret = new ArrayList<Class<? extends Node>>();

    FilterBuilder f = new FilterBuilder();
    f = f.include(FilterBuilder.prefix(packageName));

    Reflections reflections = new Reflections(
            new ConfigurationBuilder()
                    .setUrls(ClasspathHelper.forJavaClassPath())
                    .setScanners(new SubTypesScanner(false), new TypeAnnotationsScanner())
                    .filterInputsBy(f)
    );

    Set<Class<?>> allClasses = new HashSet<Class<?>>(reflections.getTypesAnnotatedWith(WebTab.class));

    for(Class<?> clasz : allClasses)
    {
      if( !Node.class.isAssignableFrom(clasz) )
      {
        log.info("Ignoring {} as it needs to implement {}", clasz, Node.class.getCanonicalName());
        continue;
      }

      ret.add((Class<? extends Node>) clasz);
    }

    return ret;
  }

  public ApplicationEditorController getMainController()
  {
    return mainController;
  }
}
