package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.*;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.appparser.token.Activity;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.*;
import javafx.scene.control.cell.*;

public class ControllersTabController extends EditTabController {

  @FXML
  Button add;

  @FXML
  TableView<Activity> tbl;

  @FXML
  TableColumn<Activity, String> idCol;

  @FXML
  TableColumn<Activity, String> classCol;

  @FXML
  TableColumn<Activity, Object> removeCol;

  private final ObservableList<Activity> entries;

  public ControllersTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    if( app.activities != null )
      this.entries= FXCollections.observableArrayList(app.activities);
    else
      this.entries= FXCollections.observableArrayList(Collections.emptyList());
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
 // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      Activity e= new Activity();
      entries.add(e);
      tbl.scrollTo(e);
      
      app.addActivity(e);

      this.mainController.setModified();
    });

    ObservableList<String> type= FXCollections.observableArrayList(Arrays.asList("string", "int", "boolean"));

    idCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().id));
    idCol.setCellFactory(TextFieldTableCell.forTableColumn());
    idCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    classCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().clasz));
    classCol.setCellFactory(TextFieldTableCell.forTableColumn());
    classCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    idCol.setOnEditCommit((CellEditEvent<Activity, String> event) -> {
      TablePosition<Activity, String> pos= event.getTablePosition();

      int row= pos.getRow();
      Activity entry= event.getTableView().getItems().get(row);
      entry.id= event.getNewValue();

      this.mainController.setModified();
    });

    classCol.setOnEditCommit((CellEditEvent<Activity, String> event) -> {
      TablePosition<Activity, String> pos= event.getTablePosition();

      int row= pos.getRow();
      Activity entry= event.getTableView().getItems().get(row);
      entry.clasz= event.getNewValue();

      this.mainController.setModified();
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Activity, Object> cell= new RemoveButtonTableCell<Activity, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        Activity e= entries.remove(index);
        
        app.removeActivity(e);

        this.mainController.setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }
}
