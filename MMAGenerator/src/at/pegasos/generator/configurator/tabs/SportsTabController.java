package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.*;

import at.pegasos.generator.configurator.Configurator;
import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.SportDialog;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.EntryString;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.*;

public class SportsTabController extends EditTabController {
  
  @FXML
  Button add;
  
  @FXML
  TableView<Sport> tbl;
  
  @FXML
  TableColumn<Sport, String> nameCol;
  @FXML
  TableColumn<Sport, String> fileCol;
  @FXML
  TableColumn<Sport, Object> editCol;
  @FXML
  TableColumn<Sport, Object> removeCol;

  private ObservableList<Sport> entries;
  
  static int sportIdx= 1;

  public SportsTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    if( app.getSports() != null )
      entries= FXCollections.observableArrayList(app.getSports());
    else
      entries= FXCollections.observableArrayList(Collections.emptyList());
    // notBuiltIn= new ArrayList<ObservableValue<Boolean>>(entries.size());
    // notBuiltIn= app.getSensors().getSensors().stream().map(s -> new SimpleBooleanProperty(!s.isBuiltin())).collect(Collectors.toList());
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      Sport s= new Sport("sport" + sportIdx, "sport-" + sportIdx++ + ".xml");
      s.icon= new EntryString(EntryString.TYPE_STRING, "edit");
      entries.add(s);
      tbl.scrollTo(s);
      app.getSports().add(s);
      mainController.setModified();
    });

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setOnEditCommit((CellEditEvent<Sport, String> event) -> {
      TablePosition<Sport, String> pos= event.getTablePosition();

      int row= pos.getRow();
      event.getTableView().getItems().get(row).name= event.getNewValue();
      this.app.getSports().get(row).name= event.getNewValue();
      mainController.setModified();
    });
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    fileCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().fileName));
    fileCol.setCellFactory(TextFieldTableCell.forTableColumn());
    fileCol.setOnEditCommit((CellEditEvent<Sport, String> event) -> {
      TablePosition<Sport, String> pos= event.getTablePosition();

      int row= pos.getRow();
      event.getTableView().getItems().get(row).fileName= event.getNewValue();
      this.app.getSports().get(row).fileName= event.getNewValue();
      mainController.setModified();
    });
    fileCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    editCol.setCellFactory(column -> {
      ButtonTableCell<Sport, Object> cell= new EditButtonTableCell<Sport, Object>();

      cell.getButton().setOnAction(event -> {
        int index= cell.getIndex();
        Sport sport= entries.get(index);

        SportDialog dlg= new SportDialog(Configurator.getPrimaryStage(), app.getSensors(), sport);
        Optional<Sport> ret= dlg.showAndWait();
        if( ret.isPresent() )
        {
          Sport s= ret.get();
          entries.set(index, s);
          app.getSports().set(index, s);
        }
      });

      return cell;
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Sport, Object> cell= new RemoveButtonTableCell<Sport, Object>();

      cell.getButton().setOnAction(event -> {
        int index= cell.getIndex();
        Sport sport= entries.remove(index);
        app.getSports().remove(sport);
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }
}
