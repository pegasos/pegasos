package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.*;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.appparser.token.Menu;
import at.pegasos.generator.parser.appparser.token.menu.*;
import at.pegasos.generator.parser.appparser.token.menu.MenuItem;
import at.pegasos.generator.parser.token.EntryString;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.*;

public class MenuTabController extends EditTabController {

  private class WrappedMenu {
    @Override
    public String toString()
    {
      return "WrappedMenu [menu=" + menu + ", isMain=" + isMain + "]";
    }

    Menu menu;

    public BooleanProperty isMain;

    public WrappedMenu(Menu menu)
    {
      this.menu= menu;
      this.isMain= new SimpleBooleanProperty(MenuTabController.this.app.getMain() == menu);
      isMain.addListener((v, oldVal, newVal) -> {
        System.out.println("main comment event " + v + " " + oldVal + " " + newVal + " " + this.menu);

        // We are now main
        if( newVal )
        {
          MenuTabController.this.app.main= this.menu;
          for(WrappedMenu om : MenuTabController.this.menus)
          {
            if( om != this && om.isMain.getValue() )
            {
              om.isMain.set(false);
            }
          }

          // We have not been main before --> need to remove us from the list
          MenuTabController.this.app.getMenus().remove(this.menu.name);
        }
        // We are no longer main
        else
        {
          MenuTabController.this.app.main= null;
          MenuTabController.this.app.getMenus().put(this.menu.name, this.menu);
        }
      });
    }
  }

  @FXML
  Button addMenu;

  @FXML
  TableView<WrappedMenu> tblMenus;

  ObservableList<WrappedMenu> menus;

  @FXML
  TableColumn<WrappedMenu, Boolean> mainCol;
  @FXML
  TableColumn<WrappedMenu,String> nameCol;
  @FXML
  TableColumn<WrappedMenu, Object> removeMenuCol;

  @FXML
  Button addEntry;
  @FXML
  TableView<MenuItem> tblEntries;
  ObservableList<MenuItem> entries;
  @FXML
  TableColumn<MenuItem,String> typeCol;
  @FXML
  TableColumn<MenuItem,String> iconCol;
  @FXML
  TableColumn<MenuItem,String> txtCol;
  @FXML
  TableColumn<MenuItem,Object> editEntryCol;
  @FXML
  TableColumn<MenuItem,Object> removeEntryCol;

  private WrappedMenu selectedMenu;

  private int menuIdx= 1;

  public MenuTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    menus= FXCollections.observableArrayList(app.getMenus().values().stream().map(m -> new WrappedMenu(m)).collect(Collectors.toSet()));
    if( app.getMain() != Menu.EmptyMenu )
      menus.add(0, new WrappedMenu(app.getMain()));
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    Tooltip tooltip= new Tooltip(addString);
    addMenu.setTooltip(tooltip);

    addMenu.setOnAction(event -> {
      Menu e= new Menu("menu-" + (menuIdx++));
      menus.add(new WrappedMenu(e));
      this.app.getMenus().put(e.name, e);
      this.mainController.setModified();
    });

    tblMenus.setItems(menus);
    tblMenus.setEditable(true);

    mainCol.setCellValueFactory(param -> param.getValue().isMain);
    mainCol.setCellFactory(column -> new CheckBoxTableCell<WrappedMenu, Boolean>());

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().menu.name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    nameCol.setOnEditCommit((CellEditEvent<WrappedMenu, String> event) -> {
      TablePosition<WrappedMenu, String> pos= event.getTablePosition();

      int row= pos.getRow();
      WrappedMenu entry= event.getTableView().getItems().get(row);
      entry.menu.name= event.getNewValue();
      this.mainController.setModified();
    });

    removeMenuCol.setCellFactory(column -> {
      ButtonTableCell<WrappedMenu, Object> cell= new RemoveButtonTableCell<WrappedMenu, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        WrappedMenu e= menus.remove(index);
        System.out.println(e);
        if( e.menu == app.getMain() )
        {
          app.main= null;
        }
        else
        {
          app.getMenus().remove(e.menu.name);
        }
        this.mainController.setModified();
      });

      return cell;
    });

    TableViewSelectionModel<WrappedMenu> selectionModel = tblMenus.getSelectionModel();
    selectionModel.setSelectionMode(SelectionMode.SINGLE);
    ObservableList<WrappedMenu> selectedMenu= selectionModel.getSelectedItems();
    selectedMenu.addListener(new ListChangeListener<WrappedMenu>() {
      @Override
      public void onChanged(Change<? extends WrappedMenu> change) {
        MenuTabController.this.selectedMenu= change.getList().get(0);
        addEntry.setDisable(false);
        loadEntries();
      }
    });

    addEntry.setDisable(true);
    addEntry.setOnAction(event -> {
      MenuItemActivity entry= new MenuItemActivity();
      MenuTabController.this.selectedMenu.menu.items.add(entry);
      entries.add(entry);
      tblEntries.scrollTo(entry);
      this.mainController.setModified();
    });

    typeCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().getClass().getSimpleName().substring(8)));
    typeCol.setCellFactory(ComboBoxTableCell.forTableColumn(new String[] {"Activity","Menu","Intent"}));
    typeCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    typeCol.setOnEditCommit((CellEditEvent<MenuItem,String> event) -> {
      TablePosition<MenuItem, String> pos= event.getTablePosition();

      int row= pos.getRow();
      String type= event.getNewValue();
      MenuItem entry= event.getTableView().getItems().get(row);
      MenuItem n;
      if( type.equals("Activity") )
      {
        n= new MenuItemActivity(entry);
      }
      else if( type.equals("Menu") )
      {
        n= new MenuItemMenu(entry);
      }
      else if( type.equals("Intent") )
      {
        n= new MenuItemIntent(entry);
      }
      else
      {
        n= null;
      }
      MenuTabController.this.selectedMenu.menu.items.set(row, n);
      entries.set(row, n);
      this.mainController.setModified();
    });

    iconCol.setCellValueFactory(
        param -> new SimpleObjectProperty<String>(param.getValue().icon != null ? param.getValue().icon.raw() : ""));
    iconCol.setCellFactory(TextFieldTableCell.forTableColumn());
    iconCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    iconCol.setOnEditCommit((CellEditEvent<MenuItem, String> event) -> {
      TablePosition<MenuItem, String> pos= event.getTablePosition();

      int row= pos.getRow();
      MenuItem entry= event.getTableView().getItems().get(row);
      String val= event.getNewValue();
      if( val.equals("" ) )
      {
        entry.icon= null;
      }
      else if( val.startsWith("R.") )
      {
        entry.icon= new EntryString(EntryString.TYPE_ID, val);
      }
      else
      {
        entry.icon= new EntryString(EntryString.TYPE_STRING, val);
      }
      this.mainController.setModified();
    });

    txtCol.setCellValueFactory(
        param -> new SimpleObjectProperty<String>(param.getValue().text != null ? param.getValue().text.raw() : ""));
    txtCol.setCellFactory(TextFieldTableCell.forTableColumn());
    txtCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    txtCol.setOnEditCommit((CellEditEvent<MenuItem, String> event) -> {
      TablePosition<MenuItem, String> pos= event.getTablePosition();

      int row= pos.getRow();
      MenuItem entry= event.getTableView().getItems().get(row);
      String val= event.getNewValue();
      if( val.equals("" ) )
      {
        entry.text= null;
      }
      else if( val.startsWith("R.") )
      {
        entry.text= new EntryString(EntryString.TYPE_ID, val);
      }
      else
      {
        entry.text= new EntryString(EntryString.TYPE_STRING, val);
      }
      this.mainController.setModified();
    });
    editEntryCol.setCellFactory(column -> {
      ButtonTableCell<MenuItem, Object> cell= new EditButtonTableCell<MenuItem, Object>();

      cell.getButton().setOnAction(event -> {
        int index= cell.getIndex();
        MenuItem entry= entries.get(index);

        MenuItemEditDialog dlg= null;;
        if( entry instanceof MenuItemActivity )
        {
          dlg= new MenuItemEditActivityDialog(Configurator.getPrimaryStage(), (MenuItemActivity) entry);
        }
        else if( entry instanceof MenuItemMenu )
        {

        }
        else if( entry instanceof MenuItemIntent )
        {

        }

        Optional<MenuItem> ret= dlg.showAndWait();
        if( ret.isPresent() )
        {
          entries.set(index, ret.get());
          MenuTabController.this.selectedMenu.menu.items.set(index, ret.get());
        }
      });

      return cell;
    });
    removeEntryCol.setCellFactory(column -> {
      ButtonTableCell<MenuItem, Object> cell= new RemoveButtonTableCell<MenuItem, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();

        MenuItem entry = MenuTabController.this.selectedMenu.menu.items.remove(index);
        entries.remove(entry);
/*
        WrappedMenu e= menus.remove(index);
        System.out.println(e);
        if( e.menu == app.getMain() )
        {
          app.main= null;
        }
        else
        {
          app.getMenus().remove(e.menu.name);
        }
*/
        this.mainController.setModified();
      });

      return cell;
    });

    tblEntries.setEditable(true);
  }

  private void loadEntries()
  {
    entries= FXCollections.observableArrayList(selectedMenu.menu.items);
    tblEntries.setItems(entries);
  }
}
