package at.pegasos.generator.configurator.tabs;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.configurator.dialog.ImplementationDialog;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.appparser.token.Sensor;
import at.pegasos.generator.parser.appparser.token.Sensor.Clasz;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.*;
import lombok.extern.slf4j.*;

@Slf4j
public class SensorsTabController extends EditTabController {
  
  private class WrappedSensor {

    Sensor sensor;

    public BooleanProperty isBuiltIn;

    public WrappedSensor(Sensor sensor)
    {
      this.sensor= sensor;
      this.isBuiltIn= new SimpleBooleanProperty(sensor.isBuiltin());
      isBuiltIn.addListener((v, oldVal, newVal) -> {
        sensor.setBuiltIn(newVal);
        app.getSensors().update();
      });
    }
  }

  @FXML
  Button add;
  
  @FXML
  TableView<WrappedSensor> tbl;
  
  @FXML
  TableColumn<WrappedSensor, Boolean> builtinCol;
  @FXML
  TableColumn<WrappedSensor, String> nameCol;
  @FXML
  TableColumn<WrappedSensor, String> iconCol;
  @FXML
  TableColumn<WrappedSensor, Object> implCol;
  @FXML
  TableColumn<WrappedSensor, Object> removeCol;

  // private List<ObservableValue<Boolean>> notBuiltIn;

  private final ObservableList<WrappedSensor> entries;

  public SensorsTabController(Application app, ApplicationEditorController controller)
  {
    super(app, controller);

    entries = FXCollections
        .observableArrayList(app.getSensors().getSensors().stream().map(m -> new WrappedSensor(m)).collect(Collectors.toSet()));
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      Sensor s= Sensor.dummyEntry();
      WrappedSensor ws= new WrappedSensor(s);
      entries.add(ws);
      app.getSensors().addSensor(s);
      tbl.scrollTo(ws);
    });

    builtinCol.setCellValueFactory(param -> param.getValue().isBuiltIn);
    builtinCol.setCellFactory(column -> new CheckBoxTableCell<WrappedSensor, Boolean>());

    nameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().sensor.name));
    nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
    nameCol.setOnEditCommit((CellEditEvent<WrappedSensor, String> event) -> {
      TablePosition<WrappedSensor, String> pos= event.getTablePosition();

      int row= pos.getRow();
      WrappedSensor entry= event.getTableView().getItems().get(row);
      entry.sensor.name= event.getNewValue();
      app.getSensors().update();
    });
    nameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    iconCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().sensor.icon));
    iconCol.setCellFactory(TextFieldTableCell.forTableColumn());
    iconCol.setOnEditCommit((CellEditEvent<WrappedSensor, String> event) -> {
      TablePosition<WrappedSensor, String> pos = event.getTablePosition();

      int row = pos.getRow();
      WrappedSensor entry = event.getTableView().getItems().get(row);
      entry.sensor.icon = event.getNewValue();

      app.getSensors().update();
    });
    iconCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    implCol.setCellFactory(column -> {
      ButtonTableCell<WrappedSensor, Object> cell= new ButtonTableCell<WrappedSensor, Object>("...");

      cell.getButton().setOnAction(event -> {
        int index= cell.getIndex();
        WrappedSensor sensor= entries.get(index);

        ImplementationDialog dlg = new ImplementationDialog(Configurator.getPrimaryStage(), sensor.sensor.classes, this.mainController);
        Optional<List<Clasz>> ret = dlg.showAndWait();
        if( ret.isPresent() )
        {
          sensor.sensor.classes = ret.get();
          app.getSensors().update();
        }
      });

      return cell;
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<WrappedSensor, Object> cell= new RemoveButtonTableCell<WrappedSensor, Object>();

      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        WrappedSensor sensor = entries.remove(index);
        app.getSensors().removeSensor(sensor.sensor);
        this.mainController.setModified();
      });

      return cell;
    });

    tbl.setItems(entries);
    tbl.setEditable(true);
  }
}
