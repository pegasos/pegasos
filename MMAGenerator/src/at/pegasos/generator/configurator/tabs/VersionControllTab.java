package at.pegasos.generator.configurator.tabs;

import at.pegasos.generator.configurator.ApplicationEditorController;
import at.pegasos.generator.parser.appparser.token.Application;

public class VersionControllTab extends EditTab {

  public VersionControllTab(Application app, ApplicationEditorController controller)
  {
    super(app, controller, "Version Control");
  }
}
