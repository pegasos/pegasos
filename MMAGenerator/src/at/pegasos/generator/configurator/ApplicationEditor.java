package at.pegasos.generator.configurator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.appparser.ApplicationParser;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.stage.Stage;

public class ApplicationEditor extends Tab {

  private final ConfiguratorController configurationController;
  private ApplicationEditorController controller;
  
  private final Application app;

  private Tab root;

  public ApplicationEditor(File f, Stage primaryStage, ConfiguratorController configuratorController) throws ParserException, IOException
  {
    super("loading ...");

    this.configurationController = configuratorController;

    Path path= Paths.get(f.getAbsolutePath());
    
    ApplicationParser parser= new ApplicationParser(path, false, false);
    parser.setLibraries(configuratorController.getLibraries());
    parser.parse();
    app= parser.getApplication();
    
    initUi(primaryStage);
  }

  public ApplicationEditor(Application application, Stage primaryStage, ConfiguratorController configurationController) throws IOException
  {
    this.app= application;
    this.configurationController = configurationController;
    initUi(primaryStage);
  }

  private void initUi(Stage primaryStage) throws IOException
  {
    this.controller= new ApplicationEditorController(primaryStage, configurationController);

    FXMLLoader loader= new FXMLLoader();
    loader.setLocation(this.getClass().getResource("ApplicationEditor.fxml"));
    loader.setController(this.controller);
    loader.setRoot(this);
    root= loader.load();

    if( app.name != null && !app.name.isEmpty() )
    {
      setText(app.name);
    }
    else
      setText("<unnamed>");

    // Send the data to the controller
    controller.setApplication(root, app);
  }

  public Application getApp()
  {
    return app;
  }

  public void save()
  {
    this.controller.onSaveApplication();
  }
  
  /**
   * Check if closing this tab without side effects is possible. If not attempt to make precautions
   * for closing without side effects
   * 
   * @return true if possible
   */
  public boolean attemptClose()
  {
    if( controller.isModified() )
    {
      return controller.askSave();
    }
    else
    {
      return true;
    }
  }
}
