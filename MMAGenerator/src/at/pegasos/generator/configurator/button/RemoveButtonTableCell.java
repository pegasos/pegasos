package at.pegasos.generator.configurator.button;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;

/**
 * Shortcut for creating a remove button table cell.
 */
public class RemoveButtonTableCell<T, S> extends ButtonTableCell<T, S> {
  // private final String removeString = Globals.getInstance().getRes().getString("remove");
  private String removeString= "remove";

  /**
   * Initializes the class.
   */
  public RemoveButtonTableCell()
  {
    super(null);

    this.getButton().getStyleClass().add("icon-delete");
    this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip= new Tooltip(this.removeString);
    this.setTooltip(tooltip);
  }
}