package at.pegasos.generator.configurator.button;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;

/**
 * Shortcut for creating a remove button table cell.
 */
public class EditButtonTableCell<T, S> extends ButtonTableCell<T, S> {
  // private final String removeString = Globals.getInstance().getRes().getString("remove");
  private String editString= "edit";

  /**
   * Initializes the class.
   */
  public EditButtonTableCell()
  {
    super(null);

    this.getButton().getStyleClass().add("icon-edit");
    this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip= new Tooltip(this.editString);
    this.setTooltip(tooltip);
  }
}