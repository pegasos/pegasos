package at.pegasos.generator.configurator.button;

import at.pegasos.generator.parser.appparser.token.*;
import javafx.event.*;
import javafx.scene.control.*;

/**
 * Button for cells in TableViews. The call is simply bound to an Object, although it has no special
 * use.
 */
public class AddButton extends Button {
  private final String addString = "add";

  /**
   * Creates a table-cell-button.
   * 
   */
  public AddButton(EventHandler<ActionEvent> event)
  {
    getStyleClass().add("icon-add");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip(addString);
    setTooltip(tooltip);

    setOnAction(event);
  }

  public AddButton()
  {
    getStyleClass().add("icon-add");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip(addString);
    setTooltip(tooltip);
  }
}
