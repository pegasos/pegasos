package at.pegasos.generator.configurator.button;

import javafx.geometry.Pos;
import javafx.scene.control.*;

/**
 * Button for cells in TableViews. The call is simply bound to an Object, although it has no special
 * use.
 */
public class ButtonTableCell<T, S> extends TableCell<T, S> {
  private final Button button;
  
  /**
   * Creates a table-cell-button.
   * 
   * @param label
   *          Label for the button
   */
  public ButtonTableCell(String label)
  {
    this.button= new Button(label);
    
    this.setAlignment(Pos.CENTER);
  }
  
  /**
   * Gets the actual button from the cell.
   * 
   * @return Button
   */
  public Button getButton()
  {
    return this.button;
  }
  
  @Override
  protected void updateItem(S item, boolean empty)
  {
    super.updateItem(item, empty);
    
    // If the row is not empty, show a button
    if( empty )
    {
      this.setGraphic(null);
      return;
    }
    
    this.setGraphic(this.button);
  }
}
