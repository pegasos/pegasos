package at.pegasos.generator.configurator;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.prefs.Preferences;

import at.pegasos.generator.configurator.dialog.ExceptionDialog;
import at.pegasos.generator.configurator.project.*;
import at.pegasos.generator.configurator.writer.WriterException;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.*;
import at.pegasos.generator.parser.appparser.token.Application;
import at.univie.mma.MMAGenerator;
import at.pegasos.generator.parser.token.Instance;
import javafx.application.Platform;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.stage.*;
import lombok.extern.slf4j.*;

@Slf4j
public class ConfiguratorController implements Initializable {
  
  private final static String LAST_APP= "LAST_APP";
  private final static String LAST_PROJECT= "LAST_PROJ";

  @FXML
  private TabPane applications;

  @FXML
  private TitledPane output;

  private final Stage primaryStage;
  
  private ApplicationEditor app;

  private Project project;

  private Map<String, Application> libraries;
  
  public ConfiguratorController(Stage primaryStage)
  {
    this.primaryStage= primaryStage;
  }

  public void initialize(URL location, ResourceBundle res)
  {
    // String javaVersion= System.getProperty("java.version");
    // String javafxVersion= System.getProperty("javafx.version");
    // label.setText("Hello, JavaFX " + javafxVersion + "\nRunning on Java " + javaVersion + ".");
    
    output.setContent(new Logoutput());

    primaryStage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closingEvent);
    
    applications.getSelectionModel().selectedItemProperty().addListener((tab, oldTab, newTab) -> {
      if( newTab != null )
      {
        app= ((ApplicationEditor) newTab);
      }
    });

    Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
    String lastproj= pref.get(LAST_PROJECT, null);

    if( Configurator.Arguments.inst.input != null )
    {
      ProjectParser parser = new ProjectParser(Paths.get(Configurator.Arguments.inst.input));
      try
      {
        parser.parse();
        this.project = parser.getProject();
      }
      catch( ParserException e )
      {
        e.printStackTrace();
        // keep silent about this mishap
        // new ExceptionDialog(e, "Opening project failed").showAndWait();
        this.project = new Project();
      }
    }
    else if( lastproj != null && Files.exists(Paths.get(lastproj)) )
    {
      ProjectParser parser= new ProjectParser(Paths.get(lastproj));
      try
      {
        // Log.debug("Opening project {}", lastproj);
        parser.parse();
        this.project= parser.getProject();
      }
      catch( ParserException e )
      {
        e.printStackTrace();
        // keep silent about this mishap
        // new ExceptionDialog(e, "Opening project failed").showAndWait();
        this.project= new Project();
      }
    }
    else
      project= new Project();

    openProject(project);
  }

  private void openProject(Project p)
  {
    log.debug("Opening project");
    try
    {
      parseLibraries(p);
      for(String app : p.open_apps)
      {
        openApp(new File(app));
      }
    }
    catch( ParserException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Opening project failed").showAndWait();
    }
  }

  private void parseLibraries(Project p) throws ParserException
  {
    log.debug("parsing libraries {}", p);
    libraries = new HashMap<>();
    Iterable<String> input;
    if( p.propertyApps().size() == 0 )
      input = List.of(p.path.getParent().toString());
    else
      input = p.propertyApps();
    for(String inputString : input)
    {
      LibraryHelper.parseInputDir(Paths.get(inputString), libraries);
    }
  }

  public void onNewProject()
  {
    // TODO: close everything before?
    this.project= new Project();
  }

  public void onOpenProject()
  {
    FileChooser chooser= new FileChooser();
    chooser.setTitle("Open Project");
    // chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Project", "*.project"));

    Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
    String lastproj= pref.get(LAST_PROJECT, null);
    if( lastproj != null )
    {
      chooser.setInitialDirectory(Paths.get(lastproj).getParent().toFile());
    }

    File f= chooser.showOpenDialog(primaryStage);
    if( f != null )
    {
      // TODO: close everything before?
      ProjectParser parser= new ProjectParser(Paths.get(f.getAbsolutePath()));
      try
      {
        parser.parse();
        this.project= parser.getProject();
        openProject(project);

        this.project.path= Paths.get(f.getAbsolutePath());
        pref.put(LAST_PROJECT, project.path.toAbsolutePath().toString());
      }
      catch( ParserException e )
      {
        e.printStackTrace();
        new ExceptionDialog(e, "Opening project failed").showAndWait();
      }
    }
  }

  public boolean onSaveProject()
  {
    Path path= null;
    boolean isNew= false;

    if( project.path == null )
    {
      isNew= true;
      FileChooser chooser= new FileChooser();
      chooser.setTitle("Open Project");
      chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Project", "project"));

      Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
      String lastproj= pref.get(LAST_PROJECT, null);
      if( lastproj != null )
      {
        chooser.setInitialDirectory(Paths.get(lastproj).getParent().toFile());
      }

      File f= chooser.showSaveDialog(primaryStage);
      if( f != null )
      {
        path= Paths.get(f.getAbsolutePath());
      }
    }
    else
    {
      path= project.path;
    }

    if( path != null )
    {
      ProjectWriter writer= new ProjectWriter(path, this.project);
      try
      {
        writer.write();
        this.project.path= path;

        Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
        pref.put(LAST_PROJECT, project.path.toAbsolutePath().toString());

        if( isNew )
        {
          project.open_apps.add(project.path.relativize(path).toString());
          project.setModified();
        }

        return true;
      }
      catch( WriterException e )
      {
        e.printStackTrace();
        new ExceptionDialog(e, "Saving project failed").showAndWait();
      }
    }

    return false;
  }

  public void onConfigureProject()
  {
    ProjectEditor dlg= new ProjectEditor(project, primaryStage);
    Optional<Project> ret= dlg.showAndWait();
    if( ret.isPresent() )
    {
      this.project= ret.get();
    }
  }

  public void onBuildProject()
  {
    MMAGenerator g= new MMAGenerator(project.getParams());

    try
    {
      g.run();
    }
    catch( Exception e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Building project failed").showAndWait();
    }
  }

  public void onOpenApplication()
  {
    DirectoryChooser chooser= new DirectoryChooser();
    chooser.setTitle("Open Application");
    
    Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
    String dir= pref.get(LAST_APP, ".");
    
    chooser.setInitialDirectory(new File(dir));
    
    File f= chooser.showDialog(primaryStage);
    if( f != null )
    {
      if( openApp(f) )
      {
        project.open_apps.add(f.toString());
        project.setModified();
      }
    }
  }

  /**
   * Open app
   * @param directory location of the application / library (only the directory)
   * @return true if open was successful
   */
  private boolean openApp(File directory)
  {
    try
    {
      if( !directory.isAbsolute() )
      {
        directory = project.path.getParent().resolve(directory.getPath()).toFile();
      }
      ApplicationEditor tab = new ApplicationEditor(directory, primaryStage, this);
      applications.getTabs().add(tab);
      applications.getSelectionModel().select(tab);

      Preferences preferences= Preferences.userRoot().node(getClass().getCanonicalName());
      preferences.put(LAST_APP, directory.toString());

      return true;
    }
    catch( ParserException | IOException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Opening Application failed").showAndWait();
    }

    return false;
  }

  public void onNewApplication()
  {
    try
    {
      // new ApplicationEditor(f);
      Tab tab = new ApplicationEditor(new Application(new Instance()), primaryStage, this);
      applications.getTabs().add(tab);
      applications.getSelectionModel().select(tab);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Creating new Application failed").showAndWait();
    }
  }

  public void onSaveApplication()
  {
    if( app != null )
    {
      app.save();
    }
  }

  public void onExitApplication()
  {
    for(int i= 0; i < applications.getTabs().size(); i++)
    {
      ApplicationEditor tab= (ApplicationEditor) applications.getTabs().get(i);
      if( !tab.attemptClose() )
      {
        return;
      }
    }
    if( project.isModified() && !askSaveProject() )
    {
      return;
    }
    Platform.exit();
  }

  private void closingEvent(WindowEvent event)
  {
    while( applications.getTabs().size() > 0 )
    {
      ApplicationEditor tab= (ApplicationEditor) applications.getTabs().get(0);
      if( !tab.attemptClose() )
      {
        event.consume();
        return;
      }
      else
      {
        applications.getTabs().remove(0);
      }
    }
    if( project.isModified() && !askSaveProject() )
    {
      event.consume();
    }
  }

  /**
   * Ask the user if he/she wants to save the project
   * 
   * @return false if operation was aborted
   */
  private boolean askSaveProject()
  {
    Alert alert= new Alert(AlertType.CONFIRMATION);
    alert.setTitle("Modified");
    alert.setHeaderText("The current project has been modified");
    alert.setContentText("Do you want to save?");

    ButtonType buttonTypeSave= new ButtonType("Save");
    ButtonType buttonTypeNo= new ButtonType("No");
    ButtonType buttonTypeCancel= new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

    alert.getButtonTypes().setAll(buttonTypeSave, buttonTypeNo, buttonTypeCancel);

    Optional<ButtonType> result= alert.showAndWait();
    if( result.get() == buttonTypeSave )
    {
      // ... user chose "Save"
      if( !onSaveProject() )
        return false;
    }
    else if( result.get() == buttonTypeNo )
    {
      // ... user chose "No"
      return true;
    }
    else
    {
      // ... user chose CANCEL or closed the dialog
      return false;
    }

    return true;
  }

  public Project getProject()
  {
    return project;
  }

  public Map<String, Application> getLibraries()
  {
    return libraries;
  }
}
