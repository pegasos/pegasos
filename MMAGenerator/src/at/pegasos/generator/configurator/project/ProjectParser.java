package at.pegasos.generator.configurator.project;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import javax.xml.parsers.*;

import at.pegasos.generator.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import at.pegasos.generator.parser.*;

public class ProjectParser {
  private Document doc;

  private final Path path;

  private Project project;

  private final Parameters params;

  public ProjectParser(Path path)
  {
    this.path= path;
    this.params= Parameters.get().copy();
  }

  public void parse() throws ParserException
  {
    openFile();

    NodeList parameters= ((Element) doc.getElementsByTagName("parameters").item(0)).getElementsByTagName("param");

    final int pL= parameters.getLength();
    for(int i= 0; i < pL; i++)
    {
      Element param= (Element) parameters.item(i);

      String type= XMLHelper.getAttribute(param, "type", null);
      String name= XMLHelper.getAttribute(param, "name", null);

      if( type.equals("string") )
      {
        try
        {
          params.getClass().getField(name).set(params, param.getFirstChild().getNodeValue());
        }
        catch( IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException | DOMException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      else if( type.equals("boolean") )
      {
        try
        {
          params.getClass().getField(name).setBoolean(params, Boolean.parseBoolean(param.getFirstChild().getNodeValue()));
        }
        catch( IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException | DOMException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      else if( type.equals("list") )
      {
        NodeList entries= param.getElementsByTagName("entry");
        final int eL= entries.getLength();
        List<String> list= new ArrayList<String>(eL);

        for(int j= 0; j < eL; j++)
        {
          list.add(entries.item(j).getFirstChild().getNodeValue());
        }

        try
        {
          params.getClass().getField(name).set(params, list);
        }
        catch( IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException | DOMException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }

    project= new Project(params);
    project.path= path;

    NodeList openapps= doc.getElementsByTagName("openapps");
    if( openapps.getLength() > 0 )
    {
      NodeList apps= ((Element) openapps.item(0)).getElementsByTagName("app");
      final int pA= apps.getLength();
      for(int i= 0; i < pA; i++)
      {
        Element openapp= (Element) apps.item(i);

        if( openapp.getFirstChild() != null )
        {
          String app = openapp.getFirstChild().getNodeValue();
          project.open_apps.add(app);
        }
      }
    }
  }

  private void openFile() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      doc= dBuilder.parse(path.toFile());

      doc.getDocumentElement().normalize();
    }
    catch( SAXException | IOException | ParserConfigurationException e )
    {
//      throw new ParserException("Could not open " + path.toAbsolutePath() + " " + e.getMessage());
      throw new ParserException(e);
    }
  }

  public Project getProject()
  {
    return project;
  }
}
