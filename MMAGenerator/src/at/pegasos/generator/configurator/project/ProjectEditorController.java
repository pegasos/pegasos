package at.pegasos.generator.configurator.project;

import java.io.File;
import java.net.URL;
import java.nio.file.*;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import at.pegasos.generator.configurator.button.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.*;

public class ProjectEditorController implements Initializable {

  private static final String LAST_DIRECTORY= ProjectEditorController.class.getName() + ".LAST_DIR";

  @FXML
  TextField name;

  @FXML
  Button addServer;
  @FXML
  TableView<String> tblServers;
  @FXML
  TableColumn<String, String> serversDirectoryCol;
  @FXML
  TableColumn<String, Object> serversRemoveCol;

  @FXML
  Button addApp;
  @FXML
  TableView<String> tblApps;
  @FXML
  TableColumn<String, String> appsDirectoryCol;
  @FXML
  TableColumn<String, Object> appsRemoveCol;
  
  @FXML
  RadioButton buildall;
  @FXML
  RadioButton serversonly;
  @FXML
  RadioButton appsonly;
  
  @FXML
  RadioButton cleanAll;
  @FXML
  RadioButton cleanNone;
  @FXML
  RadioButton cleanSelected;
  @FXML
  CheckBox cleanApps;
  @FXML
  CheckBox cleanServers;

  private Project project;
  private Project projectOrig;

  private Stage primaryStage;

  public ProjectEditorController(Project project, Stage primaryStage)
  {
    // entries= FXCollections.observableList(this.app.servers);
    this.project= project.copy();
    this.projectOrig= project;
    this.primaryStage= primaryStage;
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
//    name.setText(app.name);

    initBuildFlags();
    initCleanFlags();
    initX();
    
    set();
  }
  
  private void set()
  {
    setBuildFlags();
    setCleanFlags();
  }
  
  private void initBuildFlags()
  {
    final ToggleGroup group = new ToggleGroup();
    
    buildall.setToggleGroup(group);
    serversonly.setToggleGroup(group);
    appsonly.setToggleGroup(group);
    
    appsonly.selectedProperty().addListener((v, oldV, newV) -> {
      project.propertyAppsOnly().set(newV);
    });

    serversonly.selectedProperty().addListener((v, oldV, newV) -> {
      project.propertyServersOnly().set(newV);
    });
  }

  private void setBuildFlags()
  {
    if( !project.propertyAppsOnly().get() && !project.propertyServersOnly().get() )
    {
      buildall.setSelected(true);
    }
    else
    {
      if( project.propertyAppsOnly().get() )
      {
        appsonly.setSelected(true);
      }
      else
      {
        serversonly.setSelected(true);
      }
    }
  }
  
  private void initCleanFlags()
  {
    final ToggleGroup group= new ToggleGroup();
    
    cleanAll.setToggleGroup(group);
    cleanNone.setToggleGroup(group);
    cleanSelected.setToggleGroup(group);
    
    cleanAll.selectedProperty().addListener((v, oldV, newV) -> {
      project.propertyClean().set(newV);
      cleanApps.setSelected(false);
      cleanServers.setSelected(false);
    });
    
    cleanApps.selectedProperty().addListener((v, oldV, newV) -> {
      project.propertyCleanApps().set(newV);
      cleanSelected.setSelected(true);
    });
    
    cleanServers.selectedProperty().addListener((v, oldV, newV) -> {
      project.propertyCleanServers().set(newV);
      cleanSelected.setSelected(true);
    });
  }
  
  private void setCleanFlags()
  {
    if( !project.propertyClean().get() && !project.propertyCleanApps().get() && !project.propertyCleanServers().get() )
    {
      cleanNone.setSelected(true);
      cleanApps.setSelected(false);
      cleanServers.setSelected(false);
    }
    else
    {
      if( project.propertyClean().get() )
      {
        cleanAll.setSelected(true);
        cleanApps.setSelected(false);
        cleanServers.setSelected(false);
      }
      else
      {
        cleanSelected.setSelected(true);
        if( project.propertyCleanApps().get() )
        {
          cleanApps.setSelected(true);
        }
        if( project.propertyCleanServers().get() )
        {
          cleanServers.setSelected(true);
        }
      }
    }
  }

  private void initX()
  {
    // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString= "add";

    addServer.getStyleClass().add("icon-add");
    addServer.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip= new Tooltip(addString);
    addServer.setTooltip(tooltip);

    addServer.setOnAction(event -> {
      String e= new String();
      project.propertyServers().add(e);
      tblServers.edit(project.propertyServers().size() - 1, serversDirectoryCol);
      tblServers.scrollTo(e);
    });

    serversDirectoryCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue()));
    serversDirectoryCol.setCellFactory(TextFieldTableCell.forTableColumn());
    serversDirectoryCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    serversDirectoryCol.setOnEditStart(event -> {
      DirectoryChooser chooser= new DirectoryChooser();
      chooser.setTitle("Open Direcotry");
      
      boolean empty= event.getOldValue().isEmpty();
      if( empty )
      {
        Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
        String dir= pref.get(LAST_DIRECTORY, ".");
        chooser.setInitialDirectory(new File(dir));
      }
      else
      {
        chooser.setInitialDirectory(new File(event.getOldValue()));
      }
      
      File f= chooser.showDialog(primaryStage);
      if( f != null )
      {
        int index = event.getTablePosition().getRow();
        tblServers.getItems().set(index, f.toString());
        
        Preferences preferences= Preferences.userRoot().node(getClass().getCanonicalName());
        preferences.put(LAST_DIRECTORY, f.toString());
      }
      event.consume();
    });

    serversRemoveCol.setCellFactory(column -> {
      ButtonTableCell<String, Object> cell= new RemoveButtonTableCell<String, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        project.propertyServers().remove(index);
      });

      return cell;
    });

    tblServers.setItems(project.propertyServers());
    tblServers.setEditable(true);
    
    addApp.setOnAction(event -> {
      String e= new String();
      project.propertyApps().add(e);
      tblApps.edit(project.propertyApps().size() - 1, appsDirectoryCol);
      tblApps.scrollTo(e);
    });

    appsDirectoryCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue()));
    appsDirectoryCol.setCellFactory(TextFieldTableCell.forTableColumn());
    appsDirectoryCol.setStyle( "-fx-alignment: CENTER-LEFT;");
    appsDirectoryCol.setOnEditStart(event -> {
      DirectoryChooser chooser= new DirectoryChooser();
      chooser.setTitle("Open Direcotry");
      
      boolean empty= event.getOldValue().isEmpty();
      if( empty )
      {
        Preferences pref= Preferences.userRoot().node(getClass().getCanonicalName());
        String dir= pref.get(LAST_DIRECTORY, ".");
        chooser.setInitialDirectory(new File(dir));
      }
      else if( Files.exists(Paths.get(event.getOldValue())) )
      {
        chooser.setInitialDirectory(new File(event.getOldValue()));
      }
      
      File f= chooser.showDialog(primaryStage);
      if( f != null )
      {
        int index = event.getTablePosition().getRow();
        tblApps.getItems().set(index, f.toString());
        
        Preferences preferences= Preferences.userRoot().node(getClass().getCanonicalName());
        preferences.put(LAST_DIRECTORY, f.toString());
      }
      event.consume();
    });

    appsRemoveCol.setCellFactory(column -> {
      ButtonTableCell<String, Object> cell= new RemoveButtonTableCell<String, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        project.propertyApps().remove(index);
      });

      return cell;
    });

    tblApps.setItems(project.propertyApps());
    tblApps.setEditable(true);
  }

  public void resetProject()
  {
    this.project= projectOrig.copy();
    set();
  }

  public Project getProject()
  {
    return project;
  }
}
