package at.pegasos.generator.configurator.project;

import java.io.IOException;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.List;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import at.pegasos.generator.*;
import org.w3c.dom.Element;

import at.pegasos.generator.configurator.writer.*;

public class ProjectWriter extends Writer {

  private Path path;
  private Project project;

  public ProjectWriter(Path path, Project project)
  {
    super(null, null);
    this.path= path;
    this.project= project;
  }

  @Override
  public void write() throws WriterException
  {
    try
    {
      start();

      Parameters params= project.getParams();

      Field[] fields = Parameters.class.getFields();
      Element parameters= document.createElement("parameters");
      for(Field f : fields)
      {
        if( Modifier.isPublic(f.getModifiers()) )
        {
          Element param= document.createElement("param");
          param.setAttribute("name", f.getName());
          if( f.getType().equals(String.class))
          {
            param.setAttribute("type", "string");
            param.appendChild(document.createTextNode(params.getClass().getField(f.getName()).get(params).toString()));
          }
          else if( f.getType().equals(boolean.class))
          {
            param.setAttribute("type", "boolean");
            param.appendChild(document.createTextNode("" + params.getClass().getField(f.getName()).getBoolean(params)));
          }
          else if( f.getType().equals(List.class))
          {
            param.setAttribute("type", "list");

            @SuppressWarnings("unchecked")
            List<? extends Object> list= (List<? extends Object>) params.getClass().getField(f.getName()).get(params);
            for(Object o : list)
            {
              Element e= document.createElement("entry");
              e.appendChild(document.createTextNode(o.toString()));
              param.appendChild(e);
            }
          }
          parameters.appendChild(param);
        }
      }

      root.appendChild(parameters);

      Element apps= document.createElement("openapps");
      for(String openapp : project.open_apps)
      {
        Element app= document.createElement("app");
        app.appendChild(document.createTextNode(openapp));
        apps.appendChild(app);
      }
      if( project.open_apps.size() > 0 )
      {
        root.appendChild(apps);
      }

      finish();
    }
    catch( ParserConfigurationException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }

  private void start() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder= documentFactory.newDocumentBuilder();
    document= documentBuilder.newDocument();

    // root element
    root= document.createElement("project");
    document.appendChild(root);
  }

  private void finish() throws WriterException
  {
    try
    {
      // create the xml file
      // transform the DOM Object to an XML File
      TransformerFactory transformerFactory= TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", 4);
      Transformer transformer= transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource= new DOMSource(document);

      Path p= path;
      if( !Files.exists(p.getParent()) )
      {
        Files.createDirectories(p.getParent());
      }

      StreamResult streamResult= new StreamResult(p.toFile());

      transformer.transform(domSource, streamResult);
    }
    catch( TransformerException | IOException e )
    {
      e.printStackTrace();
      throw new WriterException(e);
    }
  }
}