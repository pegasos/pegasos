package at.pegasos.generator.configurator.project;

import java.io.IOException;

import at.pegasos.generator.configurator.dialog.ExceptionDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;

public class ProjectEditor extends Dialog<Project> {

  public ProjectEditor(Project project, Stage primaryStage)
  {
    super();
    initOwner(primaryStage);
    
    try
    {
      FXMLLoader loader= new FXMLLoader();
      loader.setLocation(this.getClass().getResource("ProjectEditor.fxml"));
      // loader.setRoot(this);
      ProjectEditorController controller= new ProjectEditorController(project, primaryStage);
      loader.setController(controller);
      // root= (DialogPane) loader.load();
      
      DialogPane root= new DialogPane();
      root.setContent(loader.load());

      setDialogPane(root);

      ButtonType okBtnType= new ButtonType("OK", ButtonBar.ButtonData.APPLY);
      ButtonType resetBtnType= new ButtonType("Reset", ButtonBar.ButtonData.BACK_PREVIOUS);
      ButtonType closeBtnType= new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

      root.getButtonTypes().addAll(okBtnType, resetBtnType, closeBtnType);

      Button resetBtn= (Button) root.lookupButton(resetBtnType);

      resetBtn.addEventFilter(ActionEvent.ACTION, (evt) -> {
        evt.consume();
        controller.resetProject();
      });
      
      setResultConverter((btnType) -> {
        if( btnType == okBtnType )
        {
          // unfillValues();
          return controller.getProject();
        }
        else
          return null;
      });

      setResizable(true);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Showing ProjectEditor failed");
    }
  }
}
