package at.pegasos.generator.configurator.project;

import java.nio.file.Path;
import java.util.*;

import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.Parameters;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;

public class Project {

  public List<String> open_apps;

  private GeneratorParameters params;

  private Parameters gp;

  public Path path;

  private boolean modified= false;

  public Project()
  {
    this.open_apps= new ArrayList<String>();
    this.gp= Parameters.get().copy();

    this.params= new GeneratorParameters(this, this.gp);
  }

  public Project(Parameters params)
  {
    this.open_apps= new ArrayList<String>();
    this.gp= params;
    this.params= new GeneratorParameters(this, this.gp);
  }

  public boolean isModified()
  {
    return modified;
  }

  public ObservableBooleanValue propertyAppsAlways()
  {
    return params.apps_always;
  }

  public BooleanProperty propertyAppsOnly()
  {
    return params.apps_only;
  }

  public ObservableList<String> propertyApps()
  {
    return params.inputdir_apps_string;
  }

  public ObservableList<String> propertyServers()
  {
    return params.inputdir_servers_string;
  }

  public BooleanProperty propertyServersOnly()
  {
    return params.servers_only;
  }

  public BooleanProperty propertyClean()
  {
    return params.clean;
  }

  public BooleanProperty propertyCleanApps()
  {
    return params.clean_tmp_apps;
  }

  public BooleanProperty propertyCleanServers()
  {
    return params.clean_tmp_servers;
  }

  private class GeneratorParameters {
    private final BooleanProperty servers_only;
 
    private final BooleanProperty apps_only;
 
    private final ObservableList<String> inputdir_apps_string;
 
    private final ObservableList<String> inputdir_servers_string;

    private final BooleanProperty ignore_app_servers;

    private final BooleanProperty apps_always;

    private final StringProperty server_out_dir;

    private final StringProperty apps_out_dir;
 
    private final StringProperty repository;
 
    private final StringProperty branch;
 
    private final BooleanProperty forceRepository;
 
    private final StringProperty lastbuilt;

    private final BooleanProperty clean;

    private final BooleanProperty clean_tmp_apps;

    private final BooleanProperty clean_tmp_servers;

    private final BooleanProperty omitweb;

    public GeneratorParameters(Project project, Parameters params)
    {
      this.apps_always= new SimpleBooleanProperty(params.apps_always);
      apps_always.addListener((v, oldVal, newVal) -> {
        params.apps_always= newVal;
        project.modified= true;
      });

      this.apps_only= new SimpleBooleanProperty(params.apps_only);
      apps_only.addListener((v, oldVal, newVal) -> {
        params.apps_only= newVal;
        project.modified= true;
      });

      this.apps_out_dir= new SimpleStringProperty(params.apps_out_dir);
      apps_out_dir.addListener((v,oldVal, newVal) -> {
        params.apps_out_dir= newVal;
        project.modified= true;
      });

      this.branch= new SimpleStringProperty(params.branch);
      branch.addListener((v,oldVal, newVal) -> {
        params.branch= newVal;
        project.modified= true;
      });

      this.clean= new SimpleBooleanProperty(params.clean);
      clean.addListener((v, oldVal, newVal) -> {
        params.clean= newVal;
        project.modified= true;
      });

      this.clean_tmp_apps= new SimpleBooleanProperty(params.clean_tmp_apps);
      clean.addListener((v, oldVal, newVal) -> {
        params.clean_tmp_apps= newVal;
        project.modified= true;
      });

      this.clean_tmp_servers= new SimpleBooleanProperty(params.clean_tmp_servers);
      clean.addListener((v, oldVal, newVal) -> {
        params.clean_tmp_servers= newVal;
        project.modified= true;
      });

      this.forceRepository= new SimpleBooleanProperty(params.forceRepository);
      forceRepository.addListener((v, oldVal, newVal) -> {
        params.forceRepository= newVal;
        project.modified= true;
      });

      this.ignore_app_servers= new SimpleBooleanProperty(params.ignore_app_servers);
      ignore_app_servers.addListener((v, oldVal, newVal) -> {
        params.ignore_app_servers= newVal;
        project.modified= true;
      });

      inputdir_apps_string= FXCollections.observableArrayList(params.inputdir_apps_string);
      inputdir_apps_string.addListener((ListChangeListener<String>) change -> {
        while( change.next() )
        {
          if( change.wasAdded() )
          {
            params.inputdir_apps_string.addAll(change.getAddedSubList());
          }
          if( change.wasRemoved() )
          {
            for(String remove : change.getRemoved() )
              params.inputdir_apps_string.remove(remove);
          }
        }
        project.modified= true;
      });

      inputdir_servers_string= FXCollections.observableArrayList(params.inputdir_servers_string);
      inputdir_servers_string.addListener((ListChangeListener<String>) change -> {
        while( change.next() )
        {
          if( change.wasAdded() )
          {
            params.inputdir_servers_string.addAll(change.getAddedSubList());
          }
          if( change.wasRemoved() )
          {
            for(String remove : change.getRemoved() )
              params.inputdir_servers_string.remove(remove);
          }
        }
        project.modified= true;
      });

      this.lastbuilt= new SimpleStringProperty(params.lastbuilt);
      lastbuilt.addListener((v,oldVal, newVal) -> {
        params.lastbuilt= newVal;
        project.modified= true;
      });

      this.omitweb= new SimpleBooleanProperty(params.omitweb);
      omitweb.addListener((v, oldVal, newVal) -> {
        params.omitweb= newVal;
        project.modified= true;
      });

      this.repository= new SimpleStringProperty(params.repository);
      repository.addListener((v,oldVal, newVal) -> {
        params.repository= newVal;
        project.modified= true;
      });

      this.server_out_dir= new SimpleStringProperty(params.server_out_dir);
      server_out_dir.addListener((v,oldVal, newVal) -> {
        params.server_out_dir= newVal;
        project.modified= true;
      });

      servers_only= new SimpleBooleanProperty(params.servers_only);
      servers_only.addListener((v, oldVal, newVal) -> {
        params.servers_only= newVal;
        project.modified= true;
      });
    }
  }

  public Parameters getParams()
  {
    return gp;
  }

  public Project copy()
  {
    Project ret= new Project();
    ret.gp= gp.copy();
    ret.params= new GeneratorParameters(ret,ret.gp);
    ret.open_apps= new ArrayList<String>(open_apps.size());
    ret.open_apps.addAll(open_apps);
    ret.modified= modified;
    return ret;
  }

  public void setModified()
  {
    modified= true;
  }

  public void closeApp(Application app)
  {
    open_apps.remove(app.directory.toString());
    // path.getParent().relativize(f.toPath()).toString()
  }
}
