package at.pegasos.generator.configurator.dialog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public abstract class MenuItemEditDialogController implements Initializable {

  private ButtonType okBtnType;

  @FXML
  DialogPane root;

  public MenuItemEditDialogController()
  {
  }

  public void setData(MenuItemEditDialog editDlg)
  {
    editDlg.setResultConverter((btnType) -> {
      if( btnType == okBtnType )
      {
        unfillValues();
        return this.getActivity();
      }
      else
        return null;
    });

    editDlg.setResizable(true);

    fillValues();
  }

  protected abstract at.pegasos.generator.parser.appparser.token.menu.MenuItem getActivity();

  protected abstract void fillValues();
  
  protected abstract void unfillValues();
  
  protected abstract void resetItem();

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    okBtnType= new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType resetBtnType= new ButtonType("Reset", ButtonBar.ButtonData.BACK_PREVIOUS);
    ButtonType closeBtnType= new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    root.getButtonTypes().addAll(okBtnType, resetBtnType, closeBtnType);

    Button resetBtn= (Button) root.lookupButton(resetBtnType);

    resetBtn.addEventFilter(ActionEvent.ACTION, (evt) -> {
      evt.consume();
      resetItem();
      fillValues();
    });
  }
}
