package at.pegasos.generator.configurator.dialog;

import java.net.URL;
import java.util.*;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.EntryString;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.ComboBoxTableCell;

public class SportDialogController implements Initializable {
  
  private final static int SENSLIST_HEIGHT= 140;
  private final static int SENSLIST_WIDTH= 120;
  
  private Sport sport;
  private Sport sportOrig;

  private ObservableList<String> requiredNames;

  private ObservableList<String> optionalNames;

  private ObservableList<Screen> screens;

  @FXML
  Label lblName;
  @FXML
  Label lblFileName;
  @FXML
  TextField icon;

  @FXML
  Button addReq;
  @FXML
  TableView<String> tblReq;
  @FXML
  TableColumn<String, String> reqNameCol;
  @FXML
  TableColumn<String, Object> removeReqCol;
  
  @FXML
  Button addOpt;
  @FXML
  TableView<String> tblOpt;
  @FXML
  TableColumn<String, String> optNameCol;
  @FXML
  TableColumn<String, Object> removeOptCol;
  
  @FXML
  Button addScreen;
  @FXML
  TableView<Screen> tblScreens;
  @FXML
  TableColumn<Screen, String> screenNameCol;
  @FXML
  TableColumn<Screen, Object> screenRemoveCol;
  private ObservableList<String> sensorTypes;

  @FXML
  DialogPane root;

  private ButtonType okBtnType;

  public SportDialogController()
  {
    this.requiredNames= FXCollections.observableArrayList(Collections.emptyList());
    this.optionalNames= FXCollections.observableArrayList(Collections.emptyList());
    this.screens= FXCollections.observableArrayList(Collections.emptyList());
    this.sensorTypes= FXCollections.observableArrayList(Collections.emptyList());
  }
  
  public void setData(SportDialog sportDialog, Sensors sensors, Sport sport)
  {
    this.sportOrig= sport.copy();
    this.sport= sportOrig.copy();

    this.sensorTypes.addAll(sensors.getNames());

    sportDialog.setResultConverter((btnType) -> {
      if( btnType == okBtnType )
      {
        unfillValues();
        return this.sport;
      }
      else
        return null;
    });

    sportDialog.setResizable(true);

    fillValues();
  }

  private void fillValues()
  {
    lblName.setText("Name: " + sport.name);
    lblFileName.setText("File: " + sport.fileName);
    if( sport.icon != null )
      icon.setText(sport.icon.raw());

    this.requiredNames.clear();
    this.requiredNames.addAll(this.sport.required_names);
    this.optionalNames.clear();
    this.optionalNames.addAll(this.sport.optional_names);
    this.screens.clear();
    this.screens.addAll(this.sport.screens);
  }
  
  private void unfillValues()
  {
    this.sport.required_names.clear();
    this.sport.required_names.addAll(requiredNames);
    
    this.sport.optional_names.clear();
    this.sport.optional_names.addAll(optionalNames);
    
    this.sport.screens.clear();
    this.sport.screens.addAll(screens);
    
    if( !icon.getText().equals("") )
    {
      if( icon.getText().startsWith("R.") )
      {
        sport.icon= new EntryString(EntryString.TYPE_ID, icon.getText());
      }
      else
      {
        sport.icon= new EntryString(EntryString.TYPE_STRING, icon.getText());
      }
    }
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    reqNameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue()));
    reqNameCol.setCellFactory(ComboBoxTableCell.forTableColumn(sensorTypes));
    reqNameCol.setOnEditCommit((CellEditEvent<String, String> event) -> {
      TablePosition<String, String> pos= event.getTablePosition();

      int row= pos.getRow();
      requiredNames.set(row, event.getNewValue());
    });
    reqNameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    removeReqCol.setCellFactory(column -> {
      ButtonTableCell<String, Object> cell= new RemoveButtonTableCell<String, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        this.requiredNames.remove(index);
      });

      return cell;
    });

    tblReq.setItems(requiredNames);
    tblReq.setEditable(true);

    tblReq.setMaxHeight(SENSLIST_HEIGHT);
    tblReq.setMinWidth(SENSLIST_WIDTH);

    String addString= "add";

    Tooltip tooltip= new Tooltip(addString);
    addReq.setTooltip(tooltip);

    addReq.setOnAction(event -> {
      String e= new String("");
      requiredNames.add(e);
      tblReq.scrollTo(e);
    });
    
    optNameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue()));
    optNameCol.setCellFactory(ComboBoxTableCell.forTableColumn(sensorTypes));
    optNameCol.setOnEditCommit((CellEditEvent<String, String> event) -> {
      TablePosition<String, String> pos= event.getTablePosition();

      int row= pos.getRow();
      optionalNames.set(row, event.getNewValue());
    });
    optNameCol.setStyle( "-fx-alignment: CENTER-LEFT;");

    removeOptCol.setCellFactory(column -> {
      ButtonTableCell<String, Object> cell= new RemoveButtonTableCell<String, Object>();

      // Try to remove the selected sensor
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        this.optionalNames.remove(index);
      });

      return cell;
    });

    tblOpt.setItems(optionalNames);
    tblOpt.setEditable(true);

    tblOpt.setMaxHeight(SENSLIST_HEIGHT);
    tblOpt.setMinWidth(SENSLIST_WIDTH);

    addOpt.setTooltip(tooltip);

    addOpt.setOnAction(event -> {
      String e= new String("");
      optionalNames.add(e);
      tblOpt.scrollTo(e);
    });

    screenNameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().name));
    screenNameCol.setCellFactory(ComboBoxTableCell.forTableColumn(sensorTypes));

    screenRemoveCol.setCellFactory(column -> {
      ButtonTableCell<Screen, Object> cell= new RemoveButtonTableCell<Screen, Object>();

      // Try to remove the selected sensor
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        this.screens.remove(index);
      });

      return cell;
    });

    tblScreens.setItems(screens);
    tblScreens.setEditable(true);

    tblScreens.setMaxHeight(SENSLIST_HEIGHT);
    tblScreens.setMinWidth(SENSLIST_WIDTH);

    addScreen.setTooltip(tooltip);

    addScreen.setOnAction(event -> {
      Screen e= new Screen();
      screens.add(e);
      tblScreens.scrollTo(e);
    });

    okBtnType= new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType resetBtnType= new ButtonType("Reset", ButtonBar.ButtonData.BACK_PREVIOUS);
    ButtonType closeBtnType= new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
    
    root.getButtonTypes().addAll(okBtnType, resetBtnType, closeBtnType);

    Button resetBtn= (Button) root.lookupButton(resetBtnType);

    resetBtn.addEventFilter(ActionEvent.ACTION, (evt) -> {
      evt.consume();
      sport= sportOrig.copy();
      fillValues();
    });
  }
}
