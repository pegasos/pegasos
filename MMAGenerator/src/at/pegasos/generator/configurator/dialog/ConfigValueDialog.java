package at.pegasos.generator.configurator.dialog;

import at.pegasos.util.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class ConfigValueDialog extends Dialog<Pair<String,String>> {
  private final static String header = "Config Value";
  // private final static String header2 = "Please select a type for the chart and click OK to proceed";

  private String currentVariable;
  private String currentValue;

  /**
   *
   * @param primaryStage primaryStage
   * @param entry entry to edit a: variable, b: value
   * @param values possible variables
   */
  public ConfigValueDialog(Stage primaryStage, Pair<String, String> entry, ObservableList<String> values)
  {
    super();
    initOwner(primaryStage);

    BorderPane root = new BorderPane();
    // root.setTop(new Label(header2));

    GridPane gridPane = new GridPane();

    ComboBox<String> dropDown = new ComboBox<String>(values);
    dropDown.setPrefWidth(200.0d);
    if( entry != null )
    {
      dropDown.valueProperty().setValue(entry.getA());
      currentVariable = entry.getA();
    }
    dropDown.valueProperty().addListener((observable, oldValue, newValue) -> currentVariable = newValue);

    gridPane.add(new Label("Variable: "), 0, 0);
    gridPane.add(dropDown, 1, 0);

    TextField valueField = new TextField();
    if( entry != null )
    {
      valueField.setText(entry.getB());
      currentValue = entry.getB();
    }
    valueField.textProperty().addListener((whatEver, oldValue, newValue) -> currentValue = newValue);
    gridPane.add(new Label("Value: "), 0, 1);
    gridPane.add(valueField, 1, 1);

    root.setCenter(gridPane);

    getDialogPane().setContent(root);
    getDialogPane().setHeaderText(header);

    ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType closeBtnType = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

    getDialogPane().getButtonTypes().addAll(okBtnType, closeBtnType);

    setResultConverter((btnType) -> {
      if( btnType == okBtnType && currentVariable != null && currentValue != null )
        return new Pair<>(currentVariable, currentValue);
      else
        return null;
    });

    this.setResizable(false);
  }
}
