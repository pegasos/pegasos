package at.pegasos.generator.configurator.dialog;

import java.io.IOException;

import at.pegasos.generator.parser.appparser.token.Sensors;
import at.pegasos.generator.parser.appparser.token.Sport;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;

public class SportDialog extends Dialog<Sport> {

  private DialogPane root;

  public SportDialog(Stage primaryStage, Sensors sensors, Sport sport)
  {
    super();
    initOwner(primaryStage);

    try
    {
      FXMLLoader loader= new FXMLLoader();
      loader.setLocation(this.getClass().getResource("SportDialog.fxml"));
      // loader.setRoot(this);
      SportDialogController controller= new SportDialogController();
      loader.setController(controller);
      root= (DialogPane) loader.load();

      setDialogPane(root);

      // Send the data to the controller
      controller.setData(this, sensors, sport);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Showing SportDialog failed");
    }
  }
}
