package at.pegasos.generator.configurator.dialog;

import at.pegasos.generator.configurator.*;
import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.appparser.token.Sensor.*;
import javafx.beans.property.*;
import javafx.collections.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import java.util.*;
import java.util.stream.*;

public class ImplementationDialog extends Dialog<List<Clasz>> {
  private final static String header = "Implementations";

  private final List<Clasz> implementationsOrig;
  private final ObservableList<Clasz> implementations;
  private final ApplicationEditorController mainController;

  public ImplementationDialog(Stage primaryStage, List<Clasz> classes, ApplicationEditorController mainController)
  {
    super();
    initOwner(primaryStage);

    if( classes != null )
    {
      this.implementationsOrig = classes;
      this.implementations = FXCollections.observableArrayList(classes.stream().map(Clasz::copy).collect(Collectors.toList()));
    }
    else
    {
      this.implementationsOrig = Collections.emptyList();
      this.implementations = FXCollections.observableArrayList(Collections.emptyList());
    }

    this.mainController = mainController;

    setupUi();
  }

  private void setupUi()
  {
    BorderPane root = new BorderPane();
    // root.setTop(new Label(header));

    TableView<Clasz> tbl;
    TableColumn<Clasz, String> classCol = new TableColumn<Clasz, String>("Class");
    TableColumn<Clasz, Clasz.Type> typeCol = new TableColumn<Clasz, Clasz.Type>("Type");
    TableColumn<Clasz, Object> removeCol = new TableColumn<Clasz, Object>();

    classCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().clasz));
    classCol.setCellFactory(TextFieldTableCell.forTableColumn());
    classCol.setOnEditCommit((TableColumn.CellEditEvent<Clasz, String> event) -> {
      TablePosition<Clasz, String> pos = event.getTablePosition();

      int row = pos.getRow();
      Clasz entry = event.getTableView().getItems().get(row);
      entry.clasz = event.getNewValue();

      this.mainController.setModified();
    });
    classCol.setPrefWidth(250);
    classCol.setStyle("-fx-alignment: CENTER-LEFT;");

    typeCol.setCellValueFactory(param -> new SimpleObjectProperty<Clasz.Type>(param.getValue().type));
    ObservableList<Clasz.Type> type = FXCollections.observableArrayList(Arrays.asList(Clasz.Type.Ant, Clasz.Type.Ble, Clasz.Type.Other));
    typeCol.setCellFactory(ComboBoxTableCell.forTableColumn(type));

    typeCol.setOnEditCommit((TableColumn.CellEditEvent<Clasz, Clasz.Type> event) -> {
      TablePosition<Clasz, Clasz.Type> pos = event.getTablePosition();

      int row = pos.getRow();
      Clasz entry = event.getTableView().getItems().get(row);
      entry.type = event.getNewValue();

      this.mainController.setModified();
    });

    removeCol.setCellFactory(column -> {
      ButtonTableCell<Clasz, Object> cell = new RemoveButtonTableCell<Clasz, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        this.implementations.remove(index);

        this.mainController.setModified();
      });

      return cell;
    });

    tbl = new TableView<Clasz>(implementations);
    tbl.getColumns().addAll(Arrays.asList(classCol, typeCol, removeCol));
    tbl.setEditable(true);

    // tbl.setMinHeight(2);
    tbl.setMaxHeight(150);
    tbl.setMinWidth(380);

    // private final String removeString = Globals.getInstance().getRes().getString("remove");
    String addString = "add";

    Button add = new Button();
    add.getStyleClass().add("icon-add");
    add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    Tooltip tooltip = new Tooltip(addString);
    add.setTooltip(tooltip);

    add.setOnAction(event -> {
      Clasz e = new Clasz();
      implementations.add(e);
      tbl.scrollTo(e);
    });

    VBox vbox = new VBox();
    vbox.getChildren().addAll(add, tbl);
    vbox.setAlignment(Pos.CENTER);
    root.setCenter(vbox);

    getDialogPane().setContent(root);
    getDialogPane().setHeaderText(header);

    ButtonType okBtnType = new ButtonType("OK", ButtonBar.ButtonData.APPLY);
    ButtonType resetBtnType = new ButtonType("Reset", ButtonBar.ButtonData.BACK_PREVIOUS);
    ButtonType closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    getDialogPane().getButtonTypes().addAll(okBtnType, resetBtnType, closeBtnType);

    Button resetBtn = (Button) getDialogPane().lookupButton(resetBtnType);

    resetBtn.addEventFilter(ActionEvent.ACTION, (evt) -> {
      evt.consume();
      this.implementations.clear();
      this.implementations.addAll(this.implementationsOrig.stream().map(Clasz::copy).collect(Collectors.toList()));
    });

    setResultConverter((btnType) -> {
      if( btnType == okBtnType )
        return this.implementations;
      else
        return null;
    });

    this.setResizable(true);
  }
}
