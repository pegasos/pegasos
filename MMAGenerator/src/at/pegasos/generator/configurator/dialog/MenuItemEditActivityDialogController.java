package at.pegasos.generator.configurator.dialog;

import java.net.URL;
import java.util.*;

import at.pegasos.generator.configurator.button.*;
import at.pegasos.generator.parser.appparser.token.menu.MenuItem;
import at.pegasos.generator.parser.appparser.token.menu.MenuItemActivity;
import at.pegasos.generator.parser.token.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;

public class MenuItemEditActivityDialogController extends MenuItemEditDialogController {

  private MenuItemActivity item;
  private MenuItemActivity itemOrig;

  @FXML
  TextField activity;
  @FXML
  TextField sport;
  @FXML
  CheckBox fixed;
  @FXML
  TextField distance;
  @FXML
  CheckBox callback_class;
  @FXML
  TextArea callback;
  @FXML
  CheckBox autostart;

  @FXML
  Button addQuestion;
  @FXML
  TableView<HashMap<String, EntryString>> tblQuestions;
  @FXML
  TableColumn<HashMap<String, EntryString>, String> questNameCol;
  @FXML
  TableColumn<HashMap<String, EntryString>, Object> questRemoveCol;

  ObservableList<HashMap<String, EntryString>> entries;

  @FXML
  DialogPane root;

  public MenuItemEditActivityDialogController()
  {
    this.entries= FXCollections.observableArrayList();
  }

  public void setData(MenuItemEditActivityDialog menuItemEditActivityDialog, MenuItemActivity activity)
  {
    this.itemOrig= activity.copy();
    this.item= itemOrig.copy();
    
    super.setData(menuItemEditActivityDialog);
  }

  protected void fillValues()
  {
    activity.setText(item.activity.toString());
    if( item.sport != null )
      sport.setText(item.sport);
    fixed.setSelected(item.fixed_distance);
    if( item.distance != null )
      distance.setText(item.distance.toString());
    callback_class.setSelected(item.callback_class);
    if( item.callback != null )
      callback.setText(item.callback.toString());
    autostart.setSelected(item.auto_start);

    this.entries.clear();
    this.entries.addAll(item.qvalues);
  }

  protected void unfillValues()
  {
    this.item.activity= new EntryInt(activity.getText());
    this.item.sport= sport.getText().equals("") ? null : sport.getText();
    this.item.fixed_distance= fixed.isSelected();
    this.item.distance= distance.getText().equals("") ? null : new EntryInt(distance.getText());
    this.item.callback_class= callback_class.isSelected();
    this.item.callback= callback.getText();
    this.item.auto_start= autostart.isSelected();
    
    this.item.qvalues.clear();
    this.item.qvalues.addAll(entries);
  }

  @Override
  public void initialize(URL location, ResourceBundle res)
  {
    super.initialize(location, res);
    
    questNameCol.setCellValueFactory(param -> new SimpleObjectProperty<String>(param.getValue().toString()));
    questNameCol.setCellFactory(TextFieldTableCell.forTableColumn());

    questRemoveCol.setCellFactory(column -> {
      ButtonTableCell<HashMap<String, EntryString>, Object> cell= new RemoveButtonTableCell<HashMap<String, EntryString>, Object>();

      // Try to remove the selected analysis
      cell.getButton().setOnAction(event -> {
        int index = cell.getIndex();
        this.entries.remove(index);
      });

      return cell;
    });

    tblQuestions.setItems(entries);
    tblQuestions.setEditable(true);

    String addString= "add";

    Tooltip tooltip= new Tooltip(addString);
    addQuestion.setTooltip(tooltip);

    addQuestion.setOnAction(event -> {
      HashMap<String, EntryString> e= new HashMap<String, EntryString>();
      entries.add(e);
      tblQuestions.scrollTo(e);
    });
  }

  @Override
  protected MenuItem getActivity()
  {
    return item;
  }

  @Override
  protected void resetItem()
  {
    item= itemOrig.copy();
  }
}
