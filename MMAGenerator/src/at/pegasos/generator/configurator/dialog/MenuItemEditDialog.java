package at.pegasos.generator.configurator.dialog;

import at.pegasos.generator.parser.appparser.token.menu.MenuItem;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;

public class MenuItemEditDialog extends Dialog<MenuItem> {

  public MenuItemEditDialog(Stage primaryStage)
  {
    super();
    initOwner(primaryStage);
  }
}
