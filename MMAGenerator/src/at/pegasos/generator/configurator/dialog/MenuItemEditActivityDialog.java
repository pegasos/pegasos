package at.pegasos.generator.configurator.dialog;

import java.io.IOException;

import at.pegasos.generator.parser.appparser.token.menu.MenuItemActivity;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;

public class MenuItemEditActivityDialog extends MenuItemEditDialog {

  private DialogPane root;

  public MenuItemEditActivityDialog(Stage primaryStage, MenuItemActivity activity)
  {
    super(primaryStage);

    try
    {
      FXMLLoader loader= new FXMLLoader();
      loader.setLocation(this.getClass().getResource("MenuItemEditActivityDialog.fxml"));
      MenuItemEditActivityDialogController controller= new MenuItemEditActivityDialogController();
      loader.setController(controller);
      root= (DialogPane) loader.load();

      setDialogPane(root);

      // Send the data to the controller
      controller.setData(this, activity);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      new ExceptionDialog(e, "Showing MenuItemEditActivityDialog failed");
    }
  }
}
