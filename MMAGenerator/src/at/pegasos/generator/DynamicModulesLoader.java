package at.pegasos.generator;

import org.reflections.*;
import org.reflections.scanners.*;
import org.reflections.util.*;

import java.lang.annotation.*;
import java.util.*;

import at.pegasos.generator.parser.*;
import lombok.extern.slf4j.*;

@Slf4j
public class DynamicModulesLoader {

  /**
   *
   * @param loadClass Type of parser each parser needs to subclass
   * @param generatorType Annotation each parser needs to have
   * @param packageName package to be searched
   * @return
   */
  @SuppressWarnings("unchecked") // this is safe as we type check before adding to the list
  public static List<Class<? extends PegasosParser>> fetchParsers(Class<? extends PegasosParser> loadClass,
                                                                  Class<? extends Annotation> generatorType, String packageName)
  {
    List<Class<? extends PegasosParser>> ret= new ArrayList<Class<? extends PegasosParser>>();

    FilterBuilder f= new FilterBuilder();
    f= f.include(FilterBuilder.prefix(packageName));

    Reflections reflections= new Reflections(
            new ConfigurationBuilder()
                    .setUrls(ClasspathHelper.forJavaClassPath())
                    .setScanners(new SubTypesScanner(false), new TypeAnnotationsScanner())
                    .filterInputsBy(f)
    );

    Set<Class<?>> allClasses = new HashSet<Class<?>>(reflections.getTypesAnnotatedWith(generatorType));

    for(Class<?> clasz : allClasses)
    {
      if( !loadClass.isAssignableFrom(clasz) )
      {
        log.info("Ignoring {} as it needs to implement {}", clasz, loadClass.getCanonicalName());
        continue;
      }

      ret.add((Class<? extends PegasosParser>) clasz);
    }

    return ret;
  }
}
