package at.pegasos.generator.webgenerator.backend;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Config;

public class ConfigGeneratorProps extends CodeGenerator {
  private final Config config;
  private final String name;

  public ConfigGeneratorProps(String name, Config config)
  {
    this.config= config;
    this.name= name;
  }

  @Override
  public void generate() throws GeneratorException
  {
    for(String name : config.getValueNames())
    {
      try
      {
        assign(name, config.getValue(name));
      }
      catch( ParserException e )
      {
        e.printStackTrace();
        throw new GeneratorException(e);
      }
    }
  }

  /**
   * Output an assignment
   * @param variable
   * @param value
   */
  protected void assign(String variable, String value)
  {
    output(variable + "= " + value);
  }

  protected void output(String line)
  {
    out.println(line);
  }

  @Override
  public void setOutputToPrintStream(Path basedir) throws IOException
  {
    Path p= basedir;
    
    if( !Files.exists(p) )
      Files.createDirectories(p);
    
    p= p.resolve(name + ".properties");
    
    this.out= new PrintStream(Files.newOutputStream(p));
  }
}
