package at.pegasos.generator.generator;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import at.pegasos.generator.generator.client.ClientGenerator;
import at.pegasos.generator.generator.server.web.frontend.WebFrontendGenerator;
import at.pegasos.generator.parser.webparser.token.Web;
import org.reflections.Reflections;
import org.reflections.scanners.*;
import org.reflections.util.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PegasosModuleGenerator {

  @SuppressWarnings("unchecked") // this is safe as we type check before adding to the list
  protected List<Class<? extends CodeGenerator>> fetchGenerators(
      // Class<? extends CodeGenerator> loadClass,
      Class<? extends Annotation> generatorType,
      String... packages) throws GeneratorException
  {
    List<Class<? extends CodeGenerator>> ret= new ArrayList<Class<? extends CodeGenerator>>();

    List<ClassLoader> classLoadersList=new LinkedList<ClassLoader>();
    // classLoadersList.add(dynalLoader);
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    FilterBuilder f = new FilterBuilder();
    for(String pkg : packages)
    {
      f= f.include(FilterBuilder.prefix(pkg));
    }

    Reflections reflections= new Reflections(
        new ConfigurationBuilder()
          .setUrls(ClasspathHelper.forJavaClassPath())
          .setScanners(new SubTypesScanner(false), new TypeAnnotationsScanner())
          .filterInputsBy(f)
    );

    Set<Class<? extends Object>> allClasses= new HashSet<Class<? extends Object>>(
        reflections.getTypesAnnotatedWith(generatorType));

    for(Class<? extends Object> clasz : allClasses)
    {
      if( !CodeGenerator.class.isAssignableFrom(clasz) )
      {
        log.error("{} needs to implement {}", clasz, CodeGenerator.class.getCanonicalName());
        continue;
      }

      ret.add((Class<? extends CodeGenerator>) clasz);
    }

    return ret;
  }
}
