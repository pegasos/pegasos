package at.pegasos.generator.generator.server;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.docker.token.*;
import at.pegasos.generator.parser.docker.token.services.*;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.*;
import org.yaml.snakeyaml.nodes.*;

@Slf4j
public class DockerCompose extends YamlGenerator {

  private final DockerComposeFile compose;

  private final ServerInstance instance;

  public DockerCompose(ServerInstance instance) throws GeneratorException
  {
    super();
    this.instance = instance;

    compose = new DockerComposeFile(instance);
  }

  @Override
  public void generate() throws GeneratorException
  {
    log.info("Check if db.url is set");
    if( !instance.config.hasValue("db.url") )
    {
      try
      {
        String dbUrl = "jdbc:mariadb://" + compose.getDatabaseServiceName() + ":3306/" + instance.config.getValue("db.name") +
                "?useJvmCharsetConverters=true&autoReconnect=true";
        log.info("Generating db.url = {}", dbUrl);
        instance.config.setValueString("db.url", dbUrl);
      }
      catch (ParserException e)
      {
        throw new GeneratorException(e);
      }
    }

    representer.addClassTag(DockerComposeFile.class, Tag.MAP);
    representer.addClassTag(MariaDbService.class, Tag.MAP);
    representer.addClassTag(BackendService.class, Tag.MAP);
    representer.addClassTag(FrontendBackendService.class, Tag.MAP);
    representer.addClassTag(FrontendWebService.class, Tag.MAP);

    write(compose);
  }
}
