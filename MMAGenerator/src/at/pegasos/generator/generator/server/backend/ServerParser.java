package at.pegasos.generator.generator.server.backend;

import java.io.IOException;
import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.servergenerator.SensorConfigParser;

public class ServerParser extends JavaCodeGenerator {
  private final boolean oldUnivie;
  private final Options options;
  private final Field[] fields;
  private final BlockSessionInsert session_insert;
  private final FieldSession[] fields_session_code;
  private final FieldDb[] fields_session_db;
  private final BlockSessionInsert extra_code;
  
  private final String db_table;
  private final String name;
  
  private int bufferIndex = 0;
  private int nByteBuffers;
  
  private boolean has_text_vars = false;

  private String line;
  private int increase;
  private int bc;
  private String set_null;
  private String set;

  public static void main(String[] args) throws IOException, ParserException, GeneratorException
  {
    Sensor s= new Sensor("13 Bike    bike    generate    sensors/bike.txt".split("\t"));
    SensorConfigParser parser= new SensorConfigParser(args[0], s);
    parser.parse();
    
    ServerParser gen= new ServerParser(s, args[1], args[2], true);
    gen.setOutput(System.out);
    gen.generate();
  }

  public ServerParser(Sensor sensor, String db_table, String name, boolean oldUnivie)
  {
    super(oldUnivie ? "at.univie.mma.server.parser" : "at.pegasos.server.parser", name, null, "ISensorDataParser");

    this.options= sensor.getOptions();
    this.fields= sensor.getFields();
    this.fields_session_db= sensor.getSessionFieldsDb();
    this.fields_session_code= sensor.getSessionFieldsCode();
    this.session_insert= sensor.getSessionInsertBlock();
    this.extra_code= sensor.getExtraCode();
    this.db_table= db_table;
		this.name= name;
		this.oldUnivie= oldUnivie;
	}
	
	private void preparedStatement()
	{
	  output("private void perpareStatement() throws SQLException");
	  output("{");
	  indent();
	  
	  StringBuilder sql = new StringBuilder("insert into " + db_table + "(");
	  StringBuilder values = new StringBuilder();
	  sql.append("session_id"); //always include session id;
	  values.append("?");
	  sql.append(",rec_time");  //always include the time of recording
	  values.append(",?");
    for(Field f : fields)
    {
      sql.append(",");
      if( f.options.hasDifferentDBname() )
      {
        sql.append(f.options.getDBname());
      }
      else
      {
        sql.append(f.name);
      }
      
      values.append(",?");
    }
    
    if( this.options.isUseSensorNr() )
      sql.append(", sensor_nr");
	  
    sql.append(") values (").append(values);
	  
    if( this.options.isUseSensorNr() )
      sql.append(",?");
	  
    sql.append(")");
	  
    output("this.db_insert= this.db.createStatement(\"" + sql + "\");");
	  indent_less();
	  output("}");
	}
	
	private void constructor()
	{
	  output("public " + name + "(Session data, Db_connector db) throws SQLException");
    output("{");
    indent();
    output("this.data= data;");
    output("this.db= db;");
    output("perpareStatement();");
    indent_less();
    output("}");
	}
	
	public void generate() throws GeneratorException
	{
    analyseVariables();
	  
	  addImport("java.io.IOException");
    if( nByteBuffers > 0 )
      addImport("java.nio.ByteBuffer");
	  addImport("java.sql.PreparedStatement");
	  addImport("java.sql.SQLException");
    addImport("database.Db_connector");
    addImport(oldUnivie ? "at.univie.mma.server.data.Session" : "at.pegasos.server.data.Session");
    if( has_text_vars )
      addImport("java.nio.charset.Charset");
	  
	  header();
	  
    // output("public class " + name + " implements ISensorDataParser");
    // output("{");
    empty();
    indent();
    output("private int time_last_measurement;");
    output("private long time_since_start_ret;");
    output("private Session data;");
    output("private Db_connector db;");
    output("private int bc_ret;");
    output("private PreparedStatement db_insert;");
    output("private int inserts;");
    output("");
    
    if( has_text_vars )
    {
      assign("private final static Charset cs", "java.nio.charset.Charset.forName(\"UTF-8\")");
    }
    
    constructor();
    empty();
    
    // Check if we have to add some ByteBuffers
    if( nByteBuffers > 0 )
    {
      //ByteBuffer buffer1= ByteBuffer.allocate(8);
      for(int i= 0; i < nByteBuffers; i++)
      {
        assign("ByteBuffer buffer"+i, "ByteBuffer.allocate(8)");
      }
    }
    
    this.preparedStatement();
    empty();
    
    output("@Override");
    output("public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,");
    output("  int datapackages, long time_since_start) throws IOException, SQLException");
    output("{");
    // System.out.println(" String sql = \"insert into foot_pod
    // (session_id,rec_time,speed_mm_s,distance_m,strides_255,calories)
    // values\";");
    
    indent();
    for (Field f : fields)
    {
      line = "";
      
      line+= f.db_type.getJavaDef() + " ";
      
      line+= f.name;
      
      // Variable initialisation TODO: add option for other values
      if( f.db_type.isInteger() || f.db_type.isLong() || f.db_type.isShort() || f.db_type.isDouble() || f.db_type.isByte() )
        line+= "= 0";
      else if( f.db_type.isText() )
        line+= "= \"\"";
      
      line+= "; // field: " + f.number;
      output(line);
      
      if( f.db_type.isText() )
      {
        assign("int l" + f.name, "0");
        assign("byte[] b" + f.name, "null");
      }
      
    }
    
    if( options.isOptionalLength() )
    	output("byte flags= 0;");
    
    empty();
    output("inserts= 0;");
    empty();
    
    output("if( datapackages > 0 )");
    output(INDENT + "time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;");
    empty();
    
    output("for (int x=0;x<datapackages;x++)");
    output("{");
    indent();
    output("time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!");
    output("time_since_start+=time_last_measurement;");
    empty();
    output("db_insert.setLong(1, data.session_id);");
    output("db_insert.setLong(2, data.starttime+time_since_start);");
    
    bc = 3;
    increase = 2;
    
    if( options.isOptionalLength() )
    {
    	output("flags= (byte) ((byte) in[bc+3]&0xFF);");
    	bc++;
    	increase++;
    }

    List<String> insert_only= new ArrayList<String>();
    List<String> stmt_null_setters= new ArrayList<String>();
    int no= 3;
    
    int flag_pow= 0;
    
    for(Field f : fields)
    {
      set = "db_insert.set";
      set_null = "db_insert.setNull(" + no + ", ";
      setStatment(no, f);
      
      line = "";
      // create lines for parsing the field
      parse(f);
      
      if( f.isOptional() )
      {
      	/*
				 * Example: 
				 if( (flags & 1) != 0 )
				 {
				 	speed_mm_s= ((int)in[bc+4]&0xFF)|((int)in[bc+5]&0xFF)<<8;
				 	db_insert.setInt(3,speed_mm_s);
				 }
				 else
				 	db_insert.setNull(3,java.sql.Types.INTEGER);
				 */
      	String check= "if( (flags & " + ((int) Math.pow(2, flag_pow++)) + ") != 0 )";
      	if( f.options != null && f.options.isInsertOnly() )
      	{
      		insert_only.add(check);
      		insert_only.add("{");
      		insert_only.add(INDENT + line);
	        insert_only.add(INDENT + set);
	        insert_only.add("}");
      		stmt_null_setters.add(set_null);
      	}
      	else
      	{
      		output(check);
      		output("{");
      		indent();
      		output(line);
      		output(set);
      		indent_less();
      		output("}");
      		output("else");
      		indent();
      		output(set_null);
      		indent_less();
      	}
      }
      else
      {
	      if( f.options != null && f.options.isInsertOnly() )
	      {
	        insert_only.add(line);
	        insert_only.add(set);
	        stmt_null_setters.add(set_null);
	      }
	      else
	      {
	        output(line);
	        output(set);
	      }
      }      
      
      no++;
    }
    
    if( options.isUseSensorNr() )
      output("db_insert.setInt(" + no + ",sensornr);");
    
    if( insert_only.size() > 0 )
    {
      output("if (data.insert_data==1)");
      output("{");
      indent();
      for(String ins : insert_only )
        output(ins);
      indent_less();
      output("}");
      output("else");
      output("{");
      indent();
      for(String set : stmt_null_setters )
        output(set);
      indent_less();
      output("}");
    }
    
    if( stmt_null_setters.size() > 0 )
    {
      
    }
    
    output("if (data.insert_data==1)");
    output("{");
    indent();
    
    if( this.session_insert != null )
    {
      for(String ins : session_insert.lines )
        output(ins);
    }

    output("db_insert.addBatch();");
    output("inserts++;");
    indent_less();
    output("}");
    
    if( this.extra_code != null )
    {
      empty();
      for(String ins : extra_code.lines )
        output(ins);
      empty();
    }
    
    // System.out.println("      sql+=\"(\"+data.session_id+\",\"+(data.starttime+time_since_start)+\",\"+fp_speed+\",\"+fp_distance+\",\"+data.fp_strides_cul+\",\"+data.fp_calories+\")\";");
    // System.out.println("      if (x==(datapackages-1)) {sql+=\";\";} else {sql+=\",\";}");
    debugOutput();
    
    output("bc= bc + " + increase + ";");
    indent_less();
    output("}");
    
    empty();
    output("if( inserts>0 )");
    output("{");
    indent();
    output("db_insert.executeBatch();");
    indent_less();
    output("}");
    empty();
    
    setSessionValues();
    
    output("bc_ret= bc;");
    output("time_since_start_ret= time_since_start;");
    empty();
    
    output("return null;");
    indent_less();
    output("}");
    
    helperMethods();
    
    indent_less();
    output("}");
  }

  /**
   * Generate db_insert.set<Type>(<field>, <value>) and db_insert.setNull(<field>, <type>)
   * @param no index of the field
   * @param f field
   * @throws GeneratorException
   */
  private void setStatment(int no, Field f) throws GeneratorException
  {
    if( f.db_type.isDouble())
    {
      set+= "Double";
      set_null+= "java.sql.Types.DECIMAL";
    }
    else if( f.db_type.isInteger() )
    {
      set += "Int";
      set_null+= "java.sql.Types.INTEGER";
    }
    else if( f.db_type.isLong() )
    {
      set += "Long";
      set_null+= "java.sql.Types.BIGINT";
    }
    else if( f.db_type.isShort() )
    {
      set += "Short";
      set_null+= "java.sql.Types.SMALLINT";
    }
    else if( f.db_type.isByte() )
    {
      set += "Byte";
      set_null+= "java.sql.Types.TINYINT";
    }
    else if( f.db_type.isText() )
    {
      set+= "String";
      set_null+= "java.sql.Types.CHAR";
    }
    else
      throw new GeneratorException("Cannot generate Setter-Statement for type: " + f.db_type);

    set+= "(" + no + "," + f.name + ");";
    set_null+= ");";
  }

  /**
   * Generate the code for extracting the value from the transmitted data
   * @param f Field
   * @throws GeneratorException
   */
  private void parse(Field f) throws GeneratorException
  {
    if( f.db_type.isInteger() &&
        (f.raw_type.isInteger() || f.raw_type.isDouble() || f.raw_type.isLong() ) )
    {
      parseInteger(f);
    }
    else if( f.db_type.isShort() && (f.raw_type.isShort() ) )
    {
      parseShort(f);
    }
    else if( f.db_type.isDouble() && (f.raw_type.isDouble() ) )
    {
      parseDouble(f);
    }
    else if( f.db_type.isLong() && (f.raw_type.isLong() ) )
    {
      parseLong(f);
    }
    else if( f.db_type.isText() && f.raw_type.isText() )
    {
      parseText(f);
    }
    else
      throw new GeneratorException("Cannot generate Java Code for combination " + f.db_type + "/" + f.raw_type);
  }

  private void parseInteger(Field f)
  {
    line+= f.name + "= ((int)in[bc+" + (bc++) + "]&0xFF)|((int)in[bc+" + (bc++) + "]";
    if( f.isSigned() )
      line += "&0xFF";
    line += ")<<8;";
    increase+= 2;
  }

  private void parseShort(Field f)
  {
    line+= f.name + "= (short)(in[bc+" + (bc++) + "]&0xFF|in[bc+" + (bc++) + "]<<8);";
    increase+= 2;
  }

  private void parseDouble(Field f)
  {
    if( f.options.hasScale() )
    {
      line+= "{long tmp= 0;";
      line+= "for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+" + bc + "+i] & 0xff);}";
      line+= f.name + "= ((double)tmp / " + f.options.getScale() + ");";
      line+= "}";
    }
    else
    {
      line+= "{";
      line+= "for(int i= 0; i < 8; i++){";
      String buf= nextBuffer();
      line+=  buf+ ".put(i, in[bc+" + bc + "+i]);} ";
      line+= f.name + "= " + buf + ".getDouble(0);";
      line+= "}";
    }

    bc+= 8;
    increase+= 8;
  }

  private void parseLong(Field f)
  {
    // for (int i = 0; i < 8; i++) {data.gps_lat=(data.gps_lat<<8)+(in[bc+3+i] & 0xff) ;}
    line+= "{long tmp= 0;";
    line+= "for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+" + bc + "+i] & 0xff);}";
    bc+= 8;
    increase+= 8;
    line+= f.name + "= tmp;";
    line+= "}";
  }

  private void parseText(Field f)
  {
    line+= "{";
    line+= "l" + f.name + "= in[bc+" + (bc++) + "];";
    line+= "b" + f.name + "= new byte[l" + f.name + "];";
    line+= "System.arraycopy(in, bc+" + bc +", b" + f.name + ", 0, l" + f.name + ");";
    line+= f.name + "= new String(b" + f.name + ", cs);";
    line+= "}";
    bc+= f.options.getMaxLength();
    increase+= f.options.getMaxLength() + 1;
  }

  private void analyseVariables()
  {
    nByteBuffers= 0;
    for(Field f : fields)
    {
      if( f.db_type.isDouble() && (f.raw_type.isDouble() ) )
      {
        nByteBuffers++;
      }
      if( f.raw_type.isText() )
      {
        has_text_vars= true;
      }
    }
  }

  private void debugOutput()
  {
    StringBuilder line = new StringBuilder("System.out.println(\"" + name + ": ");
    for(Field f: fields)
    {
      line.append(f.name).append(":\"+").append(f.name).append("+ \" ");
    }
    line.append("\");");
    output(line.toString());
  }

  private void helperMethods()
  {
    empty();
    output("@Override");
    output("public int getNewPosition()");
    output("{");
    indent();
    output("return bc_ret;");
    indent_less();
    output("}");
    
    empty();
    output("@Override");
    output("public long getNewTimeSinceStart()");
    output("{");
    indent();
    output("return time_since_start_ret;");
    indent_less();
    output("}");
  }
  
  private void setSessionValues()
  {
    boolean written= false;
    
    for(FieldDb f : this.fields_session_db)
    {
      String line= "data.v[" + f.idx + "]= ";
      
      line+= this.fields[f.number].name;
      
      line+= ";";
      
      output(line);
      
      written= true;
    }
    
    for(FieldSession f : this.fields_session_code)
    {
      String line= "data.v[" + f.idx + "]= " + f.text + ";";
      output(line);
      
      written= true;
    }
    
    if( options.isSetSessionFieldsUsingValues() )
    {
      for(Field f : fields)
      {
        output("data." + f.name + "= " + f.name + ";");
        written= true;
      }
    }
    
    if( written )
      empty();
  }
  
  private String nextBuffer()
  {
    return "buffer" + bufferIndex++;
  }
}
