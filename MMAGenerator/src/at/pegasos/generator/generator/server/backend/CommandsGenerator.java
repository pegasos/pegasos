package at.pegasos.generator.generator.server.backend;

import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.token.ServerInstance;
import at.pegasos.generator.parser.backend.token.Command;

public class CommandsGenerator extends JavaCodeGenerator {
  
  private ServerInstance inst;
  private boolean types;

  private Set<String> required;

  public CommandsGenerator(String pkg, boolean types, ServerInstance instance, String... requiredCommands)
  {
    super(pkg,"Commands", null, null);
    
    this.inst= instance;
    this.types= types;

    this.required= new HashSet<String>();
    if( requiredCommands != null )
    {
      for(String req : requiredCommands)
      {
        required.add(req);
      }
    }
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    
    indent();
    
    if( types)
    {
      member("public static final", "int", "OK= 1");
      member("public static final", "int", "FAIL= 0");
    }
    
    output("public static class Codes {");
    indent();
    for(Command command : inst.commands.getCommands())
    {
      member("public static final", "int", command.name + "= " + command.code);
      required.remove(command.name);
    }
    for(String req : required)
    {
      member("public static final", "int", req + "= -1");
    }
    indent_less();
    output("};");
    
    indent_less();
    
    footer();
  }
  
}
