package at.pegasos.generator.generator.server.backend;

import java.util.ArrayList;
import java.util.List;

import at.pegasos.generator.generator.JavaCodeGenerator;
import at.pegasos.generator.parser.backend.token.Commands;

public class ControlCommandGenerator extends JavaCodeGenerator {
  private final boolean oldUnivie;
  private Commands commands;
  
  public ControlCommandGenerator(Commands commands, boolean oldUnivie)
  {
    super(oldUnivie ? "at.univie.mma.server.parser" : "at.pegasos.server.parser", "ControlCommandParserTable", null, null);
    this.commands= commands;
    this.oldUnivie= oldUnivie;
  }
  
  @Override
  public void generate()
  {
    addImport("java.lang.reflect.InvocationTargetException");
    addImport(oldUnivie ? "at.univie.mma.server.data.Session" : "at.pegasos.server.data.Session");
    addImport("database.Db_connector");
    
    header();
    
    // output("public class SensorDataParser implements Parser{");
    indent();
    
    tablefun();
    
    footer();
  }
  
  private void tablefun()
  {
    // method_begin(access_mod, returntype, methodname_params);
    method_begin("public static", "IControlCommandParser[]",
        "createTable(ControlCommand c, Session session, Db_connector db) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException");
    
    output("IControlCommandParser[] parsers= new IControlCommandParser[32];");
    empty();
    
    int count= commands.getMaxId() + 1;
    for(int i= 0; i < count; i++)
    {
      String c= commands.getServerClass(i);
      if( c == null )
        c= oldUnivie ? "at.univie.mma.server.parser.NoCommandAvailable" : "at.pegasos.server.parser.NoCommandAvailable";

      output("parsers[" + i + "]= (IControlCommandParser) Class.forName(\"" + c
          + "\").getConstructor(ControlCommand.class, Session.class, Db_connector.class).newInstance(c, session, db);");
    }
    
    empty();
    
    output("return parsers;");
    
    method_end();
  }
  
  public List<String> generateCreateTable()
  {
    int count= commands.getMaxId() + 1;
    // <> + table + catch
    List<String> ret= new ArrayList<String>(4 + count + 35);
    ret.add("parsers= new IControlCommandParser[32];");
    ret.add("try");
    ret.add("{");
    for(int i= 0; i < count; i++)
    {
      String c= commands.getServerClass(i);
      if( c == null )
        c= oldUnivie ? "at.univie.mma.server.parser.NoCommandAvailable" : "at.pegasos.server.parser.NoCommandAvailable";

      ret.add(JavaCodeGenerator.INDENT + "parsers[" + i + "]= (IControlCommandParser) Class.forName(\"" + c
          + "\").getConstructor(ControlCommand.class, Session.class, Db_connector.class).newInstance(this, data, db);");
    }
    ret.add("}");
    ret.add("catch (IllegalArgumentException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (SecurityException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (InstantiationException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (IllegalAccessException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (InvocationTargetException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (NoSuchMethodException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    ret.add("catch (ClassNotFoundException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerRuntimeException(\"Creating parsers for Session \" + data.session_id);");
    ret.add("}");
    
    List<String> t= new ArrayList<String>(ret.size());
    ret.forEach((String s) -> t.add(JavaCodeGenerator.INDENT + JavaCodeGenerator.INDENT + s));
    
    return t;
  }
  
  public List<String> generateCreateTestTable()
  {
    int count= commands.getMaxId() + 1;
    // <> + table + catch
    List<String> ret= new ArrayList<String>(4 + count + 35);
    ret.add("IControlCommandParser[] parsers= new IControlCommandParser[32];");
    ret.add("try");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "Db_connector db = new Db_connector();");
    ret.add(JavaCodeGenerator.INDENT + "Db_connector.open_db();");
    for(int i= 0; i < count; i++)
    {
      String c= commands.getServerClass(i);
      if( c != null )
        ret.add(JavaCodeGenerator.INDENT + "parsers[" + i + "]= (IControlCommandParser) Class.forName(\"" + c
            + "\").getConstructor(ControlCommand.class, Session.class, Db_connector.class).newInstance(null, null, db);");
    }
    ret.add("}");
    ret.add("catch (IllegalArgumentException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (SecurityException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (InstantiationException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (IllegalAccessException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (InvocationTargetException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (NoSuchMethodException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    ret.add("catch (ClassNotFoundException e)");
    ret.add("{");
    ret.add(JavaCodeGenerator.INDENT + "e.printStackTrace();");
    ret.add(JavaCodeGenerator.INDENT + "throw new ServerStartupException();");
    ret.add("}");
    
    List<String> t= new ArrayList<String>(ret.size());
    ret.forEach((String s) -> t.add(JavaCodeGenerator.INDENT + JavaCodeGenerator.INDENT + s));
    
    return t;
  }
}
