package at.pegasos.generator.generator.server.backend;

import java.util.List;

import at.pegasos.generator.generator.JavaCodeGenerator;
import at.pegasos.generator.parser.serverparser.token.Sensor;

public class SensorDataParserTableGenerator extends JavaCodeGenerator {
  private final boolean oldUnivie;
  private final List<Sensor> sensors;
  private String[] classnames;

  public final static String ParsersPackage = "at.pegasos.server.parser";
  private final static String ParsersPackageU = "at.univie.mma.server.parser";

  public SensorDataParserTableGenerator(List<Sensor> sensors, boolean oldUnivie)
  {
    super(oldUnivie ? ParsersPackageU : ParsersPackage, "SensorDataParserTable", null, null);

    this.sensors = sensors;
    this.oldUnivie = oldUnivie;

    analyseSensors();
  }

  private void analyseSensors()
  {
    int max = 0;
    for(Sensor sen : sensors)
    {
      if( max < sen.identifier )
        max = sen.identifier;
    }

    classnames = new String[max + 1];
    for(int i= 0; i < max+1; i++)
    {
      classnames[i] = oldUnivie ? "at.univie.mma.server.parser.NoParserAvailable" : "at.pegasos.server.parser.NoParserAvailable";
    }

    for(Sensor sen : sensors)
    {
      classnames[sen.identifier]= sen.className;
    }
  }

  @Override
  public void generate()
  {
    addImport("java.lang.reflect.InvocationTargetException");
    addImport(oldUnivie ? "at.univie.mma.server.data.Session" : "at.pegasos.server.data.Session");
    addImport("database.Db_connector");

    header();

    // output("public class SensorDataParser implements Parser{");
    indent();

    tablefun();

    footer();
  }

  private void tablefun()
  {
    // method_begin(access_mod, returntype, methodname_params);
    method_begin("public static", "ISensorDataParser[]",
        "createTable(Session session, Db_connector db) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException");

    output("ISensorDataParser[] parsers= new ISensorDataParser[" + classnames.length + "];");
    empty();

    for(int i= 0; i < classnames.length; i++)
    {
      output("parsers[" + i + "]= (ISensorDataParser) Class.forName(\"" + classnames[i] + "\").getConstructor(Session.class, Db_connector.class).newInstance(session,db);");
    }

    empty();

    output("return parsers;");

    method_end();
  }
}
