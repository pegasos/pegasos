package at.pegasos.generator.generator.server;

import java.io.*;
import java.nio.file.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.servergenerator.SensorConfigParser;
import at.pegasos.generator.parser.serverparser.token.*;

public class Database extends CodeGenerator {
  private static final String INDENT = "  ";
  private PrintStream out;

  private final Sensor sensor;

  public Database(Sensor sensor)
  {
    this.sensor= sensor;
  }

  public void setOutput(PrintStream output)
  {
    this.out = output;
  }

  public void generate() throws GeneratorException
  {
    // If options are null there is no configuration. We cannot generate something
    // otherwise if GENERATE_MODEL is set to false we also do not want to generate a model
    if( sensor.getOptions() == null || sensor.getOptions().getOption(Options.GENERATE_MODEL) != null &&
            sensor.getOptions().getOption(Options.GENERATE_MODEL).equalsIgnoreCase("false") )
    {
    }
    else if( sensor.getDatabaseDefinition() == null && sensor.getFields() != null )
    {
      generateFields();
    }
    else if( sensor.getDatabaseDefinition() != null )
    {
      out.println("-- begin genereated table --");
      out.println("CREATE TABLE IF NOT EXISTS `" + sensor.db_table  + "` (");
      out.print(sensor.getDatabaseDefinition());
      out.println(") ENGINE=InnoDB ;");
      out.println("-- end genereated table --");
    }
    else
      throw new GeneratorException("There is possibly something wrong with sensor " + sensor.name + ": Has neither database nor fields");
  }

  private void generateFields()
  {
  	boolean fixed_length= sensor.getOptions().isFixedLength();
  	boolean optional= sensor.getOptions().isOptionalLength();
    String id_name= sensor.name.toLowerCase() + "_id";

    out.println("-- begin genereated table --");
    out.println("CREATE TABLE IF NOT EXISTS `" + sensor.db_table  + "` (");

    out.println(INDENT + "`" + id_name + "` bigint unsigned NOT NULL AUTO_INCREMENT,");
    out.println(INDENT + "`session_id` bigint unsigned NOT NULL,");
    if( sensor.getOptions().isUseSensorNr() )
    {
      // sensor numbers can be null -> ie no info was given
      out.println(INDENT + "`sensor_nr` smallint,");
    }
    out.println(INDENT + "`rec_time` bigint unsigned NOT NULL,");

    for(Field f : sensor.getFields())
    {
      String name= f.name;
      if( f.options.hasDifferentDBname() )
      {
        name= f.options.getDBname();
      }

      if( f.raw_type.isText() )
      {
        out.print(INDENT + "`" + name + "` " + f.db_type.getSqlDef() + "(" + f.options.getMaxLength() + ")");
      }
      else
      {
        out.print(INDENT + "`" + name + "` " + f.db_type.getSqlDef());
      }

      if( fixed_length || (optional && !f.isOptional()) )
        out.println(" NOT NULL,");
      else
        out.println(",");
    }

    out.println(INDENT + "UNIQUE KEY `" + id_name + "` (`" + id_name + "`),");
    out.println(INDENT + "FOREIGN KEY (`session_id`) REFERENCES session (session_id) ON DELETE CASCADE ON UPDATE CASCADE");
    out.println(") ENGINE=InnoDB ;");
    out.println("-- end genereated table --");
    out.println();
  }

  @Override
  public void setOutputToPrintStream(Path basedir) throws IOException
  {
    Path p= basedir;

    if( !Files.exists(p) )
      Files.createDirectories(p);

    p= p.resolve(sensor.db_table + ".sql");

    this.out= new PrintStream(Files.newOutputStream(p));
  }

  public static void main(String[] args) throws IOException, ParserException, GeneratorException
  {
    Sensor s= new Sensor("13 Bike    bike    generate    sensors/bike.txt".split("\t"));
    SensorConfigParser parser= new SensorConfigParser(args[0], s);
    parser.parse();
    
    Database gen= new Database(s);
    gen.setOutput(System.out);
    gen.generate();
  }
}
