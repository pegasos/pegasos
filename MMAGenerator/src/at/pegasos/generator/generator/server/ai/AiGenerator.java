package at.pegasos.generator.generator.server.ai;

import java.io.IOException;
import java.nio.file.Path;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Config;
import at.pegasos.generator.util.*;
import at.pegasos.generator.parser.token.ServerInstance;
import at.pegasos.generator.generator.ConfigGenerator;
import at.pegasos.generator.generator.server.backend.CommandsGenerator;
import at.pegasos.generator.parser.backend.token.Command;

public class AiGenerator {
  private final Path ai_gen_dir;
  private final String name;
  private final CodeGenerator[] generators;
  private final ServerInstance instance;
  private final Config config;

  public AiGenerator(Path outdir, ServerInstance instance) throws ParserException
  {
    this.name = instance.name;

    this.ai_gen_dir = outdir.resolve("server").resolve("ai").resolve("gen");

    this.instance = instance;

    this.config = ConfigDefaults.get(instance.name, "ai");

    generators = new CodeGenerator[4];
    generators[0] = new ModuleTableGenerator(instance.feedbackmodules);
    generators[1] = new PostprocessingmodulesTableGenerator(instance.postprocessingmodules);
    generators[2] = new CommandsGenerator("at.univie.mma.ai", true, instance, "GroupActivity");
    generators[3] = new ConfigGenerator("at.univie.mma.ai", this.config);
  }

  public void run() throws IOException, ParserException, GeneratorException
  {
    updateConfig();

    for(CodeGenerator gen : generators)
    {
      gen.setOutputToPrintStream(ai_gen_dir);
      gen.generate();
      gen.closeOutput();
    }

    for(Command command : instance.commands.getCommands())
    {
      CodeGenerator gen = new CommandGenerator(command);
      gen.setOutputToPrintStream(ai_gen_dir);
      gen.generate();
      gen.closeOutput();
    }
  }

  private void updateConfig() throws ParserException
  {
    config.setValueString("log_file", "ai_" + this.name + ".log");

    config.setValueString("backend.jar", "server_" + instance.name + ".jar");

    // Copy database settings from server config
    config.setValueString("db.driver", instance.config.getValueString("db.driver"));
    config.setValueString("db.url", instance.config.getValueString("db.url"));
    config.setValueString("db.user", instance.config.getValueString("db.user"));
    config.setValueString("db.password", instance.config.getValueString("db.password"));
  }
}
