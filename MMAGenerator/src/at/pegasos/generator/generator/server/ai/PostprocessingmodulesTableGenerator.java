package at.pegasos.generator.generator.server.ai;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.aiparser.token.*;

public class PostprocessingmodulesTableGenerator extends JavaCodeGenerator {

  private final FeedbackModules modules;

  public PostprocessingmodulesTableGenerator(FeedbackModules modules)
  {
    super("at.univie.mma.ai", "PostProcessingModules", null, null);
    
    this.modules= modules;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("java.util.ArrayList");
    addImport("java.util.List");
    addImport("at.univie.mma.ai.postprocessing.PostProcessingModule");
    header();
    
    indent();
    
    method_begin("public static", "List<Class<? extends PostProcessingModule>>", "fromActivity(Parameters p)");
    
    // TODO: make a better guess of the size
    output("List<Class<? extends PostProcessingModule>> ret= new ArrayList<Class<? extends PostProcessingModule>>();");
    
    for(FeedbackModule mod : modules.modules)
    {
      String line= "if( ";
      boolean and= false;
      boolean param= false;
      
      if( mod.param0 != -1 )
      {
        line+= "p.param0 == " + mod.param0;
        and= true;
        param= true;
      }
      
      if( mod.param1 != -1 )
      {
        if( and ) line+= " && ";
        line+= "p.param1 == " + mod.param1;
        and= true;
        param= true;
      }
      
      if( mod.param2 != -1 )
      {
        if( and ) line+= " && ";
        line+= "p.param2 == " + mod.param2;
        and= true;
        param= true;
      }
      
      line+= ")";
      
      if( param )
      {
        output(line);
        indent();
      }
      output("ret.add(" + mod.clasz + ".class);");
      if( param )
        indent_less();
    }
    
    empty();
    output("return ret;");
    
    method_end();
    
    indent_less();
    footer();
  }

}
