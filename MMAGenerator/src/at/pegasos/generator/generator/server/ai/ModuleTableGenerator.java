package at.pegasos.generator.generator.server.ai;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.aiparser.token.*;

public class ModuleTableGenerator extends JavaCodeGenerator {

  private final FeedbackModules modules;

  public ModuleTableGenerator(FeedbackModules modules)
  {
    super("at.univie.mma.ai", "FeedbackModuleTable", null, null);
    
    this.modules= modules;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("activities.AI_module");
    header();
    
    indent();
    
    method_begin("public static", "void", "startModulesFromParameter(Parameters p)");
    
    output("AI_module ai_module= null;");
    empty();
    
    for(FeedbackModule mod : modules.modules)
    {
      String line= "if( ";
      boolean and= false;
      boolean param= false;
      
      if( mod.param0 != -1 )
      {
        line+= "p.param0 == " + mod.param0;
        and= true;
        param= true;
      }
      
      if( mod.param1 != -1 )
      {
        if( and ) line+= " && ";
        line+= "p.param1 == " + mod.param1;
        and= true;
        param= true;
      }
      
      if( mod.param2 != -1 )
      {
        if( and ) line+= " && ";
        line+= "p.param2 == " + mod.param2;
        and= true;
        param= true;
      }
      
      line+= ")";
      
      if( param )
      {
        output(line);
        indent();
      }
      output("ai_module= new " + mod.clasz + "(p);");
      if( param )
        indent_less();
    }
    
    // If no module was started yet --> start empty module.
    empty();
    output("if( ai_module == null )");
    indent();
    output("new activities.Default(p);");
    indent_less();
    
    method_end();
    
    indent_less();
    footer();
  }

}
