package at.pegasos.generator.generator.server.ai;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.backend.token.Command;
import at.pegasos.generator.parser.serverparser.token.ResponseField;

public class CommandGenerator extends JavaCodeGenerator
{
  private final Command command;
  
  public CommandGenerator(Command command)
  {
    super("at.univie.pegasos.ai.commands", command.name, "Command", null);
    
    this.command= command;
  }
  
  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.univie.mma.ai.Commands");
    addImport("at.univie.pegasos.ai.Command");
    
    header();
    
    indent();
    
    StringBuilder params= new StringBuilder("int status");
    boolean vl= false;
    if( command.fields != null ) // TODO: temp workaround. Setting response class causes null pointer exception here
    for(ResponseField f : command.fields)
    {
      String t= f.type != null ? f.type.toJavaType() : "Object";
      
      if( f.nr == -1 )
      {
        params.append(", ").append(t).append("... ").append(f.name);
        vl= true;
      }
      else
        params.append(", ").append(t).append(" ").append(f.name);
    }
    
    method_begin("public static", command.name, "create(" + params + ")");
    
    assign(command.name + " ret", "new " + command.name + "()");
    empty();
    
    assign("ret.code", "Commands.Codes." + command.name);
    assign("ret.status", "status");
    empty();
    
    StringBuilder l;
    if( vl )
    {
      l = new StringBuilder();
      int h= 0;
      for(ResponseField f : command.fields)
      {
        if( f.nr == -1 )
          l.append(" + ").append(f.name).append(".length");
        else
          h++;
      }
      l.insert(0, h);
    }
    // else
    else if( command.fields != null ) // TODO: temp workaround. Setting response class causes null pointer exception here
      l = new StringBuilder("" + command.fields.length);
    else
      l = new StringBuilder("0");
    assign("ret.data", "new Object[" + l + "]");
    int last_nr= 0;
    if( command.fields != null ) // TODO: temp workaround. Setting response class causes null pointer exception here
    for(ResponseField f : command.fields)
    {
      if( f.nr != -1 )
      {
        assign("ret.data[" + f.nr + "]", f.name);
        last_nr= f.nr;
      }
      else
      {
        output("for(int i= 0; i < " + f.name + ".length; i++)");
        indent();
        assign("ret.data[i+" +(last_nr+1)+"]", f.name + "[i]");
        indent_less();
      }
    }
    empty();
    
    output("return ret;");
    
    method_end();
    
    footer();
  }
}
