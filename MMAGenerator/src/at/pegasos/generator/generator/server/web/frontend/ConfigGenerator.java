package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Config;

public class ConfigGenerator extends TypeScriptCodeGenerator {
  private final Config config;

  public ConfigGenerator(String pkg, Config config)
  {
    super(pkg, "Config", null, null);
    this.config = config;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();

    indent();

    for(String def : config.getValueDefinitionsTypeScript())
      output("public static " + def + ";");

    indent_less();
    output("}");
    output("");
  }
}
