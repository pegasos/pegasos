package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;
import java.util.stream.*;

public class TableDescriptionsGenerator extends JavaCodeGenerator {
  private final ArrayList<Sensor> sensors;

  public TableDescriptionsGenerator(ServerInstance inst)
  {
    super("at.pegasos.server.frontback.dbdata", "TableDescriptions", null, null);
    this.sensors = inst.getSensors();
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    indent();
    output("public static TableDescription[] tables = new TableDescription[] {");
    indent();
    for(Sensor sensor : sensors)
    {
      if( sensor.getFields() != null && sensor.getFields().length > 0 )
      {
        String output = "new TableDescription(\"" + sensor.db_table + "\", ";
        if( sensor.getOptions() != null && sensor.getOptions().isUseSensorNr() )
        {
          output += "new String[] {";
        }
        output += Arrays.stream(sensor.getFields()).map(f -> {
          if( f.getOptions().hasDifferentDBname() )
          {
            return "\"" + f.getOptions().getDBname() + "\"";
          }
          else
            return "\"" + f.name + "\"";
        }).collect(Collectors.joining(", "));

        if( sensor.getOptions() != null && sensor.getOptions().isUseSensorNr() )
        {
          output += "}, true), ";
        }
        else
        {
          output += "),";
        }

        output(output);
      }
    }
    indent_less();
    output("};");
    footer();
  }
}
