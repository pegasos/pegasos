package at.pegasos.generator.generator.server.web.frontend;

import java.nio.file.Paths;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.ParserException;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.webparser.parsers.AdminModulesParser;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.AdminModules.Module;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebFrontendGenerator
public class AdminModuleMenu extends TypeScriptCodeGenerator {

  private final AdminModules modules;

  public AdminModuleMenu(Web web)
  {
    super("admin", "menu", null, null);
    this.modules= web.getAdminModules();
  }

  @Override
  public void generate() throws GeneratorException
  {
    /*[{
      label: 'File',
      items: [
        {label: 'New', queryParams: ['test']},
        {label: 'Open'}
      ]
    },
    {
      label: 'User',
      items: [
        {label: 'Add', command: () => this.showTool('AddUser')},
        {label: 'Edit Roles', command: () => this.showTool('EditRole')}
      ]
    }]*/

    addImport("{ AdminPanelComponent } from './admin.component'");

    headerNoClass();

    output("export function getMenu(panel: AdminPanelComponent) {");

    String menu= "[\n";
    for(Module mod : modules.modules.sub.values())
    {
      menu+= modToString(mod, "  ", "  ");
    }
    menu+= "];";

    output("  return " + menu);
    output("}");

    footerNoClass();
  }
  
  private String modToString(Module mod, String curInd, String ind)
  {
    String ret= "";
    ret+= curInd + "{\n";
    ret+= curInd + ind + "label: '" + mod.label + "',\n";
    if( mod.sub.size() > 0 )
    {
      ret+= curInd + ind + "items : [\n";
      for(Module submod : mod.sub.values())
      {
        ret+= modToString(submod, curInd + ind, ind);
      }
      ret+= curInd + ind + "],\n";
    }
    else
    {
      ret+= curInd + ind + "command: () => panel.showTool('" + mod.action + "'),\n";
    }
    ret+= curInd + "},\n";
    return ret;
  }

  public static void main(String[] args) throws GeneratorException, ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("../server");
    IInstance inst= new Application(i);
    AdminModulesParser parser= new AdminModulesParser(inst, i.directory.resolve(".."));
    parser.parse();
    log.debug(inst.getWeb().getAdminModules().toString());

    AdminModuleMenu gen= new AdminModuleMenu(inst.getWeb());
    gen.setOutput(System.out);
    gen.generate();
  }
}
