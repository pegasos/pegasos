package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.ActivityNames.*;
import at.pegasos.util.DAG.*;

import java.io.*;

@WebBackendGenerator
public class ActivityNamesGenerator extends JavaCodeGenerator {
  private final ActivityNames names;

  public ActivityNamesGenerator(Web web)
  {
    super("at.pegasos.server.frontback", "ActivityNames", null, null);
    this.names = web.activitynames;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    
    indent();
    
    method_begin("public static", "String", "getActivityName(int param0, int param1, int param2)");
    
    Node<Activity> root= names.getRoot();
    if( root.getChildren().size() == 0 )
    {
      output("return \"\";");
    }
    else
    {
      root.ordered((a1, a2) -> {
        Activity aa1 = a1.getID();
        Activity aa2 = a2.getID();
        if( aa1.param1 != -1 )
        {
          if( aa1.param2 != -1 )
          {
            return Integer.compare(aa1.param2, aa2.param2);
          }
          return Integer.compare(aa1.param1, aa2.param1);
        }
        return Integer.compare(aa1.param0, aa2.param0);
      });

      // root.sortSuccesors();
      root.traverse(new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
//          System.out.println("visit " + id + " parent:" + (parent != null ? parent.getID() : ""));
          if( id.param0 == id.param1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            // This is the root
            output("switch( param0 )");
            output("{");
            indent();
          }
          else if( id.param0 != -1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            output("case " + id.param0 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2*/);
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param1 )");
              output("{");
              indent();
            }
          }
          else if( id.param0 != -1 && id.param1 != -1 && id.param2 == -1 )
          {
            output("case " + id.param1 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2*/);
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param2 ) ");
              output("{");
              indent();
            }
          }
          else
          {
            output("case " + id.param2 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2 */);
            indent();
          }
        }
      }, new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
//          System.out.println("post-visit " + id);

          if( node.getChildNodes().size() > 0 )
          {
            output("default:");
            indent();
            output("break;");
            indent_less();

            indent_less();
            output("}");

            // do we have siblings and are not an output node?
            if( parent != null && id.name == null && parent.getChildNodes().size() > 1 )
              output("break;");
          }

          if( id.name != null )
          {
            output("return \"" + id.name + "\";" /* + " " + id.param0 + "/" + id.param1 + "/" + id.param2 */);
            // indent_less();
          }
          indent_less();
        }
      });

      // output("}");
      indent();
      output("return \"\";");
      // method_end();
    }
    method_end();
    
    footer();
  }
}
