package at.pegasos.generator.generator.server.web.backend;

import java.util.*;
import java.util.Comparator;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.util.DAG.Node;
import at.pegasos.util.DAG.NodeVisitor;
import at.pegasos.generator.parser.webparser.token.charts.Activity;
import at.pegasos.generator.parser.webparser.token.Charts.Chart;

@WebBackendGenerator
public class AvailableChartsGenerator extends JavaCodeGenerator {
  private final Charts charts;
  private int csc;
  private final Collection<Chart> cs;

  private final static String defaultList = "defaultCharts";
  private final static String availList = "availableCharts";

  public AvailableChartsGenerator(Web web)
  {
    super("at.pegasos.server.frontback.charts", "AvailableCharts", null, null);
    this.charts = web.charts;

    cs = charts.getCharts();
    csc= cs.size();

    charts.getRoot().forEach((activity) -> {
      activity.chartsPre.sort(Comparator.comparingInt(c -> c.order));
      activity.chartsPost.sort(Comparator.comparingInt(c -> c.order));
    });
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.pegasos.server.frontback.data.Type");
    addImport("at.pegasos.server.frontback.data.chart.*");
    addImport("at.pegasos.server.frontback.charts.DBChartMultiLine.LineType");

    addImport("java.util.ArrayList");
    addImport("java.util.List");

    header();

    indent();

    member("private final static", "List<AvailableChart>", defaultList);
    member("private final static", "List<AvailableChart>", availList);
    empty();

    members();
    staticInitializer();

    empty();

    getById();

    forSession();

    footer();
  }

  private void getById()
  {
    method_begin("public static", "AvailableChart", "getByID(String id)");
    output("for(AvailableChart chart : " + availList + ")");
    output("{");
    indent();
    output("if( chart.name.equals(id) )");
    indent();
    output("return chart;");
    indent_less();
    indent_less();
    output("}");
    output("return null;");
    method_end();
  }

  private void members()
  {
    for(Chart c : cs)
    {
      member("private final static", "AvailableChart", c.name, c.getAvailConstructor());
      if( !c.default_avail )
        csc--;
    }
    empty();
    returnListsNames();
    empty();
  }

  void staticInitializer()
  {
    output("static {");
    indent();
    assign(defaultList, "new ArrayList<AvailableChart>(" + csc + ")");
    for(Chart c : cs)
    {
      if( c.default_avail )
        output(defaultList + ".add(" + c.name + ");");
    }
    empty();

    assign(availList, "new ArrayList<AvailableChart>(" + cs.size() + ")");
    for(Chart c : cs)
    {
      output(availList + ".add(" + c.name + ");");
    }
    empty();

    returnLists();

    indent_less();
    output("}");
  }

  private void returnListsNames()
  {
    if( charts.getRoot().getChildren().size() > 0 )
    {
      Node<Activity> root= charts.getRoot();

      root.traverse(new NodeVisitor<Activity>() {

        @Override
        public void run() { }
      }, new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
          if( ( id.param0 != -1 || id.param1 != -1 || id.param2 != -1 ) &&
              (id.adddefault || getChartsPre(id).size() > 0 || getChartsPost(id).size() > 0 ) )
          {
            String varname = "ret";
            if( id.param2 != -1 )
            {
              varname += id.param0 + "_" + id.param1 + "_" + id.param2;
            }
            else if( id.param1 != -1 )
            {
              varname += id.param0 + "_" + id.param1;
            }
            else
            {
              varname += id.param0;
            }
            member("private final static", "List<AvailableChart>", varname);
          }
        }
      });
    }
  }

  private void returnLists()
  {
    if( charts.getRoot().getChildren().size() > 0 )
    {
      Node<Activity> root= charts.getRoot();

      root.traverse(new NodeVisitor<Activity>() {

        @Override
        public void run()
        {

        }
      }, new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
//          System.out.println("Visiting " + id);
          // root
          // if( id.param0 != -1 || id.param1 != -1 || id.param2 != -1 )
          if( id.adddefault || getChartsPre(id).size() > 0 || getChartsPost(id).size() > 0 )
          {
            String varname = "ret";
            if( id.param2 != -1 )
            {
              varname += id.param0 + "_" + id.param1 + "_" + id.param2;
            }
            else if( id.param1 != -1 )
            {
              varname += id.param0 + "_" + id.param1;
            }
            else
            {
              varname += id.param0;
            }
            assign(varname, "new ArrayList<AvailableChart>(10)");

            for(Activity.Chart chart : getChartsPre(id))
            {
              output(varname + ".add(" + chart.name + ");");
            }
            if( id.adddefault )
              output(varname + ".addAll(" + defaultList + ");");
            for(Activity.Chart chart : getChartsPost(id))
            {
              output(varname + ".add(" + chart.name + ");");
            }
          }
        }
      });
    }
  }

  void forSession()
  {
    charts.debugTree();

    // public static AvailableChart[] ForSession(int param0, int param1, int param2)
    method_begin("public static", "List<AvailableChart>", "ForSession(int param0, int param1, int param2)");

    // If no nodes are present we are done here ...
    if( charts.getRoot().getChildren().size() == 0 )
    {
      output("return " + defaultList + ";");
      method_end();
    }
    else
    {
      Node<Activity> root= charts.getRoot();

      root.traverse(new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
          if( id.param0 == id.param1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            // This is the root
            output("switch( param0 )");
            output("{");
            indent();
          }
          else if( id.param0 != -1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            output("case " + id.param0 + ":");
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param1 )");
              output("{");
              indent();
            }
          }
          else if( id.param0 != -1 && id.param1 != -1 && id.param2 == -1 )
          {
            output("case " + id.param1 + ":");
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param2 ) ");
              output("{");
              indent();
            }
          }
          else
          {
            output("case " + id.param2 + ":");
            indent();
          }
        }
      }, new NodeVisitor<Activity>() {

        @Override
        public void run()
        {
//          System.out.println("pv " + id);

          boolean doOutput = id.adddefault || getChartsPre(id).size() > 0 || getChartsPost(id).size() > 0;

          if( node.getChildNodes().size() > 0 )
          {
            indent_less();
            output("}");
            // do we have siblings and are not an output node?
            if( parent != null && !doOutput && parent.getChildNodes().size() > 1 )
              output("break;");
          }

          if( doOutput )
          {
            String varname = "ret";
            if( id.param2 != -1 )
            {
              varname += id.param0 + "_" + id.param1 + "_" + id.param2;
            }
            else if( id.param1 != -1 )
            {
              varname += id.param0 + "_" + id.param1;
            }
            else
            {
              varname += id.param0;
            }
            output("return " + varname + ";");
            // indent_less();
          }
          indent_less();
        }
      });

      // output("}");
      indent();
      output("return " + defaultList + ";");
      method_end();
    }
  }

  private List<Activity.Chart> getChartsPre(Activity node)
  {
    List<Activity.Chart> ret;
    try
    {
      if( node.overwrite != null && node.overwrite )
      {
        return node.chartsPre;
      }

      Activity n = charts.getAvailNode("" + node.param0, "", "");
      ret = new ArrayList<>(n.chartsPre);
      if( node.param1 != -1 )
      {
        n = charts.getAvailNode("" + node.param0, "" + node.param1, "");
        ret.addAll(n.chartsPre);
        if( node.param2 != -1 )
        {
          ret.addAll(node.chartsPre);
        }
      }
    }
    catch (ParserException e)
    {
      throw new RuntimeException(e);
    }

    return ret;
  }

  private List<Activity.Chart> getChartsPost(Activity node)
  {
    // TODO: better look up;
    List<Activity.Chart> ret;
    try
    {
      if( node.overwrite != null && node.overwrite )
      {
        return node.chartsPost;
      }

      Activity n = charts.getAvailNode("" + node.param0, "", "");
      ret = new ArrayList<>(n.chartsPost);
      if( node.param1 != -1 )
      {
        n = charts.getAvailNode("" + node.param0, "" + node.param1, "");
        ret.addAll(n.chartsPost);
        if( node.param2 != -1 )
        {
          ret.addAll(node.chartsPost);
        }
      }
    }
    catch (ParserException e)
    {
      throw new RuntimeException(e);
    }
    return ret;
  }
}
