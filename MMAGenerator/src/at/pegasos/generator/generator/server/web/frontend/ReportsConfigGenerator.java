package at.pegasos.generator.generator.server.web.frontend;

import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.stream.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;
import lombok.extern.slf4j.*;

@WebFrontendGenerator @Slf4j
public class ReportsConfigGenerator extends TypeScriptCodeGenerator {

  private final Collection<Report> reports;

  @SuppressWarnings("unchecked")
  public ReportsConfigGenerator(Web web)
  {
    super("shared", "reports", null, null);
    Collection<Report> reports = (Collection<Report>) web.get("reports");
    if( reports == null )
    {
      reports = Collections.EMPTY_LIST;
    }
    this.reports = reports;
  }

  private final Map<String, String> configs = new HashMap<>();

  @Override
  public void generate() throws GeneratorException
  {
    addImport("{ ReportInfo, ReportConfig, MultiLineReportConfig, BarsReportConfig, CurvedLineReportConfig, SessionbasedLineReportConfig } from './reportconfig'");
    addImport(" * as transform from './datatransformation'");

    AtomicInteger i = new AtomicInteger();

    reports.stream().filter(r -> r.display != null).forEach(report -> {
      String var = "c" + (i.getAndIncrement());
      configs.put(report.name, var);

      String constr = "new " + constructor(report.display.type) + "('" + report.name + "')";
      globalvar(var, constr);
    });

    String reportsConst = "[" +
            reports.stream().filter(r -> r.display != null)
            .map(r ->
                    "new ReportInfo('" + r.name + "', '" + r.display.title + "', '"  + reportType(r.display.type)  + "', " + r.display.defaultWeeks + ", " + this.configs.get(r.name) + ")").collect(Collectors.joining(",\n" + INDENT)) +
            "]";

    constant("REPORTS: ReportInfo[]", reportsConst);

    headerNoClass();
    empty();

    reports.stream().filter(r -> r.display != null).forEach(report -> {
      String var = configs.get(report.name);

      if( report instanceof MetricBasedAccumulatedReport &&
          !report.display.config.containsKey("xAxisScale") )
      {
        inferScale((MetricBasedAccumulatedReport) report);
      }
      report.display.config.forEach((key, value) -> output(var + "." + key + " = " + value + ";"));
    });
    reportsWithNoDisplay();

    footerNoClass();
  }

  private void reportsWithNoDisplay()
  {
    // Go over all reports and check if we can infer xAxisScale
    for(Report report : reports)
    {
      log.info("{} has no display", report);
      if( report instanceof MetricBasedAccumulatedReport )
      {
        MetricBasedAccumulatedReport mbarep = (MetricBasedAccumulatedReport) report;
        if( mbarep.display != null )
        {
          if( !mbarep.display.config.containsKey("xAxisScale") )
          {
            inferScale(mbarep);
          }
        }
      }
    }
  }

  private void inferScale(MetricBasedAccumulatedReport mbarep)
  {
    switch( mbarep.groupBy )
    {
      case DAY:
        mbarep.display.config.put("xAxisScale", "'Day'");
        break;
      case WEEK:
        mbarep.display.config.put("xAxisScale", "'Week'");
        break;
      case MONTH:
        mbarep.display.config.put("xAxisScale", "'Month'");
        break;
      case YEAR:
        mbarep.display.config.put("xAxisScale", "'Year'");
        break;
      case ALL:
        // mbarep.display.config.put("xAxisScale", "None");
        break;
    }
  }

  private String constructor(Display.ReportType type)
  {
    switch( type )
    {
      case Bars:
        return "BarsReportConfig";
      case SessionbasedLines:
        return "SessionbasedLineReportConfig";
      case Lines:
        return "MultiLineReportConfig";
    }
    throw new IllegalStateException();
  }

  private static String reportType(Display.ReportType type)
  {
    switch( type )
    {
      case Bars:
        return "Bars";
      case SessionbasedLines:
        return "SessionbasedLines";
      case Lines:
        return "Multiline";
    }
    return "";
  }
}
