package at.pegasos.generator.generator.server.web.frontend;

import java.util.*;
import java.util.stream.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.MenuEntry;
import lombok.extern.slf4j.*;

@Slf4j
public class MenuGenerator extends TypeScriptCodeGenerator {
  
  private final List<MenuEntry> menu;

  public MenuGenerator(List<MenuEntry> menu)
  {
    super("shared", "menus", null, null);
    this.menu = menu;
  }

  @Override
  public void generate() throws GeneratorException
  {
    if(menu.stream().anyMatch(menu -> menu.isDummy))
    {
      menu.stream().filter(menuEntry -> menuEntry.isDummy).forEach(menuEntry -> log.error("Dummy entry: {}", menuEntry));
      for(MenuEntry e : menu)
        log.debug("{}", e);
      throw new GeneratorException("There are dummy entries in the menu");
    }

    addImport("{ Menu } from './menu'");
    
    menu.sort(Comparator.comparingInt(o -> o.order));

    StringBuilder menus= new StringBuilder("[\n");
    for(MenuEntry entry : menu)
    {
      menus.append(entryToString(entry)).append(",\n");
    }
    menus.append("]");

    constant("MENUS: Menu[]", menus.toString());

    headerNoClass();

    footerNoClass();
  }

  private static StringBuilder entryToString(MenuEntry entry)
  {
    StringBuilder menu = new StringBuilder();
    menu.append(INDENT + "new Menu('").append(entry.action).append("', '").append(entry.name).append("'");
    boolean roles = false;
    boolean isPublic = false;
    if( entry.requiredRoles.size() > 0 )
    {
      menu.append(", false, [").append(
                      entry.requiredRoles.stream()
                              .map(r -> "'" + r + "'")
                              .collect(Collectors.joining(",")))
              .append("]");
      roles = true;
    }
    else if( entry.isPublic != null )
    {
      menu.append(", ").append(entry.isPublic);
      isPublic = true;
    }

    if( entry.subMenus.size() > 0 )
    {
      if( !roles )
        menu.append(", []");
      if( !isPublic )
        menu.append(", true");

      menu.append(", [\n");
      for(MenuEntry subEntry : entry.subMenus )
        menu.append(INDENT).append(entryToString(subEntry)).append(",\n");
      menu.append(INDENT).append("]");
    }
    menu.append(")");
    return menu;
  }
}
