package at.pegasos.generator.generator.server.web.backend;

import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.util.DAG.*;

@WebBackendGenerator
public class ExportsGenerator extends JavaCodeGenerator {
  private final Exports exports;

  public ExportsGenerator(Web web)
  {
    super("at.pegasos.server.frontback.config", "Exports", null, null);
    this.exports = web.exports;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.pegasos.server.frontback.controller.*");
    addImport("at.pegasos.server.frontback.data.*");
    addImport("at.pegasos.server.frontback.dbdata.*");
    addImport("at.pegasos.server.frontback.exports.*");
    addImport("at.pegasos.server.frontback.permission.*");
    addImport("org.springframework.context.*");
    addImport("java.security.*");
    addImport("java.util.*");

    header();
    indent();

    Node<Exports.ExportActivity> root= exports.getRoot();
    method_begin("public static", "void", "fillInExports(int param0, int param1, int param2, Activity activity, Principal person, ApplicationContext applicationContext)");

    if( root.getChildren().size() == 0 )
    {
      output("// no exports configured");
    }
    else
    {
      root.ordered((a1, a2) -> {
        Activity aa1 = a1.getID();
        Activity aa2 = a2.getID();
        if( aa1.param1 != -1 )
        {
          if( aa1.param2 != -1 )
          {
            return Integer.compare(aa1.param2, aa2.param2);
          }
          return Integer.compare(aa1.param1, aa2.param1);
        }
        return Integer.compare(aa1.param0, aa2.param0);
      });

      root.traverse(new NodeVisitor<Exports.ExportActivity>() {

        @Override
        public void run()
        {
          if( id.param0 == id.param1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            // This is the root
            output("switch( param0 )");
            output("{");
            indent();
          }
          else if( id.param0 != -1 && id.param1 == id.param2 && id.param2 == -1 )
          {
            output("case " + id.param0 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2*/);
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param1 )");
              output("{");
              indent();
            }
          }
          else if( id.param0 != -1 && id.param1 != -1 && id.param2 == -1 )
          {
            output("case " + id.param1 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2*/);
            indent();
            if( node.getChildNodes().size() > 0 )
            {
              output("switch( param2 ) ");
              output("{");
              indent();
            }
          }
          else
          {
            output("case " + id.param2 + ":" /*+ " " + id.param0 + "/" + id.param1 + "/" + id.param2 */);
            indent();
          }
        }
      }, new NodeVisitor<Exports.ExportActivity>() {

        @Override
        public void run()
        {
          List<Exports.Export> exports = getChartsPre(id);
          boolean doOutput = /*id.adddefault ||*/ exports.size() > 0;

          if( node.getChildNodes().size() > 0 )
          {
            indent_less();
            output("}");
            // do we have siblings and are not an output node?
            if( parent != null && !doOutput && parent.getChildNodes().size() > 1 )
              output("break;");
          }

          if( doOutput )
          {
            output("if( activity.downloads == null )");
            indent();
            output("activity.downloads = new ArrayList<>();");
            indent_less();

            for(Exports.Export export : exports)
            {
              if( ExportsGenerator.this.exports.getType(export.typeName) == null )
                throw new RuntimeException("Cannot generate export " + export.typeName);
              beginIf(ExportsGenerator.this.exports.getType(export.typeName).className + ".isApplicable(activity, person, applicationContext)", false);
              output("activity.downloads.add(new Download(\"" + export.typeName + "\"));");
              endIf(false);
            }
            output("break;");
          }
          indent_less();
        }
      });

      // output("}");
      indent();

    }
    method_end();
    getExports();

    footer();
  }

  private void getExports()
  {
    method_begin("public static", "SessionExport", "getExporter(PrivilegeManager privilegeManager, Principal p, Session session, String type, ApplicationContext applicationContext) throws InsufficientPrivilegesException");
    if( exports.getDeclaredTypes().size() == 0 )
    {
      output("throw new IllegalStateException(\"No exports configured\");");
    }
    else
    {
      output("if( privilegeManager.hasPrivilegeActivity(p, \"export\", session) )");
      output("{");
      indent();
      output("switch (type)");
      output("{");
      indent();
      for(String stype : exports.getDeclaredTypes())
      {
        Exports.ExportType type = exports.getType(stype);
        output("case \"" + type.typeName + "\":");
        output(INDENT + "return new " + type.className + "(session, applicationContext);");
      }
      output("default:");
      output(INDENT + "throw new IllegalStateException(\"Unexpected value: \" + type);");
      indent_less();
      output("}");
      indent_less();
      output("}");
      output("else");
      output(INDENT + "throw new InsufficientPrivilegesException();");
    }
    method_end();
  }

  private List<Exports.Export> getChartsPre(Exports.ExportActivity node)
  {
    List<Exports.Export> ret = new ArrayList<>();
    try
    {
      Exports.ExportActivity n = exports.getAvailNode(String.valueOf(node.param0), "", "");
      if( n.exports != null )
        ret.addAll(n.exports);
      if( node.param1 != -1 )
      {
        n = exports.getAvailNode(String.valueOf(node.param0), String.valueOf(node.param1), "");
        if( n.exports != null )
          ret.addAll(n.exports);
        if( node.param2 != -1 && node.exports != null )
        {
          ret.addAll(node.exports);
        }
      }
    }
    catch (ParserException e)
    {
      throw new RuntimeException(e);
    }

    return ret;
  }
}
