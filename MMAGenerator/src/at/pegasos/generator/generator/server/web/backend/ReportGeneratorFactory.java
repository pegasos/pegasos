package at.pegasos.generator.generator.server.web.backend;

import java.util.*;
import java.util.stream.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;
import lombok.extern.slf4j.*;

@Slf4j
@WebBackendGenerator
public class ReportGeneratorFactory extends JavaCodeGenerator {

  private final Collection<Report> reports;

  @SuppressWarnings("unchecked")
  public ReportGeneratorFactory(Web web)
  {
    super("at.pegasos.server.frontback.reports", "ReportGeneratorFactory", null, null);
    reports = (Collection<Report>) web.get("reports");
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("java.util.*");
    addImport("at.pegasos.server.frontback.data.*");
    addImport("at.pegasos.server.frontback.repositories.*");
    addImport("at.pegasos.server.frontback.services.*");
    addImport("at.pegasos.server.frontback.controller.ReportController.*");
    addImport("org.springframework.beans.factory.annotation.*");
    addImport("org.springframework.jdbc.core.*");
    addImport("org.springframework.stereotype.*");

    header("@Service");

    indent();

    annotatedmember("private", "UserValueService", "valueservice", "Autowired");
    annotatedmember("private", "MetricInfoRepository", "metrirep", "Autowired");
    annotatedmember("private", "MetricRepository", "metrrep", "Autowired");
    annotatedmember("private", "SessionInfoRepository", "sirepo", "Autowired");
    annotatedmember("private", "RepositoryService", "repositoryService", "Autowired");
    annotatedmember("private", "JdbcTemplate", "jdbcTemplate", "Autowired");

    method_begin("public", "ReportGenerator", "getReport(String name)");

    if( reports != null && reports.size() > 0 )
    {
      variable("ReportGenerator", "gen");

      output("switch( name )");
      output("{");
      indent();

      for(Report r : reports)
      {
        generate(r);
      }

      output("default:");
      indent();
      output("throw new IllegalArgumentException(\"Unknown Report \" + name);");
      indent_less();

      indent_less();
      output("}");

      output("return gen;");
    }
    else
      output("return null;");
    method_end();

    defaultDate();

    footer();
  }

  private void defaultDate()
  {
    method_begin("public", "void", "setDefaultDate(String name, ReportGenerator generator)");

    if( reports != null && reports.size() > 0 )
    {
      variable("Calendar", "start");
      assign("start", "Calendar.getInstance()");
      output("start.setTimeInMillis(System.currentTimeMillis());");

      output("switch( name )");
      output("{");
      indent();

      for(Report r : reports)
      {
        output("case \"" + r.name + "\":");
        indent();
        if( r.display != null )
        {
          output("start.add(Calendar.WEEK_OF_YEAR, -" + r.display.defaultWeeks + ");");
        }
        else
        {
          log.warn("No default period set for {}", r);
          output("start.add(Calendar.YEAR, -1);");
        }
        output("break;");
        indent_less();
      }

      output("default:");
      indent();
      output("throw new IllegalArgumentException(\"Unknown Report \" + name);");
      indent_less();

      indent_less();
      output("}");

      output("generator.setStart(start);");
    }
    method_end();
  }

  private void generate(Report r)
  {
    log.debug("Generate report {}", r);
    output("case \"" + r.name + "\":");
    indent();
    if( r instanceof UserValueBasedReport )
    {
      generate((UserValueBasedReport) r);
    }
    else if( r instanceof MetricBasedAccumulatedReport )
    {
      generate((MetricBasedAccumulatedReport) r);
    }
    else if( r instanceof MetricBasedGroupedReport )
    {
      generate((MetricBasedGroupedReport) r);
    }
    else if( r instanceof MetricBasedReport )
    {
      generate((MetricBasedReport) r);
    }
    else if( r instanceof SessionValueReport )
    {
      generate((SessionValueReport) r);
    }
    else
    {
      throw new IllegalStateException("Unexpected value: " + r.getClass());
    }
    output("break;");
    indent_less();
  }

  private void generate(UserValueBasedReport r)
  {
    String names = Arrays.stream(r.names).map(n -> "\"" + n + "\"").collect(Collectors.joining(", "));
    assign("gen", "new UserValueBasedReportGenerator(valueservice, new String[] {" + names + "})");
  }

  private void generate(MetricBasedReport r)
  {
    String metrics = Arrays.stream(r.metrics).map(n -> "\"" + n + "\"").collect(Collectors.joining(", "));
    if( r.includeFilters != null && r.includeFilters.include.length > 0 )
    {
      assign("gen", "new MetricBasedReportGenerator(metrirep, metrrep, sirepo, new String[] {" + metrics + "}, "+
          generateFilter(r) +
          ")");
    }
    else
    {
      assign("gen", "new MetricBasedReportGenerator(metrirep, metrrep, sirepo, new String[] {" + metrics + "})");
    }
  }

  private String generateFilter(MetricBasedReport r)
  {
    return "Collections.singletonList(" + Arrays.stream(r.includeFilters.include).map(i -> "new " + i.constr()).collect(Collectors.joining(", ")) + ")";
  }

  private String generateFilter(SessionValueReport r)
  {
    if( r.includeFilters != null && r.includeFilters.include.length > 0 )
      return "Collections.singletonList(" + Arrays.stream(r.includeFilters.include).map(i -> "new " + i.constr()).collect(Collectors.joining(", ")) + ")";
    else
      return "List.of(new SessionTypeFilter[0])";
  }

  private void generate(MetricBasedAccumulatedReport r)
  {
    String c = "MetricBasedAccumulatedReportGenerator" + r.type;
    String metrics = Arrays.stream(r.metrics).map(n -> "\"" + n + "\"").collect(Collectors.joining(", "));
    if( r.includeFilters != null && r.includeFilters.include.length > 0 )
    {
      assign("gen", "new " + c + "(metrirep, metrrep, sirepo, new String[] {" + metrics + "}, "+
          generateFilter(r) +
          ")");
    }
    else
    {
      assign("gen", "new " + c + "(metrirep, metrrep, sirepo, new String[] {" + metrics + "})");
    }

    // ((MetricBasedAccumulatedReportGeneratorParam) gen).setAggregate(Aggregate.SUM);
    //      ((MetricBasedAccumulatedReportGeneratorParam) gen).setGroupBy(GroupBy.WEEK);
    output("((" + c + ") gen).setAggregate(Aggregate." + r.aggregate.toString() + ");");
    output("((" + c + ") gen).setGroupBy(GroupBy." + r.groupBy.toString() + ");");
  }

  private void generate(MetricBasedGroupedReport r)
  {
    String metrics = Arrays.stream(r.metrics).map(n -> "\"" + n + "\"").collect(Collectors.joining(", "));
    String sessionFilter = "null";
    String lapQuery = "null";
    if( r.includeFilters != null && r.includeFilters.include.length > 0 )
    {
      sessionFilter = generateFilter(r);
    }
    if( r.lapQuery != null )
    {
      output("{");
      indent();
      assign("LapFilters lapQuery", "new LapFilters()");
      lapQuery = "lapQuery";
      assign("lapQuery.conj", "Conjunction." + r.lapQuery.conj.toString());
      // List<LapFilter> filters;
      assign("lapQuery.filters", "new ArrayList<>(" + r.lapQuery.filters.size() + ")");
      for(MetricBasedGroupedReport.LapQuery.LapFilter filter : r.lapQuery.filters)
      {
        output("lapQuery.filters.add(new LapFilter(LapFilterType." + filter.type + ", " + filter.value +
            ", at.pegasos.server.frontback.data.Comparator." + filter.comp + "));");
      }
    }

    assign("gen", "new MetricBasedReportGeneratorGrouped(repositoryService, new String[] {" + metrics + "}, " +
        sessionFilter + ", " + lapQuery + ")");
    if( r.lapQuery != null )
    {
      indent_less();
      output("}");
    }
    output("((MetricBasedReportGeneratorGrouped) gen).setGroupBy(GroupBy." + r.groupBy.toString() + ");");
  }

  private void generate(SessionValueReport r)
  {
    String dataQuery = Arrays.stream(r.dataQuery.filter).map(f ->
        INDENT + INDENT + INDENT + INDENT + INDENT + "new SessionDataFilter(\"" + f.table + "\", \"" + f.field + "\", \"" + f.name + "\"" +
            (f.where != null ? ", \"" + f.where + "\"" : "") + ")").collect(Collectors.joining(",\n"));
    String sessionFilter = generateFilter(r);
    assign("gen", "new SessionValueReportGenerator(jdbcTemplate, sirepo, new SessionDataFilter[] {\n" + dataQuery +
        "\n" + INDENT + INDENT + INDENT + INDENT + "}, " +
        sessionFilter + ")");
  }
}

