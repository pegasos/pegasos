package at.pegasos.generator.generator.server.web.frontend;

import java.nio.file.Paths;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.ParserException;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.parsers.AdminModulesParser;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.AdminModules.Module;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebFrontendGenerator
public class AdminModuleFactory extends TypeScriptCodeGenerator {

  private final AdminModules modules;
  private String admincomponents;
  private boolean first;

  public AdminModuleFactory(Web web)
  {
    super("admin", "ToolFactory", null, null);
    this.modules= web.getAdminModules();
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("{ ComponentFactoryResolver, ComponentFactory } from '@angular/core'");
    addImport("{ AdminToolComponent } from './admin.tool.component'");

    admincomponents= "[";
    first= true;

    log.debug("generate addImports: modules {} modules.modules {}", modules, modules.modules);

    if( modules.modules.sub != null )
      addImports(modules.modules);
    else
      log.debug("No admin modules in this web instance");

    admincomponents+= "]";

    constant("ADMINCOMPONENTS", admincomponents);

    header();
    indent();

    method_begin("public static", "ComponentFactory<AdminToolComponent>", "factoryForType(componentFactoryResolver: ComponentFactoryResolver, name: string)");
    output("switch ( name ) {");
    indent();

    if( modules.modules.sub != null )
      addResolves(modules.modules);

    output("default:");
    indent();
    // TODO: this is a workaround since we are not supposed to output null
    output("return componentFactoryResolver.resolveComponentFactory(RunPostProcessorComponent);");
    indent_less();

    indent_less();
    output("}");

    method_end();

    footer();
  }

  private void addResolves(Module mod)
  {
    if( mod.sub.size() == 0 )
    {
      output("case '" + mod.action + "':");
      indent();
      output("return componentFactoryResolver.resolveComponentFactory(" + mod.componentName + ");");
      indent_less();
    }
    else
    {
      for(Module submod : mod.sub.values())
      {
        addResolves(submod);
      }
    }
  }

  private void addImports(Module mod)
  {
    log.debug("addImports mod: {}", mod);
    if( mod.sub.size() == 0 )
    {
      addImport("{ " + mod.componentName + " } from '" + mod.path.resolve("admin") + mod.path.file + "'");
      admincomponents+= (first ? "" : ", ") + mod.componentName;
      first= false;
    }
    else
    {
      for(Module submod : mod.sub.values())
      {
        addImports(submod);
      }
    }
  }

  public static void main(String[] args) throws GeneratorException, ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("../server");
    IInstance inst= new Application(i);
    AdminModulesParser parser= new AdminModulesParser(inst, Paths.get("../server"));
    parser.parse();
    log.debug(inst.getWeb().getAdminModules().toString());

    AdminModuleFactory gen= new AdminModuleFactory(inst.getWeb());
    gen.setOutput(System.out);
    gen.generate();
  }
}
