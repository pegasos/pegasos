package at.pegasos.generator.generator.server.web.frontend;

import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;

@WebFrontendGenerator
public class RoutesGenerator extends TypeScriptCodeGenerator {

  private final List<Route> routes;
  private final Components components;
  private final Modules modules;

  public RoutesGenerator(Web web)
  {
    super("app-routing", "routes", null, null);
    this.routes= web.routes;
    this.components= web.components;
    this.modules= web.modules;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("{ Routes } from '@angular/router'");

    StringBuilder routesC= new StringBuilder("[\n");
    for(Route r : routes)
    {
      routesC.append("  { path: '").append(r.path).append("'");
      if( r instanceof ComponentRoute )
      {
        ComponentRoute rc= (ComponentRoute) r;
        // System.out.println(r.toString());
        addImport("{ " + rc.component + " } from '" + components.resolve(rc.component, "app-routing") + "'");

        routesC.append(", component: ").append(rc.component);
      }
      else if( r instanceof ModuleRoute )
      {
        ModuleRoute mr= (ModuleRoute) r;
        // loadChildren: () => import('../powerprof/power.profile.module').then(m => m.PowerprofModule)
        // { path: 'admin', loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule), canActivate: [MenuGuard]},
        routesC.append(", loadChildren: () => import('").append(modules.resolve(mr.module, "app-routing")).append("').then(m => m.").append(mr.module).append(")");
      }

      if( r.guards.size() > 0 )
      {
        for(String guard : r.guards)
        {
          addImport("{ " + guard + " } from '" + components.resolve(guard, "app-routing") + "'");
        }
        routesC.append(", canActivate: [").append(String.join(",", r.guards)).append("]");
      }
      routesC.append(" },\n");
    }
    routesC.append("\n  { path: '', redirectTo: '/home', pathMatch: 'full' }\n");
    routesC.append("]");

    constant("routes: Routes", routesC.toString());

    headerNoClass();

    footerNoClass();
  }
}
