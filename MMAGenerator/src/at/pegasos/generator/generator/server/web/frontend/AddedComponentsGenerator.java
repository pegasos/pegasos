package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.Components.Component;

public class AddedComponentsGenerator extends TypeScriptCodeGenerator {

  private final Components components;

  public AddedComponentsGenerator(Components components)
  {
    super("", "addedcomponents", null, null);
    this.components= components;
  }
  
  @Override
  public void generate() throws GeneratorException
  {
    StringBuilder declares= new StringBuilder("[\n");
    StringBuilder entries= new StringBuilder("[\n");
    for(Component c : components.entries())
    {
      // System.out.println(c.toString());
      if( c.declare || c.entry )
      {
        addImport("{ " + c.name + " } from '" + components.resolve(c, "") + "'");
        
        if( c.declare )
          declares.append(INDENT).append(c.name).append(",\n");
        if( c.entry )
          entries.append(INDENT).append(c.name).append(",\n");
      }
    }
    declares.append("]");
    entries.append("]");
    
    constant("DECLARATIONS", declares.toString());
    constant("ENTRYCOMPONENTS", entries.toString());
    
    headerNoClass();
    
    footerNoClass();
  }
}
