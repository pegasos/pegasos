package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;

import java.util.*;

@WebBackendGenerator
public class ChartTypesGenerator extends JavaCodeGenerator {
  private final Set<Type> names;

  public ChartTypesGenerator(Web web)
  {
    super("at.pegasos.server.frontback.data", "Type", null, null, "enum");
    this.names = web.charts.getTypes();
  }

  @Override public void generate() throws GeneratorException
  {
    header();

    indent();

    int i = 1;
    final int numberNames = names.size();
    output("Poincare,");
    output("Tachogram,");
    output("Line,");
    output("MultiLine,");
    output("Bars,");
    output("Map" + (numberNames > 0 ? "," : ""));

    for(Type name : names)
    {
      output(name.type + (i < numberNames ? "," : ""));
      i++;
    }

    footer();
  }
}
