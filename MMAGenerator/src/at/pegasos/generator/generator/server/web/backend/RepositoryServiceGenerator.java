package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;

@WebBackendGenerator
public class RepositoryServiceGenerator extends JavaCodeGenerator {

  private final Repositories repositories;

  public RepositoryServiceGenerator(Web web)
  {
    super("at.pegasos.server.frontback.services", "RepositoryService", null, null);
    this.repositories = (Repositories) web.get("backend.repositories");
  }

  @Override public void generate() throws GeneratorException
  {
    addImport("at.pegasos.server.frontback.repositories.*");
    addImport("lombok.*");
    addImport("org.springframework.beans.factory.annotation.*");
    addImport("org.springframework.stereotype.*");

    header("@Service", "@Getter");
    indent();

    for(Repositories.Repository repository : repositories.repositories)
    {
      annotatedmember("private", repository.name, repository.name.substring(0, 1).toLowerCase() + repository.name.substring(1), "Autowired");
    }

    footer();
  }
}
