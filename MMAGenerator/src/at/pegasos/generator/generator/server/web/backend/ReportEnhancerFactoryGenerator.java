package at.pegasos.generator.generator.server.web.backend;

import java.util.*;
import java.util.stream.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;

@WebBackendGenerator
public class ReportEnhancerFactoryGenerator extends JavaCodeGenerator {

  private final Collection<Report> reports;

  @SuppressWarnings("unchecked")
  public ReportEnhancerFactoryGenerator(Web web)
  {
    super("at.pegasos.server.frontback.reports", "ReportEnhancerFactory", null, null);
    reports = (Collection<Report>) web.get("reports");
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("lombok.extern.slf4j.Slf4j");
    addImport("org.rosuda.REngine.REngine");
    addImport("org.springframework.beans.factory.annotation.Autowired");
    addImport("org.springframework.core.io.ClassPathResource");
    addImport("org.springframework.stereotype.Service");

    addImport("java.io.*");
    addImport("java.util.stream.Collectors");

    header("@Service", "@Slf4j");

    indent();

    /*
    @Autowired
  REngine engine;
     */

    annotatedmember("private", "REngine", "engine", "Autowired(required=false)");

    method_begin("public", "ReportEnhancer", "getEnhancer(String name)");

    for(Report r : reports)
    {
      if( r.enhancer != null )
      {
        output("if( name.equals(\"" + r.name + "\") )");
        output("{");
        indent();

        generate((REnhancer) r.enhancer);

        indent_less();
        output("}");
      }
    }

    output("return null;");
    method_end();

    footer();
  }

  private void generate(REnhancer rEnhancer)
  {
    assign("RReportEnhancer enhancer", "new RReportEnhancer()");
    assign("enhancer.engine", "engine");

    empty();
    output("try");
    output("{");
    indent();assign("InputStream in", "new ClassPathResource(\"" + rEnhancer.code_resource + "\").getInputStream()");
    output("try( BufferedReader reader= new BufferedReader(new InputStreamReader(in)) )");
    output("{");
    indent();

    assign("String code", "reader.lines().collect(Collectors.joining(\"\\n\"))");
    output("log.debug(\"Running: {}\", code);");
    assign("enhancer.code", "new String[] {code}");

    if( rEnhancer.extractions.size() > 0 )
    {
      output("enhancer.extractions= new ExtractionDefinition[] {");
      indent();
      indent_less();
      output("};");
    }

    /*
    enhancer.extractions= new ExtractionDefinition[] {
              new UserModelExtractionDefinition("model_OmPD", "models", "OmPD", new String[] {"W1", "Pmax", "CP", "A"}),
              new UserModelExtractionDefinition("model_Om3CP", "models", "Om3CP", new String[] {"W1", "Pmax", "CP", "A"}),
              new UserModelExtractionDefinition("model_OmExp", "models", "OmExp", new String[] {"W1", "Pmax", "CP", "A"}),
              new UserModelExtractionDefinition("model_2hyp", "models", "2-hyp", new String[] {"W1", "CP"})
          };
     */

    indent_less();
    output("}");

    output("return enhancer;");

    indent_less();
    output("}");
    output("catch( IOException e )");
    output("{");
    indent();
    output("log.error(e.getMessage());");
    output("e.printStackTrace();");
    indent_less();
    output("}");

    /*
    }
        return enhancer;
      }
      catch( IOException e )
     */
  }
}

