package at.pegasos.generator.generator.server.web.frontend;

import java.util.Collection;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.Charts;
import at.pegasos.generator.parser.webparser.token.Charts.Chart;
import lombok.extern.slf4j.*;;

@Slf4j
public class ChartConfigurerGenerator extends TypeScriptCodeGenerator {
  private final Charts charts;
  
  public ChartConfigurerGenerator(Charts charts)
  {
    super("charts", "ChartConfigurer", null, null);
    this.charts= charts;
  }
  
  @Override
  public void generate() throws GeneratorException
  {
    log.debug("Generating {}", charts);
    log.debug("Output: {}", charts.getCharts());
    addImport("{ ChartConfig } from './../shared/chartconfig'");
    addImport("{ ChartInfo } from './../shared/sample'");
    for(Chart c : charts.getCharts())
    {
      for(String imp : c.getConfigImports())
      {
        addImport(imp);
      }
    }
        
    header();
    indent();
    
    method_begin("public static", "ChartConfig", "getChartConfig(info: ChartInfo)"); //public static getChartConfig(info: ChartInfo): ChartConfig
    output("switch( info.name )");
    output("{");
    indent();
    
    int i= 0;
    
    for(Chart c : charts.getCharts())
    {
      log.debug("Chart {}", c);
      Collection<String> lines= c.getConfigLines("c" + i);
      if( lines != null )
      {
        output("case '" + c.name + "':");
        indent();
        for(String line : lines)
        {
          output(line);
        }
        output("return c" + i + ";");
        indent_less();
        i++;
      }
      else
        log.debug("No output for it");
    }
    
    indent_less();
    output("}");
    
    empty();
    output("return new ChartConfig(info);");
    
    method_end();
    
    footer();
  }
}
