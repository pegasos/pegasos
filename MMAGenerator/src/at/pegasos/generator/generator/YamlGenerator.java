package at.pegasos.generator.generator;

import org.yaml.snakeyaml.*;
import org.yaml.snakeyaml.introspector.*;
import org.yaml.snakeyaml.nodes.*;
import org.yaml.snakeyaml.representer.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public abstract class YamlGenerator extends CodeGenerator {

  private final Yaml yaml;
  private final Map<String, Object> data;
  protected final Representer representer;

  protected YamlGenerator()
  {
    representer = new Representer() {
      @Override
      protected NodeTuple representJavaBeanProperty(Object javaBean, Property property, Object propertyValue, Tag customTag) {
        // System.out.println(javaBean + " " + property + " " + propertyValue + " " + customTag);
        // if value of property is null, ignore it.
        if (propertyValue == null) {
          return null;
        }
        else {
          return super.representJavaBeanProperty(javaBean, property, propertyValue, customTag);
        }
      }
    };

    this.yaml = new Yaml(representer);
    this.data = new LinkedHashMap<>();
  }

  @Override
  public void setOutputToPrintStream(Path basedir) throws IOException
  {
    Path p = basedir;

    this.out = new PrintStream(Files.newOutputStream(p));
  }

  protected void put(String name, Object object)
  {
    this.data.put(name, object);
  }

  protected void write(Object object)
  {
    final DumperOptions options = new DumperOptions();
    options.setIndent(2);
    options.setPrettyFlow(true);
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);

    yaml.dump(object, new PrintWriter(out));
  }

  protected void write()
  {
    final DumperOptions options = new DumperOptions();
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
    options.setPrettyFlow(false);

    yaml.dump(data, new PrintWriter(out));
  }
}
