package at.pegasos.generator.generator;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public abstract class JavaCodeGenerator extends CodeGenerator {
  public static final String INDENT = "  ";
  protected int indent_size;
  
  protected final String package_name;
  protected final String classname;
  protected Set<String> imports;
  protected String extend_stmt;
  protected String implement_stmt;

  protected final String type;

  public JavaCodeGenerator(String pkg_name, String classname, String extend, String implement)
  {
    this.imports= new HashSet<String>();
    this.package_name= pkg_name;
    this.classname= classname;
    this.extend_stmt= extend;
    this.implement_stmt= implement;
    this.type= "class";
  }

  public JavaCodeGenerator(String pkg_name, String classname, String extend, String implement, String type)
  {
    this.imports= new HashSet<String>();
    this.package_name= pkg_name;
    this.classname= classname;
    this.extend_stmt= extend;
    this.implement_stmt= implement;
    this.type= type;
  }
  
  public void setExtends(String extend)
  {
    this.extend_stmt= extend;
  }
  
  protected void addImport(String imp)
  {
    this.imports.add(imp);
  }
  
  @Override
  public void setOutputToPrintStream(Path basedir) throws IOException
  {
    String[] pkgs= package_name.split("[.]");
    Path p= basedir;
    for(String pkg : pkgs)
    {
      p= p.resolve(pkg);
    }
    
    if( !Files.exists(p) )
      Files.createDirectories(p);
    
    p= p.resolve(classname + ".java");
    
    this.out= new PrintStream(Files.newOutputStream(p));
  }
  
  protected void output(int indent, String line)
  {
    for(int i = 0; i < indent; i++)
      out.print(INDENT);
    out.println(line);
  }
  
  protected void empty(int indent)
  {
    output(indent, "");
  }
  
  protected void output(String line)
  {
    output(indent_size, line);
  }
  
  protected void empty()
  {
    empty(indent_size);
  }
  
  protected void indent()
  {
    indent_size++;
  }
  
  protected void indent_less()
  {
    indent_size--;
  }
  
  public int getIndent()
  {
    return indent_size;
  }
  
  protected void header(String... class_annotations)
  {
    output("package " + package_name + ";");
    empty();
    empty();
    
    for(String imp : imports)
      output("import " + imp + ";");
    if( imports.size() > 0 )
      empty();
    
    if( class_annotations != null )
    {
      for(String anno : class_annotations)
        output(anno);
    }
    // String line= "public class " + classname; 
    String line= "public " + type + " " + classname; 
    if( extend_stmt != null )
      line+= " extends " + extend_stmt;
    if( implement_stmt != null )
      line+= " implements " + implement_stmt;
    output(line + " {");
  }

  /**
   * Generate the footer for a file
   */
  protected void footer()
  {
    if( indent_size > 0 )
      indent_size= 0;
    
    output("}");
    output("// End of generated class");
    empty();
  }
  
  /**
   * Generate the footer for a file without a class
   */
  protected void footerNoClass()
  {
    if( indent_size > 0 )
      indent_size= 0;
    
    output("// End of generated class");
    empty();
  }
  
  /**
   * Output the header of a constructor for this class
   * @param parameters parameters for this constructor
   */
  protected void constructor_begin(String[] parameters)
  {
    String params;
    if( parameters == null )
      params= "";
    else
      params= String.join(", ", parameters);
    
    output("public " + classname + "(" + params + ")");
    output("{");
    indent();
  }
  
  protected void method_begin(String access_mod, String returntype, String methodname_params)
  {
    method_begin(access_mod, returntype, methodname_params, false);
  }
  
  protected void method_begin(String access_mod, String returntype, String methodname_params, boolean override)
  {
    if( override ) output("@Override");
    output(access_mod + " " + returntype + " " + methodname_params);
    output("{");
    indent();
  }
  
  /**
   * Closes the opened method. (This also includes decreasing the indent)
   */
  protected void method_end()
  {
    indent_less();
    output("}");
    empty();
  }
  
  /**
   * Output a member declaration
   * @param access
   * @param type
   * @param name
   */
  protected void member(String access, String type, String name)
  {
    output(access + " " + type + " " + name + ";");
  }

  /**
   * Output a member declaration
   * @param access
   * @param type
   * @param name
   * @param annotation
   */
  protected void annotatedmember(String access, String type, String name, String annotation)
  {
    output("@" + annotation + " " + access + " " + type + " " + name + ";");
  }
  
  /**
   * Output a member declaration
   * @param access
   * @param type
   * @param name
   */
  protected void member(String access, String type, String name, String value)
  {
    output(access + " " + type + " " + name + "= " + value + ";");
  }
  
  /**
   * Output an assignment
   * @param variable
   * @param value
   */
  protected void assign(String variable, String value)
  {
    output(variable + "= " + value + ";");
  }
  
  /**
   * Output a call of a function/method
   * @param variable
   * @param value
   */
  protected void call(String name, String parameters)
  {
    output(name + "(" + parameters + ");");
  }
  
  /**
   * Output a call of a function/method
   * @param variable
   */
  protected void call(String name)
  {
    call(name, "");
  }


  protected void variable(String type, String name)
  {
    output(type + " " + name + ";");
  }

  protected void beginIf(String condition, boolean braced)
  {
    output("if( " + condition + " )");
    if( braced )
      output("{");
    indent();
  }

  protected void endIf(boolean braced)
  {
    indent_less();
    if( braced )
      output("}");
  }
}
