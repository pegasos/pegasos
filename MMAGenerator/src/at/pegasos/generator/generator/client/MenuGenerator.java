package at.pegasos.generator.generator.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import at.pegasos.generator.Defaults;
import at.pegasos.generator.generator.JavaCodeGenerator;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.appparser.token.menu.*;
import at.pegasos.generator.parser.token.EntryString;

public class MenuGenerator extends JavaCodeGenerator {
  private final Application app;
  private int entryidx;
  
  /**
   * 
   * @param app
   *          reference to the Application
   * @param name
   *          Name of the class
   */
  public MenuGenerator(Application app, String name)
  {
    super("at.pegasos.client.ui", name, "PegasosClientMenu", null);
    
    this.app= app;
    
    entryidx= 0;
    
    addImport("at.univie.mma.R");
    addImport("at.pegasos.client.values.ValueStore");
    addImport("at.univie.mma.ui.ActivityStarterFragment");
    addImport("at.univie.mma.serverinterface.Activities");
    addImport("at.pegasos.client.ui.activitystarter.*");
  }
  
  private void addImportsForMenu(MenuItem s)
  {
    if( s instanceof MenuItemMenu )
      addImport("at.univie.mma.ui.ActivityStarterFragment.OpenMenu");
    else if( s instanceof MenuItemActivity )
    {
      MenuItemActivity d= (MenuItemActivity) s;
      if( d.parameters != null )
        addImport("at.univie.mma.activitystarter.StartActivityParam");
      else if( d.sport != null && d.distance == null )
        addImport("at.univie.mma.ui.ActivityStarterFragment.StartActivitySport");
      else if( d.sport == null )
        addImport("at.univie.mma.ui.ActivityStarterFragment.SelectSportForActivity");
    }
    else if( s instanceof MenuItemIntent )
    {
      addImport("at.univie.mma.ui.ActivityStarterFragment.OpenIntent");
    }
  }
  
  /**
   * Check which type of menus are present in order to only import the necessary packages
   */
  private void analyseMenus()
  {
    for(MenuItem s : app.getMain().items)
      addImportsForMenu(s);
    
    for(Entry<String, Menu> e : app.getMenus().entrySet())
      for(MenuItem s : e.getValue().items)
        addImportsForMenu(s);
  }
  
  public void generate()
  {
    analyseMenus();
    
    header();
    
    indent();
    variables();
    
    empty();
    
    constructor();
    
    setupMenus();
    
    footer();
  }

  private void variables()
  {
    output("private MenuManager manager;");
  }

  private void constructor()
  {
    constructor_begin(new String[] {"MenuManager manager"});
    assign("this.manager", "manager");
    method_end();
  }
  
  private void setupMenus()
  {
    method_begin("public", "void", "setupMenus()", true);
    
    mainMenu();
    
    int i= 0;
    for(Entry<String, Menu> e : app.getMenus().entrySet())
    {
      menu(e.getValue(), "menu" + i);
      i++;
    }
    
    method_end();
  }

  private void mainMenu()
  {
    Menu main= app.getMain();
    
    output("main_menu= new PegasosClientSubMenu();");
    output("main_menu.menu= new ActivityStarterFragment[" + main.items.size() + "];");
    empty();
    
    int i= 0;
    for(MenuItem s : main.items)
    {
      outputMenuEntry(s, "main_menu.menu[" + (i++) + "]");
    }
  }
  
  private void menu(Menu m, String name)
  {
    /*
    MMASubMenu perpot_calib= new MMASubMenu();
    perpot_calib.menu= new ActivityStarterFragment[3];
    perpot_calib.name= "PerPot Calib Menu";
     */
    output("PegasosClientSubMenu " + name + "= new PegasosClientSubMenu();");
    output(name + ".menu= new ActivityStarterFragment[" + m.items.size() + "];");
    output(name + ".name = \"" + m.name + "\";");
    empty();
    
    int i= 0;
    for(MenuItem s : m.items)
    {
      outputMenuEntry(s, name + ".menu[" + (i++) + "]");
    }
    empty();
    output("addMenu(" + name + ");");
  }

  private void outputMenuEntry(MenuItem s, String menu_var)
  {
    String entry_var= "entry" + entryidx;
    String var= "ActivityStarterFragment " + entry_var; 
    var+= "= new ActivityStarterFragment();";
    // var+= s.icon + ", ";
    // var+= s.text + ");";
    output(var);
    String icon= Defaults.MENU_ICON;
    if( s.icon != null && s.icon.raw() != null )
      icon= s.icon.toString();
    output(entry_var + ".setPicture(" + icon + ");");
    output(entry_var + ".setText(" + s.text + ");");

    if( s instanceof MenuItemMenu )
    {
      outputMenuEntry(entry_var, (MenuItemMenu) s);
    }
    else if( s instanceof MenuItemActivity )
    {
      outputMenuEntry(entry_var, (MenuItemActivity) s);
    }
    else if( s instanceof MenuItemIntent )
    {
      outputMenuEntry(entry_var, (MenuItemIntent) s);
    }
    
    output(menu_var + "= " + entry_var + ";");
    empty();
    
    entryidx++;
  }
  
  private void outputMenuEntry(String entry_var, MenuItemMenu m)
  {
    // perpot_calibs.setCallback(new OpenMenu(this.manager, 0));
    String line;
    
    line= entry_var;
    line+= ".setCallback(new OpenMenu(this.manager, ";
    line+= app.getMenuIdx(m.menu);
    line+= "));";
    output(line);
  }
  
  private void outputMenuEntry(String entry_var, MenuItemIntent i)
  {
    StringBuilder line;
    
    line = new StringBuilder(entry_var);
    line.append(".setCallback(new OpenIntent(this.manager.getActivity(), ");
    line.append("\"").append(i.intent).append("\"");
    line.append(")");
    for(Entry<String,String> e : i.params.entrySet())
    {
      line.append("\n    .addParam(\"").append(e.getKey()).append("\", \"").append(e.getValue()).append("\")");
    }
    line.append(");");
    output(line.toString());
  }
  
  private void outputMenuEntry(String entry_var, MenuItemActivity d)
  {
    if( d.qvalues.size() > 0 )
    {
      String in= "     ";
      output(entry_var + ".setCallback(new " + d.qvalues.get(0).get("_Callback") + "()");
      output(in + ".setContext(this.manager.getActivity())");
      output(in + ".setController(" + d.activity + ")");
      if( d.sport != null )
      {
        output(in + ".setSport(\"" + d.sport + "\")");
      }
      if( d.parameters != null )
      {
        output(in + ".setArgument(\"" + d.parameters + "\")");
      }
      if( !d.callback.equals("") )
      {
        String line;
        line= in + ".setCommand(";
        line+= "new Runnable() {";
        
        output(line);
        indent();
        output(in + "@Override");
        output(in + "public void run() {");
        indent();
        for(String cline : d.callback.split("\n"))
        {
          output(in + cline);
        }
        indent_less();
        output(in + "}");
        indent_less();
        output(in + "}));");
      }      
      
      Iterator<HashMap<String, EntryString>> queries= d.qvalues.iterator();
      HashMap<String, EntryString> query= queries.next();
      // Remove the type of the Query (ie the _Callback)
      query.remove("_Callback");
      query.remove("_Type");
      indent();
      for( Entry<String, EntryString> param : query.entrySet() )
      {
        output(".set" + param.getKey() + "(" + param.getValue() + ")");
      }
      while( queries.hasNext() )
      {
        query= queries.next();
        output(".addNext(new " + query.get("_Callback") + "()");
        indent();
        query.remove("_Callback");
        query.remove("_Type");
        for( Entry<String, EntryString> param : query.entrySet() )
        {
          output(".set" + param.getKey() + "(" + param.getValue() + ")");
        }
        indent_less();
        output(")");
      }
      indent_less();
      output(");");
      return;
    }
    
    if( d.sport != null )
    {
      if( d.distance != null )
      {
        // TODO: if distance_fixed = false but distance contains a value use it as preset value 
        if( d.fixed_distance )
        {
          String line;
          line= entry_var + ".setCallback(new StartActivityDistance(this.manager.getActivity(), ";
          line+= d.activity + ", ";
          line+= "\"" + d.sport + "\", ";
          if( !d.callback.equals("") )
          {
            line+= "new Runnable() {";
            output(line);
            indent();
            output("@Override");
            output("public void run() {");
            indent();
            output(d.callback);
            indent_less();
            output("}");
            indent_less();
            line= "},";
          }
          line+= d.distance;
          line+= "));";
          
          output(line);
        }
        else if( !d.callback.equals("") )
        {
          String line;
          line= entry_var + ".setCallback(new StartActivityDistance(this.manager.getActivity(), ";
          line+= d.activity + ", ";
          line+= "\"" + d.sport + "\", ";
          line+= "new Runnable() {";
          // Activities.FOODPOD_CALIBRATION_C, 0, new Runnable() {
          
          output(line);
          indent();
          output("@Override");
          output("public void run() {");
          indent();
          output(d.callback);
          indent_less();
          output("}");
          indent_less();
          output("}));");
        }
        else
        {
          String line;
          line= entry_var + ".setCallback(new StartActivityDistance(this.manager.getActivity(), ";
          line+= d.activity + ", ";
          line+= "\"" + d.sport + "\"";
          line+= "));";
          
          output(line);
        }
      }
      else if( d.parameters != null )
      {
        String line;
        line= entry_var + ".setCallback(new StartActivityParam(this.manager.getActivity(), ";
        line+= d.activity + ", ";
        line+= "\"" + d.sport + "\", ";
        if( !d.callback.equals("") )
        {
          line+= "new Runnable() {";
          output(line);
          indent();
          output("@Override");
          output("public void run() {");
          indent();
          output(d.callback);
          indent_less();
          output("}");
          indent_less();
          line= "}, ";
        }
        line+= d.parameters + "));";
        output(line);
      }
      else
      { // distance/param not set --> start an activity with given Sport and no param
        String line;
        
        line= entry_var;
        if( d.callback_class )
          line+= ".setCallback(new " + d.callback + "(this.manager.getActivity(), ";
        else
          line+= ".setCallback(new StartActivitySport(this.manager.getActivity(), ";
        line+= d.activity + ", ";
        line+= "\"" + d.sport + "\"";
        line+= "));";
        output(line);
      }
    }
    else
    { //sport not set
      String line;
      
      line= entry_var;
      line+= ".setCallback(new SelectSportForActivity(this.manager.getActivity(), ";
      line+= d.activity;
      line+= "));";
      output(line);
    }
    
    if( d.auto_start )
      output(entry_var + ".setAutoStart(true);");
  }
}
