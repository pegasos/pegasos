package at.pegasos.generator.generator.client;

import java.util.List;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Sport;

public class SportsFactory extends JavaCodeGenerator{

  private List<Sport> sports;

  public SportsFactory(List<Sport> list)
  {
    super("at.pegasos.client.sports", "SportFactory", null, null);
    this.sports= list;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    empty();
    indent();
    output("public static Sport getSportFromString(String name)");
    output("{");
    indent();
    
    for(Sport sport : sports)
    {
      output("if(name.equals(\"" + sport.name + "\"))");
      indent();
      output("return new " + sport.java_name + "();");
      indent_less();
    }
    
    indent_less();
    if( sports.size() > 0 )
    {
      output("else");
      indent();
    }
    output("return null;");
    indent_less();
    
    // close function
    output("}");
    indent_less();
    
    // close class
    output("}");
    indent_less();
  }
  
}

/* Demo code:
package at.univie.mma.sports;

public class SportFactory {
  public static Sport getSportFromString(String name)
  {
    if(name == null )
      return null;
    
    if(name.equals("Running"))
      return new Running();
    else if(name.equals("Cycling"))
      return new Cycling();
    else
      return null;
  }
}

*/
