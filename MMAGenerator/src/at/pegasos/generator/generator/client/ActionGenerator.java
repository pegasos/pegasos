package at.pegasos.generator.generator.client;

import java.util.List;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.token.*;

public class ActionGenerator extends JavaCodeGenerator {
  private final List<OwnedString> actions;

  public ActionGenerator(String pkg, String name, List<OwnedString> actions)
  {
    super(pkg, name, null, "Runnable");
    this.actions= actions;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();

    indent();

    method_begin("public", "void", "run()", true);

    for(OwnedString action : actions )
      output("new " + action.string + "().run();");

    method_end();

    footer();
  }
}
