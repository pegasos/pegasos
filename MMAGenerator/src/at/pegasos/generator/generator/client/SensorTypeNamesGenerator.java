package at.pegasos.generator.generator.client;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Sensors;

public class SensorTypeNamesGenerator extends JavaCodeGenerator {
  private final Sensors sensors;

  public SensorTypeNamesGenerator(Sensors sensors)
  {
    super("at.univie.mma.sensors", "SensorTypeNames", null, null);
    this.sensors= sensors;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();

    indent();
    for(String sensor : sensors.sensors.keySet())
      output("public static final String " + sensor + " = \"" + sensor + "\";");

    // These type names are required as they are 'first-class' and thus used
    //  in SensorControllerService.java --> they have to be added anyway
    if( !sensors.sensors.containsKey("HeartRate") )
      output("public static final String HeartRate = \"HearRate\";");
    if( !sensors.sensors.containsKey("FootPod") )
      output("public static final String FootPod = \"FootPod\";");
    if( !sensors.sensors.containsKey("GPS") )
      output("public static final String GPS = \"GPS\";");
    if( !sensors.sensors.containsKey("Accelerometer") )
      // Although not first-class need as Accelerometer is part of the built-in classes
      // TODO: check whether necessary as the class is removed if not required
      output("public static final String Accelerometer = \"Accelerometer\";");

    StringBuilder line= new StringBuilder("public static final String[] ALL = {");
    boolean comma= false;
    for(String sensor : sensors.sensors.keySet())
    {
      if( comma )
        line.append(", ");
      line.append(sensor);
      comma= true;
    }

    line.append("};");
    output(line.toString());

    indent_less();
    output("}");
  }
}
