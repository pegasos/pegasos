package at.pegasos.generator.generator.client;

import java.util.Map.Entry;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Screen;
import at.pegasos.generator.parser.appparser.token.screen.*;

public class ScreenGenerator extends JavaCodeGenerator
{
  private final Screen screen;
  private String constr;

  private final boolean legacy;

  int screen_nr;

  public ScreenGenerator(JavaCodeGenerator generator, Screen screen, int screen_nr, boolean legacy)
  {
    super("", "", "", "");
    setOutput(generator.getOutputStream());
    for(int i= 0;i < generator.getIndent();i++)
      indent();
    this.screen= screen;
    this.screen_nr= screen_nr;
    this.legacy= legacy;
  }

  @Override
  public void generate() throws GeneratorException
  {
    String[] fragments = new String[screen.fragments.size()];
    int j= 0;
    for(Fragment frag : screen.fragments)
    {
      if( frag == null )
      {
        // output("MeasurementFragment fr" + screen_nr + "_" + j + "= new " + frag.clasz + "();");
        assign("MeasurementFragment fr" + screen_nr + "_" + j, "null");
      }
      else
      switch( frag.type )
      {
        case Fragment.TYPE_GENERATE:
          generate((ValueFragment) frag, j);
          break;

        case Fragment.TYPE_JAVA:
          if( !frag.clasz.equals("") )
          {
            if( legacy )
              output("MeassurementFragment fr" + screen_nr + "_" + j + "= new " + frag.clasz + "();");
            else
              output("MeasurementFragment fr" + screen_nr + "_" + j + "= new " + frag.clasz + "();");
            generate(frag, j, frag.clasz);
          }
          else
          {
            if( legacy )
              output("MeassurementFragment fr" + screen_nr + "_" + j + "= " + frag.method + ";");
            else
              output("MeasurementFragment fr" + screen_nr + "_" + j + "= " + frag.method + ";");
          }
          break;
      }

      fragments[j]= "fr" + screen_nr + "_" + j;

      j++;
    }
    
    String p;
    if( fragments.length > 0 )
      p= "," + String.join(",", fragments);
    else
      p= "";
    constr= "new " + screen.classFromType() + "(activity" + p + ")";
  }

  private void generate(Fragment frag, int j, String obj)
  {
    if( frag.args.size() > 0 )
    {
      output("args= new Bundle();");
      for(Entry<String, String> kv : frag.args.entrySet())
      {
        String key= "ARGUMENT_" + kv.getKey();
        key= key.replaceAll("[.]", "_");
        key= key.toUpperCase();
        output("args.putString(" + obj + "." + key + ",\"" + kv.getValue() + "\");");
      }
      output("fr" + screen_nr + "_" + j + ".setArguments(args);");
    }
  }

  private void generate(ValueFragment frag, int j)
  {
    output("args= new Bundle();");
    output("args.putString(ValueFragment.ARGUMENT_NAME, \"" + frag.value + "\");");
    if( frag.left_img != null )
      output("args.putInt(ValueFragment.ARGUMENT_IMG_L, " + frag.left_img + ");");
    else
      output("args.putString(ValueFragment.ARGUMENT_TXT_L, \"" + frag.left_txt + "\");");
    if( frag.right_img != null )
      output("args.putInt(ValueFragment.ARGUMENT_IMG_R, " + frag.right_img + ");");
    else
      output("args.putString(ValueFragment.ARGUMENT_TXT_R, \"" + frag.right_txt + "\");");
    if( frag.value_type.toLowerCase().equals("integer") )
      output("ValueFragment fr" + screen_nr + "_" + j + "= new IntValueFragment();");
    else if( frag.value_type.toLowerCase().equals("double") )
      output("ValueFragment fr" + screen_nr + "_" + j + "= new DoubleValueFragment();");
    else if( frag.value_type.toLowerCase().equals("string") )
      output("ValueFragment fr" + screen_nr + "_" + j + "= new TextValueFragment();");
    output("fr" + screen_nr + "_" + j + ".setArguments(args);");
  }

  public String getConstr()
  {
    return constr;
  }
}
