package at.pegasos.generator.generator.client;

import java.util.List;

import at.pegasos.generator.Defaults;
import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Sport;

public class SportsGenerator extends JavaCodeGenerator{

  private final List<Sport> sports;

  public SportsGenerator(List<Sport> sports)
  {
    super("at.pegasos.client.sports", "Sports", null, null);
    this.sports= sports;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.univie.mma.R");
    addImport("android.content.Context");
    
    header();
    empty();
    indent();
    StringBuilder line1;
    StringBuilder line2;
    StringBuilder line3;
    
    line1 = new StringBuilder("public static final int[] sportIcons= {");
    line2 = new StringBuilder("public static final String[] sportNames= {");
    line3 = new StringBuilder("public static final String[] sportIcons_fn= {");
    
    boolean comma= false;
    
    for(Sport sport : sports)
    {
      if( comma )
      {
        line1.append(", ");
        line2.append(", ");
        line3.append(", ");
      }

      if( sport.icon != null )
      {
        if( sport.icon.isId() )
        {
          line1.append(sport.icon);
          line3.append("\"\"");
        }
        else
        {
          line1.append("0");
          line3.append(sport.icon);
        }
      }
      else
      {
        line1.append(Defaults.SPORT_ICON);
        line3.append("\"\"");
      }
      line2.append("\"").append(sport.name).append("\"");
      comma= true;
    }
    
    line1.append("};");
    line2.append("};");
    line3.append("};");
    
    output(line1.toString());
    output(line2.toString());
    output(line3.toString());
    
    fillInMethod();
    
    indent_less();
    
    // close class
    output("}");
    indent_less();
  }

  private void fillInMethod() {
    empty();
    output("public static void fillinIds(Context ctx)");
    output("{");
    indent();
    output("for(int i= 0; i < sportIcons.length; i++)");
    output("{");
    indent();
    
    output("if( sportIcons[i] == 0 )");
    output("{");
    indent();
    output("sportIcons[i]= ctx.getResources().getIdentifier(\" + sportIcons_fn[i] + \", \"drawable\", ctx.getPackageName());");
    // close if
    indent_less();
    output("}");
    
    // close loop
    indent_less();
    output("}");
    
    // close function
    indent_less();
    output("}");
  }
  
}
