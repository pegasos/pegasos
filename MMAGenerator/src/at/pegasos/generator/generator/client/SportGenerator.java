package at.pegasos.generator.generator.client;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.appparser.token.screen.*;

public class SportGenerator extends JavaCodeGenerator {
  Sport sport;

  private final boolean legacy;
  private final boolean legacy2;

  public SportGenerator(Sport sport, boolean legacy, boolean legacy2)
  {
    super("at.pegasos.client.sports", sport.java_name, "Sport", null);
    this.sport= sport;
    this.legacy= legacy;
    this.legacy2= legacy2;
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.univie.mma.sensors.SensorTypeNames");
    addImport("at.univie.mma.R");
    addImport("at.pegasos.client.ui.screen.Screen");
    addImport(legacy2 ? "at.univie.mma.ui.TrainingActivity" : "at.pegasos.client.ui.TrainingActivity");

    analyseScreens();

    header();

    indent();

    sensors();

    screens();

    indent_less();
    output("}");
  }

  private void sensors()
  {
    StringBuilder line;

    line = new StringBuilder("private static final String[] sensors_req={");
    if( sport.required_names.size() > 0 )
    {
      int i;
      for(i= 0; i < sport.required_names.size() - 1; i++)
        line.append("SensorTypeNames.").append(sport.required_names.get(i)).append(",");
      line.append("SensorTypeNames.").append(sport.required_names.get(i));
    }
    line.append("};");
    output(line.toString());

    line = new StringBuilder("private static final String[] sensors_opt={");
    if( sport.optional_names.size() > 0 )
    {
      int i;
      for(i= 0; i < sport.optional_names.size() - 1; i++)
        line.append("SensorTypeNames.").append(sport.optional_names.get(i)).append(",");
      line.append("SensorTypeNames.").append(sport.optional_names.get(i));
    }
    line.append("};");
    output(line.toString());

    output("@Override");
    output("public String[] getRequiredSensors()");
    output("{");
    indent();
    output("return sensors_req;");
    indent_less();
    output("}");

    output("@Override");
    output("public String[] getOptionalSensors()");
    output("{");
    indent();
    output("return sensors_opt;");
    indent_less();
    output("}");
  }

  private void screens() throws GeneratorException
  {
    if( sport.screens.size() > 0 )
    {
      output("private Screen[] extra_screens;");
      output("");

      output("@Override");
      output("public Screen[] getAdditionalScreens()");
      output("{");
      indent();
      output(" return extra_screens;");
      indent_less();
      output("}");
      output("");
 
      output("@Override");
      output("public void setupAdditionalScreens(TrainingActivity activity)");
      output("{");
      indent();

      output("Bundle args;");
      output("extra_screens= new Screen[" + sport.screens.size() + "];");
      int i= 0;
      for(Screen screen : sport.screens)
      {
        output("Screen screen" + i + ";");

        ScreenGenerator g= new ScreenGenerator(this, screen, i, legacy);
        g.generate();
        assign("screen" + i, g.getConstr());

        output("extra_screens[" + i + "]= screen" + i + ";");
        output("");
        i++;
      }

      indent_less();
      output("}");
    }
    else
    {
      output("@Override");
      output("public Screen[] getAdditionalScreens()");
      output("{");
      indent();
      output("return no_screens;");
      indent_less();
      output("}");

      output("@Override");
      output("public void setupAdditionalScreens(TrainingActivity activity)");
      output("{");
      indent();
      output("// no_screens --> do nothing");
      indent_less();
      output("}");
    }
  }

  private void analyseScreens()
  {
    if( sport.screens.size() > 0 )
    {
      boolean intpresent= false;
      boolean doublepresent= false; 
      for(Screen s : sport.screens)
      {
        for(Fragment f : s.fragments)
        {
          if( f != null && f.type == Fragment.TYPE_GENERATE )
          {
            ValueFragment vf= (ValueFragment) f;
            if( vf.value_type.equalsIgnoreCase("integer") )
              intpresent= true;
            else
              doublepresent= true;
          }
        }
      }
      addImport("android.os.Bundle");
      if(doublepresent || intpresent )
        addImport("at.univie.mma.ui.widget.ValueFragment");
      if(doublepresent)
        addImport("at.univie.mma.ui.widget.DoubleValueFragment");
      if(intpresent)
        addImport("at.univie.mma.ui.widget.IntValueFragment");
      addImport("at.univie.mma.ui.MeasurementFragment");
    }
  }
}

/*
package at.univie.mma.sports;

import at.univie.mma.sensors.SensorTypeNames;
import at.univie.mma.ui.Screen;
import at.univie.mma.ui.TrainingActivity;

public class Running extends Sport {
  private static final String[] sensors_req={SensorTypeNames.FootPod, SensorTypeNames.HeartRate}; 
  private static final String[] sensors_opt={SensorTypeNames.GPS}; 
  
  @Override
  public String[] getRequiredSensors()
  {
    return sensors_req;
  }

  @Override
  public String[] getOptionalSensors()
  {
    return sensors_opt;
  }
  
  @Override
  public Screen[] getAdditionalScreens()
  {
    return no_screens;
  }

  @Override
  public void setupAdditionalScreens(TrainingActivity activity)
  {
    // no_screens --> do nothing
  }
}
*/
