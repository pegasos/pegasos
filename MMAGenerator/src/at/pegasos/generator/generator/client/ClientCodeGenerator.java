package at.pegasos.generator.generator.client;

import at.pegasos.generator.generator.CodeGenerator;
import at.pegasos.generator.generator.GeneratorException;
import at.pegasos.generator.generator.PegasosModuleGenerator;
import at.pegasos.generator.parser.appparser.token.Application;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.List;

@Slf4j
public class ClientCodeGenerator extends PegasosModuleGenerator {

  private final Application inst;
  private final Path outdir;
  private final boolean legacy;

  public ClientCodeGenerator(Application inst, Path outdir, boolean legacy)
  {
    this.inst= inst;
    this.outdir= outdir;
    this.legacy= legacy;
  }

  public void generate() throws GeneratorException
  {
    log.debug("ClientCodeGenerator here");
    List<Class<? extends CodeGenerator>> classesGen=
        fetchGenerators(ClientGenerator.class, "at.pegasos.generator.generator.client", "at.pegasos.generator.appgenerator");
    CodeGenerator[] generators= new CodeGenerator[classesGen.size()];
    log.debug("Found {}", classesGen.toString());

    int i= 0;
    for(Class<? extends CodeGenerator> clasz : classesGen)
    {
      try
      {
        CodeGenerator gen= (CodeGenerator) clasz.getConstructor(Application.class, Boolean.class).newInstance(inst, legacy);
        generators[i++]= gen;
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e )
      {
        throw new GeneratorException(e);
      }
    }

    for(CodeGenerator generator : generators)
    {
      try
      {
        generator.setOutputToPrintStream(outdir);
        generator.generate();
        generator.closeOutput();
      }
      catch(IOException e)
      {
        throw new GeneratorException(e);
      }
    }
  }
}
