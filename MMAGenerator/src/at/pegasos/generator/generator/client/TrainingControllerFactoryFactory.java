package at.pegasos.generator.generator.client;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.appparser.token.Activity;

@ClientGenerator
public class TrainingControllerFactoryFactory extends JavaCodeGenerator{

  private final Activity[] activities;

  public TrainingControllerFactoryFactory(Application inst, Boolean legacy)
  {
    super(legacy ? "at.univie.mma.controller" : "at.pegasos.client.controller", "TrainingControllerFactory", null, null);

    addImport("at.univie.mma.serverinterface.Activities");

    this.activities= inst.activities;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    empty();
    indent();
    output("public ITrainingController getControllerForActivity(int activity_c)");
    output("{");
    indent();

    if( activities.length == 0)
    {
      output("return new VoidController();");
    }
    else
    {
      boolean first= true;
      for(Activity act : activities)
      {
        if( first )
        {
          first= false;
          output("if( activity_c == " + act.id + " )");
        }
        else
          output("else if( activity_c == " + act.id + " )");
        indent();
        output("return new " + act.clasz + "();");
        indent_less();
      }

      output("return new VoidController();");
    }

    // close function
    indent_less();
    output("}");
    indent_less();

    // close class
    output("}");
    indent_less();
  }
}

/* Demo code:
package at.univie.mma.sports;

public class SportFactory {
  public static Sport getSportFromString(String name)
  {
    if(name == null )
      return null;
    
    if(name.equals("Running"))
      return new Running();
    else if(name.equals("Cycling"))
      return new Cycling();
    else
      return null;
  }
}

*/
