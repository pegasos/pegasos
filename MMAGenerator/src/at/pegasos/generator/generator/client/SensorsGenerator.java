package at.pegasos.generator.generator.client;

import java.util.List;
import java.util.Map.Entry;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.*;

@ClientGenerator
public class SensorsGenerator extends JavaCodeGenerator {
  private final Sensors sensors;

  public SensorsGenerator(Application instance, Boolean legacy)
  {
    super(legacy ? "at.univie.mma.sensors" : "at.pegasos.client.sensors", "Sensors", null, null);
    this.sensors= instance.getSensors();
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("java.util.HashSet");
    addImport("java.util.Set");
    addImport("at.univie.mma.R");

    header();
    indent();

    output("public static final Set<Class> ant_sensors;");
    output("static{");
    indent();
    output("ant_sensors= new HashSet<Class>();");
    for(String clazz : sensors.ant_sensors)
      output("ant_sensors.add(" + clazz + ".class);");
    indent_less();
    output("}");

    output("public static final Set<Class> ble_sensors;");
    output("static{");
    indent();
    output("ble_sensors= new HashSet<Class>();");
    for(String clazz : sensors.ble_sensors)
      output("ble_sensors.add(" + clazz + ".class);");
    indent_less();
    output("}");

    for(Entry<String, List<String>> sensor : sensors.sensors.entrySet())
    {
      StringBuilder line=new StringBuilder("public static final Class[] Sensors_" + sensor.getKey());
      line.append("= {");
      if( sensor.getValue().size() > 0 )
      {
        int i;
        for(i= 0; i < sensor.getValue().size() - 1; i++)
          line.append(sensor.getValue().get(i)).append(".class, ");
        line.append(sensor.getValue().get(i)).append(".class");
      }
      line.append("};");
      output(line.toString());
    }

    // Output sensor icons for non-builtin sensors
    for(Entry<String, String> icon : sensors.icons.entrySet())
    {
      output("public static final int " + icon.getKey() + "_icon = " + icon.getValue() + ";");
    }

    empty();
    getIcon();

    footer();
  }

  private void getIcon()
  {
    boolean first= true;
    method_begin("public static", "int", "getIcon(Sensor sensor)");

    boolean outIf = false;
    for(Entry<String, List<String>> sensor : sensors.sensors.entrySet())
    {
      for(int i= 0; i < sensor.getValue().size(); i++)
      {
        outIf = true;

        output((first ? "" : "else ") + "if( sensor instanceof " + sensor.getValue().get(i) + " )");
        indent();

        String icon = sensors.icons.get(sensor.getKey());

        output("return " + icon + ";");
        indent_less();
        first= false;
      }
    }

    if( outIf )
    {
      output("else");
      indent();
    }
    output("return R.drawable.android_white;");
    if( sensors.sensors.size() > 0 )
    {
      indent_less();
    }

    method_end();
  }
}
