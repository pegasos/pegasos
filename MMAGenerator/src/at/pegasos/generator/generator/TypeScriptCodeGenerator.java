package at.pegasos.generator.generator;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public abstract class TypeScriptCodeGenerator extends CodeGenerator {
  public static final String INDENT= "  ";

  private static class STuple {
    String a;
    String b;

    public STuple(String a, String b)
    {
      this.a= a;
      this.b= b;
    }
  }

  protected final String package_name;
  protected final String classname;
  protected Set<String> imports;
  protected String extend_stmt;
  protected String implement_stmt;
  protected int indent_size;

  /**
   * Constants declared in this file
   */
  private final List<STuple> consts;

  /**
   * Global variables declared in this file
   */
  private final List<STuple> globalvars;

  public TypeScriptCodeGenerator(String pkg_name, String classname, String extend, String implement)
  {
    this.imports= new HashSet<String>();
    this.package_name= pkg_name;
    this.classname= classname;
    this.extend_stmt= extend;
    this.implement_stmt= implement;
    this.indent_size= 0;

    consts= new ArrayList<STuple>();
    globalvars = new ArrayList<STuple>();
  }

  @Override
  public void setOutputToPrintStream(Path basedir) throws IOException
  {
    String[] pkgs= package_name.split("[.]");
    Path p= basedir;
    for(String pkg : pkgs)
    {
      p= p.resolve(pkg);
    }

    if( !Files.exists(p) )
      Files.createDirectories(p);

    p= p.resolve(classname + ".ts");

    this.out= new PrintStream(Files.newOutputStream(p));
  }

  public void globalvar(String name, String value)
  {
    globalvars.add(new STuple(name, value));
  }

  public void constant(String name, String value)
  {
    consts.add(new STuple(name, value));
  }

  protected void addImport(String imp)
  {
    this.imports.add(imp);
  }

  /**
   * Generate the header for a file containing a class. The header also includes all constants
   * 
   * @param class_annotations
   *          if not null the annotations will be added before the name of the class
   */
  protected void header(String... class_annotations)
  {
    _header();

    if( class_annotations != null )
    {
      for(String anno : class_annotations)
        output(anno);
    }
    String line= "export class " + classname;
    if( extend_stmt != null )
      line+= " extends " + extend_stmt;
    if( implement_stmt != null )
      line+= " implements " + implement_stmt;
    output(line + " {");
  }

  /**
   * Generate the header of this fill. Assuming it contains no class. The header also includes all
   * constants
   */
  protected void headerNoClass()
  {
    _header();
  }

  private void _header()
  {
    output("// Folder: " + package_name + ";");
    output("// Generated file. Do not edit");
    empty();
    empty();

    for(String imp : imports)
      output("import " + imp + ";");
    if( imports.size() > 0 )
      empty();

    for(STuple c : globalvars)
    {
      output("const " + c.a + " = " + c.b + ";");
    }
    for(STuple c : consts)
    {
      output("export const " + c.a + " = " + c.b + ";");
    }
    if( consts.size() > 0 )
      empty();
  }

  protected void method_begin(String access_mod, String returntype, String methodname_params)
  {
    method_begin(access_mod, returntype, methodname_params, false);
  }

  protected void method_begin(String access_mod, String returntype, String methodname_params, boolean override)
  {
    if( override )
      output("@Override");
    output(access_mod + " " + methodname_params + ": " + returntype + " {");
    indent();
  }

  /**
   * Closes the opened method. (This also includes decreasing the indent)
   */
  protected void method_end()
  {
    indent_less();
    output("}");
    empty();
  }

  protected void empty()
  {
    output("");
  }

  protected void output(String line)
  {
    output(indent_size, line);
  }

  protected void output(int indent, String line)
  {
    for(int i= 0; i < indent; i++)
      out.print(INDENT);
    out.println(line);
  }

  protected void indent()
  {
    indent_size++;
  }

  protected void indent_less()
  {
    indent_size--;
  }
  
  /**
   * Generate the footer for a file
   */
  protected void footer()
  {
    if( indent_size > 0 )
      indent_size= 0;

    output("}");
    output("// End of generated class");
    empty();
  }

  /**
   * Generate the footer for a file without a class
   */
  protected void footerNoClass()
  {
    if( indent_size > 0 )
      indent_size= 0;

    output("// End of generated class");
    empty();
  }

  abstract public void generate() throws GeneratorException;
}
