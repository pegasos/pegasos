package at.pegasos.generator.generator;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;

public abstract class CodeGenerator {

  protected PrintStream out;

  abstract public void generate() throws GeneratorException;

  public abstract void setOutputToPrintStream(Path basedir) throws IOException;

  public void setOutput(PrintStream output)
  {
    this.out = output;
  }

  public PrintStream getOutputStream()
  {
    return this.out;
  }

  public void closeOutput()
  {
    this.out.close();
  }
}
