package at.pegasos.generator.generator;

import at.pegasos.generator.parser.token.IInstance;

public class GeneratorException extends Exception {
  private static final long serialVersionUID= 1544739654348947820L;
  private final IInstance instance;

  public GeneratorException(String reason)
  {
    super(reason);
    this.instance= null;
  }

  public GeneratorException(Exception e)
  {
    super(e);
    this.instance= null;
  }

  public GeneratorException(IInstance instance, String reason)
  {
    super(reason);
    this.instance= instance;
  }

  public GeneratorException(IInstance instance, Exception e)
  {
    super(e);
    this.instance= instance;
  }

  @Override public String getMessage()
  {
    String ret= super.getMessage();

    if( this.instance != null )
      ret+= ", Path: " + instance.directory;

    return ret;
  }
}
