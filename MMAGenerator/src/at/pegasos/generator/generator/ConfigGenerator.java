package at.pegasos.generator.generator;

import at.pegasos.generator.parser.appparser.token.*;

public class ConfigGenerator extends JavaCodeGenerator {
  private final Config config;

  public ConfigGenerator(String pkg, Config config)
  {
    super(pkg, "Config", null, null);
    this.config = config;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();

    indent();

    for(String def : config.getValueDefinitionsJava())
      output("public final static " + def + ";");

    footer();
  }
}
