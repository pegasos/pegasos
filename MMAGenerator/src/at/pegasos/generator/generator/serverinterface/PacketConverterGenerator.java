package at.pegasos.generator.generator.serverinterface;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.serverparser.token.*;

public class PacketConverterGenerator extends JavaCodeGenerator {
  private final static Logger log = LoggerFactory.getLogger(PacketConverterGenerator.class);

  private final ArrayList<Sensor> sensors;

  public PacketConverterGenerator(ArrayList<Sensor> sensors)
  {
    super("at.univie.mma.serverinterface.tools", "PacketConverter", null, null);

    this.sensors= sensors;
  }

  @Override
  public void generate() throws GeneratorException
  {
    log.info("Generating PacketConverter");

    addImport("at.pegasos.serverinterface.Packet");
    addImport("at.pegasos.serverinterface.tools.TimedData");
    addImport("at.univie.mma.serverinterface.SensorTypes");
    addImport("java.util.*");

    for(Sensor sensor : sensors)
    {
      if( sensor.type == Sensor.TYPE_GENERATE )
      {
        for(Field f : sensor.getFields())
        {
          if( f.db_type.isDouble() && (f.raw_type.isDouble()) )
          {
            addImport("java.nio.ByteBuffer");
            break;
          }
        }
      }
    }

    header();

    indent();

    for(Sensor sensor : sensors)
    {
      if( sensor.type == Sensor.TYPE_GENERATE )
      {
        for(Field f : sensor.getFields())
        {
          if( f.raw_type.isText() )
          {
            assign("private final static java.nio.charset.Charset cs", "java.nio.charset.Charset.forName(\"UTF-8\")");
            break;
          }
        }
      }
    }

    mainConvertMethod();

    for(Sensor sensor : sensors)
    {
      convertMethod(sensor);
    }

    indent_less();

    footer();
  }

  private void mainConvertMethod()
  {
    method_begin("public static", "List<TimedData>", "convert(Packet packet)");
    output("return convert(0, packet);");
    method_end();

    method_begin("public static", "List<TimedData>", "convert(long starttime, Packet packet)");
    output("switch(packet.getBytes()[2])");
    output("{");
    indent();
    for(Sensor sensor : sensors)
    {
      output("case SensorTypes." + sensor.name.toUpperCase() + ":");
      indent();
      output("return convert" + sensor.name + "(starttime, packet);");
      indent_less();
    }

    output("default:");
    indent();
    output("return new ArrayList<TimedData>(0);");
    indent_less();

    indent_less();
    output("}");
    method_end();
  }

  private void convertMethod(Sensor sensor) throws GeneratorException
  {
    log.debug("Generating method for " + sensor.name);

    method_begin("public static", "List<TimedData>", "convert" + sensor.name + "(long starttime, Packet p)");

    if( sensor.type != Sensor.TYPE_GENERATE && sensor.getConverterMethod() == null )
    {
      output("return new ArrayList<TimedData>(0);");
    }
    else if( sensor.type != Sensor.TYPE_GENERATE && sensor.getConverterMethod() != null )
    {
      output(sensor.getConverterMethod());
    }
    else
    {
      generateConvertBody(sensor);
    }

    method_end();
  }

  private void generateConvertBody(Sensor sensor) throws GeneratorException
  {
    int nByteBuffers= 0;
    for(Field f : sensor.getFields())
    {
      if( f.db_type.isDouble() && (f.raw_type.isDouble()) )
      {
        assign("ByteBuffer buffer" + nByteBuffers, "ByteBuffer.allocate(8)");
        nByteBuffers++;
      }
    }

    if( sensor.getOptions().isOptionalLength() )
    {
      output("byte flags= 0;");
      empty();
    }

    assign("ArrayList<TimedData> ret", "new ArrayList<TimedData>(" + sensor.getFields().length + ")");

    assign("byte[] in", "p.getBytes()");
    assign("int sensornr", "in[3]&31&0xff");
    assign("long time_since_start", "((int)in[5]&0xFF)|((int)in[6]&0xFF)<<8|((int)in[7]&0xFF)<<16|((int)in[8]&0xFF)<<24");
    assign("int datapackages", "((int)in[4]&0xFF)|((int)((in[3]&0xff)/32)<<8)");
    assign("long time_last_measurement", "0");
    assign("int bc", "8");
    assign("String val", "\"\"");
    output("if( datapackages > 0 )");
    output(INDENT + "time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;");
    empty();

    // output("System.out.println(\"" + sensor.name + "\");");

    output("for (int x=0;x<datapackages;x++)");
    output("{");
    indent();
    output("time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!");
    output("time_since_start+=time_last_measurement;");
    empty();
    // output("db_insert.setLong(1, data.session_id);");
    // output("db_insert.setLong(2, data.starttime+time_since_start);");

    int bc= 3;
    int increase= 2;

    if( sensor.getOptions().isOptionalLength() )
    {
      output("flags= (byte) ((byte) in[bc+3]&0xFF);");
      bc++;
      increase++;
    }

    int flag_pow= 0;

    nByteBuffers= 0;

    for(Field f : sensor.getFields())
    {
      String line= "";

      if( f.db_type.isInteger() && (f.raw_type.isInteger() || f.raw_type.isDouble() || f.raw_type.isLong()) )
      {
        line = "val= \"\" + (((int)in[bc+" + (bc++) + "]&0xFF)|((int)in[bc+" + (bc++) + "]";
        if( f.isSigned() )
          line += "&0xFF";
        line += ")<<8);";
        increase+= 2;
      }
      else if( f.db_type.isShort() && (f.raw_type.isShort()) )
      {
        line= "val= \"\" + ((short)(in[bc+" + (bc++) + "]&0xFF|in[bc+" + (bc++) + "]<<8));";
        increase+= 2;
      }
      else if( f.db_type.isDouble() && (f.raw_type.isDouble()) )
      {
        if( f.options.hasScale() )
        {
          line+= "{long tmp= 0;";
          line+= "for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+" + bc + "+i] & 0xff);}";
          line+= f.name + "= ((double)tmp / " + f.options.getScale() + ");";
          line+= "}";
        }
        else
        {
          line+= "{";
          line+= "for(int i= 0; i < 8; i++){";
          String buf= "buffer" + nByteBuffers++;
          line+= buf + ".put(i, in[bc+" + bc + "+i]);} ";
          line+= "val= \"\" + " + buf + ".getDouble(0);";
          line+= "}";
        }

        bc+= 8;
        increase+= 8;
      }
      else if( f.db_type.isLong() && (f.raw_type.isLong()) )
      {
        // for (int i = 0; i < 8; i++) {data.gps_lat=(data.gps_lat<<8)+(in[bc+3+i] & 0xff) ;}
        line+= "{long tmp= 0;";
        line+= "for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+" + bc + "+i] & 0xff);}";
        bc+= 8;
        increase+= 8;
        line+= "val= \"\" + tmp;";
        line+= "}";
      }
      else if( f.db_type.isText() && f.raw_type.isText() )
      {
        line+= "{";
        line+= "byte l" + f.name + "= in[bc+" + (bc++) + "];";
        line+= "byte[] b" + f.name + "= new byte[l" + f.name + "];";
        line+= "System.arraycopy(in, bc+" + bc +", b" + f.name + ", 0, l" + f.name + ");";
        line+= "val= new String(b" + f.name + ", cs);";
        line+= "}";

        bc+= f.options.getMaxLength();
        increase+= f.options.getMaxLength() + 1;
      }
      else
        throw new GeneratorException("Cannot generate Java Code for combination " + f.db_type + "/" + f.raw_type);

      if( f.isOptional() )
      {
        String check= "if( (flags & " + ((int) Math.pow(2, flag_pow++)) + ") != 0 )";
        output(check);
        output("{");
        indent();
      }

      output(line);
      output("ret.add(new TimedData(starttime+time_since_start, \"" + sensor.name + "." + f.name + "\", val, sensornr));");
      // if( sensor.parser.getOptions().isUseSensorNr() )
      // output("db_insert.setInt(" + no + ",sensornr);");
      // output("ret.add(new TimedData(data.starttime+time_since_start, , val, sensornr);");

      if( f.isOptional() )
      {
        indent_less();
        output("}");
      }
    }

    output("bc= bc + " + increase + ";");
    indent_less();

    if( sensor.getFields().length == 0 )
    {
      output("ret.add(new TimedData(starttime+time_since_start, \"" + sensor.name + ".\", val, sensornr));");
    }

    output("}");

    output("return ret;");
  }
}