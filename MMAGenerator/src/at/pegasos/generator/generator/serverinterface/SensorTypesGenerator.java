package at.pegasos.generator.generator.serverinterface;

import java.util.ArrayList;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.serverparser.token.Sensor;

public class SensorTypesGenerator extends JavaCodeGenerator {
  private final ArrayList<Sensor> sensors;

  public SensorTypesGenerator(ArrayList<Sensor> sensors)
  {
    super("at.univie.mma.serverinterface", "SensorTypes", null, null);
    this.sensors= sensors;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header();
    
    indent();
    
    boolean setting_added= false;
    for(Sensor sensor : sensors)
    {
      assign("public final static int " + sensor.name.toUpperCase(), "" + sensor.identifier);
      
      if( sensor.name.toUpperCase().equals("SETTING") )
        setting_added= true;
    }
    
    // TODO: this is a workaround and prob. should be removed in the future
    if( !setting_added )
      assign("public final static int SETTING","0");
    
    indent_less();
    
    footer();
  }
}

/*
File f= si_gen_dir.resolve("at").resolve("univie").resolve("mma").resolve("serverinterface").resolve("SensorTypes.java")
        .toAbsolutePath().toFile();
    try
    {
      PrintStream out= new PrintStream(f);
      
      out.println("package at.univie.mma.serverinterface;\n");
      out.println("public class SensorTypes {");
      out.println("  ");
      
      boolean setting_added= false;
      for(Sensor sensor : sensors)
      {
        out.println("  public final static int " + sensor.name.toUpperCase() + "= " + sensor.identifier + ";");
        
        if( sensor.name.toUpperCase().equals("SETTING") )
          setting_added= true;
      }
      
      // TODO: this is a workaround and prob. should be removed in the future
      if( !setting_added )
        out.println("  public final static int SETTING= 0;");
      
      out.println("}");
      
      out.close();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException("Cannot generate Type-Table. " + e.getMessage());
    }
*/
