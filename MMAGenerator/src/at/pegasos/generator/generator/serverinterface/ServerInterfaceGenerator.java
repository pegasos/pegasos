package at.pegasos.generator.generator.serverinterface;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

import org.eclipse.jgit.api.errors.*;
import org.slf4j.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.Parameters;
import at.pegasos.generator.parser.serverparser.token.Sensor;
import at.pegasos.generator.parser.token.ServerInstance;
import at.pegasos.generator.util.GitHelper;
import at.pegasos.generator.generator.server.backend.CommandsGenerator;
import at.pegasos.generator.parser.backend.token.Command;

public class ServerInterfaceGenerator {
  private final Path si_gen_dir;
  private final Path si_src_dir;
  private final ServerInstance inst;
  private final static Logger log = LoggerFactory.getLogger(ServerInterfaceGenerator.class);

  private final List<String> response_table;
  private final GitHelper git;
  private final Parameters parameters;

  public ServerInterfaceGenerator(Path outdir, ServerInstance instance, GitHelper git, Parameters parameters) {
    
    this.si_gen_dir= outdir.resolve("ServerInterface").resolve("gen");
    this.si_src_dir= outdir.resolve("ServerInterface").resolve("src");
    this.inst= instance;
    this.git= git;
    this.parameters= parameters;
    
    response_table= new ArrayList<String>();
  }

  public void run() throws GeneratorException
  {
    try
    {
      CodeGenerator gen= new CommandsGenerator("at.univie.mma.serverinterface", true, inst);
      gen.setOutputToPrintStream(si_gen_dir);
      gen.generate();
      gen.closeOutput();
      
      gen= new SensorTypesGenerator(inst.getSensors());
      gen.setOutputToPrintStream(si_gen_dir);
      gen.generate();
      gen.closeOutput();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
    
    generateSensorInterfaceSensors();
    
    generateResponses();
    
    generateServerInterfaceCommandParser();
    
    generateSensorDataParser();
  }
  
  private void generateSensorDataParser() throws GeneratorException
  {
    try
    {
      if( git.lastChange(inst.getGitBranch(parameters)) < 1526719693 )
        return;
    }
    catch( IOException | GitAPIException e1 )
    {
      e1.printStackTrace();
      throw new GeneratorException(e1.getMessage());
    }

    try
    {
      PacketConverterGenerator gen= new PacketConverterGenerator(inst.getSensors());
      gen.setOutputToPrintStream(si_gen_dir);
      gen.generate();
      gen.closeOutput();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }

  private void generateSensorInterfaceSensors() throws GeneratorException
  {
    for(Sensor sensor : inst.getSensors())
    {
      if( sensor.type == Sensor.TYPE_GENERATE )
      {
        String[] types= sensor.getOptions().samplingTypes();
        for(String sampling : types)
        {
          try
          {
            ServerInterfaceSensor g2a= new ServerInterfaceSensor(sensor, sampling);
            g2a.setOutputToPrintStream(si_gen_dir);
            g2a.generate();
            g2a.closeOutput();
          }
          catch( IOException e )
          {
            e.printStackTrace();
            throw new GeneratorException(e.getMessage());
          }
        }
      }
    }
  }
  
  private void generateResponses() throws GeneratorException
  {
    try{
      for(Command command : inst.commands.getCommands())
      {
        if( command.response_class == null )
        {
          log.debug("Generating response for " + command.name);
          CodeGenerator gen= new ResponseGenerator(command);
          gen.setOutputToPrintStream(si_gen_dir);
          gen.generate();
          gen.closeOutput();
          
          command.response_class= "at.pegasos.serverinterface.response." + command.name;
        }
        
        response_table.add("case " + command.code + ":");
        response_table.add("\tret= new " + command.response_class + "(result_code);");
        if( command.fields != null )
        {
          StringBuilder h= new StringBuilder();
          boolean comma= false;
          int len= command.fields.length + 2;
          int idx;
          for(int i= 2; i < len; i++)
          {
            if( comma ) h.append(",");
            if( command.fields[i-2].nr != -1 )
            {
              idx= command.fields[i-2].nr;
              h.append("tmp[").append(idx + 2).append("]");
            }
            else
            {
              if(i+1 < len)
                h.append("(Object[]) Arrays.copyOfRange(tmp, ").append(i).append(" , ").append(command.fields[i - 1].nr).append(")");
              else
                h.append("(Object[]) Arrays.copyOfRange(tmp, ").append(i).append(", tmp.length)");
            }
            comma= true;
          }
          response_table.add("\tret.setData(" + h + ");");
        }
        else
        {
          // response_table.add("case " + command.code + ":");
          // response_table.add("\tret= new " + command.response_class + "(result_code);");
          response_table.add("\tret.setData((Object[]) Arrays.copyOfRange(tmp, 2, tmp.length));");
        }
        response_table.add("\tbreak;");
        response_table.add("");
      }
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }
  
  private void generateServerInterfaceCommandParser() throws GeneratorException
  {
    Path p= si_src_dir.resolve("at").resolve("univie").resolve("mma").resolve("serverinterface").
      resolve("ServerInterface.java").toAbsolutePath();
    
    try
    {
      // System.out.println(p.toString() + " " + Files.exists(p));
      log.info("Generating ServerInterface - Parser");
      List<String> fileContent = new ArrayList<String>(Files.readAllLines(p, StandardCharsets.ISO_8859_1));
      
      int i = 0;
      while( i < fileContent.size() )
      {
        // Find begin of create Table
        if( fileContent.get(i++).trim().contains("/* createTable - begin */") )
        {
          // Remove everything until /* createTable - end */
          int j= i;
          while(j < fileContent.size() && !fileContent.get(j).trim().contains("/* createTable - end */") )
            fileContent.remove(j);
          
          // Add the new table and exit loop
          fileContent.addAll(i, response_table);
          break;
        }
      }
      
      Files.write(p, fileContent, StandardCharsets.ISO_8859_1);
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }
}
