package at.pegasos.generator.generator.serverinterface;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.backend.token.Command;
import at.pegasos.generator.parser.serverparser.token.ResponseField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseGenerator extends JavaCodeGenerator {
  
  private final static Logger logger= LoggerFactory.getLogger(ResponseGenerator.class);
  
  private final Command command;
  
  public ResponseGenerator(Command command)
  {
    super("at.pegasos.serverinterface.response",command.name, "ServerResponse", null);

    this.command= command;
  }

  @Override
  public void generate() throws GeneratorException
  {
    header("@SuppressWarnings(\"serial\")");
    
    indent();
    
    logger.debug("Generating Response for: " + this.command);
    
    // If there are response filds generate them
    if( command.fields != null )
    {
      for(ResponseField f : command.fields)
      {
        if( f.nr != -1 )
          member("private", f.type.toJavaType(), f.name.toLowerCase());
        else
          member("private", f.type.toJavaType() + "[]", f.name.toLowerCase());
      }
    }
    
    method_begin("public", "", command.name + "(int code)");
    output("super(code);");
    method_end();
    
    if( command.fields != null && command.fields.length > 0 )
    {
      setData();
    }
    else
    {
      method_begin("public", "void", "setData(Object... data)", true);
      method_end();
    }
    
    indent_less();
    
    footer();
  }
  
  private void setData() throws GeneratorException
  {
    method_begin("public", "void", "setData(Object... data)", true);
    int last_nr= 0;
    for(ResponseField f : command.fields)
    {
      if( f.nr != -1 )
      {
        if( f.type.isText() )
          output(f.name.toLowerCase() + "= (String) data[" + f.nr + "];");
        else if( f.type.isInteger() )
          output(f.name.toLowerCase() + "= Integer.parseInt((String) data[" + f.nr + "]);");
        else
          throw new GeneratorException("Unknown type " + f.type);
      }
      else
      {
        // TODO: this code breaks when there are gaps between the numbers
        output(f.type.toJavaType() + "[] data" + (last_nr+1) + "= ("+f.type.toJavaType() + "[]) data[" + (last_nr+1) + "];");
        output("this." + f.name.toLowerCase() +"= new "+f.type.toJavaType() + "[data" + (last_nr+1) + ".length];");
        output("for(int i" + (last_nr+1) + "= 0; i" + (last_nr+1) + " < data" + (last_nr+1) + ".length; i" + (last_nr+1) + "++)");
        output("{");
        indent();
        if( f.type.isText() )
          output("this." + f.name.toLowerCase() + "[i" + (last_nr+1) + "]= (String) data" + (last_nr+1) + "[i" + (last_nr+1) + "];");
        else if( f.type.isInteger() )
          output("this." + f.name.toLowerCase() + "[i" + (last_nr+1) + "]= Integer.parseInt(data" + (last_nr+1) + "[i" + (last_nr+1) + "]);");
        else
          assign("this." + f.name.toLowerCase() + "[i" + (last_nr+1) + "]", "data" + (last_nr+1) + "[i" + (last_nr+1) + "]");
        indent_less();
        output("}");
      }
      last_nr= f.nr;
    }
    method_end();
    
    for(ResponseField f : command.fields)
    {
      String t;
      String name= "get" + ("" + f.name.charAt(0)).toUpperCase() + f.name.substring(1);
      t= f.type.toJavaType();
      if( f.nr == -1 )
        t+= "[]";
      
      method_begin("public", t, name + "()");
      output("return " + f.name.toLowerCase() + ";");
      method_end();
    }
  }
}
