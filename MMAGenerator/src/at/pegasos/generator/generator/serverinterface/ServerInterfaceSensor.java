package at.pegasos.generator.generator.serverinterface;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.serverparser.token.*;

import java.util.*;

public class ServerInterfaceSensor extends JavaCodeGenerator {
  private final Field[] fields;
  
  private final ArrayList<String> variables;
  
  private final ArrayList<ArrayList<String>> setters;
  
  private final Sensor sensor;
  
  private final boolean isSampleOnSet;
  
  private boolean has_text_vars= false;
  
  int sample_length= 0;

	private final Options options;
  
  public ServerInterfaceSensor(Sensor sensor, final String samplingtype)
  {
    super("at.univie.mma.serverinterface.sensors", getName(sensor, samplingtype), "", null);
    
    if( samplingtype.equals(Options.SAMPLING_SAMPLE_ON_SET) )
    {
      setExtends("ServerSensorSS");
      addImport("at.univie.mma.serverinterface.ServerSensorSS");
      isSampleOnSet= true;
    }
    else
    {
      setExtends("ServerSensorFR");
      addImport("at.univie.mma.serverinterface.ServerSensorFR");
      isSampleOnSet= false;
    }
    
    this.variables= new ArrayList<String>();
    this.setters= new ArrayList<ArrayList<String>>();
    
    this.fields = sensor.getFields();
    this.options= sensor.getOptions();
    this.sensor= sensor;
  }
  
  private static String getName(Sensor sensor, String samplingtype)
  {
    if( samplingtype.equals(Options.SAMPLING_CONST_FREQUENCY) )
      return sensor.name;
    else if( samplingtype.equals(Options.SAMPLING_SAMPLE_ON_SET) )
      return sensor.name + "SS";
    else
      return sensor.name;
  }

  /**
   * Generate the constructor(s) for the sensor. If the sensor has a default datasets-per-packet setting it will
   * generate two constructors one were the default value is used, one where the number of dataset-per-packet can be
   * specified.
   */
  private void constructor()
  {
    String[] parameters;
    if( isSampleOnSet )
      parameters= new String[]{"int nr", "int datasets_per_packet"};
    else
      parameters= new String[]{"int nr", "int datasets_per_packet", "int sampling_rate"};
    
    constructor_begin(parameters);
    
    if( isSampleOnSet )
      output("super(nr, " + sensor.identifier + ", datasets_per_packet);");
    else
      output("super(nr, " + sensor.identifier + ", datasets_per_packet, sampling_rate);");
    empty(0);
    if( options.isOptionalLength() )
    	output("sample_length= " + (sample_length+1) + ";");
    else
    	output("sample_length= " + sample_length + ";");
    empty(0);
    if( isSampleOnSet )
      assign("sampling", "false");
    call("init");
    method_end();

    if( sensor.defaultDatasetsPerPacket != null )
    {
      if( isSampleOnSet )
      {
        constructor_begin(new String[]{"int nr"});
        output("super(nr, " + sensor.identifier + ", " + sensor.defaultDatasetsPerPacket + ");");
      }
      else
      {
        constructor_begin(new String[]{"int nr", "int sampling_rate"});
        output("super(nr, " + sensor.identifier + ", " + sensor.defaultDatasetsPerPacket + ", sampling_rate);");
      }

      empty(0);
      if( options.isOptionalLength() )
        output("sample_length= " + (sample_length+1) + ";");
      else
        output("sample_length= " + sample_length + ";");
      empty(0);
      if( isSampleOnSet )
        assign("sampling", "false");
      call("init");
      method_end();
    }
  }
  
  @Override
  public void generate() throws GeneratorException
  {
    System.out.println("Generating " + sensor.name);
    
    analyseVariables();
    
    header();
    
    indent();
    for(String var : variables)
      output(var);
    
    if( has_text_vars )
    {
      assign("private final static java.nio.charset.Charset cs", "java.nio.charset.Charset.forName(\"UTF-8\")");
    }
    
    empty(0);
    
    constructor();
    
    generateSetterStatements();
    
    setDataFunction();
    
    footer();
  }
  
  private void setDataFunction()
  {
  	if( !isSampleOnSet )
  	{
  		boolean[] h= new boolean[fields.length];
  		for(int i= 0; i < fields.length; i++)
  		{
        h[i]= true;
        setDataFunction(h, 1, false);
        // setDataFunction(h, 1, true);
        h[i]= false;
  		}
  		
  		// setData function
  		for(int i= 0; i < fields.length; i++)
  			h[i]= true;
      setDataFunction(h, fields.length, false);
      // setDataFunction(h, fields.length, true);
  	}
  	else
  		recurs(0, new boolean[fields.length], 0);
  }
  
  private void recurs(int idx, boolean[] out, int count)
  {
  	if( idx == fields.length )
  	{
      // 2nd part is need due to special cases where a sensor has no fields
      if( count > 0 || fields.length == 0 )
      {
        if( isSampleOnSet )
          setDataFunction(out, count, true);
        setDataFunction(out, count, false);
      }
  	}
  	else
  	{
	  	out[idx]= true;
	  	recurs(idx+1, out, count+1);
	  	out[idx]= false;
	  	if( fields[idx].isOptional() )
	  		recurs(idx+1, out, count);
  	}
  }
  
  private void setDataFunction(boolean[] out, int count, boolean timestamp)
  {
  	boolean notfirst= false;
  	int i;
    // String line;
    
    //Function header
    StringBuilder name = new StringBuilder("set");
    StringBuilder params = new StringBuilder("(");
    if( count == fields.length )
    {
    	name.append("Data");
    	for(Field f : fields)
      {
        if( notfirst )
          params.append(", ");
      	notfirst= true;
      	params.append(f.raw_type.getJavaDef()).append(" ").append(f.name);
      }
    }
    else
    {
    	i= 0;
    	for(Field f : fields)
      {
        if( out[i] )
        {
        	if( notfirst )
            params.append(", ");
        	notfirst= true;
        	
        	name.append(f.name.substring(0, 1).toUpperCase()).append(f.name.substring(1));
        	params.append(f.raw_type.getJavaDef()).append(" ").append(f.name);
        }
      	i++;
      }
      
    }

    if( timestamp )
    {
      name.append("TS");
      if( count > 0 )
        params.append(", ");
      params.append("long timestamp");
    }
    params.append(")");

    String modifier= "public";
    if( isSampleOnSet && options.isOptionalLength() ) // TODO: check whether this is safe. 
    	// In theory only when multiple events can set the data (-> optional fields) then it needs to be thread safe. 
    	modifier+= " synchronized";
    method_begin(modifier, "void", name.toString() + params);
    
    if( isSampleOnSet )
      output("if( !sampling ) return;");

    if( options.isOptionalLength() )
    {
	    StringBuilder line = new StringBuilder("data_register[0] |= (byte) (");
	    int pow= 0;
	    notfirst= false;
	    boolean something_there= false;
	    for(i= 0; i < fields.length; i++)
	    {
	    	if( fields[i].isOptional() )
	    	{
	    		if(out[i])
	    		{
	    			if(notfirst)
	    				line.append(" | ");
	    			notfirst= true;
	    			line.append((int) Math.pow(2, pow));
	    			something_there= true;
	    		}
	    		pow++;
	    	}
	    }
	    line.append(");");
	    if( something_there)
	    	output(line.toString());
    }
    
    // set variables
    i= 0;
    for(ArrayList<String> stmts : setters)
    {
    	if( out[i] )
    		for(String stmt : stmts)
    			output(stmt);
    	i++;
    }
    
    if( isSampleOnSet )
    {
      if( timestamp )
        output("addData_fixed(timestamp);");
      else
        output("addData_fixed();");
      if( options.isOptionalLength() )
      	output("data_register[0] = (byte) 0;");
    }
    method_end();
  }
  
  private void analyseVariables()
  {
    int variable= 1;
    boolean buffer_imported= false;
    
    for(Field f : fields)
    {
      if( f.db_type.isDouble() && f.raw_type.isDouble() || f.db_type.isLong() && f.raw_type.isLong() )
      {
        variables.add("ByteBuffer buffer" + variable + "= ByteBuffer.allocate(8);");
        variable++;
        if( !buffer_imported )
        {
          buffer_imported= true;
          addImport("java.nio.ByteBuffer");
        }
      }
      
      if( f.db_type.isShort() && f.raw_type.isShort() || f.db_type.isInteger() && f.raw_type.isInteger() )
        sample_length+= 2;
      else if( f.db_type.isDouble() && f.raw_type.isDouble() || 
          f.db_type.isLong() && f.raw_type.isLong() )
        sample_length+= 8;
      else if( f.db_type.isInteger() && f.raw_type.isDouble() )
      {
        if( !f.options.hasDropDecimal() )
        {//TODO: throw exception here and not later?
        }
        sample_length+= 2;
      }
      else if( f.db_type.isInteger() && f.raw_type.isLong() )
      {
        if( !f.options.hasDropPrecision() )
        {//TODO: throw exception here and not later?
        }
        sample_length+= 2;
      }
      
      if( f.raw_type.isText() )
      {
        sample_length+= 1 + f.options.getMaxLength();
        has_text_vars= true;
      }
    }
    
    // System.out.println(sensor.name + " sample_length: " + sample_length);
  }
  
  /**
   * Generate the statements for setting the values in the data register
   * @throws GeneratorException when parts are miss configured and cannot be generated.
   */
  private void generateSetterStatements() throws GeneratorException
  {
    String line;
    int reg_idx= 0;
    int var_idx= 1;
    
    if( options.isOptionalLength() )
    	reg_idx+= 1;
    
    for(Field f : fields)
    {
      ArrayList<String> lines= new ArrayList<String>();
      
      if( f.db_type.isShort() && f.raw_type.isShort() || f.db_type.isInteger() && f.raw_type.isInteger() )
      {
        // buffer[16]= (byte) (alt&0xFF);         
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " & 0xFF);";
        lines.add(line);
        
        // buffer[17]= (byte) ((alt>>8) & 0xff);
        line= "data_register[" + (reg_idx++) + "] = (byte) ((" + f.name + " >> 8) & 0xFF);";
        lines.add(line);
      }
      else if( f.db_type.isDouble() && f.raw_type.isDouble() )
      {
        // long lat = (long) (location.getLatitude() * 100000000);
        if( f.options.hasScale() )
        {
          // long lat = (long) (location.getLatitude() * 100000000);
          line= "long tmp" + var_idx + "= (long) (" + f.name + " * " + f.options.getScale() + ");";
          lines.add(line);
          // byte[] bytes1 = buffer1.putLong(0, lat).array();
          line= "byte[] bytes" + var_idx + "= buffer" + var_idx + ".putLong(0, tmp" + var_idx + ").array();";
          lines.add(line);
          
          // System.arraycopy(bytes1, 0, data_register, 0, 8);
          line= "System.arraycopy(bytes" + var_idx + ", 0, data_register, " + reg_idx + ", 8);";
          lines.add(line);
        }
        else
        {
          line= "buffer" + var_idx +".putDouble(0, " + f.name + ");";
          lines.add(line);
          line= "System.arraycopy(buffer" + var_idx + ".array(), 0, data_register, " + reg_idx + ", 8);";
          lines.add(line);
        }
        
        reg_idx+= 8;
        var_idx++;
      }
      else if( f.db_type.isInteger() && f.raw_type.isDouble() )
      {
        if( !f.options.hasDropDecimal() )
          throw new GeneratorException("When converting double to int it is required to confirm this by adding option 'drop_decimal'");
        
        // data_register[0]=(byte) (speed);     // SPEED_MM LOW
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + "); // LOW";
        lines.add(line);
        
        // data_register[1]=(byte) (speed/256); // SPEED_MM HIGH
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " / 256); // HIGH";
        lines.add(line);
      }
      else if( f.db_type.isLong() && f.raw_type.isLong() )
      {
        // byte[] bytes1 = buffer1.putLong(0, lat).array();
        line= "byte[] bytes" + var_idx + "= buffer" + var_idx + ".putLong(0, " + f.name + ").array();";
        lines.add(line);
        
        // System.arraycopy(bytes1, 0, data_register, 0, 8);
        line= "System.arraycopy(bytes" + var_idx + ", 0, data_register, " + reg_idx + ", 8);";
        lines.add(line);
        
        reg_idx+= 8;
        var_idx++;
      }
      else if( f.db_type.isInteger() && f.raw_type.isLong() )
      {
        if( !f.options.hasDropPrecision() )
          throw new GeneratorException("When converting long to int it is required to confirm this by adding option 'drop_precision'");
        
        // data_register[0]=(byte) (speed);     // LOW 0....255
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + "); // LOW 0....255";
        lines.add(line);
        
        // data_register[1]=(byte) (speed/256); // HIGH 0....255
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " / 256); // HIGH 0....255";
        lines.add(line);
      }
      else if( f.db_type.isText() && f.raw_type.isText() )
      {
        /*byte[] bvalue= value.getBytes(cs);
        
        byte ln= (byte) (bname.length > 30 ? 30 : bname.length);
        byte lv= (byte) (bvalue.length > 60 ? 60 : bvalue.length);
        data_register[0]= ln;
        data_register[1]= lv;
        
        System.arraycopy(bname, 0, data_register, 2, ln);*/
        lines.add("byte[] b" + f.name + "= " + f.name + ".getBytes(cs);");
        lines.add("byte l" + f.name + 
            "= (byte) (b" + f.name + ".length > " + f.options.getMaxLength() + "? " +  
            f.options.getMaxLength() + " : b" + f.name + ".length);");
        lines.add("data_register[" + (reg_idx++) + "] = l" + f.name + ";");
        lines.add("System.arraycopy(b" + f.name + ", 0, data_register, " + reg_idx + ", l" + f.name + ");");
        reg_idx+= f.options.getMaxLength();
      }
      else
        throw new GeneratorException("Cannot generate sender for " + f.db_type + "/" + f.raw_type);
      
      setters.add(lines);
    }
  }
}
