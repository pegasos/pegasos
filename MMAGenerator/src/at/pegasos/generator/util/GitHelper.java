package at.pegasos.generator.util;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.token.*;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.ResetCommand.*;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.errors.*;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.*;
import org.slf4j.*;

import java.io.*;
import java.nio.file.*;

public class GitHelper {
  private final static Logger log = LoggerFactory.getLogger(GitHelper.class);

  private final String repo_addr;

  private Git git;

  private final Path outdir;

  /**
   * The current branch of the cloned directory
   */
  private String currentBranch;

  private boolean cloned;

  public GitHelper(GitInfo info, Path outdir, Parameters parameters)
  {
    this.repo_addr= info.getGitRoot(parameters);
    this.outdir= outdir;
    this.currentBranch= null;
    this.cloned= false;
  }

  public void initFromExising() throws IOException
  {
    git = Git.open(outdir.toAbsolutePath().toFile());
  }

  public void initAsClone() throws GitAPIException, IOException
  {
    if( !cloned )
    {
      initAsClone(null);
    }
    else
    {
      log.debug("Have cloned this before --> do nothing");
    }
  }

  private void initAsClone(String branch) throws GitAPIException
  {
    File localPath= outdir.toAbsolutePath().toFile();

    String ref= repo_addr;
    if( !isRemoteReference(repo_addr) )
    {
      ref= Paths.get(repo_addr).toAbsolutePath().normalize().toUri().toString();
    }

    // Clone repository into local path
    log.info("Cloning from {} to {}", ref, localPath);

    CloneCommand call= Git.cloneRepository()
           .setURI(ref)
           .setDirectory(localPath);

    if( branch != null )
    {
      call= call.setBranch(branch);
    }

    git= call.call();
    // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
    log.debug("Having repository: {}", git.getRepository().getDirectory());

    git.close();

    if( branch != null )
    {
      currentBranch= branch;
    }
    cloned= true;
  }

  public int lastChange(String branch) throws IOException, GitAPIException
  {
    log.debug("Determining last Change of: repository:{}, branch:{}", repo_addr, branch);
    Git remote;
    if( isRemoteReference(repo_addr) )
    {
      initAsClone(branch);
      remote= Git.open(outdir.toFile());
    }
    else
    {
      remote= Git.open(new File(repo_addr));
    }

    Ref ref_branch = remote.getRepository().findRef(branch);

    if( ref_branch == null )
    {
      log.error("Branch '{}' not found", branch);
      throw new RefNotFoundException(branch);
    }

    RevCommit commit = remote.log().add(ref_branch.getObjectId()).setMaxCount(1).call().iterator().next();
    log.debug("Last Commit: " + commit.getCommitTime());
    remote.close();

    return commit.getCommitTime();
  }

  public void prepareBranch(String branch) throws RefAlreadyExistsException, RefNotFoundException, InvalidRefNameException, CheckoutConflictException, GitAPIException, IOException
  {
    if( !branch.equals(currentBranch) )
    {
      String ref_name= "origin/" + branch;

      git.clean();

      log.info("Checking out branch" + ref_name);
      git.checkout().setName(ref_name).call();

      currentBranch= branch;
    }
  }

  public void cleanUp() throws NoWorkTreeException, GitAPIException
  {
    git.reset().setMode(ResetType.HARD).call();
    git.clean().call();
  }

  /**
   * Check if a path is a remote reference
   * @param path repository address as specified in parameters / xml
   * @return true if path is classified as an URL / remote reference
   */
  public static boolean isRemoteReference(String path)
  {
    return path.toLowerCase().endsWith(".git") && path.contains(":") && path.contains(".");
  }
}