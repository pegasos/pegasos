package at.pegasos.generator.util;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class ConfigDefaults {
  public static Config get(String instance, String type) throws ParserException
  {
    List<Parameters.ConfigFile> configs = Parameters.get().configFilesInst;
    for(Parameters.ConfigFile config : configs)
    {
      System.out.println(config.type + "/" + type + " " + config.instance + "/" + instance);
      if( config.type.equals(type) && config.instance.equals(instance) )
      {
        if( config.useDefault )
        {
          Config ret = defaultConfig(type);
          try
          {
            ret.read(config.path);
            return ret;
          }
          catch (IOException e)
          {
            throw new ParserException(e);
          }
        }
        else
          return new Config(config.path);
      }
    }

    return defaultConfig(type);
  }

  private static Config defaultConfig(String type) throws ParserException
  {
    switch (type)
    {
      case "ai":
        return new Config(Paths.get("config/ai.defaults"));
      case "server":
        return new Config(Paths.get("config/server.defaults"));
      case "web":
        return new Config(Paths.get("config/web.defaults"));
      default:
        throw new IllegalArgumentException("No type: " + type);
    }
  }
}
