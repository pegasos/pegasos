package at.pegasos.generator;

import com.beust.jcommander.*;
import com.beust.jcommander.internal.*;
import lombok.*;

import java.nio.file.*;
import java.util.*;

@ToString
public class Parameters {
  @ToString
  public static class ConfigFile {
    public String type;

    public String instance;

    public Path path;

    public boolean useDefault = true;
  }

  public static class ConfigFileConverter implements IStringConverter<ConfigFile> {
    @Override
    public ConfigFile convert(String string)
    {
      String [] paths = string.split(",");
      ConfigFile config = new ConfigFile();
      config.type = paths[0];
      config.instance = paths[1];
      config.path = Paths.get(paths[2]);
      return config;
    }
  }

  private static Parameters params;

  /**
   * Get the parameters
   *
   * @return global parameters
   */
  public static Parameters get() {
    return params;
  }

  /**
   * Set the default parameters to use for this run
   *
   * @param p Parameters to be used globally
   */
  public static void set(Parameters p) {
    params = p;
  }

  @Parameter(names = "--servers-only", description = "Build only servers.")
  public boolean servers_only = false;

  @Parameter(names = "--apps-only", description = "Build only apps and use previously built servers.")
  public boolean apps_only = false;

  @Parameter(names = {"--apps-location", "-a"}, description = "Where the apps are located")
  public List<String> inputdir_apps_string = Lists.newArrayList(new String[]{});

  @Parameter(names = {"--server-location", "-s"}, description = "Where servers are located")
  public List<String> inputdir_servers_string = Lists.newArrayList(new String[]{});

  @Parameter(names = {"--ignore-app-servers", "-i"}, description = "Ignore servers specified in app directories")
  public boolean ignore_app_servers = false;

  @Parameter(names = {"--apps-always", "-o"}, description = "Build always all apps (i.e. force apps)")
  public boolean apps_always = false;

  @Parameter(names = "--builddir-servers", description = "Where the servers will be built")
  public String server_out_dir = "/tmp/PegasosServers/";

  @Parameter(names = "--builddir-apps", description = "Where the apps will be built")
  public String apps_out_dir = "/tmp/PegasosClients/";

  @Parameter(names = "--help", help = true)
  public boolean help;

  @Parameter(names = "--repository", description = "Location of the default pegasos repository to be used for building. Default root directory")
  public String repository = "/pegasos/repo";

  @Parameter(names = "--branch", description = "Name of the default branch to be used for an app. Default stable")
  public String branch = "stable";

  @Parameter(names = "--force-repository", description = "Force specified repository")
  public boolean forceRepository;

  @Parameter(names = "--lastbuilt", description = "Location of the file containing the building information")
  public String lastbuilt = "lastbuilt.txt";

  @Parameter(names = "--clean", description = "Remove all temporary directories after building (both app and servers")
  public boolean clean;

  @Parameter(names = "--clean-tmp-apps", description = "Remove all temporary app directories after building")
  public boolean clean_tmp_apps;

  @Parameter(names = "--clean-tmp-servers", description = "Remove all temporary server directories after building")
  public boolean clean_tmp_servers;

  @Parameter(names = "--omitweb", description = "Do not build web interface")
  public boolean omitweb = false;

  /**
   * If true builders should not run compile tasks (other than to generate intermediates necessary for generation tasks)
   */
  @Parameter(names = {"--generate", "-g"}, description = "Only generate the code. Do not compile it")
  public boolean generateOnly = false;

  @Parameter(names = {"--config-instance"}, description = "Specify configurations for instances", listConverter = ConfigFileConverter.class)
  public List<ConfigFile> configFilesInst = Lists.newArrayList(new ConfigFile[]{});

  public void check()
  {
    if (servers_only && apps_only)
      throw new IllegalArgumentException();

    if (forceRepository && repository == null)
      throw new IllegalArgumentException("Need to specify repository when forcing repository");
  }

  public Parameters copy()
  {
    Parameters ret = new Parameters();
    ret.apps_always = apps_always;
    ret.apps_only = apps_only;
    ret.apps_out_dir = apps_out_dir;
    ret.branch = branch;
    ret.clean = clean;
    ret.clean_tmp_apps = clean_tmp_apps;
    ret.clean_tmp_servers = clean_tmp_servers;
    ret.configFilesInst = new ArrayList<ConfigFile>(configFilesInst.size());
    ret.configFilesInst.addAll(configFilesInst);
    ret.forceRepository = forceRepository;
    ret.help = help;
    ret.ignore_app_servers = ignore_app_servers;
    ret.inputdir_apps_string = new ArrayList<String>(inputdir_apps_string.size());
    ret.inputdir_apps_string.addAll(inputdir_apps_string);
    ret.inputdir_servers_string = new ArrayList<String>(inputdir_servers_string.size());
    ret.inputdir_servers_string.addAll(inputdir_servers_string);
    ret.lastbuilt = lastbuilt;
    ret.omitweb = omitweb;
    ret.repository = repository;
    ret.server_out_dir = server_out_dir;
    ret.servers_only = servers_only;

    return ret;
  }
}
