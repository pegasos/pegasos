package at.pegasos.generator.parser;

import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import at.pegasos.generator.parser.appparser.token.Config;

public class ConfigParser {
  private final static Logger log= LoggerFactory.getLogger(ConfigParser.class);
  
  Config conf;

  private Path file;
  
  Document doc;
  
  public ConfigParser(Path file) throws ParserException
  {
    this.conf= new Config(file);
  }
  
  public ConfigParser(Config config)
  {
    this.conf= config;
  }
  
  public void setInputFile(Path file)
  {
    this.file= file;
  }
  
  public void setDocument(Document doc)
  {
    this.doc= doc;
  }
  
  private void openDoc() throws ParserConfigurationException, SAXException, IOException
  {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    doc= dBuilder.parse(file.toFile());
    doc.getDocumentElement().normalize();
  }
  
  public void parse() throws ParserException
  {
    try
    {
      if( doc == null )
        openDoc();
      
      NodeList nList= doc.getElementsByTagName("config");
      
      // Just for fun someone could define config entries at more than one location
      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        log.info("Parsing config entries");
        NodeList pList = ((Element) nList.item(temp)).getElementsByTagName("param");
        for(int p= 0; p < pList.getLength(); p++)
        {
          parseParam(pList.item(p));
        }
      }
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
    catch (SAXException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
    catch (IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private void parseParam(Node node) throws ParserException
  {
    Element e= (Element) node;
    
    // <param name="logging" value="true" />
    String name= e.getAttribute("name");
    String value= e.getAttribute("value");
    String type= e.getAttribute("type");
    if( type.toLowerCase().equals("string") )
      conf.setValueString(name, value);
    else if( type.toLowerCase().equals("bool") )
      conf.setValueBool(name, value);
    else
      conf.setValue(name, value);
  }
  
  public Config getConfig()
  {
    return conf;
  }
}
