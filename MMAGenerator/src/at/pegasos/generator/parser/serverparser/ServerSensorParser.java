package at.pegasos.generator.parser.serverparser;

import java.io.*;

import at.pegasos.generator.parser.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.pegasos.generator.generator.server.backend.SensorDataParserTableGenerator;
import at.pegasos.generator.parser.serverparser.token.Sensor;
import at.pegasos.generator.servergenerator.SensorConfigParser;
import at.pegasos.generator.parser.token.ServerInstance;

public class ServerSensorParser {
  private final static Logger log= LoggerFactory.getLogger(ServerSensorParser.class);
  
  private final ServerInstance inst;
  
  public ServerSensorParser(ServerInstance instance)
  {
    this.inst= instance;
  }
  
  public void parse() throws ParserException
  {
    String line;
    
    try
    {
      FileReader inputStream= new FileReader(inst.directory.resolve("sensors.txt").toFile());
      
      log.debug("Parsing server sensor information in {}", inst.directory.toAbsolutePath());
      BufferedReader reader= new BufferedReader(inputStream);
      
      reader.readLine();
      while( (line= reader.readLine()) != null )
      {
        System.out.println(line);
        String[] items= line.split("\t");

        if( items.length != 5 )
          throw new ParserException("Invalid number of arguments for Sensor. Got " + items.length + " instead of 5.");

        int identifier = Integer.parseInt(items[0]);
        String name = items[1];
        String db_table = items[2];
        String parseType = items[3];
        String argument = items[4];

        Sensor sensor= new Sensor(identifier, name, db_table, parseType, argument);
        inst.addSensor(sensor);

        if( sensor.fileName != null && sensor.fileName.endsWith("txt"))
        {
          log.info("Parsing: " + sensor.name);
          SensorConfigParser parser= new SensorConfigParser(inst.directory.resolve(sensor.fileName).toString(), sensor);
          parser.parse();

          sensor.className = SensorDataParserTableGenerator.ParsersPackage + "." + sensor.name;
        }
        else if( sensor.fileName != null && (sensor.fileName.endsWith("yml") || sensor.fileName.endsWith("yaml") ) )
        {
          sensor.className = SensorDataParserTableGenerator.ParsersPackage + "." + sensor.name;

          SensorConfigYaml parser= new SensorConfigYaml(inst.directory.resolve(sensor.fileName).toString(), sensor);
          parser.parse();
        }
        else if( sensor.fileName != null )
          throw new ParserException("Cannot parse filename '" + sensor.fileName + "'");
      }
      
      reader.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new ParserException(e.getMessage(), inst.directory.resolve("sensors.txt").toString(), null);
    }
  }
}
