package at.pegasos.generator.parser.serverparser.token;

import at.pegasos.generator.parser.*;
import lombok.ToString;

@ToString
public class Sensor {
  public final static int TYPE_JAVACLASS= 1;
  public final static int TYPE_GENERATE= 2;
  public String className;
  public final String fileName;
  
  public int identifier;
  public String name;
  public String db_table;
  public int type;
  public Integer defaultDatasetsPerPacket;

  private Field[] fields;
  private BlockSessionInsert fields_session;
  private FieldDb[] fields_session_db;
  private Options options;
  private BlockSessionInsert extra_code;
  private FieldSession[] fields_session_code;
  private String dbDefition;
  private String converterMethod;

  public Sensor(String[] items) throws ParserException
  {
    if( items.length != 5 )
      throw new ParserException("Invalid number of arguments for Sensor. Got " + items.length + " instead of 5.");
    
    identifier= Integer.parseInt(items[0]);
    name= items[1];
    db_table= items[2];
    
    if( items[3].equals("java") )
    {
      type = TYPE_JAVACLASS;
      if( !items[4].endsWith("txt") && !items[4].endsWith("yml") )
      {
        className = items[4];
        fileName = null;
      }
      else
      {
        fileName = items[4];
        className = null;
      }
    }
    else if( items[3].equals("generate") )
    {
      type = TYPE_GENERATE;
      className = null;
      fileName = items[4];
    }
    else
      throw new ParserException("Invalid Type: " + items[3]);
  }

  public Sensor(int identifier, String name, String dbTable, String parseType, String argument) throws ParserException {
    this.identifier = identifier;
    this.name = name;
    this.db_table = dbTable;

    if( parseType.equals("java") )
      type= TYPE_JAVACLASS;
    else if( parseType.equals("generate") )
      type= TYPE_GENERATE;
    else
      throw new ParserException("Invalid Type: " + parseType);

    if( type == TYPE_JAVACLASS && ! (argument.endsWith("txt") || argument.endsWith("yml") ) )
    {
      className = argument;
      fileName = null;
    }
    else
    {
      fileName = argument;
      className = null;
    }
  }

  public boolean isGenerate()
  {
    return type == TYPE_GENERATE;
  }

  public void setFields(Field[] fields)
  {
    this.fields= fields;
  }
  
  public Field[] getFields()
  {
    return fields;
  }
  
  public void setSessionInsertBlock(BlockSessionInsert fields_session)
  {
    this.fields_session= fields_session;
  }
  
  public BlockSessionInsert getSessionInsertBlock()
  {
    return this.fields_session;
  }
  
  public void setFieldsDb(FieldDb[] fields_session_db)
  {
    this.fields_session_db= fields_session_db;
  }
  
  public FieldDb[] getSessionFieldsDb()
  {
    return fields_session_db;
  }
  
  public void setSessionFieldsCode(FieldSession[] fields_session_code)
  {
    this.fields_session_code= fields_session_code;
  }
  
  public FieldSession[] getSessionFieldsCode()
  {
    return fields_session_code;
  }
  
  public void setOptions(Options options)
  {
    this.options= options;
  }

  public Options getOptions()
  {
    return options;
  }

  public void setExtraCode(BlockSessionInsert extra_code)
  {
    this.extra_code= extra_code;
  }
  
  public BlockSessionInsert getExtraCode()
  {
    return extra_code;
  }

  public String getDatabaseDefinition()
  {
    return dbDefition;
  }

  public void setDatabaseDefintion(String database)
  {
    this.dbDefition = database;
  }

  public String getConverterMethod()
  {
    return converterMethod;
  }

  public void setConverterMethod(String converterMethod)
  {
    this.converterMethod = converterMethod;
  }
}
