package at.pegasos.generator.parser.serverparser.token;

public class Type {
  enum TType {
    SHORT,
    INT,
    LONG,
    DOUBLE,
    STRING
  };

  public final static int TYPE_INT= 1;
  public final static int TYPE_LONG= 2;
  public final static int TYPE_SHORT= 3;
  public final static int TYPE_DOUBLE= 4;
  public final static int TYPE_STRING= 5;
  public final static int TYPE_BYTE = 6;

  private final int type;

  public Type(int type)
  {
    this.type= type;
  }
  
  public static Type fromString(String string) {
    if( string.equals("int") )
      return new Type(TYPE_INT);
    else if( string.equals("long") )
      return new Type(TYPE_LONG);
    else if( string.equals("short") )
      return new Type(TYPE_SHORT);
    else if( string.equals("double") )
      return new Type(TYPE_DOUBLE);
    else if( string.equals("string") )
      return new Type(TYPE_STRING);
    return null;
  }

  public boolean isInteger() {
    return type == TYPE_INT;
  }
  
  public boolean isShort() {
    return type == TYPE_SHORT;
  }
  
  public boolean isLong() {
    return type == TYPE_LONG;
  }

  public boolean isDouble() {
    return type == TYPE_DOUBLE;
  }

  public boolean isByte() {
    return type == TYPE_BYTE;
  }

  public String getJavaDef()
  {
    switch( type )
    {
      case TYPE_INT:
        return "int";
      case TYPE_LONG:
        return "long";
      case TYPE_SHORT:
        return "short";
      case TYPE_DOUBLE:
        return "double";
      case TYPE_STRING:
        return "String";
      case TYPE_BYTE:
        return "byte";
      default:
        return "";
    }
  }
  
  public String toString()
  {
    return getJavaDef();
  }

  public String getSqlDef() {
    switch( type )
    {
      case TYPE_INT:
        return "int";
      case TYPE_LONG:
        return "bigint";
      case TYPE_SHORT:
        return "smallint";
      case TYPE_DOUBLE:
        return "double";
      case TYPE_STRING:
        return "varchar";
      case TYPE_BYTE:
        return "tinyint";
      default:
        return "";
    }
  }

  public boolean isText()
  {
    return type == TYPE_STRING;
  }
}
