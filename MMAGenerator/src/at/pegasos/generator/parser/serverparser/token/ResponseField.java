package at.pegasos.generator.parser.serverparser.token;

import at.pegasos.generator.parser.token.EntryType;

public class ResponseField {

  public int nr;
  public EntryType type;
  public String name;
  
  @Override
  public String toString()
  {
    return "ResponseField [nr=" + nr + ", type=" + type + ", name=" + name + "]";
  }
}
