package at.pegasos.generator.parser.serverparser.token;

import java.util.*;

import at.pegasos.generator.parser.ParserException;
import lombok.ToString;

@ToString
public class Field {

  @ToString
  public static class Options {
    private final static String FIELD_SCALE = "scale";
    private final static String FIELD_INSERTONLY = "insertonly";
    private final static String FIELD_DROPDECIMAL = "drop_decimal";
    private final static String FIELD_DROPPRECISION = "drop_precision";
    private final static String FIELD_DBNAME = "dbname";
    private final static String FIELD_MAXLEN = "maxlength";

    private final Map<String,String> values;
    private boolean signed = true;

    private boolean isOptional = false;
    
    public Options()
    {
      values= new HashMap<String,String>();
    }

    public void set(Map<String, Object> values) throws ParserException
    {
      for(String option : values.keySet() )
      {
        if( option.equals("dbtype") || option.equals("rawtype") )
          continue;

        if( option.equalsIgnoreCase("optional") )
        {
          Object optional = values.get(option);
          if( !(optional instanceof Boolean) )
            throw new ParserException("Optional needs to be a boolean value: " + option + " = " + optional);

          isOptional = (boolean) optional;
        }
        else if( option.equalsIgnoreCase("signed") )
        {
          Object signed = values.get(option);
          if( !(signed instanceof Boolean) )
            throw new ParserException("Signed needs to be a boolean value: " + option + " = " + signed);

          this.signed = (boolean) signed;
        }
        else if( option.equalsIgnoreCase(FIELD_INSERTONLY) )
        {
          Object insertOnly = values.get(option);
          if( !(insertOnly instanceof Boolean) )
            throw new ParserException(FIELD_INSERTONLY + " needs to be a boolean value: " + option + " = " + insertOnly);

          if( (boolean) insertOnly )
            this.values.put(FIELD_INSERTONLY, "");
        }
        else if( option.equalsIgnoreCase(FIELD_SCALE) )
        {
          Object scale = values.get(option);
          if( !(scale instanceof Integer) )
            throw new ParserException(FIELD_SCALE + " needs to be an integer value: " + option + " = " + scale);

          this.values.put(FIELD_SCALE, scale.toString());
        }
        else if( option.equalsIgnoreCase(FIELD_DROPDECIMAL) )
        {
          Object dropDecimal = values.get(option);
          if( !(dropDecimal instanceof Boolean) )
            throw new ParserException(FIELD_DROPDECIMAL + " needs to be a boolean value: " + option + " = " + dropDecimal);

          if( (boolean) dropDecimal )
            this.values.put(FIELD_DROPDECIMAL, "");
        }
        else if( option.equalsIgnoreCase(FIELD_DROPPRECISION) )
        {
          Object dropDecimal = values.get(option);
          if( !(dropDecimal instanceof Boolean) )
            throw new ParserException(FIELD_DROPPRECISION + " needs to be a boolean value: " + option + " = " + dropDecimal);

          if( (boolean) dropDecimal )
            this.values.put(FIELD_DROPPRECISION, "");
        }
        else if( option.equalsIgnoreCase(FIELD_DBNAME) )
        {
          Object dbName = values.get(option);
          if( !(dbName instanceof String) )
            throw new ParserException(FIELD_DBNAME + " needs to be a string: " + option + " = " + dbName);

          this.values.put(FIELD_DBNAME, dbName.toString());
        }
        else if( option.equalsIgnoreCase(FIELD_MAXLEN) )
        {
          Object maxLen = values.get(option);
          if( !(maxLen instanceof Integer) )
            throw new ParserException(FIELD_MAXLEN + " needs to be an integer value: " + option + " = " + maxLen);

          this.values.put(FIELD_MAXLEN, maxLen.toString());
        }
        else
        {
          throw new ParserException("Unknown Field-Option: " + option);
        }
      }
    }
    
    public void addOption(String text) throws ParserException
    {
      if( text.equals(FIELD_INSERTONLY) )
      {
        this.values.put(FIELD_INSERTONLY, "");
      }
      else if( text.startsWith(FIELD_SCALE+ ":"))
      {
        this.values.put(FIELD_SCALE, text.split(":")[1]);
      }
      else if( text.equals(FIELD_DROPDECIMAL) )
      {
        this.values.put(FIELD_DROPDECIMAL, "");
      }
      else if( text.equals(FIELD_DROPPRECISION) )
      {
        this.values.put(FIELD_DROPPRECISION, "");
      }
      else if( text.startsWith(FIELD_DBNAME + ":") )
      {
        this.values.put(FIELD_DBNAME, text.split(":")[1]);
      }
      else if( text.startsWith(FIELD_MAXLEN + ":") )
      {
        this.values.put(FIELD_MAXLEN, text.split(":")[1]);
      }
      else
      {
        throw new ParserException("Unknown Field-Option: " + text);
      }
    }

    public boolean isInsertOnly()
    {
      return this.values.containsKey(FIELD_INSERTONLY);
    }
    
    public boolean hasScale()
    {
      return this.values.containsKey(FIELD_SCALE);
    }
    
    public int getScale()
    {
      return Integer.parseInt(this.values.get(FIELD_SCALE));
    }
    
    public String getDBname()
    {
      return this.values.get(FIELD_DBNAME);
    }
    
    public boolean hasDropDecimal()
    {
      return this.values.containsKey(FIELD_DROPDECIMAL);
    }

    public boolean hasDropPrecision()
    {
      return this.values.containsKey(FIELD_DROPPRECISION);
    }
    
    public boolean hasDifferentDBname()
    {
      return this.values.containsKey(FIELD_DBNAME);
    }

    public int getMaxLength()
    {
      return Integer.parseInt(this.values.get(FIELD_MAXLEN));
    }

    public void setOptional(boolean b)
    {
      this.isOptional = b;
    }
  }

  public int number;
  public String name;
  public Type db_type;
  public Type raw_type;
  public Options options= new Options();

  public Options getOptions()
  {
    return options;
  }

  public boolean isOptional()
  {
    return this.options.isOptional;
  }

  public boolean isSigned()
  {
    return this.options.signed;
  }
}

