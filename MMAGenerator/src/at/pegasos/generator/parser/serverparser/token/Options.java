package at.pegasos.generator.parser.serverparser.token;

import java.util.*;
import java.util.Map.Entry;

import at.pegasos.generator.parser.ParserException;
import lombok.ToString;

@ToString
public class Options {
  private final static String FIELD_SESSION_INSERT = "field-is-session-name";
  private final static String USE_SENSOR_NR = "use-sensor-nr";
  private final static String SAMPLING = "sampling-type";
  public final static String OPTIONAL_FIELDS = "optional-fields";
  private final static String POSITIVE = "true";  
  
  public final static String SAMPLING_CONST_FREQUENCY= "frequency";
  public final static String SAMPLING_SAMPLE_ON_SET= "sample-on-set";
  public final static String GENERATE_MODEL = "dbmodelgenerate";
  
  private final Map<String,String> values;
  
  private final static Set<String> options;
  static{
    options= new HashSet<String>();
    options.add(FIELD_SESSION_INSERT);
    options.add(USE_SENSOR_NR);
    options.add(SAMPLING);
    options.add(OPTIONAL_FIELDS);
    options.add(GENERATE_MODEL);
    options.add("length");
  }
  
  public Options(int start, ArrayList<String> lines) throws ParserException
  {
    values= new HashMap<String,String>();
    
    for(String line : lines)
    {
      if( line.startsWith("#") )
        continue;
      String[] defs= line.split(": ");
      values.put(defs[0], defs[1]);
    }
    
    checkMandatoryOptions();
    
    validateOptions();
  }

  public Options(Map<String, Object> data, boolean checkMandory) throws ParserException
  {
    values = new HashMap<String,String>();

    for(String key : data.keySet())
    {
      if( options.contains(key) )
      {
        values.put(key, data.get(key).toString());
      }
    }

    if( checkMandory )
      checkMandatoryOptions();

    validateOptions();
  }

  private void checkMandatoryOptions() throws ParserException
  {
    if( !values.containsKey("length") )
      throw new ParserException("Mandatory option 'length' is missing");
  }
  
  private void validateOptions() throws ParserException
  {
    for(Entry<String, String> e : values.entrySet())
    {
      if( options.contains(e.getKey()) )
      {
        if( e.getKey().equals(SAMPLING) )
        {
          String[] vals= e.getValue().split(",");
          for(String value : vals)
          {
            value= value.trim();
            if( !value.equals(SAMPLING_CONST_FREQUENCY) && !value.equals(SAMPLING_SAMPLE_ON_SET) )
              throw new ParserException("Unknown sampling type " + value);
          }
        }
      }
      else
        throw new ParserException("Unknown option '" + e.getKey() + "'");
    }
  }
  
  public boolean isFixedLength()
  {
    // System.out.println("'" + values.get("length") + "'");
    return values.get("length").equals("fixed");
  }
  
  public boolean isOptionalLength()
  {
    // System.out.println("'" + values.get("length") + "'");
    return values.get("length").equals("optional");
  }
  
  public boolean isSetSessionFieldsUsingValues()
  {
    if( !values.containsKey(FIELD_SESSION_INSERT) )
      return false;
    
    return values.get(FIELD_SESSION_INSERT).equals(POSITIVE);
  }
  
  public boolean isUseSensorNr()
  {
    if( !values.containsKey(USE_SENSOR_NR) )
      return false;
    
    return values.get(USE_SENSOR_NR).equals(POSITIVE);
  }

  /**
   * Get the types of sampling for this sensor. If no types are specified
   *  the default value "sampling using a constant frequency" will be returned.
   * @return
   */
  public String[] samplingTypes()
  {
    if( !values.containsKey(SAMPLING) )
      return new String[]{SAMPLING_CONST_FREQUENCY};
    else 
    {
      String[] vals= values.get(SAMPLING).split(",");
      for(int i= 0; i < vals.length; i++)
        vals[i]= vals[i].trim();
      return vals;
    }
  }
  
  public String getOption(String name)
  {
  	return values.get(name);
  }
  
  public String debug()
  {
  	StringBuilder ret = new StringBuilder("Options: [");
  	boolean comma = false;
  	for(Entry<String, String> e : values.entrySet())
  	{
  		if(comma) ret.append(",");
  		ret.append(e.getKey()).append(":").append(e.getValue());
  		comma = true;
  	}
  	ret.append("]");

  	return ret.toString();
  }
}
