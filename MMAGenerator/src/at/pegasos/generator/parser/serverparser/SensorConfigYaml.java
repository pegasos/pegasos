package at.pegasos.generator.parser.serverparser;

import org.yaml.snakeyaml.*;

import java.io.*;
import java.util.*;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
public class SensorConfigYaml {

  private final Reader reader;
  private final Sensor sensor;
  private Map<String, Object> data;

  public SensorConfigYaml(String filename, Sensor sensor) throws FileNotFoundException
  {
    FileReader inputStream = new FileReader(filename);
    reader = new BufferedReader(inputStream);
    this.sensor = sensor;
  }

  public SensorConfigYaml(Reader reader, Sensor sensor)
  {
    this.reader = reader;
    this.sensor = sensor;
  }

  @SuppressWarnings("unchecked")
  public void parse() throws ParserException
  {
    Yaml yaml = new Yaml();

    Object o = yaml.load(reader);
    log.debug("{} {}", o, o.getClass());
    if( !(o instanceof Map) )
      throw new ParserException("Failed to parse : " + sensor.fileName + "/" + reader);

    data = (Map<String, Object>) o;

    if( sensor.isGenerate() )
    {
      log.debug("Fields: {} {}", data.get("fields"), data.get("fields").getClass());
      if( !(data.get("fields") instanceof Map) )
        throw new ParserException("Wrong format for : " + sensor.fileName + "/" + reader + " --> fields needs to be a mapping");

      parseFields();
    }
    else
    {
      // we can optionally specify fields
      if( data.get("fields") != null )
      {
        log.debug("Fields: {} {}", data.get("fields"), data.get("fields").getClass());
        if( !(data.get("fields") instanceof Map) )
          throw new ParserException("Wrong format for : " + sensor.fileName + "/" + reader + " --> fields needs to be a mapping");
        parseFields();
      }

      if( !data.containsKey("className") )
        throw new ParserException("Need to specify which class is used for parsing data");
      sensor.className = (String) data.get("className");
    }
    parseOptions();

    if( data.containsKey("database") )
    {
      sensor.setDatabaseDefintion((String) data.get("database"));
    }

    if( data.containsKey("convertMethod") )
    {
      sensor.setConverterMethod((String) data.get("convertMethod"));
    }

    if( data.containsKey("defaults") )
    {
      if( !(data.get("defaults") instanceof Map) )
        throw new ParserException("Wrong format for : " + sensor.fileName + "/" + reader + " --> defaults needs to be a mapping");

      if( ((Map) data.get("defaults")).containsKey("datasets_per_packet") )
      {
        Object datasets_per_packet = ((Map) data.get("defaults")).get("datasets_per_packet");
        assert datasets_per_packet instanceof Integer;
        // sensor.defaultDatasetsPerPacket = Integer.parseInt((String) datasets_per_packet);
        sensor.defaultDatasetsPerPacket = (Integer) datasets_per_packet;
      }
    }

    sensor.setSessionFieldsCode(new FieldSession[0]);
    sensor.setFieldsDb(new FieldDb[0]);
  }

  private void parseOptions() throws ParserException
  {
    this.sensor.setOptions(new Options(data, sensor.isGenerate()));
  }

  @SuppressWarnings("unchecked")
  private void parseFields() throws ParserException
  {
    Set<Map.Entry<String, Object>> fieldsY = ((Map<String, Object>) data.get("fields")).entrySet();
    List<Field> fields = new ArrayList<Field>(fieldsY.size());
    for(Map.Entry<String, Object> x : fieldsY)
    {
      Map<String, Object> f = (Map<String, Object>) x.getValue();
      log.debug("A field: {} {}", x, x.getClass());

      Field field = new Field();
      field.name = x.getKey();

      field.db_type = Type.fromString((String) f.get("dbtype"));
      field.raw_type = Type.fromString((String) f.get("rawtype"));

      field.getOptions().set(f);

      fields.add(field);
    }

    // Now store it in the sensor
    Field[] ret = new Field[fields.size()];
    ret = fields.toArray(ret);
    this.sensor.setFields(ret);
  }
}