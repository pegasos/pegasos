package at.pegasos.generator.parser.serverparser;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.aiparser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.util.*;
import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class ServerInstanceParser {
  private final static Logger log = LoggerFactory.getLogger(ServerInstanceParser.class);

  private final Map<String, ServerInstance> instances;

  private final Parameters parameters;

  public ServerInstanceParser(Parameters parameters)
  {
    instances = new HashMap<String, ServerInstance>();
    this.parameters = parameters;
  }

  /**
   * Add all instances in the specified directory
   *
   * @param directory where the server instances are located
   */
  public void parse(final Path directory)
  {
    try( DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory) )
    {
      for(Path path : directoryStream)
      {
        if( !path.getFileName().toString().startsWith(".") && path.toFile().isDirectory() )
        {
          if( Files.exists(path.resolve("instance.xml")) )
          {
            parseInstance(path);
          }
          else
          {
            log.warn("No instance.xml in " + path + " --> omitting");
          }
        }
      }
    }
    catch( ParserException e )
    {
      e.printStackTrace();
      log.error("{} {}", e.document, e.location);
      log.error("Error when parsing server instance. Stopping", e);
    }
    catch( IOException | ParserConfigurationException | SAXException e )
    {
      e.printStackTrace();
      log.error("Error when parsing server instance. Stopping", e);
    }
  }

  private void parseInstance(Path path) throws ParserConfigurationException, SAXException, IOException, ParserException
  {
    ServerInstance inst = new ServerInstance();
    int code;

    log.info("Examining {}", path.toAbsolutePath());

    inst.directory = path;

    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(path.resolve("instance.xml").toFile());
    doc.getDocumentElement().normalize();

    if( (code = XMLHelper.fillInGitInfo(doc, inst, parameters)) != 0 )
    {
      XMLHelper.logError(log, code);
      return;
    }

    inst.name = XMLHelper.getStringValueOfChild(doc.getDocumentElement(), "name");
    log.debug("Name: {} git: {}: {}", inst.name, inst.getGitRoot(parameters), inst.getGitBranch(parameters));

    inst.config = ConfigDefaults.get(inst.name, "server");


    inst.IP = XMLHelper.getStringValueOfChild(doc.getDocumentElement(), "ip");
    inst.port = XMLHelper.getIntValueOfChild(doc.getDocumentElement(), "port");

    if( inst.IP == null || inst.port == -1 )
    {
      log.error("Missing or incorrect IP/port information for instance");
      throw new ParserException("Missing or incorrect IP/port information for server instance " + inst.name);
    }

    ConfigParser cfgparser = new ConfigParser(inst.config);
    cfgparser.setDocument(doc);
    cfgparser.parse();

    if( instances.containsKey(inst.name) )
    {
      log.error("Multiple instance with name " + inst.name);
      throw new ParserException("Multiple instance with name " + inst.name);
    }

    new FeedbackModulesParser(inst).parse();
    new PostprocessingModulesParser(inst).parse();
    new ServerSensorParser(inst).parse();
    new WebParser(inst, true).parse();

    instances.put(inst.name, inst);
  }

  public Collection<ServerInstance> getInstances()
  {
    return instances.values();
  }

  public ServerInstance getInstance(String name)
  {
    return instances.get(name);
  }
}
