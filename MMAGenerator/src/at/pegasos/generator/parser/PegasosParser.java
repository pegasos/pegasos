package at.pegasos.generator.parser;

public interface PegasosParser {
  void parse() throws ParserException;
}
