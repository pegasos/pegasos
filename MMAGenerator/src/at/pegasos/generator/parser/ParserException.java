package at.pegasos.generator.parser;

@SuppressWarnings("serial")
public class ParserException extends Exception {
  private static final long serialVersionUID= -231645349410862278L;

  public String document;
  public String location;

  public ParserException(String reason)
  {
    super(reason);
  }

  public ParserException(String reason, String document, String location)
  {
    super(reason);
    this.document = document;
    this.location = location;
  }

  public ParserException(Exception e)
  {
    super(e);
  }
}
