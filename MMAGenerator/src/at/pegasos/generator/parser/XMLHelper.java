package at.pegasos.generator.parser;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.token.*;
import org.slf4j.*;
import org.w3c.dom.*;

public class XMLHelper {
  public static final int WRONG_NUMBER_SERVERS_TAGS = 4;

  /**
   * @param doc Document
   * @param name name of the element
   * @return -1 in case number is not present or invalid
   */
  public static int getIntValueOfChild(Element doc, String name)
  {
    NodeList item = doc.getElementsByTagName(name);
    if( item.getLength() == 0 )
    {
      return -1;
    }
    else
    {
      Text val = (Text) item.item(0).getChildNodes().item(0);
      if( val == null )
        return -1;

      try
      {
        return Integer.parseInt(val.getNodeValue());
      }
      catch( NumberFormatException e )
      {
        return -1;
      }

    }
  }

  /**
   * @param e some node in the document / document
   * @param name name of the tag to use
   * @param missing value to be returned when tag is missing
   * @return entry as EntryInt or `missing` if name is not present
   */
  public static EntryInt parseInt(Element e, String name, EntryInt missing)
  {
    EntryInt ret;

    NodeList item = e.getElementsByTagName(name);
    if( item.getLength() == 0 )
    {
      ret = missing;
    }
    else
    {
      Text val = (Text) item.item(0).getChildNodes().item(0);

      ret = new EntryInt(val.getNodeValue());
    }

    return ret;
  }

  /**
   * Get the value of a child element specified by name
   *
   * @param doc document
   * @param name Name of the element
   * @return null if not present or the value
   */
  public static String getStringValueOfChild(Element doc, String name)
  {
    NodeList item = doc.getElementsByTagName(name);
    if( item.getLength() != 0 )
    {
      Text val = (Text) item.item(0).getChildNodes().item(0);
      if( val != null )
        return val.getNodeValue();
    }
    return null;
  }

  /**
   * Extract the attribute/parameter of a node
   *
   * @param node         node to use
   * @param name         name of the attribute
   * @param defaultvalue default value if attribute is not set
   * @return value of the attribute or defaultValue if attribute is not set
   */
  public static String getAttribute(Node node, String name, String defaultvalue)
  {
    String att = ((Element) node).getAttribute(name);

    return (att.equals("")) ? defaultvalue : att;
  }

  /**
   * Return the value / content of a tag.
   * @param e element
   * @return null if not present or the value
   */
  public static String getStringValueOfChild(Element e)
  {
    Text val = (Text) e.getChildNodes().item(0);
    if( val != null )
      return val.getNodeValue();
    return null;
  }

  /**
   * Retrieve the value of a child element.
   *
   * @param e             entry
   * @param name          of the tag
   * @param default_value value to be returned when tag is missing
   * @return entry as EntryString or default_value if not present
   */
  public static EntryString parseString(Element e, String name, EntryString default_value)
  {
    EntryString ret;

    NodeList item = e.getElementsByTagName(name);
    if( item.getLength() == 0 )
    {
      ret = default_value;
    }
    else
    {
      Text val = (Text) item.item(0).getChildNodes().item(0);
      Element ee = (Element) item.item(0);

      if( !ee.getAttribute("id").equals("") && ee.getAttribute("id").equalsIgnoreCase("true") )
      {
        ret = new EntryString(EntryString.TYPE_ID, val.getNodeValue());
      }
      else
        ret = new EntryString(EntryString.TYPE_STRING, val.getNodeValue());
    }

    return ret;
  }

  public static int fillInGitInfo(Document doc, GitInfo data, Parameters parameters)
  {
    NodeList pList = doc.getElementsByTagName("git");
    if( pList.getLength() > 0 )
    {
      String repository = XMLHelper.getStringValueOfChild((Element) pList.item(0), "repository");
      data.setGitRoot(repository);

      String branch = XMLHelper.getStringValueOfChild((Element) pList.item(0), "branch");
      data.setGitBranch(branch);
    }

    return 0;
  }

  public static void logError(Logger log, int code)
  {
    switch( code )
    {
      case WRONG_NUMBER_SERVERS_TAGS:
        log.error("server is required in application.xml/library.xml exactly once.");
        break;

      default:
        log.error("Wrong error code");
        break;
    }
  }

  public static int fillInServersInfo(Document doc, Instance inst)
  {
    NodeList nList = doc.getElementsByTagName("servers");
    if( nList.getLength() != 1 )
    {
      return WRONG_NUMBER_SERVERS_TAGS;
    }
    else
    {
      Element e = (Element) nList.item(0);
      nList = e.getElementsByTagName("server");
      for(int i = 0; i < nList.getLength(); i++)
      {
        Node node = nList.item(i);

        String name = node.getTextContent();
        // if( !inst.servers.contains(name)) TODO: check whether it is necessary to avoid adding same name twice
        inst.servers.add(name);
      }
    }

    return 0;
  }
}
