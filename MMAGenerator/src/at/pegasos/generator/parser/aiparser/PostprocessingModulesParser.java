package at.pegasos.generator.parser.aiparser;

import java.nio.file.Files;

import at.pegasos.generator.parser.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.pegasos.generator.parser.aiparser.token.FeedbackModule;
import at.pegasos.generator.parser.token.IInstance;
import at.univie.mma.pegasos.util.XMLParser;

public class PostprocessingModulesParser extends XMLParser {
  private final static Logger log= LoggerFactory.getLogger(PostprocessingModulesParser.class);
  
  private final IInstance app;
  
  public PostprocessingModulesParser(IInstance app)
  {
    // Set file for XMLParser
    super(app.directory.resolve("ai.xml"));
    
    this.app= app;
  }

  @Override
  public void parse() throws ParserException
  {
    if( Files.exists(file) )
    {
      xml_open();
      
      NodeList nList= doc.getElementsByTagName("postprocessing");
      
      // Just for fun someone could define config entries at more than one location
      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        log.info("Parsing postprocessing");
        NodeList mList = ((Element) nList.item(temp)).getElementsByTagName("module");
        for(int p= 0; p < mList.getLength(); p++)
        {
          parseModule(mList.item(p));
        }
      }
    }
  }

  private void parseModule(Node item) throws ParserException
  {
    Element e= (Element) item;
    
    app.postprocessingmodules.add(new FeedbackModule(e));
  }
}
