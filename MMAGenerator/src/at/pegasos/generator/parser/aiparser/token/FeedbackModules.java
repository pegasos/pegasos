package at.pegasos.generator.parser.aiparser.token;

import java.util.ArrayList;
import java.util.List;

public class FeedbackModules {
  public List<FeedbackModule> modules;

  public FeedbackModules()
  {
    modules= new ArrayList<FeedbackModule>();
  }

  public void add(FeedbackModule module)
  {
    modules.add(module);
  }

  public void merge(FeedbackModules feedbackmodules)
  {
    modules.addAll(feedbackmodules.modules);
  }
}
