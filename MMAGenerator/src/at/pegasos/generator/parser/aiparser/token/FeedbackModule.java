package at.pegasos.generator.parser.aiparser.token;

import at.pegasos.generator.parser.*;
import org.w3c.dom.Element;

import at.pegasos.generator.parser.XMLHelper;

public class FeedbackModule {

  public final int param0;
  public final int param1;
  public final int param2;

  public final String clasz;

  public FeedbackModule(int param0, String clasz)
  {
    this.param0 = param0;
    this.param1 = -1;
    this.param2 = -1;

    this.clasz = clasz;
  }

  public FeedbackModule(Element e) throws ParserException
  {
    // param0="aaaa" param1="xxx" param2
    String param0= e.getAttribute("param0");
    String param1= e.getAttribute("param1");
    String param2= e.getAttribute("param2");

    if( !param0.equals("") )
      this.param0= Integer.parseInt(param0);
    else
      throw new ParserException("Need to specify at least on param: " + e.toString());
    if( !param1.equals("") )
      this.param1= Integer.parseInt(param1);
    else
      this.param1= -1;
    if( !param2.equals("") )
      this.param2= Integer.parseInt(param2);
    else
      this.param2= -1;

    clasz= XMLHelper.getStringValueOfChild(e);
  }
}