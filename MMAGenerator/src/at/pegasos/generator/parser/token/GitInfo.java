package at.pegasos.generator.parser.token;

import at.pegasos.generator.Parameters;

public interface GitInfo {
  void setGitRoot(String git_root);
  String getGitRoot(Parameters parameters);
  String getGitRootValue();

  void setGitBranch(String git_branch);
  String getGitBranch(Parameters parameters);
  String getGitBranchValue();
}
