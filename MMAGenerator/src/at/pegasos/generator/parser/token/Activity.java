package at.pegasos.generator.parser.token;

public abstract class Activity {
  public int param0;
  public int param1;
  public int param2;

  public Activity()
  {
    param0 = param1 = param2 = -1;
  }

  public int hashCode()
  {
    return (param2 != -1 ? param2 * 10000 : 0 ) + (param1 != -1 ? param1 * 100 : 0 ) + (param0 != -1 ? param0 : 0 );
  }

  public int getLevel()
  {
    if( param2 != -1 )
      return 3;
    else if ( param1 != -1 )
      return 2;
    else
      return 1;
  }
}
