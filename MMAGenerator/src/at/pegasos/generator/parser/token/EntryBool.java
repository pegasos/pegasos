package at.pegasos.generator.parser.token;

import at.pegasos.generator.parser.*;
import org.w3c.dom.Element;

/**
 * A attribute/entry representing a type
 */
public class EntryBool {
  
  private boolean val;
  
  /**
   * Create a new EntryBool object from a given node/element for the attribute. The attribute is required
   * @param element
   * @param attribute_name
   * @return
   * @throws ParserException if value is no boolean value or no attribute value is set
   */
  public static EntryBool fromAttribute(Element element, String attribute_name)  throws ParserException
  {
    return fromAttribute(element, attribute_name, false, false);
  }

  /**
   * Create a new EntryBool object from a given node/element for the attribute. 
   * If no value is specified the default_value is used. 
   * @param element
   * @param attribute_name
   * @param default_value
   * @return
   * @throws ParserException
   */
  public static EntryBool fromAttribute(Element element, String attribute_name, boolean default_value)  throws ParserException
  {
    return fromAttribute(element, attribute_name, true, default_value);
  }
  
  private static EntryBool fromAttribute(Element element, String attribute_name, boolean default_allowed, boolean default_value) throws ParserException
  {
    EntryBool ret= new EntryBool();
    String val= element.getAttribute(attribute_name);
    
    val= val.trim().toLowerCase();
    
    if( val.equals("true") || val.equals("t") )
    {
      ret.val= true;
    }
    else if( val.equals("false") || val.equals("f") )
    {
      ret.val= false;
    }
    else if( default_allowed && val.equals("") )
    {
      ret.val= default_value;
    }
    else
      throw new ParserException("Unknown boolean '" + element.getAttribute(attribute_name) + "'");
    
    return ret;
  }
  
  public static EntryBool fromString(String value, boolean default_value) throws ParserException
  {
    EntryBool ret= new EntryBool();
    
    if( value == null )
    {
      ret.val= default_value;
      return ret;
    }
    
    String val= value.trim().toLowerCase();
    
    if( val.equals("true") || val.equals("t") )
    {
      ret.val= true;
    }
    else if( val.equals("false") || val.equals("f") )
    {
      ret.val= false;
    }
    else if( val.equals("") )
    {
      ret.val= default_value;
    }
    else
      throw new ParserException("Unknown boolean '" + value + "'");
    
    return ret;
  }
  
  public boolean isTrue()
  {
    return val;
  }
}
