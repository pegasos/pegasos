package at.pegasos.generator.parser.token;

import at.pegasos.generator.parser.*;
import org.w3c.dom.*;

/**
 * A attribute/entry representing a type
 */
public class EntryType {
  
  private enum TYPE_GENERAL {
    TYPE_NUMERIC,
    TYPE_STRING,
    TYPE_GENERAL
  };
  private TYPE_GENERAL type;
  
  private enum TYPE_EXACT {
    TYPE_EXACT_INTEGER,
    TYPE_EXACT_STRING,
    TYPE_EXACT_GENERAL
  };
  TYPE_EXACT type_exact;

  public static EntryType fromAttribute(Element element, String attribute_name) throws ParserException
  {
    String txt= element.getAttribute(attribute_name);
    
    return fromString(txt);
  }
  
  public static EntryType fromString(String txt) throws ParserException
  {
    EntryType ret= new EntryType();
    
    String txth= txt.trim().toLowerCase();
    
    if( txth.equals("string") )
    {
      ret.type= TYPE_GENERAL.TYPE_STRING;
      ret.type_exact= TYPE_EXACT.TYPE_EXACT_STRING;
    }
    else if( txth.equals("int") )
    {
      ret.type= TYPE_GENERAL.TYPE_NUMERIC;
      ret.type_exact= TYPE_EXACT.TYPE_EXACT_INTEGER;
    }
    else if( txth.equals("general") )
    {
      ret.type= TYPE_GENERAL.TYPE_GENERAL;
      ret.type_exact= TYPE_EXACT.TYPE_EXACT_GENERAL;
    }
    else
      throw new ParserException("Unknown type '" + txt + "'");
    
    return ret;
  }
  
  public boolean isNumeric()
  {
    return type == TYPE_GENERAL.TYPE_NUMERIC;
  }
  
  public boolean isText()
  {
    return type == TYPE_GENERAL.TYPE_STRING;
  }

  public boolean isInteger()
  {
    return type_exact == TYPE_EXACT.TYPE_EXACT_INTEGER;
  }

  public String toJavaType()
  {
    switch( type_exact )
    {
      case TYPE_EXACT_INTEGER:
        return "int";
      case TYPE_EXACT_STRING:
        return "String";
      case TYPE_EXACT_GENERAL:
        return "Object";
      default:
        return ""; //As this can not happen.
    }
  }
}
