package at.pegasos.generator.parser.token;

import java.nio.file.Path;

import at.pegasos.generator.parser.aiparser.token.FeedbackModules;
import at.pegasos.generator.parser.webparser.token.Web;

public abstract class IInstance {
  
  /**
   * Path where the instance is located
   */
  public Path directory;
  
  /**
   * Feedbackmodules declared by this instance
   */
  public FeedbackModules feedbackmodules= new FeedbackModules();

  /**
   * Postprocessing-modules declared by this instance
   */
  public FeedbackModules postprocessingmodules= new FeedbackModules();
  
  /**
   * The web configuration of this app, library, server
   */
  protected Web web;

  public Web getWeb()
  {
    return web;
  }
}
