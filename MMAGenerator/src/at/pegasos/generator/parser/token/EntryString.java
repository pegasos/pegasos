package at.pegasos.generator.parser.token;

public class EntryString {
  public static final int TYPE_ID= 1;
  public static final int TYPE_STRING= 2;
  
  private final String internal;
  private final String raw;
  private final boolean type_id;
  
  public EntryString(int type, String value)
  {
    raw= value;
    if( type == TYPE_ID )
    {
      internal= value;
      type_id= true;
    }
    else
    {
      internal= "\"" + value + "\"";
      type_id= false;
    }
  }
  
  /**
   * Returns whether the string represents an ID (i.e. does not have quotes)
   * 
   * @return ture if it is of type id
   */
  public boolean isId()
  {
    return type_id;
  }
  
  public String toString()
  {
    return internal;
  }
  
  public String raw()
  {
    return raw;
  }

  public EntryString copy()
  {
    return new EntryString(type_id ? TYPE_ID : TYPE_STRING, new String(raw));
  }
}
