package at.pegasos.generator.parser.token;

import java.util.ArrayList;
import java.util.List;

import at.pegasos.generator.parser.aiparser.token.FeedbackModules;
import at.pegasos.generator.Parameters;

public class Instance extends IInstance implements GitInfo {
  public String name;
  
  public List<String> servers;
  
  public Instance() {
    this.servers= new ArrayList<String>();
    this.feedbackmodules= new FeedbackModules();
  }
  
  public Instance(Instance i)
  {
    // IInstance members
    this.directory= i.directory;
    this.feedbackmodules= i.feedbackmodules;
    this.postprocessingmodules= i.postprocessingmodules;
    
    // GitInfo
    this.name= i.name;
    this.servers= i.servers;
    this.git_root= i.git_root;
    this.git_branch= i.git_branch;
  }
  
  /**
   * Where the source code is located
   */
  private String git_root;
  
  /**
   * which branch to check out
   */
  private String git_branch;

  /**
   * Get the git root parameter as specified by the configuration
   */
  @Override
  public String getGitRootValue()
  {
    return git_root;
  }

  /**
   * Get the git root parameter. This is the value as specified by the general configuration parameters
   */
  @Override
  public String getGitRoot(Parameters parameters)
  {
    if( parameters.forceRepository )
    {
      return parameters.repository;
    }
    if( git_root == null )
    {
      return parameters.repository;
    }
    return git_root;
  }

  @Override
  public void setGitRoot(String git_root)
  {
    this.git_root = git_root;
  }

  @Override
  public String getGitBranch(Parameters parameters)
  {
    if( git_branch == null )
    {
      return parameters.branch;
    }
    return git_branch;
  }

  @Override
  public String getGitBranchValue()
  {
    return git_branch;
  }

  @Override
  public void setGitBranch(String git_branch)
  {
    this.git_branch = git_branch;
  }
}
