package at.pegasos.generator.parser.token;

public class EntryInt {
  private String internal;
  
  public EntryInt(String value)
  {
    internal= value;
  }
  
  public String toString()
  {
    return internal;
  }
}
