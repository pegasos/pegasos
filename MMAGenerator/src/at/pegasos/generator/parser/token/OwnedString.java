package at.pegasos.generator.parser.token;

public class OwnedString {
  public final String string;
  public final Instance owner;

  public OwnedString(Instance owner, String string)
  {
    this.owner = owner;
    this.string = string;
  }
}
