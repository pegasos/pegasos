package at.pegasos.generator.parser.token;

import java.util.ArrayList;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.WebParser;
import at.pegasos.generator.parser.webparser.token.Web;
import at.pegasos.generator.Parameters;
import at.pegasos.generator.parser.serverparser.token.Sensor;
import at.pegasos.generator.parser.backend.token.Commands;
import lombok.*;

@ToString
public class ServerInstance extends IInstance implements GitInfo {

  /**
   * Where the source code is located
   */
  public String git_root;

  /**
   * which branch to check out
   */
  public String git_branch; 

  /**
   * IP-Address of the instance
   */
  public String IP;

  /**
   * Port number of the instance
   */
  public int port;

  /**
   * ID/Name of the instance
   */
  public String name;

  /**
   * Commands the server-instance is using.
   * Applications can add additional commands to the set of commands.
   */
  public Commands commands;
  
  /**
   * Sensors declared for this instance
   */
  private final ArrayList<Sensor> sensors;

  public Config config;

  public ServerInstance()
  {
    sensors= new ArrayList<Sensor>();
    web= new Web();
  }

  /**
   * Get the git root parameter as specified by the configuration
   */
  @Override
  public String getGitRootValue()
  {
    return git_root;
  }

  /**
   * Get the git root parameter. This is the value as specified by the general configuration parameters
   */
  @Override
  public String getGitRoot(Parameters parameters)
  {
    if( parameters.forceRepository )
    {
      return parameters.repository;
    }
    if( git_root == null )
    {
      return parameters.repository;
    }
    return git_root;
  }

  @Override
  public String getGitBranch(Parameters parameters)
  {
    if( git_branch == null )
    {
      return parameters.branch;
    }
    return git_branch;
  }

  @Override
  public String getGitBranchValue()
  {
    return git_branch;
  }

  @Override
  public void setGitRoot(String git_root)
  {
    this.git_root = git_root;
  }

  @Override
  public void setGitBranch(String git_branch)
  {
    this.git_branch = git_branch;
  }

  public void addSensor(Sensor sensor)
  {
    this.sensors.add(sensor);
  }

  public final ArrayList<Sensor> getSensors()
  {
    return this.sensors;
  }
  
  /**
   * Add relevant parts of an app or library to this server 
   * @param inst app to add
   * @throws ParserException when there is a problem :)
   */
  public void addApp(Application inst) throws ParserException
  {
    this.web.add(inst.getWeb());
    WebParser.add(this.web, inst.getWeb());
  }

  public boolean generateDocker()
  {
    return true;
  }
}
