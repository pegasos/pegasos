package at.pegasos.generator.parser.webparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import javax.annotation.*;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
@PegasosWebParser(name = "Modules",
        writer = ModulesWriter.class,
        order = 1)
public class ModulesParser extends PegasosWebXmlParser {

  public ModulesParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);

    if( defaults )
    {
      assert( inst != null );
      inst.getWeb().modules.addModule(new Modules.Module("ActivitiesModule", "activities.module", "activities/", true));
      inst.getWeb().modules.addModule(new Modules.Module("AdminModule", "admin.module", "admin/", true));
      inst.getWeb().modules.addModule(new Modules.Module("HomeModule", "home.module", "home/", true));
      inst.getWeb().modules.addModule(new Modules.Module("LiveModule", "live.module", "live/", true));
      inst.getWeb().modules.addModule(new Modules.Module("ReportsModule", "reports.module", "reports/", true));
      inst.getWeb().modules.addModule(new Modules.Module("TeamModule", "team.module", "team/", true));
      inst.getWeb().modules.addModule(new Modules.Module("UserModule", "user.module", "user/", true));
    }
  }

  @Override
  protected void concreteParse()
  {
    NodeList nList= doc.getElementsByTagName("modules");

    for(int temp= 0; temp < nList.getLength(); temp++)
    {
      log.info("Parsing modules");

      NodeList mList= ((Element) nList.item(temp)).getElementsByTagName("module");
      for(int p= 0; p < mList.getLength(); p++)
      {
        Node route= mList.item(p);
        String name= XMLHelper.getAttribute(route, "name", null);
        String path= XMLHelper.getAttribute(route, "path", null);
        String file= XMLHelper.getAttribute(route, "file", null);

        log.debug("Adding module {} f:{} p:{}", name, file, path);

        assert inst != null;
        inst.getWeb().addModule(name, file, path);
      }
    }
  }

  @Override
  protected void onFileMissing()
  {
    fileMissingDefault();
  }

  @Override
  public void add(Web target, Web source)
  {
    // nothing to do here, as we are part of the big merge method
  }
}
