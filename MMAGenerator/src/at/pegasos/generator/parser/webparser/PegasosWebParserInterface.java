package at.pegasos.generator.parser.webparser;

import at.pegasos.generator.parser.PegasosParser;

/**
 * Parsers for the web-interface part of a server or an application parsing the source code of the app.
 * <br/><br/>
 * A general parser needs to implement the following constructor: <code>PegasosWebParserInterface(IInstance inst, Path directory)</code>.
 * Where directory is the path to the source code (including all code added by other apps).
 */
public interface PegasosWebParserInterface extends PegasosParser {

  // PegasosWebParserInterface(@Nullable IInstance instance, Boolean defaults);
}
