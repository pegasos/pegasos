package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import javax.annotation.*;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
@PegasosWebParser(name = "Routes",
        writer = RoutesWriter.class,
        order = 26)
public class RoutesParser extends PegasosWebXmlParser {

  public RoutesParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  public void addDefaultRoutes()
  {
    assert inst != null;

    if( inst.getWeb().default_routes_added )
      return;
    inst.getWeb().default_routes_added = true;

    inst.getWeb().addRoute(new ModuleRoute("home", "HomeModule"));
    inst.getWeb().addRoute(new ComponentRoute("login", "LoginComponent"));
    inst.getWeb().addRoute(new ComponentRoute("logout", "LoginComponent"));
    inst.getWeb().addRoute(new ComponentRoute("register", "LoginComponent"));
    inst.getWeb().addRoute(new ComponentRoute("finishregistration", "LoginComponent"));
    inst.getWeb().addRoute(new ModuleRoute("team", "TeamModule"));
    inst.getWeb().addRoute(new ModuleRoute("tagebuch", "ActivitiesModule"));
    inst.getWeb().addRoute(new ModuleRoute("reports", "ReportsModule"));
    inst.getWeb().addRoute(new ModuleRoute("user", "UserModule"));
    inst.getWeb().addRoute(new ModuleRoute("live", "LiveModule"));
    inst.getWeb().addRoute(new ModuleRoute("admin", "AdminModule", Collections.singletonList("MenuGuard")));
  }

  @Override
  protected void concreteParse() throws ParserException
  {
    assert inst != null;
    NodeList nList = doc.getElementsByTagName("routes");

    for(int temp = 0; temp < nList.getLength(); temp++)
    {
      log.info("Parsing routes");

      boolean useDefault = Boolean.parseBoolean(XMLHelper.getAttribute(nList.item(temp), "default", "false"));
      if( useDefault )
      {
        log.info("Adding default");

        addDefaultRoutes();
      }

      NodeList mList = ((Element) nList.item(temp)).getElementsByTagName("route");
      for(int p = 0; p < mList.getLength(); p++)
      {
        Node route = mList.item(p);
        String path = XMLHelper.getAttribute(route, "path", null);
        String component = XMLHelper.getAttribute(route, "component", null);
        String module = XMLHelper.getAttribute(route, "module", null);
        String guards = XMLHelper.getAttribute(route, "canActivate", null);
        Route r;
        if( component != null && module == null )
        {
          r = new ComponentRoute();
          ((ComponentRoute) r).component = component;
        }
        else if( component == null && module != null )
        {
          r = new ModuleRoute();
          ((ModuleRoute) r).module = module;
        }
        else
          throw new ParserException("Need to specify module or component for route");

        r.path = path;
        if( guards != null )
        {
          r.guards.addAll(new ArrayList<>(Arrays.asList(guards.split(","))));
        }
        inst.getWeb().addRoute(r);
      }
    }
  }

  @Override
  protected void onFileMissing()
  {
    fileMissingDefault();
  }

  @Override
  public void add(Web target, Web source)
  {
    // nothing to do here, as we are part of the big merge method
  }
}
