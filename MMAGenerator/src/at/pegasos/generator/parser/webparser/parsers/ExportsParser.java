package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import javax.annotation.*;
import java.util.*;

@Slf4j @PegasosWebParser(name = "Exports",
    writer = ExportsWriter.class,
    order = 1)
public class ExportsParser
    extends PegasosWebXmlParser {

  private Exports exports;

  public ExportsParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  @Override protected void concreteParse() throws ParserException
  {
    assert inst != null;

    exports = inst.getWeb().exports;

    NodeList nList = doc.getElementsByTagName("exports");

    // Just for fun someone could define config entries at more than one location
    for(int temp= 0; temp < nList.getLength(); temp++)
    {
      NodeList mList= ((Element) nList.item(temp)).getElementsByTagName("type");
      for(int p= 0; p < mList.getLength(); p++)
      {
        parseType(mList.item(p));
      }

      mList= ((Element) nList.item(temp)).getElementsByTagName("activity");
      for(int p= 0; p < mList.getLength(); p++)
      {
        parseModule(mList.item(p));
      }
    }
  }

  @Override protected void onFileMissing()
  {
    fileMissingDefault();
  }

  private void parseType(Node item) throws ParserException
  {
    assert inst != null;

    Element e = (Element) item;

    String typeName = e.getAttribute("name");
    String className = e.getAttribute("class");
    inst.getWeb().exports.addType(new Exports.ExportType(typeName, className));
  }

  private void parseModule(Node item) throws ParserException
  {
    assert inst != null;

    Element e = (Element) item;

    String param0 = e.getAttribute("param0");
    String param1 = e.getAttribute("param1");
    String param2 = e.getAttribute("param2");

    Exports.ExportActivity node = exports.getAvailNode(param0, param1, param2);
    // node.adddefault = adddefault;
    /*if( overwrite && node.overwrite != null && node.overwrite )
    {
      log.error("Tried to overwrite {}", node);
      throw new ParserException("Illegal specification for activity chart. Tried to overwrite activity which has already been marked as overwritten");
    }
    if( overwrite )
    {
      node.overwrite = true;
      node.chartsPre = new ArrayList<>();
      node.chartsPost = new ArrayList<>();
    }*/

    if( node.exports == null )
      node.exports = new ArrayList<>();

    NodeList mList = ((Element) item).getElementsByTagName("export");
    for(int p= 0; p < mList.getLength(); p++)
    {
      Element c = (Element) mList.item(p);
      String typeName = c.getAttribute("type");

      // check if this export has not been added already
      if( node.exports.stream().anyMatch(export -> export.typeName.equals(typeName)) )
      {
        log.warn("Tried to add duplicat export {} for activity {}", typeName, node);
      }
      else
        node.exports.add(new Exports.Export(typeName));
    }
  }

  @Override public void add(Web target, Web source)
  {
    target.exports.add(source.exports);
  }
}
