package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.Activity;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import javax.annotation.*;
import java.util.*;

@Slf4j
@PegasosWebParser(name = "ActivityCharts",
    order = 30,
    writer = ActivityChartsWriter.class)
public class ActivityChartsParser extends PegasosWebXmlParser {

  public static final int DEFAULT_ORDER = 50;
  private Charts charts;

  public ActivityChartsParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  @Override protected void concreteParse() throws ParserException
  {
    assert inst != null;

    charts = inst.getWeb().charts;

    NodeList nList = doc.getElementsByTagName("activitycharts");

    // Just for fun someone could define config entries at more than one location
    for(int temp = 0; temp < nList.getLength(); temp++)
    {
      NodeList mList = ((Element) nList.item(temp)).getElementsByTagName("activity");
      for(int p = 0; p < mList.getLength(); p++)
      {
        parseActivityChart(mList.item(p));
      }
    }

    if( nList.getLength() > 0 )
    {
      log.debug("New activity tree");
      inst.getWeb().charts.debugTree();
    }
  }

  @Override protected void onFileMissing()
  {
    fileMissingDefault();
  }

  private void parseActivityChart(Node item) throws ParserException
  {
    assert inst != null;

    Element e = (Element) item;

    String param0 = e.getAttribute("param0");
    String param1 = e.getAttribute("param1");
    String param2 = e.getAttribute("param2");
    boolean adddefault = e.getAttribute("default").equalsIgnoreCase("true");
    boolean overwrite = e.getAttribute("overwrite").equalsIgnoreCase("true");

    log.debug("Parsing activity {}/{}/{} adddefault:{}. Before", param0, param1, param2, adddefault);
    charts.debugTree();
    Activity node = charts.getAvailNode(param0, param1, param2);
    log.debug("After");
    charts.debugTree();
    log.debug("---");
    node.adddefault = adddefault;
    if( overwrite && node.overwrite != null && node.overwrite )
    {
      log.error("Tried to overwrite {}", node);
      throw new ParserException(
          "Illegal specification for activity chart. Tried to overwrite activity which has already been marked as overwritten");
    }
    if( overwrite )
    {
      node.overwrite = true;
      node.chartsPre = new ArrayList<>();
      node.chartsPost = new ArrayList<>();
    }

    NodeList mList = ((Element) item).getElementsByTagName("chart");
    for(int p = 0; p < mList.getLength(); p++)
    {
      Element c = (Element) mList.item(p);
      String name = XMLHelper.getStringValueOfChild(c);
      String sorder = c.getAttribute("order");
      boolean post = c.getAttribute("post").equalsIgnoreCase("true");
      int order = sorder.equals("") ? DEFAULT_ORDER : Integer.parseInt(sorder);
      if( post )
      {
        node.chartsPost.add(new Activity.Chart(name, order, !sorder.equals("")));
      }
      else
      {
        node.chartsPre.add(new Activity.Chart(name, order, !sorder.equals("")));
      }
    }
  }

  @Override public void add(Web target, Web source)
  {
    // currently handled in Web --> Charts.addOther
  }
}
