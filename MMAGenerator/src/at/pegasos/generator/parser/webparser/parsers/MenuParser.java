package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import javax.annotation.*;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
@PegasosWebParser(name = "Menu",
        writer = MenuWriter.class,
        order = 25)
public class MenuParser extends PegasosWebXmlParser {

  public MenuParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);

    if( defaults )
    {
      assert( inst != null );
      inst.getWeb().addMenuEntry(new MenuEntry("user/profile", "mainMenu_personalData", false, 1));
      inst.getWeb().addMenuEntry(new MenuEntry("team", "mainMenu_team", false, 20));
      inst.getWeb().addMenuEntry(new MenuEntry("live", "mainMenu_currentEvents", false, 30));
      inst.getWeb().addMenuEntry(new MenuEntry("tagebuch", "mainMenu_diary", false, 40));
      inst.getWeb().addMenuEntry(new MenuEntry("reports", "mainMenu_reports", false, 50));
      inst.getWeb().addMenuEntry(new MenuEntry("hilfe", "mainMenu_help", true, 60));
      inst.getWeb().addMenuEntry(new MenuEntry("impressum", "mainMenu_imprint", true, 70));
      inst.getWeb().addMenuEntry(new MenuEntry("admin", "mainMenu_admin", false, new String[] {"ADMIN"}, 99));
    }
  }

  @Override
  protected void concreteParse() throws ParserException
  {
    assert inst != null;
    NodeList nList= doc.getElementsByTagName("menu");

    for(int temp= 0; temp < nList.getLength(); temp++)
    {
      log.info("Parsing menu entries");

      NodeList mList= ((Element) nList.item(temp)).getElementsByTagName("entry");
      for(int p= 0; p < mList.getLength(); p++)
      {
        Node e= mList.item(p);
        String action= XMLHelper.getAttribute(e, "action", null);
        String name= XMLHelper.getAttribute(e, "name", null);

        MenuEntry entry= new MenuEntry(action, name);

        String h= XMLHelper.getAttribute(e, "order", null);
        if( h != null )
        {
          entry.order= Integer.parseInt(h);
        }

        h= XMLHelper.getAttribute(e, "public", null);
        if( h != null )
        {
          entry.isPublic= Boolean.valueOf(h);
        }

        h= XMLHelper.getAttribute(e, "requiredRoles", null);
        if( h != null )
        {
          entry.requiredRoles.addAll(Arrays.asList(h.split(",")));
        }

        h = XMLHelper.getAttribute(e, "parent", null);
        if( h != null )
        {
          inst.getWeb().addMenuEntry(h, entry);
        }
        else
        {
          inst.getWeb().addMenuEntry(entry);
        }
      }
    }
  }

  @Override
  protected void onFileMissing()
  {
    fileMissingDefault();
  }

  @Override
  public void add(Web target, Web source)
  {
    // target.menu.addAll(source.menu);
    if( source.menu.size() > 0 )
    {
      log.trace("Adding source menu: {} to {}", source.menu, target.menu);

      source.menu.forEach(target::addMenuEntry);
    }
  }
}
