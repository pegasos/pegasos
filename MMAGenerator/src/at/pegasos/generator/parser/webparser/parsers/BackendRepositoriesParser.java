package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;

import java.io.*;
import java.nio.file.*;

@Slf4j
@PegasosWebParser(name="Repositories") //We want the default order and have no writer
public class BackendRepositoriesParser implements PegasosWebParserInterface {
  protected IInstance inst;

  /**
   * Directory in which the admin files will be searched
   */
  private final Path webDir;

  public BackendRepositoriesParser(IInstance inst, Path directory)
  {
    this.inst = inst;

    webDir = directory.resolve("server").resolve("web").resolve("backend").resolve("src").resolve("main").resolve("java").resolve("at")
        .resolve("pegasos").resolve("server").resolve("frontback").resolve("repositories");
  }

  @Override
  public void parse() throws ParserException
  {
    if( !Files.exists(webDir) )
      return;
    assert(Files.exists(webDir));
    try( DirectoryStream<Path> directoryStream= Files.newDirectoryStream(webDir) )
    {
      for(Path path : directoryStream)
      {
        if( path.toFile().isFile() && path.toFile().getName().endsWith(".java") )
        {
          inst.getWeb().ensurePresent(Repositories.TAG, Repositories.class);
          Repositories repositories = (Repositories) inst.getWeb().get(Repositories.TAG);
          String fileName = path.getFileName().toString();
          fileName = fileName.substring(0, fileName.length() - 5);
          repositories.repositories.add(new Repositories.Repository(fileName));
        }
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e);
    }
  }
}
