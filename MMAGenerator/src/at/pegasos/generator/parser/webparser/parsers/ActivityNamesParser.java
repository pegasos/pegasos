package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import javax.annotation.*;

@Slf4j @PegasosWebParser(name = "ActivityNames",
    writer = ActivityNamesWriter.class,
    order = 1)
public class ActivityNamesParser
    extends PegasosWebXmlParser {

  public ActivityNamesParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  @Override protected void concreteParse() throws ParserException
  {
    assert inst != null;

    NodeList nList = doc.getElementsByTagName("names");

    // Just for fun someone could define config entries at more than one location
    for(int temp = 0; temp < nList.getLength(); temp++)
    {
      log.debug("Parsing names");
      NodeList mList = ((Element) nList.item(temp)).getElementsByTagName("name");
      for(int p = 0; p < mList.getLength(); p++)
      {
        log.debug("Parsing name");
        parseModule(mList.item(p));
      }
    }
  }

  @Override protected void onFileMissing()
  {
    fileMissingDefault();
  }

  private void parseModule(Node item) throws ParserException
  {
    assert inst != null;

    Element e = (Element) item;

    String param0 = e.getAttribute("param0");
    String param1 = e.getAttribute("param1");
    String param2 = e.getAttribute("param2");

    if( !param0.equals("") )
    {
      if( !param1.equals("") )
      {
        if( !param2.equals("") )
        {
          inst.getWeb().activitynames.addName(Integer.parseInt(param0), Integer.parseInt(param1), Integer.parseInt(param2),
              XMLHelper.getStringValueOfChild(e));
        }
        else
        {
          inst.getWeb().activitynames.addName(Integer.parseInt(param0), Integer.parseInt(param1), XMLHelper.getStringValueOfChild(e));
        }
      }
      else
      {
        // TODO: check for param2 == ""
        inst.getWeb().activitynames.addName(Integer.parseInt(param0), XMLHelper.getStringValueOfChild(e));
      }
    }/*
    else
    {
      // TODO: throw error
    }*/
  }

  @Override public void add(Web target, Web source)
  {
    target.activitynames.add(source.activitynames);
  }
}
