package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import javax.annotation.*;

@Slf4j @PegasosWebParser(name = "Charts",
    order = 29, writer = ChartsWriter.class)
public class ChartsParser
    extends PegasosWebXmlParser {

  private Charts charts;

  public ChartsParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  @Override protected void concreteParse() throws ParserException
  {
    assert inst != null;

    charts = inst.getWeb().charts;

    NodeList nList= doc.getElementsByTagName("charts");

    // Just for fun someone could define config entries at more than one location
    for(int temp= 0; temp < nList.getLength(); temp++)
    {
      log.info("Parsing charts");
      NodeList mList= ((Element) nList.item(temp)).getElementsByTagName("chart");
      for(int p= 0; p < mList.getLength(); p++)
      {
        parseChart(mList.item(p));
      }
    }
  }

  @Override protected void onFileMissing()
  {
    fileMissingDefault();
  }

  private void parseChart(Node item) throws ParserException
  {
    Element e = (Element) item;

    Charts.Chart chart;

    String type = XMLHelper.getAttribute(item, "type", "dbline");

    String name = XMLHelper.getStringValueOfChild(e, "name");
    if( name == null )
      throw new ParserException("Chart without name", e.getOwnerDocument().getDocumentURI(), file.toString());

    chart = chartFromType(type, name);
    chart.parse(e, file);
    chart.default_avail = EntryBool.fromString(XMLHelper.getStringValueOfChild(e, "default"), true).isTrue();

    charts.addChart(chart);
  }

  private Charts.Chart chartFromType(String type, String name) throws ParserException
  {
    switch( type )
    {
      case "dbline":
        return new SingleLineChart(name);
      case "dbmline":
        return new MultiLineChart(name);
      case "dbmap":
        return new MapChart(name);
      case "poincare":
        return new Poincare(name);
      case "tachogram":
        return new Tachogram(name);
      case "bars":
        return new DBTableChart(name);
      case "generated":
        return new GeneratedChartType("Generated", name);
      default:
        throw new ParserException("Unkown chart type '" + type + "' in " + file);
    }
  }

  @Override public void add(Web target, Web source)
  {
    // currently handled in Web --> Charts.addOther
  }
}
