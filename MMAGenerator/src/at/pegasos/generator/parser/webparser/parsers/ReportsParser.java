package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.token.*;
import org.w3c.dom.*;

import java.util.*;

import javax.annotation.*;

import at.pegasos.generator.configurator.writer.web.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;
import lombok.extern.slf4j.*;

@Slf4j
@PegasosWebParser(name = "Reports",
        writer = ReportsWriter.class,
        order = 55)
public class ReportsParser extends PegasosWebXmlParser {

  public ReportsParser(@Nullable IInstance inst, Boolean defaults)
  {
    super(inst, defaults);
  }

  @Override
  protected void concreteParse() throws ParserException
  {
    assert inst != null;

    // Make sure web config has a reports entry
    if( inst.getWeb().get("reports") == null )
    {
      inst.getWeb().set("reports", new ArrayList<Report>());
    }

    NodeList nList = doc.getElementsByTagName("reports");

    for(int temp = 0; temp < nList.getLength(); temp++)
    {
      log.debug("Parsing reports entry");

      NodeList mList = ((Element) nList.item(temp)).getElementsByTagName("report");
      for(int p = 0; p < mList.getLength(); p++)
      {
        Node e = mList.item(p);
        String type = XMLHelper.getAttribute(e, "type", "user");
        Report report;
        switch(type)
        {
          case "user":
          case "UserValueBased":
            report = new UserValueBasedReport();
            break;
          case "MetricBasedAccumulated":
            report = new MetricBasedAccumulatedReport();
            break;
          case "MetricBasedGrouped":
            report = new MetricBasedGroupedReport();
            break;
          case "SessionValue":
            report = new SessionValueReport();
            break;
          case "metric":
          case "MetricBased":
            if( (((Element) e).getElementsByTagName("accumulate")).getLength() > 0 )
            {
              report = new MetricBasedReport();
              report.parse((Element) e);
              throw new ParserException("Accumulating parameter found for report " + report.name + " which is marked as a report without accumulation");
            }
            report = new MetricBasedReport();
            break;
          default:
            throw new ParserException("Unknown report type '" + type + "'");
        }

        report.parse((Element) e);

        if( (((Element) e).getElementsByTagName("display")).getLength() > 0 )
        {
          Display d = new Display();
          d.parse((Element) (((Element) e).getElementsByTagName("display")).item(0));
          report.display = d;
        }

        if( (((Element) e).getElementsByTagName("enhancer")).getLength() > 0 )
        {
          Element xmlen = (Element) (((Element) e).getElementsByTagName("enhancer")).item(0);
          String etype = XMLHelper.getAttribute(xmlen, "type", null);
          if( etype == null || !etype.equals("r") )
            throw new ParserException("Unknown enhancer type '" + etype + "'");
          REnhancer enhancer = new REnhancer();
          enhancer.parse(xmlen);
          report.enhancer = enhancer;
        }

        addReport(report);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private void addReport(Report report)
  {
    assert inst != null;
    ((Collection<Report>) inst.getWeb().get("reports")).add(report);
  }

  @Override
  protected void onFileMissing()
  {
    fileMissingDefault();
  }

  @SuppressWarnings("unchecked") @Override
  public void add(Web target, Web source)
  {
    // check if there are reports to add
    if( source.get("reports") == null )
      return;
    log.debug("Adding {} to {}", source.get("reports"), target.get("reports"));

    // Make sure web config has a reports entry
    if( target.get("reports") == null )
    {
      target.set("reports", new ArrayList<Report>());
    }
    ((Collection<Report>) target.get("reports")).addAll((Collection<Report>) source.get("reports"));
  }
}
