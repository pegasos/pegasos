package at.pegasos.generator.parser.webparser.parsers;

import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.regex.*;
import java.util.stream.Collectors;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@PegasosWebParser
public class AdminModulesParser implements PegasosWebParserInterface {
  private static final String PREFIX= "// admin-module:";
  // \s -> white space
  private static final String classNamePattern= ".*export[\\s]*class[\\s]*(?<name>[A-Za-z0-9_]*)[ a-zA-Z0-9]*extends[\\sa-zA-Z0-9]*AdminToolComponent.*";
  private final Path basedir;

  protected IInstance inst;

  /**
   * Directory in which the admin files will be searched
   */
  private final Path webDir;

  public AdminModulesParser(IInstance inst, Path directory)
  {
    this.inst = inst;

    webDir = directory.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app").resolve("admin").resolve("tools");
    basedir = directory.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app");
  }

  @Override
  public void parse() throws ParserException
  {
    if( !Files.exists(webDir) )
      return;
    assert(Files.exists(webDir));
    try( DirectoryStream<Path> directoryStream= Files.newDirectoryStream(webDir) )
    {
      for(Path path : directoryStream)
      {
        if( path.toFile().isFile() && path.toFile().getName().endsWith(".ts") )
        {
          BufferedReader reader= Files.newBufferedReader(path);
          String firstLine= reader.readLine();
          if( firstLine.startsWith(PREFIX) )
          {
            parseFile(path, firstLine);
          }
        }
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e);
    }
  }

  private void parseFile(Path path, String entry) throws IOException
  {
    String def= entry.substring(PREFIX.length()+1);
    // log.debug("'{}'", def);

    Pattern pattern= Pattern.compile("entry: (?<entry>['[a-zA-Z0-9 ]+'[\\s->]*]*)(, order:[\\s]*(?<order>[0-9]*))*");
    Matcher matcher= pattern.matcher(def);
    boolean matches= matcher.matches();
    log.debug("matches {}: {}", def, matches);
    if( matches )
    {
      String h= matcher.group("entry");
      // log.debug(Arrays.toString(h.split("->")));
      String[] modulePath= h.split("->");
      for(int i= 0; i < modulePath.length; i++)
      {
        String name= modulePath[i].split("'")[1];
        // log.debug(name);
        modulePath[i]= name;
      }
      int order= 1;
      if( matcher.group("order") != null )
      {
        order= Integer.parseInt(matcher.group("order"));
      }
      // log.debug("Order {}", order);

      String componentName= findComponentName(path);

      log.debug("{} {} {}", path.toString(), basedir, basedir.relativize(path).toString());

      inst.getWeb().getAdminModules().addModule(modulePath,
          basedir.relativize(path).toString(),
          componentName,
          order);
    }
  }

  private String findComponentName(Path path) throws IOException
  {
    // export class AddUserComponent extends AdminToolComponent
    List<String> lines= Files.readAllLines(path);
    final Pattern pattern= Pattern.compile(classNamePattern);
    List<String> defs= lines.stream().filter(line -> pattern.matcher(line).matches()).collect(Collectors.toList());
    if( defs.size() > 0 )
    {
      Matcher m= pattern.matcher(defs.get(0));
      if( m.matches() )
        return m.group("name");
      else
      {
        log.error("No match for {}", defs.get(0));
        return null;
      }
    }
    else
    {
      return null;
    }
  }

  public static void main(String[] args) throws ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("../server");
    IInstance inst= new Application(i);
    AdminModulesParser parser= new AdminModulesParser(inst, i.directory.resolve(".."));
    parser.parse();
    log.debug(inst.getWeb().getAdminModules().toString());
  }
}
