package at.pegasos.generator.parser.webparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;

/**
 * Parsers for the web-interface part of a server or an application parsing the web.xml.<br/><br/>
 * Each parser needs to have the following constructor: <code>PegasosWebParserInterface(@Nullable IInstance instance, Boolean defaults)</code><br>
 * Best it subclasses <code>PegasosWebXmlParser</code>
 */
public interface PegasosWebXMLParserInterface extends PegasosParser {

  /**
   * add source to target
   * @param target target web
   * @param source source web
   */void add(Web target, Web source);
}
