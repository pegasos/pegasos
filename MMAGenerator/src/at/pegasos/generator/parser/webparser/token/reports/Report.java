package at.pegasos.generator.parser.webparser.token.reports;

import org.w3c.dom.*;

import at.pegasos.generator.parser.*;
import lombok.*;

@EqualsAndHashCode
public abstract class Report {
  public String name;

  public Display display;

  public Enhancer enhancer;

  public void parse(Element item) throws ParserException
  {
    name = XMLHelper.getStringValueOfChild(item, "name");
  }

  public void write(Document document, Element e)
  {
    Element x = document.createElement("name");
    x.appendChild(document.createTextNode(name));
    e.appendChild(x);

    String type = getClass().getSimpleName();
    if( type.endsWith("Report") )
      type = type.substring(0, type.length() - 6);
    e.setAttribute("type", type);

    if( display != null )
    {
      Element d = document.createElement("display");
      display.write(document, d);
      e.appendChild(d);
    }

    if( enhancer != null )
    {
      Element d = document.createElement("enhancer");
      enhancer.write(document, d);
      e.appendChild(d);
    }
  }
}
