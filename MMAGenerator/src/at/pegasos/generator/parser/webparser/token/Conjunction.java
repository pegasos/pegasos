package at.pegasos.generator.parser.webparser.token;

public enum Conjunction {
  AND, OR
}
