package at.pegasos.generator.parser.webparser.token.reports;

import at.pegasos.generator.parser.*;
import org.w3c.dom.*;

import java.util.*;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
public class REnhancer extends Enhancer {
  public String code_resource;
  public List<Object> extractions = new ArrayList<Object>();

  // String code_external;

  @Override
  public void parse(Element item)
  {
    code_resource= XMLHelper.getStringValueOfChild(item, "code");
  }

  @Override
  public void write(Document document, Element d)
  {
    d.setAttribute("type", "r");
    Element x = document.createElement("code");
    x.appendChild(document.createTextNode(String.valueOf(code_resource)));
    d.appendChild(x);
  }
}
