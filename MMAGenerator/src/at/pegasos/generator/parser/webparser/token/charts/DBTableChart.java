package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

public class DBTableChart extends Charts.ChartWithConfig {

  private final static Collection<String> configImports;
  private final static Set<String> allowedConfigStrings;
  protected final static Set<String> quotedConfigs;
  public final static String TypeName = "Bars";

  static
  {
    configImports = new ArrayList<String>(1);
    configImports.add("{ BarsChartConfig } from './../shared/chartconfig'");
    allowedConfigStrings = new HashSet<String>();
    allowedConfigStrings.add("yaxislabel");
    allowedConfigStrings.add("filterymin");
    allowedConfigStrings.add("filterymax");
    allowedConfigStrings.add("axisType");
    allowedConfigStrings.add("tooltip_background");
    allowedConfigStrings.add("tooltip_fontsize");
    allowedConfigStrings.add("tooltip_seriespre");
    allowedConfigStrings.add("tooltip_seriespost");
    allowedConfigStrings.add("tooltip_itempre");
    allowedConfigStrings.add("tooltip_itempost");
    quotedConfigs = new HashSet<>();
    quotedConfigs.add("yaxislabel");
    quotedConfigs.add("axisType");
    quotedConfigs.add("tooltip_background");
    quotedConfigs.add("tooltip_fontsize");
    quotedConfigs.add("tooltip_seriespre");
    quotedConfigs.add("tooltip_seriespost");
    quotedConfigs.add("tooltip_itempre");
    quotedConfigs.add("tooltip_itempost");
  }

  public String type;
  public String table_name;
  public String discriminator;
  public String value_field;
  public String where_clause;

  public String colors;

  public DBTableChart(String name)
  {
    super(name);
  }

  public DBTableChart(String name, String type, String table_name, String discriminator, String value_field, String where_clause)
  {
    super(name);
    this.type = type;
    this.table_name = table_name;
    this.discriminator = discriminator;
    this.value_field = value_field;
    this.where_clause = where_clause;
  }

  @Override public void parse(Element e, Path file)
  {
    type = "Type.Bars";
    table_name = XMLHelper.getStringValueOfChild(e, "table");
    discriminator = XMLHelper.getStringValueOfChild(e, "discriminator");
    value_field = XMLHelper.getStringValueOfChild(e, "value");
    where_clause = XMLHelper.getStringValueOfChild(e, "where");

    getSimpleConfigs(e);
    colors = XMLHelper.getStringValueOfChild(e, "colors");
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    if( !super.equals(o) )
      return false;
    DBTableChart that = (DBTableChart) o;
    return Objects.equals(type, that.type) && Objects.equals(table_name, that.table_name) && Objects.equals(discriminator,
        that.discriminator) && Objects.equals(value_field, that.value_field) && Objects.equals(where_clause, that.where_clause)
        && Objects.equals(colors, that.colors);
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    e.setAttribute("type", "bars");

    Element node = document.createElement("table");
    node.appendChild(document.createTextNode(table_name));
    e.appendChild(node);
    if( discriminator != null )
    {
      node = document.createElement("discriminator");
      node.appendChild(document.createTextNode(discriminator));
      e.appendChild(node);
    }
    if( value_field != null )
    {
      node = document.createElement("value");
      node.appendChild(document.createTextNode(value_field));
      e.appendChild(node);
    }
    if( where_clause != null )
    {
      node = document.createElement("where");
      node.appendChild(document.createTextNode(where_clause));
      e.appendChild(node);
    }

    super.writeSimpleConfigs(document, e);

    if( colors != null )
    {
      node = document.createElement("colors");
      node.appendChild(document.createTextNode(colors));
      e.appendChild(node);
    }
  }

  @Override public String getAvailConstructor()
  {
    // new DBChartTableSpecial("chart_TInPZ", Type.Bars, "session_metric", "param", "value", "metric = 16");
    return "new DBChartTable(\"" + this.name + "\", " + this.type + ", \"" + this.table_name + "\", \"" + this.discriminator + "\", \""
        + this.value_field + "\", \"" + this.where_clause + "\")";
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  @Override public Collection<String> getConfigLines(String varname)
  {
    List<String> lines = new ArrayList<String>(5);

    lines.add("const " + varname + "= new BarsChartConfig(info);");
    lines.addAll(getSimpleConfigImportLines(varname));

    if( colors != null )
      lines.add(varname + ".colors= [" + colors + "];");

    return lines;
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  @Override public Charts.Chart copy()
  {
    DBTableChart ret = new DBTableChart(name);
    ret.type = type;
    ret.table_name = table_name;
    ret.discriminator = discriminator;
    ret.value_field = value_field;
    ret.where_clause = where_clause;
    ret.colors = colors;
    ret.copySimpleConfig(this);
    return ret;
  }

  @Override public Set<String> getAllowedValues()
  {
    return allowedConfigStrings;
  }

  @Override public Set<String> getQuotedValues()
  {
    return quotedConfigs;
  }
}
