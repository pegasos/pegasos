package at.pegasos.generator.parser.webparser.token;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Config;
import lombok.extern.slf4j.*;

@Slf4j
public class Web {

  @Override
  public String toString()
  {
    return "Web [activitynames=" + activitynames + ", charts=" + charts + ", config=" + config + ", routes=" + routes + ", menu=" + menu
        + ", components=" + components + ", default_added=" + default_routes_added + "]";
  }

  public ActivityNames activitynames;

  public Exports exports;

  public Charts charts;

  public Config config;
  
  public List<Route> routes;
  
  public final List<MenuEntry> menu;

  public Components components;

  public Modules modules;
  
  public AdminModules adminmodules;

  private final Map<String, Object> data;

  public boolean default_routes_added = false;
  
  public Web()
  {
    this.activitynames= new ActivityNames();
    this.exports = new Exports();
    this.charts= new Charts();
    this.config= new Config();
    this.routes= new ArrayList<Route>();
    this.menu= new ArrayList<MenuEntry>();
    this.components= new Components();
    this.modules= new Modules();
    this.adminmodules= new AdminModules();
    this.data = new HashMap<>();
  }

  public void add(Web web) throws ParserException
  {
    this.charts.addOther(web.charts);
    this.config.add(web.config);
    this.routes.addAll(web.routes);
    this.components.add(web.components);
    this.modules.add(web.modules);
  }

  public void addRoute(Route r)
  {
    this.routes.add(r);
  }

  public void addComponent(String name, String file, String path, boolean declare, boolean entry)
  {
    this.components.addComponent(name, file, path, declare, entry);
  }

  public void addModule(String name, String file, String path)
  {
    this.modules.addModule(name, file, path);
  }

  public void addMenuEntry(MenuEntry e)
  {
    if( !e.isDummy )
    {
      Stream<MenuEntry> dummys = menu.stream().filter(menu -> menu.name.equals(e.name) && menu.isDummy);
      Optional<MenuEntry> odummy = dummys.findFirst();
      if( odummy.isPresent() )
      {
        log.info("Replacing dummy entry for {} with {}", e.name, e);
        MenuEntry dummy = odummy.get();
        dummy.replaceDummy(e);
      }
      else
        menu.add(e);
    }
    else
    {
      // we are the dummy. Can we find a concrete?
      Stream<MenuEntry> concretes = menu.stream().filter(menu -> menu.name.equals(e.name) && !menu.isDummy);
      Optional<MenuEntry> oconcrete = concretes.findFirst();
      if( oconcrete.isPresent() )
      {
        log.info("Adding info from dummy entry for {} with {} to {}", e.name, e, oconcrete.get());
        oconcrete.get().subMenus.addAll(e.subMenus);
      }
      else
      {
        log.debug("Adding dummy entry {} to menu", e);
        menu.add(e);
      }
    }
  }

  public void addMenuEntry(String parent, MenuEntry e) throws ParserException
  {
    Stream<MenuEntry> parents = menu.stream().filter(menu -> menu.name.equals(parent));
    Optional<MenuEntry> oparent = parents.findFirst();
    if( oparent.isEmpty() )
    {
      log.info("Currently there is no menu with name '{}' (required as parent from {}). Adding a dummy entry.", parent, e.name);
      MenuEntry dummy = new MenuEntry(null, parent);
      dummy.isDummy = true;
      menu.add(dummy);
      dummy.subMenus.add(e);
      log.debug("parent: '{}', entry: {}", parent, e);
      log.debug("menu: {}", menu);
    }
    else
      oparent.get().subMenus.add(e);
  }

  public AdminModules getAdminModules()
  {
    return adminmodules;
  }

  public Object get(String object)
  {
    return this.data.get(object);
  }

  public Object set(String object, Object data)
  {
    return this.data.put(object, data);
  }

  public void ensurePresent(String object, Class<?> clasz)
  {
    this.data.computeIfAbsent(object, k -> {
      try
      {
        return clasz.getConstructor().newInstance();
      }
      catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e )
      {
        e.printStackTrace();
        throw new IllegalStateException("Error ensuring " + clasz + " is present as " + object + ". No default constructor?");
      }
    });
  }

  public static String getBackendBaseUrl(Config config) throws ParserException
  {
    String baseURL= config.getValue("backend.url") + ":";
    if( config.hasValue("backend.port.public") )
      baseURL += config.getValue("backend.port.public");
    else
      baseURL += config.getValue("backend.port");
    if( !config.getValueString("backend.path").equals("") )
      baseURL+= "/" + config.getValueString("backend.path");
    return baseURL;
  }
}
