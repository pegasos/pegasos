package at.pegasos.generator.parser.webparser.token;

import java.util.*;

import lombok.*;

@ToString
public class Route {

  public List<String> guards;
  public String path;

  public Route()
  {
    this.guards= new ArrayList<String>();
  }

  public Route(Collection<String> guards)
  {
    this.guards= new ArrayList<String>();
    this.guards.addAll(guards);
  }
}