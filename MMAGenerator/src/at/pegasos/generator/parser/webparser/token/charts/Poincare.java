package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

public class Poincare extends Charts.Chart {
  public static final String TypeName = "Poincaré";

  public Poincare(String name)
  {
    super(name);
  }

  @Override public String getAvailConstructor()
  {
    return "new DBChartSimple(\"" + name + "\", Type.Poincare, \"hr\", \"hr\")";
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  @Override public Collection<String> getConfigImports()
  {
    return new ArrayList<String>(0);
  }

  @Override public Collection<String> getConfigLines(String varname)
  {
    return null;
  }

  @Override public void parse(Element e, Path file)
  {

  }

  @Override public void write(Document document, Element e)
  {
    Element node = document.createElement("name");
    node.appendChild(document.createTextNode(name));
    e.appendChild(node);

    e.setAttribute("type", "poincare");
  }

  @Override public Charts.Chart copy()
  {
    Poincare ret = new Poincare(name);
    ret.default_avail = default_avail;
    return ret;
  }
}
