package at.pegasos.generator.parser.webparser.token.reports;

import org.w3c.dom.*;

import lombok.*;

@EqualsAndHashCode
public abstract class Enhancer {
  public abstract void parse(Element item);

  public abstract void write(Document document, Element d);
}

