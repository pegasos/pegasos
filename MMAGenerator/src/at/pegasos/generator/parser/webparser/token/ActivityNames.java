package at.pegasos.generator.parser.webparser.token;

import at.pegasos.generator.parser.*;
import at.pegasos.util.DAG;
import at.pegasos.util.DAG.Node;
import at.pegasos.util.DAG.NodeMerger;
import lombok.extern.slf4j.*;

import java.util.*;
import java.util.stream.*;

@Slf4j
public class ActivityNames {

  public static class Activity extends at.pegasos.generator.parser.token.Activity {
    public String name;
    /**
     * Whether this activity is part of the name tree
     */
    public boolean added = false;

    /**
     * Whether this is a dummy
     */
    public boolean dummy = true;
    
    public Activity()
    {
      param0= param1= param2= -1;
      added = false;
    }
    
    public int hashCode()
    {
      return (param2 != -1 ? param2 * 10000 : 0 ) + (param1 != -1 ? param1 * 100 : 0 ) + (param0 != -1 ? param0 : 0 );
    }

    public String toString()
    {
      return param0 + " " + param1 + " " + param2 + " " + name;
    }
  }
  
  DAG<Activity> activities;
  
  private final Activity root;

  private final Node<Activity> rootnode;

  public ActivityNames()
  {
    activities= new DAG<Activity>();
    root= new Activity();
    this.rootnode= activities.addNode(root);
  }
  
  public Node<Activity> getRoot()
  {
    return rootnode;
  }
  
  public void add(ActivityNames other)
  {
    this.rootnode.mergeNode(this.activities, other.rootnode, new NodeMerger<Activity>() {
      
      @Override
      public void run()
      {
        if( a.getID().name == null && b.getID().name != null )
        {
          a.getID().name= b.getID().name;
        }
        else if( a.getID().name != null && b.getID().name != null )
        {
          if( !a.getID().name.equals(b.getID().name) )
          {
            // TODO: throw exception
          }
        }
      }
    });
  }
  
  public Activity addName(int param0, String name) throws ParserException {
    Activity a= new Activity();
    a.param0= param0;
    a.name= name;
    
    boolean add= true;
    
    Node<Activity> h;
    if( (h= activities.getNode(a)) != null )
    {
      if( h.getID().name == null )
      {
        h.getID().name = name;
        h.getID().dummy = false;
      }
      else
      {
        throw new ParserException("Duplicate activity name for " + param0);
      }
      add= false;
    }
    
    if( add )
    {
      a = activities.addNode(a).getID();
      activities.addEdgeNC(root, a);
      a.added = true;
      a.dummy = false;
      return a;
    }
    return activities.getNode(a).getID();
  }

  public Activity addName(int param0, int param1, String name) throws ParserException
  {
    Activity h = new Activity();
    h.param0 = param0;

    Activity a = new Activity();
    a.param0 = param0;
    a.param1 = param1;
    a.name = name;

    // Check if an activity with these parameters exists
    boolean add= true;

    Node<Activity> hh;
    if( (hh= activities.getNode(a)) != null )
    {
      if( hh.getID().name == null )
      {
        hh.getID().name = name;
        hh.getID().dummy = false;
      }
      else
      {
        throw new ParserException("Duplicate activity name for " + param0 + "/" + param1);
      }
      add= false;
    }
    
    // Check if parent exists
    if( activities.getNode(h) == null )
    {
      // Parent does not exist -> add it
      h = activities.addNode(h).getID();
      activities.addEdgeNC(root, h);
    }

    if( add )
    {
      a = activities.addNode(a).getID();
      activities.addEdgeNC(h, a);
      a.added = true;
      a.dummy = false;
      return a;
    }
    return activities.getNode(a).getID();
  }
  
  public Activity addName(int param0, int param1, int param2, String name) throws ParserException
  {
    Activity h= new Activity();
    h.param0= param0;
    
    Activity h1= new Activity();
    h1.param0= param0;
    h1.param1= param1;
    
    Activity a= new Activity();
    a.param0= param0;
    a.param1= param1;
    a.param2= param2;
    a.name= name;

    if( activities.getNode(a) != null )
    {
      throw new ParserException("Duplicate activity name for " + param0 + "/" + param1 + "/" + param2);
    }

    // Check if parent exists
    if( activities.getNode(h) == null )
    {
      // Parent does not exist -> add it
      h = activities.addNode(h).getID();
      activities.addEdgeNC(root, h);
    }
    if( activities.getNode(h1) == null )
    {
      h1 = activities.addNode(h1).getID();
      activities.addEdgeNC(h, h1);
    }

    activities.addNode(a);
    activities.addEdgeNC(h1, a);
    a.added = true;
    a.dummy = false;

    return activities.getNode(a).getID();
  }

  public Activity addName(Activity a) throws ParserException
  {
    if( a.param0 != -1 )
    {
      if( a.param1 != -1 )
      {
        if( a.param2 != -1 )
          return addName(a.param0, a.param1, a.param2, a.name);
        else
          return addName(a.param0, a.param1, a.name);
      }
      else
        return addName(a.param0, a.name);
    }
    return a;
  }

  public void remove(Activity a)
  {
    log.debug("Removing {}", a);
    if( activities.getNode(a) == null )
    {
      log.debug("Removing activity which does not exist");
      return;
    }

    Node<Activity> node = activities.getNode(a);
    if( node.getChildNodes().size() > 0 )
    {
      node.getID().name = "";
      node.getID().dummy = true;
    }
    else
      activities.removeNode(rootnode, node);
    debug();
  }

  public void debug()
  {
    System.out.println("---");
    for(Node<Activity> x : rootnode.getAllChildren())
    {
      System.out.println(x.getID());
    }
    System.out.println("---");
  }

  public List<Activity> getAllActivities()
  {
    return rootnode.getAllChildren().stream().map(Node::getID).collect(Collectors.toList());
  }
}
