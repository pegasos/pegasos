package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

public class GeneratedChartType extends Charts.Chart {
  private final static Collection<String> configImports;

  static
  {
    configImports = new ArrayList<String>(0);
  }

  private final String typeName;

  // private Function<String, Collection<String>> configLines = null;

  private String constructor;
  public String ctype;
  public boolean alwaysavailable = false;
  public boolean hasAvailable = false;
  private String col;
  private String tbl;
  private String cls;
  private String args;
  private Type type;

  public GeneratedChartType(String typeName, String name)
  {
    super(name);
    this.typeName = typeName;
  }

  @Override public void parse(Element e, Path file) throws ParserException
  {
    ctype = XMLHelper.getStringValueOfChild(e, "charttype");

    hasAvailable = e.getElementsByTagName("available").getLength() > 0;

    alwaysavailable = EntryBool.fromString(XMLHelper.getStringValueOfChild(e, "alwaysavailable"), false).isTrue();
    if( alwaysavailable )
    {
      constructor = "new AlwaysAvailableChart(\"" + name + "\", Type." + ctype + ")";
    }
    else if( hasAvailable )
    {
      Element avail = (Element) e.getElementsByTagName("available").item(0);
      String cls = XMLHelper.getAttribute(avail, "class", null);
      String args = XMLHelper.getAttribute(avail, "arguments", null);

      setClsArgs(cls,args);
    }
    else
    {
      String tbl = XMLHelper.getStringValueOfChild(e, "table");
      String col = XMLHelper.getStringValueOfChild(e, "column");

      setTblCol(tbl, col);
    }

    if( typeName.equalsIgnoreCase("generated") )
    {
      String cclass = XMLHelper.getStringValueOfChild(e, "chartclass");
      String cfile = XMLHelper.getStringValueOfChild(e, "chartfile");

      type = Type.create(ctype, cclass, cfile);
    }
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    if( !super.equals(o) )
      return false;
    GeneratedChartType that = (GeneratedChartType) o;
    return alwaysavailable == that.alwaysavailable && hasAvailable == that.hasAvailable && Objects.equals(typeName, that.typeName)
        && /*Objects.equals(configLines, that.configLines) &&*/ Objects.equals(constructor, that.constructor) && Objects.equals(ctype,
        that.ctype) && Objects.equals(col, that.col) && Objects.equals(tbl, that.tbl) && Objects.equals(cls, that.cls) && Objects.equals(
        args, that.args) && Objects.equals(type, that.type);
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    e.setAttribute("type", typeName.toLowerCase(Locale.ROOT));

    boolean typeWritten = false;

    if( alwaysavailable )
    {
      Element node = document.createElement("alwaysavailable");
      node.appendChild(document.createTextNode("true"));
      e.appendChild(node);
      node = document.createElement("charttype");
      node.appendChild(document.createTextNode(ctype));
      e.appendChild(node);
      typeWritten = true;
    }
    else if( hasAvailable )
    {
      Element node = document.createElement("available");
      node.setAttribute("class", cls);
      if( args != null )
        node.setAttribute("arguments", args);
      e.appendChild(node);
      node = document.createElement("charttype");
      node.appendChild(document.createTextNode(ctype));
      e.appendChild(node);
      typeWritten = true;
    }
    else
    {
      Element node = document.createElement("table");
      node.appendChild(document.createTextNode(tbl));
      e.appendChild(node);
      node = document.createElement("column");
      node.appendChild(document.createTextNode(col));
      e.appendChild(node);
    }

    if( type != null )
      type.write(document, e, typeWritten);
  }

  @Override public String getAvailConstructor()
  {
    return constructor;
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  /*public void setConfigLines(Function<String, Collection<String>> configLines)
  {
    this.configLines = configLines;
  }

  @Override public Collection<String> getConfigLines(String varname)
  {
    if( configLines == null )
      return null;
    else
    {
      return configLines.apply(varname);
    }
  }*/
  @Override public Collection<String> getConfigLines(String varname)
  {
    return null;
  }

  public void setType(Type type)
  {
    this.type = type;
  }

  public Type getType()
  {
    return type;
  }

  @Override public String getTypeName()
  {
    return typeName;
  }

  @Override public Charts.Chart copy()
  {
    GeneratedChartType ret = new GeneratedChartType(typeName, name);
    ret.constructor = constructor;
    ret.alwaysavailable = alwaysavailable;
    ret.default_avail = default_avail;
    ret.hasAvailable = hasAvailable;
    ret.ctype = ctype;
    ret.cls = cls;
    ret.args = args;
    ret.tbl = tbl;
    ret.col = col;

    if( type != null )
      ret.setType(type.copy());

    return ret;
  }

  public String getCls()
  {
    return cls;
  }

  public String getArgs()
  {
    return args;
  }

  /**
   * Attention: charttype / ctype needs to be set first!
   * @param value true if always available (will set constructor)
   */
  public void setAlwaysAvailable(boolean value)
  {
    alwaysavailable = value;
    if( value )
      constructor = "new AlwaysAvailableChart(\"" + name + "\", Type." + ctype + ")";
  }
  /**
   * Attention: charttype / ctype needs to be set first!
   *
   * @param cls class
   * @param args constructor arguments
   */
  public void setClsArgs(String cls, String args)
  {
    this.cls = cls;
    if( args != null && !args.equals("") )
      this.args = args;
    else
      this.args = null;

    String constr = "new " + cls + "(\"" + name + "\", Type." + ctype;
    if( this.args != null )
    {
      constr += "," + args;
    }
    constr += ")";

    constructor = constr;
  }

  public String getTbl()
  {
    return tbl;
  }

  public String getCol()
  {
    return col;
  }

  public void setTblCol(String tbl, String col)
  {
    this.tbl = tbl;
    this.col = col;
    constructor = "new DBChartSimple(\"" + name + "\", Type." + ctype + ", \"" + tbl + "\", \"" + col + "\")";
  }
}
