package at.pegasos.generator.parser.webparser.token.reports;

import at.pegasos.generator.parser.*;
import lombok.*;
import org.w3c.dom.*;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MetricBasedReport extends Report {
  @EqualsAndHashCode
  public static class SessionTypeFilter {
    public Integer p0;
    public Integer p1;
    public Integer p2;

    public SessionTypeFilter()
    {
    }

    public SessionTypeFilter(int p0)
    {
      this.p0 = p0;
    }

    public SessionTypeFilter(int p0, int p1)
    {
      this.p0 = p0;
      this.p1 = p1;
    }

    public SessionTypeFilter(int p0, int p1, int p2)
    {
      this.p0 = p0;
      this.p1 = p1;
      this.p2 = p2;
    }

    public SessionTypeFilter(Integer p0, Integer p1, Integer p2)
    {
      this.p0 = p0;
      this.p1 = p1;
      this.p2 = p2;
    }

    public String constr()
    {
      return "SessionTypeFilter(" + p0 + (p1 != null ? "," + p1 + (p2 != null ? "," + p2 : "") : "") + ")";
    }
  }

  @EqualsAndHashCode
  public static class SessionTypeFilters {
    public SessionTypeFilter[] include;

    public void parse(Element item)
    {
      NodeList filters = ((Element) (item.getElementsByTagName("sessions")).item(0)).getElementsByTagName("include");
      Element e;
      if( filters.getLength() > 0 )
      {
        include = new SessionTypeFilter[filters.getLength()];
        for(int i = 0; i < include.length; i++)
        {
          e = (Element) filters.item(i);
          String sparam0 = e.getAttribute("param0");
          String sparam1 = e.getAttribute("param1");
          String sparam2 = e.getAttribute("param2");

          Integer param0 = null, param1 = null, param2 = null;
          if( !sparam0.equals("") )
            param0 = Integer.parseInt(sparam0);
          if( !sparam1.equals("") )
            param1 = Integer.parseInt(sparam1);
          if( !sparam2.equals("") )
            param2 = Integer.parseInt(sparam2);

          include[i] = new SessionTypeFilter(param0, param1, param2);
        }
      }
    }

    public void write(Document document, Element e)
    {
      Element filters = document.createElement("sessions");
      for(SessionTypeFilter filter : include)
      {
        Element include = document.createElement("include");
        if( filter.p0 != null )
          include.setAttribute("param0", String.valueOf(filter.p0));
        if( filter.p1 != null )
          include.setAttribute("param1", String.valueOf(filter.p1));
        if( filter.p2 != null )
          include.setAttribute("param2", String.valueOf(filter.p2));
        filters.appendChild(include);
      }
      e.appendChild(filters);
    }
  }

  public String[] metrics;
  public MetricBasedReport.SessionTypeFilters includeFilters;

  @Override
  public void parse(Element item) throws ParserException
  {
    super.parse(item);

    if( (item.getElementsByTagName("metrics")).getLength() > 0 )
    {
      NodeList values = ((Element) (item.getElementsByTagName("metrics")).item(0)).getElementsByTagName("metric");
      if( values.getLength() > 0 )
      {
        metrics = new String[values.getLength()];
        for(int i = 0; i < metrics.length; i++)
        {
          metrics[i]= XMLHelper.getStringValueOfChild((Element) values.item(i));
        }
      }
    }

    if( (item.getElementsByTagName("sessions")).getLength() > 0 )
    {
      includeFilters = new SessionTypeFilters();
      includeFilters.parse(item);
    }
  }

  @Override
  public void write(Document document, Element e)
  {
    super.write(document, e);

    Element values = document.createElement("metrics");
    for(String name : metrics)
    {
      Element value = document.createElement("metric");
      value.appendChild(document.createTextNode(name));
      values.appendChild(value);
    }
    e.appendChild(values);

    if( includeFilters != null )
    {
      includeFilters.write(document, e);
    }
  }
}
