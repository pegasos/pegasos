package at.pegasos.generator.parser.webparser.token.reports;

import at.pegasos.generator.parser.*;
import lombok.*;
import org.w3c.dom.*;

@Getter
@EqualsAndHashCode(callSuper = true)
public class UserValueBasedReport extends Report {
  /**
   * Names of the values which should be displayed
   */
  public String[] names;

  @Override
  public void parse(Element item) throws ParserException
  {
    super.parse(item);

    if( (item.getElementsByTagName("values")).getLength() > 0 )
    {
      NodeList values = ((Element) (item.getElementsByTagName("values")).item(0)).getElementsByTagName("value");
      if( values.getLength() > 0 )
      {
        names = new String[values.getLength()];
        for(int i = 0; i < names.length; i++)
        {
          names[i]= XMLHelper.getStringValueOfChild((Element) values.item(i));
        }
      }
    }
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    Element values = document.createElement("values");
    for(String name : names)
    {
      Element value = document.createElement("value");
      value.appendChild(document.createTextNode(name));
      values.appendChild(value);
    }
    e.appendChild(values);
  }
}
