package at.pegasos.generator.parser.webparser.token.reports;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.Comparator;
import at.pegasos.generator.parser.webparser.token.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import java.util.*;

@Slf4j @ToString(callSuper = true) @EqualsAndHashCode(callSuper = true)
public class MetricBasedGroupedReport extends MetricBasedReport {

  public LapQuery lapQuery;
  public MetricBasedAccumulatedReport.GroupBy groupBy;

  @Override
  public void parse(Element item) throws ParserException
  {
    super.parse(item);

    if( (item.getElementsByTagName("filter")).getLength() > 0 )
    {
      NodeList values = ((Element) (item.getElementsByTagName("filter")).item(0)).getElementsByTagName("lap");
      if( values.getLength() > 0 )
      {
        final int length = values.getLength();
        lapQuery = new LapQuery(length);
        for(int i = 0; i < length; i++)
        {
          lapQuery.filters.add(LapQuery.LapFilter.fromXml((Element) values.item(i)));
        }

        String conj = ((Element) (item.getElementsByTagName("filter")).item(0)).getAttribute("conjunction");
        if( !conj.equals("") )
          lapQuery.conj = Conjunction.valueOf(conj);
      }
    }

    try
    {
      groupBy = MetricBasedAccumulatedReport.GroupBy.valueOf(XMLHelper.getStringValueOfChild(item, "groupby"));
    }
    catch( IllegalArgumentException e )
    {
      throw new ParserException("Unknown groupBy type '" + XMLHelper.getStringValueOfChild(item, "groupby") + "', or missing");
    }
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    if( lapQuery != null )
    {
      e.appendChild(lapQuery.write(document));
    }

    Element x = document.createElement("groupby");
    x.appendChild(document.createTextNode(groupBy.toString()));
    e.appendChild(x);
  }

  @EqualsAndHashCode @ToString
  public static class LapQuery {

    public final List<LapFilter> filters;
    public Conjunction conj;
    public LapQuery(int length)
    {
      this.filters = new ArrayList<>(length);
    }

    public Node write(Document document)
    {
      Element ret = document.createElement("filter");
      for(LapFilter filter : filters)
      {
        Element f = document.createElement("lap");
        filter.write(f);
        ret.appendChild(f);
      }

      if( conj != null )
        ret.setAttribute("conjunction", String.valueOf(conj));
      return ret;
    }

    @EqualsAndHashCode @ToString
    public static class LapFilter {

      public LapFilterType type;
      public long value;
      public Comparator comp;

      public static LapFilter fromXml(Element item)
      {
        LapFilter ret = new LapFilter();
        ret.type = LapFilterType.valueOf(item.getAttribute("type"));
        ret.value = Long.parseLong(item.getAttribute("value"));
        ret.comp = Comparator.valueOf(item.getAttribute("comp"));
        return ret;
      }

      public void write(Element e)
      {
        e.setAttribute("type", type.toString());
        e.setAttribute("value", String.valueOf(value));
        e.setAttribute("comp", comp.toString());
      }

      public enum LapFilterType {
        StartTime, Duration, LapNr
      }
    }
  }
}
