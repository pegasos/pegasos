package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

/**
 * A multiline standard chart generated from a database column
 */
public class MultiLineChart extends Charts.LineChart {
  private final static Collection<String> configImports;
  private final static Set<String> allowedConfigStrings;

  static
  {
    configImports = new ArrayList<String>(1);
    configImports.add("{ MultiLineChartConfig } from './../shared/chartconfig'");

    allowedConfigStrings = new HashSet<String>();
    allowedConfigStrings.addAll(Charts.LineChart.allowedConfigStrings);
  }

  /**
   * database table where the data is located
   */
  public String tbl;

  public String type;

  private String[] lines;

  public String line_colors;

  public final static String TypeName = "Multi-Line";

  public MultiLineChart(String name)
  {
    super(name);
  }

  public MultiLineChart(String name, String table, String type, String[] lines)
  {
    super(name);
    this.tbl = table;
    this.type = "LineType." + type.toUpperCase();
    this.lines = lines;
  }

  @Override public void parse(Element e, Path file) throws ParserException
  {
    tbl = XMLHelper.getStringValueOfChild(e, "table");
    String t = XMLHelper.getStringValueOfChild(e, "type");
    if( t == null )
      throw new ParserException("Need to specify 'type' for chart '" + name + "' in " + file);
    type = "LineType." + t.toUpperCase();
    String lineS = XMLHelper.getStringValueOfChild(e, "lines");
    if( lineS == null )
      throw new ParserException("Need to specify 'lines' for chart '" + name + "' in " + file);
    setLines(lineS);

    parseConfig(e);
  }

  @Override public String getAvailConstructor()
  {
    return "new DBChartMultiLine(\"" + name + "\", \"" + tbl + "\", " + type + ", new String[] {\"" + String.join("\", \"", lines) + "\"})";
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  public Collection<String> getConfigLines(String varname)
  {
    List<String> lines = new ArrayList<String>(5);

    lines.add("const " + varname + "= new MultiLineChartConfig(info);");
    if( line_colors != null )
      lines.add(varname + ".linecolors= [" + line_colors + "];");
    lines.addAll(getSimpleConfigImportLines(varname));
    return lines;
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  public void parseConfig(Element e)
  {
    this.line_colors = XMLHelper.getStringValueOfChild(e, "linecolors");
    getSimpleConfigs(e);
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    if( !super.equals(o) )
      return false;
    MultiLineChart that = (MultiLineChart) o;
    return Objects.equals(tbl, that.tbl) && Objects.equals(type, that.type) && Arrays.equals(lines, that.lines) && Objects.equals(
        line_colors, that.line_colors);
  }

  @Override public Set<String> getAllowedValues()
  {
    return allowedConfigStrings;
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    e.setAttribute("type", "dbmline");

    Element node = document.createElement("table");
    node.appendChild(document.createTextNode(tbl));
    e.appendChild(node);
    node = document.createElement("type");
    if( type.startsWith("LineType."))
      node.appendChild(document.createTextNode(type.substring(9).toLowerCase(Locale.ROOT)));
    else
      node.appendChild(document.createTextNode(type.toLowerCase(Locale.ROOT)));
    e.appendChild(node);
    node = document.createElement("lines");
    node.appendChild(document.createTextNode(String.join(",", lines)));
    e.appendChild(node);

    writeConfig(document, e);
  }

  protected void writeConfig(Document document, Element e)
  {
    Element node = document.createElement("linecolors");
    node.appendChild(document.createTextNode(line_colors));
    e.appendChild(node);
    super.writeSimpleConfigs(document, e);
  }

  @Override public Charts.Chart copy()
  {
    MultiLineChart ret = new MultiLineChart(name);
    ret.tbl = tbl;
    ret.type = type;
    ret.lines = Arrays.copyOf(lines, lines.length);

    ret.copyConfig(this);
    return ret;
  }

  protected void copyConfig(Charts.LineChart otherChart)
  {
    this.line_colors = ((MultiLineChart) otherChart).line_colors;
    super.copySimpleConfig(otherChart);
  }

  public String getLines()
  {
    if( lines != null )
      return String.join(",", lines);
    else
      return "";
  }

  public void setLines(String lineS)
  {
    lines = lineS.split(",");
  }
}
