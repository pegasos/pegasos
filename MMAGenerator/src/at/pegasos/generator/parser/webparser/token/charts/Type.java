package at.pegasos.generator.parser.webparser.token.charts;

import lombok.*;
import org.w3c.dom.*;

import java.util.*;

@ToString
public class Type implements Comparable<Type> {
  /**
   * Import statement for the chart
   */
  public String imp;
  /**
   * Name of the class
   */
  public String clas;
  public String type;
  public String file;

  public static Type create(String ctype, String cclass, String cfile)
  {
    Type t = new Type();
    t.type = ctype;
    t.clas = cclass;
    t.file = cfile;
    t.imp = "{ " + cclass + " } from '" + cfile + "'";

    return t;
  }

  public void write(Document document, Element e, boolean typeWritten)
  {
    Element node;
    if( !typeWritten )
    {
      node = document.createElement("charttype");
      node.appendChild(document.createTextNode(type));
      e.appendChild(node);
    }
    node = document.createElement("chartclass");
    node.appendChild(document.createTextNode(clas));
    e.appendChild(node);
    node = document.createElement("chartfile");
    node.appendChild(document.createTextNode(file));
    e.appendChild(node);
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    Type type1 = (Type) o;
    return Objects.equals(file, type1.file) && Objects.equals(clas, type1.clas) && Objects.equals(type, type1.type);
  }

  public int compareTo(Type anotherType)
  {
    return this.type.compareTo(anotherType.type);
  }

  public Type copy()
  {
    Type ret = new Type();
    ret.imp = this.imp;
    ret.clas = this.clas;
    ret.type = this.type;
    ret.file = this.file;
    return ret;
  }
}