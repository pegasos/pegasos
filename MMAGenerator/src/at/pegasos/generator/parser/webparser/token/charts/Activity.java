package at.pegasos.generator.parser.webparser.token.charts;

import java.util.*;

public class Activity extends at.pegasos.generator.parser.token.Activity {
  public static class Chart {
    public String name;
    public int order;
    public boolean hasOrder;

    public Chart(String name, int order, boolean hasOrder)
    {
      this.name = name;
      this.order = order;
      this.hasOrder = hasOrder;
    }

    @Override public boolean equals(Object o)
    {
      if( this == o )
        return true;
      if( o == null || getClass() != o.getClass() )
        return false;
      Chart chart = (Chart) o;
      return order == chart.order && hasOrder == chart.hasOrder && Objects.equals(name, chart.name);
    }

    @Override
    public String toString() {
      return "[" + name + (hasOrder ? "," + order : "" ) + "]";
    }
  }

  public List<Chart> chartsPre;
  public List<Chart> chartsPost;
  public boolean adddefault = false;

  public Boolean overwrite;

  public Activity()
  {
    param0 = param1 = param2 = -1;
    chartsPre = new ArrayList<>();
    chartsPost = new ArrayList<>();
  }

  public Activity copy()
  {
    Activity ret = new Activity();
    ret.param0 = param0;
    ret.param1 = param1;
    ret.param2 = param2;
    ret.chartsPre = new ArrayList<>(chartsPre);
    ret.chartsPost = new ArrayList<>(chartsPost);
    ret.adddefault = adddefault;
    ret.overwrite = overwrite;
    return ret;
  }

  public void replace(Activity newValue)
  {
    assert(param0 == newValue.param0 && param1 == newValue.param1 && param2 == newValue.param2);
    adddefault = newValue.adddefault;
    overwrite = newValue.overwrite;
    chartsPre = newValue.chartsPre;
    chartsPost = newValue.chartsPost;
  }

  public String toString()
  {
    return param0 + " " + param1 + " " + param2 + " " + chartsPre.toString() + "/" +
            chartsPost.toString()+ " default:" + adddefault;
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    Activity activity = (Activity) o;
    return adddefault == activity.adddefault && Objects.equals(chartsPre, activity.chartsPre) && Objects.equals(chartsPost,
        activity.chartsPost) && Objects.equals(overwrite, activity.overwrite);
  }
}
