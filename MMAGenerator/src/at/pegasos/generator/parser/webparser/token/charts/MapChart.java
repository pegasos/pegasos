package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

/**
 * A simple standard chart generated from a database column
 */
public class MapChart extends Charts.Chart {
  private final static Collection<String> configImports;
  public final static String TypeName = "Map (position)";

  static
  {
    configImports = new ArrayList<String>(0);
  }

  /**
   * database table where the data is located
   */
  public String tbl;

  public String collat;
  public String collon;

  public MapChart(String name)
  {
    super(name);
  }

  public MapChart(String name, String tbl, String collat, String collon)
  {
    super(name);
    this.tbl = tbl;
    this.collat = collat;
    this.collon = collon;
  }

  @Override public void parse(Element e, Path file)
  {
    tbl = XMLHelper.getStringValueOfChild(e, "table");
    collat = XMLHelper.getStringValueOfChild(e, "lat");
    collon = XMLHelper.getStringValueOfChild(e, "lon");
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    if( !super.equals(o) )
      return false;
    MapChart mapChart = (MapChart) o;
    return Objects.equals(tbl, mapChart.tbl) && Objects.equals(collat, mapChart.collat) && Objects.equals(collon, mapChart.collon);
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    e.setAttribute("type", "dbmap");

    Element node = document.createElement("table");
    node.appendChild(document.createTextNode(tbl));
    e.appendChild(node);
    node = document.createElement("lat");
    node.appendChild(document.createTextNode(collat));
    e.appendChild(node);
    node = document.createElement("lon");
    node.appendChild(document.createTextNode(collon));
    e.appendChild(node);
  }

  @Override public String getAvailConstructor()
  {
    return "new DBMapChart(\"" + name + "\", Type.Map, \"" + tbl + "\", \"" + collat + "\", \"" + collon + "\")";
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  @Override public Collection<String> getConfigLines(String varname)
  {
    return null;
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  @Override public Charts.Chart copy()
  {
    MapChart ret = new MapChart(name);
    ret.tbl = tbl;
    ret.collat = collat;
    ret.collon = collon;
    return ret;
  }
}
