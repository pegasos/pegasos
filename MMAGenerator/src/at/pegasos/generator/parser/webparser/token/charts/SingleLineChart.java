package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

/**
 * A simple standard chart generated from a database column
 */
public class SingleLineChart extends Charts.LineChart {
  private final static Collection<String> configImports;
  private final static Set<String> allowedConfigStrings;
  private final static Set<String> quotedConfigs;
  public final static String TypeName = "Single Line";

  static
  {
    configImports = new ArrayList<String>(1);
    configImports.add("{ LineChartConfig } from './../shared/chartconfig'");

    allowedConfigStrings = new HashSet<String>();
    allowedConfigStrings.addAll(Charts.LineChart.allowedConfigStrings);
    allowedConfigStrings.add("linecolor");
    quotedConfigs = new HashSet<String>();
    quotedConfigs.addAll(Charts.LineChart.quotedConfigs);
    quotedConfigs.add("linecolor");
  }

  /**
   * database table where the data is located
   */
  public String tbl;
  /**
   * Column to display
   */
  public String col;

  public SingleLineChart(String name)
  {
    super(name);
  }

  public SingleLineChart(String name, String table, String column)
  {
    super(name);
    this.tbl = table;
    this.col = column;
  }

  @Override public void parse(Element e, Path file)
  {
    tbl = XMLHelper.getStringValueOfChild(e, "table");
    col = XMLHelper.getStringValueOfChild(e, "column");

    getSimpleConfigs(e);
  }

  @Override public String getAvailConstructor()
  {
    return "new DBChartSimple(\"" + name + "\", Type.Line, \"" + tbl + "\", \"" + col + "\")";
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  public Collection<String> getConfigLines(String varname)
  {
    List<String> lines = new ArrayList<String>(5);

    lines.add("const " + varname + "= new LineChartConfig(info);");
    lines.addAll(getSimpleConfigImportLines(varname));

    return lines;
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  @Override public void write(Document document, Element e)
  {
    super.write(document, e);

    e.setAttribute("type", "dbline");

    Element node = document.createElement("table");
    node.appendChild(document.createTextNode(tbl));
    e.appendChild(node);
    node = document.createElement("column");
    node.appendChild(document.createTextNode(col));
    e.appendChild(node);

    writeSimpleConfigs(document, e);
  }

  @Override public boolean equals(Object o)
  {
    if( this == o )
      return true;
    if( o == null || getClass() != o.getClass() )
      return false;
    if( !super.equals(o) )
      return false;
    SingleLineChart that = (SingleLineChart) o;
    return Objects.equals(tbl, that.tbl) && Objects.equals(col, that.col);
  }

  @Override public Set<String> getAllowedValues()
  {
    return allowedConfigStrings;
  }

  @Override  public Set<String> getQuotedValues()
  {
    return quotedConfigs;
  }

  @Override public Charts.Chart copy()
  {
    SingleLineChart ret = new SingleLineChart(name);
    ret.tbl = tbl;
    ret.col = col;

    ret.copySimpleConfig(this);
    return ret;
  }
}
