package at.pegasos.generator.parser.webparser.token;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import at.pegasos.generator.parser.webparser.token.Components.Resolvable;
import lombok.ToString;
import lombok.extern.slf4j.*;

@ToString
@Slf4j
public class AdminModules {

  @ToString
  public static class Module {
    /**
     * Name of the entry / module
     */
    public String label;
    public String action;
    public String componentName;
    public Resolvable path;
    int order = 1;
    public Map<String, Module> sub;
    
    public Module()
    {
      this.sub= new HashMap<String, Module>();
    }

    public void add(String[] modulePath, String filePath, String componentName, int order)
    {
      log.debug("Add new module: modulePath: {}, filePath {}, componentName {} order {}", modulePath, filePath, componentName, order);

      if( modulePath.length == 1 )
      {
        Module mod= new Module();
        mod.label= modulePath[0];
        mod.action= componentName;
        mod.componentName= componentName;
        mod.order = order;
        Path p= Paths.get(filePath);
        String fn= p.getFileName().toString();
        fn= fn.substring(0, fn.length() - 3);
        mod.path= new Resolvable(componentName, fn, p.getParent().toString());
        this.sub.put(modulePath[0], mod);
      }
      else
      {
        Module mod;
        if( !this.sub.containsKey(modulePath[0]) )
        {
          mod= new Module();
          mod.label= modulePath[0];
          this.sub.put(modulePath[0], mod);
        }
        else
          mod= this.sub.get(modulePath[0]);
        String[] modPath2= new String[modulePath.length-1];
        System.arraycopy(modulePath, 1, modPath2, 0, modulePath.length - 1);
        mod.add(modPath2, filePath, componentName, order);
      }
    }

    /**
     * Add the sub modules of a module to this (sub-module) i.e. all of module.sub will be added to this.sub
     * @param module
     */
    public void add(Module module)
    {
      for(String key : module.sub.keySet())
      {
        if( this.sub.containsKey(key) )
        {
          if( this.sub.get(key).sub.size() == 0 && module.sub.get(key).sub.size() > 0 )
            throw new IllegalStateException();
          this.sub.get(key).add(module.sub.get(key));
        }
        else
        {
          this.sub.put(key, module.sub.get(key));
        }
      }
    }
  }

  public Module modules;

  public AdminModules()
  {
    this.modules= new Module();
  }
  
  /**
   * Add other admin modules to us
   * 
   * @param adminModules
   *          modules to add
   */
  public void add(AdminModules adminModules)
  {
    for(String key : adminModules.modules.sub.keySet() )
    {
      if( this.modules.sub.containsKey(key) )
      {
        this.modules.sub.get(key).add(adminModules.modules.sub.get(key));
        // throw new UnsupportedOperationException();
      }
      else
      {
        this.modules.sub.put(key, adminModules.modules.sub.get(key));
      }
    }
  }

  public void addModule(String[] modulePath, String filePath, String componentName, int order)
  {
    this.modules.add(modulePath, filePath, componentName, order);
  }
}
