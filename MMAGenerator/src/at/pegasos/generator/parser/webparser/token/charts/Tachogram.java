package at.pegasos.generator.parser.webparser.token.charts;

import at.pegasos.generator.parser.webparser.token.*;
import org.w3c.dom.*;

import java.nio.file.*;
import java.util.*;

public class Tachogram extends Charts.Chart {
  public static final String TypeName = "Tachogram";
  private final static Collection<String> configImports;
  static
  {
    configImports = new ArrayList<String>(1);
    configImports.add("{ LineChartConfig } from './../shared/chartconfig'");
  }

  public Tachogram(String name)
  {
    super(name);
  }

  @Override public String getAvailConstructor()
  {
    return "new DBChartSimple(\"" + name + "\", Type.Tachogram, \"hr\", \"hr\")";
  }

  @Override public String getTypeName()
  {
    return TypeName;
  }

  @Override public Collection<String> getConfigImports()
  {
    return configImports;
  }

  @Override public Collection<String> getConfigLines(String varname)
  {
    return List.of(new String[] {"const " + varname + "= new LineChartConfig(info);"});
  }

  @Override public void parse(Element e, Path file)
  {

  }

  @Override public void write(Document document, Element e)
  {
    Element node = document.createElement("name");
    node.appendChild(document.createTextNode(name));
    e.appendChild(node);

    e.setAttribute("type", "tachogram");
  }

  @Override public Charts.Chart copy()
  {
    Tachogram ret = new Tachogram(name);
    ret.default_avail = default_avail;
    return ret;
  }
}
