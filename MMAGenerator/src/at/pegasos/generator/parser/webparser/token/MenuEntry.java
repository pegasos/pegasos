package at.pegasos.generator.parser.webparser.token;

import java.util.*;

import lombok.*;

@Setter
@ToString
public class MenuEntry {

  public static final int DEFAULT_ORDER = 99;

  public String action;
  public String name;
  public int order = DEFAULT_ORDER;
  public Boolean isPublic= null;
  public List<String> requiredRoles= new ArrayList<String>(2);
  public final List<MenuEntry> subMenus = new ArrayList<>();
  public boolean isDummy = false;

  public MenuEntry(String action, String name)
  {
    this.action= action;
    this.name= name;
  }

  public MenuEntry(String action, String name, boolean isPublic, int order)
  {
    this.action= action;
    this.name= name;
    this.isPublic= isPublic;
    this.order= order;
  }

  public MenuEntry(String action, String name, boolean isPublic, String[] requiredRoles, int order)
  {
    this.action= action;
    this.name= name;
    this.isPublic= isPublic;
    this.requiredRoles.addAll(Arrays.asList(requiredRoles));
    this.order= order;
  }

  public void replaceDummy(MenuEntry concrete)
  {
    this.action = concrete.action;
    this.isPublic= concrete.isPublic;
    this.requiredRoles.addAll(concrete.requiredRoles);
    this.order = concrete.order;
    this.isDummy = false;
  }
}