package at.pegasos.generator.parser.webparser.token;

import java.util.*;

import lombok.*;

@ToString(callSuper = true)
public class ModuleRoute extends Route {

  public String module;
  
  public ModuleRoute()
  {
    super();
  }

  public ModuleRoute(String path, String module)
  {
    super();
    this.path= path;
    this.module= module;
  }

  public ModuleRoute(String path, String module, Collection<String> guards)
  {
    super(guards);
    this.path= path;
    this.module= module;
  }
}