package at.pegasos.generator.parser.webparser.token.reports;

import org.w3c.dom.*;

import java.util.*;

import at.pegasos.generator.parser.*;
import lombok.*;

@EqualsAndHashCode @ToString
public class Display {

  private static final int DEFAULT_WEEKS = 4;

  public enum ReportType {
    Bars, SessionbasedLines, Lines
  }

  public String title;

  public int defaultWeeks = DEFAULT_WEEKS;

  public ReportType type = ReportType.Bars;

  public Map<String, String> config = new HashMap<>();

  public void parse(Element item) throws ParserException
  {
    title = XMLHelper.getAttribute(item, "title", null);

    String types = XMLHelper.getAttribute(item, "type", null);
    if( types != null )
    {
      try
      {
        type = ReportType.valueOf(types);
      }
      catch(IllegalArgumentException e)
      {
        throw new ParserException("Unknown display type '" + types + "'");
      }
    }

    String h = XMLHelper.getStringValueOfChild(item, "defaultWeeks");
    if( h != null )
      defaultWeeks = Integer.parseInt(h);

    NodeList mList= item.getElementsByTagName("config");
    for(int p= 0; p < mList.getLength(); p++)
    {
      NodeList nList= item.getElementsByTagName("entry");
      for(int i= 0; i < nList.getLength(); i++)
      {
        Node e = nList.item(i);
        config.put(XMLHelper.getAttribute(e, "name", ""), XMLHelper.getAttribute(e, "value", ""));
      }
    }
  }

  public void write(Document document, Element e)
  {
    e.setAttribute("title", title);
    e.setAttribute("type", type.toString());
    if( defaultWeeks != DEFAULT_WEEKS )
    {
      Element x = document.createElement("defaultWeeks");
      x.appendChild(document.createTextNode(String.valueOf(defaultWeeks)));
      e.appendChild(x);
    }

    if( config.size() > 0 )
    {
      Element c = document.createElement("config");
      config.forEach((key, value) -> {
        Element x = document.createElement("entry");
        x.setAttribute("name", key);
        x.setAttribute("value", value);
        c.appendChild(x);
      });
      e.appendChild(c);
    }
  }
}
