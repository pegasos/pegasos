package at.pegasos.generator.parser.webparser.token.reports;

import lombok.*;
import org.w3c.dom.*;

import at.pegasos.generator.parser.*;
import lombok.extern.slf4j.*;

@Slf4j
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MetricBasedAccumulatedReport extends MetricBasedReport {
  public enum GroupBy {
    DAY, WEEK, MONTH, YEAR, ALL
  }

  public enum Aggregate {
    SUM, MAX, MIN, MEAN,
    FIRST, LAST
  }

  public String type;
  public MetricBasedAccumulatedReport.Aggregate aggregate;
  public MetricBasedAccumulatedReport.GroupBy groupBy;

  @Override
  public void parse(Element item) throws ParserException
  {
    super.parse(item);

    String h = XMLHelper.getStringValueOfChild(item, "accumulate");
    // actually h cannot by null unless someone messed up
    if( h != null )
    {
      if( h.equals("none") || h.equalsIgnoreCase("simple") )
        type = "Simple";
      else if( h.equalsIgnoreCase("param") )
        type = "Param";
      else
        throw new ParserException("Unknown accumulation type '" + h + "'");
    }
    else
    {
      type = "Simple";
      log.warn("Parsing accumulated report " + name + " without specification of what to accumulate");
    }

    try
    {
      aggregate = Aggregate.valueOf(XMLHelper.getStringValueOfChild(item, "aggregate"));
    }
    catch( IllegalArgumentException e)
    {
      throw new ParserException("Unknown aggregate type '" + XMLHelper.getStringValueOfChild(item, "aggregate") + "', or missing");
    }
    try
    {
      groupBy = GroupBy.valueOf(XMLHelper.getStringValueOfChild(item, "groupby"));
    }
    catch( IllegalArgumentException e)
    {
      throw new ParserException("Unknown groupBy type '" + XMLHelper.getStringValueOfChild(item, "groupby") + "', or missing");
    }
  }

  @Override
  public void write(Document document, Element e)
  {
    super.write(document, e);

    Element x = document.createElement("accumulate");
    x.appendChild(document.createTextNode(type));
    e.appendChild(x);

    x = document.createElement("aggregate");
    x.appendChild(document.createTextNode(aggregate.toString()));
    e.appendChild(x);

    x = document.createElement("groupby");
    x.appendChild(document.createTextNode(groupBy.toString()));
    e.appendChild(x);
  }
}
