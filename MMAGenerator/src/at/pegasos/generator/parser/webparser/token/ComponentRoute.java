package at.pegasos.generator.parser.webparser.token;

import java.util.*;

import lombok.*;

@ToString(callSuper = true)
public class ComponentRoute extends Route {

  public String component;

  public ComponentRoute()
  {
    super();
  }

  public ComponentRoute(String path, String component)
  {
    super();
    this.path= path;
    this.component= component;
  }

  public ComponentRoute(String path, String component, Collection<String> guards)
  {
    super(guards);
    this.path= path;
    this.component= component;
  }
}