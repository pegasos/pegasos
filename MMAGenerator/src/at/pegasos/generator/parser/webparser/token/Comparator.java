package at.pegasos.generator.parser.webparser.token;

public enum Comparator {
  /**
   * Greater than
   */
  GT,
  /**
   * Lower than
   */
  LT,
  /**
   * Greater or equal than
   */
  GTE,
  /**
   * Lower or equal than
   */
  LTE,
  /**
   * Equal to
   */
  EQ,
  /**
   * Not equal to
   */
  NEQ
}
