package at.pegasos.generator.parser.webparser.token;

import lombok.*;

import java.util.*;

@ToString
public class Repositories {
  public static final String TAG = "backend.repositories";

  @ToString
  public static class Repository {
    public final String name;

    public Repository(String name)
    {
      this.name = name;
    }
  }
  public List<Repository> repositories;

  public Repositories()
  {
    this.repositories = new ArrayList<>();
  }
}
