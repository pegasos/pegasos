package at.pegasos.generator.parser.webparser.token;

import lombok.*;
import org.slf4j.*;

import java.nio.file.*;
import java.util.*;

public class Components {
  private final static Logger logger= LoggerFactory.getLogger(Components.class);

  @Override
  public String toString()
  {
    return "Components [components=" + components + "]";
  }

  @ToString
  public static class Resolvable {
    public final String name;
    public final String file;
    public final Path location;

    private final List<Path> roots;
    
    public final List<Path> getRoots()
    {
      return roots;
    }

    /**
     * Keep in mind that we always assume we are in 'src/app'!
     *
     * @param name
     *          name of the component
     * @param file
     *          name of the file (i.e. excluding the file extension)
     * @param location
     *          location of the component (i.e. folder where it is stored)
     */
    public Resolvable(String name, String file, String location)
    {
      this.name= name;
      this.file= file;
      // If the location does not end with a / then Java will think it is a file and not a directory
      if( !location.endsWith("/") )
        location+= "/";
      this.location= Paths.get(location);
      
      Path p= this.location;
      
      roots= new ArrayList<Path>();
      
      roots.add(p);
      while(p.getParent() != null )
      {
        p= p.getParent();
        roots.add(p);
      }
      roots.add(Paths.get(""));
    }
    
    public String resolve(String location)
    {
      List<Path> roots= new ArrayList<Path>();
      if( !location.equals("") )
      {
        Path p= Paths.get(location);

        roots.add(p);
        while(p.getParent() != null )
        {
          p= p.getParent();
          roots.add(p);
        }
      }
      roots.add(Paths.get(""));

      int idxM= 0;
      int idxO= 0;
      boolean match= false;
      while( idxO < roots.size() )
      {
        idxM= 0;
        while( idxM < this.roots.size() )
        {
          // System.out.println(roots.get(idxO) + " " + this.roots.get(idxM));
          if( roots.get(idxO).equals(this.roots.get(idxM)) )
          {
            // System.out.println("Match other root:'" + roots.get(idxO) + "' myroot:'" + this.roots.get(idxM) + "' " + idxO + " " + idxM);
            match= true;
            break;
          }
          idxM++;
        }
        if( match )
          break;
        idxO++;
      }

      // found
      if( match )
      {
        StringBuilder ret= new StringBuilder();
        // we are not in the same directory
        if( idxM > 0 && idxO > 0 )
        {
          while( idxO-- > 0 )
          {
            ret.append("../");
          }
          ret.append(this.roots.get(idxM - 1)).append("/");
        }
        else if( idxM == 0 )
        {
          // we are in the same directory;
          ret.append("./");
        }
        else if( idxO == (roots.size() - 1) )
        {
          ret.append("./");
          while( idxM-- > 0 )
          {
            ret.append(this.roots.get(idxM)).append("/");
          }
        }
        else
        {
          // we end up with a weird path ... but who cares (for now)
          while( idxM-- > 0 )
          {
            ret.append("../");
          }
          ret.append(this.roots.get(0)).append("/");
        }

        return ret.toString();
      }
      
      return null;
    }
  }

  public static class Component extends Resolvable {
    public final boolean declare;
    public final boolean entry;

    public Component(String name, String file, String location, boolean declare, boolean entry)
    {
      super(name, file, location);

      this.declare= declare;
      this.entry= entry;
    }

    @Override
    public String toString()
    {
      return "Component [name=" + name + ", file=" + file + ", location=" + location + ", declare=" + declare + ", entry=" + entry
          + ", roots=" + getRoots() + "]";
    }
  }

  // private final Set<Component> components;
  private final Map<String, Component> components;
  
  public Components()
  {
    // components= new HashSet<Component>();
    components= new HashMap<String, Component>();
    
    components.put("LoginComponent", new Component("LoginComponent", "login.component", "login/", false, false));
    components.put("HomeComponent", new Component("HomeComponent", "home.component", "home/", false, false));
    components.put("ActivityListComponent", new Component("ActivityListComponent", "activity-list.component", "activities/", false, false));
    components.put("ActivityDetailComponent", new Component("ActivityDetailComponent", "activity-detail.component", "activities/", false, false));
    components.put("ReportsMainComponent", new Component("ReportsMainComponent", "reports-main.component", "reports/", false, false));
    components.put("TeamComponent", new Component("TeamComponent", "team.component", "team/", false, false));
    components.put("UserComponent", new Component("UserComponent", "user.component", "user/", false, false));
    components.put("UserValueComponent", new Component("UserValueComponent", "user.value.component", "user/", false, false));
    components.put("LiveComponent", new Component("LiveComponent", "live.component", "live/", false, false));
    components.put("AdminComponent", new Component("AdminComponent", "admin.component", "admin/", false, false));
    components.put("MenuGuard", new Component("MenuGuard", "menu.guard", "app-routing/", false, false));
    components.put("LoggedInGuard", new Component("LoggedInGuard", "loggedin.guard", "app-routing/", false, false));
  }

  /**
   * Keep in mind that we always assume we are in 'src/app'!
   *
   * @param name
   *          name of the component
   * @param file
   *          name of the file (i.e. excluding the file extension)
   * @param location
   *          location of the component (i.e. folder where it is stored)
   */
  public void addComponent(String name, String file, String location, boolean declare, boolean entry)
  {
    this.components.put(name, new Component(name, file, location, declare, entry));
  }
  
  /**
   * Get the path to a component in relation to a location
   *
   * @param name
   *          name of the component which should be resolved
   * @param location
   *          location/path from where the component should be resolved
   * @return path to the component i.e. filename excluding extension
   */
  public String resolve(String name, String location)
  {
    Component c= components.get(name);
    String res= c.resolve(location) + c.file;
    logger.debug("Resolving " + c + " from " + location + " = " + res);
    return res;
  }
  
  /**
   * Get the path to a component in relation to a location
   *
   * @param component
   *          component to be resolve
   * @param location
   *          location/path from where it should be resolved
   * @return path to the component i.e. filename excluding extension
   */
  public String resolve(Component component, String location)
  {
    String res= component.resolve(location) + component.file;
    logger.debug("Resolving " + component + " from " + location + " = " + res);
    return res;
  }

  public void add(Components other)
  {
    this.components.putAll(other.components);
  }

  public Collection<Component> entries()
  {
    return this.components.values();
  }
}
