package at.pegasos.generator.parser.webparser.token.reports;

import at.pegasos.generator.parser.*;
import lombok.*;
import org.w3c.dom.*;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SessionValueReport extends Report {

  @EqualsAndHashCode
  public static class SessionDataFilter {
    /**
     * Which table to use
     */
    public String table;
    /**
     * Which field to report
     */
    public String field;
    /**
     * Name to use in the output
     */
    public String name;
    /**
     * Additional filter to use. If null no additional filter is applied
     */
    public String where;

    public void parse(Element e)
    {
      table = e.getAttribute("table");
      field = e.getAttribute("field");
      name = e.getAttribute("name");
      where = XMLHelper.getAttribute(e, "where", null);
    }

    public Node write(Document document)
    {
      Element value = document.createElement("data");
      value.setAttribute("table", table);
      value.setAttribute("field", field);
      value.setAttribute("name", name);
      if ( where != null )
        value.setAttribute("where", where);
      return value;
    }
  }

  @EqualsAndHashCode
  public static class DataQuery {
    public SessionDataFilter[] filter;

    /**
     * parse data query
     * @param item xml element to parse
     * @param name name of the report
     */
    public void parse(Element item, String name) throws ParserException
    {
      if( (item.getElementsByTagName("dataQuery")).getLength() > 0 )
      {
        NodeList values = ((Element) (item.getElementsByTagName("dataQuery")).item(0)).getElementsByTagName("data");
        if( values.getLength() > 0 )
        {
          filter = new SessionDataFilter[values.getLength()];
          for(int i = 0; i < filter.length; i++)
          {
            filter[i]= new SessionDataFilter();
            filter[i].parse((Element) values.item(i));
          }
        }
      }
      else
        throw new ParserException("SessionValueReport(" + name + ") without specified dataQuery");
    }

    public void write(Document document, Element e)
    {
      Element values = document.createElement("dataQuery");
      for(SessionDataFilter data : filter)
      {
        values.appendChild(data.write(document));
      }
      e.appendChild(values);
    }
  }

  public DataQuery dataQuery;
  public MetricBasedReport.SessionTypeFilters includeFilters;

  @Override
  public void parse(Element item) throws ParserException
  {
    super.parse(item);

    dataQuery = new DataQuery();
    dataQuery.parse(item, name);

    if( (item.getElementsByTagName("sessions")).getLength() > 0 )
    {
      includeFilters = new MetricBasedReport.SessionTypeFilters();
      includeFilters.parse(item);
    }
  }

  @Override
  public void write(Document document, Element e)
  {
    super.write(document, e);

    dataQuery.write(document, e);

    if( includeFilters != null )
    {
      includeFilters.write(document, e);
    }
  }
}
