package at.pegasos.generator.parser.webparser.token;

import at.pegasos.generator.parser.*;
import at.pegasos.util.*;
import at.pegasos.util.DAG.*;
import lombok.extern.slf4j.*;

import java.util.*;
import java.util.stream.*;

@Slf4j
public class Exports {

  public static class Export {
    /**
     * Short name describing the export type
     */
    public String typeName;

    public Export(String typeName)
    {
      this.typeName = typeName;
    }

    public String toString()
    {
      return typeName;
    }
  }

  public static class ExportType {
    /**
     * Short name describing the export type
     */
    public String typeName;
    /**
     * Class performing the export
     */
    public String className;

    public ExportType(String typeName, String className)
    {
      this.typeName = typeName;
      this.className = className;
    }

    public String toString()
    {
      return typeName + "-" + className;
    }
  }

  public static class ExportActivity extends at.pegasos.generator.parser.token.Activity {
    public List<Export> exports = new ArrayList<>();

    public String toString()
    {
      return param0 + " " + param1 + " " + param2 + " " + exports;
    }
  }

  DAG<ExportActivity> activities;

  private final ExportActivity root;

  private final Node<ExportActivity> rootnode;
  private final Map<String,ExportType> exportTypes = new HashMap<>();
  private final Set<String> declaredExportTypes = new HashSet<>();

  public Exports()
  {
    activities = new DAG<ExportActivity>();
    root = new ExportActivity();
    this.rootnode = activities.addNode(root);

    exportTypes.put("csv", new ExportType("csv", "at.pegasos.server.frontback.exports.CsvExport"));
  }

  public Node<ExportActivity> getRoot()
  {
    return rootnode;
  }
  
  public void add(Exports other)
  {
    this.exportTypes.putAll(other.exportTypes);
    this.declaredExportTypes.addAll(other.declaredExportTypes);
    this.rootnode.mergeNode(this.activities, other.rootnode, new NodeMerger<ExportActivity>() {

      @Override
      public void run()
      {
        if( a.getID().exports == null && b.getID().exports != null )
        {
          a.getID().exports = b.getID().exports;
        }
        else if( a.getID().exports != null && b.getID().exports != null )
        {
          for(Exports.Export export : b.getID().exports)
          {
            // check if this export has not been added already
            if( a.getID().exports.stream().noneMatch(aExport -> aExport.typeName.equals(export.typeName)) )
              a.getID().exports.add(export);
          }
        }
      }
    });
  }

  public void addType(ExportType exportType)
  {
    this.declaredExportTypes.add(exportType.typeName);
    this.exportTypes.put(exportType.typeName, exportType);
  }

  public ExportActivity getAvailNode(String param0, String param1, String param2) throws ParserException
  {
    if( !param0.equals("") )
    {
      ExportActivity h = new ExportActivity();
      h.param0 = Integer.parseInt(param0);

      if( !param1.equals("") )
      {
        ExportActivity h1 = new ExportActivity();
        h1.param0 = Integer.parseInt(param0);
        h1.param1 = Integer.parseInt(param1);

        if( !param2.equals("") )
        {
          ExportActivity a = new ExportActivity();
          a.param0 = Integer.parseInt(param0);
          a.param1 = Integer.parseInt(param1);
          a.param2 = Integer.parseInt(param2);

          if( activities.getNode(a) != null )
          {
            return activities.getNode(a).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }
          if( activities.getNode(h1) == null )
          {
            activities.addNode(h1);
            activities.addEdgeNC(h, h1);
          }

          activities.addNode(a);
          activities.addEdgeNC(h1, a);

          return activities.getNode(a).getID();
        }
        else
        {
          if( activities.getNode(h1) != null )
          {
            System.out.println("return existing");
            return activities.getNode(h1).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }

          activities.addNode(h1);
          activities.addEdgeNC(h, h1);

          return activities.getNode(h1).getID();
        }
      }
      else
      {
        // Check if parent exists
        if( activities.getNode(h) == null )
        {
          // Parent does not exist -> add it
          activities.addNode(h);
          activities.addEdgeNC(root, h);
        }

        return activities.getNode(h).getID();
      }
    }
    throw new ParserException("Invalid specification for activity. param0 missing");
  }

  public Collection<ExportType> getTypes()
  {
    return exportTypes.values();
  }

  public Collection<String> getDeclaredTypes()
  {
    return declaredExportTypes;
  }

  public ExportType getType(String name)
  {
    return exportTypes.get(name);
  }

  public List<ExportActivity> getAllExports()
  {
    return rootnode.getAllChildren().stream().map(Node::getID).collect(Collectors.toList());
  }
}
