package at.pegasos.generator.parser.webparser.token;

import java.util.*;
import java.util.Map.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.Components.*;
import lombok.extern.slf4j.*;

@Slf4j
public class Modules {

  @Override
  public String toString()
  {
    return "Modules [modules=" + modules + "]";
  }

  public static class Module extends Resolvable {

    private final boolean isDefault;

    public Module(String name, String file, String location, boolean defaultMod)
    {
      super(name, file, location);
      this.isDefault = defaultMod;
    }

    public boolean isDefault()
    {
      return isDefault;
    }
  }

  // private final Set<Component> components;
  private final Map<String, Module> modules;

  public Modules()
  {
    // components= new HashSet<Component>();
    modules= new HashMap<String, Module>();
  }

  /**
   * Keep in mind that we always assume we are in 'src/app'!
   *
   * @param name
   *          name of the component
   * @param file
   *          name of the file (i.e. excluding the file extension)
   * @param location
   *          location of the component (i.e. folder where it is stored)
   */
  public void addModule(String name, String file, String location)
  {
    this.modules.put(name, new Module(name, file, location, false));
  }

  /**
   * Get the path to a component in relation to a location
   *
   * @param name
   *          name of the component which should be resolved
   * @param location
   *          location/path from where the component should be resolved
   * @return path to the component i.e. filename excluding extension
   */
  public String resolve(String name, String location) throws GeneratorException
  {
    Module c= modules.get(name);
    if( c == null )
      throw new GeneratorException("No module with name '"+name+"' exists");
    String res= c.resolve(location) + c.file;
    log.debug("Resolving " + c + " from " + location + " = " + res);
    return res;
  }

  /**
   * Get the path to a component in relation to a location
   *
   * @param component
   *          component to be resolve
   * @param location
   *          location/path from where it should be resolved
   * @return path to the component i.e. filename excluding extension
   */
  public String resolve(Module component, String location)
  {
    String res= component.resolve(location) + component.file;
    log.debug("Resolving " + component + " from " + location + " = " + res);
    return res;
  }

  public void add(Modules other)
  {
    for(Entry<String, Module> entry : other.modules.entrySet())
    {
      this.modules.put(entry.getKey(), entry.getValue());
    }
  }

  public Collection<Module> entries()
  {
    return this.modules.values();
  }

  public void removeModule(Module e)
  {
    this.modules.remove(e.name);
  }

  public void addModule(Module e)
  {
    this.modules.put(e.name, e);
  }

  public void updateModule(Module e)
  {
    this.modules.put(e.name, e);
  }
}
