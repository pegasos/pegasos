package at.pegasos.generator.parser.webparser.token;

import java.nio.file.*;
import java.util.*;
import java.util.Map.Entry;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import at.pegasos.util.DAG.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;

import at.pegasos.util.DAG;
import at.pegasos.generator.parser.XMLHelper;

@Slf4j
public class Charts {
  @Override
  public String toString()
  {
    return "Charts [charts=" + charts + ", charttypes=" + charttypes + ", activities=" + activities + "]";
  }

  public static abstract class Chart {
    @Override
    public String toString()
    {
      return "Chart [name=" + name + ", default_avail=" + default_avail + "]";
    }

    @Override public boolean equals(Object o)
    {
      if( this == o )
        return true;
      if( o == null || getClass() != o.getClass() )
        return false;
      Chart chart = (Chart) o;
      return default_avail == chart.default_avail && name.equals(chart.name);
    }

    /**
     * A unique identifier for this chart
     */
    public String name;
    
    /**
     * Whether the chart is available as default chart
     */
    public boolean default_avail = true;
    
    public Chart(String name)
    {
      this.name= name;
    }

    /**
     * Extract information about the chart from the xml element.
     * @param e xml element containing the chart
     * @param file File in which the xml element / chart is contained (can be used for errors
     */
    public abstract void parse(Element e, Path file) throws ParserException;

    /**
     * Get the constructor for the internal representation in the backend.
     * This constructor needs to populate the internal class with all necessary data.
     * The instance is used for the AvailableCharts utility
     * 
     * @return constructor to be used by the generator
     */
    public abstract String getAvailConstructor();

    /**
     * Get the necessary imports for the configuration on the frontend (typescript). If this chart
     * does not require any special configuration you can return an empty list/collection. The
     * generator takes care of all imports so you do not have to worry about duplicated imports (as
     * long as they are identical)
     * 
     * <br/>
     * The following lines are present by default:
     * <ul>
     * <li>{ ChartConfig } from './../shared/chartconfig'</li>
     * <li>{ ChartInfo } from './../shared/sample'</li>
     * </ul>
     * 
     * @return collection with all necessary imports
     */
    public abstract Collection<String> getConfigImports();
    
    /**
     * Return the code required for the configuration of this chart on the frontend. See DBChart for an example.
     * 
     * @param varname variable to be used for this configuration
     * @return lines or null if no configuration is necessary
     */
    public abstract Collection<String> getConfigLines(String varname);

    public abstract String getTypeName();

    public abstract Chart copy();

    public void write(Document document, Element e)
    {
      Element node = document.createElement("name");
      node.appendChild(document.createTextNode(name));
      e.appendChild(node);
    }
  }

  public static abstract class LineChart extends ChartWithConfig {

    protected final static Set<String> allowedConfigStrings;
    protected final static Set<String> quotedConfigs;

    static {
      allowedConfigStrings = new HashSet<String>();
      allowedConfigStrings.add("filterymax");
      allowedConfigStrings.add("filterymin");
      allowedConfigStrings.add("yaxislabel");
      allowedConfigStrings.add("scaley");
      allowedConfigStrings.add("kernel");
      quotedConfigs = new HashSet<>();
      quotedConfigs.add("kernel");
      quotedConfigs.add("yaxislabel");
    }

    @Override public boolean equals(Object o)
    {
      if( this == o )
        return true;
      if( o == null || getClass() != o.getClass() )
        return false;
      return super.equals(o);
    }

    @Override  public Set<String> getQuotedValues()
    {
      return quotedConfigs;
    }

    public LineChart(String name) {
      super(name);
    }
  }

  public static abstract class ChartWithConfig extends Chart {

    private final Map<String, String> configs;

    public ChartWithConfig(String name)
    {
      super(name);
      configs = new HashMap<String, String>();
    }

    @Override public boolean equals(Object o)
    {
      if( this == o )
        return true;
      if( o == null || getClass() != o.getClass() )
        return false;
      if( !super.equals(o) )
        return false;
      ChartWithConfig that = (ChartWithConfig) o;
      return configs.equals(that.configs);
    }

    public void setConfigs(Map<String,String> configs)
    {
      this.configs.clear();
      this.configs.putAll(configs);
    }

    public Map<String, String> getConfigs()
    {
      return configs;
    }

    public abstract Set<String> getAllowedValues();

    public abstract Set<String> getQuotedValues();
    
    protected Collection<String> getSimpleConfigImportLines(String varname)
    {
      List<String> lines= new ArrayList<String>(configs.size());
      Set<String> quotes = getQuotedValues();
      
      for(Entry<String, String> e : configs.entrySet())
      {
        if( quotes.contains(e.getKey()) )
          lines.add(varname + "." + e.getKey() + "= '" + e.getValue() + "';");
        else
          lines.add(varname + "." + e.getKey() + "= " + e.getValue() + ";");
      }

      return lines;
    }

    public void config(String variable, String value)
    {
      configs.put(variable, value);
    }
    
    public void getSimpleConfigs(Element e)
    {
      for(String config : getAllowedValues())
      {
        String val= XMLHelper.getStringValueOfChild(e, config);
        if( val != null )
        {
          configs.put(config, val);
        }
      }
    }

    protected void writeSimpleConfigs(Document document, Element e)
    {
      for(String config : getAllowedValues())
      {
        String val = configs.get(config);
        if( val != null )
        {
          Element node = document.createElement(config);
          node.appendChild(document.createTextNode(val));
          e.appendChild(node);
        }
      }
    }

    protected void copySimpleConfig(ChartWithConfig other)
    {
      for(String config : getAllowedValues())
      {
        if( other.configs.containsKey(config) )
          configs.put(config, other.configs.get(config));
      }
    }
  }

  private final Map<String, Chart> charts;
  
  private final Set<Type> charttypes;
  
  public DAG<Activity> activities;
  
  private final Activity root;
  
  private final DAG.Node<Activity> rootnode;
  
  public void debugTree()
  {
    log.debug("----> Charts.debugTree");
    for(Activity x : activities.getNodes())
    {
      // System.out.println(x + " " + x.hashCode());
      log.debug("{} / hash:{}", x, x.hashCode());
    }
    log.debug("<---- Charts.debugTree");
  }
  
  public Charts()
  {
    this.charts= new LinkedHashMap<String, Chart>();
    activities= new DAG<Activity>();
    root= new Activity();
    this.rootnode= activities.addNode(root);
    this.charttypes= new HashSet<Type>();
  }
  
  public void addChart(Chart chart) throws ParserException
  {
    log.debug("Adding chart {}", chart);
    if( charts.get(chart.name) != null )
      throw new ParserException(chart.name + " was already defined");
    
    charts.put(chart.name, chart);
  }

  public void replaceChart(Chart rowData, Chart chart)
  {
    this.charts.put(rowData.name, chart);
  }

  public void addOther(Charts other) throws ParserException
  {
    for(Chart e : other.charts.values())
    {
      addChart(e);
    }

    debugTree();
    this.rootnode.mergeNode(this.activities, other.rootnode, new NodeMerger<Activity>() {
      
      @Override
      public void run()
      {
        log.debug("Merge {} with {}", a.getID(), b.getID());
        if( a.getID().chartsPre == null && b.getID().chartsPre != null )
        {
          a.getID().chartsPre = b.getID().chartsPre;
        }
        else if( a.getID().chartsPre != null && b.getID().chartsPre != null )
        {
          a.getID().chartsPre.addAll(b.getID().chartsPre);
        }

        if( a.getID().chartsPost == null && b.getID().chartsPost != null )
        {
          a.getID().chartsPost = b.getID().chartsPost;
        }
        else if( a.getID().chartsPost != null && b.getID().chartsPost != null )
        {
          a.getID().chartsPost.addAll(b.getID().chartsPost);
        }
        // 3rd + 4th case are * both null, or b has no charts but a has. both cases require no action

        if( b.getID().adddefault )
          a.getID().adddefault = true;
      }
    });
    debugTree();
    
    this.charttypes.addAll(other.charttypes);
  }
  
  public DAG.Node<Activity> getRoot()
  {
    return rootnode;
  }

  public Collection<Chart> getCharts()
  {
    return charts.values();
  }

  public Activity getAvailNode(String param0, String param1, String param2) throws ParserException
  {
    if( !param0.equals("") )
    {
      Activity h = new Activity();
      h.param0 = Integer.parseInt(param0);

      if( !param1.equals("") )
      {
        Activity h1 = new Activity();
        h1.param0 = Integer.parseInt(param0);
        h1.param1 = Integer.parseInt(param1);

        if( !param2.equals("") )
        {
          Activity a = new Activity();
          a.param0 = Integer.parseInt(param0);
          a.param1 = Integer.parseInt(param1);
          a.param2 = Integer.parseInt(param2);

          if( activities.getNode(a) != null )
          {
            return activities.getNode(a).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }
          if( activities.getNode(h1) == null )
          {
            activities.addNode(h1);
            activities.addEdgeNC(h, h1);
          }

          activities.addNode(a);
          activities.addEdgeNC(h1, a);

          return activities.getNode(a).getID();
        }
        else
        {
          if( activities.getNode(h1) != null )
          {
            return activities.getNode(h1).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }

          activities.addNode(h1);
          activities.addEdgeNC(h, h1);

          return activities.getNode(h1).getID();
        }
      }
      else
      {
        // Check if parent exists
        if( activities.getNode(h) == null )
        {
          // Parent does not exist -> add it
          activities.addNode(h);
          activities.addEdgeNC(root, h);
        }

        return activities.getNode(h).getID();
      }
    }
    throw new ParserException("Invalid specification for activity. param0 missing");
  }

  public Activity getAvailNode(Activity activity) throws ParserException
  {
    DAG.Node<Activity> inTree = this.activities.getNode(activity);
    if( inTree != null )
      return inTree.getID();
    if( activity.param0 != -1 )
    {
      Activity h = new Activity();
      h.param0 = activity.param0;

      if( activity.param1 != -1 )
      {
        Activity h1 = new Activity();
        h1.param0 = activity.param0;
        h1.param1 = activity.param1;

        if( activity.param2 != -1 )
        {
          Activity a = new Activity();
          a.param0 = activity.param0;
          a.param1 = activity.param1;
          a.param2 = activity.param2;

          if( activities.getNode(a) != null )
          {
            return activities.getNode(a).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }
          if( activities.getNode(h1) == null )
          {
            activities.addNode(h1);
            activities.addEdgeNC(h, h1);
          }

          activities.addNode(a);
          activities.addEdgeNC(h1, a);

          return activities.getNode(a).getID();
        }
        else
        {
          if( activities.getNode(h1) != null )
          {
            return activities.getNode(h1).getID();
          }

          // Check if parent exists
          if( activities.getNode(h) == null )
          {
            // Parent does not exist -> add it
            activities.addNode(h);
            activities.addEdgeNC(root, h);
          }

          activities.addNode(h1);
          activities.addEdgeNC(h, h1);

          return activities.getNode(h1).getID();
        }
      }
      else
      {
        // Check if parent exists
        if( activities.getNode(h) == null )
        {
          // Parent does not exist -> add it
          activities.addNode(h);
          activities.addEdgeNC(root, h);
        }

        return activities.getNode(h).getID();
      }
    }
    throw new ParserException("Invalid specification for activity. param0 missing");
  }

  public Set<Type> getTypes()
  {
    Set<Type> ret = new TreeSet<>();
    for(Charts.Chart chart : charts.values())
    {
      if( chart instanceof GeneratedChartType )
      {
        Type t = ((GeneratedChartType) chart).getType();
        if( t != null )
          ret.add(t);
      }
    }
    return ret;
  }
}
