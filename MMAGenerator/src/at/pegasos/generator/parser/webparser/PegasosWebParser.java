package at.pegasos.generator.parser.webparser;

import org.w3c.dom.*;

import java.lang.annotation.*;

import at.pegasos.generator.configurator.writer.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface PegasosWebParser {
  Class<? extends Writer> writer() default DEFAULT_WRITER.class;

  String name() default "";

  int order() default 99;

  /**
   * This inner class only exists as a workaround so that parsers don't have to supply a writer
   */
  final class DEFAULT_WRITER extends Writer {
    public DEFAULT_WRITER(Document document, Element root)
    {
      super(document, root);
    }

    @Override
    public void write()
    {
    }
  }
}
