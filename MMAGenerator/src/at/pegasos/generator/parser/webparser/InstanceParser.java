package at.pegasos.generator.parser.webparser;

import java.lang.reflect.*;
import java.nio.file.*;
import java.util.*;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.*;

@Slf4j
public class InstanceParser extends PegasosModuleParser
{
  private final ServerInstance instance;
  private final Path path;

  public InstanceParser(ServerInstance instance, Path path)
  {
    this.instance = instance;
    this.path = path;
  }

  public void parse() throws ParserException
  {
    log.debug("parse dynamic parsers");
    List<Class<? extends PegasosParser>> parsers =
            DynamicModulesLoader.fetchParsers(PegasosWebParserInterface.class, PegasosWebParser.class,
                    WebParser.class.getPackageName());

    for(Class<?> clasz : parsers)
    {
      try
      {
        PegasosWebParserInterface p = (PegasosWebParserInterface) clasz.getConstructor(IInstance.class, Path.class)
                .newInstance(instance, path);
        p.parse();
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
              | NoSuchMethodException | SecurityException e )
      {
        log.debug("Parser {} does not feature instance parsing", clasz.getName());
        log.error("{}", clasz.getName(), e);
      }
    }
  }
}
