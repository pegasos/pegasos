package at.pegasos.generator.parser.webparser;

import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.util.*;
import org.slf4j.*;
import org.w3c.dom.Node;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import javax.xml.parsers.*;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.webparser.token.*;

/**
 * This parser is used for both the whole server and for each app / library individually  
 */
public class WebParser extends PegasosModuleParser {
  private final static Logger log= LoggerFactory.getLogger(WebParser.class);

  private final IInstance inst;

  private final boolean defaults;
  
  protected Path file;
  
  protected Document doc;
  
  protected void xml_open() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder= dbFactory.newDocumentBuilder();
      doc= dBuilder.parse(file.toFile());
      doc.getDocumentElement().normalize();
    }
    catch (ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  /**
   * 
   * @param inst
   * @param defaults
   *          whether parameters (config, menu) should be pre-populated with defaults
   */
  public WebParser(IInstance inst, boolean defaults)
  {
    // Set file for XMLParser. Inst can be null since when used in a static context
    if( inst != null )
      file= inst.directory.resolve("web.xml");
    else
      file= null;

    this.inst= inst;

    this.defaults= defaults;
  }

  // @Override
  public void parse() throws ParserException
  {
    if( defaults )
    {
      if( inst instanceof ServerInstance )
        inst.getWeb().config = ConfigDefaults.get(((ServerInstance) inst).name, "web");
      else
        throw new IllegalStateException("We should never get here. Defaults are only used for servers");
    }

    log.debug("WebParser " + file + " exist?: " + Files.exists(file));
    if( Files.exists(file) )
    {
      xml_open();
      
      parseConfig();
      parseComponents();

      List<? extends PegasosWebXMLParserInterface> parsers = fetchParsers();

      for(PegasosWebXMLParserInterface parser: parsers)
      {
        parser.parse();
      }
    }
  }

  public List<? extends PegasosWebXMLParserInterface> fetchParsers()
  {
    List<Class<? extends PegasosParser>> parsersClasses = DynamicModulesLoader.fetchParsers(PegasosWebXMLParserInterface.class, PegasosWebParser.class, getClass().getPackage().getName());
    @SuppressWarnings("unchecked") // this is safe as we type check before adding to the list
            List<? extends PegasosWebXMLParserInterface> parsers = (List<? extends PegasosWebXMLParserInterface>) parsersClasses
            .stream()
            // Sort the parsers according to their order
            .sorted((a, b) -> {
              PegasosWebParser an = a.getAnnotationsByType(PegasosWebParser.class)[0];
              PegasosWebParser bn = b.getAnnotationsByType(PegasosWebParser.class)[0];
              return an.order() - bn.order();
            })
            .map(c -> {
              try
              {
                return c.getConstructor(IInstance.class, Boolean.class).newInstance(inst, defaults);
              } catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e )
              {
                e.printStackTrace();
              }
              return null;
            }).collect(Collectors.toList());
    return parsers;
  }

  private void parseConfig() throws ParserException
  {
    ConfigParser configParser;
    if( this.inst.getWeb().config == null )
      this.inst.getWeb().config = new Config();
    configParser= new ConfigParser(this.inst.getWeb().config);
    configParser.setInputFile(file);
    configParser.parse();
  }

  private void parseComponents()
  {
    NodeList nList= doc.getElementsByTagName("components");
    
    for(int temp= 0; temp < nList.getLength(); temp++)
    {
      log.info("Parsing components");
      
      NodeList mList= ((Element) nList.item(temp)).getElementsByTagName("component");
      for(int p= 0; p < mList.getLength(); p++)
      {
        Node route= mList.item(p);
        String name= XMLHelper.getAttribute(route, "name", null);
        String path= XMLHelper.getAttribute(route, "path", null);
        String file= XMLHelper.getAttribute(route, "file", null);
        boolean declare= Boolean.valueOf(XMLHelper.getAttribute(route, "declare", "false"));
        boolean entry= Boolean.valueOf(XMLHelper.getAttribute(route, "entry", "false"));
        
        log.debug("Adding component " + name + " f:" + file + " p:" + path + " d:" + declare + " e:" + entry);
        
        inst.getWeb().addComponent(name, file, path, declare, entry);
      }
    }
  }

  /**
   * add source to target
   * @param target target web
   * @param source source web
   * @throws ParserException in case an error occurs during adding
   */
  public static void add(Web target, Web source) throws ParserException
  {
    List<Class<? extends PegasosParser>> parsers = DynamicModulesLoader.fetchParsers(PegasosWebXMLParserInterface.class, PegasosWebParser.class,
            WebParser.class.getPackageName());

    for(Class<?> clasz : parsers)
    {
      try
      {
        PegasosWebXMLParserInterface p= (PegasosWebXMLParserInterface) clasz.getConstructor(IInstance.class, Boolean.class).newInstance(null, false);
        p.add(target, source);
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e )
      {
        throw new ParserException(e);
      }
    }
  }
}
