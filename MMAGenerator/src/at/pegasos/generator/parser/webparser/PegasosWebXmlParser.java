package at.pegasos.generator.parser.webparser;

import java.nio.file.Files;

import javax.annotation.*;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.IInstance;
import at.univie.mma.pegasos.util.XMLParser;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class PegasosWebXmlParser extends XMLParser implements PegasosWebXMLParserInterface {

  protected @Nullable IInstance inst;
  private final boolean defaults;

  public PegasosWebXmlParser(@Nullable IInstance inst, boolean defaults)
  {
    // Set file for XMLParser
    super(inst != null ? inst.directory.resolve("web.xml") : null);

    this.inst= inst;

    this.defaults= defaults;
  }

  public boolean useDefaults()
  {
    return defaults;
  }

  @Override
  public void parse() throws ParserException
  {
    log.debug("{} " + file + " exist?: {}", this.getClass().getName(), Files.exists(file));
    if( Files.exists(file) )
    {
      xml_open();

      concreteParse();
    }
    else
    {
      onFileMissing();
    }
  }

  protected abstract void onFileMissing() throws ParserException;

  protected abstract void concreteParse() throws ParserException;

  protected void fileMissingDefault()
  {
    // When web is missing we do not have to do something
  }
}
