package at.pegasos.generator.parser.docker.token.services;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.docker.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class FrontendWebService extends Service {
  public FrontendWebService(ServerInstance instance, String backendService) throws GeneratorException
  {
    new Service();
    LinkedHashMap<String,Object> args = new LinkedHashMap<>();
    args.put("ARG_NAME", instance.name);
    build = new Build(".", "./frontend/server-frontend-web.yml", args);

    ports = List.of("80:80");

    /*volumes = new ArrayList<String>();
    volumes.add("backend-volume:/var/backend");
    volumes.add("backend-data-volume:/var/data");*/
    depends_on = List.of(backendService);
  }
}
