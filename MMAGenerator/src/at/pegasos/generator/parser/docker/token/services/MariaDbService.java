package at.pegasos.generator.parser.docker.token.services;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.docker.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class MariaDbService extends Service {
  private final String databaseVolume;
  public ArrayList<String> environment;

  // public LinkedHashMap<String, String> healthcheck;
  public Healthcheck healthcheck;

  public String container_name;

  public MariaDbService(ServerInstance instance, String containerName) throws GeneratorException
  {
    databaseVolume = instance.name + "-db";
    volumes = new ArrayList<String>();
    volumes.add(databaseVolume + ":/var/lib/mysql");

    this.container_name = containerName;

    /*
    TODO:
    Docker Secrets

As an alternative to passing sensitive information via environment variables, _FILE may be appended to the previously listed environment variables, causing the initialization script to load the values for those variables from files present in the container. In particular, this can be used to load passwords from Docker secrets stored in /run/secrets/<secret_name> files. For example:

$ docker run --name some-mysql -e MARIADB_ROOT_PASSWORD_FILE=/run/secrets/mysql-root -d mariadb:latest

Currently, this is only supported for MARIADB_ROOT_PASSWORD, MARIADB_ROOT_PASSWORD_HASH, MARIADB_ROOT_HOST, MARIADB_DATABASE, MARIADB_USER, MARIADB_PASSWORD and MARIADB_PASSWORD_HASH (and MYSQL_* equivalents of these).
     */
    environment = new ArrayList<>();
    try
    {
      environment.add("MARIADB_ROOT_PASSWORD:" +
              (instance.config.hasValue("db.rootpassword") ?
                      instance.config.getValue("db.rootpassword") :
                      "pegasosrootpassword!"));
      environment.add("MARIADB_DATABASE:" + instance.config.getValue("db.name"));
    }
    catch (ParserException e)
    {
      throw new GeneratorException(e);
    }

    healthcheck = new Healthcheck();
    healthcheck.test = List.of("CMD", "/usr/local/bin/healthcheck.sh", "--su-mysql", "--connect", "--innodb_initialized");
    healthcheck.interval = "5s";
    healthcheck.timeout = "5s";
    healthcheck.retries = 5;
  }

  public String getDatabaseVolume()
  {
    return databaseVolume;
  }
}
