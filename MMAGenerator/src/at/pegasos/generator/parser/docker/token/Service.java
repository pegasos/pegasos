package at.pegasos.generator.parser.docker.token;

import java.util.*;

public class Service {

  public Build build = new Build("./database", "database-mariadb.yml");

  public List<String> depends_on;

  public List<String> ports;

  public ArrayList<String> volumes;
}
