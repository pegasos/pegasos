package at.pegasos.generator.parser.docker.token;

import java.util.*;

public class Build {
  public String context;
  public String dockerfile;

  public LinkedHashMap<String,Object> args;

  public Build(String context, String dockerfile)
  {
    this.context = context;
    this.dockerfile = dockerfile;
  }

  public Build(String context, String dockerfile, LinkedHashMap<String,Object> args)
  {
    this.context = context;
    this.dockerfile = dockerfile;
    this.args = args;
  }
}
