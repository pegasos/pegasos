package at.pegasos.generator.parser.docker.token;

import java.util.*;

public class Healthcheck {
  public List<String> test;
  public String interval;
  public String timeout;
  public int retries;
}
