package at.pegasos.generator.parser.docker.token.services;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.docker.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class FrontendBackendService extends Service {
  public FrontendBackendService(ServerInstance instance, String databaseService) throws GeneratorException
  {
    new Service();
    LinkedHashMap<String,Object> args = new LinkedHashMap<>();
    args.put("ARG_NAME", instance.name);
    build = new Build(".", "./frontend/server-frontend-back.yml", args);

    try
    {
      String publicPort = instance.getWeb().config.hasValue("backend.port.public") ?
              instance.getWeb().config.getValue("backend.port.public") : "8080";
      ports = List.of(publicPort + ":" + instance.getWeb().config.getValue("backend.port"));
    }
    catch (ParserException e)
    {
      throw new GeneratorException(e);
    }

    /*volumes = new ArrayList<String>();
    volumes.add("backend-volume:/var/backend");
    volumes.add("backend-data-volume:/var/data");*/
    depends_on = List.of(databaseService);
  }
}
