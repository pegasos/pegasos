package at.pegasos.generator.parser.docker.token;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.docker.token.services.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class DockerComposeFile {

  public final String version = "3";
  private final MariaDbService db;

  public LinkedHashMap<String, Service> services = new LinkedHashMap<>();

  public Map<String, String> volumes;

  private final String databaseService;

  public DockerComposeFile(ServerInstance instance) throws GeneratorException
  {
    databaseService = "pegasos-" + instance.name + "-database";
    final String backendService = "pegasos-" + instance.name + "-frontend-back";

    db = new MariaDbService(instance, databaseService);
    services.put(databaseService, db);

    Service backend = new BackendService(instance, databaseService);
    services.put("pegasos-" + instance.name + "-backend", backend);

    Service frontendBack = new FrontendBackendService(instance, databaseService);
    services.put(backendService, frontendBack);

    Service frontendWeb = new FrontendWebService(instance, backendService);
    services.put("pegasos-" + instance.name + "-frontend-web", frontendWeb);

    volumes = new LinkedHashMap<>();
    volumes.put(db.getDatabaseVolume(), null);
    volumes.put("backend-volume", null);
    volumes.put("backend-data-volume", null);
  }

  public String getDatabaseServiceName()
  {
    return databaseService;
  }
}
