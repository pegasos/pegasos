package at.pegasos.generator.parser.docker.token.services;

import at.pegasos.generator.parser.docker.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class BackendService extends Service {
  public BackendService(ServerInstance instance, String databaseService)
  {
    new Service();
    LinkedHashMap<String,Object> args = new LinkedHashMap<>();
    args.put("ARG_NAME", instance.name);
    args.put("ARG_PORT", instance.port);
    build = new Build(".", "./server-backend.yml", args);

    volumes = new ArrayList<String>();
    volumes.add("backend-volume:/var/backend");
    volumes.add("backend-data-volume:/var/data");
    depends_on = List.of(databaseService);
  }
}
