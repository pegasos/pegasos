package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.util.*;

import static at.pegasos.generator.parser.appparser.token.Sensor.*;

@Slf4j @PegasosAppParser(name="SensorParser", order = 50)
public class SensorParser extends ClientLibraryParser {
  private final Application app;
  private final String fn;
  private final boolean library;

  public SensorParser(Application app, boolean library)
  {
    this.app= app;
    this.library= library;
    if( library )
    {
      fn= "library.xml";
    }
    else
    {
      fn= "application.xml";
    }
  }
  
  public void parse() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(app.directory.resolve(fn).toFile());
      doc.getDocumentElement().normalize();
      
      NodeList nList= doc.getElementsByTagName("sensors");
      if( nList.getLength() > 0 )
      {
        for(int i = 0; i < nList.getLength(); i++)
        {
          if( EntryBool.fromAttribute((Element) nList.item(i), "builtin", false).isTrue() )
          {
            if( !library )
              throw new ParserException("Only libraries can add built in sensors");

            NodeList nListS = ((Element) nList.item(i)).getElementsByTagName("sensor");
            for(int temp = 0; temp < nListS.getLength(); temp++)
            {
              Node nNode = nListS.item(temp);

              Sensors sensors = new Sensors(false);
              Sensor sensor = basicSensorInfo(nNode);
              // sensor.setBuiltIn(true);
              NodeList classes = ((Element) nNode).getElementsByTagName("class");
              sensors.addSensor(sensor, getClasses(classes));
              log.info("Add builin sensor {} {}", sensor, getClasses(classes));
              app.getSensors().addBuiltin(sensor.name, sensors);
            }
          }
          else
          {
            NodeList nListS = ((Element) nList.item(i)).getElementsByTagName("sensor");
            for(int temp = 0; temp < nListS.getLength(); temp++)
            {
              Node nNode = nListS.item(temp);

              parseSensor(nNode);
            }
          }
        }
      }
      else if( !library )
        throw new ParserException("Every application needs to define used sensors");
    }
    catch (ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private Sensor basicSensorInfo(Node node) throws ParserException
  {
    Sensor sensor = new Sensor();

    Element e = (Element) node;

    sensor.setBuiltIn(EntryBool.fromAttribute(e, "builtin", false).isTrue() );

    String attribute = e.getAttribute("name");
    if( attribute.equals("") )
    {
      NodeList x = e.getElementsByTagName("name");
      if( x.getLength() > 0 )
      {
        Text t = (Text) x.item(0).getChildNodes().item(0);
        sensor.name = t.getNodeValue();
      }
      else
        throw new ParserException("No name specified for sensor");
    }
    else
      sensor.name = attribute;

    if( e.getElementsByTagName("icon").getLength() != 0 )
    {
      Text t = (Text) e.getElementsByTagName("icon").item(0).getChildNodes().item(0);
      sensor.icon = t.getNodeValue();
    }
    else
      sensor.icon = null;

    return sensor;
  }
  
  private void parseSensor(Node node) throws ParserException
  {
    /*
    <sensor>
            <name>Bike</name>
            <icon></icon>
            <class type="ant">at.univie.mma.sensors.ANT_MoxySensor</class>
        </sensor>
        <sensor builtin="true" name="HeartRate" />
     */
    Sensor sensor = basicSensorInfo(node);
    Element e = (Element) node;

    if( sensor.isBuiltin() )
    {
      NodeList classes = e.getElementsByTagName("class");
      if( classes.getLength() > 0 )
      {
        log.info("{} adds implementations sensors to builtin {}", app.name, sensor.name);
        app.getSensors().addSensor(sensor, getClasses(classes));
      }
      else
        app.getSensors().addSensor(sensor);
    }
    else
    {
      if( sensor.icon == null )
        throw new ParserException("Need to specify a logo for sensor " + sensor.name);

      NodeList classes = e.getElementsByTagName("class");
      app.getSensors().addSensor(sensor, getClasses(classes));
    }
  }

  private static List<Clasz> getClasses(NodeList classes) throws ParserException
  {
    List<Sensor.Clasz> ret = new ArrayList<Sensor.Clasz>();

    for(int i = 0; i < classes.getLength(); i++)
    {
      if( classes.item(i).getNodeType() != Node.ELEMENT_NODE )
      {
        // log.debug("Got a " + fragments.item(i).getNodeType() + " skip...");
        continue;
      }

      Element clasz = (Element) classes.item(i);
      Sensor.Clasz clazz = new Sensor.Clasz();
      clazz.clasz = clasz.getChildNodes().item(0).getNodeValue();
      String type = clasz.getAttribute("type");
      switch( type )
      {
        case "ant":
          clazz.type = Sensor.Clasz.Type.Ant;
          break;
        case "ble":
          clazz.type = Sensor.Clasz.Type.Ble;
          break;
        case "other":
          clazz.type = Sensor.Clasz.Type.Other;
          break;
        default:
          throw new ParserException("Classtype needs to be either ant, ble or other");
      }

      ret.add(clazz);
    }

    return ret;
  }
}
