package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;

@PegasosAppParser(name="StartupParser", order = 110)
public class StartupParser extends ClientLibraryParser {
  private final Path file;
  private final Application inst;

  public StartupParser(Application inst, boolean library)
  {
    this.inst = inst;
    if( library )
      this.file = inst.directory.resolve("library.xml");
    else
      this.file = inst.directory.resolve("application.xml");
  }

  public void parse() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(file.toFile());
      doc.getDocumentElement().normalize();

      NodeList nList = doc.getElementsByTagName("startup");

      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        Node nNode = nList.item(temp);
        parseStartupEntry(nNode);
      }
    }
    catch( ParserConfigurationException | SAXException | IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private void parseStartupEntry(Node node)
  {
    parseActions(((Element) node).getElementsByTagName("action"));
  }

  private void parseActions(NodeList nList)
  {
    for(int temp = 0; temp < nList.getLength(); temp++)
    {
      Node nNode = nList.item(temp);
      Text val = (Text) nNode.getChildNodes().item(0);
      inst.addStartupAction(val.getNodeValue());
    }
  }
}
