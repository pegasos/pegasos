package at.pegasos.generator.parser.appparser;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.appparser.token.screen.Fragment;
import at.pegasos.generator.parser.appparser.token.screen.ValueFragment;
import at.pegasos.generator.parser.XMLHelper;

public class ScreenParser {
  private final static Logger log= LoggerFactory.getLogger(ScreenParser.class);

  private Screen screen;

  private final Node node;

  private final Application app;

  public ScreenParser(Application app, Node screen)
  {
    this.app= app;
    this.node= screen;
  }

  public void parse() throws ParserException
  {
    NodeList fragments= node.getChildNodes();
    
    String file= XMLHelper.getAttribute(node, "file", null);
    String name= XMLHelper.getAttribute(node, "name", null);
    
    if( file != null )
    {
      readFile(file);
      screen.fileName= file;
      if( name != null )
        screen.name= name;
    }
    else if( name != null && fragments.getLength() == 0)
    {
      screen= app.getScreen(name).referencedCopy();
      return;
    }
    else
    {
      screen= new Screen();
      screen.name= name;

      screen.clasz= XMLHelper.getAttribute(node, "type", null);

      log.debug("Number of child nodes: {}", fragments.getLength());
      log.debug("Type: {}", screen.clasz);

      for(int i= 0; i < fragments.getLength(); i++)
      {
        if( fragments.item(i).getNodeType() != Node.ELEMENT_NODE )
        {
          // log.debug("Got a " + fragments.item(i).getNodeType() + " skip...");
          continue;
        }

        Element fragment= (Element) fragments.item(i);
        Fragment f= null;

        if( fragment.getNodeName().equals("Generate") )
        {
          /*
          <Generate>
                 <Value name="SMO2" type="Double"/>
                 <Left txt="Sm02" />
                 <Right txt="Sm02" />
             </Generate>
          */
          log.debug("Parsing a generate-fragment");

          Element val= (Element) ((Element )fragment).getElementsByTagName("Value").item(0);
          Element left= (Element) ((Element )fragment).getElementsByTagName("Left").item(0);
          Element right= (Element) ((Element )fragment).getElementsByTagName("Right").item(0);

          f= new ValueFragment();
          ValueFragment nf= ((ValueFragment) f);
          f.type= Fragment.TYPE_GENERATE;
          nf.value= val.getAttribute("name");
          nf.value_type= val.getAttribute("type");
          if( !nf.value_type.toLowerCase().equals("double") && !nf.value_type.toLowerCase().equals("integer")
              && !nf.value_type.toLowerCase().equals("string") )
            throw new ParserException("Unknown value type: " + nf.value_type);

          if( !left.getAttribute("img").equals("") )
            nf.left_img= left.getAttribute("img");
          else
            nf.left_txt= left.getAttribute("txt");

          if( !right.getAttribute("img").equals("") )
            nf.right_img= right.getAttribute("img");
          else
            nf.right_txt= right.getAttribute("txt");

          parseFragmentArguments(fragment, f);
        }
        else if( fragment.getNodeName().equals("Java") )
        {
          /*
           <Java class="at.univie.mma.ui.HeartrateFragment" />
           */
          log.debug("Parsing a java-fragment");

          f= new Fragment();
          f.type= Fragment.TYPE_JAVA;
          f.clasz= ((Element ) fragment).getAttribute("class");
          f.method= ((Element ) fragment).getAttribute("method");

          parseFragmentArguments(fragment, f);
        }
        else if( fragment.getNodeName().equals("stretch") )
        {
          log.debug("Parsing stretch element");
        }
        else
        {
          // throw new ParserException("Unknown Fragment-Type: " + fragment.getNodeName());
          log.error("Unknown Fragment-Type: " + fragment.getNodeName());
        }

        screen.fragments.add(f);
      }
    }
  }

  private void readFile(String fileName) throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc= dBuilder.parse(app.directory.resolve(fileName).toFile());

      doc.getDocumentElement().normalize();

      NodeList nList= doc.getElementsByTagName("Screen");
      ScreenParser sp= new ScreenParser(app, nList.item(0));
      sp.parse();

      screen= sp.getScreen();
    }
    catch( SAXException | IOException | ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new ParserException("Could not open " + app.directory.resolve(fileName).toAbsolutePath() + " " + e.getMessage());
    }
  }

  private void parseFragmentArguments(Element elem, Fragment fragment)
  {
    NodeList list= elem.getElementsByTagName("parameter");

    for(int i= 0; i < list.getLength(); i++)
    {
      if( list.item(i).getNodeType() != Node.ELEMENT_NODE )
      {
        // log.debug("Got a " + fragments.item(i).getNodeType() + " skip...");
        continue;
      }

      Element e= (Element) list.item(i);

      String key= e.getAttribute("name");

      String val= XMLHelper.getStringValueOfChild(e);

      fragment.args.put(key, val);
    }
  }

  public Screen getScreen()
  {
    return screen;
  }
}
