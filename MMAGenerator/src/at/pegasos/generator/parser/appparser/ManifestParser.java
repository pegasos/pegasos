package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;

@Slf4j
@PegasosAppParser(name="ManifestParser", order = 100)
public class ManifestParser extends ClientLibraryParser {
  private final Application app;

  public ManifestParser(Application app, boolean library)
  {
    this.app = app;
  }

  public void parse() throws ParserException
  {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = null;
    Document doc = null;

    try
    {
      db = dbf.newDocumentBuilder();
      Path path = app.directory.resolve("AndroidManifest.xml");
      if( Files.exists(path) )
      {
        log.debug("Manifest Additions");
        doc = db.parse(path.toFile());

        NodeList nList = doc.getElementsByTagName("manifest");
        if( nList.getLength() > 0 )
        {
          for(int temp = 0; temp < nList.getLength(); temp++)
          {
            Node nNode = nList.item(temp);

            NodeList children = nNode.getChildNodes();
            for(int i = 0; i < children.getLength(); i++)
            {
              Node child = children.item(i);
              if( child.getNodeName().equals("application") )
              {
                NodeList toAdd = child.getChildNodes();
                for(int j = 0; j < toAdd.getLength(); j++)
                {
                  // Node x= toAdd.item(j);
                  // System.out.println("Node: " + x.getNodeName() + " " + x.getNodeType() + " '" + x.getNodeValue() + "'");
                  if( j % 2 == 1 ) // filter out all those text nodes
                  {
                    app.manifest.application.add(transform(toAdd.item(j)));
                  }
                }
              }
              else
              {
                app.manifest.root.add(child);
              }
            }
          }
        }
      }
      else
      {
        log.debug("No Manifest Additions");
      }
    }
    catch( ParserConfigurationException | SAXException | IOException e )
    {
      throw new ParserException(e.getMessage());
    }
  }

  private Node transform(Node node)
  {
    if( !node.hasChildNodes() )
      return node;
    else
    {
      if( node.getFirstChild().getNodeType() == Node.TEXT_NODE )
        node.removeChild(node.getFirstChild());
      if( node.getLastChild() != null && node.getLastChild().getNodeType() == Node.TEXT_NODE )
        node.removeChild(node.getLastChild());

      for(int i = 0; i < node.getChildNodes().getLength(); i++)
      {
        Node x = node.getChildNodes().item(i);
        // System.out.println("transform: " + x.getNodeName() + " " + x.getNodeType() + " '" + x.getNodeValue() + "'");
        transform(x);
      }

      return node;
    }
  }
}