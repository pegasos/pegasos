package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

@Slf4j
public class LibraryHelper {
  public static void parseInputDir(Path inputDir, Map<String, Application> libraries) throws ParserException
  {
    /*
     * This is a bit of a nasty workaround. If the directory does not exist we only warn but do
     * not fail
     */
    if( !Files.exists(inputDir) )
    {
      log.error("Parsing Libraries: " + inputDir + " does not exist --> ignored");
      return;
    }
    try( DirectoryStream<Path> directoryStream= Files.newDirectoryStream(inputDir) )
    {
      for(Path path : directoryStream)
      {
        if( path.toFile().isDirectory() && path.getFileName().toString().equals("libraries") )
        {
          try( DirectoryStream<Path> dirLibStream= Files.newDirectoryStream(path) )
          {
            for(Path libDir : dirLibStream)
            {
              // Check whether it is a directory. Skip files
              if( libDir.toFile().isDirectory() )
              {
                // check whether it contains all required files
                log.info("Examining {}", libDir.toAbsolutePath());
                if( Files.exists(Paths.get(libDir.toAbsolutePath().toString(), "library.xml")) )
                {
                  Instance i = Pegasos.parseBasicinfo(Paths.get(libDir.toAbsolutePath().toString(), "library.xml"));
                  // log.info("Library " + i.getGitRoot().toString() + " " + i.getGitRoot());
                  i.directory= libDir.toAbsolutePath().normalize();

                  ApplicationParser p= new ApplicationParser(i, true);
                  p.parse();

                  libraries.put(i.name, p.getApplication());
                }
              }
            }
          }
        }
      }
    }
    catch( IOException ex )
    {
      ex.printStackTrace();
      log.error("IOException during examining existing apps", ex);
      throw new ParserException(ex.getMessage());
    }
  }
}
