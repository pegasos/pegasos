package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

@Slf4j
@PegasosAppParser(name="SportsParser", order = 80)
public class SportsParser extends ClientLibraryParser {
  private final Application app;

  private final List<String> sport_names;
  private final List<String> sport_files;

  private final String fn;
  private final boolean library;

  /**
   * Use this method for testing purposes
   * 
   * @param args
   * @throws ParserException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SAXException
   */
  public static void main(String[] args) throws ParserException, ParserConfigurationException, SAXException, IOException
  {
    Path p= Paths.get(args[0]);
    Instance i= new Instance();
    i.directory= p.getParent();
    Application inst= new Application(i);

    // MenuParser app= new MenuParser(null);
    // addSport("MyTest", p.getFileName().toString());

    SportsParser test= new SportsParser(inst, false);
    SensorParser t2= new SensorParser(inst, false);

    t2.parse();
    test.parse();

    for(Sport s : inst.getSports())
    {
      System.out.println(s);
    }
  }

  public SportsParser(Application inst, boolean library)
  {
    this.app= inst;
    sport_names= new ArrayList<String>();
    sport_files= new ArrayList<String>();

    this.library= library;
    if( library )
    {
      fn= "library.xml";
    }
    else
    {
      fn= "application.xml";
    }
  }

  /**
   * Parse sports for the current file
   * 
   * @throws ParserException
   *           if sports are marked mandatory and not present
   */
  public void parse() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder= dbFactory.newDocumentBuilder();
      Document doc= dBuilder.parse(app.directory.resolve(fn).toFile());
      doc.getDocumentElement().normalize();

      NodeList nList;
      nList= doc.getElementsByTagName("sports");
      if( nList.getLength() > 0 )
      {
        nList= ((Element) nList.item(0)).getElementsByTagName("sport");
        for(int temp= 0; temp < nList.getLength(); temp++)
        {
          Node nNode= nList.item(temp);

          parseSport(nNode);
        }
      }
      else if( !library )
      {
        throw new ParserException("Every application needs to define used sports");
      }
      else
      {
        // We are done here
        return;
      }

      List<Sport> sports = new ArrayList<Sport>(getNumberSports());
      for(int i= 0; i < getNumberSports(); i++)
      {
        String fn= getSportFile(i);
        log.info("Parsing : " + fn);

        dbFactory= DocumentBuilderFactory.newInstance();
        dBuilder= dbFactory.newDocumentBuilder();
        doc= dBuilder.parse(app.directory.resolve(fn).toFile());
        doc.getDocumentElement().normalize();

        if( !doc.getDocumentElement().getNodeName().equals("Sport") )
        {
          throw new ParserException("The file " + getSportFile(i) + " was marked as Sport-File but does not have a root element 'Sport'");
        }

        Sport s= new Sport(getSportName(i), fn);

        s.icon= XMLHelper.parseString(doc.getDocumentElement(), "icon", null);
        parseSensors(doc, s);
        parseScreens(doc, s);

        sports.add(s);
      }

      app.setSports(sports);
    }
    catch( ParserConfigurationException | SAXException | IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private void parseSport(Node node)
  {
    Element e= (Element) node;
    addSport(e.getAttribute("name"), e.getAttribute("file"));
  }

  private void addSport(String name, String file)
  {
    sport_names.add(name);
    sport_files.add(file);
  }

  private int getNumberSports()
  {
    return sport_names.size();
  }

  private String getSportName(int idx)
  {
    return sport_names.get(idx);
  }

  private String getSportFile(int idx)
  {
    return sport_files.get(idx);
  }

  // private String[] getSportNames()
  // {
  // return sport_names.toArray(new String[sport_names.size()]);
  // }

  private void parseSensors(Document doc, Sport s) throws ParserException
  {
    NodeList nList= ((Element) doc.getElementsByTagName("RequiredSensors").item(0)).getElementsByTagName("sensor");
    for(int i= 0; i < nList.getLength(); i++)
    {
      String sensor_name= extractSensorName(nList.item(i));
      s.required_names.add(sensor_name);
      app.getSensors().sportRequires(sensor_name, s.name);
    }

    if( doc.getElementsByTagName("OptionalSensors").getLength() > 0 )
    {
      nList= ((Element) doc.getElementsByTagName("OptionalSensors").item(0)).getElementsByTagName("sensor");
      for(int i= 0; i < nList.getLength(); i++)
      {
        String sensor_name= extractSensorName(nList.item(i));
        s.optional_names.add(sensor_name);
        app.getSensors().sportRequires(sensor_name, s.name);
      }
    }
  }

  private void parseScreens(Document doc, Sport s) throws ParserException
  {
    NodeList nList= doc.getElementsByTagName("Screens");
    if( nList.getLength() > 0 )
    {
      nList= ((Element) nList.item(0)).getElementsByTagName("Screen");
      for(int i= 0; i < nList.getLength(); i++)
      {
        ScreenParser sp= new ScreenParser(app, nList.item(i));
        sp.parse();
        s.screens.add(sp.getScreen());
      }
    }
  }

  private String extractSensorName(Node item)
  {
    return ((Element) item).getAttribute("name");
  }
}
