package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;

@PegasosAppParser(name="ActivitiesParser", order = 90)
public class ActivitiesParser extends ClientLibraryParser {
  private final Path file;
  private final Application inst;

  public ActivitiesParser(Application inst, boolean library)
  {
    this.inst = inst;
    if( library )
      this.file = inst.directory.resolve("library.xml");
    else
      this.file = inst.directory.resolve("application.xml");
  }

  public void parse() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(file.toFile());
      doc.getDocumentElement().normalize();

      // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

      NodeList nList = doc.getElementsByTagName("activities");

      if( nList.getLength() > 1 )
        throw new ParserException("Activities for App defined in more than one location!");

      if( nList.getLength() > 0 )
      {
        nList = ((Element) nList.item(0)).getElementsByTagName("activity");

        int i = 0;
        if( inst.activities != null )
        {
          Activity[] newactivities = new Activity[inst.activities.length + nList.getLength()];
          System.arraycopy(inst.activities, 0, newactivities, 0, inst.activities.length);
          inst.activities = newactivities;
          i = inst.activities.length;
        }
        else
        {
          inst.activities = new Activity[nList.getLength()];
        }

        for(int temp = 0; temp < nList.getLength(); temp++)
        {
          Node nNode = nList.item(temp);

          inst.activities[i++] = parseActivity(nNode);
        }
      }
      else
        inst.activities = new Activity[0];
    }
    catch( ParserConfigurationException | SAXException | IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private Activity parseActivity(Node node)
  {
    Element e = (Element) node;

    Activity a = new Activity();
    a.id = e.getAttribute("id");
    a.clasz = e.getAttribute("class");

    return a;
  }
}
