package at.pegasos.generator.parser.appparser;

import java.io.IOException;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.List;
import java.util.Map;
import java.util.stream.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.*;
import lombok.extern.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import at.pegasos.generator.parser.aiparser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.XMLHelper;
import at.pegasos.generator.parser.ConfigParser;

@Slf4j
public class ApplicationParser {
  
  private final Application app;
  private final String fn;
  private final boolean library;

  private final boolean readDefaultConfig;

  private Document doc;
  private Map<String, Application> libraries;

  public ApplicationParser(Instance inst, boolean library)
  {
    this.app= new Application(inst);
    this.library= library;
    this.readDefaultConfig= true;
    if( library )
    {
      fn= "library.xml";
    }
    else
    {
      fn= "application.xml";
    }
  }
  
  public ApplicationParser(Path path, boolean library, boolean readDefaultconfig) throws ParserException
  {
    this.library= library;
    this.readDefaultConfig= readDefaultconfig;
    if( library )
    {
      fn= "library.xml";
    }
    else
    {
      fn= "application.xml";
    }

    if( !Files.exists(path.resolve(fn)) )
    {
      throw new ParserException("Could not open " + path.resolve(fn).toAbsolutePath());
    }

    this.app= new Application(new Instance());
    this.app.directory= path;
  }

  public void loadLibraries(final Map<String, Application> libraries) throws ParserException
  {
    for(String name : app.getLibraries())
    {
      // System.out.println(libraries.keySet().toString());
      Application lib= libraries.get(name);
      if( lib == null )
      {
        throw new ParserException(this.app.name + " requires unknown library '" + name + "'");
      }
      else
      {
        this.app.add(lib);
      }
    }
  }

  public void setLibraries(final Map<String, Application> libraries) throws ParserException
  {
    this.libraries = libraries;
  }

  public void parse() throws ParserException
  {
    try
    {
      openDoc();

      // When not loaded from the Generator this field has not been filled yet
      if( app.name == null )
        app.name= XMLHelper.getStringValueOfChild(doc.getDocumentElement(), "name");

      log.debug("Parsing {} {}", (library ? "Library" : "Application"), app.name);

      XMLHelper.fillInGitInfo(doc, app, Parameters.get());
      XMLHelper.fillInServersInfo(doc, app);

      parseLibaries();
      parseAndroidDependencies();
      parseConfig();
      parseScreens();
      parseWeb();
      parseVersion();

      List<? extends ClientLibraryParser> parsers = fetchParsers();
      for(ClientLibraryParser parser: parsers)
      {
        parser.parse();
      }

      FeedbackModulesParser fp= new FeedbackModulesParser(app);
      fp.parse();
      PostprocessingModulesParser pp= new PostprocessingModulesParser(app);
      pp.parse();
    }
    catch(ParserException e)
    {
      log.error("Error parsing {} {}", (library ? "Library" : "Application"), app.name);
      e.printStackTrace();
      throw e;
    }
  }

  private void openDoc() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      doc= dBuilder.parse(app.directory.resolve(fn).toFile());

      doc.getDocumentElement().normalize();
    }
    catch( SAXException | IOException | ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new ParserException("Could not open " + app.directory.resolve(fn).toAbsolutePath() + " " + e.getMessage());
    }
  }

  private void parseLibaries() throws ParserException
  {
    {
      NodeList nList = doc.getElementsByTagName("libraries");
      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        Node nNode = nList.item(temp);

        NodeList pList = ((Element) nNode).getElementsByTagName("library");
        log.debug("Libraries in this node: {} {}", pList.getLength(), app.directory.resolve(fn));
        for(int p= 0; p < pList.getLength(); p++)
        {
          String name= pList.item(p).getChildNodes().item(0).getNodeValue();

          log.debug("Adding: {}", name);
          this.app.addLibrary(name);
          Application lib = this.libraries.get(name);
          if( lib == null )
          {
            throw new ParserException(this.app.name + " requires unknown library '" + name + "'");
          }
          else
          {
            this.app.add(lib);
          }
          log.debug("Added library {}. Sensors {}", name, app.getSensors());
        }
      }
    }
  }

  public void parseAndroidDependencies() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(app.directory.resolve(fn).toFile());
      doc.getDocumentElement().normalize();
      
      NodeList nList = doc.getElementsByTagName("dependencies");
      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        Node nNode = nList.item(temp);
        
        NodeList pList = ((Element) nNode).getElementsByTagName("project");
        // System.out.println("Libraries in this node: " + pList.getLength());
        for(int p= 0; p < pList.getLength(); p++)
        {
          app.android_projects.add(pList.item(p).getChildNodes().item(0).getNodeValue());
        }
        
        pList = ((Element) nNode).getElementsByTagName("dependency");
        // System.out.println("Dependencies in this node: " + pList.getLength());
        for(int p= 0; p < pList.getLength(); p++)
        {
          app.android_dependencies.add(pList.item(p).getChildNodes().item(0).getNodeValue());
        }
        
        pList = ((Element) nNode).getElementsByTagName("repository");
        // System.out.println("Repositories in this node: " + pList.getLength());
        for(int p= 0; p < pList.getLength(); p++)
        {
          app.android_repositories.add(pList.item(p).getChildNodes().item(0).getNodeValue());
        }
      }
    }
    catch ( ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  public List<? extends ClientLibraryParser> fetchParsers()
  {
    List<Class<? extends PegasosParser>> parsersClasses = DynamicModulesLoader.fetchParsers(ClientLibraryParser.class,
        PegasosAppParser.class, getClass().getPackage().getName());
    @SuppressWarnings("unchecked") // this is safe as we type check before adding to the list
    List<? extends ClientLibraryParser> parsers = (List<? extends ClientLibraryParser>) parsersClasses
        .stream()
        // Sort the parsers according to their order
        .sorted((a, b) -> {
          PegasosAppParser an = a.getAnnotationsByType(PegasosAppParser.class)[0];
          PegasosAppParser bn = b.getAnnotationsByType(PegasosAppParser.class)[0];
          return Integer.compare(an.order(), bn.order());
        })
        .map(c -> {
          try
          {
            return c.getConstructor(Application.class, boolean.class).newInstance(app, library);
          }
          catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e )
          {
            e.printStackTrace();
          }
          return null;
        }).collect(Collectors.toList());
    return parsers;
  }

  private void parseConfig() throws ParserException
  {
    ConfigParser configParser= new ConfigParser(readDefaultConfig ? Paths.get("config/app.defaults") : null);
    configParser.setInputFile(app.directory.resolve(fn));
    configParser.parse();
    
    app.config= configParser.getConfig();
  }

  private void parseScreens() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc= dBuilder.parse(app.directory.resolve(fn).toFile());

      doc.getDocumentElement().normalize();

      NodeList nList= doc.getElementsByTagName("Screens");
      if( nList.getLength() > 0 )
      {
        nList= ((Element) nList.item(0)).getElementsByTagName("Screen");
        for(int i= 0; i < nList.getLength(); i++)
        {
          ScreenParser sp= new ScreenParser(app, nList.item(i));
          sp.parse();

          Screen s= sp.getScreen();

          if( s.name != null )
            app.addScreen(s.name, s);
          else
            log.error("Not adding screen to global definitions since name is missing");
        }
      }
    }
    catch( SAXException | IOException | ParserConfigurationException e )
    {
      e.printStackTrace();
      throw new ParserException("Could not open " + app.directory.resolve(fn).toAbsolutePath() + " " + e.getMessage());
    }
  }

  private void parseWeb() throws ParserException
  {
    WebParser parser= new WebParser(app, false);
    parser.parse();
  }

  private void parseVersion() throws ParserException
  {
    List<String> lines;
    try
    {
      if( Files.exists(app.directory.resolve("version")) )
      {
        lines= Files.readAllLines(app.directory.resolve("version"));

        app.version= new AppVersion();
        app.version.code= lines.get(0);
        app.version.name= lines.get(1);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e);
    }
  }

  public Application getApplication()
  {
    return app;
  }
}
