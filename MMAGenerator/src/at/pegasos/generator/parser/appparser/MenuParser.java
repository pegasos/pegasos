package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.appparser.token.menu.*;
import at.pegasos.generator.parser.token.*;
import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.util.*;

@PegasosAppParser(name="MenuParser", order = 40)
public class MenuParser extends ClientLibraryParser {
  private final static Logger log= LoggerFactory.getLogger(MenuParser.class);
  
  private final Application app;
  
  private final String fn;
  
  public MenuParser(Application app, boolean library)
  {
    this.app= app;
    if( library )
    {
      fn= "library.xml";
    }
    else
    {
      fn= "application.xml";
    }
  }
  
  public void parse() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder= dbFactory.newDocumentBuilder();
      Document doc= dBuilder.parse(app.directory.resolve(fn).toFile());
      doc.getDocumentElement().normalize();
      
      // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
      
      NodeList nList= doc.getElementsByTagName("menu");
      // System.out.println("----------------------------");
      for(int temp= 0; temp < nList.getLength(); temp++)
      {
        Node nNode= nList.item(temp);
        // System.out.println("\nCurrent Element: " + nNode.getNodeName() + " " +
        // nNode.getNodeType());
        
        parseMenuEntry(nNode);
      }
    }
    catch( ParserConfigurationException | SAXException | IOException e )
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }
  
  private void parseMenuEntry(Node nNode) throws ParserException
  {
    Element e= (Element) nNode;
    
    log.debug("Main: " + e.getAttribute("main") + " name: " + e.getAttribute("name"));
    
    if( e.getAttribute("main").equals("") || e.getAttribute("main").equals("false") )
    {
      log.debug("Parsing " + e.getAttribute("name"));
      Menu m= parseMenuItemEntries(e.getElementsByTagName("entry"), e.getAttribute("name"));
      app.menus.put(m.name, m);
      log.debug("Parsing " + e.getAttribute("name") + " done");
    }
    else
    {
      log.debug("Parsing main");
      app.main= parseMenuItemEntries(e.getElementsByTagName("entry"), e.getAttribute("name"));
      log.debug("Parsing main done");
    }
  }
  
  private Menu parseMenuItemEntries(NodeList nodeList, String name) throws ParserException
  {
    Menu ret= new Menu(name);
    
    for(int i= 0; i < nodeList.getLength(); i++)
    {
      Node node= nodeList.item(i);
      Element e= (Element) node;
      log.debug(node.getNodeName() + " " + node.getNodeType() + " " + e.getAttribute("type"));
      
      String type= e.getAttribute("type");
      
      MenuItem m;
      
      if( type.equals("Menu") )
        m= new MenuItemMenu();
      else if( type.equals("Activity") )
        m= new MenuItemActivity();
      else if( type.equals("Intent") )
        m= new MenuItemIntent();
      else
        m= null;

      m.icon= XMLHelper.parseString(e, "icon", null);
      m.text= XMLHelper.parseString(e, "text", new EntryString(EntryString.TYPE_STRING, ""));

      if( type.equals("Activity") )
      {
        /*
         * <entry type="TrainingDistance"> <icon id="true">R.drawable.perpot_logo</icon> <text
         * id="true">R.string.activity_perpot</text>
         * <activity>Activities.RUNNING_DISTANCE_C</activity> <sport>0</sport> <fixed>true</fixed>
         * <distance>10000</distance> </entry> <entry type="Activity"> <icon
         * id="true">R.drawable.perpot_logo</icon> <text id="true">R.string.activity_perpot</text>
         * <activity>120</activity> </entry>
         */
        
        log.debug("Parsing activity");
        
        ((MenuItemActivity) m).activity= XMLHelper.parseInt(e, "activity", new EntryInt("0"));
        
        if( e.getElementsByTagName("sport").getLength() != 0 )
        {
          ((MenuItemActivity) m).sport= e.getElementsByTagName("sport").item(0).getChildNodes().item(0).getNodeValue();
        }
        
        if( e.getElementsByTagName("autostart").getLength() != 0 )
        {
          ((MenuItemActivity) m).auto_start= e.getElementsByTagName("autostart").item(0).getChildNodes().item(0).getNodeValue()
              .equals("true");
        }
        
        log.debug("Distance: " + e.getElementsByTagName("distance").getLength());
        if( e.getElementsByTagName("distance").getLength() != 0 )
        {
          EntryInt fixed_item= XMLHelper.parseInt(e, "fixed", new EntryInt("false"));
          // check if there is an attribute fixed for distance
          String fixed_attr= ((Element) e.getElementsByTagName("distance").item(0)).getAttribute("fixed");
          
          if( fixed_item.toString().toLowerCase().equals("true") || fixed_attr.toLowerCase().equals("true") )
          {
            ((MenuItemActivity) m).fixed_distance= true;
            ((MenuItemActivity) m).distance= XMLHelper.parseInt(e, "distance", new EntryInt("0"));
          }
          else
          {
            ((MenuItemActivity) m).fixed_distance= false;
            ((MenuItemActivity) m).distance= new EntryInt("");
          }
        }
        
        if( e.getElementsByTagName("callback").getLength() != 0 )
        {
          ((MenuItemActivity) m).callback= e.getElementsByTagName("callback").item(0).getChildNodes().item(0).getNodeValue();
          Element ee= (Element) e.getElementsByTagName("callback").item(0);
          if( !ee.getAttribute("class").equals("") && ee.getAttribute("class").toLowerCase().equals("true") )
          {
            ((MenuItemActivity) m).callback_class= true;
          }
        }
        
        if( e.getElementsByTagName("param2").getLength() != 0 )
        {
          log.warn("usage of deprecated argument 'param2' in " + app.directory.resolve("application.xml").toAbsolutePath().toString());
          ((MenuItemActivity) m).parameters= e.getElementsByTagName("param2").item(0).getChildNodes().item(0).getNodeValue();
        }
        else if( e.getElementsByTagName("parameters").getLength() != 0 )
        {
          ((MenuItemActivity) m).parameters= e.getElementsByTagName("parameters").item(0).getChildNodes().item(0).getNodeValue();
        }
        
        if( e.getElementsByTagName("queries").getLength() > 0 )
        {
          NodeList nList= e.getElementsByTagName("queries").item(0).getChildNodes();
          for(int temp= 0; temp < nList.getLength(); temp++)
          {
            Node n= nList.item(temp);
            
            // Ignore nodes which aren't a query
            if( !n.getNodeName().equals("query") )
              continue;
            
            ((MenuItemActivity) m).qvalues.add(parseQuery(n));
          }
        }
        else
        {
          if( e.getElementsByTagName("query").getLength() > 0 )
          {
            if( e.getElementsByTagName("query").getLength() > 1 )
            {
              log.warn("More than one query tag!");
            }
            
            Node query= e.getElementsByTagName("query").item(0);
            
            ((MenuItemActivity) m).qvalues.add(parseQuery(query));
          }
        }
        
        // TODO: remove this legacy workaround once new activity starter has been implemented
        if( ((MenuItemActivity) m).qvalues.size() == 0 && ((MenuItemActivity) m).parameters != null )
        {
          if( !((MenuItemActivity) m).parameters.startsWith("\"") )
            ((MenuItemActivity) m).parameters= "\"" + ((MenuItemActivity) m).parameters + "\"";
        }
        
        /*
         * if( e.getElementsByTagName("query").getLength() > 0 ) { if(
         * e.getElementsByTagName("query").getLength() > 1 ) { log.warn("More than one query tag!");
         * }
         * 
         * Element n= (Element) e.getElementsByTagName("query").item(0); n= (Element)
         * n.getElementsByTagName("value").item(0);
         * 
         * HashMap<String, String> item= new HashMap<String,String>(); item.put("Label",
         * n.getAttribute("label")); item.put("List", n.getChildNodes().item(0).getNodeValue());
         * 
         * ((MenuItemActivity) m).qvalues.add(item); }
         */
        
        // Now check validity of Data
        // MenuItemActivity a= (MenuItemActivity) m;
        // TODO: if( a.callback ) <check validity> when sport not set --> there should be no
        // callback
      }
      else if( type.equals("Menu") )
      {
        /*
         * <icon id="true">R.drawable.heart</icon> <text
         * id="true">R.string.activity_perpot_calibration</text> <menu>PerPot Calibration</menu>
         */
        Node x= e.getElementsByTagName("name").item(0);
        if( x == null )
          throw new ParserException("Menu entry needs a name");
        ((MenuItemMenu) m).menu= ((Text) x.getChildNodes().item(0)).getNodeValue();
      }
      else if( type.equals("Intent") )
      {
        ((MenuItemIntent) m).intent= ((Text) e.getElementsByTagName("intent").item(0).getChildNodes().item(0)).getNodeValue();
        NodeList l= e.getElementsByTagName("param");
        for(int j= 0; j < l.getLength(); j++)
        {
          Node p= l.item(j);
          ((MenuItemIntent) m).params.put(XMLHelper.getAttribute(p, "name", null), XMLHelper.getStringValueOfChild((Element) p));
        }
      }
      
      log.debug(m.toString());
      
      ret.items.add(m);
    }
    
    return ret;
  }
  
  private HashMap<String, EntryString> parseQuery(Node query)
  {
    Element el= (Element) query;
    
    String qtype= el.getAttribute("type");
    
    HashMap<String, EntryString> item= new HashMap<String, EntryString>();
    item.put("_Type", new EntryString(EntryString.TYPE_STRING, qtype));
    
    /*
     * String name= XMLHelper.getStringValueOfChild(el, "name"); if( name != null ) {
     * item.put("Name", name); }
     */
    item.put("Name", XMLHelper.parseString(el, "name", null));
    
    if( qtype.toLowerCase().equals("list") )
    {
      EntryString h= XMLHelper.parseString(el, "label", null);
      if( h != null )
        item.put("Label", h);
      h= XMLHelper.parseString(el, "title", null);
      if( h != null )
        item.put("Title", h);
      item.put("List", XMLHelper.parseString(el, "list", null));
      item.put("_Callback", new EntryString(EntryString.TYPE_ID, "at.pegasos.mma.activitystarter.SelectList"));
    }
    else if( qtype.toLowerCase().equals("filelist") )
    {
      EntryString h= XMLHelper.parseString(el, "label", null);
      if( h != null )
        item.put("Label", h);
      h= XMLHelper.parseString(el, "title", null);
      if( h != null )
        item.put("Title", h);
      item.put("Directory", XMLHelper.parseString(el, "directory", null));
      item.put("Pattern", XMLHelper.parseString(el, "pattern", null));
      item.put("_Callback", new EntryString(EntryString.TYPE_ID, "at.pegasos.mma.activitystarter.FileSelect"));
    }
    else
    {
      item.put("_Callback", new EntryString(EntryString.TYPE_ID, qtype));
      NodeList children= el.getChildNodes();
      for(int i= 0; i < children.getLength(); i++)
      {
        Node child= children.item(i);
        String name= child.getNodeName();
        item.put(name, XMLHelper.parseString(el, name, null));
      }
    }
    
    return item;
  }
}
