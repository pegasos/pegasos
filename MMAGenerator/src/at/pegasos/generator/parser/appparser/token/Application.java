package at.pegasos.generator.parser.appparser.token;

import java.util.*;
import java.util.Map.Entry;

import at.pegasos.generator.parser.token.*;
import org.slf4j.*;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.Web;

public class Application extends Instance {
  @Override
  public String toString()
  {
    return "Application " + name + " [libraries=" + libraries + ",web=" + web + "]";
  }

  private final static Logger logger= LoggerFactory.getLogger(Application.class);

  public HashMap<String, Menu> menus;

  public Menu main= Menu.EmptyMenu;

  public Activity[] activities;

  private final Sensors sensors;

  private final Map<String, Screen> screens;

  private List<Sport> sports;

  public Config config;

  public ManifestAddition manifest;

  public List<OwnedString> startup_actions;

  public List<String> android_projects;

  public List<String> android_dependencies;

  public List<String> android_repositories;

  private final Set<String> libraries;

  public AppVersion version;

  public Application(Instance inst)
  {
    super(inst);

    this.menus = new HashMap<String, Menu>();
    this.config = new Config();
    this.sensors = new Sensors();
    this.sports = new ArrayList<Sport>();
    this.screens = new HashMap<String, Screen>();
    this.manifest = new ManifestAddition();
    this.android_projects = new ArrayList<String>();
    this.android_dependencies = new ArrayList<String>();
    this.android_repositories = new ArrayList<String>();
    this.startup_actions = new ArrayList<OwnedString>();
    this.libraries = new HashSet<String>();
    this.web = new Web();
  }
  
  /**
   * Add the config from another application (usually a library) to this app
   * 
   * @param other Library
   */
  public void add(Application other) throws ParserException
  {
    logger.debug("Adding " + other.name + " to " + this.name);

    // Menus
    this.menus.putAll(other.menus);
    // Only overwrite main menu if its explicitly set
    if( other.main != Menu.EmptyMenu )
    {
      this.main= other.main;
    }

    // Controllers
    if( other.activities != null && other.activities.length > 0 )
    {
      Activity[] newactivities= new Activity[activities.length + other.activities.length];
      System.arraycopy(activities, 0, newactivities, 0, activities.length);
      System.arraycopy(other.activities, 0, newactivities, activities.length, other.activities.length);
      activities= newactivities;
    }

    this.sensors.add(other.sensors);

    for(Entry<String, Screen> e : other.screens.entrySet() )
    {
      if( this.screens.containsKey(e.getKey()) )
        throw new ParserException("Screen: " + e.getKey() + " already defined!");
      this.screens.put(e.getKey(), e.getValue());
    }

    if( other.sports != null )
    {
      this.sports.addAll(other.sports);
    }

    // TODO: config

    this.manifest.add(other.manifest);

    this.startup_actions.addAll(other.startup_actions);
    this.android_projects.addAll(other.android_projects);
    this.android_dependencies.addAll(other.android_dependencies);
    this.android_repositories.addAll(other.android_repositories);

    // TODO: recursive libraries?
  }

  public void addLibrary(String name)
  {
    this.libraries.add(name);
  }

  public Set<String> getLibraries()
  {
    return this.libraries;
  }

  public Sensors getSensors()
  {
    return sensors;
  }

  public List<Sport> getSports()
  {
    return this.sports;
  }

  public void setSports(List<Sport> sports)
  {
    this.sports = sports;
  }

  public Menu getMain()
  {
    return main;
  }

  public int getMenuIdx(String menu_name)
  {
    int idx= 0;
    for(String x : menus.keySet())
    {
      if( x.equals(menu_name) )
        return idx;

      idx++;
    }

    return -1;
  }

  public HashMap<String, Menu> getMenus()
  {
    return menus;
  }

  public Config getConfig()
  {
    return config;
  }

  /**
   * Add an action which should be executed at startup
   * 
   * @param action Class (cannonical name)
   */
  public void addStartupAction(String action)
  {
    startup_actions.add(new OwnedString(this, action));
  }

  public void addScreen(String name, Screen screen) throws ParserException
  {
    if( this.screens.containsKey(name) )
      throw new ParserException("Screen: " + name + " already defined!");
    this.screens.put(name, screen);
  }

  public Screen getScreen(String name) throws ParserException
  {
    if( !this.screens.containsKey(name) )
      throw new ParserException("No screen '" + name + "' defined");
    return this.screens.get(name);
  }

  public void addActivity(Activity e)
  {
    Activity[] tmp= new Activity[activities.length + 1];
    System.arraycopy(activities, 0, tmp, 0, activities.length);
    activities= tmp;
    activities[activities.length-1]= e;
  }

  public void removeActivity(Activity e)
  {
    int i= 0;
    for(; i < activities.length; i++)
    {
      if( activities[i] == e )
        break;
    }
    if( i != activities.length )
    {
      Activity[] tmp= new Activity[activities.length - 1];
      System.arraycopy(activities, 0, tmp, 0, i);
      System.arraycopy(activities, i+1, tmp, i, activities.length - i - 1);
      activities= tmp;
    }
  }

  public Collection<Screen> getScreens()
  {
    return screens.values();
  }
}
