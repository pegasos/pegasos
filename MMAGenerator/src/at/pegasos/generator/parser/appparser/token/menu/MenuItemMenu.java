package at.pegasos.generator.parser.appparser.token.menu;

public class MenuItemMenu extends MenuItem {
  public String menu;

  public MenuItemMenu()
  {
  }

  public MenuItemMenu(MenuItem entry)
  {
    this.icon= entry.icon;
    this.text= entry.text;
  }

  @Override
  public MenuItemMenu copy()
  {
    MenuItemMenu ret= new MenuItemMenu();
    ret.icon= icon;
    ret.text= text;
    ret.menu= new String(menu);
    return ret;
  }
}