package at.pegasos.generator.parser.appparser.token;

import java.util.ArrayList;
import java.util.List;

import at.pegasos.generator.parser.appparser.token.menu.MenuItem;

public class Menu {
  public static final Menu EmptyMenu= new Menu("_EMPTY_");

  public String name;
  public List<MenuItem> items;

  public Menu(String name)
  {
    this.name= name;
    items= new ArrayList<MenuItem>();
  }
}