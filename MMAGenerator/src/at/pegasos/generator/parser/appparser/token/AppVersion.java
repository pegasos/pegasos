package at.pegasos.generator.parser.appparser.token;

/**
 * Version of the app. Currently this is only used to set the version codes of the android client
 */
public class AppVersion {
  public String code;
  public String name;
}
