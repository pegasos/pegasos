package at.pegasos.generator.parser.appparser.token.menu;

import java.util.*;

import at.pegasos.generator.parser.token.*;

public class MenuItemActivity extends MenuItem {
  public EntryInt activity= new EntryInt("0");
  public String sport;

  public boolean fixed_distance;
  public EntryInt distance;
  public String callback= "";
  public boolean callback_class= false;
  public boolean auto_start= false;
  public String parameters;
  public List<HashMap<String, EntryString>> qvalues;

  public MenuItemActivity()
  {
    this.qvalues= new ArrayList<HashMap<String,EntryString>>();
  }

  public MenuItemActivity(MenuItem entry)
  {
    this.icon= entry.icon;
    this.text= entry.text;
    this.qvalues= new ArrayList<HashMap<String,EntryString>>();
  }

  public String toString()
  {
    return "Test: " + icon + " " + text + " " + activity + " " + sport + " " + callback + " " + callback_class + " " + auto_start;
  }

  public MenuItemActivity copy()
  {
    MenuItemActivity ret= new MenuItemActivity();
    ret.icon= icon;
    ret.text= text;
    ret.activity= activity;
    ret.sport= sport;
    ret.fixed_distance= fixed_distance;
    ret.distance= distance;
    ret.callback= callback;
    ret.callback_class= callback_class;
    ret.auto_start= auto_start;
    ret.parameters= parameters;
    ret.qvalues= new ArrayList<HashMap<String, EntryString>>();
    ret.qvalues.addAll(qvalues);
    return ret;
  }
}
