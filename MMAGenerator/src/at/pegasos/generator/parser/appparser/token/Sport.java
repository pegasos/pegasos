package at.pegasos.generator.parser.appparser.token;

import java.util.*;

import at.pegasos.generator.parser.token.EntryString;

public class Sport {
  /**
   * Name of the sport
   */
  public String name;
  /**
   * File in which this sport is defined
   */
  public String fileName;
  public String java_name;
  public List<String> required_names;
  public List<String> optional_names;
  public List<Screen> screens;

  /**
   * Icon which represents the sport
   */
  public EntryString icon;

  public Sport(String name, String fileName)
  {
    this.name= name;
    this.fileName= fileName;
    this.java_name= name.replaceAll(" ", "_");
    required_names= new ArrayList<String>();
    optional_names= new ArrayList<String>();
    screens= new ArrayList<Screen>();
  }

  /**
   * Return a copy of this sport. Caution: screens are not deep copies but only references to
   * original screens
   * 
   * @return copy
   */
  public Sport copy()
  {
    Sport ret= new Sport(name, fileName);
    ret.icon= icon != null ? icon.copy() : null;
    ret.required_names.addAll(required_names);
    ret.optional_names.addAll(optional_names);
    ret.screens.addAll(screens);
    return ret;
  }
}