package at.pegasos.generator.parser.appparser.token;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.Map.Entry;

import at.pegasos.generator.parser.*;
import org.slf4j.*;

public class Config {
  private final static Logger log= LoggerFactory.getLogger(Config.class);

  private final Map<String, String> values;
  private final Map<String, Boolean> is_string;
  private final Map<String, Boolean> is_boolean;

  public Config()
  {
    values= new HashMap<String, String>();
    is_string= new HashMap<String, Boolean>();
    is_boolean= new HashMap<String, Boolean>();
  }

  public Config(Path defaults) throws ParserException
  {
    values= new HashMap<String, String>();
    is_string= new HashMap<String, Boolean>();
    is_boolean= new HashMap<String, Boolean>();
    
    init(defaults);
  }
  
  public void add(Config other) throws ParserException
  {
    other.values.keySet().forEach(k -> {if(this.values.containsKey(k)) log.warn("Chaning the value of " + k);});
    this.values.putAll(other.values);
    this.is_string.putAll(other.is_string);
    this.is_boolean.putAll(other.is_boolean);
  }
  
  private void init(Path values) throws ParserException
  {
    if( values == null )
    {
      log.debug("Not reading default values");
      return;
    }

    int line_no= 1;
    
    try
    {
      InputStream in;
      if( Files.exists(values) )
      {
        in= Files.newInputStream(values);
      }
      else
      {
        URL t= getClass().getResource("/" + values);
        if( t == null )
        {
          log.error("Config file missig: {}", values);
        }
        else
        {
          log.info("{} not found using {} instead ", values, new File(getClass().getResource("/" + values.toString()).getFile()));
        }

        in= getClass().getResourceAsStream("/" + values);
      }
      BufferedReader reader= new BufferedReader(new InputStreamReader(in));

      String line = null;
      while((line = reader.readLine()) != null)
      {
        if(line.equals("") )
          continue;

        String[] fields= line.split("[;]");
        if( fields.length != 3 && fields.length != 4 )
        {
          // throw new ParserException(reason)
          throw new ParserException("Error parsing '" + values + "'(" + line_no + "): 3 or 4 fields required. Got : " + fields.length);
        }

        if( fields[1].equals("boolean") )
        {
          String h= fields[2].toLowerCase().trim();
          if( h.equals("true") || h.equals("false") )
            setValueBool(fields[0], h);
          else
            throw new ParserException("Error parsing '" + values + "'(" + line_no + "): boolean value require 'true' or 'false' got: '" +
                fields[2] + "'", values.toAbsolutePath().toString(), String.valueOf(line_no));
        }
        else if( fields[1].equals("string") )
        {
          setValueString(fields[0], fields[2]);
        }
        else if( fields[1].equals("int") )
        {
          setValue(fields[0], Integer.parseInt(fields[2]));
        }

        line_no++;
      }

      reader.close();
      in.close();
    }
    catch (IOException x)
    {
      x.printStackTrace();
      log.error("IOException while parsing '" + values.toAbsolutePath() + "'");
      log.error(x.getMessage());
      throw new ParserException(x.getMessage(), values.toAbsolutePath().toString(), String.valueOf(line_no));
    }
  }

  public void read(Path path) throws IOException, ParserException
  {
    int line_no = 1;
    BufferedReader reader = Files.newBufferedReader(path);

    String line = null;
    while((line = reader.readLine()) != null)
    {
      if(line.equals("") )
        continue;

      String[] fields= line.split("[;]");
      if( fields.length != 3 && fields.length != 4 )
      {
        // throw new ParserException(reason)
        throw new ParserException("Error parsing '" + values + "'(" + line_no + "): 3 or 4 fields required. Got : " + fields.length);
      }

      if( fields[1].equals("boolean") )
      {
        String h= fields[2].toLowerCase().trim();
        if( h.equals("true") || h.equals("false") )
          setValueBool(fields[0], h);
        else
          throw new ParserException("Error parsing '" + values + "'(" + line_no + "): boolean value require 'true' or 'false' got: '" +
                  fields[2] + "'", path.toString(), String.valueOf(line_no));
      }
      else if( fields[1].equals("string") )
      {
        setValueString(fields[0], fields[2]);
      }
      else if( fields[1].equals("int") )
      {
        setValue(fields[0], Integer.parseInt(fields[2]));
      }

      line_no++;
    }

    reader.close();
  }
  
  public void setValue(String name, int l) throws ParserException {
    setValue(name, "" + l);
  }
  
  public void setValue(String name, String value) throws ParserException {
    String old= values.put(name, value);
    Boolean olds= is_string.put(name, false);
    Boolean oldb= is_boolean.put(name, false);
    
    if( old == null && (olds != null || oldb != null) )
      throw new ParserException("There is something wrong: Value had a type before it existed");
    else if( old != null )
    {
      if( olds )
        log.warn("Chaning the type of " + name);
      log.info("Chaning the value of " + name);
    }
  }
  
  /*
  private void setValueBool(String name, boolean b) throws ParserException {
    setValueBool(name, "" + b);
  }
  */
  
  public void setValueBool(String name, String value) throws ParserException
  {
    String old= values.put(name, value);
    Boolean olds= is_string.put(name, false);
    Boolean oldb= is_boolean.put(name, true);
    
    if( old == null && (olds != null || oldb != null) )
      throw new ParserException("There is something wrong: Value had a type before it existed");
    else if( old != null )
    {
      if( (oldb != null && oldb == false) || (olds != null && olds == true ) )
        log.warn("Chaning the type and value of " + name);
      log.info("Chaning the value of " + name);
    }
  }

  public void setValueString(String name, String value) throws ParserException
  {
    String old= values.put(name, value);
    Boolean olds= is_string.put(name, true);
    Boolean oldb= is_boolean.put(name, false);
    
    if( old == null && (olds != null || oldb != null) )
      throw new ParserException("There is something wrong: Value had a type before it existed");
    else if( old != null )
    {
      if( (olds != null && olds == false) || (oldb != null && oldb == true ) )
        log.warn("Chaning the type and value of " + name);
      log.info("Chaning the value of " + name);
    }
  }
  
  /**
   * Get the value of a variable.
   * @param name name of the variable
   * @return value of the variable a string
   * @throws ParserException if the variable does not exist
   */
  public String getValue(String name) throws ParserException
  {
    String val= values.get(name);
    if( val == null )
      throw new ParserException("No variable '"+ name + "' available");
    
    return val;
  }
  
  /**
   * Get the value of a variable. This will also check whether the variable is a string 
   * @param name name of the variable
   * @return value of the variable
   * @throws ParserException if the variable does not exists or is not a string
   */
  public String getValueString(String name) throws ParserException
  {
    Boolean check= is_string.get(name);
    if( check == null || check == false )
      throw new ParserException("No variable '"+ name + "' of type string available");
    
    String val= values.get(name);
    if( val == null )
      throw new ParserException("No variable '"+ name + "' available");
    
    return val;
  }
  
  public boolean getValueBool(String name) throws ParserException
  {
    Boolean check= is_boolean.get(name);
    if( check == null || check == false )
      throw new ParserException("No variable '"+ name + "' of type boolean available");
    
    String val= values.get(name);
    if( val == null )
      throw new ParserException("No variable '"+ name + "' available");
    
    return val.equals("true");
  }
  
  public String[] getValueDefinitionsJava()
  {
    String[] ret= new String[values.size()];
    
    int i= 0;
    for(Entry<String, String> e : values.entrySet())
    {
      boolean string= is_string.get(e.getKey()) != null ? is_string.get(e.getKey())  : false;
      boolean bool= is_boolean.get(e.getKey()) != null ? is_boolean.get(e.getKey())  : false;
      
      log.debug(e.getKey() + " " + e.getValue() + " " + string + " " + bool);
      
      if( string )
        ret[i]= "String ";
      else if( bool )
        ret[i]= "boolean ";
      else
        ret[i]= "int ";
      
      String name= e.getKey().toUpperCase().replace(".", "_").replace("-", "_");
      ret[i]+= name + " = ";
      
      if( string )
        ret[i]+= "\"" + e.getValue() + "\"";
      else if( bool )
        ret[i]+= e.getValue().toLowerCase().equals("true") ? "true" : "false";
      else
        ret[i]+= e.getValue();
      
      // ret[i++]= ";";
      log.debug(ret[i]);
      i++;
    }
    
    return ret;
  }
  
  public String[] getValueDefinitionsTypeScript()
  {
    String[] ret= new String[values.size()];
    
    int i= 0;
    for(Entry<String, String> e : values.entrySet())
    {
      boolean string= is_string.get(e.getKey()) != null ? is_string.get(e.getKey())  : false;
      boolean bool= is_boolean.get(e.getKey()) != null ? is_boolean.get(e.getKey())  : false;
      
      log.debug(e.getKey() + " " + e.getValue() + " " + string + " " + bool);

      String name= e.getKey().toUpperCase().replace(".", "_").replace("-", "_");
      ret[i]= name + " = ";

      if( string )
        ret[i]+= "'" + e.getValue() + "'";
      else if( bool )
        ret[i]+= e.getValue().toLowerCase().equals("true") ? "true" : "false";
      else
        ret[i]+= e.getValue();
      
      log.debug(ret[i]);
      i++;
    }

    return ret;
  }
  
  /**
   * Check whether a value is set for this configuration
   * 
   * @param value
   *          name of the value
   * @return true if value exists in this configuration
   */
  public boolean hasValue(String value)
  {
    return values.containsKey(value);
  }

  /**
   * Get the names of all values present in this configuration
   * 
   * @return
   */
  public Set<String> getValueNames()
  {
    return values.keySet();
  }

  public boolean isBool(String valName)
  {
    return is_boolean.get(valName) != null ? is_boolean.get(valName)  : false;
  }

  public boolean isString(String valName)
  {
    return is_string.get(valName) != null ? is_string.get(valName)  : false;
  }

  public void remove(String name)
  {
    this.values.remove(name);
    this.is_boolean.remove(name);
    this.is_string.remove(name);
  }

  public boolean hasValues()
  {
    return this.values.size() > 0;
  }
}
