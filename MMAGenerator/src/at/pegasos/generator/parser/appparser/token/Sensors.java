package at.pegasos.generator.parser.appparser.token;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.Sensor.*;
import at.pegasos.util.*;
import org.slf4j.*;

import java.nio.file.*;
import java.util.*;

public class Sensors {
  private final static Logger log= LoggerFactory.getLogger(Sensors.class);

  @Override
  public String toString()
  {
    return "Sensors [ant_sensors=" + ant_sensors + ", ble_sensors=" + ble_sensors + ", sensors=" + sensors + ", icons=" + icons
        + ", all_classes=" + all_classes + ", sensorssensors=" + sensorssensors + ", builtin=" + BuiltinSensrs + "]";
  }

  public List<String> ant_sensors;
  public List<String> ble_sensors;
  /**
   * Sensors used
   * 
   * Key: Type of the sensor
   * Value: List of implementations
   */
  public Map<String, List<String>> sensors;
  /**
   * Sensor icons
   * 
   * key: sensor type
   * value: icon
   */
  public Map<String, String> icons;
  private final Set<String> all_classes;

  private final List<Sensor> sensorssensors;

  private final Map<String, Sensors> BuiltinSensrs = new HashMap<>();

  public Sensors()
  {
    ant_sensors= new ArrayList<String>();
    ble_sensors= new ArrayList<String>();
    sensors= new HashMap<String, List<String>>();
    icons= new HashMap<String, String>();
    all_classes= new HashSet<String>();
    sensorssensors= new ArrayList<Sensor>();

    addBuiltin();
  }

  public Sensors(boolean init)
  {
    ant_sensors = new ArrayList<String>();
    ble_sensors = new ArrayList<String>();
    sensors = new HashMap<String, List<String>>();
    icons = new HashMap<String, String>();
    all_classes = new HashSet<String>();
    sensorssensors = new ArrayList<Sensor>();

    if( init )
    {
      addBuiltin();
    }
  }

  private void addBuiltin()
  {
    Sensors hr = new Sensors(false);
    hr.ant_sensors.add("at.pegasos.client.sensors.ANT_HRSensor");
    hr.addClass("HeartRate", "at.pegasos.client.sensors.ANT_HRSensor");
    hr.icons.put("HeartRate", "R.drawable.herz");

    Sensors fp = new Sensors(false);
    fp.ant_sensors.add("at.pegasos.client.sensors.ANT_FootPodSensor");
    fp.addClass("FootPod", "at.pegasos.client.sensors.ANT_FootPodSensor");
    fp.icons.put("FootPod", "R.drawable.foot_pod");

    Sensors gps = new Sensors(false);
    gps.addClass("GPS", "at.pegasos.client.sensors.GPSSensor");
    gps.icons.put("GPS", "R.drawable.icon_gps");

    Sensors acc = new Sensors(false);
    acc.addClass("Accelerometer", "at.pegasos.client.sensors.AccelerationSensor");
    acc.icons.put("Accelerometer", "R.drawable.icon_accelerometer");

    Sensors bikeSpeed = new Sensors(false);
    bikeSpeed.ant_sensors.add("at.pegasos.client.sensors.ANT_BikeSpeedCadence");
    bikeSpeed.addClass("BikeSpeed", "at.pegasos.client.sensors.ANT_BikeSpeedCadence");
    bikeSpeed.icons.put("BikeSpeed", "R.drawable.ant_pwr");

    Sensors moxy = new Sensors(false);
    moxy.ant_sensors.add("at.pegasos.client.sensors.MoxySensor");
    moxy.addClass("Moxy", "at.pegasos.client.sensors.MoxySensor");
    moxy.icons.put("Moxy", "R.drawable.muscleoxygen");

    BuiltinSensrs.put("HeartRate", hr);
    BuiltinSensrs.put("FootPod", fp);
    BuiltinSensrs.put("GPS", gps);
    BuiltinSensrs.put("BikeSpeed", bikeSpeed);
    BuiltinSensrs.put("Moxy", moxy);
  }

  /**
   * Add other sensors this sensors definition
   * 
   * @param other Instance to add
   */
  public void add(Sensors other)
  {
    this.ant_sensors.addAll(other.ant_sensors);
    this.ble_sensors.addAll(other.ble_sensors);
    this.sensors.putAll(other.sensors);
    this.icons.putAll(other.icons);
    this.all_classes.addAll(other.all_classes);

    for(Map.Entry<String, Sensors> sensor : other.BuiltinSensrs.entrySet())
    {
      addBuiltin(sensor.getKey(), sensor.getValue());
    }
  }

  public void addSensor(Sensor sensor)
  {
    addSensor(sensor, null);
  }

  public void addSensor(Sensor sensor, List<Clasz> classes)
  {
    /*
     * <sensor builtin="true" name="HeartRate" />
        <sensor builtin="true" name="FootPod" />
        <sensor builtin="true" name="GPS" />
     */
    if( sensor.isBuiltin() )
    {
      if( BuiltinSensrs.containsKey(sensor.name) )
      {
        add(BuiltinSensrs.get(sensor.name));
      }
      else
      {
        log.warn("Sensor '{}' declared as built in but no implementations in version of framework", sensor.name);
        // Handle 'corner case' --> used from application editor and no implementations are set
        // we do not have implementations but maybe want to use it
        // --> need to add an empty entry so that we can access the name
        sensors.put(sensor.name, new ArrayList<String>());
      }

      if( classes != null )
      {
        for(Sensor.Clasz clasz : classes)
        {
          addClasz(sensor.name, clasz);
        }
      }
    }
    else
    {
      // Handle 'corner case' --> used from application editor and no implementations are set
      if( classes != null )
      {
        if( sensor.classes == null )
          sensor.classes = new ArrayList<>();

        for(Sensor.Clasz clasz : classes)
        {
          addClasz(sensor.name, clasz);
          if( !sensor.classes.contains(clasz) )
            sensor.classes.add(clasz);
        }
      }
      else if( !sensors.containsKey(sensor.name) )
      {
        // we do not have implementations but maybe want to use it
        // --> need to add an empty entry so that we can access the name
        sensors.put(sensor.name, new ArrayList<String>());
      }
    }

    if( sensor.icon != null )
      icons.put(sensor.name, sensor.icon);

    this.sensorssensors.add(sensor);
  }

  private void addClasz(String name, Clasz clasz)
  {
    switch( clasz.type )
    {
      case Ant:
        ant_sensors.add(clasz.clasz);
        break;
      case Ble:
        ble_sensors.add(clasz.clasz);
        break;

      // Nothing to do for the other cases
      case Other:
      default:
        break;
    }

    addClass(name, clasz.clasz);
  }

  private void removeClasz(String name, Clasz clasz)
  {
    switch( clasz.type )
    {
      case Ant:
        ant_sensors.remove(clasz.clasz);
        break;
      case Ble:
        ble_sensors.remove(clasz.clasz);
        break;
        
      case Other:
      default:
        break;
    }

    removeClass(name, clasz.clasz);
  }

  private void removeClass(String name, String clazz)
  {
    List<String> classes= sensors.get(name);

    classes.remove(clazz);

    all_classes.remove(clazz);
  }

  private void addClass(String name, String clazz)
  {
    List<String> classes;
    if( !sensors.containsKey(name) )
    {
      classes= new ArrayList<String>();
      sensors.put(name, classes);
    }
    else
      classes= sensors.get(name);

    // Add to list of sensors implementing an interface
    classes.add(clazz);

    all_classes.add(clazz);
  }

  /**
   * checks whether a sensor with the given name is present
   * @param sensor_name Name of the sensor
   * @param sport name of the sport
   * @throws ParserException when no sensor with given name is available
   */
  public void sportRequires(String sensor_name, String sport) throws ParserException
  {
    if( !sensors.containsKey(sensor_name) )
      throw new ParserException("Sport " + sport + " marked sensor " + sensor_name + " as required/optional but no sensor was declared. ");
  }

  public void setDependencies(DependencyManager app, Path searchpath)
  {
    // Add icons?

    HashMap<String, String> extra= new HashMap<String, String>();
    extra.put("com.dsi.ant.plugins.antplus", "antpluginlib");

    app.debug();

    // Add depencencies
    for(List<String> implementations : sensors.values())
    {
      for(String implementation : implementations)
      {
        log.debug("Adding implementation " + implementation);
        if( DependencyHelper.addImplementationAndInfer(implementation, searchpath, app, extra, "sensors") )
        {
          log.debug("Depencency from sensors to " + implementation + " added");
        }
      }
    }
  }

  public List<Sensor> getSensors()
  {
    return sensorssensors;
  }

  public void removeSensor(Sensor sensor)
  {
    // For builtin sensors classes is null
    if( sensor.classes != null )
    {
      for(Sensor.Clasz clasz : sensor.classes)
        removeClasz(sensor.name, clasz);
    }

    // for builtin sensors we do not specify an icon
    if( sensor.icon != null )
      icons.remove(sensor.name);

    sensorssensors.remove(sensor);
  }

  public Collection<String> getNames()
  {
    return sensors.keySet();
  }

  public void update()
  {
    List<Sensor> sensors= new ArrayList<Sensor>(this.sensorssensors.size());
    sensors.addAll(this.sensorssensors);
    this.sensorssensors.clear();
    this.icons.clear();
    this.sensors.clear();
    this.all_classes.clear();
    this.ant_sensors.clear();
    this.ble_sensors.clear();
    
    for(Sensor sensor : sensors)
      addSensor(sensor);
  }

  public void addBuiltin(String name, Sensors sensors)
  {
    if( BuiltinSensrs.containsKey(name) )
    {
      Sensors us = BuiltinSensrs.get(name);

      for(String x : sensors.ant_sensors)
      {
        if( !us.ant_sensors.contains(x) )
          us.ant_sensors.add(x);
      }

      for(String x : sensors.ble_sensors)
      {
        if( !us.ble_sensors.contains(x) )
          us.ble_sensors.add(x);
      }

      us.icons.putAll(sensors.icons);

      us.all_classes.addAll(sensors.all_classes);

      for(String x : sensors.sensors.get(name))
      {
        if( !us.sensors.get(name).contains(x) )
          us.sensors.get(name).add(x);
      }
    }
    else
      BuiltinSensrs.put(name, sensors);
  }
}
