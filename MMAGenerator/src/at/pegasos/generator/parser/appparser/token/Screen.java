package at.pegasos.generator.parser.appparser.token;

import at.pegasos.generator.parser.appparser.token.screen.*;

import java.util.*;

public class Screen {
  public String clasz;

  public List<Fragment> fragments;

  public String name;

  /**
   * Path to the file in which this screen is defined. Caution: this path is only set when the
   * screen is defined in an external file (i.e. neither in application.xml or a sport xml. In these
   * cases it is null)
   */
  public String fileName;

  public boolean referenced= false;

  public Screen()
  {
    this.fragments= new ArrayList<Fragment>();
  }

  public Screen referencedCopy()
  {
    Screen ret= new Screen();
    ret.clasz= clasz;
    ret.fragments= fragments;
    ret.name= name;
    ret.fileName= fileName;
    ret.referenced= true;
    return ret;
  }

  /**
   * Convert the type name / clasz into a class name. If the type is not built in then the type will
   * be returned (this is done in order to enable custom made screens)
   * 
   * @return
   */
  public String classFromType()
  {
    if( clasz == null || clasz.equals("default") || clasz.equals("segment-3") )
      return "at.pegasos.client.ui.screen.ThreeSegments";
    else if( clasz.equals("garmin") )
      return "at.pegasos.client.ui.screen.GarminStyleScreen";
    return clasz;
  }
}