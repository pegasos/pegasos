package at.pegasos.generator.parser.appparser.token.screen;

import java.util.*;

public class Fragment {
  public static final int TYPE_JAVA= 1;
  public static final int TYPE_GENERATE= 2;

  public int type;

  public String clasz;
  public String method;

  public Map<String, String> args= new HashMap<String, String>();
}
