package at.pegasos.generator.parser.appparser.token;

import java.util.*;

public class Sensor {
  public static final int TYPE_BUILTIN = 1;
  public static final int TYPE_CUSTOM = 2;

  public static class Clasz {
    public enum Type {
      Ant, Ble, Other
    }

    public String clasz;
    public Type type;

    public Clasz copy()
    {
      Clasz ret= new Clasz();
      ret.clasz = this.clasz;
      ret.type= this.type;
      return ret;
    }

    @Override public String toString()
    {
      return "Clasz{" + "clasz='" + clasz + '\'' + ", type=" + type + '}';
    }

    @Override
    public boolean equals(Object o)
    {
      if( this == o )
        return true;
      if( o == null || getClass() != o.getClass() )
        return false;
      Clasz clasz1 = (Clasz) o;
      return Objects.equals(clasz, clasz1.clasz) && type == clasz1.type;
    }

    @Override
    public int hashCode()
    {
      return Objects.hash(clasz, type);
    }
  }

  @Override
  public String toString()
  {
    return "Sensor [name=" + name + ", icon=" + icon + ", classes=" + classes + ", isBuiltin()=" + isBuiltin() + "]";
  }

  private int type;
  public String name;
  public String icon;
  public List<Clasz> classes;

  public boolean isBuiltin()
  {
    return type == TYPE_BUILTIN;
  }

  static int idx= 1;
  public static Sensor dummyEntry()
  {
    Sensor ret= new Sensor();
    ret.name= "Sensor-" + (idx++);

    return ret;
  }

  public void setBuiltIn(Boolean isBuiltIn)
  {
    if( isBuiltIn )
    {
      this.type= TYPE_BUILTIN;
    }
    else
    {
      this.type= TYPE_CUSTOM;
    }
  }
}