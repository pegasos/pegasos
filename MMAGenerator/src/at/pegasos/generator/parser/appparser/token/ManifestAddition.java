package at.pegasos.generator.parser.appparser.token;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;

public class ManifestAddition {
  public final List<Node> application;
  public final List<Node> root;

  public ManifestAddition()
  {
    application = new ArrayList<Node>();
    root = new ArrayList<Node>();
  }

  public void add(ManifestAddition other)
  {
    this.application.addAll(other.application);
    this.root.addAll(other.root);
  }
}
