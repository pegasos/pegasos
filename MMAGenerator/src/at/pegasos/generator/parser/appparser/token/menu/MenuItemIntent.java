package at.pegasos.generator.parser.appparser.token.menu;

import java.util.HashMap;
import java.util.Map;

public class MenuItemIntent extends MenuItem {
  public String intent;

  /**
   * Params/extras for the intent
   */
  public Map<String,String> params= new HashMap<String,String>();
  
  public MenuItemIntent()
  {
  }

  public MenuItemIntent(MenuItem entry)
  {
    this.icon= entry.icon;
    this.text= entry.text;
  }

  @Override
  public MenuItemIntent copy()
  {
    MenuItemIntent ret= new MenuItemIntent();
    ret.icon= icon;
    ret.text= text;
    ret.intent= new String(intent);
    ret.params= new HashMap<String,String>();
    ret.params.putAll(this.params);
    return ret;
  }
}