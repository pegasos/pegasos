package at.pegasos.generator.parser.appparser.token.menu;

import at.pegasos.generator.parser.token.EntryString;

public abstract class MenuItem {
  public EntryString icon;
  public EntryString text;

  public abstract MenuItem copy();
}
