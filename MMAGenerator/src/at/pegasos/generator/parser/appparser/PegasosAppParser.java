package at.pegasos.generator.parser.appparser;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface PegasosAppParser {
  String name() default "";

  int order() default 999;
}
