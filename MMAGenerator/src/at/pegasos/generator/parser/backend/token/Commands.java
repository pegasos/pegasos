package at.pegasos.generator.parser.backend.token;

import java.util.*;

import at.pegasos.generator.generator.GeneratorException;

import java.util.Set;

public class Commands {
  private final Map<Integer, Command> commands;
  private final Set<Integer> required;
  private int max;
  
  public Commands(boolean oldUnivie)
  {
    this.commands= new HashMap<Integer, Command>();
    max= 31;
    // other commands which are built in (and not added by default) are defined in 
    // at.pegasos.generator.parser.backend.token.Command:builtin
    commands.put( 0, new Command( 0, oldUnivie ? "at.univie.mma.server.parser.command.LoginCommand" : "at.pegasos.server.parser.command.LoginCommand", null, null));
    commands.put( 1, new Command( 1, oldUnivie ? "at.univie.mma.server.parser.command.NewSession" : "at.pegasos.server.parser.command.NewSession", null, null));
    commands.put( 2, new Command( 2, oldUnivie ? "at.univie.mma.server.parser.command.ContinueSession" : "at.pegasos.server.parser.command.ContinueSession", null, null));
    commands.put( 3, new Command( 3, oldUnivie ? "at.univie.mma.server.parser.command.NoOperation" : "at.pegasos.server.parser.command.NoOperation", null, null));
    commands.put(11, new Command(11, oldUnivie ? "at.univie.mma.server.parser.command.Heartbeat" : "at.pegasos.server.parser.command.Heartbeat", null, null));
    commands.put(31, new Command(31, oldUnivie ? "at.univie.mma.server.parser.command.StopSession" : "at.pegasos.server.parser.command.StopSession", null, null));

    required= new HashSet<Integer>();
    required.addAll(commands.keySet());
  }

  public String toString()
  {
    String ret= "[";
    boolean comma= false;
    for(Map.Entry<Integer, Command> e : commands.entrySet())
    {
      if( !required.contains(e.getKey()) )
      {
        if( comma )
          ret+= ",";
        ret+= "<" + e.getValue().code + ":" + e.getValue().name + ">";
        comma= true;
      }
    }
    ret+= "]";
    
    return ret;
  }
  
  public void addCommands(List<Command> cmds) throws GeneratorException
  {
    for(Command command : cmds)
    {
      if( commands.containsKey(command.code) )
        throw new GeneratorException("Multiple definitions of command " + command.code + " in client-server communication");
      
      if( command.code > max )
        max= command.code;
      
      commands.put(command.code, command);
    }
  }
  
  public int getMaxId()
  {
    return max;
  }

  public String getServerClass(int i)
  {
    if( commands.containsKey(i) )
      return commands.get(i).server_class;
    else
      return null;
  }
  
  /**
   * Returns the list of all optional commands
   * @return
   */
  public List<Command> getCommands()
  {
    List<Command> ret= new ArrayList<Command>(commands.size() - required.size());
    for(Map.Entry<Integer, Command> x : commands.entrySet())
    {
      if( !required.contains(x.getKey()) )
        ret.add(x.getValue());
    }
    return ret;
  }
  
  /**
   * Returns the list of all commands
   * @return
   */
  public Collection<Command> getAllCommands()
  {
    return commands.values();
  }
}
