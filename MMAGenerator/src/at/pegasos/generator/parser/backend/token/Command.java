package at.pegasos.generator.parser.backend.token;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.parser.token.*;

import java.util.*;

public class Command {
  /**
   * Code which is used during the communication (client-server)
   */
  public int code;

  /**
   * Class used for parsing data sent to the server
   */
  public String server_class;

  /**
   * Class used in the ServerInterface as command/request. 
   * If null this class does not exists (because its a lowlevel command), is irrelevant, etc.
   */
  public String client_command_class;

  /**
   * Class used in the ServerInterface as response. 
   * If null this class will be generated
   */
  public String response_class;

  public String name;

  public ResponseField[] fields;

  /**
   * Whether or not this Command is a build-in command. 
   */
  public boolean builtin;

  public Command(int code, String server_class, String client_command, String client_parser)
  {
    this.code= code;
    this.server_class= server_class;
    this.client_command_class= client_command;
    this.response_class= client_parser;
  }

  public Command()
  {
  }

  public static Command builtIn(int code, String name) throws ParserException
  {
    Command ret= new Command();
    ret.name= name;
    ret.code= code;
    ret.builtin= true;

    switch (name)
    {
      case "GroupActivity":
      /*
      <server>at.univie.mma.server.parser.command.BMAx</server>
      <response>
        <field nr="0" type="int" name="Message" />
      </response>
       */
        ret.server_class = "at.pegasos.server.parser.command.GroupActivity";
        ret.fields = new ResponseField[1];
        ret.fields[0] = new ResponseField();
        ret.fields[0].nr = -1;
        ret.fields[0].name = "Data";
        ret.client_command_class = "at.univie.mma.serverinterface.command.GroupActivity";
        ret.response_class = "at.univie.mma.serverinterface.response.GroupActivity";
        break;
      case "TimeDiff":
        ret.server_class = "at.pegasos.server.parser.command.TimeDiff";
        ret.fields = new ResponseField[2];
        ret.fields[0] = new ResponseField();
        ret.fields[0].nr = 0;
        ret.fields[0].type = EntryType.fromString("string");
        ret.fields[0].name = "diff";
        ret.fields[1] = new ResponseField();
        ret.fields[1].nr = 1;
        ret.fields[1].type = EntryType.fromString("string");
        ret.fields[1].name = "servert";
        ret.client_command_class = "at.univie.mma.serverinterface.command.TimeDiff";
        ret.response_class = "at.univie.mma.serverinterface.response.TimeDiffMessage";
        break;
      case "AIMessage":
        ret.server_class = "at.pegasos.server.parser.command.AIMessage";
        ret.fields = new ResponseField[2];
        ret.fields[0] = new ResponseField();
        ret.fields[0].nr = 0;
        ret.fields[0].type = EntryType.fromString("int");
        ret.fields[0].name = "Type";
        ret.fields[1] = new ResponseField();
        ret.fields[1].nr = -1;
        ret.fields[1].type = EntryType.fromString("general");
        ret.fields[1].name = "Data";
        ret.client_command_class = "at.pegasos.serverinterface.command.AIMessage";
        // ret.response_class = "at.univie.mma.serverinterface.response.TimeDiffMessage";
        break;
      case "FileUpload":
        ret.server_class = "at.pegasos.server.parser.command.FileUpload";
        ret.client_command_class = "at.univie.mma.serverinterface.command.FileUpload";
        ret.response_class = "at.univie.mma.serverinterface.response.FileUpload";
        // System.out.println("FileUpload: " + ret);
        break;
      default:
        throw new ParserException("Unknown command '" + name + "'");
    }

    return ret;
  }

  @Override
  public String toString()
  {
    return "Command [code=" + code + ", server_class=" + server_class + ", client_command_class=" + client_command_class
        + ", response_class=" + response_class + ", name=" + name + ", fields=" + Arrays.toString(fields) + ", builtin=" + builtin + "]";
  }
}
