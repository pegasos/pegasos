package at.pegasos.generator.parser.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.backend.token.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class CommunicationParser {
  private final static Logger log = LoggerFactory.getLogger(CommunicationParser.class);

  private final Path file;
  private final List<Command> commands;

  public CommunicationParser(Path file)
  {
    this.file= file;
    this.commands= new ArrayList<Command>();
  }

  public void parse() throws ParserException, GeneratorException
  {
    // File is not required so do nothing when non-existent
    if( !Files.exists(this.file) )
    {
      log.info(this.file + " does not exist --> no additional commands from this app");
      return;
    }

    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(file.toFile());
      doc.getDocumentElement().normalize();

      NodeList nList = doc.getElementsByTagName("commands");
      for(int temp = 0; temp < nList.getLength(); temp++)
      {
        NodeList commands = ((Element) nList.item(temp)).getElementsByTagName("command");

        for(int i = 0; i < commands.getLength(); i++)
        {
          Node nNode = commands.item(i);

          parseCommand(nNode);
        }
      }
    }
    catch (ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }

  private void parseCommand(Node nNode) throws ParserException
  {
    Element e= (Element) nNode;
    Command command= new Command();

    command.name= e.getAttribute("name");
    command.code= Integer.parseInt(e.getAttribute("code"));

    log.debug("Parsing command {} {} {}", command.name, command.code, e.getAttribute("builtin"));

    command.builtin= Boolean.parseBoolean(XMLHelper.getAttribute(e, "builtin", "false"));

    // TODO: check if necessary attribute are present
    if( !command.builtin )
    {
      NodeList l= e.getElementsByTagName("server");
      if( l.getLength() > 0 )
      {
        Node s= l.item(0);
        Text val= (Text) s.getChildNodes().item(0);
        command.server_class= val.getNodeValue();
        if( l.getLength() > 1 )
          throw new ParserException("More than one server class set for " + val);
      }

      l= e.getElementsByTagName("client");
      if( l.getLength() > 0 )
      {
        command.client_command_class= XMLHelper.getStringValueOfChild((Element) l.item(0));
        if( l.getLength() > 1 )
          throw new ParserException("More than one client command class set for " + command.name);
      }

      l= e.getElementsByTagName("response");
      if( l.getLength() > 0 )
      {
        String clasz= ((Element) l.item(0)).getAttribute("class");
        if( !clasz.equals("") )
          command.response_class= clasz;
        NodeList fields= ((Element) l.item(0)).getElementsByTagName("field");
        if( fields.getLength() > 0 )
        {
          command.fields= new ResponseField[fields.getLength()];
          for(int i= 0; i < fields.getLength(); i++)
          {
            Element field= (Element) fields.item(i);
            ResponseField f= new ResponseField();
            String nr= field.getAttribute("nr");
            if( nr.equals("") )
              f.nr= -1;
            else
              f.nr= Integer.parseInt(nr);
            f.type= EntryType.fromAttribute(field, "type");
            f.name= field.getAttribute("name");
            command.fields[i]= f;
          }
        }
      }

      commands.add(command);
    }
    else
    {
      commands.add(Command.builtIn(command.code, command.name));
    }
  }

  public List<Command> getCommands()
  {
    return commands;
  }
}
