package at.univie.mma.pegasos.webgenerator;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import org.slf4j.*;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.generator.server.web.backend.*;
import at.pegasos.generator.generator.server.web.frontend.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.webgenerator.backend.*;
import at.pegasos.generator.generator.server.web.frontend.MenuGenerator;
import at.pegasos.generator.generator.ConfigGenerator;

public class WebGenerator extends PegasosModuleGenerator {
  
  private final Path outdir;
  
  private final ServerInstance inst;

  private List<CodeGenerator> generators;
  private final static Logger log= LoggerFactory.getLogger(WebGenerator.class);
  
  public WebGenerator(Path outdir, ServerInstance instance)
  {
    this.outdir= outdir;
    this.inst= instance;
  }
  
  public void run() throws GeneratorException
  {
    try
    {
      generateBackend();
      generateFrontend();
    }
    catch( IOException | ParserException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e.getMessage());
    }
  }

  private void generateBackend() throws ParserException, IOException, GeneratorException
  {
    // Copy database configuration into the web configuration
    // A copy of the configuration is created on purpose
    // If this is not done then the db password will be visible in the configuration of the frontend and thus exposed
    Config config= new Config();
    config.add(inst.getWeb().config);
    config.setValueString("spring.datasource.url", inst.config.getValueString("db.url"));
    config.setValueString("spring.datasource.driver-class-name", inst.config.getValueString("db.driver"));
    config.setValueString("spring.datasource.username", inst.config.getValueString("db.user"));
    config.setValueString("spring.datasource.password", inst.config.getValueString("db.password"));
    config.setValueString("ai.file", inst.config.getValueString("ai_file"));
    // rewrite variable to spring fashion
    String contextPath = config.getValue("backend.path");
    if( !contextPath.startsWith("/") )
      contextPath = "/" + contextPath;
    if( contextPath.endsWith("/") )
      contextPath = contextPath.substring(0, contextPath.length() - 1);
    config.setValueString("server.servlet.context-path", contextPath);
    config.setValue("server.port", config.getValue("backend.port"));
    // server.servlet.context-path backend.path 
    // server.port backend.port

    log.debug("Generating config for web backend");
    CodeGenerator gen= new ConfigGeneratorProps("pegasos", config);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("backend").resolve("gen").resolve("resources"));
    gen.generate();
    gen.closeOutput();
    log.debug("--");

    config= new Config();
    config.add(inst.getWeb().config);
    config.setValueString("ai_file", inst.config.getValueString("ai_file"));
    
    gen= new ConfigGenerator("at.pegasos.server.frontback", config);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("backend").resolve("gen").resolve("java"));
    gen.generate();
    gen.closeOutput();

    gen = new TableDescriptionsGenerator(inst);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("backend").resolve("gen").resolve("java"));
    gen.generate();
    gen.closeOutput();

    log.info("Generate Charts Configuration");

    generators= new ArrayList<CodeGenerator>();
    fetchBackendGenerators();

    for(CodeGenerator generator : generators)
    {
      generator.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("backend").resolve("gen").resolve("java"));
      generator.generate();
      generator.closeOutput();
    }
  }
  
  private void generateFrontend() throws IOException, GeneratorException, ParserException
  {
    Path dir = outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app");
    log.info("Generate Charts Configuration (Frontend)");
    CodeGenerator gen= new ChartConfigurerGenerator(inst.getWeb().charts);
    gen.setOutputToPrintStream(dir);
    gen.generate();
    gen.closeOutput();
    
    gen= new ChartFactoryGenerator(inst.getWeb().charts);
    gen.setOutputToPrintStream(dir);
    gen.generate();
    gen.closeOutput();
    
    log.info("Generate activity names (Frontend)");
    gen= new ActivityNamesGeneratorFrontend();
    ((ActivityNamesGeneratorFrontend) gen).setActivities(inst.getWeb().activitynames);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app"));
    gen.generate();
    gen.closeOutput();

    log.info("Generate Configuration");
    Config config= new Config();
    config.add(inst.getWeb().config);

    if( !config.hasValue("backend.url") )
    {
      throw new GeneratorException("Missing configuration: 'backend.url' (in web.xml)");
    }

    String baseURL = Web.getBackendBaseUrl(config);
    config.setValueString("backendBaseURL", baseURL);
    for(String varName : new HashSet<>(config.getValueNames()))
    {
      if( varName.toLowerCase().endsWith("secret") || varName.startsWith("backend.") || varName.equals("db.url") )
      {
        log.info("Not exposing variable '{}' to web-frontend", varName);
        config.remove(varName);
      }
    }
    gen= new at.pegasos.generator.generator.server.web.frontend.ConfigGenerator("", config);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app"));
    gen.generate();
    gen.closeOutput();

    log.info("Configuring routes?: {}", inst.getWeb().routes.size() == 0);
    if( inst.getWeb().routes.size() == 0 )
    {
      // TODO: can we remove this check?
      throw new GeneratorException("This should not happen. Why do we not have any routes?");
    }

    generators= new ArrayList<CodeGenerator>();
    fetchFrontendGenerators();

    for(CodeGenerator generator : generators)
    {
      generator.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app"));
      generator.generate();
      generator.closeOutput();
    }

    gen= new AddedComponentsGenerator(inst.getWeb().components);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app"));
    gen.generate();
    gen.closeOutput();

    gen= new MenuGenerator(inst.getWeb().menu);
    gen.setOutputToPrintStream(outdir.resolve("server").resolve("web").resolve("frontend").resolve("src").resolve("app"));
    gen.generate();
    gen.closeOutput();
  }

  protected void fetchFrontendGenerators() throws GeneratorException
  {
    List<Class<? extends CodeGenerator>> generatorClasses = fetchGenerators(WebFrontendGenerator.class,
            "at.pegasos.generator.generator.server.web.frontend");

    log.debug("Found generators for web: {}", generatorClasses.toString());

    for(Class<? extends CodeGenerator> clasz : generatorClasses)
    {
      try
      {
        CodeGenerator gen= (CodeGenerator) clasz.getConstructor(Web.class).newInstance(inst.getWeb());
        generators.add(gen);
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
              | NoSuchMethodException | SecurityException e )
      {
        throw new GeneratorException(e);
      }
    }
  }

  protected void fetchBackendGenerators() throws GeneratorException
  {
    List<Class<? extends CodeGenerator>> generatorClasses = fetchGenerators(WebBackendGenerator.class,
            WebBackendGenerator.class.getPackageName());

    log.debug("Found generators for web: {}", generatorClasses.toString());

    for(Class<? extends CodeGenerator> clasz : generatorClasses)
    {
      try
      {
        CodeGenerator gen= (CodeGenerator) clasz.getConstructor(Web.class).newInstance(inst.getWeb());
        generators.add(gen);
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
              | NoSuchMethodException | SecurityException e )
      {
        throw new GeneratorException(e);
      }
    }
  }
}
