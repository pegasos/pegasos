package at.univie.mma.pegasos.webgenerator;

import java.io.IOException;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.util.DAG.Node;
import at.pegasos.util.DAG.NodeVisitor;
import at.pegasos.generator.parser.webparser.token.ActivityNames;
import at.pegasos.generator.parser.webparser.token.ActivityNames.Activity;

public class ActivityNamesGeneratorFrontend extends TypeScriptCodeGenerator {
  private ActivityNames names;

  public ActivityNamesGeneratorFrontend()
  {
    super("shared", "activities", null, null);
  }
  
  public void setActivities(ActivityNames names)
  {
    this.names= names;
  }
  
  private int idn;

  @Override
  public void generate() throws GeneratorException
  {
    /*
    public activitytypes =  [
      {'id': 1, 'itemName': 'Training', 'param0': 1, 'level': 1},
      {'id': 2, 'itemName': 'Test', 'param0': 9, 'level': 1},
      {'id': 3, 'itemName': 'Test HRV', 'param0': 9, 'param1': 5, 'level': 2},
      {'id': 4, 'itemName': 'HRV', 'param0': 9, 'param1': 5, 'param2': 1, 'level': 3}
    ];
     */
    
    output("export const activitytypes = [");
    indent();
    
    Node<Activity> root= names.getRoot();
    idn= 1;
    
    root.traverse(new NodeVisitor<ActivityNames.Activity>() {
      
      @Override
      public void run()
      {
        // System.out.println("x" + id.toString() + " " + node.getChildren().size());
        if( id.name == null )
        {
          // output("Root");
        }
        else
        {
          int level= 0;
          String line= "{'id': " + (idn++) + ", 'itemName': '" + id.name + "'";
          if( id.param0 != -1 )
          {
            line+= ", 'param0': " + id.param0;
            level++;
          }
          if( id.param1 != -1 )
          {
            line+= ", 'param1': " + id.param1;
            level++;
          }
          if( id.param2 != -1 )
          {
            line+= ", 'param2': " + id.param2;
            level++;
          }
          output(line + ", 'level': " + level + "},");
        }
      }
    }, new NodeVisitor<ActivityNames.Activity>() {
      
      @Override
      public void run()
      {
        
      }
    });
    
    indent_less();
    output("];");
  }
  
  public static void main(String[] args) throws IOException, ParserException, GeneratorException
  {
    ActivityNames names= new ActivityNames();
    System.out.println("Training");
    names.addName(1, "Training");
    System.out.println("PerPot");
    names.addName(2, 1, "PerPot Run");
    System.out.println("LSCT");
    names.addName(9, 6, "LSCT");
    System.out.println("HRV");
    names.addName(9, 5, 1, "HRV");
    System.out.println("Orthostatic");
    names.addName(9, 5, 2, "Orthostatic");
    names.addName(9, 5, "Test HRV");
    names.addName(9, "Test");
    
    ActivityNames names2= new ActivityNames();
    names2.addName(3, "3");
    names2.addName(1, 1, 1, "Spezial Training");
    
    names2.add(names);
    
    names2.debug();
    
    ActivityNamesGeneratorFrontend gen= new ActivityNamesGeneratorFrontend();
    gen.setActivities(names2);
    gen.setOutput(System.out);
    gen.generate();
  }
}
