package at.univie.mma.pegasos.webgenerator;

import java.io.IOException;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.Charts;
import at.pegasos.generator.parser.webparser.token.charts.SingleLineChart;
import at.pegasos.generator.parser.webparser.token.charts.Type;

public class ChartFactoryGenerator extends TypeScriptCodeGenerator {

  private final Type[] types;

  public ChartFactoryGenerator(Charts charts)
  {
    super("charts", "ChartFactory", null, null);
    types= new Type[charts.getTypes().size() + 6];

    Type line= new Type();
    line.clas= "LineChartComponent";
    line.imp= "{ LineChartComponent } from './linechart.component'";
    line.type= "Line";
    Type mline= new Type();
    mline.clas= "MultiLineChartComponent";
    mline.imp= "{ MultiLineChartComponent } from './multilinechart.component'";
    mline.type= "MultiLine";
    Type map= new Type();
    map.clas= "MapChartComponent";
    map.imp= "{ MapChartComponent } from './mapchart.component'";
    map.type= "Map";
    Type poin= new Type();
    poin.clas= "PoincareChartComponent";
    poin.imp= "{ PoincareChartComponent } from './poincarechart.component'";
    poin.type= "Poincare";
    Type tacho= new Type();
    tacho.clas= "TachogrammChartComponent";
    tacho.imp= "{ TachogrammChartComponent } from './tachogramm.component'";
    tacho.type= "Tachogram";
    Type bars= new Type();
    bars.clas= "BarsChartComponent";
    bars.imp= "{ BarsChartComponent } from './barschart.component'";
    bars.type= "Bars";

    types[0]= line;
    types[1]= mline;
    types[2]= map;
    types[3]= poin;
    types[4]= tacho;
    types[5]= bars;

    int i= 6;
    for(Type t : charts.getTypes())
    {
      types[i++]= t;
    }
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("{ ComponentFactoryResolver, ComponentFactory } from '@angular/core'");
    addImport("{ Type } from './../shared/sample'");
    addImport("{ ChartComponent } from './chart.component'");
    StringBuilder chartcomponents= new StringBuilder("[");
    boolean first= true;
    for(Type imp : types)
    {
      addImport(imp.imp);
      chartcomponents.append(first ? "" : ", ").append(imp.clas);
      first= false;
    }
    chartcomponents.append("]");

    constant("CHARTCOMPONENTS", chartcomponents.toString());

    header();
    indent();

    method_begin("public static", "ComponentFactory<ChartComponent>", "factoryForType(componentFactoryResolver: ComponentFactoryResolver, type: Type)");
    output("switch( type )");
    output("{");
    indent();

    for(Type t : types)
    {
      // case 'Line':
      //   return componentFactoryResolver.resolveComponentFactory(D3ChartComponent);
      output("case '" + t.type + "':");
      indent();
      output("return componentFactoryResolver.resolveComponentFactory(" + t.clas + ");");
      indent_less();
    }

    output("default:");
    indent();
    output("return componentFactoryResolver.resolveComponentFactory(" + types[0].clas + ");");
    indent_less();

    indent_less();
    output("}");

    method_end();

    footer();
  }

  public static void main(String[] args) throws IOException, ParserException, GeneratorException
  {
    Charts charts= new Charts();
    charts.addChart(new SingleLineChart("chart_BikeP", "bike", "power"));
    charts.addChart(new SingleLineChart("chart_BikeC", "bike", "cadence"));

    ChartFactoryGenerator gen= new ChartFactoryGenerator(charts);
    gen.setOutput(System.out);
    gen.generate();
  }
}
