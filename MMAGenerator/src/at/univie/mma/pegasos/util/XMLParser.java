package at.univie.mma.pegasos.util;

import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import at.pegasos.generator.parser.*;

public abstract class XMLParser implements PegasosParser {
  protected final Path file;
  
  protected Document doc;

  public XMLParser(Path path)
  {
    this.file = path;
  }

  protected void xml_open() throws ParserException
  {
    try
    {
      DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder= dbFactory.newDocumentBuilder();
      doc= dBuilder.parse(file.toFile());
      doc.getDocumentElement().normalize();
    }
    catch (ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      throw new ParserException(e.getMessage());
    }
  }
}
