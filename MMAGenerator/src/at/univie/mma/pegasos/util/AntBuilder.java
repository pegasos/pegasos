package at.univie.mma.pegasos.util;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.tools.ant.*;
import org.eclipse.jgit.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AntBuilder extends Builder {
  private final static Logger log= LoggerFactory.getLogger(AntBuilder.class);
  //Ant specific stuff
  Project ant;
  
  // constants for the dirs
  private String build_file_name;
  
  private final Map<String,String> properties;
  
  
  private Writer logfile;

  /**
   * @param output Where the build will be located
   */
  public AntBuilder(Path output, String name)
  {
    super(output, name);
    
    this.properties= new HashMap<String, String>();
 
    build_file_name= "build.xml";
  }
  
  public void setBuildFileName(String name)
  {
    this.build_file_name= name;
  }
  
  @Override
  public void prepareBuild() throws IOException
  {
    ant= new Project();
    ant.init();
    ant.setSystemProperties();
    
    logfile= Files.newBufferedWriter(Paths.get(getName() + ".ant.log"));
    
    // ant.setProperty("java.home", System.getenv().get("JAVA_HOME"));
    // ant.setProperty("JAVA_HOME", System.getenv().get("JAVA_HOME"));
    
    ant.addBuildListener(new BuildListener() {
      
      @Override
      public void taskStarted(BuildEvent event) {
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] taskStarted %s" , taskName, event.getMessage());
        // System.out.println(message);
        try
        {
          logfile.write(message + "\n");
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
      
      @Override
      public void taskFinished(BuildEvent event) {
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] taskFinished %s" , taskName, event.getMessage());
        // System.out.println(message);
        try
        {
          logfile.write(message + "\n");
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
      
      @Override
      public void targetStarted(BuildEvent event) {
        System.out.println("targetStarted: " + getName());
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] targetStarted %s" , taskName, event.getMessage());
        // System.out.println(message);
        try
        {
          logfile.write(message + "\n");
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
      
      @Override
      public void targetFinished(BuildEvent event) {
        System.out.println("targetFinished: " + getName());
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] targetFinished %s" , taskName, event.getMessage());
        System.out.println(message);
        
        try
        {
          log.debug("Build finished");
          logfile.write("Build finished: " + event.getMessage() + " " + event.getTask() + " " + (event.getTask() != null ? event.getTask().getTaskName() : ""));
        }
        catch( IOException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      
      @Override
      public void messageLogged(BuildEvent event) {
        final Task task= event.getTask();
        final Target target= event.getTarget(); 
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String t= (target == null) ? "" : target.getName() + ":";
        String message= String.format("[%sant.%s] %s", t, taskName, event.getMessage());
        // System.out.println(message);
        try
        {
          logfile.write(message + "\n");
        }
        catch( IOException e )
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
          System.out.println(e.getMessage());
          log.error(e.getMessage());
        }
      }
      
      @Override
      public void buildStarted(BuildEvent event) {
        System.out.println("Build started: " + getName());
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] buildStarted %s" , taskName, event.getMessage());
        // System.out.println(message);
        try
        {
          logfile.write(message + "\n");
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
      
      @Override
      public void buildFinished(BuildEvent event) {
        final Task task= event.getTask();
        String taskName= (task == null) ? "unknown" : task.getTaskName();
        String message= String.format("[ant.%s] buildFinished %s" , taskName, event.getMessage());
        System.out.println(message);
        
        System.out.println("Build Finished: " + getName());
        try
        {
          log.debug("Build finished");
          logfile.write("Build finished: " + event.getMessage() + " " + event.getTask() + " " + (event.getTask() != null ? event.getTask().getTaskName() : ""));
          logfile.flush();
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
    });
    
    File buildFile = build_dir.resolve(build_file_name).toAbsolutePath().toFile();
    ProjectHelper.configureProject(ant, buildFile);
  }
  
  @Override
  public void buildTargets(String... targets) throws IOException, InterruptedException
  {
    // ant.executeTarget(target);
    String javaHome = System.getProperty("java.home");
    String javaBin = javaHome +
            File.separator + "bin" +
            File.separator + "java";
    String classpath = System.getProperty("java.class.path").replace("/cygdrive/c/", "C:/");
    String className = this.getClass().getCanonicalName();

    String[] args= new String[targets.length + 8 + properties.size()*2];
    int i= 8;
    args[0]= javaBin;
    args[1]= "-cp";
    args[2]= classpath;
    args[3]= className;
    args[4]= build_dir.toAbsolutePath().toString();
    args[5]= build_file_name;
    args[6]= getName();
    args[7]= "" + properties.size()*2;
    for(Entry<String, String> e : properties.entrySet())
    {
      args[i++]= e.getKey();
      args[i++]= e.getValue();
    }
    for(String target : targets)
    {
      args[i++]= target;
    }
    
    log.info("Running: " + StringUtils.join(Arrays.asList(args), " "));
    log.debug(StringUtils.join(Arrays.asList(args), " "));
    
    ProcessBuilder builder = new ProcessBuilder(args);

    builder.inheritIO();
    
    Process process = builder.start();
    /*BufferedReader line_in = new BufferedReader(new InputStreamReader(process.getInputStream()));
    String line_out;
    try {while ((line_out = line_in.readLine()) != null) {System.out.println("AI call returned with:"+line_out);}} catch (IOException e) {}
    line_in = new BufferedReader(new InputStreamReader(process.getErrorStream()));
    try {while ((line_out = line_in.readLine()) != null) {System.out.println("AI call returned with:"+line_out);}} catch (IOException e) {}*/
    process.waitFor();
  }
  
  private void buildTargetDirect(String target)
  {
    try
    {
      ant.executeTarget(target);
    }
    catch(Exception e)
    {
      if( logfile != null )
      {
        try
        {
          logfile.write("\nBuild failed!\n");
          logfile.flush();
          logfile.close();
        }
        catch( IOException e1 )
        {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
        throw e;
      }
    }
  }
  
  @Override
  public void setProperty(String name, String value)
  {
    // ant.setUserProperty(name, value);
    properties.put(name, value);
  }
  
  private void setPropertyDirect(String name, String value)
  {
    ant.setUserProperty(name, value);
  }
  
  public static void main(String[] args) throws IOException
  {
    int i;
    AntBuilder b= new AntBuilder(Paths.get(args[0]), args[2]);
    b.setBuildFileName(args[1]);
    b.prepareBuild();
    
    int p= Integer.parseInt(args[3]);
    
    for(i= 4; (i-4) < p; i+=2)
    {
      b.setPropertyDirect(args[i], args[i+1]);
    }
    
    System.out.println("Builder: building target: " + args[i]);
    for(; i < args.length; i++)
    {
      System.out.println("Builder: building target: " + args[i]);
      b.buildTargetDirect(args[i]);
    }
  }
}
