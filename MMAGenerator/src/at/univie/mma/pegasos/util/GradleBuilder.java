package at.univie.mma.pegasos.util;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.*;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

/*
    args[0] - Path to the module directory
    args[1] - Number of arguments
    args[2]... - Arguments
 */
@Slf4j
public class GradleBuilder extends Builder {
  
  private final Path projectDir;
  private String[] tasks;
  private Integer numberOfTasks;
  private ProjectConnection connection;
  
  private final List<String> args;
  
  public GradleBuilder(Path path, String name)
  {
    super(path, name);
    this.projectDir= path;
    this.args= new ArrayList<String>(3);
  }
  
  public static void main(String[] args) throws IOException, InterruptedException
  {
    GradleBuilder builder= new GradleBuilder(Paths.get(args[0]), "");
    
    builder.numberOfTasks= Integer.parseInt(args[1]);
    builder.tasks= new String[builder.numberOfTasks];
    
    for(int i= 2; (i - 2) < builder.numberOfTasks; i++)
    {
      builder.tasks[i - 2]= args[i];
    }
    
    builder.buildTargets(builder.tasks);
  }
  
  /*protected void runBuild(File projectDir, String... tasks)
  {
    
    ProjectConnection connection= GradleConnector.newConnector().forProjectDirectory(projectDir).connect();
    try
    {
      connection.newBuild().forTasks(tasks).run();
      
    }
    finally
    {
      connection.close();
    }
  }*/
  
  /*
  public void copyAndroidBuildFiles(Path configdir) throws IOException
  {
    // configuration files for the build
    Path conf= configdir.resolve("local.properties").toAbsolutePath().normalize();
    Path build= build_dir.resolve("..").resolve("local.properties");
    LoggerFactory.getLogger(this.getClass()).debug("Copy " + conf.toString() + " to " + build.toString());
    Files.copy(conf, build, REPLACE_EXISTING);
    
    /*conf= configdir.resolve("local.properties").toAbsolutePath().normalize();
    build= build_dir.resolve("..").resolve("appcompat_v7").resolve("local.properties");
    Files.copy(conf, build, REPLACE_EXISTING);*/
//  }

  
  @Override
  public void prepareBuild() throws IOException
  {
    connection= GradleConnector.newConnector().forProjectDirectory(projectDir.toAbsolutePath().toFile()).connect();
  }
  
  @Override
  public void buildTargets(String... targets) throws IOException, InterruptedException
  {
    try
    {
      BuildLauncher launcher= connection
          .newBuild()
          .forTasks(targets)
          .setStandardOutput(System.out)
          .setStandardError(System.err);
      if( args.size() > 0 )
      {
        launcher.withArguments(args.toArray(new String[0]));
      }
      launcher.run();
      // connection.newBuild().forTasks(targets).withArguments("--debug", "--stacktrace").setStandardOutput(System.out).setStandardError(System.err).run();
    }
    finally
    {
      connection.close();
    }
  }
  
  @Override
  public void setProperty(String name, String value)
  {
    args.add("-P" + name + "=" + value);
  }
}