package at.univie.mma.pegasos.util;

import java.io.IOException;
import java.nio.file.Path;

public abstract class Builder {
  protected final Path build_dir;
  
  private final String name;

  /**
   * @param output
   *          Where the build will be located
   * @param name
   *          Name of the build
   */
  public Builder(Path output, String name)
  {
    this.build_dir= output;
    this.name= name;
  }

  public String getName()
  {
    return name;
  }
  
  /**
   * Setup the build environment for the current build
   * @throws IOException
   */
  public abstract void prepareBuild() throws IOException;
  
  /**
   * Build all the targets/execute all the tasks specified by `targets`
   * @param targets
   * @throws IOException
   * @throws InterruptedException
   */
  public abstract void buildTargets(String... targets) throws IOException, InterruptedException;
  
  /**
   * Set a property specified by name. 
   * @param name
   * @param value
   */
  public abstract void setProperty(String name, String value);
}
