package at.univie.mma.pegasos.util;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemLoopException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import static java.nio.file.StandardCopyOption.*;
import static java.nio.file.FileVisitResult.*;

import java.util.EnumSet;

import org.slf4j.Logger;

public class FileHelper {
  
  /**
   * Copy source file to target location. If {@code prompt} is true then
   * prompt user to overwrite target if it exists. The {@code preserve}
   * parameter determines if file attributes should be copied/preserved.
   * @see https://docs.oracle.com/javase/tutorial/essential/io/examples/Copy.java
   */
  static void copyFile(Path source, Path target, boolean prompt, boolean preserve) {
      CopyOption[] options = (preserve) ?
          new CopyOption[] { COPY_ATTRIBUTES, REPLACE_EXISTING } :
          new CopyOption[] { REPLACE_EXISTING };
      if (!prompt || Files.notExists(target)) {
          try {
              Files.copy(source, target, options);
          } catch (IOException x) {
              System.err.format("Unable to copy: %s: %s%n", source, x);
          }
      }
  }
  
  /**
   * A {@code FileVisitor} that copies a file-tree ("cp -r")
   * @see https://docs.oracle.com/javase/tutorial/essential/io/examples/Copy.java
   */
  static class TreeCopier implements FileVisitor<Path> {
      private final Path source;
      private final Path target;
      private final boolean prompt;
      private final boolean preserve;
      
      TreeCopier(Path source, Path target, boolean prompt, boolean preserve) {
          this.source = source;
          this.target = target;
          this.prompt = prompt;
          this.preserve = preserve;
      }

      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
          // before visiting entries in a directory we copy the directory
          // (okay if directory already exists).
          CopyOption[] options = (preserve) ?
              new CopyOption[] { COPY_ATTRIBUTES } : new CopyOption[0];

          Path newdir = target.resolve(source.relativize(dir));
          try {
              Files.copy(dir, newdir, options);
          } catch (FileAlreadyExistsException x) {
              // ignore
          } catch (IOException x) {
              System.err.format("Unable to create: %s: %s%n", newdir, x);
              return SKIP_SUBTREE;
          }
          return CONTINUE;
      }

      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
          copyFile(file, target.resolve(source.relativize(file)),
                   prompt, preserve);
          return CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
          // fix up modification time of directory when done
          if (exc == null && preserve) {
              Path newdir = target.resolve(source.relativize(dir));
              try {
                  FileTime time = Files.getLastModifiedTime(dir);
                  Files.setLastModifiedTime(newdir, time);
              } catch (IOException x) {
                  System.err.format("Unable to copy all attributes to: %s: %s%n", newdir, x);
              }
          }
          return CONTINUE;
      }

      @Override
      public FileVisitResult visitFileFailed(Path file, IOException exc) {
          if (exc instanceof FileSystemLoopException) {
              System.err.println("cycle detected: " + file);
          } else {
              System.err.format("Unable to copy: %s: %s%n", file, exc);
          }
          return CONTINUE;
      }
  }

  static class TreeDeleter implements FileVisitor<Path> {
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
    {
      return CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
    {
      try
      {
        Files.delete(file);
      }
      catch( IOException x )
      {
        System.err.format("Unable to delete %s", file.toString());
      }
      return CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
    {
      // fix up modification time of directory when done
      if( exc == null )
      {
        try
        {
          Files.delete(dir);
        }
        catch( IOException x )
        {
          System.err.format("Unable to delete %s", dir.toString());
        }
      }
      return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc)
    {
      if( exc instanceof FileSystemLoopException )
      {
        System.err.println("cycle detected: " + file);
      }
      else
      {
        System.err.format("Unable to copy: %s: %s%n", file, exc);
      }
      return CONTINUE;
    }
  }

  public static void copyDirectory(Path source, Path destination) throws IOException
  {
    if( !Files.exists(destination) )
    {
      Files.createDirectories(destination);
    }

    //follow links when copying files
    EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);

    TreeCopier tc = new TreeCopier(source, destination, false, true);

    Files.walkFileTree(source, opts, Integer.MAX_VALUE, tc);
  }

  /**
   * Delete an entire directory and its contents
   * @param file
   * @throws IOException
   */
  public static void deleteDirectory(Path file) throws IOException
  {
    TreeDeleter tc = new TreeDeleter();

    Files.walkFileTree(file, tc);
  }

  /**
   * Delete an entire directory and its contents and handle ioexceptions gracefully
   * @param file
   */
  public static void deleteDirectorySafe(Path file)
  {
    deleteDirectorySafe(file, null);
  }

  /**
   * Delete an entire directory and its contents and handle ioexceptions gracefully
   * @param file
   * @param logger if logger != null any exceptions will be logged
   */
  public static void deleteDirectorySafe(Path file, Logger logger)
  {
    try
    {
      deleteDirectory(file);
    }
    catch(IOException e)
    {
      e.printStackTrace();
      if( logger != null )
      {
        logger.warn("Could not delete " + file + ": " + (e.getMessage() != null ? e.getMessage() : "<no message>"));
      }
    }
  }
}
