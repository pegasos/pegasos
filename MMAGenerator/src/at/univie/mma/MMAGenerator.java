package at.univie.mma;

import at.pegasos.generator.Parameters;
import at.pegasos.generator.builder.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.*;
import at.pegasos.generator.parser.serverparser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.util.*;
import at.pegasos.util.*;
import com.beust.jcommander.*;

import org.eclipse.jgit.api.errors.*;
import org.gradle.tooling.*;
import org.slf4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.Map.*;

import javax.xml.parsers.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.univie.mma.pegasos.util.*;

public class MMAGenerator {
  private final static Logger log= LoggerFactory.getLogger(MMAGenerator.class);

  private final Map<String, List<Application>> repos_instances;
  private final Map<String,ServerInstanceBuilder> instances_serverbuilders;
  private final List<Application> instances;
  private Map<String, Long> times;
  private final Map<String, Long> times_instances;
  private final Map<String, Application> libraries;

  ServerInstanceParser server_instances_parser;

  private final List<Path> dir_input;
  private final List<Path> dir_server_input;
  private final Path server_build_dir;
  private final Path apps_build_dir;

  private Path builds;

  private final at.pegasos.generator.Parameters parameters;

  /**
   * Collection of all directories containing apps we created during this run
   */
  private final List<Path> tmp_dirs_apps;

  /**
   * Collection of all directories containing apps we created during this run
   */
  private final List<Path> tmp_dirs_servers;

  /**
   * Get the path of the ServerInterface for a server
   * @param serverName name of the server
   * @return path to ServerInterface.jar
   */
  public String getServerInterfaceHandle(String serverName)
  {
    return this.parameters.apps_only ?
      builds.resolve("server-" + serverName).resolve("ServerInterface.jar").toAbsolutePath().toString() :
      instances_serverbuilders.get(serverName).getServerInterface().toAbsolutePath().toString();
  }

  public at.pegasos.generator.Parameters getParameters()
  {
    return parameters;
  }

  public Path getBuildsDir()
  {
    return builds;
  }

  public MMAGenerator(at.pegasos.generator.Parameters p)
  {
    p.check();
    this.parameters= p;
    at.pegasos.generator.Parameters.set(p);

    this.dir_input= new ArrayList<Path>(p.inputdir_apps_string.size());
    this.dir_server_input= new ArrayList<Path>(p.inputdir_apps_string.size());

    for(String path : p.inputdir_apps_string)
      this.dir_input.add(Paths.get(path).normalize().toAbsolutePath());
    for(String path : p.inputdir_servers_string)
      this.dir_server_input.add(Paths.get(path).normalize().toAbsolutePath());

    this.server_build_dir= Paths.get(p.server_out_dir).normalize().toAbsolutePath();
    this.apps_build_dir= Paths.get(p.apps_out_dir).normalize().toAbsolutePath();

    log.debug(dir_input.toString());
    log.debug(dir_server_input.toString());

    instances= new ArrayList<Application>();
    repos_instances= new HashMap<String, List<Application>>();
    times_instances= new HashMap<String,Long>();
    instances_serverbuilders= new HashMap<String,ServerInstanceBuilder>();

    libraries= new HashMap<String, Application>();

    this.tmp_dirs_apps= new ArrayList<Path>();
    this.tmp_dirs_servers= new ArrayList<Path>();
  }

  /**
   * Check whether a servers directory is present in the input directories as specified by the parameters
   * @return true if a server exists in the directories
   */
  private boolean checkDirectory()
  {
    boolean servers_found= false;

    log.debug("Checking if all files are present");

    if( !parameters.ignore_app_servers )
    {
      for(Path input_dir : dir_input)
      {
        Path servers= input_dir.resolve("servers");

        log.debug("servers dir present: " + servers.toFile().exists());

        servers_found|= servers.toFile().exists();
      }
    }

    // Assume that if we have specified more than one server-input directory that we have a server
    servers_found|= dir_server_input.size() > 0;

    return servers_found;
  }

  /**
   * Identify all Versions of apps and servers
   * @throws ParserException when parsing fails
   */
  private void identifyVersions() throws ParserException
  {
    server_instances_parser= new ServerInstanceParser(parameters);

    // First parse Libraries
    for(Path input_dir : dir_input)
    {
      LibraryHelper.parseInputDir(input_dir, libraries);
    }

    // Parsing apps
    for(Path input_dir : dir_input)
    {
      /*
       * This is a bit of a nasty workaround. If the directory does not exist we only warn but do
       * not fail
       */
      if( !Files.exists(input_dir) )
      {
        log.error("Parsing apps-directory: {} does not exist --> ignored", input_dir);
        continue;
      }
	    try( DirectoryStream<Path> directoryStream = Files.newDirectoryStream(input_dir) )
	    {
	      for (Path path : directoryStream)
	      {
	        // Check whether it is a directory. Skip config
	        if( path.toFile().isDirectory() && !path.getFileName().toString().equals("servers") )
	        {
	          // check whether it contains all required files
	          log.info("Examining {}", path.toAbsolutePath());
	          if( Files.exists(Paths.get(path.toAbsolutePath().toString(),"application.xml")) )
	          {
	            Instance i= parseBasicinfo(Paths.get(path.toAbsolutePath().toString(),"application.xml"));
	            if( i == null )
	            {
	              log.info("No Application");
	            }
	            else
	            {
	              i.directory= path.toAbsolutePath().normalize();

                Application app= new Application(i);
                ApplicationParser p= new ApplicationParser(app, false);
                p.setLibraries(libraries);
                p.parse();
                app= p.getApplication();
                // System.out.println("Libraries: " + app.getLibraries());
                log.info("Application repository: {} branch:{}", app.getGitRoot(parameters), app.getGitBranch(parameters));
                log.debug("Servers: {}", app.servers);

	              instances.add(app);
	              if( repos_instances.get(i.getGitRoot(parameters)) != null )
	                repos_instances.get(i.getGitRoot(parameters)).add(app);
	              else
	              {
	                List<Application> x= new ArrayList<Application>();
	                x.add(app);
	                repos_instances.put(i.getGitRoot(parameters), x);
	              }

	              long lastMod= LastModification.lastModified(i.directory);
	              times_instances.put(path.getFileName().toString(), lastMod);
	              log.warn("Last Mod: " + lastMod);
	            }
	          }
	        }
	      }
	    }
	    catch (IOException ex)
	    {
	      ex.printStackTrace();
	      log.error("IOException during examining existing apps", ex);
	      throw new ParserException(ex.getMessage());
	    }

      // If app-servers are not ignored add all servers in this directory
      if( !parameters.ignore_app_servers && Files.exists(input_dir.resolve("servers")) )
        server_instances_parser.parse(input_dir.resolve("servers"));
    }

    // Now add all servers
    for(Path input_dir : dir_server_input)
    {
      server_instances_parser.parse(input_dir);
    }
  }

  /**
   * Load an instance from an XML file
   *
   * @param input
   *          path to the input file
   * @return Instance containing the name
   * @throws ParserException when parsing fails
   */
  public static Instance parseBasicinfo(Path input) throws ParserException
  {
    Instance inst= new Instance();
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder;

    try
    {
      dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(input.toFile());
      doc.getDocumentElement().normalize();

      inst.name= XMLHelper.getStringValueOfChild(doc.getDocumentElement(), "name");
    }
    catch (ParserConfigurationException | SAXException | IOException e)
    {
      e.printStackTrace();
      log.error("XML Error while parsing '" + input.toString() + "': " + e.getMessage());
      throw new ParserException(e.getMessage());
    }

    return inst;
  }

  private void loadLastBuildTimes()
  {
    times= new HashMap<String, Long>();
    try
    {
      List<String> lines= Files.readAllLines(Paths.get(parameters.lastbuilt));
      for(String line : lines)
      {
        String[] x= line.split(";");
        if( x.length == 2 )
        {
          log.debug("Last built: " + x[0] + "  :  " + x[1]);
          times.put(x[0], Long.parseLong(x[1]));
        }
      }
    }
    catch (IOException e) {
      log.warn("Error reading 'lastbuilt.txt'");
    }
  }

  private void saveLastBuildTimes()
  {
    try (BufferedWriter out = new BufferedWriter(
        Files.newBufferedWriter(Paths.get("lastbuilt.txt"), java.nio.file.StandardOpenOption.CREATE)))
    {
      // out.write(data, 0, data.length);
      for(Entry<String, Long> e : times.entrySet())
      {
        // log.info(e.getKey());
        out.write(e.getKey() + ";" + e.getValue() + "\n");
      }
    }
    catch (IOException x)
    {
      log.error(x.getMessage());
    }
  }

  /**
   * Create the ServerInstanceBuilders for the declared servers
   * @throws GeneratorException when something fails ...
   */
  private void setupServers() throws GeneratorException
  {
    for(ServerInstance inst : server_instances_parser.getInstances())
    {
      ServerInstanceBuilder builder;
      try
      {
        builder = new ServerInstanceBuilder(inst, server_build_dir, builds, libraries, parameters);
        instances_serverbuilders.put(inst.name, builder);
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new GeneratorException(inst, e);
      }
    }
  }

  private void prepareServerBuilds() throws ParserException, GeneratorException
  {
    List<Long> timesx= new ArrayList<Long>(instances.size());

    for(ServerInstanceBuilder builder : instances_serverbuilders.values())
    {
      log.debug("Preparing " + builder.getServername() + " build " + parameters.apps_only);
      for(Instance inst : instances)
      {
        for(String server_name : inst.servers)
        {
          if( server_name.equals(builder.getServername()) )
          {
            timesx.add(times_instances.get(inst.directory.getFileName().toString()));
            break;
          }
        }
      }

      if( !parameters.apps_only )
      {
        try
        {
          Long x= times.get(serverKey(builder.getServername()));
          boolean res= builder.buildRequired(timesx, x != null ? x : 0);

          log.debug("Build required ({})?: {} {} {}", builder.getServername(), res, timesx, x);
          if( res )
          {
            builder.setOmitWeb(parameters.omitweb);
            builder.prepareBuild();
          }
          else
          {
            FileHelper.deleteDirectorySafe(builder.getBuildDir(), log);
          }
        }
        catch( IOException | GitAPIException e )
        {
          e.printStackTrace();
          throw new GeneratorException(builder.getInstance(), e);
        }
      }
      else
      {
        builder.setBuildRequired(false);
      }
    }
  }

  public void run() throws ParserException, GeneratorException
  {
    GitHelper git;

    builds= Paths.get(".").resolve("builds");
    try
    {
      Files.createDirectories(builds);
      Files.createDirectories(apps_build_dir);
      Files.createDirectories(server_build_dir);
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new GeneratorException(e);
    }

    if( !checkDirectory() )
    {
      log.error("Missing required files. Exiting");
      return;
    }

    loadLastBuildTimes();

    identifyVersions();

    if( !parameters.apps_only )
    {
      setupServers();
      checkConfigurations();
      prepareServerBuilds();
      connectInstancesAndServers();
      buildServerInstances();
    }
    else
    {
      log.info("Not building servers due to --apps-only");
    }

    if( !parameters.servers_only )
    {
      // Loop over all repositories and build all the instances for the repository
      for(Entry<String, List<Application>> entries : repos_instances.entrySet())
      {
        List<Application> instances= entries.getValue();

        // Prepare the directory where the building will take place
        Path dir;

        // Loop over all instances using this repository
        for(Application inst : instances)
        {
          try
          {
            log.info("Starting build for {}: Repo={}, branch= {}", inst.name, inst.getGitRoot(parameters), inst.getGitBranch(parameters));
            dir = Files.createTempDirectory(apps_build_dir, inst.name);

            // Put files into the directory
            git= new GitHelper(inst, dir, parameters);

            int last_modified= git.lastChange(inst.getGitBranch(parameters));

            if( buildRequired(inst, last_modified) )
            {
              this.tmp_dirs_apps.add(dir);

              log.info("Preparing Repo: " + inst.getGitRoot(parameters));
              git.initAsClone();
              git.prepareBranch(inst.getGitBranch(parameters));

              ToolsBuilder toolsBuilder = new ToolsBuilder(dir, this);

              AppInstanceBuilder appinstbuild = new AppInstanceBuilder(inst, dir, server_instances_parser, builds, git, libraries,
                  this, toolsBuilder);
              appinstbuild.build();

              // When build finished. Save the build time
              String fn= inst.directory.getFileName().toString();
              String key= instKey(inst);
              times.put(key, times_instances.get(fn));
              key= gitKey(inst);
              times.put(key, (long) last_modified);

              saveLastBuildTimes();
            }
            else
            {
              log.info("Building " + inst.directory.getFileName().toString() + " for " + inst.getGitRoot(parameters) + ":" + inst.getGitBranch(parameters) + " not required");
              FileHelper.deleteDirectorySafe(dir, log);
            }
          }
          catch( GeneratorException | IOException | InterruptedException | GitAPIException e )
          {
            e.printStackTrace();
            throw new GeneratorException(inst, e);
          }
        }
      }
    }

    clean();
  }

  private void buildServerInstances() throws ParserException, GeneratorException
  {
    for(ServerInstanceBuilder b : instances_serverbuilders.values())
    {
      try
      {
        b.setOmitWeb(parameters.omitweb);
        if( b.build() )
        {
          this.tmp_dirs_servers.add(b.getBuildDir());
          b.saveBuilds();
          times.put(serverKey(b.getServername()), System.currentTimeMillis());
        }
      }
      catch( GeneratorException | IOException | InterruptedException | BuildException e )
      {
        log.error("Building {} failed", b.getServername());
        e.printStackTrace();
        throw new GeneratorException(b.getInstance(), e);
      }
    }
  }

  private void checkConfigurations() throws GeneratorException
  {
    Collection<ServerInstance> servers= server_instances_parser.getInstances();
    List<String> server_names= new ArrayList<String>(servers.size());

    // fill in server names
    for(ServerInstance server : servers)
    {
      server_names.add(server.name);
    }

    // Check whether all servers in the application configs exist and every application mentions a server
    for(Instance inst : instances)
    {
      if( inst.servers.size() == 0 )
      {
        throw new GeneratorException("Every instance needs at least one Server! Missing server info for " + inst.name);
      }

      for(String server_name : inst.servers)
      {
        if( !server_names.contains(server_name) )
          throw new GeneratorException("Unknown server '" + server_name + "' in " + inst.name);
      }
    }
  }

  /**
   * Connect Instances and Servers. 
   * This also involves extending server using the apps (ie copy app/server --> server) 
   * @throws GeneratorException when adding failed
   */
  private void connectInstancesAndServers() throws GeneratorException
  {
    log.info("Adding apps to the servers");
    for(Application inst : instances)
    {
      log.debug("Adding {} to {}", inst.name, inst.servers);
      for(String server_name : inst.servers)
      {
        try
        {
          instances_serverbuilders.get(server_name).addAppInstance(inst, libraries);
        }
        catch (IOException | ParserException e)
        {
          e.printStackTrace();
          throw new GeneratorException("Failed adding instance " + inst.name + " to " + server_name + ": " + e.getMessage());
        }
      }
    }
  }

  /**
   * Whether a instance needs to be built
   * @param inst
   * @param git_last_modified
   * @return
   */
  private boolean buildRequired(Instance inst, int git_last_modified)
  {
    if( parameters.apps_always )
    {
      log.info("Building app has been forced by --apps-always");
      return true; 
    }

    String fn= inst.directory.getFileName().toString();

    // Was this instance built before
    long mod= times_instances.get(fn);
    String key= instKey(inst);
    if( times.containsKey(key) )
    { // Have there been changes in the instance
      if( times.get(key) < mod )
      {
        log.info("Instance has been modified: building");
        return true;
      }
      else
      {
        key= gitKey(inst);
        // have there been changes on this branch
        if( times.containsKey(key) )
        {
          if( times.get(key) < git_last_modified )
          {
            log.info("Repo has been modified: building");
            return true;
          }
          else
          {
            log.info("Repo has not been modified");
            // TODO: check whether build exists in "builds/"
            return false;
          }
        }
        else
        {
          log.info("Repo has not been built before for this app");
          return true;
        }
      }
    }
    else
    {
      log.info("Instance has not been built before: building");
      return true;
    }
  }

  private void clean()
  {
    if( parameters.clean || parameters.clean_tmp_apps )
    {
      this.tmp_dirs_apps.forEach(dir -> {
        try
        {
          FileHelper.deleteDirectory(dir);
        }
        catch(IOException e)
        {
          log.error("Could not remove temporary app " + dir);
        }
      });
    }
    if( parameters.clean || parameters.clean_tmp_servers )
    {
      log.info("Cleaning servers");
      this.tmp_dirs_servers.forEach(dir -> {
        try
        {
          log.debug("Deleting " + dir);
          FileHelper.deleteDirectory(dir);
        }
        catch(IOException e)
        {
          log.error("Could not remove temporary server " + dir);
        }
      });
    }
  }

  private static String instKey(Instance inst)
  {
    return "inst:" + inst.directory.toString().hashCode();
  }

  private static String serverKey(String name)
  {
    return "server:" + name.hashCode();
  }

  private String gitKey(Instance inst)
  {
    return "git:" + inst.getGitRoot(parameters).hashCode() + "-" + inst.getGitBranch(parameters).hashCode() + "-" + inst.name.hashCode();
  }

  private long classBuildTimeMillis() throws URISyntaxException, IllegalStateException, IllegalArgumentException
  {
    URL resource = getClass().getResource(getClass().getSimpleName() + ".class");
    if( resource == null )
    {
      throw new IllegalStateException("Failed to find class file for class: " +
              getClass().getName());
    }

    if( resource.getProtocol().equals("file") )
    {
      return new File(resource.toURI()).lastModified();
    }
    else if( resource.getProtocol().equals("jar") )
    {
      String path = resource.getPath();
      return new File(path.substring(5, path.indexOf("!"))).lastModified();
    }
    else
    {
      throw new IllegalArgumentException("Unhandled url protocol: " +
              resource.getProtocol() + " for class: " +
              getClass().getName() + " resource: " + resource.toString());
    }
  }

  public static void main(String[] args) throws IOException, GitAPIException, ParserException, GeneratorException, InterruptedException, URISyntaxException
  {
    Map<String, String> env = System.getenv();
    if( env.get("JAVA_HOME") == null )
    {
      log.error("JAVA_HOME not set. Please use export or something similar");
      throw new GeneratorException("JAVA_HOME not set");
    }

    log.info("Testing javac");
    boolean javac= true;
    try
    {
      Process p= Runtime.getRuntime().exec("javac -version");
      BufferedReader line_in = new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line_out;
      while((line_out= line_in.readLine()) != null)
        log.debug(line_out);
      p.waitFor();
      log.debug("Exit-code (javac -version): {}", p.exitValue());
    }
    catch(IOException e) {
      log.error("IOException when testing for javac: '{}'",e.getMessage());
      javac= false;
    }

    if( javac )
    {
      at.pegasos.generator.Parameters params = new Parameters();

      try
      {
        JCommander.newBuilder()
            .addObject(params)
            .build()
            .parse(args);
      }
      catch(com.beust.jcommander.ParameterException e)
      {
        log.error("Parsing arguments ({}) failed", Arrays.toString(args), e);
        throw e;
      }

      if( params.help )
      {
        JCommander.newBuilder()
            .addObject(params)
            .build()
            .usage();
        return;
      }

      log.debug("Running with parameters: {}", params);

      MMAGenerator generator = new MMAGenerator(params);
      log.info("Running generator compiled at {}", new Date(generator.classBuildTimeMillis()));
      generator.run();
    }
  }
}
