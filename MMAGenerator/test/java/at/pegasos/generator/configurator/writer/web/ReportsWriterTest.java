package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import org.junit.*;
import org.junit.rules.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.nio.file.*;

import static at.pegasos.generator.parser.webparser.parsers.ReportsParserTest.*;
import static org.junit.Assert.*;

public class ReportsWriterTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void test() throws IOException, ParserException, ParserConfigurationException, TransformerException
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    TestData(inst.getWeb());

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("web");
    document.appendChild(root);

    new ReportsWriter(document, root, inst).write();

    // create the xml file
    // transform the DOM Object to an XML File
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    transformerFactory.setAttribute("indent-number", 4);
    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    DOMSource domSource = new DOMSource(document);

    StringWriter outWriter = new StringWriter();
    StreamResult streamResult = new StreamResult( outWriter );

    transformer.transform(domSource, streamResult);

    StringBuffer sb = outWriter.getBuffer();
    String generated = sb.toString();

    assertEquals(ReportsParserTest.data, generated);
  }

  @Test public void test_empty() throws IOException, ParserException, ParserConfigurationException, TransformerException
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("web");
    document.appendChild(root);

    new ReportsWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/configurator/writer/web/emptyweb.xml"), document);
  }
}