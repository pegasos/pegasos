package at.pegasos.generator.configurator.writer.app;

import at.pegasos.generator.configurator.writer.*;
import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import java.io.*;
import java.nio.file.*;

public class StartupActionWriterTest {

  @Test public void test_empty() throws IOException, ParserConfigurationException, TransformerException, WriterException
  {
    Instance i= new Instance();
    i.directory= Paths.get("./");
    Application inst= new Application(i);

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("application");
    document.appendChild(root);

    new StartupActionWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/configurator/writer/app/emptyapp.xml"), document);
  }

  @Test public void test_simple() throws IOException, ParserConfigurationException, TransformerException, WriterException
  {
    Instance i= new Instance();
    i.directory= Paths.get("./");
    Application inst = new Application(i);
    inst.addStartupAction("some.action");

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("application");
    document.appendChild(root);

    new StartupActionWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/configurator/writer/app/startup-someaction.xml"), document);
  }

  @Test public void libsNotWritten()
      throws IOException, ParserConfigurationException, TransformerException, WriterException, ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("./");
    Application library = new Application(i);

    library.addStartupAction("some.action");

    Instance i2= new Instance();
    i2.directory= Paths.get("./");
    Application inst= new Application(i2);
    inst.add(library);

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("application");
    document.appendChild(root);

    new StartupActionWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/configurator/writer/app/emptyapp.xml"), document);
  }
}