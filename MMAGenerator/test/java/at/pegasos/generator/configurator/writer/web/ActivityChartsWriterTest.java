package at.pegasos.generator.configurator.writer.web;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;
import org.junit.rules.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import java.io.*;
import java.nio.file.*;

import static at.pegasos.generator.parser.webparser.parsers.ActivityChartsParserTest.*;

public class ActivityChartsWriterTest {
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void test() throws IOException, ParserException, ParserConfigurationException, TransformerException
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    TestData(inst.getWeb());

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("web");
    document.appendChild(root);

    new ActivityChartsWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/parser/webparser/parsers/activitychartstest.xml"), document);
  }

  @Test
  public void test_empty() throws IOException, ParserConfigurationException, TransformerException
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    // root element
    Element root = document.createElement("web");
    document.appendChild(root);

    new ChartsWriter(document, root, inst).write();

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/configurator/writer/web/emptyweb.xml"), document);
  }
}