package at.pegasos.generator.generator.server.web.backend;

import org.junit.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;

public class ReportEnhancerFactoryGeneratorTest {

  @Test
  public void test()
  {
    Web web = new Web();

    ReportsParserTest.TestData(web);

    ReportEnhancerFactoryGenerator gen= new ReportEnhancerFactoryGenerator(web);

    String expected = "package at.pegasos.server.frontback.reports;\n" +
      "\n" +
      "\n" +
      "import lombok.extern.slf4j.Slf4j;\n" +
      "import org.springframework.stereotype.Service;\n" +
      "import org.springframework.core.io.ClassPathResource;\n" +
      "import java.io.*;\n" +
      "import org.rosuda.REngine.REngine;\n" +
      "import org.springframework.beans.factory.annotation.Autowired;\n" +
      "import java.util.stream.Collectors;\n" +
      "\n" +
      "@Service\n" +
      "@Slf4j\n" +
      "public class ReportEnhancerFactory {\n" +
      "  @Autowired(required=false) private REngine engine;\n" +
      "  public ReportEnhancer getEnhancer(String name)\n" +
      "  {\n" +
      "    if( name.equals(\"mmp\") )\n" +
      "    {\n" +
      "      RReportEnhancer enhancer= new RReportEnhancer();\n" +
      "      enhancer.engine= engine;\n" +
      "      \n" +
      "      try\n" +
      "      {\n" +
      "        InputStream in= new ClassPathResource(\"omidata.R\").getInputStream();\n" +
      "        try( BufferedReader reader= new BufferedReader(new InputStreamReader(in)) )\n" +
      "        {\n" +
      "          String code= reader.lines().collect(Collectors.joining(\"\\n\"));\n" +
      "          log.debug(\"Running: {}\", code);\n" +
      "          enhancer.code= new String[] {code};\n" +
      "        }\n" +
      "        return enhancer;\n" +
      "      }\n" +
      "      catch( IOException e )\n" +
      "      {\n" +
      "        log.error(e.getMessage());\n" +
      "        e.printStackTrace();\n" +
      "      }\n" +
      "    }\n" +
      "    return null;\n" +
      "  }\n" +
      "  \n" +
      "}\n" +
      "// End of generated class\n\n";

    OutputHelper.testEquals(expected, gen);
  }
}