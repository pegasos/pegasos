package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;

public class TableDescriptionsGeneratorTest {
  @Test
  public void empty_works()
  {
    ServerInstance server = new ServerInstance();

    TableDescriptionsGenerator gen = new TableDescriptionsGenerator(server);

    String expected = "package at.pegasos.server.frontback.dbdata;\n" +
            "\n" +
            "\n" +
            "public class TableDescriptions {\n" +
            "  public static TableDescription[] tables = new TableDescription[] {\n" +
            "  };\n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test public void simple_test() throws ParserException {
    Sensor sensor = new Sensor(13, "Bike", "bike", "generate", "~/pegasos-apps/servers/testing/sensors/bikenr.txt");
    ServerInstance server = new ServerInstance();
    server.getSensors().add(sensor);
    Field[] fields = new Field[] {
            new Field()
    };
    fields[0].name = "hr";
    sensor.setFields(fields);

    TableDescriptionsGenerator gen = new TableDescriptionsGenerator(server);

    String expected = "package at.pegasos.server.frontback.dbdata;\n" +
            "\n" +
            "\n" +
            "public class TableDescriptions {\n" +
            "  public static TableDescription[] tables = new TableDescription[] {\n" +
            "    new TableDescription(\"bike\", \"hr\"),\n" +
            "  };\n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test public void db_name_is_respected() throws ParserException {
    Sensor sensor = new Sensor(13, "Gps", "gps", "generate", "~/pegasos-apps/servers/testing/sensors/gps.txt");
    ServerInstance server = new ServerInstance();
    server.getSensors().add(sensor);
    Field[] fields = new Field[] {
            new Field()
    };
    fields[0].name = "gps_lat";
    fields[0].getOptions().addOption("dbname:lat");
    sensor.setFields(fields);

    TableDescriptionsGenerator gen = new TableDescriptionsGenerator(server);

    String expected = "package at.pegasos.server.frontback.dbdata;\n" +
            "\n" +
            "\n" +
            "public class TableDescriptions {\n" +
            "  public static TableDescription[] tables = new TableDescription[] {\n" +
            "    new TableDescription(\"gps\", \"lat\"),\n" +
            "  };\n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }
}