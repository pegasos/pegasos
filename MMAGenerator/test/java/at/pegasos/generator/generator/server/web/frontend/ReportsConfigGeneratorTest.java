package at.pegasos.generator.generator.server.web.frontend;

import org.junit.*;

import java.io.*;
import java.nio.charset.*;
import java.util.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ReportsConfigGeneratorTest {

  @Test @SuppressWarnings("unchecked")
  public void report_without_display_has_noconfig() throws GeneratorException
  {
    Web web= new Web();
    web.set("reports", new ArrayList<Report>());

    MetricBasedAccumulatedReport r = new MetricBasedAccumulatedReport();
    r.name = "mmp";
    r.type = "Param";
    r.metrics = new String[] {"MMP", "MMP2k"};
    r.groupBy = MetricBasedAccumulatedReport.GroupBy.ALL;
    r.aggregate = MetricBasedAccumulatedReport.Aggregate.MAX;
    r.enhancer = new REnhancer();
    ((REnhancer) r.enhancer).code_resource = "omidata.R";
    ((Collection<Report>) web.get("reports")).add(r);

    ReportsConfigGenerator gen= new ReportsConfigGenerator(web);
    String expected = "// Folder: shared;\n" +
            "// Generated file. Do not edit\n" +
            "\n" +
            "\n" +
            "import  * as transform from './datatransformation';\n" +
            "import { ReportInfo, ReportConfig, MultiLineReportConfig, BarsReportConfig, CurvedLineReportConfig, SessionbasedLineReportConfig } from './reportconfig';\n" +
            "\n" +
            "export const REPORTS: ReportInfo[] = [];\n" +
            "\n" +
            "\n" +
            "// End of generated class\n\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test
  public void test() throws GeneratorException
  {
    Web web= new Web();

    ReportsParserTest.TestData(web);

    ReportsConfigGenerator gen= new ReportsConfigGenerator(web);
    OutputHelper.testEquals("// Folder: shared;\n" +
            "// Generated file. Do not edit\n" +
            "\n" +
            "\n" +
            "import  * as transform from './datatransformation';\n" +
            "import { ReportInfo, ReportConfig, MultiLineReportConfig, BarsReportConfig, CurvedLineReportConfig, SessionbasedLineReportConfig } from './reportconfig';\n" +
            "\n" +
            "const c0 = new MultiLineReportConfig('test');\n" +
            "const c1 = new SessionbasedLineReportConfig('lsct');\n" +
            "const c2 = new BarsReportConfig('energyweek');\n" +
            "const c3 = new BarsReportConfig('timeinz');\n" +
            "const c4 = new MultiLineReportConfig('pmc');\n" +
            "export const REPORTS: ReportInfo[] = [new ReportInfo('test', 'test', 'Multiline', 52, c0),\n" +
            "  new ReportInfo('lsct', 'lsct', 'SessionbasedLines', 16, c1),\n" +
            "  new ReportInfo('energyweek', 'energy_week', 'Bars', 4, c2),\n" +
            "  new ReportInfo('timeinz', 'timeinz', 'Bars', 4, c3),\n" +
            "  new ReportInfo('pmc', 'pmc', 'Multiline', 52, c4)];\n" +
            "\n" +"\n" +
            "c1.yaxislabel = 'lsct_stage_power';\n" +
            "c1.toolTip = '`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`';\n" +
            "c1.colors = ['steelblue', 'red', 'black', 'green', 'orange'];\n" +
            "c2.xAxisScale = 'Week';\n" +
            "c3.xAxisScale = 'Week';\n" +
            "c3.yaxislabel = 'Time in Zone [h:m:s]';\n" +
            "c3.toolTip = '`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`';\n" +
            "// End of generated class\n" +
            "\n", gen);
  }
}