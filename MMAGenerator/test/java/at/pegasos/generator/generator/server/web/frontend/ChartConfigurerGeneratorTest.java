package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import org.junit.*;

import java.io.*;
import java.nio.charset.*;

import static org.junit.Assert.*;

public class ChartConfigurerGeneratorTest {

  @Test public void test() throws ParserException
  {
    Charts charts= new Charts();
    charts.addChart(new MapChart("chart_Map", "gps", "lat", "lon"));
    SingleLineChart chart = new SingleLineChart("chart_HR", "hr", "hr");
    chart.config("yaxislabel", "hr_bpm");
    chart.config("linecolor", "red");
    charts.addChart(chart);
    charts.addChart(new MultiLineChart("chart_BikeP", "bike", "field", new String[]{"sensor_nr", "power"}));
    charts.addChart(new SingleLineChart("chart_BikeC", "bike", "cadence"));

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps = new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      ChartConfigurerGenerator gen = new ChartConfigurerGenerator(charts);

      gen.setOutput(ps);
      gen.generate();

      String output = baos.toString(Charset.defaultCharset());
      String expected = "// Folder: charts;\n" +
              "// Generated file. Do not edit\n" +
              "\n" +
              "\n" +
              "import { LineChartConfig } from './../shared/chartconfig';\n" +
              "import { ChartConfig } from './../shared/chartconfig';\n" +
              "import { MultiLineChartConfig } from './../shared/chartconfig';\n" +
              "import { ChartInfo } from './../shared/sample';\n" +
              "\n" +
              "export class ChartConfigurer {\n" +
              "  public static getChartConfig(info: ChartInfo): ChartConfig {\n" +
              "    switch( info.name )\n" +
              "    {\n" +
              "      case 'chart_HR':\n" +
              "        const c0= new LineChartConfig(info);\n" +
              "        c0.yaxislabel= 'hr_bpm';\n" +
              "        c0.linecolor= 'red';\n" +
              "        return c0;\n" +
              "      case 'chart_BikeP':\n" +
              "        const c1= new MultiLineChartConfig(info);\n" +
              "        return c1;\n" +
              "      case 'chart_BikeC':\n" +
              "        const c2= new LineChartConfig(info);\n" +
              "        return c2;\n" +
              "    }\n" +
              "    \n" +
              "    return new ChartConfig(info);\n" +
              "  }\n" +
              "  \n" +
              "}\n" +
              "// End of generated class\n" +
              "\n";
      assertEquals("Generated not equals", expected, output);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
}