package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

import java.io.*;
import java.nio.charset.*;
import java.util.*;

import static org.junit.Assert.*;

public class RoutesGeneratorTest {

  @Test public void test()
  {
    Web web= new Web();

    web.addModule("ReportsModule", "reports.module", "reports");
    web.addModule("AdminModule", "admin.module", "admin");

    web.addRoute(new ComponentRoute("home", "HomeComponent"));
    web.addRoute(new ComponentRoute("login", "LoginComponent"));
    web.addRoute(new ComponentRoute("logout", "LoginComponent"));
    web.addRoute(new ComponentRoute("register", "LoginComponent"));
    web.addRoute(new ComponentRoute("finishregistration", "LoginComponent"));
    web.addRoute(new ComponentRoute("team", "TeamComponent"));
    web.addRoute(new ComponentRoute("tagebuch", "ActivityListComponent"));
    web.addRoute(new ComponentRoute("actd/:id", "ActivityDetailComponent"));
    web.addRoute(new ModuleRoute("reports", "ReportsModule"));
    web.addRoute(new ComponentRoute("profile", "UserComponent"));
    web.addRoute(new ComponentRoute("finishmailchange", "UserComponent"));
    web.addRoute(new ComponentRoute("live", "LiveComponent"));
    web.addRoute(new ComponentRoute("values/:vid", "UserValueComponent"));
    web.addRoute(new ComponentRoute("values/:uid/:vid", "UserValueComponent"));

    web.addRoute(new ModuleRoute("admin", "AdminModule", List.of("MenuGuard")));

    // Add a demo third party route
    web.addComponent("OAuthComponent", "oauth.component", "oauth", true, true);
    web.addRoute(new ComponentRoute("oauth2_callback", "OAuthComponent"));
    web.addRoute(new ComponentRoute("oauth2_callback/:partnerid", "OAuthComponent"));

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      RoutesGenerator gen= new RoutesGenerator(web);

      gen.setOutput(ps);
      gen.generate();

      String output= baos.toString(Charset.defaultCharset());
      String expected = "// Folder: app-routing;\n" +
              "// Generated file. Do not edit\n" +
              "\n" +
              "\n" +
              "import { Routes } from '@angular/router';\n" +
              "import { UserComponent } from '../user/user.component';\n" +
              "import { OAuthComponent } from '../oauth/oauth.component';\n" +
              "import { ActivityListComponent } from '../activities/activity-list.component';\n" +
              "import { ActivityDetailComponent } from '../activities/activity-detail.component';\n" +
              "import { TeamComponent } from '../team/team.component';\n" +
              "import { LiveComponent } from '../live/live.component';\n" +
              "import { MenuGuard } from './menu.guard';\n" +
              "import { LoginComponent } from '../login/login.component';\n" +
              "import { HomeComponent } from '../home/home.component';\n" +
              "import { UserValueComponent } from '../user/user.value.component';\n" +
              "\n" +
              "export const routes: Routes = [\n" +
              "  { path: 'home', component: HomeComponent },\n" +
              "  { path: 'login', component: LoginComponent },\n" +
              "  { path: 'logout', component: LoginComponent },\n" +
              "  { path: 'register', component: LoginComponent },\n" +
              "  { path: 'finishregistration', component: LoginComponent },\n" +
              "  { path: 'team', component: TeamComponent },\n" +
              "  { path: 'tagebuch', component: ActivityListComponent },\n" +
              "  { path: 'actd/:id', component: ActivityDetailComponent },\n" +
              "  { path: 'reports', loadChildren: () => import('../reports/reports.module').then(m => m.ReportsModule) },\n" +
              "  { path: 'profile', component: UserComponent },\n" +
              "  { path: 'finishmailchange', component: UserComponent },\n" +
              "  { path: 'live', component: LiveComponent },\n" +
              "  { path: 'values/:vid', component: UserValueComponent },\n" +
              "  { path: 'values/:uid/:vid', component: UserValueComponent },\n" +
              "  { path: 'admin', loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule), canActivate: [MenuGuard] },\n" +
              "  { path: 'oauth2_callback', component: OAuthComponent },\n" +
              "  { path: 'oauth2_callback/:partnerid', component: OAuthComponent },\n" +
              "\n" +
              "  { path: '', redirectTo: '/home', pathMatch: 'full' }\n" +
              "];\n" +
              "\n" +
              "// End of generated class\n" +
              "\n";
      assertEquals("Generated not equals", expected, output);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
}