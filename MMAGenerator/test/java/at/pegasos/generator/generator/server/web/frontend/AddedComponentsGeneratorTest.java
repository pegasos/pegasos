package at.pegasos.generator.generator.server.web.frontend;

import org.junit.*;

import java.io.*;
import java.nio.charset.*;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;

import static org.junit.Assert.*;

public class AddedComponentsGeneratorTest {

  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void testSimple()
  {
    Web web= new Web();

    // Add OAuthComponent as test
    web.addComponent("OAuthComponent", "oauth.component", "oauth", true, true);

    AddedComponentsGenerator gen= new AddedComponentsGenerator(web.components);

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();

      String data= baos.toString(Charset.defaultCharset());
      String x= "// Folder: ;\n" +
              "// Generated file. Do not edit\n" +
              "\n" +
              "\n" +
              "import { OAuthComponent } from './oauth/oauth.component';\n" +
              "\n" +
              "export const DECLARATIONS = [\n" +
              "  OAuthComponent,\n" +
              "];\n" +
              "export const ENTRYCOMPONENTS = [\n" +
              "  OAuthComponent,\n" +
              "];\n" +
              "\n" +
              "// End of generated class\n\n";
//      System.out.println(x);
      assertEquals("Generated not equals", x, data);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
}
