package at.pegasos.generator.generator.server;

import static org.junit.Assert.*;

import java.io.*;
import java.nio.charset.Charset;

import at.pegasos.generator.parser.*;
import org.junit.*;

import at.pegasos.generator.generator.GeneratorException;
import at.pegasos.generator.servergenerator.SensorConfigParser;
import at.pegasos.generator.generator.server.backend.ServerParser;;
import at.pegasos.generator.parser.serverparser.token.Sensor;

public class TestServerParser {
  
  @Test
  public void testSimple() throws ParserException, IOException
  {
    Sensor s= new Sensor("13\tNotRelevant\tNotRelevant\tgenerate\ta-non-existing-file.txt".split("\t"));
    String str= "length: fixed\n" + 
        "# For now we do not want any session. = statements\n" + 
        "field-is-session-name: false\n" + 
        "sampling-type: sample-on-set\n" + 
        "----------------\n" + 
        "fields:\n" + 
        "type\tint\tint\n" + 
        "----------------\n" + 
        "\n" + 
        "";
    SensorConfigParser parser= new SensorConfigParser(
        new BufferedReader(new StringReader(str)), s);
    parser.parse();
    
    ServerParser gen= new ServerParser(s, "db_table", "name", true);
    
    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();
      String data= baos.toString(Charset.defaultCharset());
      String x= "package at.univie.mma.server.parser;\n" + 
          "\n" + 
          "\n" + 
          "import java.sql.SQLException;\n" + 
          "import database.Db_connector;\n" + 
          "import java.io.IOException;\n" + 
          "import java.sql.PreparedStatement;\n" + 
          "import at.univie.mma.server.data.Session;\n" + 
          "\n" + 
          "public class name implements ISensorDataParser {\n" + 
          "\n" + 
          "  private int time_last_measurement;\n" + 
          "  private long time_since_start_ret;\n" + 
          "  private Session data;\n" + 
          "  private Db_connector db;\n" + 
          "  private int bc_ret;\n" + 
          "  private PreparedStatement db_insert;\n" + 
          "  private int inserts;\n" + 
          "  \n" + 
          "  public name(Session data, Db_connector db) throws SQLException\n" + 
          "  {\n" + 
          "    this.data= data;\n" + 
          "    this.db= db;\n" + 
          "    perpareStatement();\n" + 
          "  }\n" + 
          "  \n" + 
          "  private void perpareStatement() throws SQLException\n" + 
          "  {\n" + 
          "    this.db_insert= this.db.createStatement(\"insert into db_table(session_id,rec_time,type) values (?,?,?)\");\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,\n" + 
          "    int datapackages, long time_since_start) throws IOException, SQLException\n" + 
          "  {\n" + 
          "    int type= 0; // field: 0\n" + 
          "    \n" + 
          "    inserts= 0;\n" + 
          "    \n" + 
          "    if( datapackages > 0 )\n" + 
          "      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;\n" + 
          "    \n" + 
          "    for (int x=0;x<datapackages;x++)\n" + 
          "    {\n" + 
          "      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!\n" + 
          "      time_since_start+=time_last_measurement;\n" + 
          "      \n" + 
          "      db_insert.setLong(1, data.session_id);\n" + 
          "      db_insert.setLong(2, data.starttime+time_since_start);\n" + 
          "      type= ((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;\n" + 
          "      db_insert.setInt(3,type);\n" + 
          "      if (data.insert_data==1)\n" + 
          "      {\n" + 
          "        db_insert.addBatch();\n" + 
          "        inserts++;\n" + 
          "      }\n" + 
          "      System.out.println(\"name: type:\"+type+ \" \");\n" + 
          "      bc= bc + 4;\n" + 
          "    }\n" + 
          "    \n" + 
          "    if( inserts>0 )\n" + 
          "    {\n" + 
          "      db_insert.executeBatch();\n" + 
          "    }\n" + 
          "    \n" + 
          "    bc_ret= bc;\n" + 
          "    time_since_start_ret= time_since_start;\n" + 
          "    \n" + 
          "    return null;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public int getNewPosition()\n" + 
          "  {\n" + 
          "    return bc_ret;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public long getNewTimeSinceStart()\n" + 
          "  {\n" + 
          "    return time_since_start_ret;\n" + 
          "  }\n" + 
          "}\n";
      assertEquals("Generated not equals", x, data);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
  
  @Test
  public void testText() throws ParserException, IOException
  {
    Sensor s= new Sensor("13\tNotRelevant\tNotRelevant\tgenerate\ta-non-existing-file.txt".split("\t"));
    String str= "length: fixed\n" + 
        "# For now we do not want any session. = statements\n" + 
        "field-is-session-name: false\n" + 
        "sampling-type: sample-on-set\n" + 
        "----------------\n" + 
        "fields:\n" + 
        "field\tstring\tstring\tmaxlength:45\n" + 
        "----------------\n" + 
        "\n" + 
        "";
    SensorConfigParser parser= new SensorConfigParser(
        new BufferedReader(new StringReader(str)), s);
    parser.parse();
    
    ServerParser gen= new ServerParser(s, "db_table", "name", true);
    
    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();
      String data= baos.toString(Charset.defaultCharset());
      String x= "package at.univie.mma.server.parser;\n" + 
          "\n" + 
          "\n" + 
          "import java.sql.SQLException;\n" + 
          "import database.Db_connector;\n" + 
          "import java.nio.charset.Charset;\n" + 
          "import java.io.IOException;\n" + 
          "import java.sql.PreparedStatement;\n" + 
          "import at.univie.mma.server.data.Session;\n" + 
          "\n" + 
          "public class name implements ISensorDataParser {\n" + 
          "\n" + 
          "  private int time_last_measurement;\n" + 
          "  private long time_since_start_ret;\n" + 
          "  private Session data;\n" + 
          "  private Db_connector db;\n" + 
          "  private int bc_ret;\n" + 
          "  private PreparedStatement db_insert;\n" + 
          "  private int inserts;\n" + 
          "  \n" + 
          "  private final static Charset cs= java.nio.charset.Charset.forName(\"UTF-8\");\n" + 
          "  public name(Session data, Db_connector db) throws SQLException\n" + 
          "  {\n" + 
          "    this.data= data;\n" + 
          "    this.db= db;\n" + 
          "    perpareStatement();\n" + 
          "  }\n" + 
          "  \n" + 
          "  private void perpareStatement() throws SQLException\n" + 
          "  {\n" + 
          "    this.db_insert= this.db.createStatement(\"insert into db_table(session_id,rec_time,field) values (?,?,?)\");\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,\n" + 
          "    int datapackages, long time_since_start) throws IOException, SQLException\n" + 
          "  {\n" + 
          "    String field= \"\"; // field: 0\n" + 
          "    int lfield= 0;\n" + 
          "    byte[] bfield= null;\n" + 
          "    \n" + 
          "    inserts= 0;\n" + 
          "    \n" + 
          "    if( datapackages > 0 )\n" + 
          "      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;\n" + 
          "    \n" + 
          "    for (int x=0;x<datapackages;x++)\n" + 
          "    {\n" + 
          "      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!\n" + 
          "      time_since_start+=time_last_measurement;\n" + 
          "      \n" + 
          "      db_insert.setLong(1, data.session_id);\n" + 
          "      db_insert.setLong(2, data.starttime+time_since_start);\n" + 
          "      {lfield= in[bc+3];bfield= new byte[lfield];System.arraycopy(in, bc+4, bfield, 0, lfield);field= new String(bfield, cs);}\n" + 
          "      db_insert.setString(3,field);\n" + 
          "      if (data.insert_data==1)\n" + 
          "      {\n" + 
          "        db_insert.addBatch();\n" + 
          "        inserts++;\n" + 
          "      }\n" + 
          "      System.out.println(\"name: field:\"+field+ \" \");\n" + 
          "      bc= bc + 48;\n" + 
          "    }\n" + 
          "    \n" + 
          "    if( inserts>0 )\n" + 
          "    {\n" + 
          "      db_insert.executeBatch();\n" + 
          "    }\n" + 
          "    \n" + 
          "    bc_ret= bc;\n" + 
          "    time_since_start_ret= time_since_start;\n" + 
          "    \n" + 
          "    return null;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public int getNewPosition()\n" + 
          "  {\n" + 
          "    return bc_ret;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public long getNewTimeSinceStart()\n" + 
          "  {\n" + 
          "    return time_since_start_ret;\n" + 
          "  }\n" + 
          "}\n";
      assertEquals("Generated not equals", x, data);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
  
  @Test
  public void testSessionInsert() throws ParserException, IOException
  {
    Sensor s= new Sensor("13\tNotRelevant\tNotRelevant\tgenerate\ta-non-existing-file.txt".split("\t"));
    String str= "length: fixed\n" + 
        "field-is-session-name: true\n" + 
        "sampling-type: sample-on-set\n" + 
        "----------------\n" + 
        "fields:\n" + 
        "type\tint\tint\n" + 
        "----------------\n" + 
        "session data database:\n" +
        "1\ttype\n" +
        "----------------\n" + 
        "session data code:\n" +
        "2\t1 + 2 + 3 + 4\n" +
        "----------------\n" + 
        "\n" + 
        "";
    SensorConfigParser parser= new SensorConfigParser(
        new BufferedReader(new StringReader(str)), s);
    parser.parse();
    
    ServerParser gen= new ServerParser(s, "db_table", "name", true);
    
    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();
      String data= baos.toString(Charset.defaultCharset());
      String x= "package at.univie.mma.server.parser;\n" + 
          "\n" + 
          "\n" + 
          "import java.sql.SQLException;\n" + 
          "import database.Db_connector;\n" + 
          "import java.io.IOException;\n" + 
          "import java.sql.PreparedStatement;\n" + 
          "import at.univie.mma.server.data.Session;\n" + 
          "\n" + 
          "public class name implements ISensorDataParser {\n" + 
          "\n" + 
          "  private int time_last_measurement;\n" + 
          "  private long time_since_start_ret;\n" + 
          "  private Session data;\n" + 
          "  private Db_connector db;\n" + 
          "  private int bc_ret;\n" + 
          "  private PreparedStatement db_insert;\n" + 
          "  private int inserts;\n" + 
          "  \n" + 
          "  public name(Session data, Db_connector db) throws SQLException\n" + 
          "  {\n" + 
          "    this.data= data;\n" + 
          "    this.db= db;\n" + 
          "    perpareStatement();\n" + 
          "  }\n" + 
          "  \n" + 
          "  private void perpareStatement() throws SQLException\n" + 
          "  {\n" + 
          "    this.db_insert= this.db.createStatement(\"insert into db_table(session_id,rec_time,type) values (?,?,?)\");\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,\n" + 
          "    int datapackages, long time_since_start) throws IOException, SQLException\n" + 
          "  {\n" + 
          "    int type= 0; // field: 0\n" + 
          "    \n" + 
          "    inserts= 0;\n" + 
          "    \n" + 
          "    if( datapackages > 0 )\n" + 
          "      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;\n" + 
          "    \n" + 
          "    for (int x=0;x<datapackages;x++)\n" + 
          "    {\n" + 
          "      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!\n" + 
          "      time_since_start+=time_last_measurement;\n" + 
          "      \n" + 
          "      db_insert.setLong(1, data.session_id);\n" + 
          "      db_insert.setLong(2, data.starttime+time_since_start);\n" + 
          "      type= ((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;\n" + 
          "      db_insert.setInt(3,type);\n" + 
          "      if (data.insert_data==1)\n" + 
          "      {\n" + 
          "        db_insert.addBatch();\n" + 
          "        inserts++;\n" + 
          "      }\n" + 
          "      System.out.println(\"name: type:\"+type+ \" \");\n" + 
          "      bc= bc + 4;\n" + 
          "    }\n" + 
          "    \n" + 
          "    if( inserts>0 )\n" + 
          "    {\n" + 
          "      db_insert.executeBatch();\n" + 
          "    }\n" + 
          "    \n" + 
          "    data.v[1]= type;\n" +
          "    data.v[2]= 1 + 2 + 3 + 4;\n" +
          "    data.type= type;\n" +
          "    \n" +
          "    bc_ret= bc;\n" + 
          "    time_since_start_ret= time_since_start;\n" + 
          "    \n" + 
          "    return null;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public int getNewPosition()\n" + 
          "  {\n" + 
          "    return bc_ret;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public long getNewTimeSinceStart()\n" + 
          "  {\n" + 
          "    return time_since_start_ret;\n" + 
          "  }\n" + 
          "}\n";
      assertEquals("Generated not equals", x, data);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }
}
