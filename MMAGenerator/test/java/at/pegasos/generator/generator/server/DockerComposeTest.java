package at.pegasos.generator.generator.server;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;
import org.junit.rules.*;
import org.w3c.dom.ls.*;
import org.yaml.snakeyaml.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import static org.junit.Assert.*;

public class DockerComposeTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void test() throws GeneratorException, IOException, ParserException
  {
    Path path = folder.getRoot().toPath();

    ServerInstance instance = new ServerInstance();
    instance.name = "production";
    instance.config = new Config();
    instance.config.setValue("db.name", "pegasos_test");
    instance.port = 64222;
    instance.getWeb().config.setValue("backend.port", "8081");
    instance.directory = path;

    Path outFile = path.resolve("server").resolve("docker").resolve("docker-compose.yml");
    Files.createDirectories(outFile.getParent());

    DockerCompose gen = new DockerCompose(instance);
    gen.setOutputToPrintStream(path.resolve("docker-compose.yml"));

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/docker-compose.yml"), gen);
  }
}
