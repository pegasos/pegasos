package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

public class ActivityNamesGeneratorTest {
  @Test
  public void testSimple() throws ParserException, GeneratorException {
    ActivityNames names3 = new ActivityNames();

    names3.addName(9, 5, 2, "Orthostatic");
    names3.addName(9, 5, 1, "HRV");
    names3.addName(9, 5, "Test HRV");
    names3.addName(9, 6, "LSCT");
    names3.addName(9, "Test");
    names3.addName(1, "Training");

    Web web = new Web();
    web.activitynames = names3;

    ActivityNamesGenerator gen = new ActivityNamesGenerator(web);
    String expected = "package at.pegasos.server.frontback;\n" +
            "\n" +
            "\n" +
            "public class ActivityNames {\n" +
            "  public static String getActivityName(int param0, int param1, int param2)\n" +
            "  {\n" +
            "    switch( param0 )\n" +
            "    {\n" +
            "      case 1:\n" +
            "        return \"Training\";\n" +
            "      case 9:\n" +
            "        switch( param1 )\n" +
            "        {\n" +
            "          case 5:\n" +
            "            switch( param2 ) \n" +
            "            {\n" +
            "              case 1:\n" +
            "                return \"HRV\";\n" +
            "              case 2:\n" +
            "                return \"Orthostatic\";\n" +
            "              default:\n" +
            "                break;\n" +
            "            }\n" +
            "            return \"Test HRV\";\n" +
            "          case 6:\n" +
            "            return \"LSCT\";\n" +
            "          default:\n" +
            "            break;\n" +
            "        }\n" +
            "        return \"Test\";\n" +
            "      default:\n" +
            "        break;\n" +
            "    }\n" +
            "    return \"\";\n" +
            "  }\n" +
            "  \n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test
  public void test_output_sorted() throws ParserException, GeneratorException {
    ActivityNames names3 = new ActivityNames();

    names3.addName(9, 5, 1, "HRV");
    names3.addName(9, 6, "LSCT");
    names3.addName(9, 5, 2, "Orthostatic");
    names3.addName(9, 5, "Test HRV");
    names3.addName(9, "Test");
    names3.addName(1, "Training");
    names3.addName(3, "Test");

    Web web = new Web();
    web.activitynames = names3;

    ActivityNamesGenerator gen = new ActivityNamesGenerator(web);
    String expected = "package at.pegasos.server.frontback;\n" +
            "\n" +
            "\n" +
            "public class ActivityNames {\n" +
            "  public static String getActivityName(int param0, int param1, int param2)\n" +
            "  {\n" +
            "    switch( param0 )\n" +
            "    {\n" +
            "      case 1:\n" +
            "        return \"Training\";\n" +
            "      case 3:\n" +
            "        return \"Test\";\n" +
            "      case 9:\n" +
            "        switch( param1 )\n" +
            "        {\n" +
            "          case 5:\n" +
            "            switch( param2 ) \n" +
            "            {\n" +
            "              case 1:\n" +
            "                return \"HRV\";\n" +
            "              case 2:\n" +
            "                return \"Orthostatic\";\n" +
            "              default:\n" +
            "                break;\n" +
            "            }\n" +
            "            return \"Test HRV\";\n" +
            "          case 6:\n" +
            "            return \"LSCT\";\n" +
            "          default:\n" +
            "            break;\n" +
            "        }\n" +
            "        return \"Test\";\n" +
            "      default:\n" +
            "        break;\n" +
            "    }\n" +
            "    return \"\";\n" +
            "  }\n" +
            "  \n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }
}