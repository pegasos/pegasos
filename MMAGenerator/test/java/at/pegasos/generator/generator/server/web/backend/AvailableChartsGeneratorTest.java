package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import org.junit.*;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.*;

public class AvailableChartsGeneratorTest {

  @Test
  public void generate() throws ParserException
  {
    Web web = new Web();
    ActivityChartsParserTest.TestData(web);
    AvailableChartsGenerator gen = new AvailableChartsGenerator(web);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/AvailableChartsTestData.java"), gen);
  }

  @Test
  public void generateEmpty()
  {
    Charts charts= new Charts();

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps = new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      Web web = new Web();
      web.charts = charts;
      AvailableChartsGenerator gen = new AvailableChartsGenerator(web);

      gen.setOutput(ps);
      gen.forSession();

      String output = baos.toString(Charset.defaultCharset());
      String expected = "public static List<AvailableChart> ForSession(int param0, int param1, int param2)\n" +
              "{\n" +
              "  return defaultCharts;\n" +
              "}\n" +
              "\n";
      assertEquals("Generated not equals", expected, output);
    }
  }

  @Test public void orderIsRespected() throws ParserException {
    Charts charts= new Charts();

    Activity node = charts.getAvailNode("1", "", "");
    node.adddefault = true;

    node.chartsPre.add(new Activity.Chart("chart_C", 3, true));
    node.chartsPre.add(new Activity.Chart("chart_A", 1, true));
    node.chartsPre.add(new Activity.Chart("chart_B", 2, true));
    node.chartsPost.add(new Activity.Chart("chart_P", 99, true));

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps = new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      Web web = new Web();
      web.charts = charts;
      AvailableChartsGenerator gen = new AvailableChartsGenerator(web);

      gen.setOutput(ps);
      gen.staticInitializer();

      String output = baos.toString(Charset.defaultCharset());
      String expected = "static {\n" +
              "  defaultCharts= new ArrayList<AvailableChart>(0);\n" +
              "  \n" +
              "  availableCharts= new ArrayList<AvailableChart>(0);\n" +
              "  \n" +
              "  ret1= new ArrayList<AvailableChart>(10);\n" +
              "  ret1.add(chart_A);\n" +
              "  ret1.add(chart_B);\n" +
              "  ret1.add(chart_C);\n" +
              "  ret1.addAll(defaultCharts);\n" +
              "  ret1.add(chart_P);\n" +
              "}\n";
      assertEquals("Generated not equals", expected, output);
    }
  }

  @Test public void generateParserTestData() throws ParserException
  {
    Web web = new Web();
    ChartsParserTest.TestData(web);

    AvailableChartsGenerator gen = new AvailableChartsGenerator(web);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/ChartsTests.java"), gen);
  }
}