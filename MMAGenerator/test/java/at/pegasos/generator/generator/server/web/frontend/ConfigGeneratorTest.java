package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import org.junit.*;

import java.nio.file.*;

public class ConfigGeneratorTest {

  @Test public void empty_works()
  {
    ConfigGenerator gen = new ConfigGenerator("", new Config());
    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/frontend/ConfigEmpty.ts"), gen);
  }

  @Test public void test_simple() throws ParserException
  {
    Config config = new Config();
    config.setValueString("aString", "test");
    config.setValueBool("aBoolT", "true");
    config.setValueBool("aBoolF", "false");
    config.setValue("someting", "1");
    ConfigGenerator gen = new ConfigGenerator("", config);
    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/frontend/Config.ts"), gen);
  }
}