package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

public class ReportGeneratorFactoryTest {
  @Test
  public void testdata_generates_expected_code()
  {
    Web web= new Web();

    ReportsParserTest.TestData(web);

    ReportGeneratorFactory gen= new ReportGeneratorFactory(web);

    String expected = "package at.pegasos.server.frontback.reports;\n" +
            "\n" +
            "\n"
        + "import at.pegasos.server.frontback.repositories.*;\n"
        + "import at.pegasos.server.frontback.data.*;\n"
        + "import at.pegasos.server.frontback.services.*;\n"
        + "import at.pegasos.server.frontback.controller.ReportController.*;\n"
        + "import java.util.*;\n" + "import org.springframework.stereotype.*;\n"
        + "import org.springframework.beans.factory.annotation.*;\n"
        + "import org.springframework.jdbc.core.*;\n" +
            "\n" +
            "@Service\n" +
            "public class ReportGeneratorFactory {\n" +
            "  @Autowired private UserValueService valueservice;\n" +
            "  @Autowired private MetricInfoRepository metrirep;\n" +
            "  @Autowired private MetricRepository metrrep;\n" +
            "  @Autowired private SessionInfoRepository sirepo;\n" +
            "  @Autowired private RepositoryService repositoryService;\n" +
            "  @Autowired private JdbcTemplate jdbcTemplate;\n" +
            "  public ReportGenerator getReport(String name)\n" +
            "  {\n" +
            "    ReportGenerator gen;\n" +
            "    switch( name )\n" +
            "    {\n" +
            "      case \"test\":\n" +
            "        gen= new UserValueBasedReportGenerator(valueservice, new String[] {\"WEIGHTx\", \"BODYFATx\"});\n" +
            "        break;\n" +
            "      case \"lsct\":\n" +
            "        gen= new MetricBasedReportGenerator(metrirep, metrrep, sirepo, new String[] {\"LSCT_POWER1\", \"LSCT_POWER2\", \"LSCT_POWER3\"}, Collections.singletonList(new SessionTypeFilter(9,6)));\n" +
            "        break;\n" +
            "      case \"energyweek\":\n" +
            "        gen= new MetricBasedAccumulatedReportGeneratorSimple(metrirep, metrrep, sirepo, new String[] {\"ENERGY\"});\n" +
            "        ((MetricBasedAccumulatedReportGeneratorSimple) gen).setAggregate(Aggregate.SUM);\n" +
            "        ((MetricBasedAccumulatedReportGeneratorSimple) gen).setGroupBy(GroupBy.WEEK);\n" +
            "        break;\n" +
            "      case \"timeinz\":\n" +
            "        gen= new MetricBasedAccumulatedReportGeneratorParam(metrirep, metrrep, sirepo, new String[] {\"TIME_IN_POWER_ZONE\"});\n" +
            "        ((MetricBasedAccumulatedReportGeneratorParam) gen).setAggregate(Aggregate.SUM);\n" +
            "        ((MetricBasedAccumulatedReportGeneratorParam) gen).setGroupBy(GroupBy.WEEK);\n" +
            "        break;\n" +
            "      case \"pmc\":\n" +
            "        gen= new UserValueBasedReportGenerator(valueservice, new String[] {\"CTL\", \"ATL\", \"TSB\", \"TSS\"});\n" +
            "        break;\n" +
            "      case \"mmp\":\n" +
            "        gen= new MetricBasedAccumulatedReportGeneratorParam(metrirep, metrrep, sirepo, new String[] {\"MMP\", \"MMP2k\"});\n" +
            "        ((MetricBasedAccumulatedReportGeneratorParam) gen).setAggregate(Aggregate.MAX);\n" +
            "        ((MetricBasedAccumulatedReportGeneratorParam) gen).setGroupBy(GroupBy.ALL);\n" +
            "        break;\n" +
            "      case \"keb\":\n" +
            "        gen= new SessionValueReportGenerator(jdbcTemplate, sirepo, new SessionDataFilter[] {\n" +
            "          new SessionDataFilter(\"questionnaire_answers\", \"answer\", \"Körperliche Leistungsfähigkeit\", \"question = 1\")\n" +
            "        }, Collections.singletonList(new SessionTypeFilter(9,7,1)));\n" +
            "        break;\n" +
            "      case \"l60\":\n" +
            "        {\n" + "          LapFilters lapQuery= new LapFilters();\n"
        + "          lapQuery.conj= Conjunction.AND;\n" + "          lapQuery.filters= new ArrayList<>(2);\n"
        + "          lapQuery.filters.add(new LapFilter(LapFilterType.Duration, 55000, at.pegasos.server.frontback.data.Comparator.GT));\n"
        + "          lapQuery.filters.add(new LapFilter(LapFilterType.Duration, 65000, at.pegasos.server.frontback.data.Comparator.LT));\n"
        + "          gen= new MetricBasedReportGeneratorGrouped(repositoryService, new String[] {\"AVG_POWER\"}, null, lapQuery);\n"
        + "        }\n" + "        ((MetricBasedReportGeneratorGrouped) gen).setGroupBy(GroupBy.DAY);\n" + "        break;\n"
        + "      default:\n" +
            "        throw new IllegalArgumentException(\"Unknown Report \" + name);\n" +
            "    }\n" +
            "    return gen;\n" +
            "  }\n" +
            "  \n" +
            "  public void setDefaultDate(String name, ReportGenerator generator)\n" +
            "  {\n" +
            "    Calendar start;\n" +
            "    start= Calendar.getInstance();\n" +
            "    start.setTimeInMillis(System.currentTimeMillis());\n" +
            "    switch( name )\n" +
            "    {\n" +
            "      case \"test\":\n" +
            "        start.add(Calendar.WEEK_OF_YEAR, -52);\n" + "        break;\n" +
            "      case \"lsct\":\n" +
            "        start.add(Calendar.WEEK_OF_YEAR, -16);\n" +
            "        break;\n" +
            "      case \"energyweek\":\n" +
            "        start.add(Calendar.WEEK_OF_YEAR, -4);\n" + "        break;\n" +
            "      case \"timeinz\":\n" + "        start.add(Calendar.WEEK_OF_YEAR, -4);\n" + "        break;\n" +
            "      case \"pmc\":\n" + "        start.add(Calendar.WEEK_OF_YEAR, -52);\n" + "        break;\n" + "      case \"mmp\":\n"
        + "        start.add(Calendar.YEAR, -1);\n" + "        break;\n" + "      case \"keb\":\n"
        + "        start.add(Calendar.YEAR, -1);\n" + "        break;\n" + "      case \"l60\":\n"
        + "        start.add(Calendar.YEAR, -1);\n" + "        break;\n" + "      default:\n"
        + "        throw new IllegalArgumentException(\"Unknown Report \" + name);\n" + "    }\n" + "    generator.setStart(start);\n" +
            "  }\n" +
            "  \n" +
            "}\n" +
            "// End of generated class\n" +
            "\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test
  public void empty_reports_works()
  {
    Web web= new Web();

    ReportGeneratorFactory gen= new ReportGeneratorFactory(web);

    String expected = "package at.pegasos.server.frontback.reports;\n" + "\n" + "\n"
        + "import at.pegasos.server.frontback.repositories.*;\n"
        + "import at.pegasos.server.frontback.data.*;\n"
        + "import at.pegasos.server.frontback.services.*;\n"
        + "import at.pegasos.server.frontback.controller.ReportController.*;\n"
        + "import java.util.*;\n" + "import org.springframework.stereotype.*;\n"
        + "import org.springframework.beans.factory.annotation.*;\n"
        + "import org.springframework.jdbc.core.*;\n"
        + "\n"
        + "@Service\n"
        + "public class ReportGeneratorFactory {\n"
        + "  @Autowired private UserValueService valueservice;\n"
        + "  @Autowired private MetricInfoRepository metrirep;\n"
        + "  @Autowired private MetricRepository metrrep;\n"
        + "  @Autowired private SessionInfoRepository sirepo;\n"
        + "  @Autowired private RepositoryService repositoryService;\n"
        + "  @Autowired private JdbcTemplate jdbcTemplate;\n"
        + "  public ReportGenerator getReport(String name)\n"
        + "  {\n"
        + "    return null;\n"
        + "  }\n"
        + "  \n"
        + "  public void setDefaultDate(String name, ReportGenerator generator)\n"
        + "  {\n"
        + "  }\n"
        + "  \n"
        + "}\n"
        + "// End of generated class\n"
        + "\n";

    OutputHelper.testEquals(expected, gen);
  }
}