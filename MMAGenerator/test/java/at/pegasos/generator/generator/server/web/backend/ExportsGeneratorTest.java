package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

import java.nio.file.*;

public class ExportsGeneratorTest {
  @Test
  public void empty_works() {
    Web web = new Web();

    ExportsGenerator gen = new ExportsGenerator(web);
    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/EmptyExports.java"), gen);
  }

  @Test
  public void test() throws ParserException {
    Exports exports = new Exports();

    exports.addType(new Exports.ExportType("csv", "CsvExport"));
    exports.addType(new Exports.ExportType("test", "TestExport"));
    exports.getAvailNode("1", "", "").exports.add(new Exports.Export("csv"));
    exports.getAvailNode("1", "1", "").exports.add(new Exports.Export("test"));
    exports.getAvailNode("2", "", "").exports.add(new Exports.Export("test"));

    Web web = new Web();
    web.exports = exports;

    ExportsGenerator gen = new ExportsGenerator(web);
    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/Exports.java"), gen);
  }
}