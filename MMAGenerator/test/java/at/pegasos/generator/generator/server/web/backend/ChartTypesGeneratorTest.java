package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

import java.nio.file.*;

public class ChartTypesGeneratorTest {

  @Test public void testDataWorks() throws ParserException
  {
    Web web = new Web();
    ChartsParserTest.TestData(web);
    ChartTypesGenerator gen = new ChartTypesGenerator(web);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/ChartsTestsTypes.java"), gen);
  }

  @Test public void empty_works() throws ParserException
  {
    Web web = new Web();
    ChartTypesGenerator gen = new ChartTypesGenerator(web);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/server/web/backend/ChartsTestsTypes2.java"), gen);
  }
}