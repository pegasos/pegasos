package at.pegasos.generator.generator.server.web.frontend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.parsers.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

import java.nio.file.*;
import java.util.*;

public class MenuGeneratorTest {
  @Test
  public void defaultsGenerateCorrectCode()
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = Paths.get("/");
    new MenuParser(inst, true);
    MenuGenerator gen= new MenuGenerator(inst.getWeb().menu);
    String expected = "// Folder: shared;\n" +
            "// Generated file. Do not edit\n" +
            "\n" +
            "\n" +
            "import { Menu } from './menu';\n" +
            "\n" +
            "export const MENUS: Menu[] = [\n" +
            "  new Menu('user/profile', 'mainMenu_personalData', false),\n" +
            "  new Menu('team', 'mainMenu_team', false),\n" +
            "  new Menu('live', 'mainMenu_currentEvents', false),\n" +
            "  new Menu('tagebuch', 'mainMenu_diary', false),\n" +
            "  new Menu('reports', 'mainMenu_reports', false),\n" +
            "  new Menu('hilfe', 'mainMenu_help', true),\n" +
            "  new Menu('impressum', 'mainMenu_imprint', true),\n" +
            "  new Menu('admin', 'mainMenu_admin', false, ['ADMIN']),\n" +
            "];\n" +
            "\n" +
            "// End of generated class\n\n";

    OutputHelper.testEquals(expected, gen);
  }

  @Test
  public void generateParentCorrect() throws ParserException
  {
    ServerInstance inst = new ServerInstance();
    inst.directory = Paths.get("/");
    new MenuParser(inst, true);
    MenuEntry test = new MenuEntry("pwrprf", "mainMenu_PowerProfile");
    test.isPublic = false;
    test.requiredRoles = new ArrayList<>(List.of(new String[]{"PowerProfile"}));
    inst.getWeb().addMenuEntry("mainMenu_reports", test);

    MenuGenerator gen= new MenuGenerator(inst.getWeb().menu);
    String expected = "// Folder: shared;\n" +
            "// Generated file. Do not edit\n" +
            "\n" +
            "\n" +
            "import { Menu } from './menu';\n" +
            "\n" +
            "export const MENUS: Menu[] = [\n" +
            "  new Menu('user/profile', 'mainMenu_personalData', false),\n" +
            "  new Menu('team', 'mainMenu_team', false),\n" +
            "  new Menu('live', 'mainMenu_currentEvents', false),\n" +
            "  new Menu('tagebuch', 'mainMenu_diary', false),\n" +
            "  new Menu('reports', 'mainMenu_reports', false, [], [\n" +
            "    new Menu('pwrprf', 'mainMenu_PowerProfile', false, ['PowerProfile']),\n" +
            "  ]),\n" +
            "  new Menu('hilfe', 'mainMenu_help', true),\n" +
            "  new Menu('impressum', 'mainMenu_imprint', true),\n" +
            "  new Menu('admin', 'mainMenu_admin', false, ['ADMIN']),\n" +
            "];\n" +
            "\n" +
            "// End of generated class\n\n";

    OutputHelper.testEquals(expected, gen);
  }
}