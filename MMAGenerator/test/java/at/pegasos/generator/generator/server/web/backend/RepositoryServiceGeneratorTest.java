package at.pegasos.generator.generator.server.web.backend;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

public class RepositoryServiceGeneratorTest {

  @Test public void default_works()
  {
    String expected =
        "package at.pegasos.server.frontback.services;\n" + "\n" + "\n" +
        "import at.pegasos.server.frontback.repositories.*;\n" +
        "import org.springframework.stereotype.*;\n" +
        "import lombok.*;\n" +
        "import org.springframework.beans.factory.annotation.*;\n" + "\n" +
        "@Service\n" + "@Getter\n" +
        "public class RepositoryService {\n" +
        "  @Autowired private GroupRepository groupRepository;\n" +
        "}\n" +
        "// End of generated class\n\n";

    Web web = new Web();
    Repositories repositories = new Repositories();
    repositories.repositories.add(new Repositories.Repository("GroupRepository"));
    web.ensurePresent(Repositories.TAG, Repositories.class);
    web.set(Repositories.TAG, repositories);
    RepositoryServiceGenerator gen = new RepositoryServiceGenerator(web);

    OutputHelper.testEquals(expected, gen);
  }

}