package at.pegasos.generator.generator.client;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;

import java.nio.file.*;
import java.util.*;

public class ActionGeneratorTest {

  @Test public void test_simple() throws ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("./");
    Application inst = new Application(i);
    inst.addStartupAction("some.action");
    ActionGenerator gen = new ActionGenerator("at.pegasos.client", "StartupActions", inst.startup_actions);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/client/ActionGeneratorSimpleTest.java"), gen);
  }

  @Test public void empty_works() throws ParserException
  {
    ActionGenerator gen = new ActionGenerator("at.pegasos.client", "StartupActions", new ArrayList<OwnedString>());

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/client/ActionGeneratorEmpty.java"), gen);
  }
}