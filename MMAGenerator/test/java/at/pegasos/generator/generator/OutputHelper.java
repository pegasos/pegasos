package at.pegasos.generator.generator;

import org.w3c.dom.*;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.*;

public class OutputHelper {

  public static final Path resDir = Paths.get("test/res");

  public static void testEquals(String expected, CodeGenerator gen)
  {
    testEquals("Generated code does not match specification", expected, gen);
  }

  public static void testEquals(String message, String expected, CodeGenerator gen)
  {
    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();

      String data= baos.toString(Charset.defaultCharset());
      assertEquals(message, expected, data);
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }

  public static void testEquals(Path file, CodeGenerator gen)
  {
    testEquals("Generated code does not match specification", file, gen);
  }

  public static void testEquals(String message, Path file, CodeGenerator gen)
  {
    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, Charset.defaultCharset()) )
    {
      gen.setOutput(ps);
      gen.generate();

      String data= baos.toString(Charset.defaultCharset());

      List<String> expectedList = Files.readAllLines(resDir.resolve(file));
      String expected = String.join("\n", expectedList);

      assertEquals(message, expected, data);
    }
    catch( GeneratorException | IOException e )
    {
      e.printStackTrace();
      fail();
    }
  }

  public static void testEquals(Path file, Document document) throws IOException, TransformerException
  {
    testEquals("Generated file does not match specification", file, document);
  }

  public static void testEquals(String message, Path file, Document document) throws TransformerException, IOException
  {
    // create the xml file
    // transform the DOM Object to an XML File
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    transformerFactory.setAttribute("indent-number", 4);
    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    DOMSource domSource = new DOMSource(document);

    StringWriter outWriter = new StringWriter();
    StreamResult streamResult = new StreamResult( outWriter );

    transformer.transform(domSource, streamResult);

    StringBuffer sb = outWriter.getBuffer();
    String generated = sb.toString();

    List<String> expectedList = Files.readAllLines(resDir.resolve(file));
    String expected = String.join("\n", expectedList);

    assertEquals(message, expected, generated);
  }
}
