package at.pegasos.generator.generator;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import org.junit.*;

public class TestConfigGenerator {

  @Test
  public void testEmpty()
  {
    ConfigGenerator gen= new ConfigGenerator("at.test", new Config());


      String x= "package at.test;\n" + 
          "\n" + 
          "\n" + 
          "public class Config {\n" + 
          "}\n" +
          "// End of generated class\n" +
          "\n";

    OutputHelper.testEquals(x, gen);
  }

  @Test
  public void testValueInt() throws ParserException
  {
    Config c= new Config();
    c.setValue("test", 1);
    ConfigGenerator gen= new ConfigGenerator("at.test", c);

      String x= "package at.test;\n" +
          "\n" +
          "\n" +
          "public class Config {\n" +
          "  public final static int TEST = 1;\n" +
          "}\n" +
          "// End of generated class\n" +
          "\n";

    OutputHelper.testEquals(x, gen);
  }
  
  @Test
  public void testValueIntFromString() throws ParserException
  {
    Config c= new Config();
    c.setValue("test", "1");
    ConfigGenerator gen= new ConfigGenerator("at.test", c);

      String x= "package at.test;\n" +
          "\n" +
          "\n" +
          "public class Config {\n" +
          "  public final static int TEST = 1;\n" +
          "}\n" +
          "// End of generated class\n" +
          "\n";

    OutputHelper.testEquals(x, gen);
  }
  
  @Test
  public void testValueString() throws ParserException
  {
    Config c= new Config();
    c.setValueString("test", "test");
    ConfigGenerator gen= new ConfigGenerator("at.test", c);

      String x= "package at.test;\n" +
          "\n" +
          "\n" +
          "public class Config {\n" +
          "  public final static String TEST = \"test\";\n" +
          "}\n" +
          "// End of generated class\n" +
          "\n";

    OutputHelper.testEquals(x, gen);
  }
  
  @Test
  public void testValueBool() throws ParserException
  {
    Config c= new Config();
    c.setValueBool("test", "false");
    ConfigGenerator gen= new ConfigGenerator("at.test", c);

      String x= "package at.test;\n" +
          "\n" +
          "\n" +
          "public class Config {\n" +
          "  public final static boolean TEST = false;\n" +
          "}\n" +
          "// End of generated class\n" +
          "\n";

    OutputHelper.testEquals(x, gen);
  }
}
