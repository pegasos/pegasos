package at.pegasos.generator.generator.serverinterface;

import at.pegasos.generator.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.serverparser.token.*;
import at.pegasos.generator.servergenerator.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;

public class ServerInterfaceSensorTest {

  @Rule public TemporaryFolder folder = new TemporaryFolder();

  @Test public void generate_empty() throws ParserException, IOException
  {
    File conf = folder.newFile("test.txt");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(conf.getAbsolutePath()));
    writer.write("length: fixed\n" + "# For now we do not want any session. = statements\n" + "field-is-session-name: false\n"
        + "sampling-type: sample-on-set\n" + "----------------\n" + "fields:\n" + "----------------\n");
    writer.close();

    Sensor s = new Sensor(new String[] {"13", "Bike", "bike", "generate", "sensors/bike.txt"});
    SensorConfigParser parser = new SensorConfigParser(Paths.get(conf.getAbsolutePath()).toString(), s);
    parser.parse();

    ServerInterfaceSensor gen = new ServerInterfaceSensor(s, Options.SAMPLING_SAMPLE_ON_SET);

    OutputHelper.testEquals(Paths.get("at/pegasos/generator/generator/serverinterface/SimpleSensor.java"), gen);
  }
}