package at.pegasos.generator;

import java.io.*;
import java.nio.file.*;

public class TestHelper {
  public static void copy(Path from, Path to) throws IOException
  {
    Files.copy(Paths.get("test/res").resolve(from), to, StandardCopyOption.REPLACE_EXISTING);
  }
}
