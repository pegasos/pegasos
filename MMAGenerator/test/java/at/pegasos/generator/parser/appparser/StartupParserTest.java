package at.pegasos.generator.parser.appparser;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.*;

public class StartupParserTest {
  @Rule public TemporaryFolder folder = new TemporaryFolder();

  @Test public void test_empty() throws IOException, ParserException
  {
    TestHelper.copy(Paths.get("at/pegasos/generator/configurator/writer/app/emptyapp.xml"), folder.newFile("application.xml").toPath());

    Instance i= new Instance();
    i.directory = folder.getRoot().toPath();
    Application app = new Application(i);

    StartupParser parser = new StartupParser(app, false);
    parser.parse();

    Application exp = new Application(i);
    exp.addStartupAction("some.action");

    assertEquals(0, app.startup_actions.size());
  }

  @Test public void test_simple() throws IOException, ParserException
  {
    TestHelper.copy(Paths.get("at/pegasos/generator/configurator/writer/app/startup-someaction.xml"), folder.newFile("application.xml").toPath());

    Instance i= new Instance();
    i.directory = folder.getRoot().toPath();
    Application app = new Application(i);

    StartupParser parser = new StartupParser(app, false);
    parser.parse();

    Application exp = new Application(i);
    exp.addStartupAction("some.action");

    assertEquals(exp.startup_actions.size(), app.startup_actions.size());

    Iterator<OwnedString> i1 = app.startup_actions.iterator();
    Iterator<OwnedString> i2 = exp.startup_actions.iterator();
    while( i1.hasNext() && i2.hasNext() )
    {
      assertEquals(i2.next().string, i1.next().string);
    }
  }
}