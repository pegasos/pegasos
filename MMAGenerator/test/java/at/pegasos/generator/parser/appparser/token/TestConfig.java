package at.pegasos.generator.parser.appparser.token;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.nio.file.*;

import at.pegasos.generator.parser.*;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

public class TestConfig {
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();
  private Path fileName;
  
  @Before
  public void setUp() throws Exception
  {
    fileName= Paths.get(folder.newFile().getAbsolutePath());
    BufferedWriter writer= Files.newBufferedWriter(fileName);
    writer.write("string.var;string;string\n" + 
        "boolean.var;boolean;true\n" + 
        "int.var;int;1000\n");
    writer.close();
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void read_throwsNoException()
  {
    try
    {
      new Config(fileName);
    }
    catch( ParserException e )
    {
      fail();
    }
  }
  
  @Test
  public void changingValues_throwsNoException() throws ParserException
  {
    Config c= new Config(fileName);
    c.setValue("int.var", 2);
    c.setValue("string.var", "string");
    try
    {
      c.setValueBool("boolean.var", "true");
    }
    catch( ParserException e )
    {
      fail();
    }
  }
  
  @Test
  public void changingValues_throwsException() throws ParserException
  {
    
  }
  
  @Test
  public void getValues() throws ParserException
  {
    Config c= new Config(fileName);
    assertEquals("string", c.getValue("string.var"));
    assertEquals("true", c.getValue("boolean.var"));
    assertEquals("1000", c.getValue("int.var"));
    
    assertEquals(true, c.getValueBool("boolean.var"));
  }
  
  @Test
  public void cant_get_unset_values() throws ParserException
  {
    Config c= new Config(fileName);
    try
    {
      c.getValue("idonotexist");
      fail();
    }
    catch( ParserException e )
    {
      
    }
    try
    {
      new Config().getValue("idonotexist");
      fail();
    }
    catch( ParserException e )
    {
      
    }
  }
}
