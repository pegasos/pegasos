package at.pegasos.generator.parser.aiparser.token;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class TestFeedbackModules {

  @Test
  public void merge_merges()
  {
    FeedbackModules mods1 = new FeedbackModules();
    FeedbackModules mods2 = new FeedbackModules();

    mods1.add(new FeedbackModule(1, "mod1"));
    mods2.add(new FeedbackModule(2, "mod2"));

    mods1.merge(mods2);

    assertEquals(2, mods1.modules.size());
    assertEquals(1, mods2.modules.size());
  }
}