package at.pegasos.generator.parser.aiparser;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.ServerInstance;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class FeedbackModulesParserTest {

  @Rule
  public TemporaryFolder folder= new TemporaryFolder();
  private Path fileName;
  private ServerInstance inst;

  @Before
  public void setUp() throws Exception
  {
    fileName = Paths.get(folder.newFile("ai.xml").getAbsolutePath());

    inst = new ServerInstance();
    inst.directory= folder.getRoot().toPath();
  }

  @Test
  public void missingParam_fails() throws IOException
  {
    BufferedWriter writer = Files.newBufferedWriter(fileName);
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
      "<ai>\n" +
      "    <feedbackmodules>\n" +
      "        <module>mod1</module>\n" +
      "    </feedbackmodules>\n" +
      "</ai>\n");
    writer.close();

    // new FeedbackModulesParser(inst).parse();
    assertThrows(ParserException.class, () -> new FeedbackModulesParser(inst).parse());
  }

  @Test
  public void asimpletest() throws IOException, ParserException
  {
    BufferedWriter writer = Files.newBufferedWriter(fileName);
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
      "<ai>\n" +
      "    <feedbackmodules>\n" +
      "        <module param0=\"1\">mod1</module>\n" +
      "        <module param0=\"2\" param1=\"1\">mod2</module>\n" +
      "        <module param0=\"3\" param1=\"1\" param2=\"2\">mod3</module>\n" +
      "    </feedbackmodules>\n" +
      "</ai>\n");
    writer.close();

    new FeedbackModulesParser(inst).parse();
    assertEquals(3, inst.feedbackmodules.modules.size());
    assertEquals("mod1", inst.feedbackmodules.modules.get(0).clasz);
    assertEquals("mod2", inst.feedbackmodules.modules.get(1).clasz);
    assertEquals("mod3", inst.feedbackmodules.modules.get(2).clasz);

  }
}