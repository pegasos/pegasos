package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.Comparator;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.reports.*;

import static org.junit.Assert.*;

public class ReportsParserTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  public static String data = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
          "<web>\n" +
          "    <reports>\n" +
          "        <report type=\"UserValueBased\">\n" +
          "            <name>test</name>\n" +
          "            <display title=\"test\" type=\"Lines\">\n" +
          "                <defaultWeeks>52</defaultWeeks>\n" +
          "            </display>\n" +
          "            <values>\n" +
          "                <value>WEIGHTx</value>\n" +
          "                <value>BODYFATx</value>\n" +
          "            </values>\n" +
          "        </report>\n" +
          "        <report type=\"MetricBased\">\n" +
          "            <name>lsct</name>\n" +
          "            <display title=\"lsct\" type=\"SessionbasedLines\">\n" +
          "                <defaultWeeks>16</defaultWeeks>\n" +
          "                <config>\n" +
          "                    <entry name=\"yaxislabel\" value=\"'lsct_stage_power'\"/>\n" +
          "                    <entry name=\"toolTip\" value=\"'`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`'\"/>\n" +
          "                    <entry name=\"colors\" value=\"['steelblue', 'red', 'black', 'green', 'orange']\"/>\n" +
          "                </config>\n" +
          "            </display>\n" +
          "            <metrics>\n" +
          "                <metric>LSCT_POWER1</metric>\n" +
          "                <metric>LSCT_POWER2</metric>\n" +
          "                <metric>LSCT_POWER3</metric>\n" +
          "            </metrics>\n" +
          "            <sessions>\n" +
          "                <include param0=\"9\" param1=\"6\"/>\n" +
          "            </sessions>\n" +
          "        </report>\n" +
          "        <report type=\"MetricBasedAccumulated\">\n" +
          "            <name>energyweek</name>\n" +
          "            <display title=\"energy_week\" type=\"Bars\"/>\n" +
          "            <metrics>\n" +
          "                <metric>ENERGY</metric>\n" +
          "            </metrics>\n" +
          "            <accumulate>Simple</accumulate>\n" +
          "            <aggregate>SUM</aggregate>\n" +
          "            <groupby>WEEK</groupby>\n" +
          "        </report>\n" +
          "        <report type=\"MetricBasedAccumulated\">\n" +
          "            <name>timeinz</name>\n" +
          "            <display title=\"timeinz\" type=\"Bars\">\n" +
          "                <config>\n" +
          "                    <entry name=\"yaxislabel\" value=\"'Time in Zone [h:m:s]'\"/>\n" +
          "                    <entry name=\"toolTip\" value=\"'`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`'\"/>\n" +
          "                </config>\n" +
          "            </display>\n" +
          "            <metrics>\n" +
          "                <metric>TIME_IN_POWER_ZONE</metric>\n" +
          "            </metrics>\n" +
          "            <accumulate>Param</accumulate>\n" +
          "            <aggregate>SUM</aggregate>\n" +
          "            <groupby>WEEK</groupby>\n" +
          "        </report>\n" +
          "        <report type=\"UserValueBased\">\n" +
          "            <name>pmc</name>\n" +
          "            <display title=\"pmc\" type=\"Lines\">\n" +
          "                <defaultWeeks>52</defaultWeeks>\n" +
          "            </display>\n" +
          "            <values>\n" +
          "                <value>CTL</value>\n" +
          "                <value>ATL</value>\n" +
          "                <value>TSB</value>\n" +
          "                <value>TSS</value>\n" +
          "            </values>\n" +
          "        </report>\n" +
          "        <report type=\"MetricBasedAccumulated\">\n" +
          "            <name>mmp</name>\n" +
          "            <enhancer type=\"r\">\n" +
          "                <code>omidata.R</code>\n" +
          "            </enhancer>\n" +
          "            <metrics>\n" +
          "                <metric>MMP</metric>\n" +
          "                <metric>MMP2k</metric>\n" +
          "            </metrics>\n" +
          "            <accumulate>Param</accumulate>\n" +
          "            <aggregate>MAX</aggregate>\n" +
          "            <groupby>ALL</groupby>\n" +
          "        </report>\n" +
          "        <report type=\"SessionValue\">\n" +
          "            <name>keb</name>\n" +
          "            <dataQuery>\n" +
          "                <data field=\"answer\" name=\"Körperliche Leistungsfähigkeit\" table=\"questionnaire_answers\" where=\"question = 1\"/>\n" +
          "            </dataQuery>\n" +
          "            <sessions>\n" +
          "                <include param0=\"9\" param1=\"7\" param2=\"1\"/>\n" +
          "            </sessions>\n" +
          "        </report>\n" +
          "        <report type=\"MetricBasedGrouped\">\n" +
          "            <name>l60</name>\n" +
          "            <metrics>\n" +
          "                <metric>AVG_POWER</metric>\n" +
          "            </metrics>\n" +
          "            <filter conjunction=\"AND\">\n" +
          "                <lap comp=\"GT\" type=\"Duration\" value=\"55000\"/>\n" +
          "                <lap comp=\"LT\" type=\"Duration\" value=\"65000\"/>\n" +
          "            </filter>\n" +
          "            <groupby>DAY</groupby>\n" +
          "        </report>\n" +
          "    </reports>\n" +
          "</web>\n";

  @SuppressWarnings("unchecked")
  public static void TestData(Web web)
  {
    web.set("reports", new ArrayList<Report>());

    Report r = new UserValueBasedReport();
    r.name = "test";
    ((UserValueBasedReport) r).names = new String[] {"WEIGHTx", "BODYFATx"};
    ((Collection<Report>) web.get("reports")).add(r);
    r.display = new Display();
    r.display.type = Display.ReportType.Lines;
    r.display.title = "test";
    r.display.defaultWeeks = 52;

    r = new MetricBasedReport();
    r.name = "lsct";
    ((MetricBasedReport) r).metrics = new String[] {"LSCT_POWER1", "LSCT_POWER2", "LSCT_POWER3"};
    ((MetricBasedReport) r).includeFilters = new MetricBasedReport.SessionTypeFilters();
    ((MetricBasedReport) r).includeFilters.include = new MetricBasedReport.SessionTypeFilter[] {new MetricBasedReport.SessionTypeFilter(9, 6)};
    ((Collection<Report>) web.get("reports")).add(r);
    r.display = new Display();
    r.display.title = "lsct";
    r.display.type = Display.ReportType.SessionbasedLines;
    r.display.defaultWeeks = 16;
    r.display.config.put("toolTip", "'`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`'");
    r.display.config.put("colors", "['steelblue', 'red', 'black', 'green', 'orange']");
    r.display.config.put("yaxislabel", "'lsct_stage_power'");

    r = new MetricBasedAccumulatedReport();
    r.name = "energyweek";
    ((MetricBasedAccumulatedReport) r).type = "Simple";
    ((MetricBasedAccumulatedReport) r).metrics = new String[] {"ENERGY"};
    ((MetricBasedAccumulatedReport) r).groupBy = MetricBasedAccumulatedReport.GroupBy.WEEK;
    ((MetricBasedAccumulatedReport) r).aggregate = MetricBasedAccumulatedReport.Aggregate.SUM;
    r.display = new Display();
    r.display.title = "energy_week";
    r.display.type = Display.ReportType.Bars;
    r.display.defaultWeeks = 4;
    ((Collection<Report>) web.get("reports")).add(r);

    r = new MetricBasedAccumulatedReport();
    r.name = "timeinz";
    ((MetricBasedAccumulatedReport) r).type = "Param";
    ((MetricBasedAccumulatedReport) r).metrics = new String[] {"TIME_IN_POWER_ZONE"};
    ((MetricBasedAccumulatedReport) r).groupBy = MetricBasedAccumulatedReport.GroupBy.WEEK;
    ((MetricBasedAccumulatedReport) r).aggregate = MetricBasedAccumulatedReport.Aggregate.SUM;
    r.display = new Display();
    r.display.title = "timeinz";
    r.display.type = Display.ReportType.Bars;
    r.display.defaultWeeks = 4;
    r.display.config.put("toolTip", "'`${personResolver.getPerson(d.session.userId).name} ${series.name}: ${d.value}`'");
    r.display.config.put("yaxislabel", "'Time in Zone [h:m:s]'");
    ((Collection<Report>) web.get("reports")).add(r);

    r = new UserValueBasedReport();
    r.name = "pmc";
    ((UserValueBasedReport) r).names = new String[] {"CTL", "ATL", "TSB", "TSS"};
    ((Collection<Report>) web.get("reports")).add(r);
    r.display = new Display();
    r.display.type = Display.ReportType.Lines;
    r.display.title = "pmc";
    r.display.defaultWeeks = 52;

    r = new MetricBasedAccumulatedReport();
    r.name = "mmp";
    ((MetricBasedAccumulatedReport) r).type = "Param";
    ((MetricBasedAccumulatedReport) r).metrics = new String[] {"MMP", "MMP2k"};
    ((MetricBasedAccumulatedReport) r).groupBy = MetricBasedAccumulatedReport.GroupBy.ALL;
    ((MetricBasedAccumulatedReport) r).aggregate = MetricBasedAccumulatedReport.Aggregate.MAX;
    /*r.display = new Display();
    r.display.title = "timeinz";
    r.display.defaultWeeks = 4;*/
    r.enhancer = new REnhancer();
    ((REnhancer) r.enhancer).code_resource = "omidata.R";
    ((Collection<Report>) web.get("reports")).add(r);

    SessionValueReport svr = new SessionValueReport();
    svr.name = "keb";
    svr.includeFilters = new MetricBasedReport.SessionTypeFilters();
    svr.includeFilters.include = new MetricBasedReport.SessionTypeFilter[] {new MetricBasedReport.SessionTypeFilter(9, 7, 1)};
    svr.dataQuery = new SessionValueReport.DataQuery();
    svr.dataQuery.filter = new SessionValueReport.SessionDataFilter[]{new SessionValueReport.SessionDataFilter()};
    svr.dataQuery.filter[0].table = "questionnaire_answers";
    svr.dataQuery.filter[0].field = "answer";
    svr.dataQuery.filter[0].name = "Körperliche Leistungsfähigkeit";
    svr.dataQuery.filter[0].where = "question = 1";
    ((Collection<Report>) web.get("reports")).add(svr);

    MetricBasedGroupedReport l60 = new MetricBasedGroupedReport();
    l60.name = "l60";
    l60.metrics = new String[] {"AVG_POWER"};
    l60.lapQuery = new MetricBasedGroupedReport.LapQuery(3);
    l60.lapQuery.conj = Conjunction.AND;
    MetricBasedGroupedReport.LapQuery.LapFilter f = new MetricBasedGroupedReport.LapQuery.LapFilter();
    f.type = MetricBasedGroupedReport.LapQuery.LapFilter.LapFilterType.Duration;
    f.comp = Comparator.GT;
    f.value = 55000;
    l60.lapQuery.filters.add(f);
    f = new MetricBasedGroupedReport.LapQuery.LapFilter();
    f.type = MetricBasedGroupedReport.LapQuery.LapFilter.LapFilterType.Duration;
    f.comp = Comparator.LT;
    f.value = 65000;
    l60.lapQuery.filters.add(f);
    l60.groupBy = MetricBasedAccumulatedReport.GroupBy.DAY;
    ((Collection<Report>) web.get("reports")).add(l60);
  }

  @Test public void toStringsAndHashsWork()
  {
    Display display = new Display();
    display.type = Display.ReportType.Lines;
    display.title = "pmc";
    display.defaultWeeks = 52;

    assertEquals("Display(title=pmc, defaultWeeks=52, type=Lines, config={})", display.toString());
    assertNotEquals(0, display.hashCode());

    MetricBasedGroupedReport l60 = new MetricBasedGroupedReport();
    l60.name = "l60";
    l60.metrics = new String[] {"AVG_POWER"};
    l60.lapQuery = new MetricBasedGroupedReport.LapQuery(3);
    l60.lapQuery.conj = Conjunction.AND;
    MetricBasedGroupedReport.LapQuery.LapFilter f = new MetricBasedGroupedReport.LapQuery.LapFilter();
    f.type = MetricBasedGroupedReport.LapQuery.LapFilter.LapFilterType.Duration;
    f.comp = Comparator.GT;
    f.value = 55000;
    l60.lapQuery.filters.add(f);
    f = new MetricBasedGroupedReport.LapQuery.LapFilter();
    f.type = MetricBasedGroupedReport.LapQuery.LapFilter.LapFilterType.Duration;
    f.comp = Comparator.LT;
    f.value = 65000;
    l60.lapQuery.filters.add(f);
    l60.groupBy = MetricBasedAccumulatedReport.GroupBy.DAY;
    assertTrue(l60.toString().endsWith(
        ", metrics=[AVG_POWER], includeFilters=null), lapQuery=MetricBasedGroupedReport.LapQuery(filters=[MetricBasedGroupedReport.LapQuery.LapFilter(type=Duration, value=55000, comp=GT), MetricBasedGroupedReport.LapQuery.LapFilter(type=Duration, value=65000, comp=LT)], conj=AND), groupBy=DAY)"));

    SessionValueReport svr = new SessionValueReport();
    svr.name = "keb";
    svr.includeFilters = new MetricBasedReport.SessionTypeFilters();
    svr.includeFilters.include = new MetricBasedReport.SessionTypeFilter[] {new MetricBasedReport.SessionTypeFilter(9, 7, 1)};
    svr.dataQuery = new SessionValueReport.DataQuery();
    svr.dataQuery.filter = new SessionValueReport.SessionDataFilter[]{new SessionValueReport.SessionDataFilter()};
    svr.dataQuery.filter[0].table = "questionnaire_answers";
    svr.dataQuery.filter[0].field = "answer";
    svr.dataQuery.filter[0].name = "Körperliche Leistungsfähigkeit";
    svr.dataQuery.filter[0].where = "question = 1";
    // assertEquals("", svr.toString());
    // increase coverage ...
    assertNotEquals(0, svr.toString());
    assertNotEquals(0, svr.hashCode());
    assertNotEquals(0, l60.hashCode());

    REnhancer enhancer = new REnhancer();
    enhancer.code_resource = "omidata.R";
    assertNotEquals(0, enhancer.hashCode());

    MetricBasedAccumulatedReport r = new MetricBasedAccumulatedReport();
    r.name = "timeinz";
    r.type = "Param";
    r.metrics = new String[] {"TIME_IN_POWER_ZONE"};
    r.groupBy = MetricBasedAccumulatedReport.GroupBy.WEEK;
    r.aggregate = MetricBasedAccumulatedReport.Aggregate.SUM;
    assertTrue(r.toString().endsWith(", metrics=[TIME_IN_POWER_ZONE], includeFilters=null), type=Param, aggregate=SUM, groupBy=WEEK)"));
    assertNotEquals(0, r.hashCode());
  }

  @SuppressWarnings("unchecked")
  @Test public void test() throws IOException, ParserException
  {
    File conf= folder.newFile("web.xml");
    BufferedWriter writer= Files.newBufferedWriter(Paths.get(conf.getAbsolutePath()));
    writer.write(data);
    writer.close();

    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    ReportsParser parser = new ReportsParser(inst, false);
    parser.parse();

    List<Report> reports = (List<Report>) inst.getWeb().get("reports");
    assertEquals(8, reports.size());

    Web web = new Web();
    TestData(web);
    List<Report> r2 = (List<Report>) web.get("reports");
    for(int i = 0; i < reports.size(); i++)
    {
      assertEquals(r2.get(i), reports.get(i));
    }
  }
}