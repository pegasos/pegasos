package at.pegasos.generator.parser.webparser.parsers;

import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Test;

import at.pegasos.generator.parser.ParserException;
import at.pegasos.generator.parser.appparser.token.Application;
import at.pegasos.generator.parser.token.IInstance;
import at.pegasos.generator.parser.token.Instance;

public class TestAdminModulesParser {
  @Test
  public void test() throws ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("../server");
    IInstance inst= new Application(i);
    AdminModulesParser parser= new AdminModulesParser(inst, Paths.get("../"));
    parser.parse();
    
    System.out.println(inst.getWeb().getAdminModules().modules.sub);

    assertTrue(inst.getWeb().getAdminModules().modules.sub.containsKey("User"));
  }
}
