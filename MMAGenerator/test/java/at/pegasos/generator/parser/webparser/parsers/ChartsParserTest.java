package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import org.junit.*;
import org.junit.rules.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.*;

public class ChartsParserTest {
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  public static void TestData(Web web) throws ParserException
  {
    MapChart mapChart = new MapChart("chart_Map");
    mapChart.tbl = "gps"; mapChart.collat = "lat"; mapChart.collon = "lon";
    web.charts.addChart(mapChart);

    SingleLineChart hrChart = new SingleLineChart("chart_HR");
    hrChart.col = "hr"; hrChart.tbl = "hr"; hrChart.config("linecolor", "red"); hrChart.config("yaxislabel", "hr_bpm");
    web.charts.addChart(hrChart);

    MultiLineChart bikePower = new MultiLineChart("chart_BikePower", "bike", "field", new String[] {"sensor_nr", "power"});
    bikePower.config("yaxislabel", "bikepower_w"); bikePower.line_colors = "'red', 'blue', 'orange'";
    bikePower.config("filterymin", "-10"); bikePower.config("filterymax", "2500");
    web.charts.addChart(bikePower);

    Poincare p = new Poincare("chart_Poincare");
    p.default_avail = false;
    web.charts.addChart(p);

    Tachogram t = new Tachogram("Tachogram");
    t.default_avail = false;
    web.charts.addChart(t);

    DBTableChart bars = new DBTableChart("chart_BikePowerTinZ2", "Type.Bars", "session_metric", "param", "value", "metric = 25");
    bars.config("yaxislabel", "bikepower_w");
    bars.config("tooltip_seriespre", "Zone ");
    bars.config("tooltip_itempre", "Spent: ");
    web.charts.addChart(bars);

    GeneratedChartType metrics = new GeneratedChartType("Generated", "chart_Metrics");
    metrics.ctype = "Metrics";
    metrics.setAlwaysAvailable(true);
    metrics.default_avail = false;
    metrics.setType(Type.create("Metrics", "MetricsChartComponent", "./metricschart.component"));
    web.charts.addChart(metrics);

    GeneratedChartType strava = new GeneratedChartType("Generated", "chart_Strava");
    // strava.setAlwaysAvailable(false);
    strava.hasAvailable = true;
    strava.ctype = "Strava";
    strava.setClsArgs("StravaChartAvailable", null);
    strava.default_avail = false;
    strava.setType(Type.create("Strava", "StravaChartComponent","./strava.chart.component"));
    web.charts.addChart(strava);
  }

  @Test public void test() throws IOException, ParserException
  {
    File conf = folder.newFile("web.xml");
    List<String> lines = Files.readAllLines(Paths.get("test/res/at/pegasos/generator/parser/webparser/parsers/chartstest.xml"));
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(conf.getAbsolutePath()));
    for(String line : lines)
    {
      writer.write(line + "\n");
    }
    writer.close();

    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    ChartsParser parser = new ChartsParser(inst, false);
    parser.parse();

    Web web = new Web();
    TestData(web);
    Collection<Charts.Chart> r2 = web.charts.getCharts();

    Collection<Charts.Chart> charts = inst.getWeb().charts.getCharts();
    assertEquals(r2.size(), charts.size());

    Iterator<Charts.Chart> i1 = charts.iterator();
    Iterator<Charts.Chart> i2 = r2.iterator();
    while( i1.hasNext() && i2.hasNext() )
    {
      assertEquals(i2.next(), i1.next());
    }
  }


  @Test public void test_copy() throws IOException, ParserException
  {
    TestHelper.copy(Paths.get("at/pegasos/generator/parser/webparser/parsers/chartstest.xml"),
        folder.getRoot().toPath().resolve("web.xml"));

    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    ChartsParser parser = new ChartsParser(inst, false);
    parser.parse();

    Web web = new Web();
    TestData(web);
    Collection<Charts.Chart> r2 = web.charts.getCharts();

    Collection<Charts.Chart> charts = inst.getWeb().charts.getCharts();
    assertEquals(r2.size(), charts.size());

    Iterator<Charts.Chart> i1 = charts.iterator();
    Iterator<Charts.Chart> i2 = r2.iterator();
    while( i1.hasNext() && i2.hasNext() )
    {
      assertEquals(i2.next(), i1.next().copy());
    }
  }

  @Test public void exceptions_work() throws ParserConfigurationException
  {
    DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
    Document document = documentBuilder.newDocument();

    Element multiLine = document.createElement("chart");
    MultiLineChart c = new MultiLineChart("x");
    assertThrows(ParserException.class, () -> c.parse(multiLine, null));

    Node x = document.createElement("type");
    x.appendChild(document.createTextNode("test"));
    multiLine.appendChild(x);
    assertThrows(ParserException.class, () -> c.parse(multiLine, null));
  }
}