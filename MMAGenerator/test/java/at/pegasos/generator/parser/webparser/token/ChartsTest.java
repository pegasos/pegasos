package at.pegasos.generator.parser.webparser.token;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.webparser.token.charts.*;
import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class ChartsTest {

  @Test public void test_mergingWorks() throws ParserException
  {
    Charts chartsA = new Charts();
    Charts chartsB = new Charts();
    Charts chartsC = new Charts();

    Charts.Chart chart = new MapChart("a", "b", "c", "d");
    chartsA.addChart(chart);

    Activity node = chartsA.getAvailNode("1","1", "");
    node.adddefault = false;
    node.chartsPre = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("a", 1, true));

    node = chartsB.getAvailNode("1","", "");
    node.adddefault = true;
    node.chartsPre = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("b", 1, true));

    chartsC.addOther(chartsB);
    chartsC.addOther(chartsA);

    assertTrue("Chart 1-- should add default charts", chartsC.getAvailNode("1", "", "").adddefault);
    assertFalse("Chart 1-1- should not add default charts", chartsC.getAvailNode("1", "1", "").adddefault);
  }
}