package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.appparser.token.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import org.junit.*;

import java.nio.file.*;

import static org.junit.Assert.assertEquals;

public class BackendRepositoriesParserTest {
  @Test
  public void test() throws ParserException
  {
    Instance i= new Instance();
    i.directory= Paths.get("").toAbsolutePath();
    if( i.directory.getFileName().endsWith("MMAGenerator") )
      i.directory= Paths.get("../").toAbsolutePath();
    IInstance inst= new Application(i);
    BackendRepositoriesParser parser= new BackendRepositoriesParser(inst, i.directory);
    parser.parse();

    // Repositories(repositories=[Repositories.Repository(name=MessageTemplatesRepository),
    // Repositories.Repository(name=TeamWithGroupWithUsersRepository), Repositories.Repository(name=UserValueIntRepository),
    // Repositories.Repository(name=UserRolesRepository), Repositories.Repository(name=RoleRepository),
    // Repositories.Repository(name=TempEmailChangeRepository), Repositories.Repository(name=UserValueService),
    // Repositories.Repository(name=TempRegistrationRepository), Repositories.Repository(name=NotificationsRepository),
    // Repositories.Repository(name=TeamGroupsRepository), Repositories.Repository(name=SessionsRepository),
    // Repositories.Repository(name=TeamWithUsersRepository), Repositories.Repository(name=GroupRepository),
    // Repositories.Repository(name=NotificationTypesRepository), Repositories.Repository(name=UserDetailsRepository),
    // Repositories.Repository(name=TeamMembershipRepository), Repositories.Repository(name=MetricInfoRepository),
    // Repositories.Repository(name=UserValueDecRepository), Repositories.Repository(name=TeamRepository),
    // Repositories.Repository(name=TokenRepository), Repositories.Repository(name=MetricRepository),
    // Repositories.Repository(name=TUserRepository), Repositories.Repository(name=UserValueInfoRepository),
    // Repositories.Repository(name=MarkerRepository), Repositories.Repository(name=UserRepository),
    // Repositories.Repository(name=TeamInvitesRepository), Repositories.Repository(name=SessionInfoRepository)])
    Repositories repositories = (Repositories) inst.getWeb().get("backend.repositories");
    assertEquals(28, repositories.repositories.size());
  }
}