package at.pegasos.generator.parser.webparser.token;

import at.pegasos.generator.parser.*;
import org.junit.*;

import java.io.*;

import static org.junit.Assert.*;

public class ComponentsTest {
  @Test
  public void test() throws IOException, ParserException
  {
    Components components = new Components();
    components.addComponent("OAuthComponent", "oauth.component", "oauth", true, true);

    Components.Component oauth = components.entries().stream().filter(c -> c.name.equals("OAuthComponent")).findFirst().get();

    Components.Resolvable r = new Components.Resolvable("RunPostProcessorComponent", "run.postprocessor.component",
            "admin/tools");

    assertEquals("../admin/tools/", r.resolve("admin"));
    assertEquals("./oauth/oauth.component", components.resolve(oauth, ""));
    assertEquals("./loggedin.guard", components.resolve("LoggedInGuard", "app-routing"));
  }
}