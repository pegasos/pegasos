package at.pegasos.generator.parser.webparser.parsers;

import at.pegasos.generator.*;
import at.pegasos.generator.parser.*;
import at.pegasos.generator.parser.token.*;
import at.pegasos.generator.parser.webparser.token.*;
import at.pegasos.generator.parser.webparser.token.charts.Activity;
import at.pegasos.util.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class ActivityChartsParserTest {

  @Rule public TemporaryFolder folder = new TemporaryFolder();

  public static void TestData(Web web) throws ParserException
  {
    Charts charts= new Charts();
    Activity node = charts.getAvailNode("1", "", "");
    node.adddefault = true;

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("chart_Metrics", 1, true));
    node.chartsPre.add(new Activity.Chart("chart_Laps", 2, true));
    node.chartsPost.add(new Activity.Chart("chart_Strava", 99, true));

    node = charts.getAvailNode("1", "1", "");
    node.adddefault = true;
    node.overwrite = true;

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("chart_Metrics", 1, true));
    node.chartsPre.add(new Activity.Chart("chart_Laps", 2, true));
    node.chartsPre.add(new Activity.Chart("chart_Target", 3, true));
    node.chartsPre.add(new Activity.Chart("chart_HitsOnTarget", 4, true));

    node = charts.getAvailNode("9", "5", "");
    node.adddefault = false;

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("chart_HR", 1, true));
    node.chartsPre.add(new Activity.Chart("chart_Tachogram", 2, true));
    node.chartsPre.add(new Activity.Chart("chart_Poincare", 3, true));
    node.chartsPre.add(new Activity.Chart("chart_NoOrder", ActivityChartsParser.DEFAULT_ORDER, false));

    node = charts.getAvailNode("9", "5", "1");

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("chart_HRxxx", 1, true));

    node = charts.getAvailNode("9", "4", "");
    node.adddefault = true;

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPost.add(new Activity.Chart("X", 1, true));

    node = charts.getAvailNode("8", "", "");
    node.adddefault = false;

    node.chartsPre = new ArrayList<>();
    node.chartsPost = new ArrayList<>();
    node.chartsPre.add(new Activity.Chart("Y", 1, true));

    web.charts = charts;
  }

  @Test public void test() throws IOException, ParserException
  {
    TestHelper.copy(Paths.get("at/pegasos/generator/parser/webparser/parsers/activitychartstest.xml"),
        folder.getRoot().toPath().resolve("web.xml"));

    ServerInstance inst = new ServerInstance();
    inst.directory = folder.getRoot().toPath();

    ActivityChartsParser parser = new ActivityChartsParser(inst, false);
    parser.parse();

    Web web = new Web();
    TestData(web);

    List<DAG.Node<Activity>> expectedList = web.charts.getRoot().getAllChildren();
    Iterator<DAG.Node<Activity>> expected = expectedList.iterator();
    List<DAG.Node<Activity>> actualList = inst.getWeb().charts.getRoot().getAllChildren();
    Iterator<DAG.Node<Activity>> actual = actualList.iterator();

    assertEquals("Node lists have equal size", expectedList.size(), actualList.size());

    while( expected.hasNext() && actual.hasNext() )
    {
      assertEquals(expected.next().getID(), actual.next().getID());
    }
  }
}