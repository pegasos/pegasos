package at.pegasos.server.frontback.data;


public enum Type {
  Poincare,
  Tachogram,
  Line,
  MultiLine,
  Bars,
  Map
}
// End of generated class


