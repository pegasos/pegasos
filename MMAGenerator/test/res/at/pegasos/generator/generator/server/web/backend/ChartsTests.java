package at.pegasos.server.frontback.charts;


import at.pegasos.server.frontback.data.Type;
import java.util.List;
import at.pegasos.server.frontback.charts.DBChartMultiLine.LineType;
import at.pegasos.server.frontback.data.chart.*;
import java.util.ArrayList;

public class AvailableCharts {
  private final static List<AvailableChart> defaultCharts;
  private final static List<AvailableChart> availableCharts;
  
  private final static AvailableChart chart_Map= new DBMapChart("chart_Map", Type.Map, "gps", "lat", "lon");
  private final static AvailableChart chart_HR= new DBChartSimple("chart_HR", Type.Line, "hr", "hr");
  private final static AvailableChart chart_BikePower= new DBChartMultiLine("chart_BikePower", "bike", LineType.FIELD, new String[] {"sensor_nr", "power"});
  private final static AvailableChart chart_Poincare= new DBChartSimple("chart_Poincare", Type.Poincare, "hr", "hr");
  private final static AvailableChart Tachogram= new DBChartSimple("Tachogram", Type.Tachogram, "hr", "hr");
  private final static AvailableChart chart_BikePowerTinZ2= new DBChartTable("chart_BikePowerTinZ2", Type.Bars, "session_metric", "param", "value", "metric = 25");
  private final static AvailableChart chart_Metrics= new AlwaysAvailableChart("chart_Metrics", Type.Metrics);
  private final static AvailableChart chart_Strava= new StravaChartAvailable("chart_Strava", Type.Strava);
  
  
  static {
    defaultCharts= new ArrayList<AvailableChart>(4);
    defaultCharts.add(chart_Map);
    defaultCharts.add(chart_HR);
    defaultCharts.add(chart_BikePower);
    defaultCharts.add(chart_BikePowerTinZ2);
    
    availableCharts= new ArrayList<AvailableChart>(8);
    availableCharts.add(chart_Map);
    availableCharts.add(chart_HR);
    availableCharts.add(chart_BikePower);
    availableCharts.add(chart_Poincare);
    availableCharts.add(Tachogram);
    availableCharts.add(chart_BikePowerTinZ2);
    availableCharts.add(chart_Metrics);
    availableCharts.add(chart_Strava);
    
  }
  
  public static AvailableChart getByID(String id)
  {
    for(AvailableChart chart : availableCharts)
    {
      if( chart.name.equals(id) )
        return chart;
    }
    return null;
  }
  
  public static List<AvailableChart> ForSession(int param0, int param1, int param2)
  {
    return defaultCharts;
  }
  
}
// End of generated class


