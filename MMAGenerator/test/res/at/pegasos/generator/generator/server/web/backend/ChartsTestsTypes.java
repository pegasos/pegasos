package at.pegasos.server.frontback.data;


public enum Type {
  Poincare,
  Tachogram,
  Line,
  MultiLine,
  Bars,
  Map,
  Metrics,
  Strava
}
// End of generated class


