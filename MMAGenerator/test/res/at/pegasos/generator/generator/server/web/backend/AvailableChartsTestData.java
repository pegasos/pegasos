package at.pegasos.server.frontback.charts;


import at.pegasos.server.frontback.data.Type;
import java.util.List;
import at.pegasos.server.frontback.charts.DBChartMultiLine.LineType;
import at.pegasos.server.frontback.data.chart.*;
import java.util.ArrayList;

public class AvailableCharts {
  private final static List<AvailableChart> defaultCharts;
  private final static List<AvailableChart> availableCharts;
  
  
  private final static List<AvailableChart> ret1_1;
  private final static List<AvailableChart> ret1;
  private final static List<AvailableChart> ret9_5_1;
  private final static List<AvailableChart> ret9_5;
  private final static List<AvailableChart> ret9_4;
  private final static List<AvailableChart> ret8;
  
  static {
    defaultCharts= new ArrayList<AvailableChart>(0);
    
    availableCharts= new ArrayList<AvailableChart>(0);
    
    ret1_1= new ArrayList<AvailableChart>(10);
    ret1_1.add(chart_Metrics);
    ret1_1.add(chart_Laps);
    ret1_1.add(chart_Target);
    ret1_1.add(chart_HitsOnTarget);
    ret1_1.addAll(defaultCharts);
    ret1= new ArrayList<AvailableChart>(10);
    ret1.add(chart_Metrics);
    ret1.add(chart_Laps);
    ret1.addAll(defaultCharts);
    ret1.add(chart_Strava);
    ret9_5_1= new ArrayList<AvailableChart>(10);
    ret9_5_1.add(chart_HR);
    ret9_5_1.add(chart_Tachogram);
    ret9_5_1.add(chart_Poincare);
    ret9_5_1.add(chart_NoOrder);
    ret9_5_1.add(chart_HRxxx);
    ret9_5= new ArrayList<AvailableChart>(10);
    ret9_5.add(chart_HR);
    ret9_5.add(chart_Tachogram);
    ret9_5.add(chart_Poincare);
    ret9_5.add(chart_NoOrder);
    ret9_4= new ArrayList<AvailableChart>(10);
    ret9_4.addAll(defaultCharts);
    ret9_4.add(X);
    ret8= new ArrayList<AvailableChart>(10);
    ret8.add(Y);
  }
  
  public static AvailableChart getByID(String id)
  {
    for(AvailableChart chart : availableCharts)
    {
      if( chart.name.equals(id) )
        return chart;
    }
    return null;
  }
  
  public static List<AvailableChart> ForSession(int param0, int param1, int param2)
  {
    switch( param0 )
    {
      case 1:
        switch( param1 )
        {
          case 1:
            return ret1_1;
        }
        return ret1;
      case 9:
        switch( param1 )
        {
          case 5:
            switch( param2 ) 
            {
              case 1:
                return ret9_5_1;
            }
            return ret9_5;
          case 4:
            return ret9_4;
        }
        break;
      case 8:
        return ret8;
    }
    return defaultCharts;
  }
  
}
// End of generated class


