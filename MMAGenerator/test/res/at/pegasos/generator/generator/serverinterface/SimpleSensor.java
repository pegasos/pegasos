package at.univie.mma.serverinterface.sensors;


import at.univie.mma.serverinterface.ServerSensorSS;

public class BikeSS extends ServerSensorSS {

  public BikeSS(int nr, int datasets_per_packet)
  {
    super(nr, 13, datasets_per_packet);

    sample_length= 0;

    sampling= false;
    init();
  }
  
  public void setDataTS(long timestamp)
  {
    if( !sampling ) return;
    addData_fixed(timestamp);
  }
  
  public void setData()
  {
    if( !sampling ) return;
    addData_fixed();
  }
  
}
// End of generated class


