# Git Versioning
For each app or server you want to build you need to select the branch, version, etc. you want to use for the app.
Depending whether you are supported or not @dobiaschm42 (Martin Dobiasch) has provided you with the information which branch of the framework you should target. If you do not have such information it is propably best to target the stable branch.

## Basic Configuration
For each application or server you can specify against which branch of which repository it should be built against.

```
<git>
    <repository>../.git</repository>
    <branch>stable</branch>
</git>
```

## Global Configuration
By default any application will be built against the root of the current directory (i.e. `../.git`) using the 'stable' branch.
You can change this on an app by app or server by server basis.
To modify this behaviour globally use the `--repository` and `--branch` flags of the generator.

* `--repository` sets the global default repository address
* `--force-repository` enforce global repository. All repository configurations of apps / servers will be ignored
* `--branch` sets the global default branch

**Remember** that this changes only the default branch. If an app or server contains a `<git>` tag then this information will be used.
However, it is possible that the git information of an app/server contains only repository or branch information and the missing information is used from the global specification.

## Remote References
It is possible to specify a remote reference for the repository. This has, however, several drawbacks:
* Every time the repository is checked it will be cloned to your disk (this might change in the future as this can be optimised)
* Specifying the URL/Connection can be tedious. Depending on your configuration the URL might be rewritten. In this case you are on your own and need to dig into all possible git config files. See some remarks below.

*Remarks*: During testing both a `git@gitlab.phaidra.org` and a `https://github.com` reference worked. What did not work was to specify `https://gitlab.phaidra.org/...` while running it from a directory where `gitlab.phaidra.org` was specified as a remote. In this case the `https://` was translated into `https:///` ...

*Remarks*: It is important that you specify the connection information / URL correctly. Especially gitlab.phaidra.org will block you if you provide a wrong URL and thus try to login to the server with your own user rather than `git`.

*Credentials*: Currently only public key login is supported. Pegasos will identify your public key automatically using `~/.ssh/id_rsa`. It is important that your key is not password protected (as this is currently not supported).

### Known Issues
* Problems can arrise if you have a `known_hosts` file. JSch on which JGit relies needs the information in the following format `<hostname> ssh-rsa <longstring/longstring>`. To fix this problem run `ssh-keyscan -t rsa hostname >> ~/.ssh/known_hosts`.
