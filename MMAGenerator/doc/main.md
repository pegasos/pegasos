# Pegasos
## Running
By default Pegasos will use the directory `apps` to find the apps which it shoud build.
Use the switch `--apps-location` to point Pegsos to a different directory.
In this directory every folder represents one version of your App.
The only exception is the directory named `servers` which will be treated like a directory added with the `--server-location` switch.

## Servers
Every configuration can have several servers which are located in the config-directory `servers`.
See [servers](server.md).

## Sensors
All sensors are configured for a specific server-instance. See [servers](server.md) for more details.

## Applications
An application/MC-system consist of two parts: the specification and additional source code.

Any additional code for the app it self is placed in the folder `app`. Additional in that sense means source-code (`src`) and resources (`res`).
Moreover, additional code for the server can be placed in `server/src`, for feedback modules in `ai/src` and for the ServerInterface in `serverinterface/src`.

The specification consists of various files. The main file is `application.xml` containing the [menu](../../doc/client/menu.asciidoc), the [config](../../doc/client/config.asciidoc), additionally one has to declare the sports and sensors used by the application. % TODO (sensor declaration, sports and activities)

## Libraries
Can be used to provide common functionality to several apps or servers.
This also avoids problems or errors with redefining commands.

## Common files
* Files that should be present both in the app and for ai/server/serverinterface/webbackend can be placed in `common/src` of an application.
* `common-servers` will be present in ai, server and webbackend
* `common-backend` will be present in ai and server
* `common-server-webbackend` will be present in server and webbackend
* `common-ai-webbackend` will be present in ai and webbackend

## Further information
For more information on creating an app see [this](../../doc/client/README.asciidoc)
For more information on writing a feedback-module, extending the server see [this](create_ai.md)
