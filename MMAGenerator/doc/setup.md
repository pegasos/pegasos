# Required / Recommended programs

## IDE setup (if desired)

* Android Studio
* Android SDK (Android Studio should guide you through the install)
* Java

## OR build tools only:

* sdk tools (read below to setup Android SDK tools)
* Java

# Configuration

* Copy `environment.sample` to `environment` (in directory `MMAGenerator`) . Fill in the required information (Java_HOME and path separator)
* Create `server.defaults` in `MMAGenerator/config` (sample file `server.defaults.sample`)

## Downloading Android SDK
First download the Android tools:

`wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip`

Unzip and move to the correct directory:

```
unzip sdk-tools-linux-*.zip
sudo mkdir -p /opt/android
sudo mv tools/ /opt/android/
```

You might want to also run:

`sudo chown $USER:$USER /opt/android/`


Next we download API 28 and the platform tools.

```
yes | /opt/android/tools/bin/sdkmanager "platforms;android-28"
yes | /opt/android/tools/bin/sdkmanager "platform-tools"
```


