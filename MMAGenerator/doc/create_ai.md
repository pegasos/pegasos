# Creating an AI-Component on the Server
There are two options for extending the functionality of the server:
* Creating a feedbackmodule
* Creating a postprocessing module

Each feedbackmodule can have 0 or more postprocessing modules.

In order to add the component to the server there are two options:
1. add the component to your app
2. add it directly to the server

## Step by step
### 1. Add your component to `ai.xml`
```xml
<ai>
<feedbackmodules>
	<module param0="1" param1="1">Your.Class</module>
</feedbackmodules>
<postprocessing>
	<module param0="1">your.class</module>
	<module param0="1" param1="5">could.be.you</module>
	<module param0="9" param1="5" param2="1">possible.as.well</module>
</postprocessing>
</ai>
```
While feedback modules have to be specified with both `param0` and `param1` postprocessing modules can be installed for 0,1, or 2
TODO: check what options are possible. It might be possible to add something for 0 and 2, or only 2, or only 1.

### 2. Run the generator
Test your configuration by running the generator. This should work even though you have not implemented your modules yet.
Running can be speed up by adding the `--servers-only` flag ie ```./run.sh --apps-location <your-locaion> --servers-only`

### 3. Prepare your test environment
If you haven't done it already copy `local.properties.sample` to `local.properties` and fill in your server-IP and password. You can make things easier for you if you add the additional line
```
name=<name of the server>
```

Note: When only extending the AI and not the server, everything about `mma_server` can be ignored.

Next copy the generated files into your environemt (ai to ai, and mma_server to mma_server).
* Copy all files from `gen`
* copy `server/mma_server/src/at/univie/mma/server/parser/ControlCommand.java`
* copy `server/mma_server/src/at/univie/mma/server/parser/SensorDataParser.java`

### 4. Test your environment
To do so just compile your project. Forget about your lousy IDE and do so by running `ant create_run_jar`.

### 5. Now implement your components
Once you have a working prototype you can deploy it to the server by running `ant deploy`.
When chaning the server do not forget to restart the server.

## Implementing a Feedbackmodule
Your class needs to be a subclass of `AI_module`.
Everything what you want to do has to be started in the constructor.
The constructor must call the super constructor.

## Implementing a PostProcessingModule
Start by extending `BasePostProcessingModule`. Keep in mind that your module can be executed for just a single session or for multiple sessions.

One example are `at.univie.mma.ai.postprocessing.Unterricht` or `at.univie.mma.ai.postprocessing.MaxHearRate`. Both classes deal correctly with multiple sessions.

## Extending the communciation capabilities
TODO: document this
This can be usefull when the feedbackmodule has to send data to the client. There are some examples which make use of this. Take a look at exisiting `communication.xml` files. Generated interfaces for the communication can then be found in the ServerInterface (client). The server, however, still has to use the `feedback` table in the database.

## Testing your module
In addition to ... you can also invoke your modules manually on the server by running
```
java -cp mma_ai_testing.jar:mysql-connector-java-5.1.16-bin.jar at.univie.mma.ai.postprocessing.PostProcessor <session-id>
```
or
```
java -jar mma_ai_bleeding.jar <param0> <session-id> <param1> <param2> 1 0 0
```