# Server
## Base-Config
```
<?xml version="1.0" encoding="UTF-8"?>
<server>
    <name>testing</name>
    <git>
        <repository>../.git</repository>
        <branch>generator</branch>
    </git>
    <ip>131.130.176.217</ip>
    <port>64322</port>
</server>
```

Above a minimal example. You need to specifiy the name, ip and port of a server instance.

For specifying the build / git information see [gitversioning.md](gitversioning).

## Sensors
All sensors the server can understand have to be defined. 
Based on this information the ServerInterface and the parsers for the server are generated. Information on specifying the sensors can be found in [sensor.md](sensor).

## Customising
You can use the following directories to add additional code to your server or to overwrite existing files in order to customise your server

* `server/src` for the backend server source code
* `server/lib` for libraries used by the backend server
* `ai/src` for the ai source code
* `ai/lib` for libraries used by the ai
* `ai/assets` for assets packaed into the ai.jar
* `serverinterface/src` for the serverinterface
* `web/backend` for additions to the web backend (keep in mind to respect the directory structure. i.e. source code needs to be located in `web/backend/src`)
* `web/frontend/src` source for the web frontend
* `web/frontend/lang` language files for the frontend
* common files as described in [main.md](main)

## Web Configuration
For now all configuration for the web is done in the optional file `web.xml`. Use `web` as root element.

### Activitynames
Example:
``
<names>
	<name param0="9">Test</name>
	<name param0="9" param1="5">Test HRV</name>
	<name param0="9" param1="5" param2="1">HRV</name>
	<name param0="9" param1="5" param2="2">Orthostatic Test</name>
	<name param0="9" param1="6">LSCT</name>
	<name param0="1">Training</name>
</names>
``

### Charts
The configuration for charts is twofold:
* First the available charts have to be defined
* Charts can then be added to activities

In case an activity has no listing for charts the backend will return all charts with matching criteria (current behaviour).


#### Specifiying available charts
The following types of charts are available:
* dbline
* dbmline
* map
* poincare
* generated

Charts are listed in the `web.xml` inside the `<charts>` tag. Each available chart is defined using the `chart` tag. **The order in which you add the charts matters**.

All charts use `name` and `default`. When default is set to false this chart is not part of the charts added to activities by default.

##### 'dbline'
For a chart showing a line using the values from a column of a database table use the type `dbline`.
The chart then can specify the following attributes:
* `name`. Mandatory: a unique identifier for your chart
* `table`. Mandatory: name of the database table
* `column`. Mandatory: name of the column in the table
* `linecolor`. Optional: Color of the line in the chart
* `yaxislabel`. Mandorty: identifier used in the translations for your labeling your axis.
* `filterymin`. Optional
* `filterymax`. Optional

##### 'dbmline'
For a chart showing several lines in the same chart using the values of a database table use the type `dbmline`.
The chart then can specify the following attributes:
* `name`. Mandatory: a unique identifier for your chart
* `table`. Mandatory: name of the database table
* `type`. Mandatory: How the data will be assembled. The following types are available:
    - `field`. Data-lines are defined by a field in the table for example sensor_nr. lines[0]= discriminatory field, lines[1] data field
    - `cols`. Data-lines are defined in several columns of this table. For example acceleration data. (lines={acc_x, acc_y, acc_z)
* `lines`. Mandatory: specification of the data lines (see above). Separate entries by commas (no spaces).
* `linecolors`. Optional: Colors of the lines in the chart. Separate the colors with commas and put all under `'` (e.g. `'red','blue', 'orange'`)
* `yaxislabel`. Mandatory: identifier used in the translations for your labeling your axis.
* `filterymin`. Optional
* `filterymax`. Optional

##### 'dbmap'
For a map use type `dbmap`:
* `name`. Mandatory: a unique identifier for your chart
* `table`. Mandaotry: name of the database table
* `lat`. Mandatory: name of the column for latitudes in the table
* `lon`. Mandatory: name of the column for longitudes in the table

##### 'generated'
For adding a custom chart (thirdparty):
* `charttype`. Mandatory: type name i.e. the name of the type (to be used for referencing it internally)
* `chartclass`. Mandatory: name of the class / component in the frontend (i.e. the typescript component on the frontend)
* `chartfile`. Mandatory: file where the typescript code is stored. Has to be relative to 'app/charts'

You can then choose between three types of availability:

**Database**
You will have to add the two folliwing tags
* `table`. Mandatory: name of the database table used for checking if the chart is available
* `column`. Mandatory: name of the column in the table

**Always Available**
Add a `<alwaysavailable>true</alwaysavailable>` to the chart

**Custom Checker**
Add a `<available>` tag to the chart. Use the attribute `class` for specifying the class (needs to implement AvailableChart). Additionall arguments can be passed using the `arguments` attribute.

Example chargs
```
<chart type="dbline">
	<name>chart_HR</name>
	<table>hr</table>
	<column>hr</column>
	<linecolor>red</linecolor>
	<yaxislabel>hr_bpm</yaxislabel>
</chart>
<chart type="dbmap">
	<name>chart_Map</name>
	<table>gps</table>
	<lat>lat</lat>
	<lon>lon</lon>
</chart>
<chart type="generated">
	<name>chart_Laps</name>
	<charttype>Lapchart</charttype>
	<chartclass>LapChartComponent</chartclass>
	<chartfile>lapchart.component</chartfile>
	<table>marker</table>
	<column>rec_time</column>
	<default>false</default>
</chart>
```

#### Adding charts to activities
Place all your settings in a `activitycharts` tag. For each activity you want to configure use `activity`.
The specification of this activity is similar to the one in `activitynames`. 
When desired all available charts can be added by specifying `default="true"` as attribute for the activity.

```
<activitycharts>
	<activity param0="9" param1="5">
		<chart>chart_HR</chart>
		<chart>chart_Poincare</chart>
		<chart>chart_Tachogram</chart>
	</activity>
	<activity param0="1">
		<chart>chart_Laps</chart>
	</activity>
</activitycharts>
```

### Extending the web
* Place all your additional code in either `web/frontend/` or `web/backend`.
* routes can be added (see below)

#### Adding Components
Declare additional components using the `<components>` tag.
For very component add a `<component name="Name" file="file" path="path" />` tag.
* name: Name of your component
* file: filename excluding extension
* path: where the file is stored. Trailing `/` is not necessary. All locations are relative to `src/app`. You should not specify your paths with a leading `./`.
* declare (optional): when the component has to be declared by the app.module.ts add `declare="true"`
* entry (optional): for global entry components (specified in app.module.ts) add `entry="true"`

Example: Declaring component OAuthComponent located in `src/app/oauth/oauth.component.ts`

```
<web>
  <components>
    <component name="OAuthComponent" file="oauth.component" path="oauth" />
  </components>
</web>
```

#### Adding Modules
When developping larger functionalities which are not part of exisiting 'pages' or independend from the existing functionality (They can make use of existing functionality!) it is advised to develop a module.
This has the benefit of keeping loading times (size of the main script) down.
In order to have routing working similar to components declare a module the following way:

```
<web>
  <modules>
    <module name="MyModule" file="my.module" path="mymodule" />
  </modules>
</web>
```

* name: Name of the module. This name is used when loading it and refering to it (e.g. when using it in a route)
* file: name of the file in which the module is declared
* path: where the files are located (or at least the module file)

#### Routing
Use the `<routes>` element to contain your routing information.
When adding routes make sure the global configuration (in the server directory, not the application) contains a `<routes default="true">`. Otherwise you will have to add all default routes manualy.

Add a route using `<route path="your path" component="NameOfYourComponent" />` or `<route path="your path" module="NameOfYourModule" />`

The following options need to be specified for a route:
* `path`: action to be invoked when the entry is activated (i.e. the route to be activated)
* `component`: the name of the component which handles the route (as specified using the `component` tag)
* `module`: the name of the module which handles the route (as specified using the `module` tag, see above)

Optionally, the following parameter can be specified:
* `canActivate`: a list of guards for this route (comma separated without spaces). Each guard needs to be a component (either built in or defined using `component`).

The following guards are currently built in:
* `MenuGuard`: used for routes of the menu. Route can only be activated if the corresponding menu entry is available for the user (matching logged in status and roles).
* `LoggedInGuard`: route can only be activated if the user is logged in

Eamples:
```
<routes>
  <route path="oauth2_callback" component="OAuthComponent" canActivate="LoggedInGuard" />
  <route path="oauth2_callback/:partnerid" component="OAuthComponent" canActivate="LoggedInGuard" />
  <route path="testcomponent" component="TestComponent canActivate="MenuGuard" />
</routes>
```

* Route 1 and 2 are only accessible for logged in users. Presumably, they are not connected to a menu.
* Route 3 links to a component and is / should be part of the menu. Who can access is it configured in the corresponding menu entry

#### Menu
In order to change the menu of your web interface an app or server configuration can place `entry` tags inside a `menu` tag of the respective `web.xml`. The following example will define a sample menu entry:

```
<web>
  <menu>
    <entry name="mainMenu_same" action="sample" />
  </menu>
</web>
```
The following options can be specified for an entry:
* `name`: the name to be displayed in the menu. Use a translateable string if possible (i.e. also add an entry to the value files)
* `action`: action to be invoked when the entry is activated (i.e. the route to be activated, see above to change the default routing)
* `isPublic`: when set to false the menu entry is only visible to logged in users
* `requiredRoles`: a list of roles a user needs to have in order to be able to see/use the menu (comma separated without spaces)
* `order`: Used for sorting the menu entries

Note: the corresponding routes have to configured separately.