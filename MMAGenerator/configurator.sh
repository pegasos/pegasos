#!/bin/bash

source ./environment

echo "Compiling project"
ant compile

java -cp "src${JAVA_PATH_SEP}bin${JAVA_PATH_SEP}lib/gradle-tooling-api-7.3.1.jar${JAVA_PATH_SEP}lib/ant-1.10.12.jar${JAVA_PATH_SEP}lib/ant-launcher-1.10.12.jar${JAVA_PATH_SEP}lib/org.eclipse.jgit-4.3.1.201605051710-r.jar${JAVA_PATH_SEP}lib/slf4j-api-1.7.21.jar${JAVA_PATH_SEP}lib/jcommander-1.81.jar${JAVA_PATH_SEP}lib/log4j-core-2.13.3.jar${JAVA_PATH_SEP}lib/log4j-api-2.13.3.jar${JAVA_PATH_SEP}lib/log4j-slf4j-impl-2.13.3.jar${JAVA_PATH_SEP}lib/reflections-0.9.11.jar${JAVA_PATH_SEP}lib/guava-28.1-jre.jar${JAVA_PATH_SEP}lib/javassist-3.26.0-GA.jar" --module-path /usr/share/openjfx/lib --add-modules javafx.controls,javafx.fxml at.pegasos.generator.configurator.Configurator "$@"

