<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project default="compile" name="Create Pegasos" xmlns:ivy="antlib:org.apache.ivy.ant">

	<property name="javafx.path" value="/usr/share/openjfx/lib"/>
	<property name="java.target.version" value="8"/>

	<property name="main.build.dir" value="bin"/>
	<property name="test.dir" value="test"/>
	<property name="test.junit.build.dir" value="build/junit"/>
	<property name="junit.output.dir" value="junit" />

	<property name="ivy.install.version" value="2.5.0" />
	<condition property="ivy.home" value="${env.IVY_HOME}">
		<isset property="env.IVY_HOME" />
	</condition>

	<property name="ivy.home" value="${user.home}/.ant" />
	<property name="ivy.jar.dir" value="${ivy.home}/lib" />
	<property name="ivy.jar.file" value="${ivy.jar.dir}/ivy-${ivy.install.version}.jar" />

	<target name="-set-libs-local" if="javafx.exists" depends="test.if.javafx.exists">
	<path id="libs">
		<fileset dir="lib">
			<include name="*.jar" />
		</fileset>
		<fileset dir="${javafx.path}">
			<include name="*.jar" />
		</fileset>
	</path>
	</target>
	<target name="-set-libs-ivy" unless="javafx.exists" depends="test.if.javafx.exists">
		<path id="libs">
			<fileset dir="lib">
				<include name="*.jar" />
			</fileset>
		</path>
	</target>

	<target name="-set-libs" depends="-set-libs-local, -set-libs-ivy"/>

	<target name="compile" depends="-set-libs">
		<mkdir dir="${main.build.dir}" />
		<javac
		    fork="yes"
		    destdir="bin"
		    encoding="iso-8859-1"
		    includeantruntime="false"
		    source="${java.target.version}"
		    target="${java.target.version}"
		    debug="on">

			<src path="src"/>
			<classpath refid="libs" />
		</javac>
	</target>

	<target name="-create-dependencies">
		<echo message="Gathering dependencies"/>
		<jar jarfile="pegasos-dependencies.jar">
			<zipgroupfileset dir="lib">
				<include name="gradle-tooling-api-7.3.1.jar"/>
				<include name="ant-1.10.12.jar"/>
				<include name="ant-launcher-1.10.12.jar"/>
				<include name="guava-30.1-jre.jar"/>
				<include name="javassist-3.21.0-GA.jar"/>
				<include name="jcommander-1.81.jar"/>
				<include name="jsch-0.1.53.jar"/>
				<include name="jsch-0.1.55.jar"/>
				<include name="slf4j-api-1.7.21.jar"/>
				<include name="log4j-core-2.13.3.jar"/>
				<include name="log4j-api-2.13.3.jar"/>
				<include name="log4j-slf4j-impl-2.13.3.jar"/>
				<include name="guava-28.1-jre.jar"/>
				<include name="reflections-0.9.11.jar"/>
				<include name="snakeyaml-1.25.jar"/>
			</zipgroupfileset>
			<zipfileset src="lib/org.eclipse.jgit-4.3.1.201605051710-r.jar" excludes="META-INF/*.SF,META-INF/*.DSA,META-INF/*.RSA"/>
		</jar>
	</target>

	<target name="create_standalonejar" depends="compile, -create-dependencies">
		<available file="local.properties" property="local.properties.present"/>
		<antcall target=".notify-use-localproperties" />

		<jar destfile="pegasos-generator.jar">
			<manifest>
				<attribute name="Main-Class" value="at.univie.mma.MMAGenerator"/>
			</manifest>
			<fileset dir="${main.build.dir}"/>
			<zipfileset dir="src/at/pegasos/generator/configurator/res"
						prefix="at/pegasos/generator/configurator/res/"/>
			<zipfileset dir="src/at/pegasos/generator/configurator/"
						includes="**/*.fxml"
						prefix="at/pegasos/generator/configurator/"/>
			<fileset dir="." includes="config/*"/>
			<fileset dir="." includes="local.properties"/>
			<fileset dir="src" includes="log4j2.xml"/>
		</jar>

		<jar destfile="pegasos-generator-standalone.jar">
			<manifest>
				<attribute name="Main-Class" value="at.univie.mma.MMAGenerator"/>
			</manifest>
			<zipfileset src="pegasos-generator.jar"/>
			<zipfileset src="pegasos-dependencies.jar" excludes="META-INF/*.SF"/>
		</jar>
	</target>

	<target name=".notify-use-localproperties" if="local.properties.present">
		<echo message="'local.properties' present. Adding it to pegasos-generator.jar." />
	</target>

	<target name="download-ivy" if="${not file::exists('${ivy.jar.file}')}">
		<echo message="Downloading Apache Ivy" />
		<mkdir dir="${ivy.jar.dir}"/>
		<!-- download Ivy from web site so that it can be used even without any special installation -->
		<get src="https://repo1.maven.org/maven2/org/apache/ivy/ivy/${ivy.install.version}/ivy-${ivy.install.version}.jar"
			    dest="${ivy.jar.file}" usetimestamp="true"/>
	</target>

	<target name="init-ivy" depends="download-ivy">
		<!-- try to load ivy here from ivy home, in case the user has not already dropped
	              it into ant's lib dir (note that the latter copy will always take precedence).
	              We will not fail as long as local lib dir exists (it may be empty) and
	              ivy is in at least one of ant's lib dir or the local lib dir. -->
		<path id="ivy.lib.path">
			<fileset dir="${ivy.jar.dir}" includes="*.jar" />
		</path>
		<taskdef resource="org/apache/ivy/ant/antlib.xml" uri="antlib:org.apache.ivy.ant" classpathref="ivy.lib.path" />
	</target>

	<target name="resolve" description="--> retrieve dependencies with ivy" depends="init-ivy, resolve-jfx">
		<ivy:resolve file="ivy.xml"/>
		<ivy:retrieve />
	</target>

	<target name="test.if.javafx.exists">
		<condition property="javafx.exists">
			<available file="${javafx.path}" type="dir"/>
		</condition>
	</target>

	<target name="resolve-jfx" if="javafx.exists" depends="test.if.javafx.exists">
		<ivy:resolve file="ivy-javafx.xml"/>
	    <ivy:retrieve />
	</target>

	<path id="librariesJacoco">
		<fileset dir="lib">
			<include name="org.jacoco.ant-0.8.7.jar" />
			<include name="org.jacoco.core-0.8.7.jar" />
			<include name="org.jacoco.report-0.8.7.jar" />
			<include name="org.jacoco.agent-0.8.7.jar" />
			<include name="asm-9.1.jar" />
			<include name="asm-commons-9.1.jar" />
			<include name="asm-tree-9.1.jar" />
		</fileset>
	</path>

	<target name="jacoco-taskdef" depends="resolve">
		<!--  <taskdef uri="antlib:org.jacoco.ant" resource="org/jacoco/ant/antlib.xml" classpathref="librariesJacoco" />  -->
		<taskdef name="jacococoverage" classname="org.jacoco.ant.CoverageTask" classpathref="librariesJacoco" />
		<taskdef name="jacocoreport" classname="org.jacoco.ant.ReportTask" classpathref="librariesJacoco" />
	</target>

	<path id="classpath.test">
		<pathelement location="lib/junit-4.13.jar" />
		<pathelement location="lib/junit-addons-1.4.jar" />
		<pathelement location="lib/hamcrest-2.2.jar" />
		<pathelement location="lib/guava-30.1-jre.jar" />
		<pathelement location="lib/javassist-3.26.0-GA.jar" />
		<pathelement location="lib/jcommander-1.58.jar" />
		<pathelement location="lib/slf4j-api-1.7.21.jar" />
		<pathelement location="lib/guava-28.1-jre.jar" />
		<pathelement location="lib/reflections-0.9.11.jar" />
		<pathelement location="lib/snakeyaml-1.25.jar" />
		<pathelement location="${main.build.dir}" />

		<fileset dir="${javafx.path}">
			<include name="*.jar" />
		</fileset>
	</path>

	<target name="test-compile" depends="compile">
		<property name="debugJava" value="on" />

		<mkdir dir="${test.junit.build.dir}" />
		<javac srcdir="${test.dir}/java" destdir="${test.junit.build.dir}"
				includeantruntime="false">
			<classpath refid="classpath.test" />
		</javac>

		<!-- <copy todir="${test.junit.build.dir}">
				<fileset dir="src/test/experiment/resources/" excludes="**/*.java" />
			</copy> -->
	</target>

	<target name="test" depends="resolve, jacoco-taskdef, test-compile">
		<mkdir dir="${junit.output.dir}" />

		<jacococoverage>
			<junit printsummary="on" haltonfailure="yes" fork="true">
				<formatter type="xml"/>
				<classpath>
					<path refid="classpath.test" />
					<pathelement location="${test.junit.build.dir}" />
				</classpath>
				<formatter type="brief" usefile="false" />
				<batchtest todir="${junit.output.dir}">
					<fileset dir="${test.junit.build.dir}">
						<include name="**/*.class" />
						<exclude name="at/pegasos/generator/TestHelper.class" />
						<exclude name="at/pegasos/generator/generator/OutputHelper.class" />
						<exclude name="**/*$*.class" />
					</fileset>
				</batchtest>
			</junit>
		</jacococoverage>

		<jacocoreport>
			<executiondata>
				<file file="jacoco.exec" />
			</executiondata>

			<structure name="Pegasos">
				<classfiles>
					<fileset dir="${main.build.dir}" />
				</classfiles>
				<sourcefiles encoding="UTF-8">
					<fileset dir="src/" />
				</sourcefiles>
			</structure>

			<html destdir="report" />
		</jacocoreport>

		<junitreport todir="${junit.output.dir}">
			<fileset dir="${junit.output.dir}">
				<include name="TEST-*.xml" />
			</fileset>
			<report format="frames" todir="${junit.output.dir}" />
		</junitreport>
	</target>
</project>
