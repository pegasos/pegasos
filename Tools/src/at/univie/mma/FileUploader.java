package at.univie.mma;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.serverinterface.response.ServerResponse;
import at.univie.mma.serverinterface.sensors.FootPodSensor;
import at.univie.mma.serverinterface.sensors.HRSensor;

public class FileUploader {
	public static void main(String args[]) throws InterruptedException {
		if( args.length != 4 ) {
			printUsage();
			return;
		}
		
		FileUploader f= new FileUploader(args[0], args[1], Integer.parseInt(args[2]));
		f.connect();
		f.setUpSensors();
		f.readFile(args[3]);
		System.out.println("Done Readining");
		f.close();
		System.out.println("Closed");
	}
	
	private static void printUsage() {
		System.out.println("MMAv3 FileUploader usage: FileUploader username password activity_c filename");
	}
	
	byte hr_register[];
	byte fp_register[];
	HRSensor sensor_hr;
	FootPodSensor sensor_fp;
	ServerInterface server;
	
	String username;
	String password;
	int activity_c;
	
	public FileUploader(String user, String pass, int activity_c) {
		hr_register= new byte[11];
		fp_register= new byte[8];
		
		username= user;
		password= pass;
		this.activity_c= activity_c;
	}
	
	public void connect() {
		System.out.println("Connecting");
		server= ServerInterface.getInstance("131.130.176.217", 64111, 1000);
		server.connect();
		System.out.println("Logging in");
		server.login(username, password, "1", "123", ServerInterface.LANGUAGE_DE);
		System.out.println("Logged in");
	}
	
	private void close() {
		server.close();
	}
	
	public void setUpSensors() {
		sensor_hr= new HRSensor(1, 1);
		sensor_fp= new FootPodSensor(2, 1);
		
		server.setSensorCount(2);
		// ServerSensor ss= new ServerSensor(i, s.getSensorType(), s.getDataSetsPerPacket());
		server.attachSensor(sensor_hr);
		server.attachSensor(sensor_fp);
	}
	
	public void readFile(String filename) throws InterruptedException {
		long starttime = System.currentTimeMillis();
		int activity=(activity_c/100)%10;
		int param1=(activity_c/10)%10;
		int param2=activity_c%10;
		server.startSending();
		server.startSensorTime(starttime);
	  server.startActivity(activity, param1, param2);
	  // server.restartSession();
	  Thread.sleep(1000);

		try(FileReader inputStream = new FileReader(filename)) {
			try(BufferedReader reader= new BufferedReader(inputStream)) {
				//skip 5 lines
				for(int i= 0; i < 5; i++)
					reader.readLine();

				String line;
			    while( (line= reader.readLine()) != null)
			    {
			       String data[]= line.split(";");
			       /*System.out.print("{");
			       for(String s : data )
			    	   System.out.print(s + " ");
			       System.out.println("}");*/
			       
			       int hr= Integer.parseInt(data[2]);
			       double speed_kmh= Double.parseDouble(data[3].replace(',','.'));
			       int distance_m= Integer.parseInt(data[4]);
			       int strides= Integer.parseInt(data[5]);
			       System.out.println("HR: " + hr + " Speed_km/h: " + speed_kmh + " distance: " + distance_m + " strides:" + strides);
			       
			       int fp_speed= (int) (speed_kmh * 1000.0 / 3.6);
			       // hr_register[]
			       
			       sensor_hr.setCurrentHR(hr);
			       sensor_fp.setCurrentSpeed(fp_speed);
			       sensor_fp.setDistance(distance_m);
			       sensor_fp.setStrideCount(strides);
			       
			       sensor_hr.sample();
			       sensor_fp.sample();
			       
			       Thread.sleep(1000);
			       pollResponses();
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Done uploading");
		server.stopSending(true);
		server.stopActivity();
	}
	
	private void pollResponses() {
		System.out.println("Start polling");
		List<ServerResponse> responses = server.pollResonses();
		for(ServerResponse r: responses)
		{
			System.out.println(r.getClass().getName() + " " + r.getResultCode() + " " + r.getRaw());
		}
		System.out.println("End polling");
	}
	
}
