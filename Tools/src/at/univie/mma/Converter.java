package at.univie.mma;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import at.univie.mma.serverinterface.SensorTypes;

public class Converter {
	public static Map<String,Integer> vals;
	
	static{
		vals= new HashMap<String,Integer>();
		vals.put("HR", 11);
		
		vals.put("GPS.LAT",   21);
		vals.put("GPS.LON",   22);
		vals.put("GPS.ACC",   23);
		vals.put("GPS.SPEED", 24);
		vals.put("GPS.BEAR",  25);
		vals.put("GPS.ALT",   26);
		
		vals.put("BIKE.CAD",  31);
		vals.put("BIKE.SPD",  32);
		vals.put("BIKE.PWR",  33);
		vals.put("BIKE.DIST", 34);
		
		vals.put("MARKER", 41);
	}
	

	public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException {
		String filename;
		
		filename= args[0];			
		
		String fn= Paths.get(filename).getFileName().toString().split("[.]")[0];
		String a[]= fn.split("_")[0].split("-");
		String b= fn.split("_")[1];
		
		Calendar d= Calendar.getInstance();
		// System.out.println(d.toString());
		d.clear();
		d.set(Integer.parseInt(a[0]), Integer.parseInt(a[1])-1, Integer.parseInt(a[2]), 
		    Integer.parseInt(b.substring(0, 2)), Integer.parseInt(b.substring(2, 4)), 
		    Integer.parseInt(b.substring(4, 6)));
		// System.out.println(d.toString() + " " + d.getTime
		
		new Converter(d.getTime().getTime()).readFile(filename);
	}

	private long starttime;
	
	public Converter(long time)
	{
		starttime= time;
	}
	
	Packet p;
	
	public void readFile(String filename) throws ClassNotFoundException, IOException, InterruptedException
	{
		FileInputStream in= new FileInputStream(new File(filename)) ;
		p= null;
		
		byte b_len[]= new byte[2];
		
		//long pos= in.getChannel().position();
		while( (b_len[0]= (byte) in.read() ) != -1 )
		{
      b_len[1]= (byte) in.read();
      p= new Packet(in, b_len);
      // System.out.println(String.format("%08X", pos) + " " + p);
      
      packetparse();
      
      //pos= in.getChannel().position();
		}
	}
	
	int datapackages;
	byte[] in;
	int bc;

	private void packetparse()
	{
		in= p.getBytes();
		
		datapackages= ((int)in[4]&0xFF)|((int)((in[3]&0xff)/32)<<8);
		long time_since_start   = ((int)in[5]&0xFF)|((int)in[6]&0xFF)<<8|((int)in[7]&0xFF)<<16|((int)in[8]&0xFF)<<24;
		// System.out.println("NEW PACKAGE:TIME SINCE START:"+time_since_start);
		
		bc= 8;
		
		// System.out.println("" + p.getBytes()[2] + " packs: " + datapackages + " ts" + time_since_start);
		switch(p.getBytes()[2])
    {
    	case SensorTypes.HR:
    		parseHr(time_since_start);
				break;
				
    	case SensorTypes.GPS:
    		parseGps(time_since_start);
				break;
				
			case SensorTypes.BIKENR:
				parseBike(time_since_start);
				break;
				
			case SensorTypes.MARKER:
				parseMarker(time_since_start);
				break;
    }
	}

	private void parseHr(long time_since_start)
	{
		int time_last_measurement;
		int hr_var[]= new int[5];

    long time;
    
    for (int x=0;x<datapackages;x++)
    { 
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      int hr=((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
      int hr_var_count= (int)in[bc+5];
      if( hr_var_count > 4 )
        hr_var_count= 4; //drop additional values // TODO: not good to do this
      //System.out.println("Hrv_count: " + hr_var_count);
      
      int y;
      for(y= 0; y < hr_var_count; y++)
      {
        hr_var[y]=((int)in[bc+6+y*2]&0xFF)|((int)in[bc+7+y*2]&0xFF)<<8;
      }
      
      time= starttime+time_since_start;
      // System.out.println("HR:"+hr+" HRV: {" + hr_var[0] + "," + hr_var[1] + ","+ hr_var[2] + ","+ hr_var[3] + "} STARTTIME:"+starttime+" TIME LAST MEASURMENT:"+time_last_measurement+" / LAST MEASUREMENT ABSOLUTE:"+new Date(time)+" ");
      output("HR", time, hr);
      
      bc=bc+6+hr_var_count*2;
    }
	}
	
	private void parseGps(long time_since_start)
	{
		int time_last_measurement;
		double gps_lat= 0; // field: 0
    double gps_lon= 0; // field: 1
    short gps_alt= 0; // field: 2
    short gps_speed= 0; // field: 3
    short gps_acc= 0; // field: 4
    short gps_bear= 0; // field: 5
    
    long time;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      {long tmp= 0;for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+3+i] & 0xff);}gps_lat= ((double)tmp / 100000000);}
      {long tmp= 0;for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+11+i] & 0xff);}gps_lon= ((double)tmp / 100000000);}
      gps_alt= (short)(in[bc+19]&0xFF|in[bc+20]<<8);
      gps_speed= (short)(in[bc+21]&0xFF|in[bc+22]<<8);
      gps_acc= (short)(in[bc+23]&0xFF|in[bc+24]<<8);
      gps_bear= (short)(in[bc+25]&0xFF|in[bc+26]<<8);
      
      time= starttime+time_since_start;
      //System.out.println("Gps: gps_lat:"+gps_lat+ " gps_lon:"+gps_lon+ " gps_alt:"+gps_alt+ " gps_speed:"+gps_speed+ " gps_acc:"+gps_acc+ " gps_bear:"+gps_bear+ " ");
      output("GPS.LAT", time, gps_lat);
      output("GPS.LON", time, gps_lon);
      output("GPS.ALT", time, gps_alt);
      output("GPS.SPEED", time, gps_speed);
      output("GPS.ACC", time, gps_acc);
      output("GPS.BEAR", time, gps_bear);
      
      bc= bc + 26;
    }
	}
	
	private void parseBike(long time_since_start)
	{
		int time_last_measurement;
		int speed_mm_s= 0; // field: 0
    int distance_m= 0; // field: 1
    int cadence= 0; // field: 2
    short power= 0; // field: 3
    
    long time;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      speed_mm_s= ((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
      cadence= ((int)in[bc+7]&0xFF)|((int)in[bc+8]&0xFF)<<8;
      power= (short)(in[bc+9]&0xFF|in[bc+10]<<8);
      distance_m= ((int)in[bc+5]&0xFF)|((int)in[bc+6]&0xFF)<<8;

      // System.out.println("Bike: speed_mm_s:"+speed_mm_s+ " distance_m:"+distance_m+ " cadence:"+cadence+ " power:"+power);
      time= starttime+time_since_start;
      
      bc= bc + 10;
      
      output("BIKE.SPD", time, speed_mm_s);
      output("BIKE.CAD", time, cadence);
      output("BIKE.PWR", time, power);
      output("BIKE.DIST", time, distance_m);
    }
	}
	
	private void parseMarker(long time_since_start)
	{
		int time_last_measurement;
    
    long time;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      // System.out.println("Marker: ");
      time= starttime+time_since_start;
      
      bc= bc + 2;
      
      output("MARKER", time, 1);
    }
	}
	
	private void output(String name, long timestamp, Object value)
	{
		String out= timestamp + ";"; 
		out+= vals.get(name) + ";";
		out+= value;
		
		System.out.println(out);
	}
	
	
	
	public class Packet implements Serializable {
		private static final long serialVersionUID = -7559351673721248131L;
		
		private byte[] bytes;
		
		/**
		 * Reads a packet of a file. Starting at the current position
		 * @param in the file stream
		 * @param b_len length of the packet (as two bytes, read from the input stream, yes you have to do that)
		 * @throws IOException
		 */
		public Packet(FileInputStream in, byte[] b_len) throws IOException {
			int number_of_bytes=  ((int)b_len[0]&0xFF)|((int)b_len[1]&0xFF)<<8;
			
			this.bytes= new byte[number_of_bytes];
			
			this.bytes[0]= (byte) (number_of_bytes & 0xff); // Number of bytes in Package LOW
			this.bytes[1]= (byte) ((number_of_bytes >> 8) & 0xff); // Number of bytes in HIGH
			
			for(int i= 2; i < number_of_bytes; i++)
			{
				this.bytes[i]= (byte) in.read();
			}
			// in.read(this.bytes, 2, number_of_bytes - 2);
		}
		
		public final byte[] getBytes() {
			return this.bytes;
		}
		
		public String toString() {
			String ret= "[";
			for(int i= 0; i < bytes.length; i++)
			{
				if( i> 0)
					ret+= " ";
				ret+= "" + bytes[i];
			}
			ret+= "]";
			return ret;
		}
	}
}
