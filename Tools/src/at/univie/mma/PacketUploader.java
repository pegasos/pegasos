package at.univie.mma;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

import at.univie.mma.serverinterface.Packet;
import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.serverinterface.response.*;

public class PacketUploader {
	public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException {
		if( args.length != 7 && args.length != 6 ) {
			printUsage();
			System.out.println(args.length);
			System.exit(1);
		}
		
		int activity_c;
		int param2_hook;
		int club;
		String filename;
		
		club= Integer.parseInt(args[3]);
		activity_c= Integer.parseInt(args[4]);
		if( args.length == 6 )
		{
			param2_hook= 0;
			filename= args[5];			
		}
		else
		{
			param2_hook= Integer.parseInt(args[5]);
			filename= args[6];
		}
		
		String fn= Paths.get(filename).getFileName().toString().split("[.]")[0];
		String a[]= fn.split("_")[0].split("-");
		String b= fn.split("_")[1];
		
		Calendar d= Calendar.getInstance();
		// System.out.println(d.toString());
		d.clear();
		d.set(Integer.parseInt(a[0]), Integer.parseInt(a[1])-1, Integer.parseInt(a[2]), 
		    Integer.parseInt(b.substring(0, 2)), Integer.parseInt(b.substring(2, 4)), 
		    Integer.parseInt(b.substring(4, 6)));
		// System.out.println(d.toString() + " " + d.getTime().getTime());
		
		boolean simulate= true;
		PacketUploader u= new PacketUploader(args[1], args[2], club, activity_c, param2_hook, simulate);
		if( !simulate )
		{
		  u.connect(Integer.parseInt(args[0]));
		  u.preFile(d.getTime().getTime());
		}
		u.readFile(filename);
		if( !simulate ) 
		  u.close();
	}
	
	private static void printUsage() {
		System.out.println("PacketUploader server[0:p,1:s,2:t] username password club([0,1]) activity_c [param2_hook] filename");
	}
	
	ServerInterface server;
	
	String username;
	String password;
	int club;
	int activity_c;
	int param2_hook;
	
	long start;
	long last_time;
	
	boolean simulate= false;
	
	public PacketUploader(String username, String password, int club, int activity_c, int param2_hook, boolean simulate) {
		this.username= username;
		this.password= password;
		this.club= club;
		this.activity_c= activity_c;
		this.param2_hook= param2_hook;
		this.simulate= simulate;
	}
	
	public void connect(int servernr) {
		System.out.println("Connecting");
		
		int port;
		switch(servernr)
		{
		  case 0:
		    port= 64111;
		    break;
		  case 1:
		    port= 64222;
		    break;
		  case 2:
		    port= 64322;
		    break;
	    default:
		    throw new IllegalArgumentException();
		}
		
		server= ServerInterface.getInstance("131.130.176.217", port, 1000);
		
		if( !server.connect() )
		{
			System.err.println("Connecting to server failed. Exiting");
			System.exit(1);
		}
		System.out.println("Logging in");
		server.login(username, password, "" + club, "123", ServerInterface.LANGUAGE_DE);
		
		List<ServerResponse> responses= null;
		while( responses == null || responses.size() == 0 )
			responses= server.pollResonses();
		
		boolean loggedin= false;
		System.out.println("Responses: " + responses);
		for(ServerResponse r : responses)
		{
			if( r instanceof LoginMessage )
			{
				if(r.getResultCode() == ServerResponse.RESULT_OK)
				{
					loggedin= true;
				}
				else
				{
					System.err.println("Logging in failed.\n" + r.getRaw() + "\nExiting");
					System.exit(1);
				}
			}
		}
		
		if( loggedin == false )
		{
			System.err.println("Logging in failed. Exiting");
			System.exit(1);
		}
	}
	
	private void close() throws InterruptedException {
		server.stopSending(true);
		
		List<ServerResponse> responses= null;
		while( responses == null || responses.size() == 0 )
			responses= server.pollResonses();
		
		System.out.println("Respones: " + responses);
		for(ServerResponse r : responses)
		{
			System.out.println(r.getClass().getName());
		}
		
		server.stopActivity(start+last_time);
		
		while( responses == null || responses.size() == 0 )
			responses= server.pollResonses();
		
		System.out.println("Respones: " + responses);
		for(ServerResponse r : responses)
		{
			System.out.println(r.getClass().getName());
		}
		
		Thread.sleep(1000);
		
		server.close();
	}
	
	private void preFile(long start) throws InterruptedException {
		System.out.println("Start Sending");
		server.startSending();
		System.out.println("Start sensortime " + start);
		server.startSensorTime(start);
		this.start= start;

		System.out.println("Start Activity " + start);
		if( param2_hook != 0 )
		{
			int param1 = 0, param2 = 0;

			int activity= (activity_c / 100) % 10;
			param1= (activity_c / 10) % 10;
			param2= param2_hook;
			server.startActivityT(activity, param1, param2, start);
		}
		else
			server.startActivityT(activity_c, start);

		Thread.sleep(1000);
		
		List<ServerResponse> responses= null;
		responses= server.pollResonses();
		
		System.out.println("Respones: " + responses);
		for(ServerResponse r : responses)
		{
			System.out.println(r.getClass().getName());
		}
	}
	
	public void readFile(String filename) throws ClassNotFoundException, IOException, InterruptedException
	{
		FileInputStream in= new FileInputStream(new File(filename)) ;
		Packet p = null;
		
		byte b_len[]= new byte[2];
		
		long pos= in.getChannel().position();
		while( (b_len[0]= (byte) in.read() ) != -1 )
		{
      b_len[1]= (byte) in.read();
      p= new Packet(in, b_len);
      System.out.println(String.format("%08X", pos) + " " + p);
      pos= in.getChannel().position();
				/*
				byte[] bytes= p.getBytes();
				int sensortype = bytes[0] & 0xff;
				int sensornr = bytes[1] & 31 & 0xff;
				int datapackages = ((int) bytes[2] & 0xFF) | ((int) ((bytes[1] & 0xff) / 32) << 8);
				int time_since_start = ((int) bytes[3] & 0xFF) | ((int) bytes[4] & 0xFF) << 8 | ((int) bytes[5] & 0xFF) << 16 | ((int) bytes[6] & 0xFF) << 24;
				
				// System.out.println("Number_of_bytes:" + number_of_bytes);
				System.out.println("Sensortype:" + sensortype);
				System.out.println("Sensornr:" + sensornr);
				System.out.println("Packages:" + datapackages);
				System.out.println("NEW PACKAGE:TIME SINCE START:" + time_since_start);*/
			
      if( !simulate )
      {
        server.addPacket(p);
        Thread.sleep(25);
      }
		}
		
		in.close();
		last_time= (p != null ? p.getTimeSinceStart() : 0);
	}
}
