package at.univie.mma;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.univie.mma.serverinterface.Packet;
import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.serverinterface.response.LoginMessage;
import at.pegasos.serverinterface.response.ServerResponse;

public class TcxUploader {
  private static class Parameters {
    @Parameter(names = "--club", description = "This is a club-session [true by default]")
    public boolean club= true;
    
    @Parameter(names = {"--activityc", "-a"}, description = "Activity")
    public int activity_c;
    
    @Parameter(names = "--param2", description = "Param2")
    public int param2= 0;
    
    @Parameter(names = "--user", description = "Username")
    public String user;
    
    @Parameter(names = "--password", description = "Password")
    public String passwd;
    
    @Parameter(names = "--server", description = "server[0:p,1:s,2:t]")
    public int server;
    
    @Parameter(names = "--fn", description = "filename")
    public String fn;
    
    @Parameter(names = "--help", help = true)
    public boolean help = false;
  }
  
	public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException, ParserConfigurationException, SAXException, ParseException {
	  Parameters p= new Parameters();
    
	  JCommander jCommander = new JCommander(p, args);
    
    if( p.help )
    {
      jCommander.usage();
      return ;
    }
	  
		/*String fn= Paths.get(p.fn).getFileName().toString().split("[.]")[0];
		String a[]= fn.split("_")[0].split("-");
		String b= fn.split("_")[1];
		
		Calendar d= Calendar.getInstance();
		// System.out.println(d.toString());
		d.clear();
		d.set(Integer.parseInt(a[0]), Integer.parseInt(a[1])-1, Integer.parseInt(a[2]), 
		    Integer.parseInt(b.substring(0, 2)), Integer.parseInt(b.substring(2, 4)), 
		    Integer.parseInt(b.substring(4, 6)));*/
		// System.out.println(d.toString() + " " + d.getTime().getTime());
		
		TcxUploader u= new TcxUploader(p.activity_c, p.param2);
		u.connect(p.server,p.user, p.passwd, p.club ? 1 : 0);
		u.openFile(p.fn);
		u.preRead();
		u.readFile();
		u.close(true,true);
	}
	
	ServerInterface server;
	
	String username;
	String password;
	int club;
	int activity_c;
	int param2_hook;
	
	long start;
	long last_time;

  private Document doc;

  private SimpleDateFormat sdf;
	
	public TcxUploader(int activity_c, int param2_hook) {
		this.activity_c= activity_c;
		this.param2_hook= param2_hook;
		
		sdf= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
	}
	
	public void setSI(ServerInterface instance)
  {
    this.server= instance;
  }
	
	public void connect(int servernr, String username, String password, int club) {
	  this.username= username;
    this.password= password;
    this.club= club;
	  
		System.out.println("Connecting");
		
		int port;
		switch(servernr)
		{
		  case 0:
		    port= 64111;
		    break;
		  case 1:
		    port= 64222;
		    break;
		  case 2:
		    port= 64322;
		    break;
	    default:
		    throw new IllegalArgumentException();
		}
		
		server= ServerInterface.getInstance("131.130.176.217", port, 1000);
		
		if( !server.connect() )
		{
			System.err.println("Connecting to server failed. Exiting");
			System.exit(1);
		}
		System.out.println("Logging in");
		server.login(username, password, "" + club, "123", ServerInterface.LANGUAGE_DE);
		
		List<ServerResponse> responses= null;
		while( responses == null || responses.size() == 0 )
			responses= server.pollResonses();
		
		boolean loggedin= false;
		System.out.println("Respones: " + responses);
		for(ServerResponse r : responses)
		{
			if( r instanceof LoginMessage )
			{
				if(r.getResultCode() == ServerResponse.RESULT_OK)
				{
					loggedin= true;
				}
				else
				{
					System.err.println("Logging in failed.\n" + r.getRaw() + "\nExiting");
					System.exit(1);
				}
			}
		}
		
		if( loggedin == false )
		{
			System.err.println("Logging in failed. Exiting");
			System.exit(1);
		}
	}
	
	public void close(boolean need_responses, boolean close_server) throws InterruptedException {
    server.stopSending(true);
    
    List<ServerResponse> responses= null;
    if( need_responses )
    {
      while( responses == null || responses.size() == 0 )
        responses= server.pollResonses();
    }
    
    System.out.println("Respones: " + responses);
    if( responses != null )
      for(ServerResponse r : responses)
      {
        System.out.println(r.getClass().getName());
      }
    
    server.stopActivity(start+last_time);
    
    if( need_responses )
    {
      while( responses == null || responses.size() == 0 )
        responses= server.pollResonses();
      
      System.out.println("Respones: " + responses);
      for(ServerResponse r : responses)
      {
        System.out.println(r.getClass().getName());
      }
      
      Thread.sleep(1000);
    }
    
    if( close_server )
      server.close();
  }
	
	public void openFile(String fn) throws ParserConfigurationException, SAXException, IOException, ParseException
	{
	  DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    doc = dBuilder.parse(new File(fn));
    doc.getDocumentElement().normalize();
    
    System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
    
    NodeList nList = doc.getElementsByTagName("Id");
    System.out.println("----------------------------");
    
    assert(nList.getLength() == 1);
    
    String x= nList.item(0).getChildNodes().item(0).getNodeValue();
    
    System.out.println(x);
    //                           "2016-08-15T14:43:59.000Z"
    Date d= sdf.parse(x);
    System.out.println(d);
    
    start= d.getTime();
	}
	
	private void preRead() throws InterruptedException {
		System.out.println("Start Sending");
		server.startSending();
		System.out.println("Start sensortime " + start);
		server.startSensorTime(start);

		System.out.println("Start Activity " + start);
		if( param2_hook != 0 )
    {
      int param1, param2;
      int activity= (activity_c / 100) % 10;
      param1= (activity_c / 10) % 10;
      param2= param2_hook;
      server.startActivityT(activity, param1, param2, start);
    }
		else
			server.startActivityT(activity_c, start);

		Thread.sleep(1000);
		
		List<ServerResponse> responses= null;
		responses= server.pollResonses();
		
		System.out.println("Respones: " + responses);
		for(ServerResponse r : responses)
		{
			System.out.println(r.getClass().getName());
		}
	}
	
	public void readFile() throws ClassNotFoundException, IOException, InterruptedException, ParseException
	{
	  double last_dist_m= 0;
	  long last_dist_t= -1;
	  double speed;
    NodeList points = doc.getElementsByTagName("Trackpoint");
	  for(int i= 0; i < points.getLength(); i++)
	  {
	    Element point= (Element) points.item(i);
	    
	    // Time
	    String x= point.getElementsByTagName("Time").item(0).getChildNodes().item(0).getNodeValue();
	    Date d= sdf.parse(x);
	    System.out.println(d + " " + d.getTime());
	    
	    if( point.getElementsByTagName("Position").getLength() > 0 )
      {
	      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
	      x= point.getElementsByTagName("LatitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
	      long lat = (long) (Double.parseDouble(x) * 100000000);
	      x= point.getElementsByTagName("LongitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
	      long lon = (long) (Double.parseDouble(x) * 100000000);
        // AltitudeMeters
	      x= point.getElementsByTagName("AltitudeMeters").item(0).getChildNodes().item(0).getNodeValue();
	      short alt = (short) (Double.parseDouble(x)*100);
	      
        System.out.println("has position " + x);
        sendGpsData(d.getTime(), d.getTime(), alt, (short) 0, (short) 0, (short) 0, lat, lon);
      }
      else
        System.out.println("No position");
      
	    if( point.getElementsByTagName("HeartRateBpm").getLength() > 0 )
      {
	      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
        x= point.getElementsByTagName("Value").item(0).getChildNodes().item(0).getNodeValue();
        System.out.println("has hr " + x);
        sendHrData(d.getTime(), d.getTime(), Integer.parseInt(x));
      }
      else
        System.out.println("No hr");
      
	    if( point.getElementsByTagName("DistanceMeters").getLength() > 0 )
      {
        // <DistanceMeters>9.137747022840712</DistanceMeters>
        x= point.getElementsByTagName("DistanceMeters").item(0).getChildNodes().item(0).getNodeValue();
        double dist= Double.parseDouble(x);
        if( last_dist_t != -1 )
          speed= (dist - last_dist_m)*1000/((double)(d.getTime() - last_dist_t))*1000;
        else
          speed= (dist - last_dist_m)*1000;
        System.out.println("has dist speed_mm_s:" + speed + " dist:" + dist + " " + (dist - last_dist_m) + " " + (d.getTime() - last_dist_t));
        last_dist_m= dist;
        last_dist_t= d.getTime();
        
        
        sendFootPodData(d.getTime(), d.getTime(), speed, dist, 0, 0);
      }
      else
        System.out.println("No dist");
      
      
	    /* <Time>2016-08-15T14:44:01.000Z</Time>
	    <Position><LatitudeDegrees>48.18989</LatitudeDegrees>
	    <LongitudeDegrees>16.35253</LongitudeDegrees></Position>
	    <AltitudeMeters>205.285</AltitudeMeters>
	    <DistanceMeters>9.137747022840712</DistanceMeters>
	    <HeartRateBpm><Value>126</Value></HeartRateBpm>*/
	  }
	}
	
	private void sendFootPodData(long t, long last_measurement, double fp_speed, double fp_distance, long fp_strides, long fp_calories)
	{
	  int packet_length= 9+ 8+2+2;
	  byte[] packet = new byte[packet_length];

    packet[0] = (byte) (packet_length & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((packet_length >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[2] = (byte) (2); // Sensortype
    packet[3] = (byte) (((1 >> 8 << 5)) + 1);
    packet[4] = (byte) (1 & 0xff); // Number of Sensor values in Package - LOW
    
    long ii = (t - start);
    packet[5] = (byte) (ii  & 0xFF);
    packet[6] = (byte) ((ii >> 8) & 0xFF);
    packet[7] = (byte) ((ii >> 16) & 0xFF);
    packet[8] = (byte) ((ii >> 24) & 0xFF);
    
    int i = (int) (t - last_measurement);
    packet[9] = (byte) (i & 0xFF); // counter
    packet[10] = (byte) ((i >> 8) & 0xFF); // counter
	  
    packet[11]=(byte) (fp_speed);     // SPEED_MM LOW
    packet[12]=(byte) (fp_speed/256); // SPEED_MM HIGH 
    packet[13]=(byte) (fp_distance);     // DIST_M 
    packet[14]=(byte) (fp_distance/256); // DIST_M HIGH 0....25500
    packet[15]=(byte) (fp_strides);     // STRIDES LOW 0....255
    packet[16]=(byte) (fp_strides/256); // STRIDES HIGH 0....255
    packet[17]=(byte) (fp_calories);   // CALORIES LOW
    packet[18]=(byte) (fp_calories/256);   // CALORIES HIGH
    
    packet[19] = (byte) (packet_length & 0xff);
    packet[20] = (byte) ((packet_length >> 8) & 0xff);
    
    Packet p= new Packet(packet, true);
    
    server.addSensorData(p);
	}
	
	private void sendHrData(long t, long last_measurement, int hr)
  {
    int packet_length= 9+ 11+2+2;
    byte[] packet = new byte[packet_length];

    packet[0] = (byte) (packet_length & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((packet_length >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[2] = (byte) (1); // Sensortype
    packet[3] = (byte) (((1 >> 8 << 5)) + 1);
    packet[4] = (byte) (1 & 0xff); // Number of Sensor values in Package - LOW
    
    long ii = (t - start);
    packet[5] = (byte) (ii  & 0xFF);
    packet[6] = (byte) ((ii >> 8) & 0xFF);
    packet[7] = (byte) ((ii >> 16) & 0xFF);
    packet[8] = (byte) ((ii >> 24) & 0xFF);
    
    int i = (int) (t - last_measurement);
    packet[9] = (byte) (i & 0xFF); // counter
    packet[10] = (byte) ((i >> 8) & 0xFF); // counter
    
    packet[11]=(byte) (hr);     // hr LOW
    packet[12]=(byte) 0; // hr HIGH 
    packet[13]=(byte) 0; // hrv_count
    packet[14]=(byte) 0;
    packet[15]=(byte) 0;
    packet[16]=(byte) 0;
    packet[17]=(byte) 0;
    packet[18]=(byte) 0;
    packet[19]=(byte) 0;
    packet[20]=(byte) 0;
    
    packet[21] = (byte) (packet_length & 0xff);
    packet[22] = (byte) ((packet_length >> 8) & 0xff);
    
    Packet p= new Packet(packet, true);
    
    server.addSensorData(p);
  }
	
	ByteBuffer buffer1= ByteBuffer.allocate(8);
  ByteBuffer buffer2= ByteBuffer.allocate(8);
	
	private void sendGpsData(long t, long last_measurement, short alt, short speed, short accur, short bearing, long lat, long lon)
  {
    int packet_length= 9+ 24+2+2;
    byte[] packet = new byte[packet_length];

    packet[0] = (byte) (packet_length & 0xff); // Number of bytes in Package LOW
    packet[1] = (byte) ((packet_length >> 8) & 0xff); // Number of bytes in Package HIGH
    packet[2] = (byte) (7); // Sensortype
    packet[3] = (byte) (((1 >> 8 << 5)) + 1);
    packet[4] = (byte) (1 & 0xff); // Number of Sensor values in Package - LOW
    
    long ii = (t - start);
    packet[5] = (byte) (ii  & 0xFF);
    packet[6] = (byte) ((ii >> 8) & 0xFF);
    packet[7] = (byte) ((ii >> 16) & 0xFF);
    packet[8] = (byte) ((ii >> 24) & 0xFF);
    
    int i = (int) (t - last_measurement);
    packet[9] = (byte) (i & 0xFF); // counter
    packet[10] = (byte) ((i >> 8) & 0xFF); // counter
    
    byte[] bytes1 = buffer1.putLong(0, lat).array();
    byte[] bytes2 = buffer2.putLong(0, lon).array();
    
    System.arraycopy(bytes1, 0, packet, 11, 8); // LAT
    System.arraycopy(bytes2, 0, packet, 19, 8); // LON

    packet[27]= (byte) (alt & 0xFF);
    packet[28]= (byte) ((alt >> 8) & 0xff);
    packet[29]= (byte) (speed & 0xFF);
    packet[30]= (byte) ((speed >> 8) & 0xff);
    packet[31]= (byte) (accur & 0xFF);
    packet[32]= (byte) ((accur >> 8) & 0xff);
    packet[33]= (byte) (bearing & 0xFF);
    packet[34]= (byte) ((bearing >> 8) & 0xff);
    
    packet[35] = (byte) (packet_length & 0xff);
    packet[36] = (byte) ((packet_length >> 8) & 0xff);
    
    Packet p= new Packet(packet, true);
    
    server.addSensorData(p);
  }
}
