package at.univie.mma;

import java.util.List;

import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.serverinterface.response.ServerResponse;
import at.univie.mma.serverinterface.sensors.FootPodSensor;
import at.univie.mma.serverinterface.sensors.GpsSensor;
import at.univie.mma.serverinterface.sensors.HRSensor;

public class RandomClient {
	public static void main(String args[]) throws InterruptedException
	{
		if (args.length != 4 && args.length != 5)
		{
			printUsage();
			return;
		}
		
		int param2_hook;
		double duration;
		
		if( args.length == 4 )
		{
			param2_hook= 0;
			duration= Double.parseDouble(args[3]);			
		}
		else
		{
			param2_hook= Integer.parseInt(args[3]);
			duration= Double.parseDouble(args[4]);
		}
		RandomClient f = new RandomClient(args[0], args[1], Integer.parseInt(args[2]), param2_hook);
		f.connect();
		f.setUpSensors();
		f.run(duration);
		System.out.println("Done Readining");
		f.close();
		System.out.println("Closed");
	}
	
	private static void printUsage()
	{
		System.out.println("MMAv3 RandomClient usage: RandomClient username password activity_c [param2] duration[sec]");
	}
	
	byte hr_register[];
	byte fp_register[];
	HRSensor sensor_hr;
	FootPodSensor sensor_fp;
	GpsSensor sensor_gps;
	ServerInterface server;
	
	String username;
	String password;
	int activity_c;
	int param2_hook;
	
	public RandomClient(String user, String pass, int activity_c, int param2_hook)
	{
		hr_register = new byte[11];
		fp_register = new byte[8];
		
		username = user;
		password = pass;
		this.activity_c = activity_c;
		this.param2_hook= param2_hook;
	}
	
	public void connect()
	{
		System.out.println("Connecting");
		server = ServerInterface.getInstance("131.130.176.217", 64111, 1000);
		server.connect();
		System.out.println("Logging in");
		server.login(username, password, "1", "123", ServerInterface.LANGUAGE_DE);
		System.out.println("Logged in");
	}
	
	private void close()
	{
		server.close();
	}
	
	public void setUpSensors()
	{
		sensor_hr = new HRSensor(1, 1);
		sensor_fp = new FootPodSensor(2, 1);
		sensor_gps= new GpsSensor(3,  1);
		
		server.setSensorCount(2);
		// ServerSensor ss= new ServerSensor(i, s.getSensorType(),
		// s.getDataSetsPerPacket());
		server.attachSensor(sensor_hr);
		server.attachSensor(sensor_fp);
	}
	
	public void run(double duration) throws InterruptedException
	{
		long starttime = System.currentTimeMillis();
		server.startSending();
		server.startSensorTime(starttime);

		int activity = (activity_c/100)%10;
		int param1 = (activity_c/10)%10;
		int param2 = activity_c%10;

		if( param2_hook != 0 )
			server.startActivity(activity, param1, param2_hook);
		else
			server.startActivity(activity, param1, param2);
		// server.restartSession();
		Thread.sleep(1000);
		duration-= 1;
		
		int last_dist= 0;
		int last_strides= 0;
		
		while (duration > 0)
		{
			int hr = (int) (Math.random() * 140 + 40);
			double speed_kmh = Math.random() * 20;
			int distance_m = (int) (Math.random() * 3);
			int strides = (int) (Math.random() * 3);
			
			short alt= (short) (Math.random() * 20 + 25800);
			short speed= (short) (Math.random() * 20 + 30);
			short accur= (short) (Math.random() * 20);
			short bearing= (short) (Math.random() * 360);
			long lat= (long) ((Math.random() + 48) * 100000000);
			long lon= (long) ((Math.random() + 16) * 100000000);
			
			distance_m+= last_dist;
			strides+= last_strides;
			last_dist= distance_m;
			last_strides= strides;
			
			System.out.println("HR: " + hr + " Speed_km/h: " + speed_kmh + " distance: " + distance_m + " strides:" + strides + " remaining:" + duration);
			
			int fp_speed = (int) (speed_kmh * 1000.0 / 3.6);
			// hr_register[]
			
			sensor_hr.setCurrentHR(hr);
			sensor_fp.setCurrentSpeed(fp_speed);
			sensor_fp.setDistance(distance_m);
			sensor_fp.setStrideCount(strides);
			sensor_gps.setData(alt, speed, accur, bearing, lat, lon);
			
			sensor_hr.sample();
			sensor_fp.sample();
			sensor_gps.sample();
			
			Thread.sleep(1000);
			duration-= 1;
			pollResponses();
		}
		
		System.out.println("Done uploading");
		server.stopSending(true);
		server.stopActivity();
	}
	
	private void pollResponses()
	{
		System.out.println("Start polling");
		List<ServerResponse> responses = server.pollResonses();
		for (ServerResponse r : responses)
		{
			System.out.println(r.getClass().getName() + " " + r.getResultCode() + " " + r.getRaw());
		}
		System.out.println("End polling");
	}
	
}
