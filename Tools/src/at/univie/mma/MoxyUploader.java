package at.univie.mma;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import at.univie.mma.serverinterface.ServerInterface;
import at.pegasos.serverinterface.response.ServerResponse;
import at.univie.mma.serverinterface.sensors.BikeNr;
import at.univie.mma.serverinterface.sensors.HRSensor;
import at.univie.mma.serverinterface.sensors.Moxy;

public class MoxyUploader {
	public static void main(String[] args) throws InterruptedException {
		if( args.length != 3 ) {
			printUsage();
			return;
		}

		MoxyUploader f= new MoxyUploader(args[0], args[1], 9, 6, 0);
		f.connect();
		/*if( args.length == 5 )
		{
		  f.setParam2(Integer.parseInt(args[3]));
		  f.readFile(args[4]);
		}
		else*/
		  f.readFile(args[2]);
		System.out.println("Done Readining");
		f.close();
		System.out.println("Closed");
	}
	
  private static void printUsage() {
		System.out.println("MMAv3 FileUploader usage: MoxyUploader username password filename");
	}
	
	byte hr_register[];
	byte fp_register[];
	Moxy sensor_moxy;
	BikeNr sensor_bike;
	HRSensor sensor_hr;
	ServerInterface server;
	
	String username;
	String password;
	final int param0;
	final int param1;
	final int param2;
	
	public MoxyUploader(String user, String pass, int param0, int param1, int param2) {
		hr_register= new byte[11];
		fp_register= new byte[8];
		
		username= user;
		password= pass;
		this.param0 = param0;
		this.param1 = param1;
		this.param2 = param2;
	}

	public void connect() {
		System.out.println("Connecting");
		server= ServerInterface.getInstance("131.130.176.217", 64322, 1000);
		server.connect();
		System.out.println("Logging in");
		server.login(username, password, "1", "123", ServerInterface.LANGUAGE_DE);
		System.out.println("Logged in");
	}
	
	private void close() {
		server.close();
	}
	
	public void setUpSensors(boolean hr) {
		sensor_moxy= new Moxy(1,1,1000);
		sensor_bike= new BikeNr(2, 1, 1000);
		if( hr )
		  sensor_hr= new HRSensor(3, 1);
		
		server.setSensorCount(2 + (hr ? 1 : 0));
		// ServerSensor ss= new ServerSensor(i, s.getSensorType(), s.getDataSetsPerPacket());
		server.attachSensor(sensor_moxy);
		server.attachSensor(sensor_bike);
		if( hr )
		  server.attachSensor(sensor_hr);
	}
	
	public void readFile(String filename) throws InterruptedException {
	  int linenr= 1;
		try(FileReader inputStream = new FileReader(filename)) {
			try(BufferedReader reader= new BufferedReader(inputStream)) {
				String line;
				
				// Read header
				line= reader.readLine();
				
				String[] header= line.split(";");
				linenr++;
		    
		    int sm_idx= -1, power_idx= -1, time_idx= -1, thb_idx= -1, hr_idx= -1;
		    boolean ms= false;
		    
		    for(int i= 0; i < header.length; i++)
		    {
		      String help= header[i].toLowerCase();
		      if( help.equals("smo2") || help.equals("smo2 averaged") )
		        sm_idx= i;
		      else if( help.equals("target power") || help.equals("pm watts") || help.equals("power") )
		        power_idx= i;
		      else if( help.equals("hh:mm:ss") )
		        time_idx= i;
		      else if( help.equals("ms") )
		      {
		        time_idx= i;
		        ms= true;
		      }
		      else if( help.equals("thb") )
		        thb_idx= i;
		      else if( help.startsWith("hr") || help.startsWith("heart") )
            hr_idx= i;
		    }
		    
		    System.out.println(time_idx + " " + power_idx + " " + sm_idx + " " + ms + " " + thb_idx + " " + hr_idx);
		    
		    boolean has_hr= hr_idx != -1;
		    
		    setUpSensors(has_hr);
		    
		    long starttime = System.currentTimeMillis();
  	    server.startSending();
	      server.startSensorTime(starttime);
	      server.startActivityT(param0, param1, param2, starttime);
	      // server.restartSession();
	      Thread.sleep(1000);
        
			    while( (line= reader.readLine()) != null)
			    {
			      linenr++;
			       String data[]= line.split(";");
			       /*System.out.print("{");
			       for(String s : data )
			    	   System.out.print(s + " ");
			       System.out.println("}");*/
			       
			       int smo2= Integer.parseInt(data[sm_idx]);
			       int thb= (int) (Double.parseDouble(data[thb_idx].replace(',','.')) * 100);
			       int power= (int) (Double.parseDouble(data[power_idx].replace(',','.'))); // Integer.parseInt(data[power_idx]);
			       int hr= 0;
			       if(has_hr)
			         hr= Integer.parseInt(data[hr_idx]);
			       
			       System.out.println(linenr + " HR: " + hr + " Power: " + power + " sm02: " + smo2 + " thb:" + thb);
			       
			       
			       /*sensor_moxy.setCurrentHR(hr);
			       sensor_bike.setCurrentSpeed(fp_speed);
			       sensor_bike.setDistance(distance_m);
			       sensor_bike.setStrideCount(strides);*/
			       sensor_moxy.setData(smo2, thb);
			       sensor_bike.setPower((short)power);
			       sensor_moxy.sample();
             sensor_bike.sample();
             
			       if( has_hr )
			       {
			         sensor_hr.setCurrentHR(hr);
			         sensor_hr.sample();
			       }
			       
			       Thread.sleep(1000);
			       pollResponses();
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Done uploading");
		server.stopSending(true);
		server.stopActivity();
	}
	
	private void pollResponses() {
		System.out.println("Start polling");
		List<ServerResponse> responses = server.pollResonses();
		for(ServerResponse r: responses)
		{
			System.out.println(r.getClass().getName() + " " + r.getResultCode() + " " + r.getRaw());
		}
		System.out.println("End polling");
	}
	
}
