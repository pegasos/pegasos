package at.pegasos.tool;

import at.pegasos.serverinterface.tools.TimedData;
import at.univie.mma.serverinterface.tools.SensorDataParser;

import java.io.*;
import java.util.*;

public class CSVConverter {
  public static void main(String[] args) throws IOException
  {
    SensorDataParser parser= new SensorDataParser(new File(args[0]));
    parser.parse();
    List<TimedData> data= parser.getData();
    for(TimedData d : data)
    {
      System.out.println(d.time + ";" + d.type + ";" + d.sensornr + ";" + d.data);
    }
  }
}
