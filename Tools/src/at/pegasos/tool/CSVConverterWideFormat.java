package at.pegasos.tool;

import at.pegasos.serverinterface.tools.TimedData;
import at.univie.mma.serverinterface.tools.SensorDataParser;

import java.io.*;
import java.util.*;

/**
 * Converts an ser file to a csv table.
 * 
 *  <BR/><BR/>Known issues<ul>
 *  <li>when two or more packets are recorded with the exact same time and type they will be printed as if they are one packet</li>
 *  </ul>
 */
public class CSVConverterWideFormat {
  public static void main(String[] args) throws IOException
  {
    SensorDataParser parser= new SensorDataParser(new File(args[0]));
    parser.parse();
    List<TimedData> data= parser.getData();
    
    String last_type= "";
    long last_time= -1;
    for(TimedData d : data)
    {
      String type = d.type.split("[.]")[0];
      // new row
      if( d.time != last_time || !last_type.equals(type) )
      {
        last_time = d.time;
        last_type = type;
        // end previous line
        System.out.println("");
        System.out.print(d.time + ";" + type + ";" + d.sensornr);
      }
      System.out.print(";" + d.data);
    }
    // end line
    System.out.println("");
  }
}
