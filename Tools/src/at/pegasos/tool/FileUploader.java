package at.pegasos.tool;

import at.pegasos.serverinterface.response.*;
import at.pegasos.tool.MessageListener.*;
import at.univie.mma.serverinterface.command.FileUpload;
import com.beust.jcommander.*;

import java.io.*;
import java.util.*;

public class FileUploader extends Tool implements IMessageListener {

  public final static String UploadCreated= "FileUploader.UploadCreated";
  public final static String ChunkReceived= "FileUploader.ChunkReceived";
  // public final static String FileReadStarted= "FileUploader.FileReadStarted";
  // public final static String FileReadStopped= "FileUploader.FileReadStopped";
  public final static String FileReadFinished= "FileUploader.FileReadFinished";

  private static final int CHUNKSIZE= 1024;

  public static class Params extends Parameters {
    @Parameter(names= {"--filename", "-fn"}, description= "Filename", required= true)
    public String filename;
  };

  protected Params parameters;

  public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException
  {
    FileUploader uploader= new FileUploader(args);

    uploader.run();
  }

  public FileUploader(String[] args)
  {
    parameters= new Params();
    new JCommander(parameters, args);
  }

  public FileUploader(Params params)
  {
    parameters= params;
  }

  public void run() throws InterruptedException, ClassNotFoundException, IOException
  {
    if( !getParams().simulate )
      connect();

    if( !getParams().simulate )
      preFile();

    upload();

    if( !getParams().simulate )
    {
      fetchNprintResponses(false);

      System.out.println("Close server");

      close();
    }

    System.out.println("Upload done");
  }

  public void upload() throws ClassNotFoundException, IOException, InterruptedException
  {
    readFile(getParams().filename);

    System.out.println("Read File done");
    if( !getParams().simulate )
      server.heartbeat();
  }

  @Override
  protected Params getParams()
  {
    return parameters;
  }

  private void preFile() throws InterruptedException
  {
    System.out.println("Start Sending");
    server.startSending();
    // System.out.println("Start sensortime " + start);
    // server.startSensorTime(start);

    // System.out.println("Start Activity " + start);
    // server.startActivityT(getParams().param0, getParams().param1, getParams().param2, start);

    // Thread.sleep(1000);

    List<ServerResponse> responses= null;
    responses= server.pollResonses();

    System.out.println("Respones: " + responses);
    for(ServerResponse r : responses)
    {
      System.out.println(r.getClass().getName());
    }
  }

  /**
   * ID of the created Upload. Value is returned from server
   */
  private long uploadId;

  /**
   * Number of chunks to be uploaded
   */
  private int nchunks;

  private FileInputStream in;

  private String filePath;
  private String fileName;

  private final Object waiter = new Object();
  
  private boolean finished = false;

  private void readFile(String filename) throws ClassNotFoundException, IOException, InterruptedException
  {
    listener.addListener(this);

    startPolling();

    System.out.print("ReadFile " + filename);
    in = new FileInputStream(new File(filename));
    nchunks = (int) Math.ceil(in.getChannel().size() / (double) CHUNKSIZE);
    System.out.println(" #nChunks: " + nchunks);

    uploadId = 0;

    this.filePath = filename;

    createUpload();
    System.out.println("Upload created");

    synchronized( waiter )
    {
      while( !finished )
      {
        waiter.wait();
      }
    }

    stopPolling();
    System.out.println("Polling stopped");

    listener.removeListener(this);
  }

  private void createUpload() throws InterruptedException
  {
    // fileName= Paths.get(filePath).toFile().getName();
    fileName= new File(filePath).getName();

    server.sendCommand(FileUpload.createUpload(fileName, nchunks));
  }

  private void uploadChunks() throws IOException
  {
    byte[] buffer= new byte[CHUNKSIZE];

    int chunknr= 0;
    while( chunknr < nchunks )
    {
      int count= in.read(buffer);
      System.out.println("Read " + count);

      if( count == CHUNKSIZE )
        server.sendCommand(FileUpload.sendDataChunk(uploadId, chunknr, buffer));
      else
      {
        byte[] buffer2= new byte[count];
        System.arraycopy(buffer, 0, buffer2, 0, count);
        server.sendCommand(FileUpload.sendDataChunk(uploadId, chunknr, buffer2));
      }

      chunknr++;
    }
  }

  @Override
  public boolean message(String message, Object... params)
  {
    if( message.equals(MessageListener.SERVERRESPONSE) )
    {
      ServerResponse r= (ServerResponse) params[0];
      if( r instanceof at.univie.mma.serverinterface.response.FileUpload )
      {
        at.univie.mma.serverinterface.response.FileUpload rr= (at.univie.mma.serverinterface.response.FileUpload) r;

        switch( rr.getType() )
        {
          case CreateUpload:
            if( rr.getData()[1].equals(fileName) )
            {
              uploadId= Long.parseLong(rr.getData()[0]);
              listener.message(UploadCreated, fileName, uploadId, nchunks);

              try
              {
                onUploadIdReceived();
              }
              catch( IOException e )
              {
                e.printStackTrace();
              }

              return true;
            }
            else
            {
              System.err.println("Filename missmatch: " + rr.getData()[1]);
            }
            break;

          case DataChunk:
            // uploadId, chunkNr
            listener.message(ChunkReceived, Long.parseLong(rr.getData()[0]), Long.parseLong(rr.getData()[1]));
            break;

          case FinishUpload:
            System.out.println("Fileupload done ...");
            listener.message(FileReadFinished, Long.parseLong(rr.getData()[0]), Long.parseLong(rr.getData()[1]));
            finished = true;
            synchronized( waiter )
            {
              waiter.notifyAll();
            }
            return true;

          default:
            break;
        }
      }
    }
    else if( message.equals(UploadCreated) )
    {
      String fn= (String) params[0];
      long id= (Long) params[1];
      int nchunks= (Integer) params[2];

      System.out.println("Upload created: " + fn + " " + id + " " + nchunks);
    }
    else if( message.equals(ChunkReceived) )
    {
      long id= (Long) params[0];
      long chunkId= (Long) params[1];

      System.out.println("Chunk received: " + id + " " + chunkId);
    }

    return false;
  }

  private void onUploadIdReceived() throws IOException
  {
    if( uploadId == 0 )
    {
      System.out.println("Failed");
      in.close();
      return;
    }

    uploadChunks();

    in.close();

    String fn = fileName.substring(0, fileName.length()-4);
    System.out.println(fn);
    File file = new File(filePath.substring(0, filePath.length()-4) + ".meta");
    if( file.exists() )
    {
      try
      {
        BufferedReader in = new BufferedReader(new FileReader(file));
        ArrayList<String> lines= new ArrayList<String>();
        String line;
        while( (line= in.readLine()) != null )
        {
          lines.add(line);
        }
        in.close();

        StringBuilder meta = new StringBuilder();
        for(String ll : lines)
        {
          meta.append(ll).append("\n");
        }
        System.out.println("Finish file upload " + meta);
        server.sendCommand(FileUpload.finishUpload(uploadId, meta.toString()));
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    else
    {
      System.out.println("No meta exists");
      server.sendCommand(FileUpload.finishUpload(uploadId));
    }

    synchronized( waiter )
    {
      waiter.notifyAll();
    }
  }
}
