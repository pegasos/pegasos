package at.pegasos.tool.frontend;

import javafx.application.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class GUI extends Application {
  @Override
  public void start(Stage primaryStage) throws Exception
  {
    StackPane root= new StackPane();
    root.getChildren().add(new FileEditor());

    Scene scene= new Scene(root, 500, 250);

    primaryStage.setTitle("Pegasos Tool GUI");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args)
  {
    launch(args);
  }
}
