package at.pegasos.tool.frontend;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import at.pegasos.serverinterface.*;
import at.pegasos.tool.*;
import at.pegasos.tool.frontend.buttons.*;
import at.univie.mma.serverinterface.tools.*;
import javafx.collections.*;
import javafx.collections.transformation.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.fxml.*;
import javafx.stage.*;

public class FileEditor extends VBox implements Initializable {

  private final String ALL_PACKETS = "All";

  private int selectedIndex;
  private ObservableList<Packet> items;
  // private Path fileName = Paths.get("/tmp/7636.ser");
  private Path fileName;

  @SuppressWarnings("unused") @FXML private RemoveButton removeButton;
  @SuppressWarnings("unused") @FXML private InsertAboveButton insertAboveButton;
  @SuppressWarnings("unused") @FXML private InsertBelowButton insertBelowButton;
  @SuppressWarnings("unused") @FXML private SaveButton saveButton;
  @SuppressWarnings("unused") @FXML private ComboBox<String> filterPacket;
  @SuppressWarnings("unused") @FXML Label fileNameLabel;
  @SuppressWarnings("unused") @FXML private Button openFileButton;

  private final ListView<Packet> list = new ListView<>();

  private boolean ignoreTextAreaUpdate = false;

  TextArea textArea;
  Label dataDisplay;
  private FilteredList<Packet> filteredData;

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle)
  {
    textArea = new TextArea();
    dataDisplay = new Label();

    setupListView();

    filterPacket.getSelectionModel().selectedItemProperty().addListener((ignored, old_val, newValue) -> {
      if( newValue == ALL_PACKETS )
        filteredData.setPredicate(s -> true);
      else
      {
        byte type = Byte.parseByte(newValue.substring(6));
        filteredData.setPredicate(packet -> {
          byte[] bytes = packet.getBytes();
          if( bytes.length < 2 )
            return true;
          else
            return bytes[2] == type;
        });
      }
    });

    textArea.textProperty().addListener((observableValue, oldValue, newValue) -> {
      System.out.println("New value: " + newValue + " index:" + selectedIndex);
      if( newValue.equals("") )
      {
        dataDisplay.setText("Invalid");
      }
      else
      {
        updatePacketFromTextArea();
      }
    });

    textArea.wrapTextProperty().set(true);
    dataDisplay.wrapTextProperty().set(true);

    textArea.setPrefWidth(450);
    textArea.setPrefHeight(200);
    dataDisplay.setPrefWidth(450);
    dataDisplay.setPrefHeight(300);

    removeButton.setOnAction(actionEvent -> {
      items.remove(list.getSelectionModel().getSelectedItem());
    });
    insertAboveButton.setOnAction(actionEvent -> {
      Packet p = new Packet(new byte[0], true);
      items.add(selectedIndex, p);
      list.getSelectionModel().select(selectedIndex+1);
    });
    insertBelowButton.setOnAction(actionEvent -> {
      Packet p = new Packet(new byte[0], true);
      items.add(selectedIndex+1, p);
      list.getSelectionModel().select(selectedIndex+1);
    });
    saveButton.setOnAction(actionEvent -> save());
    openFileButton.setOnAction(actionEvent -> {
      final FileChooser fileChooser = new FileChooser();
      File file = fileChooser.showOpenDialog(this.getScene().getWindow());
      if (file != null) {
        fileName = file.toPath();
        openFile();
      }
    });

    HBox hbox = new HBox();
    VBox vBox = new VBox();
    vBox.getChildren().addAll(textArea,dataDisplay);
    hbox.getChildren().addAll(list, vBox);
    this.getChildren().addAll(hbox);
  }

  private void openFile()
  {
    Set<Byte> packetTypes;
    fileNameLabel.setText(fileName.toString());

    try
    {
      items = FXCollections.observableArrayList();
      items.addAll(PacketReader.readFile(fileName.toAbsolutePath().toString()));
      packetTypes = items.stream().map(packet -> packet.getBytes()[2]).collect(Collectors.toSet());
      filteredData = new FilteredList<Packet>(items, s -> true);
    }
    catch( IOException e )
    {
      throw new RuntimeException(e);
    }

    filterPacket.getItems().clear();
    filterPacket.getItems().add(ALL_PACKETS);
    filterPacket.getItems().addAll(packetTypes.stream().map(b ->"Type: " + b).collect(Collectors.toSet()));
    list.setItems(filteredData);
  }

  private void setupListView()
  {
    list.setItems(filteredData);
    list.setCellFactory(packetListView -> new PacketCell());
    list.setPrefWidth(330);
    list.getSelectionModel().selectedItemProperty().addListener((ignored, old_val, newValue) -> {
      FileEditor.this.selectedIndex = list.getSelectionModel().getSelectedIndex();

      byte[] bytes = newValue != null ? newValue.getBytes() : new byte[0];
      StringBuilder stringBuilder = new StringBuilder();
      for(int i= 0; i < bytes.length; i++)
      {
        if( i > 0 )
          stringBuilder.append(" ");
        stringBuilder.append(String.format("%02X", bytes[i]));
      }
      ignoreTextAreaUpdate = true;
      textArea.setText(stringBuilder.toString());
      try
      {
        if( newValue != null )
          dataDisplay.setText(PacketConverter.convert(newValue).toString());
        else
          dataDisplay.setText("Invalid");
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
        dataDisplay.setText("Invalid");
      }
    });
  }

  static class PacketCell extends ListCell<Packet> {
    @Override
    public void updateItem(Packet item, boolean empty) {
      super.updateItem(item, empty);

      if (item != null)
      {
        showText(item);
      }
    }

    private void showText(Packet item)
    {
      final byte[] bytes = item.getBytes();
      if( bytes.length > 2 )
      {
        byte type = bytes[2];
        setText(String.format("%08d", item.getTimeSinceStart()) + " " + type);
      }
      else
      {
        setText("");
      }
    }

    @Override
    public void updateSelected(boolean selected)
    {
      super.updateSelected(selected);
      if (!isEmpty())
      {
        showText(getItem());
      }
    }
  }

  public FileEditor()
  {
    try
    {
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(this.getClass().getResource(getClass().getSimpleName() + ".fxml"));
      // loader.setResources(ResourceBundle.getBundle("vcpx.analysis.lang.AnalysisPane", Settings.getInstance().getLocale()));
      loader.setRoot(this);

      loader.setController(this);

      loader.load();
    }
    catch( IOException | IllegalArgumentException | SecurityException e )
    {
      e.printStackTrace();
    }
  }

  private void updatePacketFromTextArea()
  {
    if( ignoreTextAreaUpdate )
    {
      ignoreTextAreaUpdate = false;
      return;
    }

    String newValue = textArea.textProperty().getValue();
    try
    {
      Byte[] Bytes = Arrays.stream(newValue.split(" ")).map(s -> (byte) Integer.parseInt(s.toLowerCase(), 16)).toArray(Byte[]::new);
      byte[] bytes = new byte[Bytes.length];
      int i = 0;
      for(Byte b : Bytes)
        bytes[i++] = b;
      Packet p = new Packet(bytes, true);
      dataDisplay.setText(PacketConverter.convert(p).toString());
      items.set(selectedIndex, p);
    }
    catch( NumberFormatException | ArrayIndexOutOfBoundsException e )
    {
      dataDisplay.setText("Invalid");
    }
  }

  private void save()
  {
    try(OutputStream out =Files.newOutputStream(fileName))
    {
      for(Packet p : items)
      {
        out.write(p.getBytes());
      }
    }
    catch( IOException e )
    {
      throw new RuntimeException(e);
    }
  }
}
