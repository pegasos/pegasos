package at.pegasos.tool.frontend.buttons;

import java.util.*;

import javafx.event.*;
import javafx.scene.control.*;
import javafx.scene.image.*;

public class SaveButton extends Button {
  ResourceBundle res;

  public SaveButton()
  {
    // this.setText(res.getString("remove"));
    this.setGraphic(new ImageView("at/pegasos/tool/frontend/gfx/save.png"));

    // getStyleClass().add("icon-remove");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip("remove");
    setTooltip(tooltip);
  }

  public SaveButton(EventHandler<ActionEvent> event)
  {
    // this.setText(res.getString("remove"));
    this.setGraphic(new ImageView("at/pegasos/tool/frontend/gfx/save.png"));

    // getStyleClass().add("icon-remove");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip("remove");
    setTooltip(tooltip);

    setOnAction(event);
  }
}
