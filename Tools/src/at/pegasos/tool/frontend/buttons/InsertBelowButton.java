package at.pegasos.tool.frontend.buttons;

import java.util.*;

import javafx.event.*;
import javafx.scene.control.*;
import javafx.scene.image.*;

public class InsertBelowButton extends Button {
  ResourceBundle res;


  /**
   * Initializes the class.
   */
  public InsertBelowButton()
  {
    // this.res = ResourceBundle.getBundle("vcpx.utility.dialog.lang.Dialog", Settings.getInstance().getLocale());
    // this.res = ResourceBundle.getBundle("at.pegasos.tool.frontend");

    // this.setText(res.getString("remove"));
    this.setGraphic(new ImageView("at/pegasos/tool/frontend/gfx/insert-below.png"));
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip("remove");
    setTooltip(tooltip);
  }

  public InsertBelowButton(EventHandler<ActionEvent> event)
  {
    // this.setText(res.getString("remove"));
    this.setGraphic(new ImageView("at/pegasos/tool/frontend/gfx/insert-below.png"));

    // getStyleClass().add("icon-remove");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    Tooltip tooltip = new Tooltip("remove");
    setTooltip(tooltip);

    setOnAction(event);
  }
}
