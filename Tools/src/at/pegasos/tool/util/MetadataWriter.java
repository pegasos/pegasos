package at.pegasos.tool.util;

import java.io.*;
import java.util.*;

public class MetadataWriter {

  private final String fileName;
  private final Metadata data;

  public MetadataWriter(Metadata data, String fileName)
  {
    this.data = data;
    this.fileName = fileName;
  }

  public void write() throws IOException
  {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    for(Map.Entry<String, String> kv : data.getValues())
    {
      writer.write(kv.getKey() + ":" + kv.getValue() + "\n");
    }
    writer.close();
  }
}