package at.pegasos.tool;

public class MessageListener {
  
  public static final String SERVERRESPONSE= "serverresponse";
  
  public interface IMessageListener {
    public boolean message(String message, Object ...params);
  }
  
  private IMessageListener[] listeners;
  
  public MessageListener()
  {
    listeners= new IMessageListener[0];
  }
  
  public void addListener(IMessageListener listener)
  {
    IMessageListener[] tmp= new IMessageListener[listeners.length + 1];
    System.arraycopy(listeners, 0, tmp, 0, listeners.length);
    tmp[listeners.length]= listener;
    listeners= tmp;
  }
  
  public void removeListener(IMessageListener listener)
  {
    IMessageListener[] tmp= new IMessageListener[listeners.length - 1];
    int pos= 0;
    for(IMessageListener list : listeners)
    {
      if( list == listener )
        break;
      pos++;
    }
    
    System.arraycopy(listeners, 0, tmp, 0, pos);
    System.arraycopy(listeners, pos + 1, tmp, pos, listeners.length - pos - 1);
    
    listeners= tmp;
  }
  
  public boolean message(String msg, Object... params)
  {
    for(IMessageListener listener : listeners)
    {
      if( listener.message(msg, params) )
        return true;
    }
    return false;
  }
}
