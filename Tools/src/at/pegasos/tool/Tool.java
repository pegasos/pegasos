package at.pegasos.tool;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.beust.jcommander.Parameter;

import at.univie.mma.serverinterface.ResponseCallback;
import at.univie.mma.serverinterface.ServerInterface;
import at.univie.mma.serverinterface.response.*;
import at.pegasos.serverinterface.response.*;
import at.pegasos.serverinterface.response.ServerResponse;

public abstract class Tool implements ResponseCallback {
  public static class Parameters {
    @Parameter(names= "--server", description= "Server name")
    public String server;
    
    @Parameter(names= "--server-ip", description= "Server ip")
    public String server_ip= ""; // TODO: Config.SERVER_IP;
    
    @Parameter(names= "--server-port", description= "Server port")
    public int server_port= 0; // TODO: Config.PORT;
    
    @Parameter(names= "--user", description= "Username")
    public String user;
    
    @Parameter(names= "--password", description= "Password")
    public String password;
    
    @Parameter(names= "--no-team", description= "Do not login as team / Login as private")
    public boolean no_team= false;
    
    @Parameter(names= "--simulate", description= "Simulate")
    public boolean simulate= false;
  }
  
  protected ServerInterface server;
  
  protected MessageListener listener= new MessageListener();
  
  private Timer responsePoller;
  
  protected abstract Parameters getParams();
  
  protected void connect()
  {
    System.out.println("Connecting");
    
    server= ServerInterface.getInstance(getParams().server_ip, getParams().server_port, 1000);
    
    if( !server.connect() )
    {
      System.err.println("Connecting to server failed. Exiting");
      System.exit(1);
    }
    System.out.println("Logging in");
    String club= getParams().no_team ? "0" : "1";
    server.login(getParams().user, getParams().password, club, "0", ServerInterface.LANGUAGE_DE);
    
    List<ServerResponse> responses= null;
    while( responses == null || responses.size() == 0 )
      responses= server.pollResonses();
    
    boolean loggedin= false;
    System.out.println("Respones: " + responses);
    for(ServerResponse r : responses)
    {
      if( r instanceof LoginMessage )
      {
        if( r.getResultCode() == ServerResponse.RESULT_OK )
        {
          loggedin= true;
        }
        else
        {
          System.err.println("Logging in failed.\n" + r.getRaw() + "\nExiting");
          System.exit(1);
        }
      }
    }
    
    if( loggedin == false )
    {
      System.err.println("Logging in failed. Exiting");
      System.exit(1);
    }
  }
  
  protected void close(long endtime) throws InterruptedException
  {
    close(endtime, false);
  }
  
  protected void close(long endtime, boolean confirm) throws InterruptedException
  {
    server.stopSending(true);
    
    fetchNprintResponses(false);
    
    server.stopActivity(endtime);
    server.singleSend();
    
    fetchNprintResponses(confirm);
    
    Thread.sleep(1000);
    
    server.close();
  }
  
  protected void close()
  {
    server.stopSending(true);
    
    fetchNprintResponses(false);
    
    server.close();
  }
  
  protected void fetchNprintResponses(boolean wait_for_stop)
  {
    List<ServerResponse> responses= null;
    int tries= 0;
    while( (responses == null || responses.size() == 0) && tries < 2 )
    {
      responses= server.pollResonses();
      tries++;
      try
      {
        Thread.sleep(250);
      }
      catch( InterruptedException e )
      {
        
      }
    }
    
    boolean stop= false;
    System.out.println("Respones: " + responses);
    for(ServerResponse r : responses)
    {
      System.out.println(r.getClass().getName());
      if( r instanceof ActivityStoppedMessage )
        stop= true;
      printResponse(r);
    }
    
    if( wait_for_stop && stop == false )
    {
      System.out.println("Waiting for Server to confirm stop of activity");
      fetchNprintResponses(wait_for_stop);
    }
  }
  
  protected boolean printResponse(ServerResponse r)
  {
    if( r instanceof FeedbackMessage )
    {
      System.out.println("Feedback: " + ((FeedbackMessage) r).getMessage());
    }
    else if( r instanceof StartSessionMessage )
    {
      System.out.println("Message: StartSessionMessage");
    }
    else if( r instanceof LoginMessage )
    {
      System.out.println("LoginMessage recieved");
    }
    else if( r instanceof ActivityStoppedMessage )
    {
      // TODO: handle this
      System.out.println("ActivityStopped received");
    }
    else if( r instanceof ServerDisconnectedMessage )
    {
      System.out.println("ServerDisconnected received");
    }
    else if( r instanceof ServerReconnected )
    {
      System.out.println("ServerReconnected received");
    }
    else if( r instanceof RestartSessionMessage )
    {
      System.out.println("RestartSessionMessage received");
    }
    else if( r instanceof ControlCommand )
    {
      ControlCommand command= (ControlCommand) r;
      if( command.getType() == ControlCommand.TYPE_LOGIN_REQUIRED )
      {
        System.out.println("ControlCommand LOGIN_REQUIRED");
      }
      else if( command.getType() == ControlCommand.TYPE_SESSION_REQUIRED )
      {
        System.out.println("ControlCommand SESSION_REQUIRED");
      }
    }
    else
    {
      System.out.println("<" + r.getClass() + ">");
      return false;
    }
    
    return true;
  }
  
  public void setListener(MessageListener listener)
  {
    this.listener= listener;
  }
  
  public MessageListener getListener()
  {
    return listener;
  }
  
  protected void startPolling()
  {
    responsePoller= new Timer();
    responsePoller.schedule(new TimerTask() {
      
      @Override
      public void run()
      {
        pollResponses();
      }
    }, 1000, 1000);
  }
  
  protected void stopPolling()
  {
    responsePoller.cancel();
    responsePoller= null;
  }

  protected void pollResponses()
  {
    List<ServerResponse> responses= null;
    
    responses= server.pollResonses();
    System.out.println("Respones: " + responses);
    
    for(ServerResponse r : responses)
    {
      System.out.println(r.getRaw() + " " + r.getResultCode());
      
      listener.message(MessageListener.SERVERRESPONSE, r);
    }
  }

  public void setSI(ServerInterface instance)
  {
    this.server= instance;
  }
  
  public void registerAsListener()
  {
    this.server.addResponseCallback(this);
  }
  
  @Override
  public void messageRecieved(ServerResponse r)
  {
    listener.message(MessageListener.SERVERRESPONSE, r);
  }
  
  public void unregisterAsListener()
  {
    this.server.removeResponseCallback(this);
  }
}
