package at.pegasos.tool;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.pegasos.tool.util.Metadata;
import at.pegasos.tool.util.MetadataParser;

public class FolderUploader extends Tool
{
  public static class Params extends Parameters
  {
    @Parameter(names= {"--directory", "-d"}, description= "Directory", required= true)
    public String dir;
  };
  
  private static class Entry {
    public Path file;
    public Metadata meta;
  }
  
  protected Params parameters;
  
  protected List<Entry> files;
  
  public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException
  {
    FolderUploader uploader= new FolderUploader(args);
    
    uploader.run();
  }
  
  public FolderUploader(String[] args)
  {
    parameters= new Params();
    new JCommander(parameters, args);
    files= new ArrayList<Entry>();
  }
  
  @Override
  protected Params getParams()
  {
    return parameters;
  }
  
  public void run()
  {
    fetchFiles();
    
    files.sort(new Comparator<Entry>() {

      @Override
      public int compare(Entry o1, Entry o2)
      {
        return (int) (Long.parseLong(o1.meta.getValue("Starttime")) - Long.parseLong(o2.meta.getValue("Starttime")));
      }
    });
    
    for(Entry file : files)
    {
      try
      {
        uploadFile(file);
      }
      catch( ClassNotFoundException | InterruptedException | IOException e )
      {
        e.printStackTrace();
      }
    }
  }
  
  private void uploadFile(Entry file) throws ClassNotFoundException, InterruptedException, IOException
  {
    PacketUploader.Params p= new PacketUploader.Params();
    p.param0= Integer.parseInt(file.meta.getValue("Param0"));
    p.param1= Integer.parseInt(file.meta.getValue("Param1"));
    p.param2= Integer.parseInt(file.meta.getValue("Param2"));
    p.simulate= getParams().simulate;
    p.simulate= false;
    p.user= file.meta.getValue("User");
    p.filename= file.file.toAbsolutePath().toString();
    p.server_ip= getParams().server_ip;
    p.server_port= getParams().server_port;
    p.password= "hgm4bt";
    
    PacketUploader uploader= new PacketUploader(p);
    uploader.start= Long.parseLong(file.meta.getValue("Starttime"));
    uploader.run();
  }
  
  public void fetchFiles()
  {
    final File folder= new File(parameters.dir);
    for(final File fileEntry : folder.listFiles())
    {
      if( !fileEntry.isDirectory() && fileEntry.getName().toLowerCase().endsWith(".ser") )
      {
        Path path= Paths.get(fileEntry.getAbsolutePath());
        Path parent= path.toAbsolutePath().getParent();
        String fn= path.getFileName().toString();
        fn= fn.substring(0, fn.length()-4);
        
        if( Files.exists(parent.resolve(fn + ".meta")) )
        {
          // System.out.println(fileEntry + " has meta");
          try
          {
            Metadata meta= MetadataParser.parse(Files.readAllLines(parent.resolve(fn + ".meta")));
            if( checkMeta(meta) )
              addFile(path, meta);
            else
              System.err.println("Could not add file " + fileEntry + " due to error in metadata");
          }
          catch( IOException e )
          {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
        else
          System.out.println(fileEntry + " has no meta");
      }
    }
  }
  
  private void addFile(Path path, Metadata meta)
  {
    Entry e= new Entry();
    e.file= path;
    e.meta= meta;
    files.add(e);
  }

  private static boolean checkMeta(Metadata meta)
  {
    return meta.hasKey("Param0") && meta.hasKey("Param1") && meta.hasKey("Param2") && meta.hasKey("User");
  }
}
