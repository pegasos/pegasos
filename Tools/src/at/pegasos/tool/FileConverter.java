package at.pegasos.tool;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;

public class FileConverter {

  private final Class<?> outPacket;
  private final Constructor<?> newOutPacket;
  private final String outJarName;
  private final String inJarFile;
  private final String outDataName;
  private Class<?> inPacket;
  private Field getTime;
  private Field getType;
  private Field getData;
  private Field getSensornr;
  private Method getBytes;

  private final File inputDataFile;
  private final Map<Byte, Mapping> mappingMap;
  /**
   * ClassLoader for the output
   */
  private final URLClassLoader outJar;

  final Object outInterface;

  private Method converter;

  private static class Mapping {

  }

  private class MapData extends Mapping {
    final String outClass;
    final Map<String, MethodMapping> methodsMapping = new HashMap<>();
    public Class<?> clasz;

    public class MethodMapping {
      final String name;
      final Class<?>[] types;
      Method method;

      public MethodMapping(String name, Class<?>[] types)
      {
        this.name = name;
        this.types = types;
      }

      public Object[] convert(List<Object> data) throws IllegalAccessException
      {
        Object[] ret = new Object[types.length+1];
        int i = 0;
        for(Class<?> type : types)
        {
          if( type == byte.class || type == Byte.class )
          {
            // ret[i] = Byte.parseByte(data.get(i).data);
            ret[i] = Byte.parseByte((String) getData.get(data.get(i)));
          }
          else if( type == short.class || type == Short.class )
          {
            ret[i] = Short.parseShort((String) getData.get(data.get(i)));
          }
          else if( type == int.class || type == Integer.class )
          {
            ret[i] = Integer.parseInt((String) getData.get(data.get(i)));
          }
          else if( type == long.class || type == Long.class )
          {
            ret[i] = Long.parseLong((String) getData.get(data.get(i)));
          }
          else if( type == double.class || type == Double.class )
          {
            ret[i] = Double.parseDouble((String) getData.get(data.get(i)));
          }
          i++;
        }
        return ret;
      }
    }

    private MapData(String outClass)
    {
      this.outClass = outClass;
    }

    public void addMethodMapping(String inMethod, String outMethod, Class<?>... types)
    {
      methodsMapping.put(inMethod, new MethodMapping(outMethod, types));
    }
  }

  private static class AsIs extends Mapping {

  }

  public FileConverter(String[] args)
      throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
  {
    inJarFile = args[0]; // "/home/martin/pegasos.Test/MMAGenerator/builds/server-production/ServerInterface.jar";
    outJarName = args[1];// "../ServerInterface/ServerInterface.jar";
    inputDataFile = new File(args[2]);
    outDataName = args[3];

    mappingMap = new HashMap<>();
    // Hr
    mappingMap.put((byte) 1, new AsIs());
    // Gps
    mappingMap.put((byte) 7, new AsIs());
    MapData bike = new MapData("BikeNrSSCompact");
    // MapData bike = new MapData("BikeNrSS");
    bike.addMethodMapping("CadencePower", "setCadencePowerTS", int.class, short.class);
    mappingMap.put((byte) 13, bike);
    // Marker
    mappingMap.put((byte) 17, new AsIs());
    // Setting
    mappingMap.put((byte) 18, new AsIs());

    outJar = new URLClassLoader(new URL[] {
        new File(outJarName).toURI().toURL()},
        this.getClass().getClassLoader());

    // ServerInterface.getInstance("none", 0, 0);
    outInterface = outJar.loadClass("at.univie.mma.serverinterface.ServerInterface").getMethod("getInstance", String.class, int.class, int.class).invoke(null, "none", 0, 0);
    outJar.loadClass("at.univie.mma.serverinterface.ServerInterface").getMethod("setSensorCount", int.class).invoke(outInterface, 0);

    outPacket = outJar.loadClass("at.pegasos.serverinterface.Packet");
    newOutPacket = outPacket.getConstructor(byte[].class, boolean.class);
  }

  private void getInputReader()
      throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, NoSuchFieldException
  {
    // inputParser = getReader("../ServerInterface/ServerInterface.jar");
    // inputParser = getReader("/home/martin/pegasos.Test/MMAGenerator/builds/server-production/ServerInterface.jar");

    URLClassLoader child = new URLClassLoader(new URL[] {new File(inJarFile).toURI().toURL()});

    Class<?> parserClass = child.loadClass("at.univie.mma.serverinterface.tools.PacketConverter");

    inPacket = child.loadClass("at.univie.mma.serverinterface.Packet");
    getBytes = inPacket.getMethod("getBytes");

    Class<?> timedData = child.loadClass("at.univie.mma.serverinterface.tools.TimedData");
    getTime = timedData.getField("time");
    getType = timedData.getField("type");
    getData = timedData.getField("data");
    getSensornr = timedData.getField("sensornr");

    // converter = (PacketConverter) parserClass.getConstructor().newInstance();
    converter = parserClass.getMethod("convert", inPacket);
  }

  public void readFile()
      throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException,
      ClassNotFoundException
  {
    // System.out.println("Open " + inputDataFile);
    FileInputStream in = new FileInputStream(inputDataFile);
    Object p;

    byte[] b_len = new byte[2];

    // setSensorDataFile(FileOutputStream stream)
    FileOutputStream outFile = new FileOutputStream(outDataName);
    outInterface.getClass().getMethod("setSensorDataFile", FileOutputStream.class).invoke(outInterface, outFile);

    // long pos= in.getChannel().position();
    while( (b_len[0]= (byte) in.read()) != -1 )
    {
      b_len[1]= (byte) in.read();
      // p= new Packet(in, b_len);
      p = inPacket.getConstructor(FileInputStream.class, byte[].class).newInstance(in, b_len);
      // System.out.println(p.toString());

      byte[] bytes = (byte[]) getBytes.invoke(p);
      byte type = bytes[2];
      Mapping map = mappingMap.get(type);
      if( map == null )
      {
        System.err.println("No mapping for dataType " + type);
        // return;
      }
      else if( map instanceof MapData )
      {
        @SuppressWarnings("unchecked")
        List<Object> ddx = (List<Object>) converter.invoke(null, p);
        // String dtypex = (String) getType.get(ddx.get(0));
        // String dtype = dtypex.substring(0, dtypex.indexOf("."));
        // outputType(dtype, ddx, p);
        outputMethodMapping((MapData) map, ddx);
      }
      else if ( map instanceof AsIs )
      {
        // addSensorData(Packet p)
        Object op = newOutPacket.newInstance(bytes, true);
        outInterface.getClass().getMethod("addSensorData", outPacket).invoke(outInterface, op);
      }
    }

    in.close();

    outInterface.getClass().getMethod("closeSensorDataFile").invoke(outInterface);
  }

  private void outputMethodMapping(MapData mapData, List<Object> type)throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ClassNotFoundException
  {
    StringBuilder method = new StringBuilder();
    for(Object data : type)
    {
      String data_type = (String) getType.get(data);
      String x = data_type.substring(data_type.indexOf(".") + 1);
      if( x.length() > 0 )
        method.append(x.substring(0, 1).toUpperCase()).append(x.substring(1));
    }

    MapData.MethodMapping methodMapping = mapData.methodsMapping.get(method.toString());
    if( methodMapping != null )
    {
      Object[] data = methodMapping.convert(type);
      data[data.length-1] = getTime.get(type.get(0));
      // Object sensorObject = mapData.clasz.getConstructor(int.class, int.class).newInstance(type.get(0).sensornr, 1);
      // Object sensorObject = getSensor(mapData.clasz, type.get(0).sensornr);
      Object sensorObject = getSensor(mapData.clasz, (Integer) getSensornr.get(type.get(0)));
      methodMapping.method.invoke(sensorObject, data);
    }
    else
      System.err.println("No mapping for " + method);
  }

  private Object getSensor(Class<?> clasz, int sensornr)
      throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ClassNotFoundException
  {
    // ServerSensor sensor = (ServerSensor) clasz.getConstructor(int.class, int.class).newInstance(sensornr, 1);
    Object sensor = clasz.getConstructor(int.class, int.class).newInstance(sensornr, 1);
    // outInterface.safeSensorAttach(sensor);
    outInterface.getClass().getMethod("safeSensorAttach", outJar.loadClass("at.pegasos.serverinterface.ServerSensor"))
        .invoke(outInterface, sensor);
    clasz.getMethod("start_sending").invoke(sensor);
    return sensor;
  }

  public static void main(String[] args)
      throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException,
      IllegalAccessException, NoSuchFieldException
  {
    FileConverter conv = new FileConverter(args);

    conv.getInputReader();
    conv.assignOutClasses();
    conv.readFile();
  }

  private void assignOutClasses() throws ClassNotFoundException, NoSuchMethodException
  {
    for(Mapping m : mappingMap.values())
    {
      if( m instanceof MapData )
      {
        MapData x = (MapData) m;
        x.clasz = outJar.loadClass("at.univie.mma.serverinterface.sensors." + x.outClass);

        for(MapData.MethodMapping mapping : x.methodsMapping.values())
        {
          Class<?>[] types = new Class<?>[mapping.types.length+1];
          System.arraycopy(mapping.types, 0, types, 0, mapping.types.length);
          types[mapping.types.length]= long.class;
          mapping.method = x.clasz.getMethod(mapping.name, types);
        }
      }
    }
  }
}
