package at.pegasos.tool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import at.univie.mma.serverinterface.*;

public class AddMarker {
  private static class MarkerSS extends ServerSensorSS {

    public MarkerSS(int nr, int datasets_per_packet)
    {
      super(nr, SensorTypes.MARKER, datasets_per_packet);

      sample_length = 0;

      sampling = false;
      init();
    }

    public void setData(long ts)
    {
      addData_fixed(ts);
    }
  }

  public static void main(String[] args) throws IOException
  {
    ServerInterface server= ServerInterface.getInstance("", 0, 0);
    MarkerSS marker= new MarkerSS(1, 1);
    
    long start_time= 1525413032000L;
    
    SimpleDateFormat sdf= new SimpleDateFormat("yyy-MM-dd_HHmmss");
    final String date_string= sdf.format(new Date(start_time));

    String fn= "marker_" + date_string;
    
    BufferedWriter meta= new BufferedWriter(new FileWriter(new File(fn + ".meta")));
    FileOutputStream out= new FileOutputStream(new File(fn + ".ser"));
    
    meta.write("Starttime:" + start_time);
    meta.close();
    
    server.setSensorDataFile(out);
    
    server.setSensorCount(1);
    server.attachSensor(marker);
    server.startSensorTime(start_time);
    
    /*for(int i= 0; i < 10; i++)
    {
      marker.setData(start_time);
      start_time+= 60* 1000;
      marker.setData(start_time);
      start_time+= 30* 1000;
      marker.setData(start_time);
    }*/
    marker.setData(start_time);
    for(int i= 0; i < 3; i++)
    {
      start_time+= 60* 1000 * 5;
      marker.setData(start_time);
      start_time+= 60* 1000 * 3;
      marker.setData(start_time);
    }
    start_time+= 60* 1000 * 2.5;
    marker.setData(start_time);
    
    server.closeSensorDataFile();
    server.close();
  }
}
