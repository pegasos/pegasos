package at.pegasos.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.univie.mma.serverinterface.Packet;
import at.pegasos.serverinterface.response.ServerResponse;

public class PacketUploader extends Tool {
  public static class Params extends Parameters {
    @Parameter(names= {"--param0", "-p0"}, description= "Param0")
    public int param0;
    
    @Parameter(names= {"--param1", "-p1"}, description= "Param1")
    public int param1;
    
    @Parameter(names= {"--param2", "-p2"}, description= "Param2")
    public int param2= 0;
    
    @Parameter(names= {"--filename", "-fn"}, description= "Filename", required= true)
    public String filename;
    
    @Parameter(names= {"--print-packetdata", "-pp"}, description= "Print data of individual packets", required= false)
    public boolean pp= false;
    /*
     * @Parameter(names= "--activity_c", description= "Activity_c") public int activity_c;
     * 
     * @Parameter(names= "--param2_hook", description= "Param2") public int param2_hook;
     */
  };
  
  protected Params parameters;
  long start;
  long last_time;
  long pcount= 0;
  
  public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException
  {
    PacketUploader uploader= new PacketUploader(args);
    
    String fn= Paths.get(uploader.getParams().filename).getFileName().toString().split("[.]")[0];
    String a[]= fn.split("_")[0].split("-");
    String b= fn.split("_")[1];
    
    Calendar d= Calendar.getInstance();
    // System.out.println(d.toString());
    d.clear();
    d.set(Integer.parseInt(a[0]), Integer.parseInt(a[1]) - 1, Integer.parseInt(a[2]), Integer.parseInt(b.substring(0, 2)),
        Integer.parseInt(b.substring(2, 4)), Integer.parseInt(b.substring(4, 6)));
    
    uploader.start= d.getTime().getTime();
    
    uploader.run();
  }
  
  public PacketUploader(String[] args)
  {
    parameters= new Params();
    new JCommander(parameters, args);
  }
  
  public PacketUploader(Params params)
  {
    parameters= params;
  }
  
  public void run() throws InterruptedException, ClassNotFoundException, IOException
  {
    if( !getParams().simulate )
      connect();
    
    if( !getParams().simulate )
      preFile();
    
    readFile(getParams().filename);
    
    System.out.println("Read File done");
    if( !getParams().simulate )
      server.heartbeat();
    
    if( !getParams().simulate )
      // TODO: check whether sending is necessary
      // close(start + last_time);
      server.stopActivity(start + last_time);
    
    if( !getParams().simulate )
    {
      fetchNprintResponses(true);
    
      server.close();
    }
    
    System.out.println("Uploaded " + pcount + " packets");
    System.out.println("Duration of activity: " + last_time);
  }
  
  @Override
  protected Params getParams()
  {
    return parameters;
  }
  
  private void preFile() throws InterruptedException
  {
    System.out.println("Start Sending");
    server.startSending();
    System.out.println("Start sensortime " + start);
    server.startSensorTime(start);
    
    System.out.println("Start Activity " + start);
    server.startActivityT(getParams().param0, getParams().param1, getParams().param2, start);
    
    Thread.sleep(1000);
    
    List<ServerResponse> responses= null;
    responses= server.pollResonses();
    
    System.out.println("Respones: " + responses);
    for(ServerResponse r : responses)
    {
      System.out.println(r.getClass().getName());
    }
  }
  
  public void readFile(String filename) throws ClassNotFoundException, IOException, InterruptedException
  {
    FileInputStream in= new FileInputStream(new File(filename));
    Packet p= null;
    
    byte b_len[]= new byte[2];
    
    long pos= in.getChannel().position();
    while( (b_len[0]= (byte) in.read()) != -1 )
    {
      b_len[1]= (byte) in.read();
      p= new Packet(in, b_len);
      if( getParams().pp )
        System.out.println(String.format("%08X", pos) + " " + p);
      pcount++;
      pos= in.getChannel().position();
      
      if( !getParams().simulate )
      {
        server.addPacket(p);
        Thread.sleep(1);
      }
    }
    
    in.close();
    last_time= (p != null ? p.getTimeSinceStart() : 0);
  }
}
