package at.pegasos.tool;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.*;

import at.pegasos.tool.util.*;
import at.univie.mma.serverinterface.Packet;
import at.univie.mma.serverinterface.SensorTypes;
import at.univie.mma.serverinterface.tools.TimedData;

public class ConcatFiles {
  public static class CFile {
    @Override
    public String toString()
    {
      return "CFile [fn=" + fn + ", meta=" + meta + "]";
    }
    public String fn;
    public Metadata meta;
  }
  
  public static void main(String[] args) throws IOException
  {
    List<CFile> files= new ArrayList<CFile>(args.length);
    
    for(int i= 0; i < args.length; i++)
    {
      CFile f= new CFile();
      f.fn= args[i];
      f.meta= MetadataParser.parse(Files.readAllLines(MetadataParser.metaFileFromFile(args[i])));
      files.add(f);
    }

    ConcatFiles.sort(files);

    new ConcatFiles().concat(files, "out.ser");
  }

  public static void sort(List<CFile> files)
  {
    try
    {
      files.sort(new Comparator<CFile>() {

        @Override
        public int compare(CFile o1, CFile o2)
        {
          // return (int) (Long.parseLong(o1.meta.getValue("Starttime")) - Long.parseLong(o2.meta.getValue("Starttime")));
          // return ((Long) o1.getStartTime()).compareTo(o2.getStartTime());
          return ((Long) Long.parseLong(o1.meta.getValue("Starttime"))).compareTo(Long.parseLong(o2.meta.getValue("Starttime")));
        }
      });
    }
    catch(java.lang.NoSuchMethodError e)
    {
      Collections.sort(files, new Comparator<CFile>() {

        @Override
        public int compare(CFile o1, CFile o2)
        {
          // return (int) (Long.parseLong(o1.meta.getValue("Starttime")) - Long.parseLong(o2.meta.getValue("Starttime")));
          // return ((Long) o1.getStartTime()).compareTo(o2.getStartTime());
          return ((Long) Long.parseLong(o1.meta.getValue("Starttime"))).compareTo(Long.parseLong(o2.meta.getValue("Starttime")));
        }
      });
    }
  }

  public void concat(List<CFile> files, String outName) throws IOException
  {
    // calc offset for fn2
    long t1= Long.parseLong(files.get(0).meta.getValue("Starttime"));
    // copy file 1 to new file
    System.out.println("First: " + files.get(0).fn);
    FileInputStream in= new FileInputStream(new File(files.get(0).fn));
    FileOutputStream out= new FileOutputStream(new File(outName));
    Packet p= null;
    byte b_len[]= new byte[2];
    ByteBuffer buffer= ByteBuffer.allocate(8);
    
    while( (b_len[0]= (byte) in.read()) != -1 )
    {
      b_len[1]= (byte) in.read();
      p= new Packet(in, b_len);
      
      handlePacket(p.getBytes());

      out.write(p.getBytes());
    }
    in.close();
    out.close();

    bike_dist_offset= bike_dist_last;

    // copy content file 2 but offset
    for(int i= 1; i < files.size(); i++)
    {
      System.out.println("File: " + files.get(i).fn);
      long t2= Long.parseLong(files.get(i).meta.getValue("Starttime"));
      long offset= t2 - t1;
      System.out.println(t1 + " " + t2 + " " + (t2 - t1));

      in = new FileInputStream(new File(files.get(i).fn));
      out = new FileOutputStream(new File(outName), true);

      while( (b_len[0]= (byte) in.read()) != -1 )
      {
        b_len[1] = (byte) in.read();
        p = new Packet(in, b_len);
        long t = p.getTimeSinceStart() + offset;

        buffer.putLong(0, t);
        // ret = ((int) bytes[5] & 0xFF) | ((int) bytes[6] & 0xFF) << 8 | ((int) bytes[7] & 0xFF) << 16 | ((int) bytes[8] & 0xFF) << 24
        p.getBytes()[5]= buffer.get(7);
        p.getBytes()[6]= buffer.get(6);
        p.getBytes()[7]= buffer.get(5);
        p.getBytes()[8]= buffer.get(4);

        handlePacket(p.getBytes());

        out.write(p.getBytes());
      }

      in.close();
      out.close();

      bike_dist_offset= bike_dist_last;
    }
  }

  private void handlePacket(byte[] packet)
  {
    switch(packet[2])
    {
      case SensorTypes.BIKENR:
        handleBike(packet);
        break;
      default:
        break;
    }
  }

  private long bike_dist_offset= 0;
  private long bike_dist_last= 0;

  private void handleBike(byte[] packet)
  {
    byte flags= 0;
    ByteBuffer buffer= ByteBuffer.allocate(8);

    int sensornr= packet[3]&31&0xff;
    long time_since_start= ((int)packet[5]&0xFF)|((int)packet[6]&0xFF)<<8|((int)packet[7]&0xFF)<<16|((int)packet[8]&0xFF)<<24;
    int datapackages= ((int)packet[4]&0xFF)|((int)((packet[3]&0xff)/32)<<8);
    long time_last_measurement= 0;
    int bc= 8;

    if( datapackages > 0 )
      time_since_start-= ((int)packet[bc+1]&0xFF)|((int)packet[bc+2]&0xFF)<<8;

    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)packet[bc+1]&0xFF)|((int)packet[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;

      flags= (byte) ((byte) packet[bc+3]&0xFF);
      if( (flags & 2) != 0 )
      {
        long tmp= 0;
        for(int i= 0; i < 8; i++)
        {
          tmp= (tmp<<8) + (packet[bc+6+i] & 0xff);
        }
        tmp+= bike_dist_offset;
        bike_dist_last= tmp;
        buffer.putLong(0, tmp);
        packet[bc+6]= buffer.get(0);
        packet[bc+6+1]= buffer.get(1);
        packet[bc+6+2]= buffer.get(2);
        packet[bc+6+3]= buffer.get(3);
        packet[bc+6+4]= buffer.get(4);
        packet[bc+6+5]= buffer.get(5);
        packet[bc+6+6]= buffer.get(6);
        packet[bc+6+7]= buffer.get(7);
      }
      bc= bc + 17;
    }
  }

  private static void copyFileUsingStream(File source, File dest) throws IOException
  {
    InputStream is= null;
    OutputStream os= null;
    try
    {
      is= new FileInputStream(source);
      os= new FileOutputStream(dest);
      byte[] buffer= new byte[1024];
      int length;
      while( (length= is.read(buffer)) > 0 )
      {
        os.write(buffer, 0, length);
      }
    }
    finally
    {
      is.close();
      os.close();
    }
  }

  public static void backup(List<CFile> files, String dest) throws IOException
  {
    for(CFile f : files)
    {
      File ff = new File(f.fn);
      copyFileUsingStream(ff, new File(dest + ff.getName()));
      // TODO: this is bad ..
      ff = new File(f.fn.substring(0, f.fn.length() - 4) + ".meta");
      if( ff.exists() )
        copyFileUsingStream(ff, new File(dest + ff.getName()));
      ff = new File(f.fn.substring(0, f.fn.length() - 4) + ".res");
      if( ff.exists() )
        copyFileUsingStream(ff, new File(dest + ff.getName()));
    }
  }
}
