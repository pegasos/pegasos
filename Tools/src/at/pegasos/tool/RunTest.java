package at.pegasos.tool;

import java.io.IOException;
import java.util.*;

import com.beust.jcommander.*;

import at.pegasos.serverinterface.response.*;
import at.univie.mma.serverinterface.sensors.FootPodSS;

public class RunTest extends Tool
{
  public static class Params extends Parameters
  {
    @Parameter(names= {"--time", "-t"}, description= "Time")
    public int time;
    
    @Parameter(names= {"--speed", "-s"}, description= "Speed [m/s]")
    public int speed;
  };
  
  protected Params parameters;
  long start;
  private FootPodSS sensor_fp;
  
  public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException
  {
    RunTest runner= new RunTest(args);
    
    if( !runner.getParams().simulate )
      runner.connect();
    
    runner.setUpSensors();
    runner.run();
    
    if( !runner.getParams().simulate )
      runner.close(runner.start+ runner.getParams().time* 1000L);
  }
  
  public RunTest(String[] args)
  {
    parameters= new Params();
    new JCommander(parameters, args);
  }
  
  @Override
  protected Params getParams()
  {
    return parameters;
  }
  
  public void setUpSensors()
  {
    sensor_fp = new FootPodSS(2, 1);
    sensor_fp.start_sending();
    
    server.setSensorCount(2);
    // ServerSensor ss= new ServerSensor(i, s.getSensorType(),
    // s.getDataSetsPerPacket());
    server.attachSensor(sensor_fp);
  }
  
  public void run() throws InterruptedException
  {
    long dist_m= 0;
    
    start= System.currentTimeMillis();
    server.startSending();
    System.out.println("Start sensortime " + start);
    server.startSensorTime(start);
    server.startActivityT(870, start);
    
    Thread.sleep(1000);
    
    List<ServerResponse> responses= null;
    responses= server.pollResonses();
    
    System.out.println("Respones: " + responses);
    for(ServerResponse r : responses)
    {
      System.out.println(r.getClass().getName());
    }
    
    for(int i= 0; i < getParams().time; i++)
    {
      System.out.println(i + ": "  + dist_m + "m");
      sensor_fp.setData(getParams().speed, dist_m, 0, 0);
      dist_m+= getParams().speed;

      responses= server.pollResonses();
      if( responses.size() > 0 )
      {
        System.out.println("Respones: " + responses);
        for(ServerResponse r : responses)
          printResponse(r);
      }
      
      if( !getParams().simulate )
        Thread.sleep(1000);
    }
  }
  
  protected boolean printResponse(ServerResponse r)
  {
    if( !super.printResponse(r) )
    {
      if( r instanceof AIMessage )
      {
        AIMessage ai= (AIMessage) r;
        StringBuilder out= new StringBuilder();
        for(Object x : ai.getData())
          out.append(x).append(" ");
        System.out.println("AIMessage " + ai.getType() + " data: " + out);

        return true;
      }
      
      return false;
    }
    
    return true;
  }
}
