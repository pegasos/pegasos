#!/bin/bash

ACTIVITY=110
PARAM2=10000
TIME=600
cd bin
while IFS=' ' read -r user passwd
do 
    echo "$user $passwd"
    java -classpath ".;../../ServerInterface/ServerInterface.jar" at.univie.mma.RandomClient $user $passwd $ACTIVITY $PARAM2 $TIME >> log.$user.txt &

done < ../users.txt
