#!/bin/bash

ACTIVITY=133
TIME=1200
cd bin
while IFS=' ' read -r user passwd
do 
    echo "$user $passwd"
    java -classpath ".;../../ServerInterface/ServerInterface.jar" at.univie.mma.RandomClient $user $passwd $ACTIVITY $TIME >> log.$user.txt &
	sleep $[ ( $RANDOM % 10 )  + 1 ]s
done < ../users.txt
