length: fixed
sampling-type: frequency, sample-on-set
----------------
fields:
speed_mm_s	int	double	drop_decimal
distance_m	int	double	drop_decimal
strides_255	int	long	drop_precision
calories	int	long	drop_precision
----------------
session insert:
// CALCULATE FP STRIDES
if (data.tmp_fp_strides<0)
{
  data.tmp_fp_strides=strides_255;
}

data.fp_strides_cul+= strides_255 - data.tmp_fp_strides;
if (data.tmp_fp_strides>strides_255) {data.fp_strides_cul+=256;}
data.tmp_fp_strides= strides_255;
----------------
session data database:
3	distance_m
4	speed_mm_s
----------------
session data code:
5	(int) data.fp_strides_cul
----------------

