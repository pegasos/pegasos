= Menu for the application
Menus can be declared using `<menu></menu>`. 
Every application needs to have a main menu (`<menu main="true">`). 
A menu consists of several entries (either activity `<entry type="Activity">` or references to other menus `<entry type="Menu">`). 
Every entry can set its displayed icon and text (`<icon id="true">R.drawable.id</icon><text>Text</text>`).
Currently, there are two entry types supported:
* Activity
* Intent

== Menu
Referencing to other menu is done using the tag `<name>Name</name>`. 
For every reference there has to be a other menu declared using `<menu name="Name">`. 
(Hence, sub-menus can be reached using different paths.)

== Activity
=== Controller
Specify the controller using `<activity>your id as specified in the controller list</activity>`.

=== Sport
If desired to start with a fixed sport (i.e. not sending the user to the SportSelectionScreen) set the sport using `<sport>Your Sport without apostrophes</sport>`.

=== Auto start
Usually starting an activity will lead the user to the TrainingLanding-Screen where the sensors are displayed. The user can then start the activity pressing the start button. 
An auto start where no start button is visible can be requested using `<autostart>true</autostart>`.
When autostart is set to `true` the requested activity will automatically start once 
* it is connected (checked with heartbeats)
* and all **required** sensors are OK.

=== Distances
* When the user should be queried for a distance (m) use ``<distance />``
* For setting a fixed distance use either ``<fixed>true</fixed><distance>METER</distance>`` or ``<distance fixed="true">METER</distance>``
* To give a preset value to the user use either ``<fixed>false</fixed><distance>METER</distance>`` or ``<distance fixed="false">METER</distance>`` (check before hand whether this has been implemented -- 2016/09/23) *If you need this feature bug Martin to implement it. It should not be more than 2 hours to do so*

*Side note: Es gibt den Beginn eines neuen Codes fuer Parameter. Mit dem waere das Distanz eingeben wieder nur ein Spezialfall vom generellen Fall*

=== Parameters
To pass additional values to your controller as "param2" you can use the `<parameters>` element.
There are no limitations (besides not breaking the XML-validity of the file) on what can be passed to the controller.
The controller is responsible for dealing with the input.

As a general guideline: when passing multiple parameters use `;` as a separator.

`parameters` is a shortcut for `<distance fixed="true">value</distance>` (although it uses/used different code).

=== Callbacks
In order to invoke some Java-Code before going to the LandingScreen or SportSelection use `callback`. A usecase for this is to reset the calibration factor of the foodpod which is loaded at the startup of the sensor.
Any statements written here will be wrapped inside a Runable and executed before going to the LandingScreen or SportSelection
`<callback>ValueStore.getInstance().setFpCalibInt(1000);</callback>`

=== Querying user input
There are two scenarios: 
* You have only one query. Then you can use the `<query>` element directly
* Otherwise: Wrap your `<query>` elements in a `<queries>` element.

Two types of builtin queries are implemented:
* List
* FileList

Both follow the same pattern:
* Specify an additional text using `<label>`
* Specify the title of the dialog using `<title>`
* Specify the name of the parameter using `<name>`

Keep in mind that you can only use either label or title (for some stupid Android reason).

When setting label, title or name you can either set the text directly or using `id="true"` as parameter to specify a string resource. When using string resources don't forget to add `R.string` e.g. `<title id="true">R.string.select_test</title>`.

Examples:

[source,xml]
----
<query type="List">
    <label>Test</label>
    <list>7;8;9</list>
</query>
<query type="FileList">
    <label>Test</label>
    <directory>wmt</directory>
    <pattern>.*.csv</pattern>
    <name>filename</name>
</query>
----

When desired you can create your own custom query. This needs to extend `StartActivityBase`. Just specify your class name as type eq `type="at.my.package.Name"`. For every parameter you would like to set add a child node to the xml and a setter with the same name to your class.
For example:

[source,xml]
----
<query type="at.my.package.Name">
  <Attribute1>A1</Attribute1>
  <attribute2>A2</attribute2>
</query>
----

setAttribute1(...)

setattribute2(...)


== Intent
Specify the class you want to invoke using `<intent>Your class as fully specified name here</intent>`.
