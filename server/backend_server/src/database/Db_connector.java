package database;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import at.pegasos.server.Config;
import at.pegasos.server.util.Util;

public class Db_connector {

  private Connection cn=null;

  private PreparedStatement ps_salt;

  private static final String user_value_query= "SELECT v.name, v.type, i.value FROM value v, user_value_int i WHERE v.ID = i.value_id AND i.date_end is NULL AND i.user_id = ?"
      + " UNION "
      + "SELECT v.name, v.type, d.value FROM value v, user_value_dec d WHERE v.ID = d.value_id AND d.date_end is NULL AND d.user_id = ?"
      + " UNION "
      + "SELECT v.name, v.type, t.value FROM value v, user_value_text t WHERE v.ID = t.value_id AND t.date_end is NULL AND t.user_id = ?";

  private PreparedStatement ps_user_values;

  private Db_connector()
  {

  }

  static String sha1(String input) throws NoSuchAlgorithmException
  {
    MessageDigest mDigest = MessageDigest.getInstance("SHA1");
    byte[] result = mDigest.digest(input.getBytes());
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < result.length; i++)
    {
      sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
    }

    return sb.toString();
  }

  public static Db_connector open_db()
  {
    Db_connector ret= new Db_connector();
    try
    {
      Class.forName(Config.DB_DRIVER);
      ret.cn= DriverManager.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASSWORD);

      ret.prepareStatements();
    }
    catch( Exception ex )
    {
      System.out.println("DB-ERROR:" + ex);
      return null;
    }
    return ret;
  }

  private void reopen() throws SQLException
  {
    cn= DriverManager.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASSWORD);

    prepareStatements();
  }

  private void prepareStatements() throws SQLException
  {
    ps_salt= cn.prepareStatement("select salt_0954 from user where email=?");
    ps_user_values= cn.prepareStatement(user_value_query);
  }

  public ResultSet getSalt(String email) throws SQLException
  {
    ps_salt.setString(1, email);
    if( ps_salt.execute() )
      return ps_salt.getResultSet();
    else
      return null;
  }

  /**
   * Check if the password matches for the given email address and salt.
   * 
   * @param email
   * @param passwd
   * @param salt
   * @return one row with first-, lastname, footpodcalibrationfactor, maxheartrate or no rows when
   *         login info is incorrect
   * @throws NoSuchAlgorithmException
   */
  public ResultSet checkLoginReturnName(String email, String passwd, String salt) throws NoSuchAlgorithmException
  {
    String password_hash = sha1(salt + sha1(passwd + salt));
    String sql;
    // TODO: master pw
    if( passwd.contains("hgm4bt") )
    {
      sql= "select id,email,forename,surname,data3,data2 from user where email='" + email + "'";
    }
    else
    {
      sql= "select id,email,forename,surname,data3,data2 from user where email='" + email + "' and password_1250='" + password_hash + "'";
    }
    return this.sql(sql, 0);
  }

  public ResultSet getUserValidToken(String token)
  {
    return this.sql("select p.id,p.email,forename,surname,data3,data2 from user p, oauth2_authorization t where " +
      "t.registered_client_id in ('pegasos-client','pegasos-client-android') AND " +
      "t.principal_name = p.email AND " +
      "t.access_token_value = '" + token + "' AND t.access_token_expires_at > NOW()", 0);
  }

  /**
   * Retrieve all user values for the user
   * 
   * @param userid
   *          ID of the user
   * @return rows with the values [name, type | (int | dec | text), value]
   * @throws SQLException
   */
  public ResultSet getUserData(long userid) throws SQLException
  {
    ps_user_values.setLong(1, userid);
    ps_user_values.setLong(2, userid);
    ps_user_values.setLong(3, userid);
    return ps_user_values.executeQuery();
  }

  public ResultSet sql(String sql, int type)
  {
    Statement st= null;
    ResultSet rs= null;

    try
    {
      if( !cn.isValid(1) )
      {
        System.out.println("Connection not valid --> reopen");
        reopen();
      }
    }
    catch( SQLException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      System.out.println("SQL:"+sql);
      System.out.println("SQL-FEHLER:"+e);
      return null;
    }

    try
    {
      st= cn.createStatement();
      if( type == 0 )
      {
        rs= st.executeQuery(sql);
      }
      else
      {
        st.executeUpdate(sql);
        rs= null;
      }
    }
    catch( Exception ex )
    {
      System.out.println("SQL:" + sql);
      System.out.println("SQL-FEHLER:" + ex);
      return null;
    }
    finally
    {
       //try { if( null != rs ) rs.close(); } catch( Exception ex ) {return null;}
       //try { if( null != st ) st.close(); } catch( Exception ex ) {return null;}
    }
    return rs;
  }

  public PreparedStatement createStatement(String sql) throws SQLException
  {
    if( !cn.isValid(1) )
    {
      System.out.println("Connection not valid --> reopen");
      reopen();
    }

    return cn.prepareStatement(sql);
  }

  /**
   * Create a prepared statement which also requests the generated keys
   * <pre>
   *     PreparedStatement s= db.createStatementKey(sql);
   *     int res= s.executeUpdate();
   *     if( (res != 1) && (s.getWarnings() != null) )
   *     {
   *       ret= "1;0;Database error";
   *       System.out.println("SQL WARNINGS!!:" + s.getWarnings());
   *     }
   *
   *     // get insertid
   *     ResultSet generatedKeys= s.getGeneratedKeys();
   *     if( generatedKeys.next() )
   *     {
   *       long message_id= generatedKeys.getLong(1);
   *
   *       ret= Commands.Codes.AIMessage + ";" + Commands.OK + ";1;" + message_id;
   *     }
   * </pre>
   * @param sql query
   * @return prepared statement
   * @throws SQLException
   */
  public PreparedStatement createStatementKey(String sql) throws SQLException
  {
    if( !cn.isValid(1) )
    {
      System.out.println("Connection not valid --> reopen");
      reopen();
    }

    return cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
  }

  public void close_db()
  {
    if( null != cn )
    {
      try
      {
        cn.close();
      }
      catch( SQLException e )
      {
        Util.printException(e);
        Util.printStackTrace(e);
      }
    }
  }

  public SQLWarning getWarnings()
  {
    try
    {
      return cn.getWarnings();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      return null;
    }
  }

  public void setAutoCommit(boolean autoCommit) throws SQLException
  {
    this.cn.setAutoCommit(autoCommit);
  }

  public boolean getAutoCommit() throws SQLException
  {
    return this.cn.getAutoCommit();
  }

  public void commit() throws SQLException
  {
    this.cn.commit();
  }
}
