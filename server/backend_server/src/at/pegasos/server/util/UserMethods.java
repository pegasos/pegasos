package at.pegasos.server.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Db_connector;

public class UserMethods {
  public class UserParam
  {
    public long user_id;
    
    /**
     * Value of the requested param
     */
    public double value;
    
    public String toString()
    {
      return String.format("User <%d,%f>", user_id, value);
    }
  }
  
  private static UserMethods instance;
  
  private Db_connector db;
  
  public synchronized static UserMethods getInstance(Db_connector db)
  {
    if( instance == null )
      instance= new UserMethods(db);
    
    return instance;
  }

  private UserMethods(Db_connector db)
  {
    this.db= db;
  }
  
  // TODO: document me
  public UserParam[] requestParam(String param, boolean sorted, long... user_ids) throws SQLException
  {
    UserParam[] ret= null;
    
    String tmp= param.toLowerCase();
    
    if( tmp.equals("vo2max") )
    {
      int i;
      
      System.out.println("N users to get from db " + user_ids.length);
      String sql= "SELECT id, data1 from user WHERE id in (";
      sql+= user_ids[0];
      for(i= 1; i < user_ids.length; i++)
        sql+= "," + user_ids[i];
      sql+= ")";
      if( sorted )
        sql+= " ORDER BY data1 DESC";
      
      ret= new UserParam[user_ids.length];
      ResultSet res= db.createStatement(sql).executeQuery();
      i= 0;
      while(res.next())
      {
        System.out.println("Adding user " + i);
        ret[i]= new UserParam();
        ret[i].user_id= res.getLong(1);
        ret[i].value= res.getDouble(2);
        i++;
      }
      res.close();
    }
    return ret;
  }
}
