package at.pegasos.server.util;

public class Util {

  public static void printStackTrace(Exception e)
  {
    StringBuilder builder= new StringBuilder();
    StackTraceElement[] trace= e.getStackTrace();
    for(StackTraceElement traceElement : trace)
      builder.append("\tat " + traceElement + "\n");
    
    System.out.println(builder.toString());
  }
  
  public static void printException(Exception e)
  {
    System.out.println("Exception: " + e.getClass().getCanonicalName());
    System.out.println("Message: " + e.getMessage());
    printStackTrace(e);
  }
}
