package at.pegasos.server.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import at.pegasos.data.Pair;
import at.pegasos.server.data.Session;
import database.Db_connector;

public class GroupMethods {
  public class Group {
    /**
     * ID of the group
     */
    public long group_id;
    
    /**
     * ID of the team it belongs to
     */
    public long team_id;
    
    /**
     * Name of the group
     */
    public String name;
    
    public String toString()
    {
      return String.format("Group<id:%d,team_id:%d,%s",group_id, team_id, name); 
    }
  }
  
  private static GroupMethods instance;
  
  private Db_connector db;
  
  public synchronized static GroupMethods getInstance(Db_connector db)
  {
    if( instance == null )
      instance= new GroupMethods(db);
    
    return instance;
  }
  
  private GroupMethods(Db_connector db)
  {
    this.db= db;
  }
  
  /**
   * Request the groups for a user of a specific team
   * 
   * @param user
   *          the user for which the request is made
   * @param team
   *          the team in which the groups will be
   * @param ngroups
   *          maximum number of groups which should be returned
   * @return
   * @throws SQLException
   */
  public Group[] getGroups(long user, long team, int ngroups) throws SQLException
  {
    int count= 0;
    PreparedStatement st= db.createStatement("select id, name from teams_groups where userid = ? AND teamid = ? AND deleted = false");
    st.setLong(1, user);
    st.setLong(2, team);
    
    ArrayList<Group> groups= new ArrayList<Group>(ngroups);
    
    ResultSet res= st.executeQuery();
    while( res.next() )
    {
      Group g= new Group();
      g.group_id= res.getLong(1);
      g.name= res.getString(2);
      g.team_id= team;
      
      groups.add(g);
      count++;
      if( count >= ngroups )
        break;
    }
    res.close();
    
    Group[] ret= new Group[groups.size()];
    
    return groups.toArray(ret);
  }
  
  /**
   * Create groups in a team for a user using a naming scheme
   * 
   * @param user_id
   *          the user for which the groups will be created
   * @param team
   *          the team in which the groups will be create
   * @param scheme
   *          naming scheme to be used
   * @param count
   *          number of groups to be created
   * @throws SQLException
   */
  public Group[] createGroups(long user_id, long team, String scheme, int count) throws SQLException
  {
    Group[] ret= new Group[count];
    PreparedStatement st= db.createStatement("INSERT INTO teams_groups(userid, teamid, name, crtime, deleted) values (?,?,?,?,?)");
    PreparedStatement get= db
        .createStatement("SELECT id from teams_groups where userid = ? AND teamid = ? AND name = ? AND crtime = ? AND deleted = false");
    
    // Get how many other groups are in that team
    ResultSet res= db.createStatement("SELECT count(*) FROM teams_groups WHERE teamid = " + team + " AND deleted = false").executeQuery();
    int nr= res.getInt(1);
    res.close();
    
    for(int i= 0; i < count; i++)
    {
      ret[i]= new Group();
      st.setLong(1, user_id);
      st.setLong(2, team);
      String name= String.format(scheme, ++nr);
      st.setString(3, name);
      long time= System.currentTimeMillis() / 1000;
      st.setLong(4, time);
      st.setInt(5, 0);
      st.executeUpdate();
      
      get.setLong(1, user_id);
      get.setLong(2, team);
      get.setString(3, String.format(scheme, i + 1));
      get.setLong(4, time);
      res= get.executeQuery();
      ret[i].group_id= res.getLong(1);
      ret[i].name= name;
      res.close();
    }
    st.executeBatch();
    
    return ret;
  }
  
  /**
   * Arrange groups so that they contain only the specified users
   * 
   * @param assignment
   *          List of pairs of <user-id,group-id>
   * @param usergroups 
   * @throws SQLException
   */
  public void setUsersInGroups(long teamid, ArrayList<Pair<Long, Long>> assignment, Map<Long, Long> usergroups) throws SQLException
  {
    // Get list of all groups with their users
    Map<Long, List<Long>> groups= new HashMap<Long, List<Long>>();
    String allusers= "";
    boolean first= true;
    for(Pair<Long, Long> ug : assignment)
    {
      if( !groups.containsKey(ug.getB()) )
        groups.put(ug.getB(), new ArrayList<Long>());
      groups.get(ug.getB()).add(ug.getA());
      if( first )
        allusers+= ug.getA();
      else
        allusers+= "," + ug.getA();
      first= false;
    }
    
    Set<Long> lostusers= new HashSet<Long>(assignment.size());
    PreparedStatement deletes= db.createStatement("UPDATE teams_memberships SET deleted = true WHERE id = ?");
    boolean delete= false;
    for(Long group_id : groups.keySet())
    {
      boolean delete_group= false;
      // Get all users which should not be in our group
      String sql= "SELECT id, userid FROM teams_memberships WHERE deleted = false AND groupid = " + group_id;
      sql+= " AND userid not in ";
      String users= "(";
      first= true;
      for(Long user : groups.get(group_id))
      {
        if( !first )
          users+= ",";
        
        users+= user;
        
        first= false;
      }
      users+= ")";
      sql+= users;
      System.out.println(sql);
      ResultSet res= db.createStatement(sql).executeQuery();
      while( res.next() )
      {
        deletes.setLong(1, res.getLong(1));
        deletes.addBatch();
        lostusers.add(res.getLong(2));
        delete= true;
        delete_group= true;
      }
      res.close();
      
      if( delete_group )
      {
        // execute the deletion
        System.out.println("Remove users from group: " + deletes.toString());
        deletes.executeBatch();
      }
      
      // Fetch all active users in the group --> these should not be inserted again
      sql= "SELECT userid FROM teams_memberships where teamid = " + teamid + " AND groupid = " + group_id + " AND userid in " + users
          + " AND deleted = false";
      System.out.println(sql);
      res= db.createStatement(sql).executeQuery();
      Set<Long> ing= new HashSet<Long>();
      while( res.next() )
        ing.add(res.getLong(1));
      res.close();
      
      // Delete all other memberships of the users in the group
      db.createStatement(
          "UPDATE teams_memberships SET deleted = true WHERE userid in " + users + " AND teamid <> " + teamid + " AND groupid <> " + group_id)
          .executeUpdate();
      
      // Insert users not already present
      PreparedStatement ins= db
          .createStatement("INSERT INTO teams_memberships(userid, teamid, groupid, crtime, deleted) values (?,?,?,?,?)");
      long time= System.currentTimeMillis() / 1000;
      for(Long user : groups.get(group_id))
      {
        if( ing.contains(user) )
          continue;
        
        ins.setLong(1, user);
        ins.setLong(2, teamid);
        ins.setLong(3, group_id);
        ins.setLong(4, time);
        ins.setBoolean(5, false);
        System.out.println(ins.toString());
        ins.addBatch();
      }
      ins.executeBatch();
    }
    
    // are there users which have lost their team?
    if( delete )
    {
      System.out.println("There are users without a group");
      
      // Get default group
      ResultSet res= db.sql("SELECT id from teams_groups where name = 'default' AND teamid = " + teamid + " AND deleted = false", 0);
      long gid;
      if( !res.next() )
      {
        db.sql("INSERT INTO teams_groups(userid, teamid, name, crtime, deleted) select userid, id, 'default',"
            + System.currentTimeMillis() / 1000 + ",0 from teams where id = " + teamid, 1);
        res.close();
        res= db.sql("SELECT id from teams_groups where name = 'default' AND teamid = " + teamid + " AND deleted = false", 0);
        res.next();
        gid= res.getLong(1);
      }
      else
        gid= res.getLong(1);
      res.close();
      
      System.out.println("Default team: " + gid);
      
      String tmp= "";
      boolean comma= false;
      for(Long user : lostusers)
      {
        if( comma )
          tmp+= ",";
        tmp+= user;
        comma= true;
      }
      
      // fetch all users that have no team
      System.out.println("Lost users: " + lostusers.toString());
      Set<Long> lost_with_team= new HashSet<Long>(lostusers.size());
      res= db.createStatement("select userid from teams_memberships where userid in (" + tmp + ") and deleted = false").executeQuery();
      while( res.next() )
        lost_with_team.add(res.getLong(1));
      
      lostusers.removeAll(lost_with_team);
      System.out.println("Lost users with team: " + lost_with_team.toString());
      System.out.println("Lost users: " + lostusers.toString());
      
      PreparedStatement insert= db.createStatement("insert into teams_memberships(userid,teamid,groupid,crtime,deleted) values (?," + teamid
          + "," + gid + "," + System.currentTimeMillis() / 1000 + ",false)");
      for(Long user : lostusers)
      {
        insert.setLong(1, user);
        insert.addBatch();
      }
      System.out.println(insert.toString());
      insert.executeBatch();
      
      System.out.println("Insert done");
    }
    
    try
    {
      System.out.println("Vorhandene Sessions aktualisieren");
      System.out.println("SELECT session_id, userid FROM session where userid in (" + allusers + ") and endtime = 0 and last_update > " + (System.currentTimeMillis() - 30000));
      ResultSet res= db.sql("SELECT session_id, userid FROM session where userid in (" + allusers + ") and endtime = 0 and last_update > " + (System.currentTimeMillis() - 30000), 0);
      PreparedStatement update= db.createStatement("UPDATE session set teamid = ?, groupid = ? WHERE session_id = ?");
      boolean updates= false;
      while( res.next() )
      {
        update.setLong(1, teamid);
        update.setLong(2, usergroups.get(res.getLong(2)));
        update.setLong(3, res.getLong(1));
        update.addBatch();
        updates= true;
      }
      if( updates )
      {
        System.out.println(update.toString());
        update.executeBatch();
      }
    }
    catch(Exception e)
    {
      System.out.println("Session update fehlgeschlagen");
      Util.printException(e);
    }
  }
  
  /**
   * Updates the group_id and team_id in the session
   * 
   * @param session
   *          The user's session.
   * @return True if update was successful
   */
  public void updateGroup(Session session) throws SQLException
  {
    if( session.club == 0 )
      return;

    String sql= "SELECT teamid, groupid FROM teams_memberships WHERE deleted = false AND userid = " + session.user_id;
    System.out.println(sql);
    ResultSet resultSet= db.sql(sql, 0);

    // when an error occurs resultSet == null
    if( resultSet != null && resultSet.next() )
    {
      session.team_id= resultSet.getInt(1);
      session.group_id= resultSet.getInt(2);
    }
  }
}
