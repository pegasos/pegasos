package at.pegasos.server.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.*;
import java.security.spec.*;

import javax.crypto.*;
import javax.crypto.spec.*;

public class Security {

  private static Security instance;
  private final Cipher cipherDecrypt;
  private final PrivateKey privKey;

  private Security() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
  {
    privKey = readPrivKeyFromFile("/private.key");

    cipherDecrypt= Cipher.getInstance("RSA/ECB/PKCS1Padding");
    cipherDecrypt.init(Cipher.DECRYPT_MODE, privKey);
  }
  
  public byte[] decryptRaw(byte[] data) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
      NoSuchPaddingException, IOException, InvalidKeyException
  {
    return cipherDecrypt.doFinal(data);
  }

  public byte[] decryptRawBlock(byte[] data, Cipher blockCypher) throws IllegalBlockSizeException, BadPaddingException
  {
    return blockCypher.doFinal(data);
  }
  
  public String decrypt(String msg) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException,
      IOException, InvalidKeyException
  {
    byte[] cipherData= cipherDecrypt.doFinal(msg.getBytes(StandardCharsets.UTF_8));
    return new String(cipherData, StandardCharsets.UTF_8);
  }
  
  public static PublicKey readPubKeyFromFile(String keyFileName) throws IOException
  {
    InputStream in = Security.class.getResourceAsStream(keyFileName);

    // now read the actual key from the file
    ByteArrayOutputStream buffer = new ByteArrayOutputStream(2048);

    // Just to be safe we read it in loop
    int nRead;
    byte[] data= new byte[2048];

    while( (nRead= in.read(data, 0, data.length)) != -1 )
    {
      buffer.write(data, 0, nRead);
    }

    byte[] keyBytes= buffer.toByteArray();

    try
    {
      X509EncodedKeySpec keySpec= new X509EncodedKeySpec(keyBytes);
      KeyFactory fact= KeyFactory.getInstance("RSA");
      return  fact.generatePublic(keySpec);
    }
    catch( Exception e )
    {
      throw new RuntimeException("Spurious serialisation error", e);
    }
    finally
    {
      in.close();
    }
  }
  
  private PrivateKey readPrivKeyFromFile(String keyFileName) throws IOException
  {
    InputStream in= null;

    try
    {
      System.out.println(getClass().getResource(keyFileName));
      in= getClass().getResourceAsStream(keyFileName);
      if( in == null )
      {
        System.out.println(getClass().getClassLoader().getResource(keyFileName));
        System.out.println(ClassLoader.getSystemClassLoader().getResource(keyFileName));
        in= getClass().getClassLoader().getResourceAsStream(keyFileName);
        if( in == null )
        {
          // TODO:
        }
      }
      
      // now read the actual key from the file
      ByteArrayOutputStream buffer= new ByteArrayOutputStream(2048);

      // Just to be safe we read it in loop
      int nRead;
      byte[] data= new byte[2048];
      
      while( (nRead= in.read(data, 0, data.length)) != -1 )
      {
        buffer.write(data, 0, nRead);
      }

      byte[] keyBytes= buffer.toByteArray();
      
      PKCS8EncodedKeySpec keySpec= new PKCS8EncodedKeySpec(keyBytes);
      KeyFactory fact = KeyFactory.getInstance("RSA");
      PrivateKey privKey= fact.generatePrivate(keySpec);
      return privKey;
    }
    catch( Exception e )
    {
      // throw new RuntimeException("Spurious serialisation error", e);
      System.err.println(e);
      e.printStackTrace();
      throw new RuntimeException("Spurious serialisation error", e);
    }
    finally
    {
      if( in != null )
      {
        in.close();
      }
    }
  }

  public Cipher getBlockCypher(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException
  {
    final int keySize = 32;
    byte[] decryptedSKey = new byte[keySize];
    System.arraycopy(data, 0, decryptedSKey, 0, keySize);
    SecretKey skey = new SecretKeySpec(decryptedSKey, "AES");

    // --- Initialization vector 16 byte
    // byte[] iv = in.readNBytes(128 / Byte.SIZE);
    byte[] iv = new byte[16];
    System.arraycopy(data, keySize, iv, 0, 16);
    IvParameterSpec ivspec = new IvParameterSpec(iv);

    // --- initialize AES cipher
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
    cipher.init(Cipher.DECRYPT_MODE, skey, ivspec);

    return cipher;
  }
  
  public static Security getInstance() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
  {
    if( instance == null )
      instance = new Security();
    return instance;
  }
}
