package at.pegasos.server.util;

import java.io.IOException;
import java.util.Arrays;

import at.pegasos.server.Config;
import at.pegasos.server.data.Session;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.MMAProtocolParser;

public class AIHelper
{
  public static void RunAI(final Session data, final ControlCommand parser, int param0, int param1, int param2)
  {
    RunAI(data, parser, param0, param1, param2, data.session_id, 0, 0, data.language);
  }
  
  public static void RunAISessionAi(Session data, ControlCommand parser, int param0, int param1, int param2, long session_id, int ai)
  {
    RunAI(data, parser, param0, param1, param2, session_id, ai, 0, data.language);
  }
  
  public static void RunAISessionAiRestart(Session data, ControlCommand parser, int param0, int param1, int param2, long session_id, int ai, int restart)
  {
    RunAI(data, parser, param0, param1, param2, session_id, ai, 2, data.language);
  }
  
  public static void RunAI(final Session data, final ControlCommand parser, int param0, int param1, int param2, int ai, int restart, int language)
  {
    RunAI(data, parser, param0, param1, param2, data.session_id, ai, restart, language);
  }
  
  public static void RunAI(final Session data, final ControlCommand parser, int param0, int param1, int param2, long session_id, int ai, int restart, int language)
  {
    final String[] cmd= {"java", "-jar", Config.AI_FILE, String.valueOf(param0), String.valueOf(session_id), String.valueOf(param1),
        String.valueOf(param2), String.valueOf(ai), String.valueOf(restart), String.valueOf(language)};
    System.out.println(Arrays.toString(cmd));
    Thread runner= new Thread() {
      public void run()
      {
        try
        {
          Runtime.getRuntime().exec(cmd);
        }
        catch( IOException e )
        {
          e.printStackTrace();
          parser.log(e.getMessage());
        }
      }
    };
    runner.run();
  }
  
  public static void RunPostProcessor(final long sessionId, final MMAProtocolParser parser)
  {
    final String[] cmd= {"java", "-cp", Config.AI_FILE, "at.univie.mma.ai.postprocessing.PostProcessor", String.valueOf(sessionId)};
    System.out.println(Arrays.toString(cmd));
    Thread runner= new Thread() {
      public void run()
      {
        try
        {
          Runtime.getRuntime().exec(cmd);
        }
        catch( IOException e )
        {
          e.printStackTrace();
          parser.log(e.getMessage());
        }
      }
    };
    runner.run();
  }
}
