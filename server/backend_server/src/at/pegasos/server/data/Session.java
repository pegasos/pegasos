package at.pegasos.server.data;

import activities.Activity_sensordescription;

public class Session {
	public long user_id;
	public long session_id;

	public Activity_sensordescription activity_sensordescription;
	public long param1=0,param2=0,param3;
	public int club=0,team_id=0,group_id=0;
	public int language=0;
	public int sensortype = 0;
	public int sensornr = 0;
	public int datapackages = 0;
	public int number_of_bytes = 0;
	public long starttime=0;  // start time of session
	public int activity=0,tmp=0;
	public String salt="";
	public int[] v;
	public int[] v_last;
  public int insert_data=0;

  // IMU3d VARS
  public short acc_x = 0;
  public short acc_y = 0;
  public short acc_z = 0;
  public short gyr_x = 0;
  public short gyr_y = 0;
  public short gyr_z = 0;
  public short mag_x = 0;
  public short mag_y = 0;
  public short mag_z = 0;
  public short eul_x = 0;
  public short eul_y = 0;
  public short eul_z = 0;

  // GPS
  public double gps_lat = 0, gps_lon = 0;
  public short gps_alt = 0, gps_speed = 0, gps_acc = 0, gps_bear = 0, gps_dist = 0; 
 
 // FP VARS
 //private long distance_gps_cul=0,fp_strides_cul=0,fp_calories=0;
 //private int tmp_distance_gps=-1,tmp_fp_strides=-1,fp_calib_factor=1000;
  public long fp_strides_cul = 0, fp_calories = 0; // 18.03.
  public int tmp_fp_strides = -1; // 18.03
  
  public Session() {
    activity_sensordescription = new Activity_sensordescription();
  }

}
