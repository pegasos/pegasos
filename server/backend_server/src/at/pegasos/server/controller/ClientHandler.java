package at.pegasos.server.controller;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import at.pegasos.server.ServerRuntimeException;
import at.pegasos.server.parser.MMAProtocolParser;
import at.pegasos.server.util.Util;
import database.Db_connector;

public class ClientHandler implements Runnable {

  private String line;
  private MMAProtocolParser p;
  private Db_connector db;
  private DataOutputStream out;
  private BufferedInputStream in;
  private BufferedWriter f_out;
  private Calendar cal;
  private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy hh:mm:ss");
  
  private final Socket client;
  
  public ClientHandler(ServerSocket serverSocket, Socket client, BufferedWriter fstream) throws ServerRuntimeException
  { // Server/Client-Socket
    this.client = client;
    this.f_out = fstream;
    cal = Calendar.getInstance();
    db= Db_connector.open_db();
    this.p = new MMAProtocolParser(db, f_out);
    // this.startup=cal.getTimeInMillis();
  }
  
  public void run()
  {
    line = "";
    String feedback = "0";
    byte[] buffer = new byte[65535];
    byte[] buffer_tmp = new byte[65535];
    boolean online = true;
    int footer = 0;
    try
    {
      // Get input from the client
      f_out.write(sdf.format(cal.getTime()) + " :new Client connected:" + client.getInetAddress() + " THREAD:" + Thread.currentThread());
      f_out.newLine();
      f_out.flush();

           out = new DataOutputStream (client.getOutputStream());
           //in =new ByteArrayInputStream(new InputStreamReader(client.getInputStream()));
           in = new BufferedInputStream(client.getInputStream());
           
      do
      {
        int i= in.read(buffer, 0, 2);
        // System.out.print("Anzahl zeichen:"+i);
        if( i == 2 )
        {
          int number_of_bytes= ((int) buffer[0] & 0xFF) | ((int) buffer[1] & 0xFF) << 8;
          int anzahlZeichen= 2;
          int anzahlZeichen_tmp= 0;
          
          while( anzahlZeichen < number_of_bytes )
          {
            // System.out.println("GOT DATAPACKAGE PART ");
            anzahlZeichen_tmp= in.read(buffer_tmp, 0, number_of_bytes - anzahlZeichen);
            // System.out.println("anzzeichen tmp: " + anzahlZeichen_tmp);
            System.arraycopy(buffer_tmp, 0, buffer, anzahlZeichen - 2, anzahlZeichen_tmp);
            anzahlZeichen+= anzahlZeichen_tmp;
            // System.out.print("ANZAHLZEICHEN:"+number_of_bytes+" READ ALREADY:"+anzahlZeichen+"
            // -->");
          }
          
          if( anzahlZeichen > 4 )
          {
            footer= ((int) buffer[anzahlZeichen - 4] & 0xFF) | ((int) buffer[anzahlZeichen - 3] & 0xFF) << 8;
          }
          
          /*System.out.print("Footer:" + footer);
          System.out.print("Number of Bytes:" + number_of_bytes);
          System.out.print("Anzahl Zeichen:" + anzahlZeichen);
          System.out.print("FOOTER LOW:" + (buffer[anzahlZeichen - 4] & 0xFF));
          System.out.print("FOOTER HIGH:" + ((buffer[anzahlZeichen - 3] & 0xFF) << 8));*/

          if( ((anzahlZeichen == number_of_bytes) && (anzahlZeichen == footer) && ((buffer[2] & 0xff) > 0)) || ((buffer[2] & 0xff) == 0) )
          {
            line= new String(buffer, 0, anzahlZeichen - 2); // ohne Footer, CR
            // System.out.print("Received " + anzahlZeichen + " chars/bytes --> ");
            
            feedback= p.parse(buffer, line);
            out.writeBytes(feedback + "\n");
            // System.out.println("------------------------------------------------"); //18.03.
            // System.out.println("FEEDBACK:"+feedback); //18.03.
            // System.out.println("------------------------------------------------"); //18.03.
            out.flush();
            f_out.flush();
          }
          else
          {
            
            System.out.print("ANZAHLZEICHEN:" + number_of_bytes + " READ ALREADY:" + anzahlZeichen + " FOOTER SAYS:" + footer
                + " bytes BUFFER[2]=" + buffer[2] + "\n");
            // System.out.println(" FOOTER SAYS:"+footer+ "bytes\n"); // 18.03
            out.writeBytes("3;1;Send error!;\n"); // 18.03
            for(int tz= 0; tz < anzahlZeichen; tz++)
            {
              System.out.print("b[" + tz + "]=" + buffer[tz] + " ");
            }
            
            // System.out.println("Received "+anzahlZeichen+" chars/bytes --> ERROR RECEIVING!!!");
            // in.read(buffer,0,number_of_bytes-anzahlZeichen);
          }
        	   } 
        else
        {
          line= "";
          System.out.println("NO DATA");
          if( !client.isClosed() )
          {
            try
            {
              System.out.println("CLOSE CLIENT");
              client.close();
              System.out.println(sdf.format(cal.getTime()) + " :client connection " + client.getInetAddress() + " closed\n");
              f_out.write(sdf.format(cal.getTime()) + " :client connection " + client.getInetAddress() + " closed");
              f_out.newLine();
            }
            catch( IOException e )
            {
              Util.printException(e);
            }
            online= false;
          }
        }
      } while( online );
    }
    catch( IOException e )
    {
      System.out.println("IOException, Handler-run:" + e.getMessage());
      Util.printException(e);
    }
    catch( Exception e)
    {
      Util.printException(e);
    }
    finally
    {
      System.out.println("END OF SESSION!");
    } // END FINALY
    } // END RUN
} // END CLASSE 