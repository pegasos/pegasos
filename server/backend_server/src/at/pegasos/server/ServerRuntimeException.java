package at.pegasos.server;

public class ServerRuntimeException extends Exception {
  public ServerRuntimeException(String message)
  {
    super(message);
  }
  
  public ServerRuntimeException(Exception e)
  {
    super(e);
  }
  
  private static final long serialVersionUID= 6218100207953586441L;
}
