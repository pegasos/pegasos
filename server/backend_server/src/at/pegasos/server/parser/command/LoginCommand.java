package at.pegasos.server.parser.command;

import java.io.IOException;
import java.security.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.crypto.*;

import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import at.pegasos.server.util.Security;
import at.pegasos.server.data.Session;
import at.pegasos.server.util.Util;
import database.Db_connector;

public class LoginCommand implements IControlCommandParser {
  private final ControlCommand parser;
  private final Session data;
  private final Db_connector db;
  private Cipher blockCypher;

  public LoginCommand(ControlCommand parser, Session sess, Db_connector db)
  {
    this.parser= parser;
    this.data= sess;
    this.db= db;
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    StringBuilder ret = new StringBuilder();

    // System.out.println("Login command: " + Arrays.toString(t));
    if( t[1].equals("1") )
    {
      try
      {
        this.blockCypher = Security.getInstance().getBlockCypher(Security.getInstance().decryptRaw(Base64.getDecoder().decode(t[2])));
        // System.out.println("IV set");
        return "0;1;OK";
      }
      catch( InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException e1 )
      {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
    }

    try
    {
      String raw= ";" + new String(Security.getInstance().decryptRawBlock(Base64.getDecoder().decode(t[1]), this.blockCypher));
      t = raw.split("[;]");
      // We need a trim on the last value as it can contain some 'garbage'
      t[t.length - 1] = t[t.length - 1].trim();
      // System.out.println("Decrypted: " + Arrays.toString(t) + " " + t.length);
    }
    catch( InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e1 )
    {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    if( t.length < 3 )
    {
      ret = new StringBuilder("0;0;Login failed!");
    }
    else
    {
      if( t.length == 4 )
      {
        return tokenLogin(t[1], t[2], t[3]);
      }

      // Legacy Login with username / password
      data.activity = Integer.parseInt(t[3]);
      data.club = Integer.parseInt(t[4]);
 
      try
      {
        ResultSet res = db.getSalt(t[1]);
        String salt= "";
        if( res.getWarnings() != null )
        {
          ret = new StringBuilder("0;0;Database error");
          System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        }
        else
        {
          if( res.first() )
          {
            salt = res.getString(1);
          }
        }

        res= db.checkLoginReturnName(t[1], t[2], salt);
        if( res.getWarnings() != null )
        {
          ret = new StringBuilder("0;0;Database error");
          System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        }
        else
        {
          if( res.first() )
          {
            data.user_id = res.getLong(1);

            ret = new StringBuilder("0;1;" + res.getString(2) + ";" + res.getString(3) + ";" +
                    res.getString(4) + ";" + res.getString(5) + ";" + res.getString(6));

            // return user values
            ret.append(getUserValues(data.user_id));

            // ret+=";"+activity_sensordescription.get_activity_sensordesc(activity);
            System.out.println(ret);
            System.out.println("USER_ID:" + data.user_id);

            if( data.club == 1 )
            { // 18.03
              setTeam();
            }

            if( t.length >= 7 )
            {
              try
              {
                if( !setSession(t[6]) )
                  ret = new StringBuilder("0;0;Login failed");
              }
              catch(NumberFormatException e)
              {
                // TODO: meaningful error
                ret = new StringBuilder("0;0;Database error");
              }
            }

            parser.log("User login " + data.user_id + " " + data.team_id + " " + data.group_id);
          }
          else
          {
            ret = new StringBuilder("0;0;login_failed_pw");
          }
        }
      } // end try
      catch (SQLException e)
      {
        Util.printException(e);
        Util.printStackTrace(e);
        ret = new StringBuilder("0;0;SQL Exception");
        e.printStackTrace();
      }
      catch (NoSuchAlgorithmException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
        ret = new StringBuilder("0;0;Login failed!");
      }
    }
    
    return ret.toString();
  }

  private boolean setSession(String sessionId) throws SQLException, NumberFormatException
  {
    data.session_id = Long.parseLong(sessionId);
    try(ResultSet res = db.sql(
            "select session_id,starttime,param1,param2,ai,activity,last_update,userid from session where session_id="
                    + data.session_id + " order by session_id desc limit 1", 0))
    {
      if (res.getWarnings() != null)
      {
        System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        return false;
      }
      else
      {
        if (res.first())
        {
          if( res.getLong(7) != data.user_id )
          {
            parser.log("User " + data.user_id + " tried to attach to session " + data.session_id + " which does not belong to them");
            data.session_id = 0;
            return false;
          }
          data.session_id = res.getLong(1);
          data.starttime = res.getLong(2);
          data.param1 = res.getLong(3);
          data.param2 = res.getLong(4);
          data.param3 = res.getLong(5);
          data.activity = res.getInt(6);
          return true;
        }
      }
    }
    return false;
  }

  private void setTeam() throws SQLException
  {
    String sql = "select teamid,groupid from teams_memberships where deleted=false and userid=" + data.user_id + " and teamid in (select id from teams where type=1);";
    // System.out.println(sql);
    try( ResultSet res = db.sql(sql, 0) )
    {
      if (res.first())
      {
        data.team_id = res.getInt(1);
        data.group_id = res.getInt(2);
        // System.out.println(sql);
        // System.out.println("TEAM:" + data.team_id);
        // System.out.println("GROUP:" + data.group_id);
      }
    }
    catch (SQLException e)
    {
      throw e;
    }
  }

  private String tokenLogin(String token, String clubUsage, String sessionId) throws SQLException
  {
    ResultSet res = db.getUserValidToken(token);
    if( res.getWarnings() != null )
    {
      System.err.println("Login error " + res.getWarnings());
      return "0;0;Database error";
    }

    StringBuilder ret = new StringBuilder("0;1;");

    if( res.first() )
    {
      data.user_id = res.getLong(1);
      ret.append(res.getString(2)).append(";").append(res.getString(3)).append(";").append(res.getString(4)).append(";").append(res.getString(5)).append(";").append(res.getString(6));

      if( res.next() )
      {
        System.out.println("WFT?");
        System.out.println(res.getObject(1) + " " + res.getObject(2) + " " + res.getObject(3) + " " + res.getObject(4) + " " + res.getObject(5));
      }
    }
    else
    {
      return "0;0;token";
    }
    data.club = Integer.parseInt(clubUsage);

    parser.log("User login " + data.user_id + " " + data.team_id + " " + data.group_id);

    ret.append(getUserValues(data.user_id));

    if( data.club == 1 )
    {
      setTeam();
    }

    if( !sessionId.equals("") )
    {
      if( !setSession(sessionId) )
        return "0;0;Login failed";
    }

    return ret.toString();
  }

  private String getUserValues(long userId) throws SQLException
  {
    StringBuilder ret = new StringBuilder();
    ResultSet res = db.getUserData(userId);
    // System.out.println(res.getWarnings() + " " + res.first());
    if( res.getWarnings() == null && res.first() )
    {
      do
      {
        ret.append(";").append(res.getString(1)).append(";").append(res.getString(2)).append(";").append(res.getString(3));
      } while( res.next() );
    }
    res.close();
    return ret.toString();
  }
}
