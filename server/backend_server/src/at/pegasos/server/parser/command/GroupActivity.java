package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.*;
import java.util.*;

import at.pegasos.data.Pair;
import at.pegasos.server.parser.*;
import at.pegasos.server.util.*;
import at.pegasos.server.util.UserMethods.UserParam;
import at.pegasos.server.util.GroupMethods.Group;
import at.pegasos.server.Commands;
import at.pegasos.server.Commands.Codes;
import at.pegasos.server.data.Session;
import database.Db_connector;

public class GroupActivity implements IControlCommandParser {
  private ControlCommand parser;
  private Session data;
  private Db_connector db;
  private GroupMethods helper;
  
  PreparedStatement teammembers;
  
  public GroupActivity(ControlCommand parser, Session sess, Db_connector db) throws SQLException
  {
    this.parser = parser;
    this.data = sess;
    this.db = db;
    helper= GroupMethods.getInstance(db);
    
    teammembers= db.createStatement("SELECT t.id as teamid, u.id as userid, u.forename, u.surname "
        + "FROM teams_memberships m, user u, teams t where u.id = m.userid AND t.id = m.teamid AND t.userid = ? AND m.deleted = false ORDER BY t.id");
  }
  
  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret= null;
    String sql;
    
    System.out.println("Parse beginn");
    
    try
    {
      // GET Users waiting for starting a group session, no params needed
      int code= Integer.parseInt(t[1]);
      
      if( code == 1 ) // Get user counter
      {
        ret= getUserCounter(t);
      }
      else if( code == 2 ) // Start Group
      {
        ret= startGroup(t);
      }
      else if( code == 3 ) // Stop Group
      {
        // String h= ";3;" + group + ";8;2;";
        long group= Long.parseLong(t[2]);
        int x= Integer.parseInt(t[3]);
        int activity= Integer.parseInt(t[4]);
        
        parser.log("Stopping Group Activity! group:" + group + " activity:" + activity);
        System.out.println("Stopping Group Activity! group:" + group + " activity:" + activity);
        sql = "select session_id from session where activity=" + x
            + " and param1=" + activity + " and groupid=" + group
            + " and last_update>" + (System.currentTimeMillis() - 30000);
        // parser.log(sql);
        System.out.println(sql);
        
        boolean fail= false;
        ResultSet res= db.sql(sql, 0);
        while( res.next() )
        {
          sql= "insert into feedback(session_id,message,stop_session,itime) values (" + res.getLong(1) + ",' ',1,"
              + System.currentTimeMillis() + ")";
          // TODO: check for error
          db.sql(sql, 1);
          // if( r.getWarnings() != null )
          //  fail= true;
        }
        res.close();
        
        ret= Codes.GroupActivity + ";";
        if( fail )
          ret+= Commands.FAIL + ";";
        else
          ret+= Commands.OK + ";";
        ret+= "3;" + group + ";" + activity;
        
        System.out.println("Return: " + ret);
      }
      else if( code == 4 ) // send message to AI:GroupActivity
      {
        long sid= Long.parseLong(t[2]);
        
        String text;
        
        if( t.length == 4 )
          text= t[3];
        else
        {
          text= t[3];
          for(int i= 4; i < t.length; i++)
            text+= ";" + t[i];
        }
        
        sql= "insert into ai_message(session_id,message,itime) values (" + sid + ",'" + text + "'," + System.currentTimeMillis() + ")";
        System.out.println(sql);
        
        db.sql(sql, 1);
        // TODO: notify whether insert was successful
      }
      else if( code == 5 ) // Get messages for a certain group session
      {
        long sid= Long.parseLong(t[2]);
        ret= getMessages(sid);
      }
      else if( code == 7 )
      {
        ret= getTeams();
      }
      else if( code == 8 )
      {
        System.out.println("GetTeamMembers args: " + t.length);
        if( t.length >= 3 )
        {
          long teams[]= new long[t.length-2];
          int i;
          for(i= 2; i < t.length; i++)
            teams[i-2]= Long.parseLong(t[i]);
          ret= getTeamMembers(teams);
        }
        else
          ret= getTeamMembers();
      }
      else if( code == 9 )
      {
        long team= Long.parseLong(t[2]);
        int ngroups= Integer.parseInt(t[3]);
        String mode= t[4];
        long userids[]= new long[t.length - 5];
        for(int i= 5; i < t.length; i++)
          userids[i-5]+= Long.parseLong(t[i]);
        ret= requestGroupAssgingnment(team, ngroups, mode, userids);
      }
    }
    catch( SQLException e )
    {
      ret = "3;1;Datenbankproblem";
      System.out.println(e.getMessage());
      
    }
    
    System.out.println("Parse end");
    
    return ret;
  }

  private String getUserCounter(String[] t) throws SQLException
  {
    String ret= "";
    String sql;
    String temp= "";
    int count= 0;
    int a= Integer.parseInt(t[2]);
    int p= Integer.parseInt(t[3]);
    String group_name = null;
    
    System.out.println("CHECK_USERSCOUNTER: activity: " + a + " p:" + p);
    
    sql= "select id,teamid,name from teams_groups where deleted=0 and userid=" + data.user_id;
    // System.out.println(sql);
    ResultSet res= db.sql(sql, 0);
    while( res.next() )
    {
      data.group_id= res.getInt(1);
      System.out.println("GROUP:" + data.group_id);
      data.team_id= res.getInt(2);
      System.out.println("TEAM:" + data.team_id);
      group_name= res.getString(3);
      System.out.println("GROUPNAME:" + group_name);
      
      String q= "select count(*),sum(v10) from session where activity=" + a + " and param1=" + p + " and teamid=" + data.team_id
          + " and groupid=" + data.group_id + " and last_update>" + (System.currentTimeMillis() - 30000) 
          + " and ai=1";
      // System.out.println(q);
      
      ResultSet res1= db.sql(q, 0);
      if( res1.next() )
      {
        if( res1.getInt(2) < 1 )
          temp+= data.group_id + ";" + group_name + ";" + res1.getInt(1) + ";";
        else
          temp+= "-" + data.group_id + ";" + group_name + ";" + res1.getInt(1) + ";";
        count++;
      }
      res1.close();
    }
    res.close();

    ret= Commands.Codes.GroupActivity + ";" + Commands.OK + ";1;" + count + ";" + temp;
    System.out.println(ret);
    
    return ret;
  }
  
  private String startGroup(String[] t) throws SQLException
  {
    String ret= "";
    
    // String h= ";2;" + group + ";8;" + activity + ";";
    long group= Long.parseLong(t[2]);
    int param0= Integer.parseInt(t[3]);
    int activity= Integer.parseInt(t[4]);
    // TODO: 
    int param2= 0;
    
    System.out.println("Starting GroupActivity! " + param0);
    
    // Start a session
    data.starttime= System.currentTimeMillis();
    String sql= "insert into session (teamid,groupid,userid,activity,param1,param2,starttime,last_update,ai) " + "select teamid," + group
        + "," + data.user_id + "," + param0 + "," + activity + "," + param2 + "," + data.starttime + "," + data.starttime + "," + 0
        + " from teams_groups tg where tg.id = " + group;
    System.out.println(sql);
    
    PreparedStatement s= db.createStatementKey(sql);
    int res= s.executeUpdate();
    if( (res != 1) && (s.getWarnings() != null) )
    {
      ret= "1;0;Database error";
      System.out.println("SQL WARNINGS!!:" + s.getWarnings());
    }
    
    // get insertid
    ResultSet generatedKeys = s.getGeneratedKeys();
    if( generatedKeys.next() )
    {
      long session_id= generatedKeys.getLong(1);
      
      // TODO: setting restart=2 is a workaround ... GrouActivity can detect that this is the master
      AIHelper.RunAISessionAiRestart(data, parser, param0, activity, param2, session_id, (int) group, 2);
      parser.log("Starting Feedback Module: Group Activity new");
      
      ret= Commands.Codes.GroupActivity + ";" + Commands.OK + ";2;" + session_id + ";" + activity + ";" + group;
    }
    else
    {
      ret = "3;1;Datenbankproblem";
      System.out.println("Failed to obtain session id");
    }
    
    return ret;
  }
  
  private String getMessages(long sid)
  {
    String ret= null;
    String sql= "select message,feedback_id,stop_session from feedback where session_id=" + sid
        + " and (ttime=0 or ttime is null) order by itime ASC";
    
    ResultSet res= db.sql(sql, 0);
    try
    {
      if( res.first() )
      {
        System.out.println(res);
        String msg_user= res.getString(1);
        long msg_id= res.getLong(2);
        int stop_session= res.getInt(3);
        sql= "update feedback set ttime=" + System.currentTimeMillis() + " where feedback_id=" + msg_id;
        res.close();
        res= db.sql(sql, 1);
        
        if( msg_user.startsWith(";c;") )
        {
          ret= msg_user.substring(3);
        }
        else if( stop_session == 0 )
        {
          ret= "3;1;" + msg_user;
        }
        else
        {
          ret= "4;1;" + msg_user;
        }
      }
      else
      {
        System.out.println("No messages for " + sid);
        ret= "1000;0;no message";
      }
    }
    catch( SQLException e )
    {
      ret= "3;1;Database error";
    }
    
    return ret;
  }
  
  /**
   * Return a list of my teams. Formatted as <nr of teams>;<id-1>;<name-1>;...<id-n>;<name-n>
   * @return
   */
  private String getTeams()
  {
    String ret= null;
    String sql= "select id, name from teams where userid = " + data.user_id + " and deleted = false";
    
    String data= "";
    int count= 0;
    
    ResultSet res= db.sql(sql, 0);
    
    try
    {
      while(res.next())
      {
        data+= res.getLong(1) + ";" + res.getString(2) + ";";
        count++;
      }
      
      ret= Commands.Codes.GroupActivity + ";" + Commands.OK + ";7;" + count + ";" + data;
    }
    catch( SQLException e )
    {
      ret= "3;1;Database error";
    }
    
    return ret;
  }
  
  /**
   * Return a list of my team members, or members of teams. <BR/>
   * Formatted as <nr of members>;<team-id-1>;<user-id-1>;<forename-1>;<surname-1>;...
   * 
   * @return
   */
  private String getTeamMembers(long... teams)
  {
    PreparedStatement s;
    String ret= null;
    
    try
    {
      if(teams == null || teams.length == 0)
      {
        s= teammembers;
        s.setLong(1, data.user_id);
      }
      else
      {
        String sql= "SELECT t.id as teamid, u.id as userid, u.forename, u.surname "
            + "FROM teams_memberships m, user u, teams t where u.id = m.userid AND t.id = m.teamid AND t.id in (";
        sql+= teams[0];
        for(int i= 1; i < teams.length; i++)
          sql+= ","+teams[i];
        sql+= ") AND m.deleted = false ORDER BY t.id";
        s= db.createStatement(sql);
      }
      
      int count= 0;
      String data= "";
      System.out.println(s);
      ResultSet res= s.executeQuery();
      while(res.next())
      {
        // System.out.println("Res: " + res);
        data+= res.getLong(1) + ";" + res.getLong(2) + ";" + res.getString(3) + ";" + res.getString(4) + ";";
        count++;
      }
      
      ret= Commands.Codes.GroupActivity + ";" + Commands.OK + ";8;" + count + ";" + data;
      System.out.println("getTeamMembers return: " + ret);
    }
    catch( SQLException e )
    {
      System.out.println("SQL Exception: " + e.getMessage());
      ret= "3;1;Database error";
      Util.printException(e);
    }
    
    return ret;
  }
  
  private String requestGroupAssgingnment(long team, int ngroups, String mode, long... userids)
  {
    String ret;
    
    try
    {
      // check number of available groups for user
      Group[] g= helper.getGroups(data.user_id, team, ngroups);
      
      parser.log(String.format("Request GroupAssignment team:%d groups:%d mode:%s", team, ngroups, mode) + " " + Arrays.toString(userids));
      
      // create new groups if necessary
      if( g.length < ngroups )
      {
        Group[] g2= helper.createGroups(data.user_id, team, "Group %d", ngroups - g.length);
        Group[] go= g;
        g= new Group[g2.length + go.length];
        System.arraycopy(go, 0, g, 0, go.length);
        System.arraycopy(g2, 0, g, go.length, g2.length);
      }
      
      // sort users by <mode>
      UserParam[] users= null;
      if( mode.equals("VO2M") )
      {
        users= UserMethods.getInstance(db).requestParam("VO2Max", true, userids);
      }
      else if( mode.equals("Rank") )
      {
        users= UserMethods.getInstance(db).requestParam("VO2Max", true, userids);
      }
      else
        return Commands.Codes.GroupActivity + ";" + Commands.FAIL + ";Unknown Mode;";
      
      parser.log("Users: " + Arrays.toString(users));
      
      System.out.println("Userids length: " + userids.length + " " + users.length);
      // assign users to groups
      int iu= 0;
      int ig= 0;
      ArrayList<Pair<Long, Long>> assignment= new ArrayList<Pair<Long,Long>>(userids.length);
      Map<Long,Long> usergroups= new HashMap<Long,Long>();
      for(iu= 0; iu < users.length; iu++)
      {
        System.out.println("Assign " + iu + " to " + ig + " " + users[iu] + " " + g[ig]);
        Pair<Long,Long> a= new Pair<Long,Long>(users[iu].user_id, g[ig].group_id);
        usergroups.put(users[iu].user_id, g[ig].group_id);
        assignment.add(a);
        ig++;
        
        if( ig >= g.length )
          ig= 0;
      }
      
      // remove users from groups (if necessary) and add users to groups (if necessary)
      helper.setUsersInGroups(team, assignment, usergroups);
      
      assert(g.length == ngroups);
      String groups= "";
      for(Group group : g)
        groups+= group.group_id + ";" + group.name + ";";
      assert(assignment.size() == userids.length);
      int assigns= userids.length;
      String assignments= "";
      for(Pair<Long,Long> p : assignment)
        assignments+= p.getB() + ";" + p.getA() + ";";
      
      ret= Commands.Codes.GroupActivity + ";" + Commands.OK + ";9;" + ngroups + ";" + groups + assigns + ";" + assignments;
      System.out.println("getTeamMembers: " + ret);
    }
    catch ( SQLException e )
    {
      e.printStackTrace();
      Util.printException(e);
      ret= "3;1;Database error";
    }
    return ret;
  }
}
