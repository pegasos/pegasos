package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import database.Db_connector;

public class ContinueSession implements IControlCommandParser {
  private ControlCommand parser;
  private Session data;
  private Db_connector db;
  
  public ContinueSession(ControlCommand parser, Session sess, Db_connector db)
  {
    this.parser= parser;
    this.data= sess;
    this.db= db;
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret= "";
    ResultSet res;
    
    try
    {
      res = db.sql(
              "select session_id,starttime,param1,param2,ai,activity,last_update from session where userid="
                  + data.user_id + " order by session_id desc limit 1", 0);
      
      if( res.getWarnings() != null )
      {
        ret = "2;0;Database error";
        System.out.println("SQL WARNINGS!!:" + res.getWarnings());
      }
      else
      {
        if( res.first() )
        {
          data.session_id = res.getLong(1);
          data.starttime = res.getLong(2);
          data.param1 = res.getLong(3);
          data.param2 = res.getLong(4);
          data.param3 = res.getLong(5);
          data.activity = res.getInt(6);
//          long tmp = res.getLong(7);
          
          System.out.println("ACTIVITY:" + data.activity + " P1:" + data.param1 + " P2:" + data.param2 + " P3:" + data.param3);
          
          ret = "2;1;" + data.session_id + "";
          
          // START FEEDBACK MODULE - but only when last update is older than 10
          // seconds - MIT 30.07.2014 raus da AI-Modul nur beendet wird wenn...
          /*
           * if ( (param3>0) && ((System.currentTimeMillis()-tmp)>10000)) {
           * insert_data=0; System.out.println("Restarting Feedback Module!");
           * String[] cmd = {"java", "-jar",
           * "mma_ai.jar",String.valueOf(activity
           * ),String.valueOf(session_id),String
           * .valueOf(param1),String.valueOf(param2
           * ),String.valueOf(param3),"1",language+""};
           * Runtime.getRuntime().exec(cmd); } else {
           */
          data.insert_data = 1;
          // }
          
          res = db.sql("select v5 from session where session_id=" + data.session_id, 0);
          if( res.getWarnings() != null )
          {
            ret = "2;0;Database error";
            System.out.println("SQL WARNINGS!!:" + res.getWarnings());
          }
          else
          {
            res.first();
            data.fp_strides_cul = res.getInt(1);
            // System.out.println("STRIDES:"+fp_strides_cul);
            // tmp_fp_strides=res.getInt(1);
          }
          
          parser.log("Restarted session");
        }
        else
        {
          ret = "2;0;No session available!";
        }
      }
    }
    catch (SQLException e)
    {
      ret = "2;0;SQL Exception";
    }

    return ret;
  }
  
}