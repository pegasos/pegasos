package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import database.Db_connector;

public class Heartbeat implements IControlCommandParser {
//  private ControllCommand parser;
  private Session data;
//  private Db_connector db;
  
  public Heartbeat(ControlCommand parser, Session sess, Db_connector db)
  {
//    this.parser= parser;
    this.data= sess;
//    this.db= db;
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    System.out.println("Heartbeat for user: " + data.user_id + ", session: " + data.session_id + " " + System.currentTimeMillis());

    if(data.user_id==0)
      return "4;1;Login required!";
    
    // Do not check for sessions.
    // if(data.session_id==0)
    //  return "4;0;Session required!";
    
    // When the server gets this far then everything is ok. 
    String ret= "11;1;OK";
    
    return ret;
  }
  
}