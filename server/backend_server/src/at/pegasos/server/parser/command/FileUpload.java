package at.pegasos.server.parser.command;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

import at.pegasos.server.parser.*;
import at.pegasos.tool.util.*;
import at.pegasos.server.Commands;
import at.pegasos.server.Commands.Codes;
import at.pegasos.server.ServerRuntimeException;
import at.pegasos.server.data.Session;
import at.pegasos.server.util.AIHelper;
import database.Db_connector;

public class FileUpload implements IControlCommandParser {
  
  private final static int CHUNK_SIZE= 1024;
  
  private static long idh= 0;
  
  private static synchronized long nextID()
  {
    long ret;
    if( idh == Long.MAX_VALUE )
    {
      ret= 1;
    }
    else
    {
      ret= ++idh;
    }
    idh= ret;
    return ret;
  }
  
  private static class FileObject {
    /**
     * Name of the file
     */
    public String fn;

    public int nchunks;

    public Set<Integer> received;
  }

  private final Session data;
  private final Map<Long, FileObject> uploads;

  public FileUpload(ControlCommand parser, Session sess, Db_connector db)
  {
    this.data = sess;
    uploads= new HashMap<Long, FileObject>();
    try
    {
      Files.createDirectories(Paths.get("/tmp").resolve("upload"));
    }
    catch( IOException e )
    {
      // TODO what to do here? abort? (i.e. propagate exception somehow e.g. IllegalStateException?)
      e.printStackTrace();
    }
  }
  
  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret;
    
    if(data.user_id==0)
      return "4;1;Login required!";
    
    try
    {
      int action= Integer.parseInt(t[1]);
      
      switch( action )
      {
        // create file upload
        case 1:
          int chunks= Integer.parseInt(t[3]);
          long id= nextID();
          String filename= id + "_" + data.user_id + "_" + t[2];
          FileObject o= new FileObject();
          o.fn= filename;
          o.nchunks= chunks;
          o.received= new HashSet<Integer>(chunks);
          uploads.put(id, o);
          System.out.println("Upload created: id:" + id + " nchunks:" + chunks);
          
          ret= Codes.FileUpload + ";" + Commands.OK + ";1;" + id + ";" + t[2];
          
          break;
        
        // send data chunk
        case 2:
          long uploadId= Long.parseLong(t[2]);
          int chunknr= Integer.parseInt(t[3]);
          FileObject fo= uploads.get(uploadId);
          RandomAccessFile out= new RandomAccessFile(Paths.get("/tmp").resolve("upload").resolve(fo.fn).toFile(), "rw");
          out.getChannel().position((long) CHUNK_SIZE * chunknr);
          // out.write(DatatypeConverter.parseBase64Binary(t[4]));
          out.write(Base64.getDecoder().decode(t[4]));
          out.close();
          /*byte[] dd= DatatypeConverter.parseBase64Binary(t[4]);
          for(int i= 0; i < dd.length; i++)
          {
            // System.out.print(String.format("%02X ", dd[i]));
            System.out.print(dd[i] + " ");
          }
          System.out.println("");*/
          fo.received.add(chunknr);
          
          ret= Codes.FileUpload + ";" + Commands.OK + ";2;" + uploadId + ";" + chunknr;
          
          break;
          
        // Finish upload -> read file
        case 3:
          uploadId= Long.parseLong(t[2]);
          fo= uploads.get(uploadId);
          
          if( fo.received.size() != fo.nchunks )
          {
            ret= Codes.FileUpload + ";" + Commands.FAIL + ";3;" + uploadId + ";Wrong chunknr";
          }
          else
          {
            Metadata meta= null;
            if( t.length > 3 )
            {
              // byte[] dm= DatatypeConverter.parseBase64Binary(t[3]);
              // String ml= new String(dm);
              byte[] dm= Base64.getDecoder().decode(t[3]);
              String ml= new String(dm);
              meta= MetadataParser.parse(ml);
            }
            long sessionId = readFile(Paths.get("/tmp").resolve("upload").resolve(fo.fn).toFile(), meta);
            ret= Codes.FileUpload + ";" + Commands.OK + ";3;" + uploadId + ";" + sessionId;
          }
          break;
          
        default:
          ret= Codes.FileUpload + ";" + Commands.FAIL;
          break;
      }
    }
    catch( ServerRuntimeException e )
    {
      ret = "3;1;Datenbankproblem";
      System.out.println(e.getMessage());
    }
    return ret;
  }

  private long readFile(File file, Metadata meta) throws ServerRuntimeException
  {
    BufferedWriter f_out;
    try
    {
      f_out= new BufferedWriter(new FileWriter(Files.createTempFile(Paths.get("/tmp"), "readlog_" + file.getName(), ".txt").toFile()));
      
      Db_connector db= Db_connector.open_db();
      assert db != null;

      MMAProtocolParser parser= new MMAProtocolParser(db, f_out);

      // set login information
      parser.getSession().user_id= data.user_id;
      parser.log("Reading " + file.getName() + " started. AutoCommit" + db.getAutoCommit() + " Metadata:" + meta);

      parser.readFile(file, meta);
      System.out.println("Readfile done. AutoCommit? " + db.getAutoCommit());
      parser.log("Reading " + file.getName() + " done");

      // TODO: decide whether to run post processor
      AIHelper.RunPostProcessor(parser.getSession().session_id, parser);

      return parser.getSession().session_id;
    }
    catch( IOException | SQLException e )
    {
      // TODO notify user?
      e.printStackTrace();
      throw new ServerRuntimeException(e);
    }
  }
}
