package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.*;

import at.pegasos.server.parser.*;
import at.pegasos.server.Commands;
import at.pegasos.server.data.Session;
import database.Db_connector;

public class AIMessage implements IControlCommandParser {
  private Session data;
  private Db_connector db;
  
  public AIMessage(ControlCommand parser, Session sess, Db_connector db)
  {
    this.data= sess;
    this.db= db;
  }
  
  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    System.out.println("Parsing AIMessage " + t[1]);
    String ret= null;
    
    try
    {
      // GET Users waiting for starting a group session, no params needed
      int code= Integer.parseInt(t[1]);
      
      if( code == 1 ) // insert message for myself
      {
        System.out.println("Insert Message");
        ret= insertMessage(t);
      }
    }
    catch( SQLException e )
    {
      ret= "3;1;Datenbankproblem";
      System.out.println(e.getMessage());
      
    }
    catch( Exception e )
    {
      ret= "3;1;Datenbankproblem";
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
    return ret;
  }
  
  private String insertMessage(String[] t) throws SQLException
  {
    String ret= null;
    
    String text;
    
    text= t[2];
    for(int i= 3; i < t.length; i++)
      text+= ";" + t[i];
    
    String sql= "insert into ai_message(session_id,message,itime) values (" + data.session_id + ",'" + text + "',"
        + System.currentTimeMillis() + ")";
    System.out.println(sql);
    
    PreparedStatement s= db.createStatementKey(sql);
    int res= s.executeUpdate();
    if( (res != 1) && (s.getWarnings() != null) )
    {
      ret= "1;0;Database error";
      System.out.println("SQL WARNINGS!!:" + s.getWarnings());
    }
    
    // get insertid
    ResultSet generatedKeys= s.getGeneratedKeys();
    if( generatedKeys.next() )
    {
      long message_id= generatedKeys.getLong(1);
      
      ret= Commands.Codes.AIMessage + ";" + Commands.OK + ";1;" + message_id;
    }
    else
    {
      ret= "3;1;Datenbankproblem";
      System.out.println("Failed to obtain session id");
    }
    
    return ret;
  }
}
