package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import database.Db_connector;

public class NoOperation implements IControlCommandParser {
  
  public NoOperation(ControlCommand parser, Session sess, Db_connector db)
  {
    
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    return null;
  }
}
