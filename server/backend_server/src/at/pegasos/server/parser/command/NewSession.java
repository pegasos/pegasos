package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import at.pegasos.server.util.GroupMethods;
import at.pegasos.server.data.Session;
import at.pegasos.server.util.AIHelper;
import database.Db_connector;

public class NewSession implements IControlCommandParser {
  private final ControlCommand parser;
  private final Session data;
  private final Db_connector db;

  public NewSession(ControlCommand parser, Session sess, Db_connector db)
  {
    this.parser = parser;
    this.data = sess;
    this.db = db;
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret = "";
    // Start NEW Session <Activity>;<Starttimestamp in ms>;<Param1>;<Param2>;<use AI/param3>;[tzLocal];[endTime]

    if( t.length < 5 )
    {
      ret = "1;0;Session start failed!";
      parser.log("Starting session failed. Not enough parameters. Data was: " + Arrays.toString(t));
      return ret;
    }
    else
    {
      data.activity = Integer.parseInt(t[1]);
      if( t[2].equals("") )
        data.starttime = System.currentTimeMillis();
      else
        data.starttime = Long.parseLong(t[2]);
      data.param1 = Long.parseLong(t[3]);
      data.param2 = Long.parseLong(t[4]);
      data.param3 = Integer.parseInt(t[5]);

      // We have also been sent the local timeZone
      if( t.length >= 7 )
      {
        int tzLocal = Integer.parseInt(t[6]);
        if( t.length >= 8 )
        {
          long endTime = Long.parseLong(t[7]);
          return createSession(tzLocal, endTime);
        }
        else
          return startSession(tzLocal);
      }
      else
        return startSession();
    }
  }

  public String startSession()
  {
    int tzServer = TimeZone.getDefault().getOffset(data.starttime) / 1000 / 60 / 60 * 2;

    return startSession(tzServer, tzServer);
  }

  public String startSession(int tzLocal)
  {
    int tzServer = TimeZone.getDefault().getOffset(data.starttime) / 1000 / 60 / 60 * 2;

    return startSession(tzServer, tzLocal);
  }

  private String startSession(int tzServer, int tzLocal)
  {
    String ret;

    data.v[0] = 0;
    data.v[1] = 0;
    data.v[2] = 0;
    data.v[3] = 0;
    data.v[4] = 0;
    data.v[5] = 0;
    data.v[6] = 0;
    data.v[7] = 0;
    data.v[8] = 0;
    data.v[9] = 0;
    data.v[10] = 0;

    System.out.println("SESSION ACTIVITY:" + data.activity);
    System.out.println("SESSION STARTTIME:" + data.starttime);
    System.out.println("SESSION PARAM1:" + data.param1);
    System.out.println("SESSION PARAM2:" + data.param2);
    System.out.println("SESSION PARAM3:" + data.param3);

    try
    {
      String sql;
      ResultSet res = null;

      GroupMethods.getInstance(db).updateGroup(data);

      // sql = "insert into session (userid,activity,param1,param2,starttime,last_update,ai) values ("+user_id+","+activity+","+param1+","+param2+","+System.currentTimeMillis()+","+System.currentTimeMillis()+","+param3+");";
      sql = "insert into session (teamid,groupid,userid,activity,param1,param2,starttime,last_update,tz_server,tz_local,ai) values ("
              + data.team_id + "," + data.group_id + "," + data.user_id + "," + data.activity + "," + data.param1 + ","
              + data.param2 + "," + data.starttime + "," + data.starttime + "," + tzServer + "," + tzLocal + "," + data.param3 + ");";
      System.out.println(sql);
      res = db.sql(sql, 1);

      if( (res != null) && (res.getWarnings() != null) )
      {
        ret = "1;0;Database error";
        System.out.println("SQL WARNINGS!!:" + res.getWarnings());
      }
      else
      {
        res = db.sql("select max(session_id) from session where userid=" + data.user_id, 0);
        if( res.getWarnings() != null )
        {
          ret = "1;0;Database error";
          System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        }
        else
        {
          res.next();
          data.session_id = res.getLong(1);
          ret = "1;1;" + data.session_id + "" + ";message";
          ret += ";" + data.activity_sensordescription.get_activity_sensordesc(data.activity);

          parser.log("Session started with param0: " + data.activity + ", param1:" + data.param1 + "param2:" + data.param2);

          // START FEEDBACK MODULE
          if( data.param3 > 0 )
          {
            data.insert_data = 0;
            System.out.println("Starting Feedback Module!");
            parser.log("Starting Feedback Module");

            AIHelper.RunAI(data, parser, data.activity, (int) data.param1, (int) data.param2, data.session_id, (int) data.param3, 0, data.language);
          }
          else
          {
            data.insert_data = 1;
          }
          res.close();
        }
      }
    }
    catch( SQLException e )
    {
      ret = "1;0;SQL Exception";
      e.printStackTrace();
      System.out.println(e.getMessage());
    }
    data.tmp_fp_strides = -1;
    data.fp_strides_cul = 0;

    return ret;
  }

  private String createSession(int tzLocal, long endTime)
  {
    String ret;

    data.v[0] = 0;
    data.v[1] = 0;
    data.v[2] = 0;
    data.v[3] = 0;
    data.v[4] = 0;
    data.v[5] = 0;
    data.v[6] = 0;
    data.v[7] = 0;
    data.v[8] = 0;
    data.v[9] = 0;
    data.v[10] = 0;
    int tzServer = TimeZone.getDefault().getOffset(data.starttime) / 1000 / 60 / 60 * 2;

    System.out.println("SESSION ACTIVITY:" + data.activity);
    System.out.println("SESSION STARTTIME:" + data.starttime);
    System.out.println("SESSION PARAM1:" + data.param1);
    System.out.println("SESSION PARAM2:" + data.param2);
    System.out.println("SESSION PARAM3:" + data.param3);

    try
    {
      String sql;
      ResultSet res = null;

      GroupMethods.getInstance(db).updateGroup(data);

      // sql = "insert into session (userid,activity,param1,param2,starttime,last_update,ai) values ("+user_id+","+activity+","+param1+","+param2+","+System.currentTimeMillis()+","+System.currentTimeMillis()+","+param3+");";
      sql = "insert into session (teamid,groupid,userid,activity,param1,param2,starttime,last_update,endtime,tz_server,tz_local,ai) values ("
              + data.team_id + "," + data.group_id + "," + data.user_id + "," + data.activity + "," + data.param1 + ","
              + data.param2 + "," + data.starttime + "," + endTime + "," + endTime + "," + tzServer + "," + tzLocal + ",0);";
      System.out.println(sql);
      res = db.sql(sql, 1);

      if( (res != null) && (res.getWarnings() != null) )
      {
        ret = "1;0;Database error";
        System.out.println("SQL WARNINGS!!:" + res.getWarnings());
      }
      else
      {
        res = db.sql("select max(session_id) from session where userid=" + data.user_id, 0);
        if( res.getWarnings() != null )
        {
          ret = "1;0;Database error";
          System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        }
        else
        {
          res.next();
          data.session_id = res.getLong(1);
          ret = "1;1;" + data.session_id + "" + ";message";
          ret += ";" + data.activity_sensordescription.get_activity_sensordesc(data.activity);

          parser.log("Session created with param0: " + data.activity + ", param1:" + data.param1 + "param2:" + data.param2);

          // START FEEDBACK MODULE
          AIHelper.RunPostProcessor(data.session_id, parser.getProtocolParser());
          if( data.param3 > 0 )
          {
            data.insert_data = 0;
            System.out.println("Starting Feedback Module!");
            parser.log("Starting Feedback Module");

            AIHelper.RunAI(data, parser, data.activity, (int) data.param1, (int) data.param2, data.session_id, (int) data.param3, 0, data.language);
          }
          else
          {
            data.insert_data = 1;
          }
          res.close();
        }
      }
    }
    catch( SQLException e )
    {
      ret = "1;0;SQL Exception";
      e.printStackTrace();
      System.out.println(e.getMessage());
    }
    data.tmp_fp_strides = -1;
    data.fp_strides_cul = 0;

    return ret;
  }
}
