package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.IControlCommandParser;
import database.Db_connector;

public class StopSession implements IControlCommandParser {
  private ControlCommand parser;
  private Session data;
  private Db_connector db;
  
  public StopSession(ControlCommand parser, Session sess, Db_connector db)
  {
    this.parser= parser;
    this.data= sess;
    this.db= db;
  }

  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret;
    String sql; 
    
    parser.log("Stopping session");
    
    long time;
    if( t.length < 2 )
      time= System.currentTimeMillis();
    else
    {
      time= Long.parseLong(t[1]);
    }
      
    try
    { // STOP SESSION
      sql = "update session set endtime=" + time + " where session_id=" + data.session_id;
      ResultSet res = db.sql(sql, 1);
      
      if( (res != null) && (res.getWarnings() != null) )
      {
        ret = "31;0;0;";
        System.out.println("SQL WARNINGS!!:" + res.getWarnings());
        res.close();
      }
      else
      {
        if( t.length != 0 )
        {
          sql = "update session set last_update=" + time + " where session_id=" + data.session_id;
          res = db.sql(sql, 1);
          
          if( (res != null) && (res.getWarnings() != null) )
          {
            ret = "31;0;0;";
            System.out.println("SQL WARNINGS!!:" + res.getWarnings());
            res.close();
          }
          else
          {
            ret = "31;1;1;";
          }
        }
        else
        {
          ret = "31;0;0;";
          System.out.println("SQL WARNINGS!!:" + res.getWarnings());
          res.close();
        }
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      ret = "31;0;0;";
    }
    
    return ret;
  }
  
}