package at.pegasos.server.parser.command;

import java.io.IOException;
import java.sql.SQLException;

import at.pegasos.server.parser.*;
import at.pegasos.server.Commands.Codes;
import at.pegasos.server.data.Session;
import database.Db_connector;

public class TimeDiff implements IControlCommandParser {
  // private ControlCommand parser;
  // private Session data;
  // private Db_connector db;
  
  public TimeDiff(ControlCommand parser, Session sess, Db_connector db)
  {
    // this.parser= parser;
    // this.data= sess;
    // this.db= db;
  }
  
  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    String ret= "";
    System.out.println("'" + t[1] + "'");
    int action= Integer.parseInt(t[1]);
    
    switch( action )
    {
      case 1:
        long s= System.currentTimeMillis();
        long c= Long.parseLong(t[2]);
        long h= s - c;
        ret= Codes.TimeDiff + ";1;" + h + ";" + s;
        System.out.println(s + " " + c + " " + ret);
        break;
    }
    
    return ret;
  }
  
}
