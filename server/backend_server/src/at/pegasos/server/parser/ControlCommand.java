package at.pegasos.server.parser;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import at.pegasos.server.*;
import at.pegasos.server.data.Session;
import database.Db_connector;

public class ControlCommand implements Parser{
  private final Session data;
  private final BufferedWriter f_out;
  private final Db_connector db;

  private final MMAProtocolParser protocolParser;

  private IControlCommandParser[] parsers;

  public ControlCommand(Session data, BufferedWriter out, Db_connector db, MMAProtocolParser protocolParser) throws ServerRuntimeException
  {
    this.data = data;
    this.f_out = out;
    this.db = db;
    this.protocolParser = protocolParser;

    createTable();
  }

  public void log(String tx)
  {
    try
    {
      // cal = Calendar.getInstance();
      // f_out.write(sdf.format(cal.getTime())+":"+tx+"\n");
      f_out.write(System.currentTimeMillis() + ";" + data.user_id + ";" + data.session_id + ";" + tx + "\n");
      f_out.flush();
    }
    catch (IOException e)
    {
    }
  }

  public MMAProtocolParser getProtocolParser()
  {
    return protocolParser;
  }

  private void createTable() throws ServerRuntimeException
  {
    try
    {
      parsers= ControlCommandParserTable.createTable(this, data, db);
    }
    catch (IllegalArgumentException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (SecurityException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (InvocationTargetException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (NoSuchMethodException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
  }
  
  public static void testTable() throws ServerStartupException
  {
    try
    {
      Db_connector db= Db_connector.open_db();
      
      @SuppressWarnings("unused")
      IControlCommandParser[] parsers= ControlCommandParserTable.createTable(null, null, db);
    }
    catch (IllegalArgumentException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (SecurityException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (InvocationTargetException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (NoSuchMethodException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
  }
  
  public String parse(byte[] in, String line, int sensortype, int sensornr, int datapackages, long time_since_start) throws IOException
  {
    // System.out.println("SENSORTYPE:"+sensortype);
    String[] t = line.split("[;]"); // split user_data
    System.out.println("LINE:" + line + " /" + t.length + "/" + sensornr);

    String ret;
    try
    {
      ret = parsers[sensornr].parse(t);
    }
    catch (SQLException e2)
    {
      e2.printStackTrace();
      ret = "0;0;0;";
    }

    return ret;
  }
}
