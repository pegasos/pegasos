package at.pegasos.server.parser;

import java.io.IOException;

public interface Parser {
  public String parse(byte[] in,String line, int sensortype, int sensornr, int datapackages, long time_since_start) throws IOException;
}
