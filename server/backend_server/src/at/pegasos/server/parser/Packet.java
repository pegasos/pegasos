package at.pegasos.server.parser;

import java.io.FileInputStream;
import java.io.IOException;

public class Packet {
  public byte[] bytes;
  
  /**
   * Reads a packet of a file. Starting at the current position
   * 
   * @param in
   *          the file stream
   * @param b_len
   *          length of the packet (as two bytes, read from the input stream, yes you have to do
   *          that)
   * @throws IOException if reading fails
   */
  public Packet(FileInputStream in, byte[] b_len) throws IOException
  {
    int number_of_bytes= ((int) b_len[0] & 0xFF) | ((int) b_len[1] & 0xFF) << 8;
    
    this.bytes= new byte[number_of_bytes];
    
    this.bytes[0]= (byte) (number_of_bytes & 0xff); // Number of bytes in Package LOW
    this.bytes[1]= (byte) ((number_of_bytes >> 8) & 0xff); // Number of bytes in HIGH
    
    for(int i= 2; i < number_of_bytes; i++)
    {
      this.bytes[i]= (byte) in.read();
    }
    // in.read(this.bytes, 2, number_of_bytes - 2);
  }

  /**
   * Create a packet using a byte array. This byte array should contain the whole packet (header, footer, ...)
   * @param bytes content ohe packet
   */
  public Packet(byte[] bytes)
  {
    final int number_of_bytes = bytes.length;
    this.bytes = new byte[number_of_bytes];

    System.arraycopy(bytes, 0, this.bytes, 0, number_of_bytes);
    // in.read(this.bytes, 2, number_of_bytes - 2);
  }

  public int getSensorType()
  {
    return bytes[2] & 0xff;
  }

  public int getSensorNr()
  {
    return bytes[3]&31&0xff;
  }

  public long getTimeSinceStart()
  {
    return (this.bytes[5] & 0xFF | (this.bytes[6] & 0xFF) << 8 | (this.bytes[7] & 0xFF) << 16 | ((long) (this.bytes[8] & 0xFF)) << 24);
  }

  public String toString()
  {
    StringBuilder ret= new StringBuilder("[");
    for(int i= 0; i < bytes.length; i++)
    {
      if( i > 0 )
        ret.append(" ");
      ret.append(bytes[i]);
    }
    ret.append("]");

    return ret.toString();
  }
}