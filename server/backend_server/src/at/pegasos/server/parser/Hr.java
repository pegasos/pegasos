package at.pegasos.server.parser;


import java.io.IOException;
import java.sql.*;

import database.Db_connector;
import at.pegasos.server.data.Session;

public class Hr implements ISensorDataParser
{
  private int time_last_measurement;
  private long time_since_start_ret;
  private Session data;
  private Db_connector db;
  private int bc_ret;
  private PreparedStatement db_insert;
  private int inserts;
  
  public Hr(Session data, Db_connector db) throws SQLException
  {
    this.data= data;
    this.db= db;
    perpareStatement();
  }
  
  private void perpareStatement() throws SQLException
  {
    this.db_insert= this.db.createStatement("insert into hr (session_id,rec_time,hr,hr_var,v1,v2,v3,v4) values (?,?,?,?,?,?,?,?)");
  }
  
  @Override
  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,
    int datapackages, long time_since_start) throws IOException, SQLException
  {
    int hr=0; 
    int hr_var[]= new int[4];
    int hr_var_count= 0;
    
    inserts= 0;
    
    if( datapackages > 0 )
      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;
    
    for (int x=0;x<datapackages;x++)
    { 
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      db_insert.setLong(1, data.session_id);
      db_insert.setLong(2, data.starttime+time_since_start);
      
      hr=((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
      hr_var_count= (int)in[bc+5];
      if( hr_var_count > 4 )
        hr_var_count= 4; //drop additional values // TODO: not good to do this
      System.out.println("Hrv_count: " + hr_var_count);
      
      db_insert.setInt(3, hr);
      int idx= 5;
      
      int y;
      for(y= 0; y < hr_var_count; y++)
      {
        hr_var[y]=((int)in[bc+6+y*2]&0xFF)|((int)in[bc+7+y*2]&0xFF)<<8;
        db_insert.setInt(idx++, hr_var[y]);
      }
      for(; y < 4; y++)
      {
        db_insert.setNull(idx++, java.sql.Types.INTEGER);
      }
      
      db_insert.setInt(4, hr_var[0]); //TODO: this uses the last hr_var value for this iteration
      
      System.out.println("HR:"+hr+" HRV: {" + hr_var[0] + "," + hr_var[1] + ","+ hr_var[2] + ","+ hr_var[3] + "} STARTTIME:"+data.starttime+" TIME LAST MEASURMENT:"+time_last_measurement+" / LAST MEASUREMENT ABSOLUTE:"+time_since_start+" ");
      bc=bc+6+hr_var_count*2;
      
      if (data.insert_data==1)
      {
        // System.out.println(db_insert);
        db_insert.addBatch();
        inserts++;
      }
    }
    
    data.v[1]=hr;
    data.v[2]=hr_var[0];
    
    if( inserts>0 )
    {
      db_insert.executeBatch();
    }
    
    bc_ret= bc;
    time_since_start_ret= time_since_start;
    
    return null;
  }
  
  @Override
  public int getNewPosition()
  {
    return bc_ret;
  }

  @Override
  public long getNewTimeSinceStart()
  {
    return time_since_start_ret;
  }
}
