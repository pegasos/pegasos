package at.pegasos.server.parser;

import java.io.IOException;
import java.sql.SQLException;

public interface IControlCommandParser {
  
  public String parse(String[] t) throws IOException, SQLException;
}
