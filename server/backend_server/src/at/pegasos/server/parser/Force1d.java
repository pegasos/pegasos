package at.pegasos.server.parser;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.Db_connector;
import at.pegasos.server.data.Session;

public class Force1d implements ISensorDataParser
{
  private int time_last_measurement;
  private long time_since_start_ret;
  private Session data;
  private Db_connector db;
  private int bc_ret;
  private PreparedStatement db_insert;
  private int inserts;
  
  public Force1d(Session data, Db_connector db) throws SQLException
  {
    this.data= data;
    this.db= db;
    perpareStatement();
  }
  
  private void perpareStatement() throws SQLException
  {
    this.db_insert= this.db.createStatement("insert into force_1d(session_id,rec_time,force_1d, sensor_nr) values (?,?,?,?)");
  }
  
  @Override
  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,
    int datapackages, long time_since_start) throws IOException, SQLException
  {
    int force_1d= 0; // field: 0
    
    inserts= 0;
    
    if( datapackages > 0 )
      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      db_insert.setLong(1, data.session_id);
      db_insert.setLong(2, data.starttime+time_since_start);
      force_1d= ((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
      db_insert.setInt(3,force_1d);
      db_insert.setInt(4,sensornr);
      if (data.insert_data==1)
      {
        db_insert.addBatch();
        inserts++;
      }
      System.out.println("Force1d: force_1d:"+force_1d+ " ");
      bc= bc + 4;
    }
    
    /*
    sql="insert into force_1d (session_id,rec_time,force_1d,sensor_nr) values";
    data.acc_x=0; 
    for (int x=0;x<datapackages;x++)
    { 
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      data.acc_x=(short)(in[bc+3]&0xFF|in[bc+4]<<8);
      time_since_start+=time_last_measurement;
      sql+="("+data.session_id+","+(data.starttime+time_since_start)+","+data.acc_x+","+sensornr+")";
      
      if (x==(datapackages-1)) {sql+=";";} else {sql+=",";}
      
      System.out.println("LAST MEASUREMENT:"+time_last_measurement+" FORCE:"+data.acc_x);
      
      bc=bc+4;
    }
    */
    
    if( inserts>0 )
    {
      db_insert.executeBatch();
    }
    
    bc_ret= bc;
    time_since_start_ret= time_since_start;
    
    return null;
  }
  
  @Override
  public int getNewPosition()
  {
    return bc_ret;
  }
  
  @Override
  public long getNewTimeSinceStart()
  {
    return time_since_start_ret;
  }
}
