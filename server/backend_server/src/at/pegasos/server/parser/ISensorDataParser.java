package at.pegasos.server.parser;

import java.io.IOException;
import java.sql.SQLException;

public interface ISensorDataParser {
  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr, int datapackages, long time_since_start) throws IOException, SQLException;
  public int getNewPosition();
  public long getNewTimeSinceStart();
}
