package at.pegasos.server.parser;

import at.pegasos.server.*;
import at.pegasos.server.data.*;
import at.pegasos.server.parser.command.*;
import at.pegasos.tool.util.*;
import database.*;

import java.io.*;
import java.security.*;
import java.sql.*;
import java.util.*;

public class MMAProtocolParser {

  private final Db_connector db;
  // for logging
  private final BufferedWriter f_out;
  private final ControlCommand controll_parser;
  private final SensorDataParser sensor_parser;
  private final Session data;
  /**
   * Time since the start of the session (last received)
   */
  public long time_since_start = 0;
  /**
   * Time when the last update was performed
   */
  public long last_insert = 0;
  private ResultSet res = null;

  public MMAProtocolParser(Db_connector d, BufferedWriter bf) throws ServerRuntimeException
  {
    this.db = d;
    this.f_out = bf;

    data = new Session();
    data.v = new int[11];
    data.v_last = new int[11];

    for(int i = 0; i < 11; i++)
    {
      data.v_last[i] = -1;
    }

    controll_parser = new ControlCommand(data, f_out, d, this);
    sensor_parser = new SensorDataParser(data, f_out, d);
  }

  static String sha1(String input) throws NoSuchAlgorithmException
  {
    MessageDigest mDigest = MessageDigest.getInstance("SHA1");
    byte[] result = mDigest.digest(input.getBytes());
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < result.length; i++)
    {
      sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
    }

    return sb.toString();
  }

  /**
   *
   */
  public static long getLastTimestamp(File file) throws IOException
  {
    RandomAccessFile raf = new RandomAccessFile(file, "r");
    long maxTime = 0;

    byte[] buffer = new byte[2];

    raf.seek(file.length() - 2);
    raf.read(buffer, 0, 2);

    int footer = ((int) buffer[0] & 0xFF) | ((int) buffer[1] & 0xFF) << 8;

    raf.seek(file.length() - footer);

    byte[] b_len = new byte[2];

    long pos = raf.getChannel().position();

    int pcount = 0;
    // read in some packets
    while( pcount < 30 && (b_len[0] = (byte) raf.read()) != -1 )
    {
      b_len[1] = (byte) raf.read();
      int packet_length = ((int) b_len[0] & 0xFF) | ((int) b_len[1] & 0xFF) << 8;
      if( packet_length != footer )
        return 0;

      buffer = new byte[footer + 2];
      buffer[0] = b_len[0];
      buffer[1] = b_len[1];
      raf.read(buffer, 2, footer);

      Packet p = new Packet(buffer);
      // System.out.println(String.format("%08X", pos) + " " + p + " " + p.bytes.length + " " + p.getTimeSinceStart());
      if( p.getSensorType() != 0 )
      {
        long t = p.getTimeSinceStart();
        if( t > maxTime )
        {
          maxTime = t;
        }
      }
      else
      {
        String line = new String(buffer, 2, footer - 2);
        System.out.println("Controll: " + line);
      }

      // return p.getTimeSinceStart();
      pcount++;

      if( pos - packet_length - 4 > 0 )
      {
        raf.seek(pos - 2);
        // System.out.println("Go to " + (pos - 2));

        raf.read(buffer, 0, 2);
        footer = ((int) buffer[0] & 0xFF) | ((int) buffer[1] & 0xFF) << 8;

        raf.seek(pos - footer);

        pos = raf.getChannel().position();
      }
      else
      {
        break;
      }
    }

    return maxTime;
  }

  public Session getSession()
  {
    return data;
  }

  public void log(String tx)
  {
    try
    {
      f_out.write(System.currentTimeMillis() + ";" + tx + "\n");
      f_out.flush();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }

  public String parse(byte[] in, String line) throws IOException
  {
    String ret;

    // HEADER DATA (first two bytes cut off!!)
    int sensortype   = in[0] & 0xff;
    int sensornr     = in[1] & 31 & 0xff;
    int datapackages = ((int) in[2] & 0xFF) | ((int) ((in[1] & 0xff) / 32) << 8);
    time_since_start = ((int) in[3] & 0xFF) | ((int) in[4] & 0xFF) << 8 | ((int) in[5] & 0xFF) << 16 | ((long) in[6] & 0xFF) << 24;

    // System.out.println("Number_of_bytes:"+number_of_bytes);
		/*System.out.println("Sensortype:"+sensortype);
		System.out.println("Sensornr:"+sensornr);
		System.out.println("Packages:"+datapackages);
		System.out.println("NEW PACKAGE:TIME SINCE START:"+time_since_start);
		System.out.print("b0:"+in[0]);
		System.out.print(" b1:"+in[1]);
		System.out.print(" b2:"+in[2]);
		System.out.print(" b3:"+in[3]);
		System.out.print(" b4:"+in[4]);
		System.out.print(" b5:"+in[5]);
		System.out.println(" b6:"+in[6]);*/

    // CHECK IF LOGGED IN --> if not return
    if( (sensortype != 0) && (data.user_id == 0) )
    {
      return "4;1;Login required!";
    }
    if( (sensortype != 0) && (data.session_id == 0) )
    {
      return "4;0;Session required!";
    }

    // STATUS CHANGES
    String sql = null;
    if( sensortype == 0 )
    {
      ret = controll_parser.parse(in, line, sensortype, sensornr, datapackages, time_since_start);
    } // ENDE SENSORTYPE=0 (STATUS CHANGE)
    else
    {
      // SENSOR DATA
      ret = sensor_parser.parse(in, line, sensortype, sensornr, datapackages, time_since_start);

      System.out.println(
          "v1=" + data.v[1] + ",v2=" + data.v[2] + ",v3=" + data.v[3] + ",v4=" + data.v[4] + ",v5=" + data.v[5] + ",v6=" + data.v[6]
              + ",v7=" + data.v[7] + " T: " + System.currentTimeMillis() + " last_insert: " + last_insert);

      // update actual data in session table
      if( System.currentTimeMillis() - last_insert > 500 )
      {
        last_insert = System.currentTimeMillis();
        sql = "update session set last_update=" + System.currentTimeMillis() + ",last_gps_alt_cm=" + data.gps_alt + ",last_gps_speed_cms="
            + data.gps_speed + ",last_gps_accur=" + data.gps_acc + ",last_gps_bearing=" + data.gps_bear + ",last_gps_lat="
            + data.gps_lat * 100000000 + ",last_gps_lon=" + data.gps_lon * 100000000;
        // sql+=",v1="+v[1]+",v2="+v[2]+",v3="+v[3]+",v4="+v[4]+",v5="+v[5]+",v6="+v[6]+",v7="+v[7]+",v8="+v[8]+",v9="+v[9];
        for(int i = 1; i < 8; i++)
        {
          if( data.v[i] != data.v_last[i] )
            sql += ",v" + i + "=" + data.v[i];
          data.v_last[i] = data.v[i];
          // sql+=",v1="+v[1]+",v2="+v[2]+",v3="+v[3]+",v4="+v[4]+",v5="+v[5]+",v6="+v[6]+",v7="+v[7];
        }

        sql += " where session_id=" + data.session_id;
        res = db.sql(sql, 1);
        System.out.println(sql);
      }
    }

    // Feedback available?
    if( ret == null )
    {
      sql = "select message,feedback_id,stop_session from feedback where session_id=" + data.session_id
          + " and (ttime=0 or ttime is null) order by itime ASC";
      res = db.sql(sql, 0);
      try
      {
        if( res.next() )
        {
          System.out.println(res);
          String msg_user = res.getString(1);
          long msg_id = res.getLong(2);
          int stop_session = res.getInt(3);
          sql = "update feedback set ttime=" + System.currentTimeMillis() + " where feedback_id=" + msg_id;
          res.close();
          res = db.sql(sql, 1);

          if( msg_user.startsWith(";c;") )
          {
            ret = msg_user.substring(3);
          }
          else if( stop_session == 0 )
          {
            ret = "3;1;" + msg_user;
          }
          else
          {
            ret = "4;1;" + msg_user;
          }
        }
        else
        {
          System.out.println("No messages for " + data.session_id);
          ret = "1000;0;no message";
        }
      }
      catch( SQLException e )
      {
        ret = "3;1;Database error";
      }
    }

    // System.out.println("Answer: " + ret);
    return ret;
  }

  /**
   *
   */
  public void readFile(File file, Metadata meta) throws IOException, SQLException
  {
    if( meta != null )
    {
      // TODO: security check here
      int param0 = Integer.parseInt(meta.getValue("Param0"));
      int param1 = Integer.parseInt(meta.getValue("Param1"));
      int param2 = Integer.parseInt(meta.getValue("Param2"));

      // TODO: allow user change? Ie allow that we upload files for a different user?
      // For now we are ignoring the user part of the meta data
      // String user= meta.getValue("User");

      if( meta.hasKey("club") || meta.hasKey("group") )
      {
        if( // for club two versions exist: older integer and newer boolean
            meta.hasKey("club") && (meta.getValue("club").equals("true") || meta.getValue("club").equals("1")) ||
            // team / group is here for future use
            meta.hasKey("group") && meta.getValue("group").equals("true") )
        {
          String sql = "select teamid,groupid from teams_memberships where deleted=0 and userid=" + data.user_id
              + " and teamid in (select id from teams where type=1);";
          System.out.println(sql);
          res = db.sql(sql, 0);
          if( res.next() )
          {
            getSession().team_id = res.getInt(1);
            getSession().group_id = res.getInt(2);
            System.out.println(sql);
            System.out.println("TEAM:" + data.team_id);
            System.out.println("GROUP:" + data.group_id);
          }
        }
      }

      getSession().starttime = Long.parseLong(meta.getValue("Starttime"));
      // parser.getSession().starttime= System.currentTimeMillis() - 10*60000;

      getSession().activity = param0;
      getSession().param1 = param1;
      getSession().param2 = param2;

      // No AI
      getSession().param3 = 0;

      NewSession ns = new NewSession(controll_parser, getSession(), db);

      String res = null;
      int tzLocal;
      if( meta.hasKey("timezone") )
      {
        tzLocal = Integer.parseInt(meta.getValue("timezone"));
        res = ns.startSession(tzLocal);
      }
      else
        res = ns.startSession();

      System.out.println("startSession parse: " + res);
    }

    db.setAutoCommit(false);
    FileInputStream in = new FileInputStream(file);
    Packet p;

    // long pcount= 0;

    byte[] b_len = new byte[2];

    // long pos= in.getChannel().position();
    while( (b_len[0] = (byte) in.read()) != -1 )
    {
      b_len[1] = (byte) in.read();
      // System.out.println("Packet length: " + (((int)b_len[0]&0xFF)|((int)b_len[1]&0xFF)<<8));
      p = new Packet(in, b_len);
      // System.out.println(String.format("%08X", pos) + " " + p + " " + p.bytes.length);
      // pcount++;
      // pos= in.getChannel().position();

      byte[] buffer = new byte[p.bytes.length - 2];
      System.arraycopy(p.bytes, 2, buffer, 0, p.bytes.length - 2);

      int footer = 0;
      int number_of_bytes = p.bytes.length;

      if( number_of_bytes > 4 )
      {
        footer = ((int) buffer[number_of_bytes - 4] & 0xFF) | ((int) buffer[number_of_bytes - 3] & 0xFF) << 8;
      } // 18.03
      if( number_of_bytes == footer && (buffer[2] & 0xff) > 0 || (buffer[2] & 0xff) == 0 )
      {
        String line = new String(buffer, 0, number_of_bytes - 3); // ohne Footer, CR

        parse(buffer, line);
        System.out.println("NEW PACKAGE:TIME SINCE START:" + time_since_start);
      }
      else
      {
        f_out.write("ANZAHLZEICHEN:" + number_of_bytes + " READ ALREADY:" + number_of_bytes + " FOOTER SAYS:" + footer + " bytes BUFFER[2]="
            + buffer[2] + "\n");

        // TODO: notify client about sending error?
        // out.writeBytes("3;1;Send error!;\n"); // 18.03
      }

      // Thread.sleep(1);
    }

    in.close();

    if( meta != null )
    {
      long time = getSession().starttime + time_since_start;

      String sql = "update session set endtime=" + time + ", last_update = " + time + " where session_id=" + getSession().session_id;
      ResultSet res = db.sql(sql, 1);

      if( (res != null) && (res.getWarnings() != null) )
      {
        // String ret = "31;0;0;";
        f_out.write("SQL WARNINGS!!:" + res.getWarnings() + "\n");
        res.close();
      }
    }
    db.setAutoCommit(true);
  }
}
