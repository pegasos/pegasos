package at.pegasos.server.parser;


import java.io.IOException;
import java.sql.*;

import database.Db_connector;
import at.pegasos.server.data.Session;

public class StrokeRate implements ISensorDataParser
{

  private int time_last_measurement;
  private long time_since_start_ret;
  private Session data;
  private Db_connector db;
  private int bc_ret;
  private PreparedStatement db_insert;
  private int inserts;
  
  public StrokeRate(Session data, Db_connector db) throws SQLException
  {
    this.data= data;
    this.db= db;
    perpareStatement();
  }
  
  private void perpareStatement() throws SQLException
  {
    this.db_insert= this.db.createStatement("insert into stroke_rate(session_id,rec_time,stroke_count,stroke_rate_pmin) values (?,?,?,?)");
  }
  
  @Override
  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,
    int datapackages, long time_since_start) throws IOException, SQLException
  {
    int stroke_count= 0; // field: 0
    int stroke_avg= 0; // field: 1
    
    inserts= 0;
    
    if( datapackages > 0 )
      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      db_insert.setLong(1, data.session_id);
      db_insert.setLong(2, data.starttime+time_since_start);
      stroke_count= ((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
      db_insert.setInt(3,stroke_count);
      stroke_avg= ((int)in[bc+5]&0xFF)|((int)in[bc+6]&0xFF)<<8;
      db_insert.setInt(4,stroke_avg);
      
      if (data.insert_data==1)
      {
        db_insert.addBatch();
        inserts++;
      }
      
      System.out.println("Ruderschl�ge anzahl/avg:"+stroke_count+"/"+stroke_avg+" STARTTIME:"+data.starttime+" TIME LAST MEASURMENT:"+time_last_measurement+" / LAST MEASUREMENT ABSOLUTE:"+time_since_start+" ");
      bc= bc + 6;
    }
    
    /*
    sql="insert into stroke_rate (session_id,rec_time,stroke_rate_pmin,stroke_count) values";
        int stroke_count=0; 
        int stroke_avg=0;
        for (int x=0;x<datapackages;x++)
        { 
          time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
          stroke_count=((int)in[bc+3]&0xFF)|((int)in[bc+4]&0xFF)<<8;
          stroke_avg=((int)in[bc+5]&0xFF)|((int)in[bc+6]&0xFF)<<8;
          time_since_start+=time_last_measurement;
          
          sql+="("+data.session_id+","+(data.starttime+time_since_start)+","+stroke_avg+","+stroke_count+")";
          if (x==(datapackages-1)) {sql+=";";} else {sql+=",";}
          
          System.out.println("Ruderschl�ge anzahl/avg:"+stroke_count+"/"+stroke_avg+" STARTTIME:"+data.starttime+" TIME LAST MEASURMENT:"+time_last_measurement+" / LAST MEASUREMENT ABSOLUTE:"+time_since_start+" ");
          bc=bc+6;
          data.v[6]=stroke_count;
          data.v[7]=stroke_avg;
          }
     */
    
    if( inserts>0 )
    {
      db_insert.executeBatch();
    }
    
    data.v[6]=stroke_count;
    data.v[7]=stroke_avg;
    
    bc_ret= bc;
    time_since_start_ret= time_since_start;
    
    return null;
  }
  
  @Override
  public int getNewPosition()
  {
    return bc_ret;
  }
  
  @Override
  public long getNewTimeSinceStart()
  {
    return time_since_start_ret;
  }
}
