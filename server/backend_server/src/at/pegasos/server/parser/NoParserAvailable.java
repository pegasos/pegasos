package at.pegasos.server.parser;

import java.io.IOException;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import database.Db_connector;

public class NoParserAvailable implements ISensorDataParser {
  private int bc_ret;
  private long time_since_start_ret;

  public NoParserAvailable(Session sess, Db_connector db)
  {

  }

  @Override
  public String parse(byte[] in, int bc, String line, int sensortype,
      int sensornr, int datapackages, long time_since_start)
      throws IOException, SQLException
  {
    bc_ret= bc;
    time_since_start_ret= time_since_start;
    return null;
  }

  @Override
  public int getNewPosition()
  {
    return bc_ret;
  }

  @Override
  public long getNewTimeSinceStart()
  {
    return time_since_start_ret;
  }

}
