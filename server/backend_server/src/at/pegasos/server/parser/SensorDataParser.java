package at.pegasos.server.parser;


import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import at.pegasos.server.data.Session;
import at.pegasos.server.ServerStartupException;
import at.pegasos.server.ServerRuntimeException;
import database.Db_connector;

public class SensorDataParser implements Parser {
  private Session data;
  String ret= null;
  private BufferedWriter f_out;
  private Db_connector db;
  private ISensorDataParser[] parsers;
  
  public SensorDataParser(Session data, BufferedWriter out, Db_connector db) throws ServerRuntimeException
  {
    this.data= data;
    this.f_out= out;
    this.db= db;
    createTable();
  }
  
  protected void log(String tx)
  {
    try
    {
      f_out.write(System.currentTimeMillis() + ";" + tx + "\n");
      f_out.flush();
    }
    catch (IOException e)
    {
    }
  }
  
  private void createTable() throws ServerRuntimeException
  {
    parsers= new ISensorDataParser[20];
    try
    {
      parsers= SensorDataParserTable.createTable(data, db);
    }
    catch (IllegalArgumentException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (SecurityException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (InvocationTargetException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (NoSuchMethodException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
      throw new ServerRuntimeException("Creating parsers for Session " + data.session_id);
    }
  }
  
  public static void testTable() throws ServerStartupException
  {
    try
    {
      Db_connector db= Db_connector.open_db();
      SensorDataParserTable.createTable(null, db);
    }
    catch (IllegalArgumentException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (SecurityException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (InvocationTargetException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (NoSuchMethodException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
      throw new ServerStartupException();
    }
  }
  
  @Override
  public String parse(byte[] in, String line, int sensortype, int sensornr,
    int datapackages, long time_since_start) throws IOException
  {
    String ret;
    int bc;
    // System.out.println(" AT "+time_since_start+" RECEIVED:"+datapackages+" SENSORVALUES FROM SENSORNR:"+sensornr+" TYPE:"+sensortype);
    bc=6;
    
    try
    {
      ret= parsers[sensortype].parse(in, bc, line, sensortype, sensornr, datapackages, time_since_start);
      bc+= parsers[sensortype].getNewPosition();
      time_since_start+= parsers[sensortype].getNewTimeSinceStart();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      System.out.println("SQL WARNINGS!!: "+ db.getWarnings());
      ret= "3;1;Database error";
    }
    catch(ArrayIndexOutOfBoundsException e )
    {
      e.printStackTrace();
      ret= "3;1;Data parse error";
    }
    
    try
    {
      ResultSet res;
      if( data.insert_data != 1 )
      {
        res = db.sql("select v10 from session where session_id=" + data.session_id, 0);
        if( res.first() )
        {
          data.insert_data = res.getInt(1);
          System.out.println("select v10 from session where session_id=" + data.session_id + " INSERTDATA:" + data.insert_data);
        }
      }
    }
    catch (SQLException e)
    {
      System.out.println("SQL Exception!!:"+e.getMessage());
      ret= "3;1;Database error";
    }
    
    return ret;
  }
}
