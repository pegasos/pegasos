package at.pegasos.server.parser;

import java.io.IOException;
import java.sql.SQLException;

import at.pegasos.server.data.Session;
import database.Db_connector;

public class NoCommandAvailable implements IControlCommandParser {

  public NoCommandAvailable(ControlCommand parser, Session sess, Db_connector db)
  {

  }
  
  @Override
  public String parse(String[] t) throws IOException, SQLException
  {
    return null;
  }
}
