package at.pegasos.server.parser;


import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.*;
import database.Db_connector;
import at.pegasos.server.data.Session;

public class Setting implements ISensorDataParser {

  private int time_last_measurement;
  private long time_since_start_ret;
  private Session data;
  private Db_connector db;
  private int bc_ret;
  private PreparedStatement db_insert;
  private int inserts;
  
  private final static Charset cs= Charset.forName("UTF-8");
  
  public Setting(Session data, Db_connector db) throws SQLException
  {
    this.data= data;
    this.db= db;
    perpareStatement();
  }
  
  private void perpareStatement() throws SQLException
  {
    this.db_insert= this.db.createStatement("insert into setting(session_id,rec_time,name,value) values (?,?,?,?)");
  }
  
  @Override
  public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,
    int datapackages, long time_since_start) throws IOException, SQLException
  {
    byte ln= 0; // field: 0
    byte lv= 0; // field: 1
    byte[] bname; // field: 2
    byte[] bvalue; // field: 3
    
    inserts= 0;
    
    if( datapackages > 0 )
      time_since_start-= ((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8;
    
    for (int x=0;x<datapackages;x++)
    {
      time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!
      time_since_start+=time_last_measurement;
      
      db_insert.setLong(1, data.session_id);
      db_insert.setLong(2, data.starttime+time_since_start);
      
      ln= in[bc+3];
      lv= in[bc+4];
      
      bname= new byte[ln];
      bvalue= new byte[lv];
      
      System.arraycopy(in, bc+5, bname, 0, ln);
      System.arraycopy(in, bc+5+ln, bvalue, 0, lv);
      
      db_insert.setString(3, new String(bname, cs));
      db_insert.setString(4, new String(bvalue, cs));
      
      // We do insert all settings irrespective of their time.
      // The reason for this is simple: Sensor might report their settings before the training has actually started. 
      // if( data.insert_data == 1 )
      {
        db_insert.addBatch();
        inserts++;
      }
      
      System.out.printf("Setting: ln:%d lv:%d name:%s value:%s\n", ln, lv, new String(bname, cs), new String(bvalue, cs));
      bc= bc + 94;
    }
    
    if( inserts>0 )
    {
      db_insert.executeBatch();
    }
    
    bc_ret= bc;
    time_since_start_ret= time_since_start;
    
    return null;
  }
  
  @Override
  public int getNewPosition()
  {
    return bc_ret;
  }
  
  @Override
  public long getNewTimeSinceStart()
  {
    return time_since_start_ret;
  }
}
