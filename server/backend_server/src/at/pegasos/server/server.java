package at.pegasos.server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import at.pegasos.server.controller.ClientHandler;
import at.pegasos.server.parser.ControlCommand;
import at.pegasos.server.parser.SensorDataParser;

/*
- Server benutzt Port je nachdem ob MAINZ;UNIWIEN,LIVE-DEMO
- Jede Client-Anforderung wird von einem Thread des erzeugten Thread-Pools
  behandelt. Server-Socket kann mit Strg+C geschlossen werden oder vom Client mit
  dem Wert 'Exit'.
*/

public class server {
	
    private static int port=64111;private static String log_name="backend_produktiv.log";   // INITIAL UNI WIEN
	public static BufferedWriter fstream;
	
	
	
	public static void main( String[] args ) throws ServerStartupException {
		
		final ExecutorService pool;
	    final ServerSocket serverSocket;
	    String var = "C";
	    String zusatz;
	    
	    port= Config.PORT;
	    log_name= "backend_" + Config.NAME + ".log";
	    
	    System.out.println("Starting " + Config.NAME);
	    
	    SensorDataParser.testTable();
	    ControlCommand.testTable();
	    
	    if (args.length > 1 ) var = args[0].toUpperCase(); // THREAD POOL SIZE	
	    
	    if (var == "C") {
	        pool = Executors.newCachedThreadPool();       //Liefert einen Thread-Pool, dem bei Bedarf neue Threads hinzugef�gt werden. Vorrangig werden jedoch vorhandene freie Threads benutzt.
	        zusatz = "CachedThreadPool";
	      } else {
	        int poolSize = 100;
	        pool = Executors.newFixedThreadPool(poolSize); //Liefert einen Thread-Pool f�r maximal poolSize Threads
	        zusatz = "poolsize="+poolSize;
	      }
	    
	    try {
	    	System.out.println("Welcome to the MMA Backend on Port Nr:"+port);
	    	serverSocket = new ServerSocket(port);  // create socket
	    	fstream = new BufferedWriter(new FileWriter(log_name, true)); // open log-file
	    	fstream.write(" ------------ Server started ---------------------\n");
	    	//Thread zur Behandlung der Client-Server-Kommunikation, der Thread-
	    	//Parameter liefert das Runnable-Interface (also die run-Methode f�r t1).
	    	Thread t1 = new Thread(new NetworkService(pool,serverSocket,fstream));
	    	System.out.println("Start NetworkService, " + zusatz + ", Thread: "+Thread.currentThread());
	    	t1.start();    //Start der run-Methode von NetworkService: warten auf Client-request
      	    Runtime.getRuntime().addShutdownHook(     //reagiert auf Strg+C, der Thread(Parameter) darf nicht gestartet sein
	    	      new Thread() {
	    	        public void run() {
	    	          System.out.println("Strg+C, pool.shutdown");
	    	          pool.shutdown();  //keine Annahme von neuen Anforderungen
	    	          try {fstream.write(" ------------ Server stopped ---------------------\n");} catch (IOException e1) {}
	    	          
	    	          try {
	    	            pool.awaitTermination(4L, TimeUnit.SECONDS);             //warte maximal 4 Sekunden auf Beendigung aller Anforderungen
	    	            if (!serverSocket.isClosed()) {
	    	              System.out.println("ServerSocket close");
	    	              serverSocket.close();
	    	            }	
	    	          } catch ( IOException e ) { }
	    	          catch ( InterruptedException ei ) { }
	    	        }
	    	      }
	    	    );
	    } catch (IOException ioe) {
	      System.out.println("IOException on socket listen: " + ioe);
	      ioe.printStackTrace();
	    }     
	}  // END MAIN
}	
	
	//Thread bzw. Runnable zur Entgegennahme der Client-Anforderungen oder extends Thread
	class NetworkService implements Runnable { 
	  private final ServerSocket serverSocket;
	  private final ExecutorService pool;
	  private final BufferedWriter fstream;
	  
	  public NetworkService(ExecutorService pool,ServerSocket serverSocket,BufferedWriter fstream) {
	    this.serverSocket = serverSocket;
	    this.pool = pool;
	    this.fstream=fstream;
	  }
	  
	  public void run() { // run the service
	    try {
	      //Endlos-Schleife: warte auf Client-Anforderungen
	      //Abbruch durch Strg+C oder Client-Anforderung 'Exit',
	      //dadurch wird der ServerSocket beendet, was hier zu einer IOException
	      //f�hrt und damit zum Ende der run-Methode mit vorheriger Abarbeitung der
	      //finally-Klausel.
	      while ( true ) {
	        Socket cs = serverSocket.accept();  //warten auf Client-Anforderung
	        pool.execute(new ClientHandler(serverSocket,cs,fstream));
	      }
	    } catch (IOException ex) {
	      System.out.println("--- Interrupt NetworkService-run");
	    }
	    /*catch (IllegalArgumentException e)
      {
        System.out.println("Haha der Server startet nicht, weil du was verbockt hast" + e.getMessage()); //TODO !!!!!!!!!!!!!!!!!!!!!
        e.printStackTrace();
      } catch (SecurityException e)
      {
        System.out.println("Haha der Server startet nicht, weil du was verbockt hast" + e.getMessage()); //TODO !!!!!!!!!!!!!!!!!!!!!
        e.printStackTrace();
      }*/
	    catch (ServerRuntimeException e)
      {
        System.err.println("Runtime Error");
        e.printStackTrace();
      }
	    finally {
	      System.out.println("--- Ende NetworkService(pool.shutdown)");
	      pool.shutdown();  //keine Annahme von neuen Anforderungen
	      try {
	        //warte maximal 4 Sekunden auf Beendigung aller Anforderungen
	        pool.awaitTermination(4L, TimeUnit.SECONDS);
	        if ( !serverSocket.isClosed() ) {
	          System.out.println("--- Ende NetworkService:ServerSocket close");
	          serverSocket.close();
	        }
	      } catch ( IOException e ) { }
	      catch ( InterruptedException ei ) { }
	    }
	  }
	}
	 
	
	
	
	
	

	

	

	
