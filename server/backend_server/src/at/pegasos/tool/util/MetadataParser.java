package at.pegasos.tool.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MetadataParser {
  
  public static Metadata parse(List<String> lines)
  {
    Metadata ret= new Metadata();
    for(String line : lines)
    {
      String[] d= line.split(":");
      ret.kv.put(d[0], d[1]);
    }
    return ret;
  }
  
  /**
   * Parse meta data information from a string containing multiple lines separated by '\n'
   * @param lines
   * @return
   */
  public static Metadata parse(String lines)
  {
    String[] ll= lines.split("\n");
    Metadata ret= new Metadata();
    for(String line : ll)
    {
      String[] d= line.split(":");
      ret.kv.put(d[0], d[1]);
    }
    return ret;
  }
  
  public static Path metaFileFromFile(String filename)
  {
    Path path= Paths.get(filename).toAbsolutePath();
    
    Path folder= path.getParent();
    
    String fn= path.getFileName().toString();
    fn= fn.substring(0, fn.length() - 4);
    
    return folder.resolve(fn + ".meta");
  }
}
