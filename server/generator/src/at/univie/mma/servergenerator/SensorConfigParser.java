package at.univie.mma.servergenerator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import at.univie.mma.servergenerator.token.Field;
import at.univie.mma.servergenerator.token.BlockSessionInsert;
import at.univie.mma.servergenerator.token.FieldDb;
import at.univie.mma.servergenerator.token.FieldSession;
import at.univie.mma.servergenerator.token.Options;

public class SensorConfigParser {
  private final static String BLOCK_SEPARATOR= "----------------";
  
  
  BufferedReader reader; 
  ArrayList<Field> fields;
  ArrayList<FieldDb> fields_session_db;
  ArrayList<FieldSession> fields_session_code;
  BlockSessionInsert fields_session;
  ArrayList<String> session_insert;
  private BlockSessionInsert extra_code;
  
  private int line_no;

  private String lastline_unconsumed;

  private Options options;

  public SensorConfigParser(String filename) throws FileNotFoundException {
    FileReader inputStream = new FileReader(filename);
    reader= new BufferedReader(inputStream);
    
    fields= new ArrayList<Field>();
    fields_session_db= new ArrayList<FieldDb>();
    fields_session_code= new ArrayList<FieldSession>();
    fields_session= null;
    extra_code= null;
    session_insert= new ArrayList<String>();
  }
  
  public void parse() throws IOException, ParserException {
    line_no= 1;
    
    /*String line= reader.readLine();
    if( line == null )
      throw new ParserException("First line missing.");
    
    if( !line.equals("length: fixed") )
      throw new ParserException("First line needs to describe protocoltype-length");
    
    line_no++;*/
    
    parseOptions();

    if( !options.isFixedLength() )
      throw new ParserException("Its required to describe protocoltype-length");
    
    parseFixed();
    
    parseSessionInsert();
    
    parseExtraCode();
    
    parseSessionDataValues();
    
    parseSessionDataSession();
  }
  
  private void parseOptions() throws IOException, ParserException
  {
    ArrayList<String> opts= new ArrayList<String>();
    String line;
    int start= line_no;
    
    while( (line= reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      opts.add(line);
      line_no++;
    }
    
    this.options= new Options(start, opts);
  }
  
  private void parseFixed() throws IOException, ParserException
  {
    Block b= parseMandatoryBlock("fields:", "Fields definition");
    System.out.println(b);
    
    int blno= b.startline; //block line number
    int no= 0;
    for(String line : b.lines)
    {
      String[] def= line.split("\t");
      if( def.length < 3 )
        throw new ParserException("Error in line " + blno + ": Expected <name> <dbtype> <rawtype> <options>");
      Field f= new Field();
      f.number= no++;
      f.name= def[0];
      
      f.db_type= Type.fromString(def[1]);
      f.raw_type= Type.fromString(def[2]);
      
      if( def.length >= 4 )
      {
        f.options= f.new Options();
        String[] opts= def[3].split(",");
        for(String opt : opts )
        {
          f.options.addOption(opt);
        }
      }
      
      fields.add(f);
    }
  }
  
  private Block parseMandatoryBlock(String start_token, String name) throws IOException, ParserException
  {
    String line;
    ArrayList<String> lines= new ArrayList<String>();
    int start, end;
    
    if( lastline_unconsumed != null )
    {
      line= lastline_unconsumed;
      lastline_unconsumed= null;
    }
    else
    {
      while( (line= reader.readLine()) != null && !line.equals(start_token) )
      {
        line_no++;
      }
    }
    
    if( line == null || !line.equals(start_token) )
      throw new ParserException("Could not find mandatory block " + name);
    
    start= line_no;
    
    while( (line= reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      lines.add(line);
      line_no++;
    }
    
    end= line_no;
    
    return new Block(start, end, lines);
  }

  private Block parseOptionalBlock(String start_token, String name) throws IOException, ParserException
  {
    String line;
    ArrayList<String> lines= new ArrayList<String>();
    int start, end;
    
    if( lastline_unconsumed != null )
    {
      if( lastline_unconsumed.equals(start_token) )
      {
        lastline_unconsumed= null;
      }
      else
        return null;
    }
    else
    {
      if( (line= reader.readLine()) == null || !line.equals(start_token) )
      {
        lastline_unconsumed= line;
        return null;
      }
      
      line_no++;
    }
    
    start= line_no;
    
    while( (line= reader.readLine()) != null && !line.equals(BLOCK_SEPARATOR) )
    {
      lines.add(line);
      line_no++;
    }
    
    end= line_no;
    
    return new Block(start, end, lines);
  }
  
  private void parseSessionInsert() throws IOException, ParserException
  {
    Block b= parseOptionalBlock("session insert:", "code executed when session insert is =1");
    if( b != null )
    {
      this.fields_session= new BlockSessionInsert();
      this.fields_session.lines= b.lines;
    }
  }
  
  private void parseExtraCode() throws IOException, ParserException
  {
    Block b= parseOptionalBlock("extra code:", "extra code to be added");
    if( b != null )
    {
      this.extra_code= new BlockSessionInsert();
      this.extra_code.lines= b.lines;
    }
  }
  
  private void parseSessionDataValues() throws IOException, ParserException
  {
    Block b= parseOptionalBlock("session data database:", "Setting session values using transmitted values");
    if( b != null )
    {
      for(String line : b.lines)
      {
        String[] def= line.split("\t");
        FieldDb f= new FieldDb();
        f.idx= Integer.parseInt(def[0]);
        f.number= lookupDbField(def[1]);
        fields_session_db.add(f);
      }
    }
  }
  
  private void parseSessionDataSession() throws IOException, ParserException
  {
    Block b= parseOptionalBlock("session data code:", "Setting session values using code");
    if( b != null )
    {
      for(String line : b.lines)
      {
        String[] def= line.split("\t");
        FieldSession f= new FieldSession();
        f.idx= Integer.parseInt(def[0]);
        f.text= def[1];
        fields_session_code.add(f);
      }
    }
    System.out.println(b);
  }
  
  private int lookupDbField(String name) {
    int i= 0;
    
    for(Field f : fields)
    {
      if( f.name.equals(name) )
        return i;
      i++;
    }
    
    return -1;
  }

	public Field[] getFields() {
		Field[] ret = new Field[this.fields.size()];
		return fields.toArray(ret);
	}
	
	public BlockSessionInsert getSessionInsertBlock() {
	  return this.fields_session;
	}

  public FieldDb[] getSessionFieldsDb()
  {
    FieldDb[] ret = new FieldDb[this.fields_session_db.size()];
    return fields_session_db.toArray(ret);
  }
  
  public FieldSession[] getSessionFieldsCode()
  {
    FieldSession[] ret = new FieldSession[this.fields_session_code.size()];
    return fields_session_code.toArray(ret);
  }

  public Options getOptions()
  {
    return options;
  }

  public BlockSessionInsert getExtraCode()
  {
    return extra_code;
  }
}
