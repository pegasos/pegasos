package at.univie.mma.servergenerator.token;

import java.util.HashMap;
import java.util.Map;

import at.univie.mma.servergenerator.ParserException;
import at.univie.mma.servergenerator.Type;

public class Field {
  /*public enum Option{
    insert_only;
    
    public static Option fromString(String text) throws ParserException {
      if( text.equals("insertonly") )
        return insert_only;
      
      throw new ParserException("Unknown Field-Option: " + text);
    }
  }*/
  
  public class Options {
    private final static String FIELD_SCALE = "scale";
    private final static String FIELD_INSERTONLY = "insertonly";
    private final static String FIELD_DROPDECIMAL = "drop_decimal";
    private final static String FIELD_DROPPRECISION = "drop_precision";
    private final static String FIELD_DBNAME = "dbname";
    
    private Map<String,String> values;
    
    public Options()
    {
      values= new HashMap<String,String>();
    }
    
    public void addOption(String text) throws ParserException
    {
      if( text.equals(FIELD_INSERTONLY) )
      {
        this.values.put(FIELD_INSERTONLY, "");
      }
      else if( text.startsWith(FIELD_SCALE+ ":"))
      {
        this.values.put(FIELD_SCALE, text.split(":")[1]);
      }
      else if( text.equals(FIELD_DROPDECIMAL) )
      {
        this.values.put(FIELD_DROPDECIMAL, "");
      }
      else if( text.equals(FIELD_DROPPRECISION) )
      {
        this.values.put(FIELD_DROPPRECISION, "");
      }
      else if( text.startsWith(FIELD_DBNAME + ":") )
      {
        this.values.put(FIELD_DBNAME, text.split(":")[1]);
      }
      else
      {
        throw new ParserException("Unknown Field-Option: " + text);
      }
    }

    public boolean isInsertOnly()
    {
      if( this.values.containsKey(FIELD_INSERTONLY) )
        return true;
      return false;
    }
    
    public boolean hasScale()
    {
      if( this.values.containsKey(FIELD_SCALE) )
        return true;
      return false;
    }
    
    public int getScale()
    {
      return Integer.parseInt(this.values.get(FIELD_SCALE));
    }
    
    public String getDBname()
    {
      return this.values.get(FIELD_DBNAME);
    }
    
    public boolean hasDropDecimal()
    {
      if( this.values.containsKey(FIELD_DROPDECIMAL) )
        return true;
      return false;
    }

    public boolean hasDropPrecision()
    {
      if( this.values.containsKey(FIELD_DROPPRECISION) )
        return true;
      return false;
    }
    
    public boolean hasDifferentDBname()
    {
      if( this.values.containsKey(FIELD_DBNAME) )
        return true;
      return false;
    }
  }
  
  public int number;
  public String name;
  public Type db_type;
  public Type raw_type;
  public Options options= new Options();
}