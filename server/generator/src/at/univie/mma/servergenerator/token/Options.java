package at.univie.mma.servergenerator.token;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import at.univie.mma.servergenerator.ParserException;

public class Options {
  private final static String FIELD_SESSION_INSERT = "field-is-session-name";
  private final static String USE_SENSOR_NR = "use-sensor-nr";
  private final static String POSITIVE = "true";  
  
  private Map<String,String> values;
  
  public Options(int start, ArrayList<String> lines) throws ParserException
  {
    values= new HashMap<String,String>();
    
    for(String line : lines)
    {
      String[] defs= line.split(": ");
      values.put(defs[0], defs[1]);
    }
    
    checkMandatoryOptions();
  }

  private void checkMandatoryOptions() throws ParserException
  {
    if( !values.containsKey("length") )
      throw new ParserException("Mandatory option 'length' is missing");
  }
  
  public boolean isFixedLength()
  {
    // System.out.println("'" + values.get("length") + "'");
    return values.get("length").equals("fixed");
  }
  
  public boolean isSetSessionFieldsUsingValues()
  {
    if( !values.containsKey(FIELD_SESSION_INSERT) )
      return false;
    
    return values.get(FIELD_SESSION_INSERT).equals(POSITIVE);
  }
  
  public boolean isUseSensorNr()
  {
    if( !values.containsKey(USE_SENSOR_NR) )
      return false;
    
    return values.get(USE_SENSOR_NR).equals(POSITIVE);
  }
}
