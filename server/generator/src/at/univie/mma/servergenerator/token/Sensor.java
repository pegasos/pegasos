package at.univie.mma.servergenerator.token;

import at.univie.mma.servergenerator.ParserException;

public class Sensor {
  public final static int TYPE_JAVACLASS= 1;
  public final static int TYPE_GENERATE= 2;
  // public final static int TYPE_JAVACLASS= 1;
  
  public int identifier;
  public String name;
  public String db_table;
  public int type;
  public String argument;
  
  public Sensor(String[] items) throws ParserException
  {
    if( items.length != 5 )
      throw new ParserException("Invalid number of arguments for Sensor. Got " + items.length + " instead of 5.");
    
    identifier= Integer.parseInt(items[0]);
    name= items[1];
    db_table= items[2];
    
    if( items[3].equals("java") )
      type= TYPE_JAVACLASS;
    else if( items[3].equals("generate") )
      type= TYPE_GENERATE;
    else
      throw new ParserException("Invalid Type: " + items[3]);
    
    argument= items[4];
  }
}
