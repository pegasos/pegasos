package at.univie.mma.servergenerator;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import at.univie.mma.servergenerator.generator.Database;
import at.univie.mma.servergenerator.generator.GeneratorException;
import at.univie.mma.servergenerator.generator.SensorDataParser;
import at.univie.mma.servergenerator.generator.ServerInterface;
import at.univie.mma.servergenerator.generator.ServerParser;
import at.univie.mma.servergenerator.token.Sensor;

public class Generator {
  private ArrayList<Sensor> sensors;
  
  public static void main(String[] args) throws IOException, ParserException, GeneratorException
  {
    Generator g= new Generator();
    
    g.parseSensors();
    
    g.parseAndgenerateSensors();
    
    g.generateSensorParser();
  }
  
  public Generator() {
    sensors= new ArrayList<Sensor>();
  }
  
  public void parseSensors() throws IOException, ParserException
  {
    String line;
    
    FileReader inputStream = new FileReader("config/sensors.txt");
    BufferedReader reader = new BufferedReader(inputStream);
    
    reader.readLine();
    while( (line= reader.readLine()) != null )
    {
      System.out.println(line);
      String[] items= line.split("\t");
      sensors.add(new Sensor(items));
    }
    
    reader.close();
  }
  
  public void parseAndgenerateSensors() throws IOException, ParserException, GeneratorException
  {
    for(Sensor sensor : sensors)
    {
      if( sensor.type == Sensor.TYPE_GENERATE )
      {
        System.out.println("Parsing: " + sensor.name);
        SensorConfigParser parser= new SensorConfigParser("config/" + sensor.argument);
        parser.parse();
        
        ServerParser gen= new ServerParser(parser, sensor.db_table, sensor.name);
        
        // g.generate();
        PrintStream stream= new PrintStream(new BufferedOutputStream(new FileOutputStream("../mma_server/gen/at/univie/mma/server/parser/" + sensor.name + ".java")));
        gen.setOutput(stream);
        gen.generate();
        stream.close();
        
        ServerInterface g2a= new ServerInterface(parser, sensor);
        g2a.setOutput(System.out);
        g2a.generate();
        
        Database gen_db= new Database(parser, sensor);
        gen_db.setOutput(System.out);
        gen_db.generate();
      }
    }
  }
  
  private void generateSensorParser() throws GeneratorException
  {
    SensorDataParser gen= new SensorDataParser(sensors);
    gen.setOutput(System.out);
    gen.generate();
  }

  
  public void parse() throws IOException, ParserException, GeneratorException {
    makedirs();
    File f= new File("config/sensors");
    for(File ff : f.listFiles())
      System.out.println(ff);
  }
  
  private void makedirs() {
    // TODO
    File dir= new File("gen/at/univie/mma/server/parser");
    
    if( dir.exists() == false)
    {
      if( !dir.mkdirs() )
      {
        System.err.println("Creating directory failed");
        System.exit(1);
      }
    }
  }
}
