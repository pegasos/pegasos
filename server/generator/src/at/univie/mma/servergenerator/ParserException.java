package at.univie.mma.servergenerator;

public class ParserException extends Exception {
  private static final long serialVersionUID = -231645349410862278L;
  
  public ParserException(String reason) {
    super(reason);
  }
}
