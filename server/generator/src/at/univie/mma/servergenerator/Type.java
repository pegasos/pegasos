package at.univie.mma.servergenerator;

public class Type {
  public final static int TYPE_INT= 1;
  public final static int TYPE_LONG= 2;
  public final static int TYPE_SHORT= 3;
  public final static int TYPE_DOUBLE= 4;
  
  public int type;
  
  public Type(int type)
  {
    this.type= type;
  }
  
  public static Type fromString(String string) {
    if( string.equals("int") )
      return new Type(TYPE_INT);
    else if( string.equals("long") )
      return new Type(TYPE_LONG);
    else if( string.equals("short") )
      return new Type(TYPE_SHORT);
    else if( string.equals("double") )
      return new Type(TYPE_DOUBLE);;
    return null;
  }

  public boolean isInteger() {
    return type == TYPE_INT;
  }
  
  public boolean isShort() {
    return type == TYPE_SHORT;
  }
  
  public boolean isLong() {
    return type == TYPE_LONG;
  }
  
  public boolean isDouble() {
    return type == TYPE_DOUBLE;
  }

  public String getJavaDef()
  {
    switch( type )
    {
      case TYPE_INT:
        return "int";
      case TYPE_LONG:
        return "long";
      case TYPE_SHORT:
        return "short";
      case TYPE_DOUBLE:
        return "double";
      default:
        return "";  
    }
  }
  
  public String toString()
  {
    return getJavaDef();
  }

  public String getSqlDef() {
    switch( type )
    {
      case TYPE_INT:
        return "int(11)";
      case TYPE_LONG:
        return "bigint(13)";
      case TYPE_SHORT:
        return "smallint(6)";
      case TYPE_DOUBLE:
        return "double";
      default:
        return "";  
    }
  }
}
