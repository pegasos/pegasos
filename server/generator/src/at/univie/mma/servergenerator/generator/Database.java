package at.univie.mma.servergenerator.generator;

import java.io.PrintStream;

import at.univie.mma.servergenerator.SensorConfigParser;
import at.univie.mma.servergenerator.token.Field;
import at.univie.mma.servergenerator.token.Sensor;

public class Database {
  private static final String INDENT = "  ";
  private PrintStream out;
  
  private SensorConfigParser config;
  private Sensor sensor;
  
  public Database(SensorConfigParser config, Sensor sensor)
  {
    this.config= config;
    this.sensor= sensor;
  }
  
  public void setOutput(PrintStream output)
  {
    this.out = output;
  }
  
  public void generate()
  {
    String id_name= sensor.name.toLowerCase() + "_id";
    
    out.println("CREATE TABLE IF NOT EXISTS `" + sensor.db_table  + "` (");
    
    out.println(INDENT + "`" + id_name + "` bigint(20) unsigned NOT NULL AUTO_INCREMENT,");
    out.println(INDENT + "`session_id` int(11) unsigned NOT NULL,");
    out.println(INDENT + "`rec_time` bigint(13) NOT NULL,");
    
    for(Field f : config.getFields())
    {
      out.println(INDENT + "`" + f.name + "` " +f.db_type.getSqlDef() + " NOT NULL,");
    }
    
    out.println(INDENT + "UNIQUE KEY `" + id_name + "` (`" + id_name + "`),");
    out.println(INDENT + "KEY `session_id` (`session_id`)");
    out.println(") ENGINE=InnoDB ;");
    
    /*
CREATE TABLE IF NOT EXISTS `gps` (
  `gps_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  `alt_cm` smallint(6) NOT NULL,
  `speed_cm_s` smallint(6) NOT NULL,
  `acc` smallint(6) NOT NULL,
  `bearing` smallint(6) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `distance_m` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `gps_id` (`gps_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB
     */
  }
}
