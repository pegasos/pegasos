package at.univie.mma.servergenerator.generator;

public class GeneratorException extends Exception {
	private static final long serialVersionUID = 1544739654348947820L;

	public GeneratorException(String reason) {
		super(reason);
	}
}
