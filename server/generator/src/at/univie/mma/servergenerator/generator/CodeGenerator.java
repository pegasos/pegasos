package at.univie.mma.servergenerator.generator;

import java.io.PrintStream;
import java.util.ArrayList;

public abstract class CodeGenerator {
  private static final String INDENT = "  ";
  private int indent_size;
  private PrintStream out;
  
  private final String package_name;
  private ArrayList<String> imports;
  
  public CodeGenerator(String pkg_name)
  {
    this.imports= new ArrayList<String>();
    this.package_name= pkg_name;
  }
  
  protected void addImport(String imp)
  {
    this.imports.add(imp);
  }
  
  public void setOutput(PrintStream output)
  {
    this.out = output;
  }
  
  protected void output(int indent, String line)
  {
    for(int i = 0; i < indent; i++)
      out.print(INDENT);
    out.println(line);
  }
  
  protected void empty(int indent)
  {
    output(indent, "");
  }
  
  protected void output(String line)
  {
    output(indent_size, line);
  }
  
  protected void empty()
  {
    empty(indent_size);
  }
  
  protected void indent()
  {
    indent_size++;
  }
  
  protected void indent_less()
  {
    indent_size--;
  }
  
  protected void header()
  {
    output("package " + package_name + ";");
    empty();
    empty();
    
    for(String imp : imports)
      output("import " + imp + ";");
    empty();
  }

  abstract public void generate() throws GeneratorException;
}
