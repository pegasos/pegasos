package at.univie.mma.servergenerator.generator;

import java.util.ArrayList;
import java.util.List;

import at.univie.mma.servergenerator.SensorConfigParser;
import at.univie.mma.servergenerator.token.BlockSessionInsert;
import at.univie.mma.servergenerator.token.Field;
import at.univie.mma.servergenerator.token.FieldDb;
import at.univie.mma.servergenerator.token.FieldSession;
import at.univie.mma.servergenerator.token.Options;

public class ServerParser extends CodeGenerator {
  private Options options;
  private Field[] fields;
  private BlockSessionInsert session_insert;
  private FieldSession[] fields_session_code;
  private FieldDb[] fields_session_db;
  private BlockSessionInsert extra_code;
  
  private String db_table;
  private String name;

  
	public ServerParser(SensorConfigParser config, String db_table, String name) {
	  super("at.univie.mma.server.parser");
	  
	  this.options= config.getOptions();
	  this.fields= config.getFields();
	  this.fields_session_db= config.getSessionFieldsDb();
	  this.fields_session_code= config.getSessionFieldsCode();
	  this.session_insert= config.getSessionInsertBlock();
	  this.extra_code= config.getExtraCode();
    this.db_table= db_table;
		this.name= name;
	}
	
	private void preparedStatement()
	{
	  output("private void perpareStatement() throws SQLException");
	  output("{");
	  indent();
	  
	  String sql= "insert into " + db_table + "(";
	  String values= "";
	  sql+= "session_id"; //always include session id;
	  values+= "?";
	  sql+= ",rec_time";  //always include the time of recording
	  values+= ",?";
    for(Field f : fields)
	  {
      sql+= ",";
      if( f.options.hasDifferentDBname() )
      {
        sql+= f.options.getDBname();
      }
      else
      {
        sql+= f.name;
      }
	    values+= ",?";
	  }
    
    if( this.options.isUseSensorNr() )
      sql+= ", sensor_nr";
	  
    sql+= ") values (" + values;
	  
    if( this.options.isUseSensorNr() )
      sql+= ",?";
	  
    sql+= ")";
	  
    output("this.db_insert= this.db.createStatement(\"" + sql + "\");");
	  indent_less();
	  output("}");
	}
	
	private void constructor()
	{
	  output("public " + name + "(Session data, Db_connector db) throws SQLException");
    output("{");
    indent();
    output("this.data= data;");
    output("this.db= db;");
    output("perpareStatement();");
    indent_less();
    output("}");
	}
	
	public void generate() throws GeneratorException
	{
	  addImport("java.io.IOException");
	  addImport("java.sql.PreparedStatement");
	  addImport("java.sql.SQLException");
    addImport("database.Db_connector");
    addImport("at.univie.mma.server.data.Session");
	  
	  header();
	  
    output("public class " + name + " implements ISensorDataParser");
    output("{");
    empty();
    indent();
    output("private int time_last_measurement;");
    output("private long time_since_start_ret;");
    output("private Session data;");
    output("private Db_connector db;");
    output("private int bc_ret;");
    output("private PreparedStatement db_insert;");
    output("private int inserts;");
    output("");
    
    constructor();
    empty();

    this.preparedStatement();
    empty();
    
    output("@Override");
    output("public String parse(byte[] in, int bc, String line, int sensortype, int sensornr,");
    output("  int datapackages, long time_since_start) throws IOException, SQLException");
    output("{");
    // System.out.println(" String sql = \"insert into foot_pod
    // (session_id,rec_time,speed_mm_s,distance_m,strides_255,calories)
    // values\";");
    
    indent();
    for (Field f : fields)
    {
      String line= "";
      
      line+= f.db_type.getJavaDef() + " ";
      
      line+= f.name;
      
      // Variable initialisation TODO: add option for other values
      if( f.db_type.isInteger() || f.db_type.isLong() || f.db_type.isShort() || f.db_type.isDouble() )
        line+= "= 0";
      
      line+= "; // field: " + f.number;
      output(line);
    }
    
    empty();
    output("inserts= 0;");
    empty();
    
    output("for (int x=0;x<datapackages;x++)");
    output("{");
    indent();
    output("time_last_measurement=((int)in[bc+1]&0xFF)|((int)in[bc+2]&0xFF)<<8; // 0xff damit positiv!!");
    output("time_since_start+=time_last_measurement;");
    empty();
    output("db_insert.setLong(1, data.session_id);");
    output("db_insert.setLong(2, data.starttime+time_since_start);");
    
    int bc= 3;
    int increase= 2;
    
    List<String> insert_only= new ArrayList<String>();
    List<String> stmt_setters= new ArrayList<String>();
    int no= 3;
    for(Field f : fields)
    {
      String set= "db_insert.set";
      if( f.db_type.isDouble())
        set+= "Double";
      else if( f.db_type.isInteger() )
        set += "Int";
      else if( f.db_type.isLong() )
        set += "Long";
      else if( f.db_type.isShort() )
        set += "Short";
      else
        throw new GeneratorException("Cannot generate Setter-Statement for type: " + f.db_type);
      set+= "(" + no + "," + f.name + ");";
      
      String line= "";
      
      if( f.db_type.isInteger() &&
        (f.raw_type.isInteger() || f.raw_type.isDouble() || f.raw_type.isLong() ) )
      {
        line+= f.name + "= ((int)in[bc+" + (bc++) + "]&0xFF)|((int)in[bc+" + (bc++) + "]&0xFF)<<8;";
        increase+= 2;
      }
      else if( f.db_type.isShort() && (f.raw_type.isShort() ) )
      {
        line+= f.name + "= (short)(in[bc+" + (bc++) + "]&0xFF|in[bc+" + (bc++) + "]<<8);";
        increase+= 2;
      }
      else if( f.db_type.isDouble() && (f.raw_type.isDouble() ) )
      {
        // for (int i = 0; i < 8; i++) {data.gps_lat=(data.gps_lat<<8)+(in[bc+3+i] & 0xff) ;}
        line+= "{long tmp= 0;";
        line+= "for(int i= 0; i < 8; i++) {tmp= (tmp<<8) + (in[bc+" + bc + "+i] & 0xff);}";
        bc+= 8;
        // line+= f.name + "= (short)(in[bc+" + (bc++) + "]&0xFF|in[bc+" + (bc++) + "]<<8);";
        increase+= 8;
        if( f.options.hasScale() )
        {
          line+= f.name + "= ((double)tmp / " + f.options.getScale() + ");";
        }
        else
        {
          line+= f.name + "= (double) tmp;";
        }
        line+= "}";
      }
      else
        throw new GeneratorException("Cannot generate Java Code for combination " + f.db_type + "/" + f.raw_type);
      
      if( f.options != null && f.options.isInsertOnly() )
      {
        insert_only.add(line);
        stmt_setters.add(set);
      }
      else
      {
        output(line);
        output(set);
      }
      
      no++;
    }
    
    if( options.isUseSensorNr() )
      output("db_insert.setInt(" + no + ",sensornr);");
    
    if( insert_only.size() > 0 )
    {
      output("if (data.insert_data==1)");
      output("{");
      indent();
      for(String ins : insert_only )
        output(ins);
      indent_less();
      output("}");
    }
    
    if( stmt_setters.size() > 0 )
    {
      for(String set : stmt_setters )
        output(set);
    }
    
    output("if (data.insert_data==1)");
    output("{");
    indent();
    
    if( this.session_insert != null )
    {
      for(String ins : session_insert.lines )
        output(ins);
    }

    output("db_insert.addBatch();");
    output("inserts++;");
    indent_less();
    output("}");
    
    if( this.extra_code != null )
    {
      empty();
      for(String ins : extra_code.lines )
        output(ins);
      empty();
    }
    
    // System.out.println("      sql+=\"(\"+data.session_id+\",\"+(data.starttime+time_since_start)+\",\"+fp_speed+\",\"+fp_distance+\",\"+data.fp_strides_cul+\",\"+data.fp_calories+\")\";");
    // System.out.println("      if (x==(datapackages-1)) {sql+=\";\";} else {sql+=\",\";}");
    debugOutput();
    
    output("bc= bc + " + increase + ";");
    indent_less();
    output("}");
    
    empty();
    output("if( inserts>0 )");
    output("{");
    indent();
    output("db_insert.executeBatch();");
    indent_less();
    output("}");
    empty();
    
    setSessionValues();
    
    output("bc_ret= bc;");
    output("time_since_start_ret= time_since_start;");
    empty();
    
    output("return null;");
    indent_less();
    output("}");
    
    helperMethods();
    
    indent_less();
    output("}");
  }

  private void debugOutput()
  {
    String line= "System.out.println(\"" + name + ": ";
    for(Field f: fields)
    {
      line+= f.name + ":\"+" + f.name + "+ \" ";
    }
    line+= "\");";
    output(line);
  }

  private void helperMethods()
  {
    empty();
    output("@Override");
    output("public int getNewPosition()");
    output("{");
    indent();
    output("return bc_ret;");
    indent_less();
    output("}");
    
    empty();
    output("@Override");
    output("public long getNewTimeSinceStart()");
    output("{");
    indent();
    output("return time_since_start_ret;");
    indent_less();
    output("}");
  }
  
  private void setSessionValues()
  {
    boolean written= false;
    
    for(FieldDb f : this.fields_session_db)
    {
      String line= "data.v[" + f.idx + "]= ";
      
      line+= this.fields[f.number].name;
      
      line+= ";";
      
      output(line);
      
      written= true;
    }
    
    for(FieldSession f : this.fields_session_code)
    {
      String line= "data.v[" + f.idx + "]= " + f.text + ";";
      output(line);
      
      written= true;
    }
    
    if( options.isSetSessionFieldsUsingValues() )
    {
      for(Field f : fields)
      {
        output("data." + f.name + "= " + f.name + ";");
        written= true;
      }
    }
    
    if( written )
      empty();
  }
}
