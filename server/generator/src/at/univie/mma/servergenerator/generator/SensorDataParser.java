package at.univie.mma.servergenerator.generator;

import java.util.List;

import at.univie.mma.servergenerator.token.Sensor;

public class SensorDataParser extends CodeGenerator {
  private List<Sensor> sensors;
  private String[] classnames;
  
  private final static String ParsersPackage= "at.univie.mma.server.parser";
  
  public SensorDataParser(List<Sensor> sensors)
  {
    super("at.univie.mma.server.parser");
    
    this.sensors= sensors;
    
    analyseSensors();
  }
  
  private void analyseSensors()
  {
    int max= 0;
    for(Sensor sen : sensors)
    {
      if( max < sen.identifier )
        max= sen.identifier;
    }
    
    classnames= new String[max + 1];
    for(int i= 0; i < max+1; i++)
    {
      classnames[i]= "at.univie.mma.server.parser.NoParserAvailable";
    }
    
    for(Sensor sen : sensors)
    {
      if( sen.type == Sensor.TYPE_JAVACLASS )
        classnames[sen.identifier]= sen.argument;
      else
        classnames[sen.identifier]= ParsersPackage + "." + sen.name;
    }
  }

  @Override
  public void generate() throws GeneratorException
  {
    addImport("java.io.BufferedWriter");
    addImport("java.io.IOException");
    addImport("java.sql.ResultSet");
    addImport("java.sql.SQLException");
    addImport("at.univie.mma.server.data.Session");
    addImport("at.univie.mma.server.ServerStartupException");
    addImport("at.univie.mma.server.ServerRuntimeException");
    addImport("database.Db_connector");
    
    header();
    
    output("public class SensorDataParser implements Parser{");
    indent();
    output("private Session data;");
    output("String ret= null;");
    output("private BufferedWriter f_out;");
    output("private Db_connector db;");
    output("private ISensorDataParser[] parsers;");
    empty();
    output("public SensorDataParser(Session data, BufferedWriter out, Db_connector db) throws ServerRuntimeException");
    output("{");
    indent();
    output("this.data= data;");
    output("this.f_out= out;");
    output("this.db= db;");
    output("createTable();");
    indent_less();
    output("}");
    empty();
    output("protected void log(String tx)");
    output("{");
    indent();
    output("try");
    output("{");
    indent();
    output("f_out.write(System.currentTimeMillis() + \";\" + tx + \"\\n\");");
    output("f_out.flush();");
    indent_less();
    output("}");
    output("catch (IOException e)");
    output("{");
    output("}");
    indent_less();
    output("}");
    
    createTable();
    
    testTable();
    
    parserFunc();
  }
  
  private String[] exceptions= {"IllegalArgumentException", "SecurityException", "InstantiationException", 
      "IllegalAccessException", "InvocationTargetException", "NoSuchMethodException", "ClassNotFoundException"};
  
  private void exceptionTable(String throwing)
  {
    for(String exception : exceptions)
    {
      output("catch (" + exception + " e)");
      output("{");
      indent();
      output("e.printStackTrace();");
      output("throw new " + throwing + ";");
      indent_less();
      output("}");
    }
  }

  private void createTable()
  {
    empty();
    output("private void createTable() throws ServerRuntimeException");
    output("{");
    indent();
    
    output("parsers= new ISensorDataParser[" + classnames.length + "];");
    output("try");
    output("{");
    indent();
    for(int i= 0; i < classnames.length; i++)
    {
      output("parsers[" + i + "]= (ISensorDataParser) Class.forName(\"" + classnames[i] + "\").getConstructor(Session.class, Db_connector.class).newInstance(data,db);");
    }
    indent_less();
    output("}");
    
    exceptionTable("ServerRuntimeException(\"Creating parsers for Session \" + data.session_id)");
    
    indent_less();
    output("}");
  }
  
  private void testTable()
  {
    empty();
    output("public static void testTable() throws ServerStartupException");
    output("{");
    indent();
    
    output("ISensorDataParser[] parsers= new ISensorDataParser[" + classnames.length + "];");
    output("try");
    output("{");
    indent();
    output("Db_connector db = new Db_connector();");
    output("Db_connector.open_db();");
    for(int i= 0; i < classnames.length; i++)
    {
      output("parsers[" + i + "]= (ISensorDataParser) Class.forName(\"" + classnames[i] + "\").getConstructor(Session.class, Db_connector.class).newInstance(null, db);");
    }
    indent_less();
    output("}");
    
    exceptionTable("ServerStartupException()");
    
    indent_less();
    output("}");
  }
  
  private void parserFunc()
  {
    empty();
    output("@Override");
    output("public String parse(byte[] in, String line, int sensortype, int sensornr,");
    output("  int datapackages, long time_since_start) throws IOException");
    output("{");
    indent();
    
    output("String ret;");
    output("int bc;");
    // output("String sql = null;");
    output("// System.out.println(\" AT \"+time_since_start+\" RECEIVED:\"+datapackages+\" SENSORVALUES FROM SENSORNR:\"+sensornr+\" TYPE:\"+sensortype);");
    output("bc=6;");
    empty();
    
    output("try");
    output("{");
    indent();
    output("ret= parsers[sensortype].parse(in, bc, line, sensortype, sensornr, datapackages, time_since_start);");
    output("bc+= parsers[sensortype].getNewPosition();");
    output("time_since_start+= parsers[sensortype].getNewTimeSinceStart();");
    indent_less();
    output("}");
    output("catch (SQLException e)");
    output("{");
    indent();
    output("e.printStackTrace();");
    output("System.out.println(\"SQL WARNINGS!!: \"+ db.getWarnings());");
    output("ret= \"3;1;Database error\";");
    indent_less();
    output("}");
    
    empty();
    output("try");
    output("{");
    indent();
    output("ResultSet res;");
    output("if( data.insert_data != 1 )");
    output("{");
    indent();
    output("res = db.sql(\"select v10 from session where session_id=\" + data.session_id, 0);");
    output("if( res.first() )");
    output("{");
    indent();
    output("data.insert_data = res.getInt(1);");
    output("System.out.println(\"select v10 from session where session_id=\" + data.session_id + \" INSERTDATA:\" + data.insert_data);");
    indent_less();
    output("}");
    indent_less();
    output("}");
    indent_less();
    output("}");
    output("catch (SQLException e)");
    output("{");
    indent();
    output("System.out.println(\"SQL Exception!!:\"+e.getMessage());");
    output("ret= \"3;1;Database error\";");
    indent_less();
    output("}");
    
    empty();
    output("return ret;");
    
    indent_less();
    output("}");
  }
}
