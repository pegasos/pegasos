package at.univie.mma.servergenerator.generator;

import java.util.ArrayList;
import java.util.Iterator;

import at.univie.mma.servergenerator.SensorConfigParser;
import at.univie.mma.servergenerator.token.Field;
import at.univie.mma.servergenerator.token.Sensor;

public class ServerInterface extends CodeGenerator {
  private Field[] fields;
  
  private ArrayList<String> variables;
  
  private ArrayList<ArrayList<String>> setters;
  
  private Sensor sensor;
  
  public ServerInterface(SensorConfigParser config, Sensor sensor)
  {
    super("at.univie.mma.serverinterface.sensors");
    
    this.variables= new ArrayList<String>();
    this.setters= new ArrayList<ArrayList<String>>();
    
    this.fields = config.getFields();
    this.sensor= sensor;
  }
  
  private void constructor()
  {
    output("public " + sensor.name + "(datasets_per_packet, sampling_rate)");
    output("{");
    indent();
    output("super(nr, " + sensor.identifier + ", datasets_per_packet, sampling_rate);");
    indent_less();
    output("}");
  }
  
  @Override
  public void generate() throws GeneratorException
  {
    addImport("at.univie.mma.serverinterface.ServerSensor");
    
    analyseVariables();
    
    
    header();
    
    output("public class " + sensor.name + " extends ServerSensor");
    output("{");
    indent();
    for(String var : variables)
      output(var);
    
    empty();
    
    constructor();
    empty();
    
    setDataFunction();
    
    /*
    long lat = (long) (location.getLatitude() * 100000000);
      long lon = (long) (location.getLongitude() * 100000000);
     */
    
    
    indent_less();
    output("}");
  }

  private void setDataFunction() throws GeneratorException
  {
    boolean notfirst= false;
    String line;
    
    generateSetterSatements();
    
    //Function header
    line= "public void setData(";
    for(Field f : fields)
    {
      if( notfirst )
        line+= ", ";
      notfirst= true;
      line+= f.raw_type.getJavaDef() + " " + f.name;
    }
    line+= ")";
    output(line);
    
    output("{");
    indent();
    
    for(ArrayList<String> stmts : setters)
    {
      for(String stmt : stmts)
        output(stmt);
    }
    
    indent_less();
    output("}");
    
    Iterator<ArrayList<String>> it = setters.iterator();
    for(Field f : fields)
    {
      line= "public void set" + f.name.substring(0, 1).toUpperCase() + f.name.substring(1) + "(";
      line+= f.raw_type.getJavaDef() + " " + f.name;
      line+= ")";
      output(line);
      
      output("{");
      indent();
      
      ArrayList<String> stmts= it.next();
      for(String stmt : stmts)
        output(stmt);
      
      indent_less();
      output("}");
    }
  }

  private void analyseVariables()
  {
    int variable= 1;
    for(Field f : fields)
    {
      if( f.db_type.isDouble() && f.raw_type.isDouble() )
      {
        variables.add("ByteBuffer buffer" + variable + "= ByteBuffer.allocate(8);");
        variable++;
      }
    }
  }
  
  private void generateSetterSatements() throws GeneratorException
  {
    String line;
    int reg_idx= 0;
    int var_idx= 1;
    
    for(Field f : fields)
    {
      ArrayList<String> lines= new ArrayList<String>();
      
      if( f.db_type.isShort() && f.raw_type.isShort() || f.db_type.isInteger() && f.raw_type.isInteger() )
      {
        // buffer[16]= (byte) (alt&0xFF);         
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " & 0xFF);";
        lines.add(line);
        
        // buffer[17]= (byte) ((alt>>8) & 0xff);
        line= "data_register[" + (reg_idx++) + "] = (byte) ((" + f.name + " >> 8) & 0xFF);";
        lines.add(line);
      }
      else if( f.db_type.isDouble() && f.raw_type.isDouble() )
      {
        // long lat = (long) (location.getLatitude() * 100000000);
        if( f.options.hasScale() )
        {
          // long lat = (long) (location.getLatitude() * 100000000);
          line= "long tmp" + var_idx + "= (long) (" + f.name + " * " + f.options.getScale() + ");";
          lines.add(line);
          // byte[] bytes1 = buffer1.putLong(0, lat).array();
          line= "byte[] bytes" + var_idx + "= buffer" + var_idx + ".putLong(0, tmp" + var_idx + ").array();";
          lines.add(line);
        }
        else
        {
          // byte[] bytes1 = buffer1.putLong(0, lat).array();
          line= "byte[] bytes" + var_idx + "= buffer" + var_idx + ".putLong(0, " + f.name + ").array();";
          lines.add(line);
        }
        
        // System.arraycopy(bytes1, 0, data_register, 0, 8);
        line= "System.arraycopy(bytes" + var_idx + ", 0, data_register, " + reg_idx + ", 8);";
        lines.add(line);
        
        reg_idx+= 8;
        var_idx++;
      }
      else if( f.db_type.isInteger() && f.raw_type.isDouble() )
      {
        if( !f.options.hasDropDecimal() )
          throw new GeneratorException("When converting double to int it is required to confirm this by adding option 'drop_decimal'");
        
        // data_register[0]=(byte) (speed);     // SPEED_MM LOW
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + "); // LOW";
        lines.add(line);
        
        // data_register[1]=(byte) (speed/256); // SPEED_MM HIGH
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " / 256); // HIGH";
        lines.add(line);
      }
      else if( f.db_type.isInteger() && f.raw_type.isLong() )
      {
        if( !f.options.hasDropPrecision() )
          throw new GeneratorException("When converting long to int it is required to confirm this by adding option 'drop_precision'");
        
        // data_register[0]=(byte) (speed);     // LOW 0....255
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + "); // LOW 0....255";
        lines.add(line);
        
        // data_register[1]=(byte) (speed/256); // HIGH 0....255
        line= "data_register[" + (reg_idx++) + "] = (byte) (" + f.name + " / 256); // HIGH 0....255";
        lines.add(line);
      }
      else
        throw new GeneratorException("Cannot generate sender for " + f.db_type + "/" + f.raw_type);
      
      setters.add(lines);
    }
  }
}
