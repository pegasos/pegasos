package activities;

import at.univie.mma.ai.Parameters;

/**
 * This class is a work around since many parts of the framework need access to AI_module for logging purposes
 * This module does nothing except opening the logfile and logging AI MODULE STARTED started!
 */
public class NoPost extends AI_module{
	public NoPost(Parameters p) {
		super(p);
	}
}
