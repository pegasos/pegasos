package activities;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;

import org.slf4j.LoggerFactory;

import at.univie.mma.ai.Parameters;
import at.univie.mma.ai.postprocessing.PostProcessor;
import at.univie.pegasos.ai.Command;
import ch.qos.logback.classic.Logger;

public abstract class AI_module {

  protected Db_connector db;
  protected Timer timer;
  private ResultSet res= null;
  private Parameters p;
  
  protected PostProcessor postprocessor;
  
  protected String sql;
  
  private PreparedStatement stmt_stopped;
  private PreparedStatement stmt_alive3s;
  private PreparedStatement stmt_lastupd;
  private PreparedStatement stmt_maxs;

  /**
   * Root logger for this module
   */
  private final Logger logger;

  public AI_module(Parameters p)
  {
    this.p= p;
    db= new Db_connector();
    db.open_db();
    
    logger= ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME));
    
    try
    {
      stmt_stopped= db.createStatement("select endtime from session where session_id = ?");
      stmt_stopped.setLong(1, p.session);
      stmt_alive3s= db.createStatement("select last_update,userid from session where session_id = ?");
      stmt_alive3s.setLong(1, p.session);
      stmt_lastupd= db.createStatement("select last_update,userid from session where session_id = ?");
      stmt_lastupd.setLong(1, p.session);
      stmt_maxs= db.createStatement("select max(session_id) from session where userid = ?");
    }
    catch( SQLException e )
    {
      e.printStackTrace();
      log("SQLException setting up statements for AI_module " + e.getMessage());
    }
    
    log(p.toString());
    log("AI MODULE STARTED started!");
  }
  
  public void log(String tx)
  {
    logger.info(tx);
  }
  
  protected void start_insert()
  {
    try
    {
      PreparedStatement stmt= db.createStatement("update session set v10=1,starttime=? where session_id = ?");
      stmt.setLong(1, System.currentTimeMillis());
      stmt.setLong(2, this.p.session);
      stmt.executeUpdate();
    }
    catch( SQLException e )
    {
      e.printStackTrace();
      logStackTrace(e);
    }
  }
  
  protected void send_feedback(String text, int stop)
  {
    log("Feedback:" + text);
    sql= "insert into feedback(session_id,message,stop_session,itime) values (" + p.session + ",'" + text + "'," + stop + ","
        + System.currentTimeMillis() + ")";
    // log(sql);
    res= db.sql(sql, 1);
  }
  
  protected void send_feedback_stop(String feedback)
  {
    log("Feedback:" + feedback + " | stop");
    sql= "insert into feedback(session_id,message,stop_session,itime) values (" + p.session + ",'" + feedback + "',1,"
        + System.currentTimeMillis() + ")";
    // log(sql);
    res= db.sql(sql, 1);
  }
  
  protected void setupPostprocessor()
  {
    postprocessor= new PostProcessor(this, p);
  }
  
  /**
   * Run the postprocessor and close the module
   */
  protected void close_ai()
  {
    if( postprocessor == null )
      setupPostprocessor();
    postprocessor.run();
    close_ai_no_post();
  }
  
  /**
   * Close the module but do not run the post processor
   */
  protected void close_ai_no_post()
  {
    db.close_db();
  }
  
  /**
   * Check if the session is still alive. A session is alive iff<BR/>
   * * the user has sent data in the last 10.000 seconds (~3 hours) and <BR/>
   * * has not started a new session
   * 
   * <BR/>
   * Wenn user neue Session gestartet hat oder seit ~3h keine daten --> kill AI-MODULE
   * 
   * @return true if session is still alive
   */
  protected boolean alive()
  {
    boolean a= false;
    int user= 0;
    
    try
    {
      res= stmt_lastupd.executeQuery();
      if( res.next() )
      {
        long t= res.getLong(1);
        user= res.getInt(2);
        if( (System.currentTimeMillis() - t) < 10000000 )
        {
          a= true;
        } // 10 000 sekunden
      }
    }
    catch( SQLException e )
    {
      logger.error("SQL Exception: " + e.getMessage());
      logStackTrace(e);
    }

    try
    {
      stmt_maxs.setInt(1, user);
      res= stmt_maxs.executeQuery();
      if( res.next() )
      {
        double t= res.getDouble(1);
        if( t > p.session )
        {
          a= false;
        }
      }
    }
    catch( SQLException e )
    {
      logger.error("SQL Exception: " + e.getMessage());
      logStackTrace(e);
    }

    if( a == false )
    {
      log("AI-Module for Session " + p.session + " stopped");
    }
    return a;
  }
  
  protected boolean alive_3sec()
  { // Wenn 3 sekunden keine daten
    boolean a= false;
    // int user=0;
    
    try
    {
      res= stmt_alive3s.executeQuery();
      if( res.next() )
      {
        double t= res.getDouble(1);
        // user=res.getInt(2);
        if( (System.currentTimeMillis() - t) < 13500 )
        {
          a= true;
        } //
      }
    }
    catch( SQLException e )
    {
    }
    
    if( a == false )
    {
      log("AI-Module for Session " + p.session + " stopped");
    }
    return a;
  }
  
  protected boolean activity_stopped()
  {
    boolean a= false;
    
    try
    {
      res= stmt_stopped.executeQuery();
      if( res.next() )
      {
        double t= res.getDouble(1);
        if( t > 0 )
        {
          a= true;
        } //
      }
    }
    catch( SQLException e )
    {
      logger.error("SQL Exception: " + e.getMessage());
      logStackTrace(e);
    }

    return a;
  }
  
  protected long send_command(long session, int command_nr, int status, Object... data)
  {
    String x= "";
    for(Object o : data)
      x+= ";" + o;
    try
    {
      return db.insert_command(session, ";c;" + command_nr + ";" + status + x);
    }
    catch( SQLException e )
    {
      e.printStackTrace();
      log("Error sending command " + x);
      log(e.getMessage());
      logStackTrace(e);
      return -1;
    }
  }
  
  protected long send_command(long session, Command command)
  {
    String x= "";
    for(Object o : command.data)
      x+= ";" + o;
    try
    {
      return db.insert_command(session, ";c;" + command.code + ";" + command.status + x);
    }
    catch( SQLException e )
    {
      e.printStackTrace();
      log("Error sending command " + x);
      log(e.getMessage());
      logStackTrace(e);
      return -1;
    }
  }
  
  public void logStackTrace(Exception e)
  {
    StringBuilder builder= new StringBuilder();
    StackTraceElement[] trace= e.getStackTrace();
    for(StackTraceElement traceElement : trace)
      builder.append("\tat " + traceElement + "\n");
    logger.error(builder.toString());
  }
}
