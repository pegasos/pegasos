package activities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import at.univie.mma.ai.Config;

public class Db_connector {
	
  private static Connection cn= null;
  private PreparedStatement msg;	
	
   public int open_db() {
      try {
           Class.forName( Config.DB_DRIVER );
           cn = DriverManager.getConnection( Config.DB_URL, Config.DB_USER, Config.DB_PASSWORD );
           
           msg= cn.prepareStatement("insert into feedback(session_id, itime, message) values (?, ?, ?)");
      } catch( Exception ex ) {
        System.out.println( ex );
        return -1;
      } 
      return 1;
    }

  
  
  public ResultSet sql(String sql,int type) {
	  
	  Statement  st = null;
      ResultSet  rs = null;
      
      try {
    	   st = cn.createStatement();
    	   if (type==0) 
    	      {rs = st.executeQuery(sql);}
    	   else
              {
    		   st.executeUpdate(sql);
    		   //rs=null;
              }
     } catch( Exception ex ) { 
       System.out.println("SQL:"+sql); 
       System.out.println("SQL-FEHLER:"+ex);
       return null;
     } finally {
    	 //try { if( null != rs ) rs.close(); } catch( Exception ex ) {return null;}
    	 //try { if( null != st ) st.close(); } catch( Exception ex ) {return null;}
     }
	  return rs;
  }

   public void close_db() {
	   
	   if( null != cn )
		try {cn.close();} catch (SQLException e) {}
	   
   }
  
  public PreparedStatement createStatement(String sql) throws SQLException
  {
    return cn.prepareStatement(sql);
  }
  
  public PreparedStatement createStatementRO(String sql) throws SQLException
  {
    return cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
  }
  
  /**
   * Insert a command into the feedback table
   * 
   * @param session
   *          session to which the command is sent
   * @param command
   *          command
   * @return insert time
   * @throws SQLException
   */
  public long insert_command(long session, String command) throws SQLException
  {
    long itime= System.currentTimeMillis();
    msg.setLong(1, session);
    msg.setLong(2, itime);
    msg.setString(3, command);
    
    msg.execute();
    
    return itime;
  }
}





