package activities;

import java.util.Timer;
import java.util.TimerTask;

import at.univie.mma.ai.Parameters;

// import java.sql.ResultSet;

/**
 * This activity will only do the following two things<br/>
 * * send a 'session started' message
 * * check whether there are PostProcessingModules for this activity. 
 * If yes it will wait until the activity has finished and run them. Otherwise, it will
 * terminate immediately.  
 */
public class Default extends AI_module{
	private Timer timer;
	
	public Default(Parameters p) {
		super(p);
		log("Activity "+ p.param0+"" + p.param1 + "" + p.param2 +" has no AI-Module!");
		// res=db.sql("update session set v10=1 where session_id="+session,1);
		db.sql("update session set v10=1 where session_id="+ p.session,1); //TODO: check result
    send_feedback("Session started",0);
		
    setupPostprocessor();
    if( postprocessor.hasModules() )
      waitTillEndTimer();
    else
      close_ai();
	}
	
	protected Default(Parameters p, boolean no_init)
	{
		super(p);
		if( !no_init )
		{
			log("Activity "+ p.param0+"" + p.param1 + "" + p.param2 +" has no AI-Module!");
			// res=db.sql("update session set v10=1 where session_id="+session,1);
			db.sql("update session set v10=1 where session_id="+ p.session,1); //TODO: check result
	    send_feedback("Session started",0);
			
	    setupPostprocessor();
	    if( postprocessor.hasModules() )
	      waitTillEndTimer();
	    else
	      close_ai();
		}
	}

  protected void waitTillEndTimer()
  {
    timer= new Timer();
    timer.schedule(new TimerTask() {
      public void run()
      {
        if( activity_stopped() || !alive() )
        {
          log("Killed AI-Session!");
          stop_timer();
          close_ai();
        }
      }
    }, 60000, 60000);
  }
  
  private void stop_timer()
  {
    timer.cancel();
    timer= null;
  }
	
}
