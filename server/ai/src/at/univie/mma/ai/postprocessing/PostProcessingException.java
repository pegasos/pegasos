package at.univie.mma.ai.postprocessing;

public class PostProcessingException extends Exception {
  private static final long serialVersionUID = -980463191147326754L;
  
  public PostProcessingException(Exception e) {
    super(e);
  }
  
  public PostProcessingException(String reason) {
    super(reason);
  }
}
