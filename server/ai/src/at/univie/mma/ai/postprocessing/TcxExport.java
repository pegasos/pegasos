package at.univie.mma.ai.postprocessing;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import activities.Db_connector;
import activities.NoPost;
import at.univie.mma.ai.Parameters;

public class TcxExport extends ConvenienceBasePostProcessingModule {
  private long session_id;
  
  private String sport;
  private long startt;
  private long endt;
  
  public void export() throws SQLException, IOException
  {
    prepare();
    
    loadData();
    header();
    
    data();
    
    footer();
  }
  
  private void loadData() throws SQLException
  {
    final String sql= "select starttime, endtime, activity from session where session_id = " + session_id;
    PreparedStatement  st = null;
    ResultSet  rs = null;
    
    st = db.createStatement(sql);
    rs = st.executeQuery();
    
    rs.next();
    startt= rs.getLong(1);
    endt= rs.getLong(2);
    
    if( rs.getInt(3) == 1 )
      sport= "running";
    else
      sport= "cycling";
    
    if( sport.equals("cycling") )
      loadDataCycling();
    else if( sport.equals("running") )
      loadDataRunning();
    
  }
  
  private static abstract class DataPoint {
    public long rec_time;
  }
  
  private static class DataHrPoint extends DataPoint {
    public int hr;
    
    public DataHrPoint(long t, int h) {
      this.rec_time= t;
      this.hr= h;
    }
  }
  
  private static class DataBikePoint extends DataPoint {
    public int speed_mm_s;
    public long distance;
    public int cadence;
    public int power;
    
    public DataBikePoint(long t, int s, long d, int c, int p) {
      this.rec_time= t;
      this.speed_mm_s= s;
      this.distance= d;
      this.cadence= c;
      this.power= p;
    }
  }
  
  private static class DataFpPoint extends DataPoint {
    public int speed_mm_s;
    public long distance;
    public int strides;
    
    public DataFpPoint(long t, int s, long d, int st) {
      this.rec_time= t;
      this.speed_mm_s= s;
      this.distance= d;
      this.strides= st;
    }
  }
  
  private static class DataGpsPoint extends DataPoint {
    public int alt_cm;
    public int speed_cm_s;
    public double lat;
    public double lon;
    
    public DataGpsPoint(long t, int a, int s, double la, double lo) {
      this.rec_time= t;
      this.alt_cm= a;
      this.speed_cm_s= s;
      this.lat= la;
      this.lon= lo;
    }
  }
  
  private ArrayList<DataHrPoint> hr_data= new ArrayList<DataHrPoint>();
  private ArrayList<DataBikePoint> bike_data= new ArrayList<DataBikePoint>();
  private ArrayList<DataFpPoint> fp_data= new ArrayList<DataFpPoint>();
  private ArrayList<DataGpsPoint> gps_data= new ArrayList<DataGpsPoint>();
  
  private boolean hr_gps_only= true;

  private BufferedWriter outfile;
  
  private void loadDataCycling() throws SQLException
  {
    PreparedStatement  st = null;
    ResultSet  rs = null;
    String sql;
    
    sql= "select rec_time, hr from hr where session_id = " + session_id + " ORDER BY rec_time ASC";
    st = db.createStatement(sql);
    rs = st.executeQuery();
    while( rs.next() )
    {
      hr_data.add(new DataHrPoint(rs.getLong(1), rs.getInt(2)));
    }
    
    // TODO: check if sensor_nr = 1 is available
    sql= "select rec_time, speed_mm_s, distance_m, cadence, power from bike where session_id = " + session_id + " and sensor_nr = 1 ORDER BY rec_time ASC";
    rs = st.executeQuery(sql);
    while( rs.next() )
    {
      long r= rs.getLong(1);
      int s= rs.getInt(2);
      long d= rs.getLong(3);
      int c= rs.getInt(4);
      int p= rs.getInt(5);
      
      bike_data.add(new DataBikePoint(r,s,d,c,p));
    }
    
    sql= "select rec_time, alt_cm, speed_cm_s, acc, bearing, lat, lon, distance_m from gps where session_id = " + session_id + " ORDER BY rec_time ASC";
    rs = st.executeQuery(sql);
    while( rs.next() )
    {
      gps_data.add(new DataGpsPoint(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getDouble(6), rs.getDouble(7)));
    }
  }
  
  private void loadDataRunning() throws SQLException
  {
    PreparedStatement  st = null;
    ResultSet  rs = null;
    String sql;
    
    sql= "select rec_time, hr from hr where session_id = ? ORDER BY rec_time ASC";
    st = db.createStatement(sql);
    
    // rs = st.executeQuery(sql);
    st.setLong(1, session_id);
    rs = st.executeQuery();
    while( rs.next() )
    {
      hr_data.add(new DataHrPoint(rs.getLong(1), rs.getInt(2)));
    }
    
    sql= "select rec_time, speed_mm_s, distance_m, strides_255 from foot_pod where session_id = " + session_id + " ORDER BY rec_time ASC";
    rs = st.executeQuery(sql);
    while( rs.next() )
    {
      long r= rs.getLong(1);
      int s= rs.getInt(2);
      long d= rs.getLong(3);
      int c= rs.getInt(4);
      
      fp_data.add(new DataFpPoint(r,s,d,c));
    }
    
    sql= "select rec_time, alt_cm, speed_cm_s, acc, bearing, lat, lon, distance_m from gps where session_id = " + session_id + " ORDER BY rec_time ASC";
    rs = st.executeQuery(sql);
    while( rs.next() )
    {
      gps_data.add(new DataGpsPoint(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getDouble(6), rs.getDouble(7)));
    }
  }
  
  private void header()
  {
    /*
    <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
    <TrainingCenterDatabase>
    <Activities>
      <Activity Sport="Biking">
        <Id>2016-08-14T07:27:09.000Z</Id>
        <Lap StartTime="2016-08-14T07:27:09.000Z">
    */
    output("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
    output("<TrainingCenterDatabase xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\">");
    output("  <Activities>");
    output("    <Activity Sport=\"" + sport + "\">");
    output("      <Id>" + timeToStr(startt) + "</Id>");
  }
  
  private void data()
  {
    output("        <Lap StartTime=\"" + timeToStr(startt) + "\">");
    output("          <TotalTimeSeconds>" + (int)((endt - startt)/1000.0) + "</TotalTimeSeconds>");
    output("          <Intensity>Active</Intensity>");
    output("          <TriggerMethod>Manual</TriggerMethod>");
    // output("         <DistanceMeters>0</DistanceMeters>");
    output("          <Track>");
    if( hr_gps_only )
    {
      int i_h= 0;
      int i_g= 0;
      
      int hr= 0;
      double alt= 0;
      double lat= 0;
      double lon= 0;
      
      boolean done= false;
      while(!done)
      {
        long mit= Math.min(hr_data.get(i_h).rec_time,gps_data.get(i_g).rec_time);
        long mat= Math.max(hr_data.get(i_h).rec_time,gps_data.get(i_g).rec_time);
        if( mat - mit > 1000 )
        {
          // TODO:
        }
        else
        {
          hr= hr_data.get(i_h).hr;
          alt= gps_data.get(i_g).alt_cm / 100;
          lat= gps_data.get(i_g).lat;
          lon= gps_data.get(i_g).lon;
        }
        
        alt= 0;
        
        output("<Trackpoint>");
        output("<Time>" + timeToStr(mat) + "</Time>");
        // output("<DistanceMeters>0</DistanceMeters>");
        output("<Position><LatitudeDegrees>" + lat + "</LatitudeDegrees><LongitudeDegrees>" + lon + "</LongitudeDegrees></Position>");
        // output("<AltitudeMeters>" + alt + "</AltitudeMeters>");
        output("<HeartRateBpm><Value>" + hr + "</Value></HeartRateBpm>");
        output("</Trackpoint>");
        
        i_g++;
        i_h++;
        
        if( i_g == gps_data.size() || i_h == hr_data.size() )
          done= true;
      }
    }
    else
    {
      int i_h= 0;
      int i_g= 0;
      int i_d= 0;
      
      int hr= 0;
      double alt= 0;
      double lat= 0;
      double lon= 0;
      
      ArrayList<? extends DataPoint> data;
      if( sport.equals("cycling") )
        data= bike_data;
      else
        data= fp_data;
      
      boolean done= false;
      while(!done)
      {
        long mit= Math.min(hr_data.get(i_h).rec_time,Math.min(data.get(i_d).rec_time, gps_data.get(i_g).rec_time));
        long mat= Math.max(hr_data.get(i_h).rec_time,Math.max(data.get(i_d).rec_time, gps_data.get(i_g).rec_time));
        
        if( mat - mit > 1000 )
        {
          // TODO: 
        }
        else
        {
          hr= hr_data.get(i_h).hr;
          alt= gps_data.get(i_g).alt_cm / 100;
          lat= gps_data.get(i_g).lat;
          lon= gps_data.get(i_g).lon;
        }
        
        alt= 0;
        
        output("<Trackpoint>");
        output("<Time>" + timeToStr(mat) + "</Time>");
        // output("<DistanceMeters>0</DistanceMeters>");
        output("<Position><LatitudeDegrees>" + lat + "</LatitudeDegrees><LongitudeDegrees>" + lon + "</LongitudeDegrees></Position>");
        // output("<AltitudeMeters>" + alt + "</AltitudeMeters>");
        output("<HeartRateBpm><Value>" + hr + "</Value></HeartRateBpm>");
        // output(data.get(i).data());
        // TODO:
        output("</Trackpoint>");
        
        i_g++;
        i_h++;
        i_d++;
        
        if( i_g == gps_data.size() || i_h == hr_data.size() )
          done= true;
      }
    }
    output("          </Track>");
    output("        </Lap>");
    /*
    <Trackpoint>
      <Time>2016-08-14T07:27:12.000Z</Time>
      <Position>
        <LatitudeDegrees>47.26071667</LatitudeDegrees>
        <LongitudeDegrees>9.61837833</LongitudeDegrees>
      </Position>
      <AltitudeMeters>439.679</AltitudeMeters>
      <DistanceMeters>30.676895777384438</DistanceMeters>
      <HeartRateBpm><Value>119</Value></HeartRateBpm>
      <Cadence>91</Cadence><SensorState>Present</SensorState></Trackpoint>
    */
    /*
    function trackpoint($time,$distance,$bpm,$speed,$cadence) {
    $ret="";
      $ret.="<Trackpoint>\n";
      $ret.="<Time>".date('Y-m-d',($time/1000))."T".date('H:i:s',($time/1000))."Z</Time>\n"; // 2012-10-20T12:07:27Z
      $ret.="<DistanceMeters>".$distance."</DistanceMeters>\n";
      $ret.="<HeartRateBpm xsi:type=\"HeartRateInBeatsPerMinute_t\">\n";
      $ret.="<Value>".$bpm."</Value>\n";
    //echo "<Value>".round($speed*36)."</Value>\n";
      $ret.="</HeartRateBpm>\n";
      $ret.="<Extensions>\n";
      $ret.="<TPX xmlns=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\" CadenceSensor=\"Footpod\">\n";
      $ret.="<Speed>".$speed."</Speed>\n";
      $ret.="<RunCadence>".$cadence."</RunCadence>\n";
      $ret.="</TPX>\n";
      $ret.="</Extensions>\n";
      $ret.="</Trackpoint>\n";
    if (($bpm>0) && ($speed>0)) {return $ret;} else {return "";}
    }
    */
  }
  
  private void footer()
  {
    // output("<Creator xsi:type=\"Device_t\"><Name>MMA</Name><UnitId>0</UnitId><ProductID>13</ProductID></Creator>");
    // output("<Creator xsi:type=\"Device_t\"><Name>MMA</Name></Creator>");
    output("</Activity>");
    output("</Activities>");
    // <Author xsi:type=\"Application_t\"><Name>no application</Name></Author>
    output("</TrainingCenterDatabase>");
  }
  
  private void prepare() throws IOException
  {
    outfile= Files.newBufferedWriter(Paths.get("/tmp").resolve(session_id + ".tcx"), Charset.defaultCharset());
  }
  
  private void output(String line)
  {
    // System.out.println(line);
    try {
      outfile.write(line);
      outfile.newLine();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  private final static SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
  private String timeToStr(long time)
  {
    Date dNow = new Date(time);
    
    return ft.format(dNow);
  }
  
  public static void main(String[] args) throws PostProcessingException {
    TcxExport e= new TcxExport();
    
    e.runIt(args);
  }

  @Override
  public void process(long session_id, long user_id) throws PostProcessingException
  {
    this.session_id= session_id;
    try
    {
      export();
    }
    catch (SQLException | IOException e)
    {
      e.printStackTrace();
      throw new PostProcessingException(e);
    }
  }
}
