package at.univie.mma.ai.postprocessing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import activities.AI_module;
import activities.Db_connector;
import activities.NoPost;
import at.pegasos.ai.util.log.LoggerSetup;
import at.pegasos.ai.util.log.Stacktrace;
import at.univie.mma.ai.Parameters;
import at.univie.mma.ai.PostProcessingModules;
import org.slf4j.LoggerFactory;

public class PostProcessor {
  private final List<PostProcessingModule> modules;
  private final AI_module ai_module;
  
  public PostProcessor(AI_module ai_module, Parameters p) {
    ai_module.log("PostProcessor created for " + ai_module.getClass().getName());
    
    this.ai_module= ai_module;
    
    Db_connector db= new Db_connector();
    db.open_db();
    
    ResultSet res = db.sql("select userid from session where session_id=" + p.session,0);
    long user_id= 0;
    try
    {
      if( res.next() )
      {
        user_id = res.getLong(1);
      }
    }
    catch (SQLException e)
    {
      ai_module.logStackTrace(e);
    }
    
    List<Class<? extends PostProcessingModule>> mods = PostProcessingModules.fromActivity(p);
    if( mods.isEmpty() )
      modules= new ArrayList<PostProcessingModule>(0);
    else
    {
      modules= new ArrayList<PostProcessingModule>(mods.size());
      for(Class<? extends PostProcessingModule> clasz : mods)
      {
        try
        {
          PostProcessingModule m= clasz.getConstructor().newInstance();
          m.addUser(user_id, p.session);
          m.setDB(db);
          modules.add(m);
        }
        catch (InstantiationException | IllegalAccessException e) {
          ai_module.log("Error instanciating " + clasz.getCanonicalName());
          e.printStackTrace();
        }
        catch (Exception e)
        {
          ai_module.log("Exception running " + clasz.getCanonicalName() + ": " + e + " " + e.getMessage());
          // Stacktrace.logStackTrace(e);
          ai_module.logStackTrace(e);
        }
      }
    }
  }
  
  public boolean hasModules()
  {
    return modules.size() > 0;
  }

  public void run()
  {
    for(PostProcessingModule mod : modules)
    {
      try
      {
        ai_module.log("PostProcessor: running " + mod);
        mod.process(ai_module);
      }
      catch( PostProcessingException e )
      {
        ai_module.log("Error running " + mod.getClass().getCanonicalName());
        ai_module.logStackTrace(e);
        // e.printStackTrace();
      }
      catch (Exception e)
      {
        ai_module.log("Exception running " + mod.getClass().getCanonicalName() + ": " + e + " " + e.getMessage());
        // Stacktrace.logStackTrace(e);
        ai_module.logStackTrace(e);
      }
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      long sessionId= Long.parseLong(args[0]);
      LoggerSetup.setupLogging(sessionId);
      LoggerFactory.getLogger(PostProcessor.class.getName()).info("Starting post processor from main / commandline");

      // new PostProcessor(AI_module ai_module, Parameters p)
      Parameters p= new Parameters();

      Db_connector db= new Db_connector();
      if( db.open_db() != 1 )
        System.err.println("Error opening DB");
      PreparedStatement s= db.createStatement(
          "select activity, param1, param2, ai from session where session_id = ?");

      s.setLong(1, sessionId);
      ResultSet res=s.executeQuery();
      if( res == null || res.next() == false )
      {
        System.err.println("Error getting session info");
        System.exit(1);
      }

      p.param0=res.getInt(1);
      p.param1=res.getInt(2);
      p.param2=res.getInt(3);
      p.session=Integer.parseInt(args[0]);
      p.ai=res.getInt(4);
      p.restart=0;
      p.lang=0;

      // new PostProcessor(new Default(p.session,p.param0, p.lang), p).run();
      new PostProcessor(new NoPost(p), p).run();
    }
    catch( Exception e )
    {

    }
  }
}
