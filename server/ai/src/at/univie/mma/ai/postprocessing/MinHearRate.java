package at.univie.mma.ai.postprocessing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import activities.AI_module;
import activities.Db_connector;

public class MinHearRate implements PostProcessingModule {
  /**
   * IDs of the users for which the activity was started
   */
  private long[] user_ids;
  
  /**
   * IDs of the sessions
   */
  private long[] session_ids;
  
  private Db_connector db;
  
  @Override
  public void addUser(long user_id, long session_id) {
    this.user_ids= new long[1];
    user_ids[0]= user_id;
    this.session_ids= new long[1];
    session_ids[0]= session_id;
  }

  @Override
  public void addUser_batch(long[] user_ids, long[] session_ids) {
    this.user_ids= user_ids;
    this.session_ids= session_ids;
  }
  
  @Override
  public void setDB(Db_connector db)
  {
    this.db= db;
  }

  @Override
  public void process(AI_module ai) throws PostProcessingException
  {
    try
    { //TODO: ident
    // TODO: it would be nice if updating user parameters would be a function of db. AI should not need to know about data-fields
    PreparedStatement update= db.createStatement("UPDATE user SET data4 = ?,data4_crtime=? WHERE id = ?");
    
    PreparedStatement get= db.createStatement("SELECT min(hr) FROM hr WHERE session_id = ?");
    
    ResultSet res;
    int hr_max;
    
    for(int i= 0; i < session_ids.length; i++)
    {
      get.setLong(1, session_ids[i]);
      res= get.executeQuery();
      ai.log("Getting min hr from " + session_ids[i] + " for " + user_ids[i]);
      if( res.next() )
      {
        // ai.log("Gefunden!");
        hr_max= res.getInt(1);
        
        // res=db.sql("update user set data1="+hr_max+" where id="+user_id,1);
        update.setInt(1, hr_max);
        update.setLong(2, System.currentTimeMillis() / 1000);
        update.setLong(3, user_ids[i]);
        update.executeUpdate();
      }
      else
      {
        //TODO: report error
      }
    }
  }
  catch(SQLException e)
  {
    e.printStackTrace();
    throw new PostProcessingException(e);
  }
  }
}
