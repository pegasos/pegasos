package at.univie.mma.ai.postprocessing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import activities.AI_module;
import activities.Db_connector;
import activities.NoPost;
import at.univie.mma.ai.Parameters;

public abstract class ConvenienceBasePostProcessingModule extends BasePostProcessingModule {
  protected AI_module ai;
  
  @Override
  public void process(AI_module ai) throws PostProcessingException
  {
    this.ai= ai;
    
    if( this.session_ids.length != this.user_ids.length )
      throw new IllegalStateException("Session and user info has different length");
    
    for(int i= 0; i < session_ids.length; i++)
      process(session_ids[i], user_ids[i]);
  }
  
  protected abstract void process(long session_id, long user_id) throws PostProcessingException;
  
  protected void runIt(String[] args) throws PostProcessingException
  {
    db= new Db_connector();
    db.open_db();
    
    session_ids= new long[1];
    session_ids[0]= Integer.parseInt(args[0]);
    user_ids= new long[1];
    
    try
    {
      PreparedStatement st= db.createStatementRO("select userid, activity, param1, param2 from session where session_id = " + session_ids[0]);
      st.execute();
      ResultSet res= st.getResultSet();
      res.next();
      user_ids[0]= res.getLong(1);
      
      Parameters p= new Parameters();
      p.session= (int) session_ids[0];
      p.param0= res.getInt(2);
      p.param1= res.getInt(3);
      p.param2= res.getInt(4);
      
      process(new NoPost(p));
    }
    catch( SQLException e )
    {
      System.out.println(e.getMessage());
      e.printStackTrace();
      throw new PostProcessingException(e);
    }
  }
}
