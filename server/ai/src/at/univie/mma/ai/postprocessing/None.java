package at.univie.mma.ai.postprocessing;

import activities.AI_module;
import activities.Db_connector;

public class None implements PostProcessingModule {
  @Override
  public void addUser(long user_id, long session_id) {
    
  }

  @Override
  public void addUser_batch(long[] user_ids, long[] session_ids) {
    
  }
  
  @Override
  public void setDB(Db_connector db) {
    
  }

  @Override
  public void process(AI_module ai) throws PostProcessingException {
    
  }
  
}
