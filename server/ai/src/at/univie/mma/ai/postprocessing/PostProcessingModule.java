package at.univie.mma.ai.postprocessing;

import activities.AI_module;
import activities.Db_connector;

public interface PostProcessingModule {
  
  /**
   * Add a user for whom the post-processing should be done. 
   * TODO: this function is allowed to throw an exception if the module is designed for multi user operation
   * @param user_id
   * @param session_id
   */
  public void addUser(long user_id, long session_id);
  
  /**
   * Add a users for which the post-processing should be done. This should be the preferred method when 
   * batch processing is done
   * TODO: this function is allowed to throw an exception if the module is designed for single user operation
   * @param user_id
   * @param session_id
   */
  public void addUser_batch(long user_ids[], long[] session_ids);
  
  /**
   * Set Database access
   * @param db
   */
  public void setDB(Db_connector db);
  
  /**
   * Do the post-processing
   * @throws PostProcessingException 
   */
  // public void process() throws PostProcessingException;

  public void process(AI_module ai) throws PostProcessingException;
}
