package at.univie.mma.ai.postprocessing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import activities.AI_module;
import activities.Db_connector;

public class HRVParams implements PostProcessingModule {
  public static class HRVParameters {
    private ArrayList<Integer> rr_ints;
    
    public double rmssd;
    public int max;
    public int min;
    public double rmsm;
    public double avg;
    public double hrv_score;
    public double pNN50;
    public double sdsd;
    public double SI;
    
    public HRVParameters(ArrayList<Integer> rr_vals) {
      rr_ints= new ArrayList<Integer>(rr_vals);
    }
    
    public HRVParameters(int[] rr_data) {
      rr_ints= new ArrayList<Integer>();
      for(int i= 0; i < rr_data.length; i++)
        rr_ints.add(rr_data[i]);
    }
    
    public void calculate() {
      int i;
      
      int prev= rr_ints.get(0);
      int cur;
      double rmssd= 0;
      double rmsm= 0;
      double avg= prev;
      double N= rr_ints.size();
      double pNN50= 0;
      double sdsd= 0;
      double avg_diff= 0;
      double modal= 0;
      double var_spread= 0;
      double AMo= 0;
      int diffs[] =new int[Double.valueOf(N).intValue()-1];
      int bin_count;
      double bin_width;
      int bins[];
      
      this.max= prev;
      this.min= prev;
      
      for(i= 1; i < N; i++)
      {
        cur= rr_ints.get(i);
        if( cur > this.max ) 
          this.max= cur;
        if( cur < this.min ) 
          this.min= cur;
        
        int diff= prev - cur;
        avg_diff+= diff;
        diffs[i-1]= diff;
        
        if( diff > 50 )
          pNN50 += 1;
        
        // if( i >= 2 )
        //  rmssd+= (diffs[i-2]-diffs[i-1]) * (diffs[i-2]-diffs[i-1]);
        rmssd+= diff * diff;
        avg+= cur;
        
        // System.out.println("prev " + prev + " cur " + cur + " diff " + diff + " rmssd" + rmssd);
        
        prev= cur;
      }
      
      avg/= N;
      avg_diff/= (N-1);
      
      for(i= 0; i < N; i++)
      {
        double h= rr_ints.get(i) - avg;
        rmsm+= h*h;
      }
      
      for(i= 0; i < N-1;i++)
      {
        double h= diffs[i] - avg_diff;
        sdsd+= h*h;
      }
      
      rmssd= rmssd / (N - 1);
      rmsm= rmsm / N;
      sdsd= sdsd / (N - 2);
      
      var_spread= this.max - this.min;
      bin_width= 50;
      bin_count= Double.valueOf(var_spread / bin_width).intValue() + 1;
      bins= new int[bin_count];
      for(i= 0; i < N; i++)
      {
        int h;
        
        h= rr_ints.get(i) - min;
        h/= bin_width;
        bins[h]++;
      }
      
      int mmax= 0;
      for(i= 0; i < bin_count; i++)
      {
        System.out.println("Bin " + i + ": " + bins[i]);  
        if( bins[i] > mmax )
        {
          modal= min + bin_width * i;
          mmax= bins[i];
          // AMo= bins[i] / N * 100.0;
        }
      }
      
      System.out.println("Max: " + max);
      System.out.println("Min: " + min);
      System.out.println("Bin_c: " + bin_count);
      System.out.println("Bin_w: " + bin_width);
      System.out.println("Var_spread: " + var_spread);
      
      AMo= mmax / N * 100;
      this.SI= AMo / (2 * (modal/1000.0) * (var_spread/1000.0));
      
      this.rmssd= Math.sqrt( rmssd );
      this.rmsm= Math.sqrt( rmsm );
      this.pNN50= pNN50 / N;
      this.sdsd= Math.sqrt( sdsd );
      this.hrv_score= 20 * Math.log(this.rmssd);
      this.avg= avg;
      
      System.out.println("Modal " + modal);
      System.out.println("Amo " + AMo);
      System.out.println("mmax " + mmax);
      System.out.println("var_spread " + var_spread);
      System.out.println("max " + max);
      System.out.println("min " + min);
      System.out.println("rmssd " + this.rmssd);
    }
  }
  
  /**
   * IDs of the users for which the activity was started
   */
  private long[] user_ids;
  
  /**
   * IDs of the sessions
   */
  private long[] session_ids;
  
  private Db_connector db;
  
  @Override
  public void addUser(long user_id, long session_id) {
    this.user_ids= new long[1];
    user_ids[0]= user_id;
    this.session_ids= new long[1];
    session_ids[0]= session_id;
  }

  @Override
  public void addUser_batch(long[] user_ids, long[] session_ids) {
    this.user_ids= user_ids;
    this.session_ids= session_ids;
  }
  
  @Override
  public void setDB(Db_connector db)
  {
    this.db= db;
  }

  @Override
  public void process(AI_module ai) throws PostProcessingException
  {
    ai.log(Arrays.toString(session_ids));
    try
    {
      ResultSet res;
      String sql;
      
      for(int i= 0; i < session_ids.length; i++)
      {
        ArrayList<Integer> rr= new ArrayList<Integer>(); //TODO: allocate size depending on row count
        
        res= db.sql("select starttime,endtime from session where session_id = " + session_ids[i], 0);
        res.next();
        double startt= res.getDouble(1);
        double endt= res.getDouble(2);
        
        ai.log(session_ids[i] + " " + startt + " " + endt);
        
        sql= "select rec_time,hr,v1,v2,v3,v4 from hr where session_id = " + session_ids[i] + " ORDER BY rec_time ASC";
        System.out.println(sql);
        res= db.sql(sql, 0);
        
        int v1_last= 0;
        int v2_last= 0;
        int v3_last= 0;
        int v4_last= 0;
        
        int hr_min= Integer.MAX_VALUE;
        
        List<Integer> hrs= new ArrayList<Integer>(5*4);
        List<Long> times= new ArrayList<Long>(5*4);
        
        while(res.next())
        {
          double rec_time= res.getDouble(1);
          int hr= res.getInt(2);
          int v1= res.getInt(3);
          int v2= res.getInt(4);
          int v3= res.getInt(5);
          int v4= res.getInt(6);
          
          if( rec_time - 10000 < startt )
            continue;
          if( rec_time + 10000 > endt )
            break;
          
          // ai.log(String.format("%f %d %d %d %d %d", rec_time, hr, v1, v2, v3, v4));
          
          // TODO: better algorithm for failure detection
          if( v1 == v1_last && v2 == v2_last )
          {
            
          }
          else
          {
            if( v1 != 0 )
            {
              rr.add(v1);
              if( v2 != 0 )
              {
                rr.add(v2);
                if( v3 != 0 )
                {
                  rr.add(v3);
                  if( v4 != 0 )
                  {
                    rr.add(v4);
                  }
                }
              }
            }
          }
          
          v1_last= v1;
          v2_last= v2;
          v3_last= v3;
          v4_last= v4;
          
          times.add((long) rec_time);
          hrs.add(hr);
          if( times.size() > 1 )
          {
            if( times.get(times.size() - 1) - times.get(0) >= 5.000 )
            {
              long sum= 0;
              for(Integer h : hrs )
              {
                sum+= h;
              }
              int hr_avg= (int) (sum / (double) times.size());
              while( times.get(times.size() - 1) - times.get(0) >= 5.000 )
              {
                times.remove(0);
                hrs.remove(0);
              }
              
              if( hr_avg < hr_min )
                hr_min= hr_avg;
            }
          }
          
          // System.out.println(hr_min);
        }
        
        HRVParameters p = new HRVParameters(rr);
        p.calculate();
        ai.log(String.format("HRV: rmssd %.2f, rmsm %.2f, pNN50 %.2f, sdsd %.2f, score: %.2f, avg: %.2f", p.rmssd, p.rmsm, p.pNN50, p.sdsd, p.hrv_score, p.avg));
        PreparedStatement st = db.createStatement("INSERT INTO `hrv_params`(`session_id`, `rmssd`, `rmsm`, `pNN50`, `sdsd`, `avg`, `max`, `hr`) VALUES (?,?,?,?,?,?,?,?)");
        st.setLong(1, session_ids[i]);
        st.setInt(2, (int) p.rmssd);
        st.setInt(3, (int) p.rmsm);
        st.setInt(4, (int) p.pNN50 * 100);
        st.setInt(5, (int) p.sdsd);
        st.setInt(6, (int) p.avg);
        st.setInt(7, (int) p.max);
        st.setInt(8, hr_min);
        st.execute();
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      throw new PostProcessingException(e);
    }
  }
}
