package at.univie.mma.ai.postprocessing;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import activities.AI_module;

public class Unterricht extends BasePostProcessingModule {
  @Override
  public void process(AI_module ai) throws PostProcessingException
  {
    try
    {
    PreparedStatement update= db.createStatement("UPDATE session SET v3=?, v5=?, v7=? WHERE session_id = ?");
    
    PreparedStatement get_hr= db.createStatement("SELECT max(hr) FROM hr WHERE session_id = ?");
    PreparedStatement get_fp= db.createStatement("SELECT max(distance_m) as dist, max(strides_255) as steps FROM foot_pod WHERE session_id = ?");
    
    boolean update_added= false;
    
    ResultSet res;
    int hr_max;
    int dist;
    int steps;
    
    for(int i= 0; i < session_ids.length; i++)
    {
      get_hr.setLong(1, session_ids[i]);
      res= get_hr.executeQuery();
      ai.log("Getting parameters from " + session_ids[i] + " for " + user_ids[i]);
      hr_max= steps= dist= 0;
      
      if( res.next() )
        hr_max= res.getInt(1);
      else
      {
        //TODO: report error
      }
      
      get_fp.setLong(1, session_ids[i]);
      res= get_fp.executeQuery();
      if( res.next() )
      {
        dist= res.getInt(1);
        steps= res.getInt(2);
      }
      else
      {
        //TODO: report error
      }
      
      if( hr_max != 0 || dist != 0 || steps != 0 )
      {
        ai.log("Parameters for " + session_ids[i] + "/" + user_ids[i] + " " + hr_max + " " + dist + " " + steps);
        update.setInt(1, dist); //dist
        update.setInt(2, steps); //steps
        update.setInt(3, hr_max);
        update.setLong(4, session_ids[i]);
        update.addBatch();
        update_added= true;
      }
    }
    
    if(update_added )
      update.executeBatch();
    
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      throw new PostProcessingException(e);
    }
  }
}
