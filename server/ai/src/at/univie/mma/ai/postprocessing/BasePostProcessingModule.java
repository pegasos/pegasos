package at.univie.mma.ai.postprocessing;

import activities.Db_connector;

public abstract class BasePostProcessingModule implements PostProcessingModule {
  /**
   * IDs of the users for which the activity was started
   */
  protected long[] user_ids;
  
  /**
   * IDs of the sessions
   */
  protected long[] session_ids;
  
  protected Db_connector db;
  
  @Override
  public void addUser(long user_id, long session_id)
  {
    this.user_ids= new long[1];
    user_ids[0]= user_id;
    this.session_ids= new long[1];
    session_ids[0]= session_id;
  }
  
  @Override
  public void addUser_batch(long[] user_ids, long[] session_ids)
  {
    this.user_ids= user_ids;
    this.session_ids= session_ids;
  }
  
  @Override
  public void setDB(Db_connector db)
  {
    this.db= db;
  }
}
