package at.univie.mma.ai.activities;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;



public class Intervalltraining extends AI_module {

	private double speed;
	private String feedb="";
	private ResultSet res1=null;
	private int time=0,duration_sec,speed_mm_s,mistakes=0,mistakes_hr=0,user_id;
	private int mistakes_allowed=10;
	private String feedback;
	private ResultSet res=null;
	private final long session;
	private int hr_max_perc,hr_min_perc,hr,hr_max,param1;
	
	public Intervalltraining(Parameters p) {
		super(p);
		this.session= p.session;
		this.param1= p.param1; //TODO: check if this is really param1 and not param2
	    log("Intervalltraining Level "+param1+" started!");
	    
	    
	       
        res=db.sql("select data2,id from user where id in (select userid from session where session_id="+session+")",0); // GET HR_MAX
        try {if (res.first()) {hr_max=res.getInt(1);user_id=res.getInt(2);}} catch (SQLException e) {}
        if (hr_max==0) {hr_max=180;}
	    
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
	    res1=db.sql("select section,duration_sec,speed_mm_s,feedback,hr_min_perc,hr_max_perc from intervalltraining where type="+param1+" order by section", 0);
		start_timer();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	
	private void insert_fp_data() {
		 // FP-Calibration-Intervall? --> get FP-Calibration Factors
   	  
   		  log ("CHECK FP_CALIBRATION");
   		  res1=db.sql("insert into user_fp_calib(user_id,session_id) values ("+user_id+","+session+")",1);
   		  
   		    sql="select speed_mm_s from foot_pod where session_id="+session+" order by rec_time asc";   // GET ACTUAL SPEED
            res=db.sql(sql,0);
            int step=7;
            int counter=0;
            double fp_sum=0;
            try {
				while (res.next()) {
				 log (""+fp_sum);
				 if ((counter>25) && (counter<56)) {
					 fp_sum+=res.getInt(1);
				 } else 
				 if (counter==59) {
				  double ca=Math.floor(step/((fp_sum*0.0036/30.0000))*1000.000);
				  res1=db.sql("update user_fp_calib set a"+step+"="+ca+" where session_id="+session, 1);
				  step++;
				  if (param1==4) {counter=-1;} else if (param1==5) {counter=-46;}
				  fp_sum=0;
				 }
				 
				 counter++;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
   	  
	}
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive_3sec()) 
	        	 {
	        		if ((param1==4) || (param1==5)) {insert_fp_data();}
	        		log("Killed AI-Session!");stop_timer();close_ai();
	        	 }
	        	
	        	try {
                     if (time==0) {
                    	  time++;
                          if (res1.next()) {	
                              duration_sec=res1.getInt(2);
                              speed_mm_s=res1.getInt(3);
                              feedback=res1.getString(4);
                              hr_min_perc=res1.getInt(5);
                              hr_max_perc=res1.getInt(6);
                              send_feedback(feedback,0);
                          } else {
                        	  send_feedback("Intervalltraining abgeschlossen",1);
                        	  if (param1==4) {insert_fp_data();}
                        	  if (param1==5) {insert_fp_data();}
 	    			    	  stop_timer();
 	    			    	  close_ai();
                          }
                     } else {
                    	  sql="select v4,v1 from session where session_id="+session;   // GET ACTUAL SPEED, HR
                          res=db.sql(sql,0);
                          if (res.first()) {speed=res.getDouble(1)*0.0036;hr=res.getInt(2);}
                          double tmp1;
                          
                          
                          if ((speed_mm_s>0) && (param1!=4) && (param1!=5)) {
                             tmp1=speed-speed_mm_s*0.0036;
                             
                             if (Math.abs(tmp1)>0.3) {mistakes++;}
                             if ((mistakes>mistakes_allowed) && (tmp1>0.3)) {send_feedback("Langsamer um "+(Math.round((tmp1)*10)/10.0d)+" km/h",0);mistakes=0;} 
                             if ((mistakes>mistakes_allowed) && (tmp1<0.3)) {send_feedback("Schneller um "+Math.abs(Math.round((tmp1)*10)/10.0d)+" km/h",0);mistakes=0;}
                          }  
                          
                          if ((hr_max_perc>0) && (param1!=4) && (param1!=5)) {
                        	  
                        	  if (hr>(hr_max*(hr_max_perc/100.000))) {mistakes_hr++;feedb="Herzfrequenz ist zu hoch";}
                        	  if (hr<(hr_max*(hr_min_perc/100.000))) {mistakes_hr++;feedb="Herzfrequenz ist zu niedrig";}
                              //if (mistakes_hr>mistakes_allowed) {send_feedback("Da stimmt was nicht",0);mistakes_hr=0;} 
                        	  if (mistakes_hr>mistakes_allowed) {send_feedback(feedb,0);mistakes_hr=0;}
                          }
                          
                          time++;
         				  if (time>duration_sec) {time=0;mistakes=0;}
         				  //if (time_fb>30) {send_feedback("1",0);time_fb=0;}
                     }
	        	} catch (SQLException e) {}
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
   
	
}
