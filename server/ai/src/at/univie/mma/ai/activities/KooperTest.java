package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import activities.*;
import at.univie.mma.ai.Parameters;
import lib.kooper_array;

// 12 Minuten laufen --> entsprechend Distanz werden abhängig von Alter und Geschlecht Punkte vergeben
// Bei dem Test auf Max HR achten und entsprechende Feedback geben!
// Jede Minute ansagen "noch x Minuten!"


public class KooperTest extends AI_module {

	private final long session;
	private ResultSet res=null;
	private int time=0,time60=0,hr=0;
	private int minutes_left=12;
	private kooper_array k;
	private int user_id,gender,age,dist,level=5,max_hr=0,send_feedback=0;
	private long birthday; 
	
	
	public KooperTest(Parameters p) {
		super(p);
		this.session= p.session;
	    log("Koopertest started!");
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
	    
	    try {
	       sql="select userid from session where session_id="+session;   // GET ACTUAL HR
    	   res=db.sql(sql,0);
  		   if (res.first()) {user_id=res.getInt(1);}
	    
	       sql="select data1 from user where id="+user_id;        // GET MAX_HR
    	   res=db.sql(sql,0);
           if (res.first()) {max_hr=res.getInt(1);}
	    } catch (SQLException e) {
	    	send_feedback("Koopertest konnte nicht gestartet werden.",1);
	    	log("Killed AI-Session!");stop_timer();close_ai();
		} 
	    
	    if (max_hr<50) 
	      {send_feedback("Koopertest wurde gestartet. Achtung, kein Feedback zur Herzfrequenz möglich!",0);max_hr=300;}
	    else
	      {send_feedback("Koopertest wurde gestartet",0);}
	    
	    k = new kooper_array();
		start_timer();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
	        	
	        	try {
                     time++;
                     time60++;
                   
                    if (time>=60*12) {
                     	sql="select v3,userid from session where session_id="+session;   // GET ACTUAL DIST
                    	res=db.sql(sql,0);
                        if (res.first()) {dist=res.getInt(1);user_id=res.getInt(2);}
                        
                        sql="select gender,birthday from user where id="+user_id;        // GET gender,birthday from user
                    	res=db.sql(sql,0);
                        if (res.first()) {gender=res.getInt(1);birthday=res.getInt(2);}
                        
                        //log("A:"+System.currentTimeMillis());
                        //log("B:"+birthday*1000);
                        
                        long diffMillis = System.currentTimeMillis()  - (birthday*1000);  // Calculate Age
                        //log("C:"+diffMillis);
                        DatatypeFactory datatypeFactory = null;
						try {datatypeFactory = DatatypeFactory.newInstance();} catch (DatatypeConfigurationException e) {log("ERROR DATATYPEFACT");}
                        Duration duration = datatypeFactory.newDuration(diffMillis);
                        age=duration.getYears();
                        if (age>50) {age=50;}
                        if (age<13) {age=13;}
                        
                        // k.kooper_array[1][1][1]; // gender, age, level --> distance         // Get Result from CooperTest_Table
                        for (int x=5;x>1;x--) {
                        	//log ("KOOPER:"+k.kooper_array[gender][age][x]+" < DIST:"+dist+"?");
                        	if (k.kooper_array[gender][age][x]<dist) {level=x-1;} 
                         }
                        
                        String level_st="";
                        if (level==1) {level_st="Sehr gut";}
                        if (level==2) {level_st="Gut";}
                        if (level==3) {level_st="Durchschnitt";}
                        if (level==4) {level_st="Schlecht";}
                        if (level==5) {level_st="Sehr schlecht";}
                        
                	    send_feedback("Koopertest abgeschlossen, Level "+level_st,1);
                	    sql="update session set v6="+level+" where session_id="+session;
                    	res=db.sql(sql,1);
                	    
			    	    stop_timer();
			    	    close_ai();
			        } else {
			    	   if (time60>=60) 
			    	       {time60=0;minutes_left--;send_feedback("Noch "+minutes_left+" Minuten",0);}
			    	   
			    	   sql="select v1 from session where session_id="+session;   // GET ACTUAL HR
                       res=db.sql(sql,0);
                       if (res.first()) {hr=res.getInt(1);}
                       
                       if (hr>max_hr*0.9) {
                    	   send_feedback++;
                    	   if (send_feedback>15) {send_feedback("Achtung, Herzfrequenz zu hoch!",0);send_feedback=0;}
                       } else {send_feedback=0;}
                       
			        }
                  
	        	} catch (SQLException e) {}
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
	
}
