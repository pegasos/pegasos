//CALCULATION OF RAI

// (Schrittanzahl(Person) / Zeit(PersonimSpiel))  /   (Mittel (Schrittanzahl(Person) / Zeit(PersonimSpiel)) )  

// CALCULATION OF RANKING




// CLUB START --> Check for Sessions with same team and group--> Feedbackmeldung in welchem Team man mitmacht 

// PRIVATE START --> Nachsehen ob in einem befreundeten Team schon eine Session läuft - wenn ja, dann AI-Modul abdrehen und Feedback über Name des Session-Halters,
// Andernfalls selbst AI-Modul initieren. Teilnehmen können dann alle Freunde auf der eigenen Liste. Ebenfalls Feedbackmeldung!


// Beenden der Session --> bis keiner mehr läuft (last update von ALLEN>5 sekunden!!)



package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;
import lib.LinearRegression;

public class BeatMyActivity_v2  extends AI_module {

// Parameters
	private static int timer_interval=5000;              // check hr every ... ms
	private static int feedback_intervall=180/(timer_interval/1000);  // wie oft kommt feedback zur position ?
	//private static int feedback_intervall=10/(timer_interval/1000);  // wie oft kommt feedback zur position ?
	private static int groups=3;                // anzahl gruppenplatzierungen (wenn wenige Teilnehmer als Gruppen --> Jeder Teilnehmer ist eine Gruppe)  
	
// vars for calculating linear regression
	double  steigung=0.000,y_start=0.000,korr=0.000;
	
	
// futher vars 
	private int now=0;
	private ResultSet res=null,res1=null;
	private long session,user_id,team_id,group_id,lang;
	private int started=0,x=0;
	private String msg="";
	
	
	private double[][] user_index;  
	private String[] feedback;
	private String team,group;
	
	
	public BeatMyActivity_v2(Parameters p) {
		super(p);
		this.session= p.session;
		this.lang= p.lang;
		this.group_id= p.param2;
		
		user_index = new double[100][14];
		feedback   = new String[100]; 
				
		for (x=0;x<100;x++) {
			feedback[x]="";
			
			user_index[x][0]=0;  // user_id
			user_index[x][1]=0;  // session_id
			user_index[x][2]=0;  // vo2max
			user_index[x][3]=0;  // max_hr
			user_index[x][4]=0;  // step_minus
			user_index[x][5]=0;  // time>max_hr
			user_index[x][6]=0;  // steps in this interval
			user_index[x][7]=0;  // all steps till now
			user_index[x][8]=0;  // time<min_hr
			user_index[x][9]=0;  // points
			user_index[x][10]=0;  // platzierung
			user_index[x][11]=0;  // time in game
			user_index[x][12]=0;  // RAI
			user_index[x][13]=0;  // (Schrittanzahl / Zeit im Spiel) * 10000
			
			
			/* TEST
			user_index[x][0]=x;
			user_index[x][9]=x;
			*/
			//user_index[x][10]=0;
		}
		
		// TEST
		/*
		  user_index[99][0]=0;
		  started=99;
		  bubblesort();
		  for (x=0;x<started;x++) {System.out.println("USER:"+user_index[x][0]+" PUNKTE:"+user_index[x][9]+" PLATZ:"+user_index[x][10]);}
		*/
		// END TEST
		
		
		// Get user_id who actually started session
	
		
		if (session!=0) {
		   try {
  			   int tmp=0;
			   res=db.sql("select userid,teamid,groupid from session where session_id="+session,0);
			   if (res.next()) {user_id=res.getLong(1);team_id=res.getLong(2);group_id=res.getLong(3);}
		       log("Beat My Activity for user "+user_id+" started!");
			   //if (res.next()) {user_id=res.getLong(1);team_id=res.getLong(2);group_id=res.getLong(3);}
			
			   res=db.sql("select name from teams where id="+team_id,0); if (res.next()) {team=res.getString(1);}
			   res=db.sql("select name from teams_groups where id="+group_id,0); if (res.next()) {group=res.getString(1);}
			   group=group.replace("'","");
	    	   team=team.replace("'","");		    
		    
		    	if (lang==0) {msg="Bitte warte auf das Startsignal";} else {msg="Please wait for the start signal";}
		    	db.sql("insert into feedback (session_id,message) values ("+session+",'"+msg+"')",1);
		    	close_ai();
		} catch (SQLException e) {}
	  } else {
		 start_timer(); 
	  }
		    
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,timer_interval); 
    }
	
    private TimerTask task = new TimerTask() {
        public void run() {
        	
        	try {
        		 // TODO: DO NOT WAIT FOR EVER!!!!!!
        	     if (started==0) {
        		 // Start feedback synchroniously for all users!
							res=db.sql("select count(session_id) from session where activity=8 and param1=1 and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
							res.next();
							int count=res.getInt(1);
							res=db.sql("select session_id,userid,v5 from session where activity=8 and param1=1 and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
							x=0;
							
							while (res.next()) {
                                   started++;
                                   session=res.getLong(1);
								   user_id=res.getLong(2);
								   user_index[x][0]=user_id;  // user_id
								   log("Start for User_id:"+user_id);
								   user_index[x][1]=session;  // session_id
								   res1=db.sql("select data1,data2 from user where id="+user_id,0);res1.next();
								   if (res1.getInt(1)>00) {user_index[x][2]=res1.getInt(1);} else {user_index[x][2]=30;}  // vo2max
								   
								   if (res1.getInt(2)>60) {user_index[x][3]=res1.getInt(2);} else {user_index[x][3]=200;}  // max_hr
								   user_index[x][4]=res.getLong(3); // step_minus
								   
								   if (lang==0) {msg="Los gehts!";} else {msg="Go!";}
								   db.sql("insert into feedback (session_id,message) values ("+session+",'"+msg+"')",1);
								   db.sql("insert into feedback (session_id,message) values ("+session+",'10')",1);
								   
								   //db.sql("update session set v10=1,v6="+user_index[x][2]+",v7="+user_index[x][3]+",v9="+count+",starttime="+System.currentTimeMillis()+",last_update="+System.currentTimeMillis()+" where session_id="+session,1);
								   db.sql("update session set v10=1,v6="+user_index[x][2]+",v7="+user_index[x][3]+",v9="+groups+",starttime="+System.currentTimeMillis()+",last_update="+System.currentTimeMillis()+" where session_id="+session,1);
								   x++;
							}
        	     }
        	     else
        	         {    // started=1
        	    	 // GET HR EVERY 5 SECONDS
        	    	      x=0;

        	    	 if (now>=feedback_intervall) {
        	    		 x=0;
        	    		 double avg=0;
        	    		 int persons_playing=0;
        	    		 
        	    		 // Calculate (Schrittanzahl/Zeit im Spiel in sec) x 10 000 for eahc user
        	    		 while (user_index[x][0]>0) {
         	    		      res=db.sql("select v5,last_update-starttime from session where session_id="+user_index[x][1],0);res.next();  // v5=steps
         	    		      user_index[x][13]=(res.getLong(1)-user_index[x][4]) / (res.getLong(2)/1000.000);  // (Schrittanzahl/Zeit im Spiel in sec)          	    		      avg+=user_index[x][13];
         	    		      persons_playing++;
         	    		      avg+=user_index[x][13];
         	    		      x++;
        	    		 }    
        	    		 
        	    		 avg=avg/persons_playing;
        	    		 log("AVG"+avg);
        	    		 
        	    		 // calculate RAI for each user
        	    		 x=0;
        	    		 while (user_index[x][0]>0) { 
        	    			 user_index[x][12]=user_index[x][13]/avg;
        	    			 log("USER "+x+" SCHRITTE/ZEIT:"+user_index[x][13]+" RAI:"+user_index[x][12]+" Vo2MAX :"+user_index[x][2]);
        	    		     x++;
       	    		     }    
        	    		 
        	    		 
        	    		 // calculate linear regression based on x=vo2max (x[2]) and y=RAI x[12]
        	    		      double[] x_vo2max = new double[persons_playing];
        	    		      double[] y_RAI = new double[persons_playing];
        	    		      for (int x=0;x<persons_playing;x++) {x_vo2max[x]=user_index[x][2];y_RAI[x]=user_index[x][12];}
        	    		      
        	    		     // double[] x_vo2max = { 2, 3, 4, 5, 6, 8, 10, 11 };
        	    		     // double[] y_RAI = { 21.05, 23.51, 24.23, 27.71, 30.86, 45.85, 52.12, 55.98 };
        	    		      
        	    		      LinearRegression linearregression = new LinearRegression(x_vo2max,y_RAI);
        	    		 
        	    		      y_start = linearregression.intercept();
        	    		      steigung = linearregression.slope();
       	    			      korr = linearregression.R2();
       	    		            
        	    		 
       	    			      log ("REGRESSION STEIGUNG:"+steigung+" y_start:"+y_start+" KORR:"+korr);
         	    		     
         	    		      
         	    		 // PUNKTE BERECHNEN!!! = TATSÄCHLICHE RAI - BERECHNETE RAI  
         	    		      //user_index[x][9]=Math.round(((1.00000/user_index[x][2])*(res.getLong(1)-user_index[x][4]))*10); // points= (1/vo2max)*steps
         	    		x=0;
       	    		    while (user_index[x][0]>0) {
         	    		      user_index[x][9]=(user_index[x][12] - (y_start+steigung*user_index[x][2]))*1000.000;
         	    		      log ("punkte=("+user_index[x][12]+"-"+(y_start+steigung*user_index[x][2])+")*1000.000="+user_index[x][9]);
         	    		      x++;
       	    		    } 
  						  
 						 bubblesort();
        	    	     now=0;
        	    	 }
        	    	 
        	    	// SEND FEEDBACK
        	    	 // TODO: INSERT V8=actual level, 
						 x=0;
						 while (user_index[x][0]>0) {
							 //log("SEND FEEDBACK:"+x);
							 if (!feedback[x].isEmpty()) {
								  String tmp1="5";
								  String tmp2="18";
								  if (user_index[x][10]==1) {tmp1="5";tmp2="18";}  // GRÜN
								  if (user_index[x][10]==2) {tmp1="7";tmp2="19";}  // GELB
								  if (user_index[x][10]==3) {tmp1="6";tmp2="20";}  // ROT
								  res=db.sql("insert into feedback (session_id,message) values ("+user_index[x][1]+",'"+tmp1+"')",1);
								  res=db.sql("insert into feedback(session_id,message) values ("+user_index[x][1]+",'"+feedback[x]+"')",1);
								  res=db.sql("insert into feedback (session_id,message) values ("+user_index[x][1]+",'"+tmp2+"')",1);
								  
								  db.sql("update session set v8="+user_index[x][10]+" where session_id="+user_index[x][1],1);
							      //log ("insert into feedback(session_id,message) values ("+user_index[x][1]+",'"+feedback[x]+"')");
							 }
	        	    		 feedback[x]="";
							 x++;
						 }
        	    	 now++;
        	     } // END STARTED = 1
        	     
        	    // Alive?! --> solange alive bis keiner mehr läuft!!
        	       res=db.sql("select last_update from session where activity=8 and param1=1 and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-5000),0);
        	       if (!res.next()) {log("Beat my Activity for Team "+team+" Gruppe "+group+" beendet!");stop_timer();close_ai();}
        		  
        	     
        	     
        	} catch (SQLException e) {
			}
        	
        	// Kill session after last user has stopped session!?
        	//if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
        	
           
           // 
        	
        
    } // end task  	
 };
    
    
   
 private double sqr (double x) {
    return x * x;                            /* compute square of argument    */
 } 
 
   
private void stop_timer() {
      try {timer.cancel();} catch (Exception e) {}
}



public void bubblesort() {

	int grp=1,group_size=0;
	if (started<groups) {groups=started;} // wenn weniger teilnehmer als gruppen, dann ist jeder Teilnehmer eine Gruppe
	group_size=(int) Math.floor(started/groups);  // anzahl teilnehmer in einer gruppe
	log ("Group_size:"+group_size);
	
// 1. Sortieren nach Punkten
	double[] temp;
	for(int i=1; i<started; i++) {
		for(int j=0; j<started-i; j++) {
			if(user_index[j][9]<user_index[j+1][9]) {
				temp=user_index[j];
				user_index[j]=user_index[j+1];
				user_index[j+1]=temp;
			}
		}
	}
 
// 2. Gruppieren lt. Anzahl Gruppen, Platz in Array schreiben
	x=0;
	while (user_index[x][0]>0) {
		if (((grp*group_size)<(x+1)) && (grp<groups)) {grp++;}
		user_index[x][10]=grp;
		if (lang==0) {msg="Du bist derzeit auf Rang "+grp;} else {msg="You are currently in Group "+grp;}
		feedback[x]+=msg;
		x++;
	}
 
	// 3. 10%Regel (Gruppe x--> wenn Leistung*1.1> Leistung Bester von Gruppe x-1 --> eine Gruppe höher) ??
	// 4. wenn 50% der Intervalle mit steigerung --> 1 Rang höher???
	
	
	
	
}

	
}





