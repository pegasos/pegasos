package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import activities.*;
import at.univie.mma.ai.Parameters;

/* 
VO2max= 
132,853 - (0,0769 x kg KG x 2,2) - (0,3877 x Alter) + (6,315 x Geschlecht) - (3,2649 x Zeit) - (0,1565 x HF) 
Geschlecht: m = 1; w = 0
-> aus Originalartikel von Kline et al. 1987



~	Ziel: Beurteilung der aeroben Leistung (VO2 max)
~	Geräte: flache 1600 m (1 mile) Strecke
~	Stoppuhr
~	Pulsuhr (optional)
~	Statistik: Validität: r = 0.88 (Kline et al. 1987)
~	 Test-retest Reliabilität: r = 0.98 (Kline et al. 1987)

Der Proband muss die 1600 m so schnell wie moeglich gehend, mit forciertem Armeinsatz zuruecklegen.
Die Zeit für die 1600 m wird gemessen. Gleich nach Beendigung des Test wird die Herzfrequenz gemessen.

*/

public class RockportWalkingFitnessTest extends AI_module {

	private final long session;
	private final int lang;
	private ResultSet res=null;
	private int dist100,actualdist=0,round=1,hr=0,user_id,weight,gender,age;
	private long birthday,start;
	private double vo2max=0;
	
	
	public RockportWalkingFitnessTest(Parameters p) {
		super(p);
		this.session= p.session;
		this.lang= p.lang;
	    log("RockportWalkingFitnessTest started!");
	    if (lang==0) {send_feedback("Rockport Walking Test wurde gestartet.",0);} else {send_feedback("Rockport Walking Test started.",0);}
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
		start_timer();
		start=System.currentTimeMillis();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
	        	
	        	try {
	        		sql="select v3,v1,userid from session where session_id="+session;   // GET ACTUAL DIST
                    res=db.sql(sql,0);
                    if (res.first()) {actualdist=res.getInt(1);hr=res.getInt(2);user_id=res.getInt(3);}
                    //log("D:"+actualdist);
                    if (actualdist>=1600) {
                   // if (actualdist>=200) {
                    	  
                          sql="select birthday,weight,gender from user where id="+user_id;   // GET USER_ID
                          res=db.sql(sql,0);
                          if (res.first()) {birthday=res.getLong(1);weight=res.getInt(2);gender=res.getInt(3);}
                          
                          long diffMillis = System.currentTimeMillis()  - (birthday*1000);  // Calculate Age
                          DatatypeFactory datatypeFactory = null;
  						  try {datatypeFactory = DatatypeFactory.newInstance();} catch (DatatypeConfigurationException e) {log("ERROR DATATYPEFACT");}
                          Duration duration = datatypeFactory.newDuration(diffMillis);
                          age=duration.getYears();
                          
                          vo2max=132.853-(0.0769*weight*2.2)-(0.3877*age) + (6.315*gender)-(3.2649*((System.currentTimeMillis()-start)/1000/60))-(0.1565*hr);
                          //log ("weight:"+weight);
                          //log ("age:"+age);
                          //log ("gender:"+gender);
                          //log ("time:"+((System.currentTimeMillis()-start)/1000));
                          //log ("hr:"+hr);
                          if (lang==0) {send_feedback("Rockport Walking Test abgeschlossen, VO2max ist "+(long)vo2max,1);} else {send_feedback("Rockport Walking Test finished, Vo2max="+(long)vo2max,1);}
                          
                          
                          res=db.sql("update session set v6="+(long)vo2max+" where session_id="+session,1); // set VO2max
                          res=db.sql("update user set data1="+(long)vo2max+" where id="+user_id,1); // set VO2max
                    	  
                          log("Killed AI-Session!");stop_timer();close_ai();
                          
                    } else 
                    if ((actualdist-dist100)>=100) 
                    {
                     dist100+=100;
                     if (lang==0) {send_feedback("Noch "+(1600-round*100)+" Meter",0);} else {send_feedback((1600-round*100)+" Meters left",0);}
                     
                     round++;
                     }
                    
                    
	        	} catch (SQLException e) {}
	        	
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
	
}

