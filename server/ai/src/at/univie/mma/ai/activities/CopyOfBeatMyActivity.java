// TODO: DIFFER USERS GROUPS: PRIVATE SESSION AND CLUB SESSION!!!

// CLUB START --> Check for Sessions with same team and group--> Feedbackmeldung in welchem Team man mitmacht 

// PRIVATE START --> Nachsehen ob in einem befreundeten Team schon eine Session läuft - wenn ja, dann AI-Modul abdrehen und Feedback über Name des Session-Halters,
// Andernfalls selbst AI-Modul initieren. Teilnehmen können dann alle Freunde auf der eigenen Liste. Ebenfalls Feedbackmeldung!


// Beenden der Session --> bis keiner mehr läuft (last update von ALLEN>5 sekunden!!)


package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;

public class CopyOfBeatMyActivity  extends AI_module {

// Parameters
	private static int timer_interval=5000;              // check hr every ... ms
	private static int step_start=20;           // ab wie vielen schritten wird gestartet? 
	private static int feedback_intervall=180/(timer_interval/1000);  // wie oft kommt feedback zur position ?
	//private static int feedback_intervall=20/(timer_interval/1000);  // wie oft kommt feedback zur position ?
	private static int hr_message=60/(timer_interval/1000);         // wann wird meldung über zu hohe herzfrequenz ausgegeben?
	private static int max_hr_percent=90;       // bis zu welcher hr-belastung darf gelaufen werden?
	private static int min_hr_percent=50;       // welche hr-belastung muss mind. gelaufen werden?	
	private static int groups=3;                // anzahl gruppenplatzierungen (wenn wenige Teilnehmer als Gruppen --> Jeder Teilnehmer ist eine Gruppe)  
	
	 
// futher vars 
	private int now=0;
	private ResultSet res=null,res1=null;
	private long session,user_id,team_id,group_id,lang;
	private int started=0,x=0;
	private String msg="";
	
	
	private long[][] user_index;  
	private String[] feedback;
	private String team,group;
	
	
	public CopyOfBeatMyActivity(Parameters p) {
		super(p);
		this.session= p.session;
		this.lang= p.lang;
		
		user_index = new long[100][11];
		feedback   = new String[100]; 
				
		for (x=0;x<100;x++) {
			feedback[x]="";
			
			user_index[x][0]=0;  // user_id
			user_index[x][1]=0;  // session_id
			user_index[x][2]=0;  // vo2max
			user_index[x][3]=0;  // max_hr
			user_index[x][4]=0;  // step_minus
			user_index[x][5]=0;  // time>max_hr
			user_index[x][6]=0;  // steps in this interval
			user_index[x][7]=0;  // all steps till now
			user_index[x][8]=0;  // time<min_hr
			user_index[x][9]=0;  // points
			user_index[x][10]=0;  // platzierung
			
			/* TEST
			user_index[x][0]=x;
			user_index[x][9]=x;
			*/
			//user_index[x][10]=0;
		}
		
		// TEST
		/*
		  user_index[99][0]=0;
		  started=99;
		  bubblesort();
		  for (x=0;x<started;x++) {System.out.println("USER:"+user_index[x][0]+" PUNKTE:"+user_index[x][9]+" PLATZ:"+user_index[x][10]);}
		*/
		// END TEST
		
		
		// Get user_id who actually started session
	
		try {
			int tmp=0;
			res=db.sql("select userid,teamid,groupid from session where session_id="+session,0);
			if (res.next()) {user_id=res.getLong(1);team_id=res.getLong(2);group_id=res.getLong(3);}
		    log("Beat My Activity for user "+user_id+" started!");
			//if (res.next()) {user_id=res.getLong(1);team_id=res.getLong(2);group_id=res.getLong(3);}
			
			res=db.sql("select name from teams where id="+team_id,0); if (res.next()) {team=res.getString(1);}
			res=db.sql("select name from teams_groups where id="+group_id,0); if (res.next()) {group=res.getString(1);}
			
			
			group=group.replace("'","");
			
	    	team=team.replace("'","");		    
		    
		    // check for further users which currently have started a session in same group and team
	        res=db.sql("select count(*) from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
	        //System.out.println("select count(*) from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-2000));
		    if (res.next()) {tmp=res.getInt(1);}
		    if (tmp>1) {
		    	log("--> AI Module has been started already ("+tmp+" active users in group)");
		    	if (lang==0) {msg="Bitte warte mit dem Start bis alle Teilnehmer der Gruppe "+group+" bereit sind";} else {msg="Please wait till the other members of the group "+group+" are ready to start";}
		    	db.sql("insert into feedback (session_id,message) values ("+session+",'"+msg+"')",1);
		    	close_ai();
		    } else {
		    	log("--> First user call, waiting for further users. Module starts on first user reaching 25 Steps"); start_timer();
		    	if (lang==0) {msg="Bitte warte mit dem Start bis alle Teilnehmer der Gruppe "+group+" bereit sind";} else {msg="Please wait till the other members of the group "+group+" are ready to start";}
		    	db.sql("insert into feedback (session_id,message) values ("+session+",'"+msg+"')",1);
		    }
		    log("insert into feedback (session_id,message) values ("+session+",'Bitte warte mit dem Start bis alle Teilnehmer der Gruppe "+group+" bereit sind')");
		    
		} catch (SQLException e) {}
		    
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,timer_interval); 
    }
	
    private TimerTask task = new TimerTask() {
        public void run() {
        	
        	try {
        		 // TODO: DO NOT WAIT FOR EVER!!!!!!
        	     if (started==0) {
        		   //  Wait for first user reaching step_start Steps
        		    res=db.sql("select max(v5) from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
        		    //log("select max(v5) from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-2000));
					if (res.next()){
						log("STEPS:"+res.getInt(1)+" STEP START:"+step_start);
						if (res.getInt(1)>=step_start){ // Start feedback synchroniously for all users!
							res=db.sql("select count(session_id) from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
							res.next();
							int count=res.getInt(1);
							res=db.sql("select session_id,userid,v5 from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-4000),0);
							x=0;
							
							while (res.next()) {
                                   started++;
                                   session=res.getLong(1);
								   user_id=res.getLong(2);
								   user_index[x][0]=user_id;  // user_id
								   log("Start for User_id:"+user_id);
								   user_index[x][1]=session;  // session_id
								   res1=db.sql("select data1,data2 from user where id="+user_id,0);res1.next();
								   if (res1.getInt(1)>00) {user_index[x][2]=res1.getInt(1);} else {user_index[x][2]=30;}  // vo2max
								   
								   if (res1.getInt(2)>60) {user_index[x][3]=res1.getInt(2);} else {user_index[x][3]=200;}  // max_hr
								   user_index[x][4]=res.getLong(3); // step_minus
								   
								   if (lang==0) {msg="Los gehts!";} else {msg="Go!";}
								   db.sql("insert into feedback (session_id,message) values ("+session+",'"+msg+"')",1);
								   db.sql("insert into feedback (session_id,message) values ("+session+",'10')",1);
								   
								   //db.sql("update session set v10=1,v6="+user_index[x][2]+",v7="+user_index[x][3]+",v9="+count+",starttime="+System.currentTimeMillis()+",last_update="+System.currentTimeMillis()+" where session_id="+session,1);
								   db.sql("update session set v10=1,v6="+user_index[x][2]+",v7="+user_index[x][3]+",v9="+groups+",starttime="+System.currentTimeMillis()+",last_update="+System.currentTimeMillis()+" where session_id="+session,1);
								   x++;
							}
							
							
							
							
						}
					}
        	     }
        	     else
        	         {    // started=1
        	    	 // GET HR EVERY 5 SECONDS
        	    	      x=0;
        	    	      /*
        	    	      while (user_index[x][0]>0) {
        	    	    	  
      	    		         res=db.sql("select v1 from session where session_id="+user_index[x][1],0);res.next();
      	    		         
      	    		         if (res.getLong(1)>(user_index[x][3]*(max_hr_percent/100.00))) {user_index[x][5]++;} else {user_index[x][5]=0;}  // max hr reached? time>max_hr?
      	    		         if (res.getLong(1)<(user_index[x][3]*(min_hr_percent/100.00))) {user_index[x][8]++;} else {user_index[x][8]=0;}  // hr<min_hr? 

        	    		     // Feedback HR
      	    		         if (user_index[x][5]>=hr_message) {feedback[x]="Herzfrequenz zu hoch!";user_index[x][5]=0;} else 
      	    		         if (user_index[x][8]>=hr_message) {feedback[x]="Das kannst du besser!";user_index[x][8]=0;} 
      	    		         x++;	 
      	    		      }
      	    		      */


        	    	 if (now>=feedback_intervall) {
        	    		 x=0;
        	    		 while (user_index[x][0]>0) {
         	    		      res=db.sql("select v5,last_update-starttime from session where session_id="+user_index[x][1],0);res.next();
         	    		      int t1=0,t2=0;
         	    		      
         	    		   // Anzahl Schritte diesmal höher als im letzten Intervall   
         	    		     if (user_index[x][6]<(res.getLong(1)-user_index[x][4]-user_index[x][7])) {t1=1;} 
         	    		     //log ("i[6]:"+user_index[x][6]+"<"+(res.getLong(1)-user_index[x][4]-user_index[x][7])+"?-->"+t1);
         	    		     
         	    		   // Aktueller Durchschnitt besser als Gesamtdurchschnitt (steps/minute)
         	    		     double multi=60/(feedback_intervall*(timer_interval/1000));
         	    		     if (((res.getLong(1)-user_index[x][4]-user_index[x][7])*multi) > ((res.getLong(1)-user_index[x][4])/(res.getLong(2)/1000/60.00))  ) {t2=1;}
         	    		     log ("aktueller schnitt:"+(res.getLong(1)-user_index[x][4]-user_index[x][7])*multi);
         	    		     log ("gesamtschnitt:"+((res.getLong(1)-user_index[x][4])/(res.getLong(2)/1000/60.00)));
         	    		     
         	    		     
         	    		     //log ("Schritte bis jetzt:"+(res.getLong(1)-user_index[x][4]));
         	    		     //log ("Zeit bis jetzt in min:"+(res.getLong(2)/1000/60.00));
         	    		     //log ("Durchschnitt/Minute gesamt:"+((res.getLong(1)-user_index[x][4])/(res.getLong(2)/1000/60.00)));
         	    		     
         	    		     // log ("i[6]/60.00xmulti:"+(user_index[x][6]/(60.00*multi))+">"+((res.getLong(1)-user_index[x][4])/(res.getLong(2)/1000/60.00))+"?-->"+t2);
         	    		      
         	    		   // PUNKTE BERECHNEN!!!
         	    		      user_index[x][9]=Math.round(((1.00000/user_index[x][2])*(res.getLong(1)-user_index[x][4]))*10); // points= (1/vo2max)*steps
         	    		      
         	    		      
         	    		      log ("punkte:"+user_index[x][9]);
         	    		      log ("t1:"+t1);
         	    		      log ("t2:"+t2);
         	    		      
         	    		      // FEEDBACK ANZAHL SCHRITTE 2.Priorität (nur wenn HR weder zu niedrig noch zu hoch!)
         	    		 //     if ((t1==1) && (t2==1) && (feedback[x].isEmpty())) {feedback[x]="Hervorragende Leistung!";} else  // Anzahl Schritte aktuelles Intervall besser als letztes Intervall, sowie besser als Durchschnitt über Gesamtlauf!
         	    		 //     if ((t2==1) && (feedback[x].isEmpty())) {feedback[x]="Tolle Leistung, weiter so!";} else  // Anzahl Schritte aktuelles Intervall besser als Durchschnitt über Gesamtlauf!
         	    		 //      if ((t1==1) && (feedback[x].isEmpty())) {feedback[x]="Gut gemacht, du steigerst dich!";}  // Anzahl Schritte aktuelles Intervall besser als letztes Intervall!
         	    		      
         	    		      user_index[x][6]=res.getLong(1)-user_index[x][4]-user_index[x][7];   // steps in this intervall
         	    		      user_index[x][7]=res.getLong(1)-user_index[x][4];   // all steps till now
         	    		      log("steps this intervall:"+user_index[x][6]);
         	    		      log("steps till now:"+user_index[x][7]);
         	    		      x++;	 
         	    		}
        	    		 
  						 // PLATZIERUNG 
 						 bubblesort();
        	    	     now=0;
        	    	 }
        	    	 
        	    	// SEND FEEDBACK
        	    	 // TODO: INSERT V8=actual level, 
						 x=0;
						 while (user_index[x][0]>0) {
							 //log("SEND FEEDBACK:"+x);
							 if (!feedback[x].isEmpty()) {
								  String tmp1="5";
								  String tmp2="18";
								  if (user_index[x][10]==1) {tmp1="5";tmp2="18";}  // GRÜN
								  if (user_index[x][10]==2) {tmp1="7";tmp2="19";}  // GELB
								  if (user_index[x][10]==3) {tmp1="6";tmp2="20";}  // ROT
								  res=db.sql("insert into feedback (session_id,message) values ("+user_index[x][1]+",'"+tmp1+"')",1);
								  res=db.sql("insert into feedback(session_id,message) values ("+user_index[x][1]+",'"+feedback[x]+"')",1);
								  res=db.sql("insert into feedback (session_id,message) values ("+user_index[x][1]+",'"+tmp2+"')",1);
								  
								  db.sql("update session set v8="+user_index[x][10]+" where session_id="+user_index[x][1],1);
							      //log ("insert into feedback(session_id,message) values ("+user_index[x][1]+",'"+feedback[x]+"')");
							 }
	        	    		 feedback[x]="";
							 x++;
						 }
        	    	 now++;
        	     }
        	     
        	    // Alive?! --> solange alive bis keiner mehr läuft!!
        	       res=db.sql("select last_update from session where activity=8 and param1=1 and teamid="+team_id+" and groupid="+group_id+" and last_update>"+(System.currentTimeMillis()-5000),0);
        	       if (!res.next()) {log("Beat my Activity for Team "+team+" Gruppe "+group+" beendet!");stop_timer();close_ai();}
        		  
        	     
        	     
        	} catch (SQLException e) {
			}
        	
        	// Kill session after last user has stopped session!?
        	//if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
        	
           
           // 
        	
        
    } // end task  	
 };
    
    
   
   
private void stop_timer() {
      try {timer.cancel();} catch (Exception e) {}
}



public void bubblesort() {

	int grp=1,group_size=0;
	if (started<groups) {groups=started;} // wenn weniger teilnehmer als gruppen, dann ist jeder Teilnehmer eine Gruppe
	group_size=(int) Math.floor(started/groups);  // anzahl teilnehmer in einer gruppe
	log ("Group_size:"+group_size);
	
// 1. Sortieren nach Punkten
	long[] temp;
	for(int i=1; i<started; i++) {
		for(int j=0; j<started-i; j++) {
			if(user_index[j][9]<user_index[j+1][9]) {
				temp=user_index[j];
				user_index[j]=user_index[j+1];
				user_index[j+1]=temp;
			}
		}
	}
 
// 2. Gruppieren lt. Anzahl Gruppen, Platz in Array schreiben
	x=0;
	while (user_index[x][0]>0) {
		if (((grp*group_size)<(x+1)) && (grp<groups)) {grp++;}
		user_index[x][10]=grp;
		if (lang==0) {msg="Du bist derzeit auf Rang "+grp;} else {msg="You are currently in Group "+grp;}
		feedback[x]+=msg;
		x++;
	}
 
	// 3. 10%Regel (Gruppe x--> wenn Leistung*1.1> Leistung Bester von Gruppe x-1 --> eine Gruppe höher) ??
	// 4. wenn 50% der Intervalle mit steigerung --> 1 Rang höher???
	
	
	
	
}

	
}





