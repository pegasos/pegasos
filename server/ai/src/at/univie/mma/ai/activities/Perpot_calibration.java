package at.univie.mma.ai.activities;

import java.io.*;
import java.sql.*;
import java.util.*;

import activities.*;
import at.univie.mma.ai.*;

// Stufentest für Marathonlauf
// davor unbedingt footpod-kalibrierung
// Ansprechpartner für Weiterentwicklungen: Stefan Endler (Uni Mainz) -->  'Endler, Stefan' (endler@uni-mainz.de) 




public class Perpot_calibration extends AI_module {

	private int calib_time=0,calib_round=0,calib_feedback=0,calib_end_detection=0;
	private double calib_faktor,calib_start,speed;  // Steigerungsgeschwindigkeit, Startgeschwindigkeit
	private int start=0;
	private ResultSet res=null;
	private final long session;
	
	public Perpot_calibration(Parameters p) {
		super(p);
		this.session= p.session;
		if (p.param2==1) {this.calib_faktor=1;this.calib_start=6;}
		if (p.param2==2) {this.calib_faktor=1.5;this.calib_start=7;}
		if (p.param2==3) {this.calib_faktor=2;this.calib_start=8;}
	    log("PerPotCalibration Module level"+p.param2+" started!");	
		start_timer();
	}
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
  private TimerTask task = new TimerTask() {
    int km = 0;
    
    public void run()
    {
      
      if( !alive() )
      {
        log("Killed AI-Session!");
        stop_timer();
        close_ai();
      }
      
      sql = "select v4 from session where session_id=" + session; // GET ACTUAL SPEED
      // log("select v4 from session where session_id="+session);
      res = db.sql(sql, 0);
      try
      {
        if( res.first() )
        {
          speed = res.getDouble(1) * 0.0036;
          // log("SPEED:"+speed);
          if( ((speed - calib_start) > 0) && (start < 2) )
          {
            log("CALIB_START:" + (speed - calib_start));
            start++;
            if( start == 2 )
            {
              res = db.sql(
                  "update session set v10=1,starttime="
                      + System.currentTimeMillis() + " where session_id="
                      + session, 1);
              send_feedback("Kalibrierungslauf wurde gestartet!", 0);
            }
          }
        }
      }
      catch (SQLException e)
      {
      }
                
	        	if (start>=2) {
        		    calib_time++;
        		    calib_feedback++;
	    		 
	   		        if (calib_time>=180) {    // 3 minutes have passed
	    			     if (calib_round==-1) {  // Ende
	    			    	 stop_timer();
	    			    	 int ok=0; int call=1;
	    			    	 while ((ok==0) && (call<4)) {  // calculate anaerobe treshold out of perpot calibration run and stop session
	    			    		call++;
	    			    		log("try calling perpot_kalibrierung");
	    			    	    //String[] cmd = {"java", "-jar", "perpot_kalibrierung.jar",String.valueOf(session)};
	    			    		send_feedback("Lauf abgeschlossen",0);
	    			    		String[] cmd = {"java", "-jar", "perpot_kalibrierung.jar",String.valueOf(session)};
	    			    	    Process pr=null;
					            try {pr = Runtime.getRuntime().exec(cmd);} catch (IOException e) {log("Error calculating anaerobe treshold!");}
					            BufferedReader line_in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				                String line_out=null;
				        	    try {while ((line_out = line_in.readLine()) != null) {log("AI call returned with:"+line_out);ok=0;}} catch (IOException e) {}
				        	    if (line_out==null) {ok=1;log("AI call returned with:1 (calibration succesfull)");}
	    			    	 }
	    			    	 
	    			    	   
	 					      //send_feedback("Kalibrierungslauf abgeschlossen",1);  
	    			    	  close_ai();
	    			    	 
      			          }
	    			      else {
	    			          calib_time=0;calib_round++;
	    			          calib_feedback=0;
  	    	                  //   double newspeed=calib_run+calib_round*calib_faktor; 
	    			          send_feedback("Steigern auf "+(calib_start+calib_round*calib_faktor)+" km/h",0);
	    		       }
	    	      } else {
	    	    	      double tmp2;	    		   
	    		      // CALIB_END_DETECTION
	    		         if (((tmp2=speed-(calib_start+calib_round*calib_faktor))<-3.0) && (speed>0) && (calib_round>2)) {
		     	              calib_end_detection++;
		     	              calib_feedback=0;
		     	              if (calib_end_detection>=3) {
   		     	                  calib_round=-1;calib_time=0;
		     	                 //feedback("Endphase - neue Geschwindigkeit "+calib_start+" km/h.",0);
   		     	                 send_feedback("Endphase - neue Geschwindigkeit "+calib_start+" km/h.",0);
		     	              }
		     	         } else 
		     	         {
		     	    	   calib_end_detection=0;   
	                       if (calib_feedback>15) { // check speed every 15 seconds
	      	                   if (calib_round>=0) {    
	      	                        calib_feedback=0;
	      	                        if (((tmp2=speed-(calib_start+calib_round*calib_faktor))>0.30) &&  (speed>0)) {send_feedback("Langsamer um "+(Math.round((tmp2)*10)/10.0d)+" km/h",0);calib_end_detection=0;} else
	      	                        if (((tmp2=speed-(calib_start+calib_round*calib_faktor))<-0.30) && (speed>0)) {send_feedback("Schneller um "+Math.abs(Math.round((tmp2)*10)/10.0d)+" km/h",0);}
	                           } else {  // Auslaufen
	                                calib_feedback=0;
	       	                        if (((tmp2=speed-(calib_start))>0.30)  && (speed>0)) {send_feedback("Langsamer um "+(Math.round((tmp2)*10)/10.0d)+" km/h",0);} else
	       	                        if (((tmp2=speed-(calib_start))<-0.30) && (speed>0)) {send_feedback("Schneller um "+Math.abs(Math.round((tmp2)*10)/10.0d)+" km/h",0);}
	                           } // end else
	                      } // end if  
	                     } // end else 
      	           }  // end else
             } // end start
	        } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
   
   

   

}  // END CLASS
