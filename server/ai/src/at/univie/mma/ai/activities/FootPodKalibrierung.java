package at.univie.mma.ai.activities;

import java.sql.PreparedStatement;

// am handy: distanz eingeben die gelaufen wird (z.b 400 Meter)
// Session starten (also Lauf starten)
// genau bei Ziellienie stehen bleiben
// Der vom FootPod gemessene Wert wird mit dem tatsaechlichen Wert verglichen und der entsprechende Faktor in der DB erfasst. 


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;

public class FootPodKalibrierung extends AI_module {
  protected static final long TIMEOUT = 60000;
	
  private final long session;
  private long user_id;
  private int calib;
	private ResultSet res;
	// private long start;
	private float distance,fp_distance=0;
	
	PreparedStatement stmt_run;
	PreparedStatement stmt_u_data3;
	
	
	public FootPodKalibrierung(Parameters p) {
		super(p);
		this.session= p.session;
		this.distance= p.param2;
	    log("FootPodKalibrierung started! ("+distance+" m)");
	    if (p.lang==0) {send_feedback("FootPodKalibrierungslauf wurde gestartet!",0);} else {send_feedback("FootPod calibration started!",0);}
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
	    
	    try {
        stmt_run= db.createStatement("select endtime,last_update,userid from session where session_id=" + session);
        stmt_u_data3= db.createStatement("UPDATE user SET data3 = ?, data3_crtime = ? WHERE id = ?");
        
        start_timer();
        // start=System.currentTimeMillis();
      } catch (SQLException e) {
        // TODO report error
        e.printStackTrace();
      }
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
  private TimerTask task = new TimerTask() {
    public void run() {

      boolean finished = false;
      try {
        res= stmt_run.executeQuery();
        if(res.next())
        {
          long t= res.getLong(1);
          if( t != 0 ) // session has stopped
            finished= true;
          else
          {
            t = res.getLong(2);
            if((System.currentTimeMillis() - t) < TIMEOUT)
              finished= false;
            else
              finished= true;
          }
        }
      }
      catch (SQLException e) {
      }

	        	
	        	
      if(finished)
      {
        log("Kalibrierungslauf beendet, kill AI-Session and store calibration faktor!");
        
        try
        {
          sql= "select v3 from session where session_id=" + session; // GET ACTUAL DIST,hr,speed
          res= db.sql(sql, 0);
          calib= 1000;
          if( res.first() )
          {
            fp_distance= res.getInt(1);
            calib= (int) Math.round((distance / fp_distance) * 1000.00);
          }
          // log("FP_DIST:"+fp_distance);
          // log("DIST REAL:"+distance);
          // log("CALIB:"+calib);
          
          res= db.sql("select userid from session where session_id=" + session, 0);
          if( res.first() && calib > 0 )
          {
            user_id= res.getInt(1);
            
            stmt_u_data3.setInt(1, calib);
            stmt_u_data3.setLong(2, System.currentTimeMillis() / 1000);
            stmt_u_data3.setLong(3, user_id);
            stmt_u_data3.executeUpdate();
            
            log("Kalib-Faktor: " + calib + " for user:" + user_id);
          }
          else
          {
            // TODO: report error
            log("Calculating FP-calib failed");
          }
          db.sql("update session set v6=" + calib + " where session_id=" + session, 1);
          
        }
        catch( SQLException e )
        {
        }
        
        stop_timer();
        close_ai();
      }
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
	
}
