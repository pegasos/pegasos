package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;

public class CoconiTest extends AI_module {   // bestimmt max_hr!!

	private long session, user_id;
	private int dist_last=0,dist=0,hr_max=0,hr=0,start=0,round=0,feedback=0,feedback_end=0;
	private double actualspeed,shallspeed=6;
	private ResultSet res=null;
	
	public CoconiTest(Parameters p) {
		super(p);
		this.session= p.session;
	    log("CoconiTest started!");
	    
	    res=db.sql("update session set last_update="+System.currentTimeMillis()+" where session_id="+session,1);
        send_feedback("Coconitest startet bei erreichen von "+shallspeed+"km/h",0);
        start_timer();
	}
	
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
	        	
	        	
	        	sql="select v4 from session where session_id="+session;   // GET ACTUAL SPEED
                res=db.sql(sql,0);
                try {
    			      if (res.first()) {
				      actualspeed=res.getDouble(1)*0.0036;
				      if ((actualspeed>shallspeed) && (start<3)) {
				    	   start++;
				    	   if (start==3) {
				    	      res=db.sql("update session set v10=1,starttime="+System.currentTimeMillis()+" where session_id="+session,1);
				    	      send_feedback("Los gehts!",0);
				    	   }   
				      }
				    }     
			    } catch (SQLException e) {}
	        	
	        	
	        	
                if (start>=3) {
	        	     try {
	        		      sql="select v1,v3,v4,userid from session where session_id="+session;        // GET HR, Distance,Speed, User_id
	         	          res=db.sql(sql,0);
	                      if (res.first()) {hr=res.getInt(1);dist=res.getInt(2);actualspeed=res.getDouble(3)*0.0036;user_id=res.getInt(4);}
	                      if (hr>hr_max) {hr_max=hr;}
	                      //log("DIST:"+dist);
	                      //log("LAST_DIST:"+dist_last);
	                      
	                      if ((dist-dist_last)>=200) {log("NEXT ROUND!");dist_last+=200;round++;send_feedback("next!",0);}
	                      
	                      //log("SHALL:"+(shallspeed+round*0.5)+" ACTUAL:"+actualspeed);
	                      
	                      if ((shallspeed+round*0.5) > actualspeed+0.3) {feedback++;if (feedback>5) {send_feedback("schneller!",0);feedback=0;}} else 
	                      if ((shallspeed+round*0.5) < actualspeed-0.3) {feedback++;if (feedback>5) {send_feedback("langsamer!",0);feedback=0;}} else 
	                      {feedback=0;}
	                      
	                      if ((shallspeed+round*0.5) > actualspeed+3) {
	                    	    feedback_end++;
	                    	    if (feedback_end>5) {
	                    	         send_feedback("Coconitest abgeschlossen!",1);stop_timer();close_ai();}
	                    	         res=db.sql("update session set v7="+hr_max+" where session_id="+session,1);
	                    	         res=db.sql("update user set data1="+hr_max+" where id="+user_id,1);
	                    	    
	                      } else {feedback_end=0;}
	                      
	                      
	                      //
	                      // steigern alle 200m um 0,5km/h
	                      
	        	     } catch (SQLException e) {}
                }	
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
	
}
