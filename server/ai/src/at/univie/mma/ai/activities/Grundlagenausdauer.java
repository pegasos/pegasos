package at.univie.mma.ai.activities;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;



public class Grundlagenausdauer extends AI_module {

	
	private ResultSet res=null;
	private int mistakes=0;
	private int mistakes_allowed=10;
	private String feedback;
	private final long session;
	private int hr,hr_max,hr_min,level;
	long hr_shall_min,hr_shall_max;
	
	public Grundlagenausdauer(Parameters p) {
		super(p);
		this.session= p.session;
		this.level= p.param2;
	    log("Intervalltraining Level "+ p.param2+" started!");
	       
        res=db.sql("select data2,id,data4 from user where id in (select userid from session where session_id="+session+")",0); // GET HR_MAX
        try {if (res.first()) {hr_max=res.getInt(1);hr_min=res.getInt(3);}} catch (SQLException e) {}
        if (hr_max==0) {hr_max=180;}
	    
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
		start_timer();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive()) {log("Killed AI-Session Grundlagenausdauer for session:"+session);stop_timer();close_ai();}
	        	
	        	try {
                     	  sql="select v1 from session where session_id="+session;   // GET ACTUAL SPEED, HR
                          res=db.sql(sql,0);
                          if (res.first()) {hr=res.getInt(1);}
                          
                          if (level==1) { // anfänger
                        	  hr_shall_min=Math.round(hr_min+0.6*(hr_max-hr_min));
                        	  hr_shall_max=Math.round(hr_min+0.69*(hr_max-hr_min));
                          }	 else
                          if (level==2) { // fortgeschrittene
                            	  hr_shall_min=Math.round(hr_min+0.7*(hr_max-hr_min));
                            	  hr_shall_max=Math.round(hr_min+0.79*(hr_max-hr_min));
                              }	 else
                          if (level==3) { // profi
                                	  hr_shall_min=Math.round(hr_min+0.8*(hr_max-hr_min));
                                	  hr_shall_max=Math.round(hr_min+0.89*(hr_max-hr_min));
                          }
                              
                          if (hr>hr_shall_max) {mistakes++;feedback="Herzfrequenz zu hoch!";}  	
                          if (hr<hr_shall_min) {mistakes++;feedback="Herzfrequenz zu niedrig!";}
                          
                          if (mistakes>mistakes_allowed) {send_feedback(feedback,0);mistakes=0;}
	        	} catch (SQLException e) {}
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
   
	
}
