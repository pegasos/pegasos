package at.univie.mma.ai.activities;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;

public class DistanceRun extends AI_module {
  /**
   * Intervall [ms] in welchen eine Nachricht geschickt wird, dass auf den Start gewartet wird. 
   */
  public final int TIME_BETWEEN_WAIT_MESSAGES= 120000;
  /**
   * Nach wieviel [ms] das Warten auf den Start abgebrochen wird
   */
  public final int WAIT_TIMEOUT= 600000;
  
  private final long session;
  /**
   * Distance which needs to be covered. This will be increased by the distance traveled to the start.
   */
  private int dist;
  private int lang;
  private long timeStart;
  private long timeEnd;
  
  private final int thresh_speed = 6;
  private int next_km_dist;
  private int next_km= 1;
  private PreparedStatement stat_dist;
  
  private String feedback_notstarted_error= "Fehler beim Starten des Distanzlaufs";
  private String feedback_dist_covered = "Schon %d km gelaufen!";
  
  class AbwarterThread extends Thread
  {
    PreparedStatement stat;
    
    public AbwarterThread() throws SQLException
    {
      super();
      
      String sql = "select v4 from session where session_id=" + session;
      stat= db.createStatement(sql);
    }
    
    public void run()
    {
      boolean waiting= true;
      long t_last_msg= System.currentTimeMillis();
      long t_start= t_last_msg;
      
      while( waiting )
      {
        try
        {
          if( !stat.execute() )
          {
            //TODO: report error
          }
          else
          {
            ResultSet res = stat.getResultSet();
            res.next();
            double speed= ((double) res.getInt(1)) * 0.0036D;
            
            // log("Aktuelle Geschwindigkeit: " + speed);
            if(speed > thresh_speed)
            {
              waiting= false;
              start_timer();
            }
            else
            {
              long t_cur= System.currentTimeMillis();
              if( t_last_msg + TIME_BETWEEN_WAIT_MESSAGES < t_cur )
              {
                if( lang == 0 )
                  send_feedback("Warten auf Erreichen von " + thresh_speed +"km/h",0);
                else
                  send_feedback("Run with " + thresh_speed +"km/h to start the run",0);
                
                t_last_msg= t_cur;
              }
              else if( t_start + WAIT_TIMEOUT < t_cur )
              {
                if( lang == 0 )
                  send_feedback("Distanzlauf wird abgebrochen",1);
                else
                  send_feedback("Distancerun canceled",1);
                
                waiting= false;
              }
            }
          }
          Thread.sleep(500);
        }
        catch(SQLException e)
        {
          log(e.getMessage());
          //TODO: report error
          StringWriter sw = new StringWriter();
          PrintWriter pw = new PrintWriter(sw);
          e.printStackTrace(pw);
          log(sw.toString());
          e.printStackTrace();
          log("DistanceRun::WaitForStart:run " + e.getMessage());
          waiting= false;
        }
        catch (InterruptedException e)
        {
          // TODO Auto-generated catch block, report error
          e.printStackTrace();
          waiting= false;
        }
      }
    }
  }

  /**
   * 
   */
  public DistanceRun(Parameters p)
  {
    super(p);
    
    this.session= p.session;
    this.dist= p.param2;
    this.lang= p.lang;
    
    log("DistanceRun for session "+session+", distance "+dist+" started!");
    
    if( lang == 0 )
      send_feedback("Distanzlauf startet bei erreichen von " + thresh_speed +"km/h",0);
    else
      send_feedback("distance run starts when reaching " + thresh_speed +"km/h",0);
    
    try
    {
      String sql = "select v3 from session where session_id=" + session;
      stat_dist= db.createStatement(sql);
      
      db.sql("update session set v10=1 where session_id="+session,1);
      
      startAbwarten();
    }
    catch (SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log("DistanceRun::DistanceRun " + e.getMessage());
    }
    
    db.sql("update session set v10=1 where session_id="+session,1);
  }
  
  private void startAbwarten() throws SQLException
  {
    Thread t= new AbwarterThread();
    t.start();
  }
  
  private void start_timer()
  {
    try
    {
      if( !stat_dist.execute() )
        send_feedback(feedback_notstarted_error, 1);
      
      ResultSet res = stat_dist.getResultSet();
      res.next();
      int off= res.getInt(1);
      dist+= off;
      next_km_dist= off + 1000;
      timeStart= System.currentTimeMillis();
      
      log("DistanceRun:" + session + " start at dist:" + off + " t: " + timeStart);
      
      send_feedback("Los gehts!", 0);
      
      timer = new Timer();
      timer.schedule(task, 0, 1000);
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block, report error
      e.printStackTrace();
      log("DistanceRun::start_timer " + e.getMessage());
    }
  }
  
  private TimerTask task = new TimerTask() {
    public void run()
    {
      if( !alive() )
      {
        log("Killed AI-Session DistanceRun for session:" + session);
        stop_timer();
        close_ai();
      }
      
      try
      {
        if( !stat_dist.execute() )
        {
          //TODO: report error
        }
        else
        {
          ResultSet res = stat_dist.getResultSet();
          res.next();
          int curdist= res.getInt(1);
          
          if( curdist >= dist )
          {
            timeEnd= System.currentTimeMillis();
            double elapsed_seconds= (timeEnd - timeStart) / 1000D;
            String zeit= String.format("%1d Stunden %2d Minuten %2d Sekunden", new Object[] { Long.valueOf((long) (elapsed_seconds / 3600L)), Long.valueOf((long) (elapsed_seconds % 3600L / 60L)), Long.valueOf((long) (elapsed_seconds % 60L)) });
            
            log("DistanceRun:" + session + " stopped at" + curdist + " t: " + timeEnd + " elapsed[s]: " + elapsed_seconds + " zeit: " + zeit);
  
            send_feedback("Lauf beendet. Zeit: " + zeit, 1);
            
            stop_timer();
            close_ai();
          }
          else if( curdist >= next_km_dist )
          {
            send_feedback(String.format(feedback_dist_covered  , next_km), 0);
            next_km_dist+= 1000;
            next_km++;
          }
        }
      }
      catch (SQLException e)
      {
        // TODO Auto-generated catch block, report error
        e.printStackTrace();
        log("DistanceRun::TimerTask " + e.getMessage());
      }
    } // end task
  };
  
  private void stop_timer()
  {
    try
    {
      timer.cancel();
    }
    catch (Exception e)
    {
      logStackTrace(e);
    }
  }
}
