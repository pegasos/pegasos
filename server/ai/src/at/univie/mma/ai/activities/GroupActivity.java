//CALCULATION OF RAI

// (Schrittanzahl(Person) / Zeit(PersonimSpiel))  /   (Mittel (Schrittanzahl(Person) / Zeit(PersonimSpiel)) )

// CALCULATION OF RANKING




// CLUB START --> Check for Sessions with same team and group--> Feedbackmeldung in welchem Team man mitmacht 

// PRIVATE START --> Nachsehen ob in einem befreundeten Team schon eine Session läuft - wenn ja, dann AI-Modul abdrehen und Feedback über Name des Session-Halters,
// Andernfalls selbst AI-Modul initieren. Teilnehmen können dann alle Freunde auf der eigenen Liste. Ebenfalls Feedbackmeldung!


// Beenden der Session --> bis keiner mehr läuft (last update von ALLEN>5 sekunden!!)



package at.univie.mma.ai.activities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;
import at.univie.mma.ai.postprocessing.MaxHearRate;
import at.univie.mma.ai.postprocessing.MinHearRate;
import at.univie.mma.ai.postprocessing.None;
import at.univie.mma.ai.postprocessing.PostProcessingException;
import at.univie.mma.ai.postprocessing.PostProcessingModule;
import at.univie.mma.ai.postprocessing.Unterricht;
import at.univie.mma.ai.Commands;

public class GroupActivity  extends AI_module {

  // Parameters
  
  /**
   * Check current status every [x] ms
   */
  private static final int timer_interval = 5000;
  
  /**
   * Maximum time between <now> and session_update allowed for a session to be started/considered to be alive.
   */
  private static final int TIMEOUT_INTERVAL = 30000;

  private ResultSet res = null;
  private long session, user_id, group_id;
  
  /**
   * Number of users for which the activity has been started
   */
  private int started = 0;
  
  /**
   * IDs of the users for which the activity was started
   */
  private long[] user_ids;
  
  /**
   * IDs of the sessions
   */
  private long[] session_ids;
  
  /**
   * Param2 of the sessions for which the activity was started
   */
  private int[] param2s;
  
  private String msg = "";
  int a;
	int param2;
	
	public GroupActivity(Parameters p) {
		super(p);
		this.session= p.session;
		this.group_id= p.param2;
		this.a= p.param1;
		
		log("GroupActivity s:" + session + " l:" + p.lang + " g:" + group_id + " a:" + a);
		
    if (session != 0)
    {
      try
      {
        res = db.sql("select userid,teamid,groupid from session where session_id=" + session, 0);
        if (res.next())
        {
          user_id = res.getLong(1);
          // team_id = res.getLong(2);
          group_id = res.getInt(3);
        
          log("GroupActivity for user " + user_id + " started!");
          
          if (p.lang == 0)
            msg = " ";
          else
            msg = "Please wait for the start signal";
          
          db.sql("insert into feedback (session_id,message) values (" + session + ",'" + msg + "')", 1);
        }
        else
        {
          log("There was an error selecting user-info from session");
        }
        close_ai();
      }
      catch (SQLException e)
      {
        log(e.getMessage());
        e.printStackTrace();
      }
    }
    else
    {
      log("Starting GroupActivity Module");
      start_timer();
    }
	}

  private void start_timer()
  {
    timer = new Timer();
    timer.schedule(task, 0, timer_interval);
  }

  private TimerTask task = new TimerTask()
  {
    @Override
    public void run()
    {
      try
      {
        // TODO: DO NOT WAIT FOR EVER!!!!!!
        if (started == 0) // -> start, iff users are waiting
        {
          // Start feedback synchroniously for all users!
          String q = "select count(session_id) from session where activity=8 and param1=" + a 
              + " and groupid=" + group_id 
              + " and last_update>" + (System.currentTimeMillis() - TIMEOUT_INTERVAL);
          
          res = db.sql(q, 0);
          res.next();
          int count = res.getInt(1);
          if( count <= 0 )
            return;
          
          user_ids= new long[count];
          session_ids= new long[count];
          param2s= new int[count];
          
          q = "select session_id,userid,param2 from session where activity=8 and param1=" + a 
              + " and groupid=" + group_id
              + " and last_update>" + (System.currentTimeMillis() - TIMEOUT_INTERVAL);
          res = db.sql(q, 0);
          
          long start_time= System.currentTimeMillis();
          while (res.next()) // Loop over all users
          {
            session = res.getLong(1);
            user_id = res.getLong(2);
            param2 = res.getInt(3);
            
            log("Start for Group-Activity User_id:" + user_id);

            send_command(session, Commands.Codes.GroupActivity, Commands.OK, "1");
            
            db.sql("update session set v10=1,starttime=" + start_time + ",last_update="
                + start_time + " where session_id=" + session, 1);
            
            user_ids[started]= user_id;
            session_ids[started]= session;
            param2s[started]= param2;
            
            started++;
          }
          
          if( started != count )
            log("Error starting Group activity. Only " + started + " sessions started (instead of " + count + ")");
        }
        else
        { // started=1
          // Alive?! --> stay alive as long someone is running!
          
          res = db.sql("select last_update from session where activity=8 and param1=" 
              + a + " and groupid=" + group_id
              + " and last_update > " + (System.currentTimeMillis() - TIMEOUT_INTERVAL), 0);
          
          if (!res.next())
          {
            log("Group-Activity for Team beendet!");
            stop_timer();
            
            // TODO: do post operation (in a generated, more modular fashion not hard-coded
            PostProcessingModule mod;
            switch( param2s[0] ) // TODO: i guess this is not safe!
            {
              case 1:
                mod= new MaxHearRate();
                break;
                
              case 2:
                mod= new None();
                break;
                
              case 3:
                mod= new MinHearRate();
                break;
              
              default:
                log("Error(?): No post-processing module for " + param2 + " declared");
                mod= new None();
                break;
            }
            
            mod.setDB(db);
            
            log("Adding Users: " + Arrays.toString(user_ids) + " Sessions: " + Arrays.toString(session_ids));
            mod.addUser_batch(user_ids, session_ids);
            
            mod.process(GroupActivity.this);
            
            log("Setting Session values");
            PostProcessingModule mod2= new Unterricht();
            mod2.setDB(db);
            log("Adding Users: " + Arrays.toString(user_ids) + " Sessions: " + Arrays.toString(session_ids));
            mod2.addUser_batch(user_ids, session_ids);
            mod2.process(GroupActivity.this);
            
            close_ai();
          }
        }
      }
      catch (PostProcessingException | SQLException e)
      {
        log("Exception " + e.getMessage());
      }
    } // end task   
  };
  
  private void stop_timer() {
    try {
      timer.cancel();
    } catch (Exception e) {
    }
  }
}
