package at.univie.mma.ai.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.*;
import at.univie.mma.ai.Parameters;



public class PerPot extends AI_module {

	private final long session;
	private final int dist;
	private ResultSet res=null;
	private double speed;
	
	public PerPot(Parameters p) {
		super(p);
		this.session= p.session;
		this.dist= p.param2;
		
		log("PerPot Module for session "+session+", distance "+dist+" started!");
		start_feedback(dist);
		//start_timer();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }	
	
	private TimerTask task = new TimerTask() {
        public void run() {
        	
        	if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
        	
    	    sql="select v4 from session where session_id="+session;   // GET ACTUAL SPEED
    	    //log("select v4 from session where session_id="+session);
            res=db.sql(sql,0);
            try {
			      if (res.first()) {
			      speed=res.getDouble(1)*0.0036;
			      //log("SPEED:"+speed);
			      if (speed>8) 
			       {
			    	res=db.sql("update session set v10=1,starttime="+System.currentTimeMillis()+" where session_id="+session,1);
			    	stop_timer();start_feedback(dist);
			       }
			    }     
		    } catch (SQLException e) {}
        } // end task  	
 };
    
    
   
   
private void stop_timer() {
      try {timer.cancel();} catch (Exception e) {}
}
	
	
	private void start_feedback(int dist) {
		String[] cmd = {"java", "-jar", "perpot_wettkampf.jar",String.valueOf(session),String.valueOf(dist)};
		Process pr=null;
		
		try
		{
			pr = Runtime.getRuntime().exec(cmd);
		}
		catch (IOException e)
		{
			log("Error starting PerPot Module!");
		}
		
    BufferedReader line_in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
    String line_out = null;
    
    try
    {
      while((line_out = line_in.readLine()) != null)
      {
        log("AI call returned with:" + line_out);
      }
    }
    catch (IOException e)
    {
      
    }
    
    if( line_out == null )
      log("AI call returned with:1 (calibration succesfull)");
	    
	    log("PerPot ended for Session: " + session);
	    
		close_ai();
	}
}
