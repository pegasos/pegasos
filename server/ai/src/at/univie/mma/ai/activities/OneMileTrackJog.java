package at.univie.mma.ai.activities;

import java.sql.*;
import java.util.*;

import javax.xml.datatype.*;

import activities.*;
import at.univie.mma.ai.*;
   


/* 
Submaximal One Mile Track Jog nach George et al., 1993

VO2max=
100,5 + (8,344 x Geschlecht)  (0,1636 x Gewicht)  (1,438 x Zeit)  (0,1928 x HF)
Geschlecht: 1 für Männer; 0 für Frauen, Zeit in Minuten 


Distanz: 1600m
Intensität: Frauen nicht schneller als 9 Minuten = 10,7km/h
Männer nicht schneller als 8 Minuten = 12km/h
Herzfrequenz: <= 180 Schläge/Min

*/




public class OneMileTrackJog extends AI_module {

	private final long session;
	private final int lang;
	private ResultSet res=null;
	private int dist100,actualdist=0,round=1,hr=0,user_id,weight,gender,age,hr_feedback=0,speed_feedback=0;
	private long birthday,start;
	private double vo2max=0,speed=0;
	
	public OneMileTrackJog(Parameters p) {
		super(p);
		this.session= p.session;
		this.lang= p.lang;
	    log("One Mile Track Jog started!");
	    if (lang==0) {send_feedback("One Mile Track Jog wurde gestartet",0);} else {send_feedback("One Mile Track Jog started",0);}
	    
	    res=db.sql("update session set v10=1,last_update="+System.currentTimeMillis()+" where session_id="+session,1);
	    
	    try {
	         sql="select userid from session where session_id="+session;   // GET ACTUAL DIST
             res=db.sql(sql,0);
			 if (res.first()) {user_id=res.getInt(1);}
			 
			 sql="select birthday,weight,gender from user where id="+user_id;   // GET USER_ID
             res=db.sql(sql,0);
             if (res.first()) {birthday=res.getLong(1);weight=res.getInt(2);gender=res.getInt(3);}
		} catch (SQLException e) {}
	    
		start_timer();
		start=System.currentTimeMillis();
	}
	
	
	private void start_timer() {
        timer = new Timer();
        timer.schedule(task,0,1000); 
    }
	   
	    
    private TimerTask task = new TimerTask() {
	        public void run() {
	        	
	        	if (!alive()) {log("Killed AI-Session!");stop_timer();close_ai();}
	        	
	        	try {
	        		sql="select v3,v1,v4 from session where session_id="+session;   // GET ACTUAL DIST,hr,speed
                    res=db.sql(sql,0);
                    if (res.first()) {actualdist=res.getInt(1);hr=res.getInt(2);speed=res.getDouble(3)*0.0036;}
                    
                    if (actualdist>=1600) {
                    	  
                          long diffMillis = System.currentTimeMillis()  - (birthday*1000);  // Calculate Age
                          DatatypeFactory datatypeFactory = null;
  						  try {datatypeFactory = DatatypeFactory.newInstance();} catch (DatatypeConfigurationException e) {log("ERROR DATATYPEFACT");}
                          Duration duration = datatypeFactory.newDuration(diffMillis);
                          age=duration.getYears();
                          vo2max=100.5 + (8.344*gender)-(0.1636*weight)-(1.438*((System.currentTimeMillis()-start)/1000/60))-(0.1928*hr);
                          if (lang==0) {send_feedback("One Mile Track Jog abgeschlossen, VO2max ist "+(long)vo2max,1);} else {send_feedback("One Mile Track Jog finished, VO2max is "+(long)vo2max,1);}
                          
                          res=db.sql("update session set v6="+(long)vo2max+" where session_id="+session,1); // set VO2max  
                          res=db.sql("update user set data1="+(long)vo2max+" where id="+user_id,1); // set VO2max
                    	  
                          log("Killed AI-Session!");stop_timer();close_ai();
                          
                    } else {
                    	
                      if ((actualdist-dist100)>100) {
                    	  dist100+=100;
                    	  if (lang==0) {send_feedback("Noch "+(1600-round*100)+" Meter",0);} else {send_feedback((1600-round*100)+" Meters left",0);}
        	              round++;
                      }
                      
                      if (hr>180) {
                    	  hr_feedback++;
                    	  if (hr_feedback>15) 
                    	   {
                    		  if (lang==0) {send_feedback("Herzfrequenz zu hoch",0);} else {send_feedback("Heartrate too high",0);}
                    		  hr_feedback=0;                    		  
                    	   }
                      }
                      
                    //  Intensität: Frauen nicht schneller als 9 Minuten = 10,7km/h
                    //  Männer nicht schneller als 8 Minuten = 12km/h
                      
                      if (((speed>10.7) && (gender==0)) || ((speed>12) && (gender==1))) {
                    	  speed_feedback++;
                    	  if (speed_feedback>15) {
                    		  if (lang==0) {send_feedback("Geschwindigkeit zu hoch",0);} else {send_feedback("You are too fast!",0);}
                    		  speed_feedback=0;
                    	  }
                      }
                      
                    }
                    
                    
	        	} catch (SQLException e) {}
	        	
	    } // end task  	
     };
	    
	    
	   
	   
   private void stop_timer() {
	      try {timer.cancel();} catch (Exception e) {}
   }
	
}

