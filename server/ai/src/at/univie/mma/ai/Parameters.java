package at.univie.mma.ai;

public class Parameters {
  public int param0;
  public int param1;
  public int param2;
  
  public long session;
  public int lang;
  public int restart;
  public int ai;
  
  @Override
  public String toString()
  {
    return "Parameters [param0=" + param0 + ", param1=" + param1 + ", param2=" + param2 + ", session=" + session + ", lang=" + lang
        + ", restart=" + restart + ", ai=" + ai + "]";
  }
}
