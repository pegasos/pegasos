// FEEDBACKQUEUE, FEEDBACKS MIT PRIORITÄT ZUERST, ABSTAND FEEDBACKS 10 Sekunden o.Ä

package at.univie.mma.ai;

import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import at.pegasos.ai.util.log.LoggerLayout;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;

//$ screen
//$ node server.js

public class AI {
  
  public static void main(String[] args) throws NumberFormatException, SQLException
  {
    // System.out.println("Welcome to the MMA AI-Manager with Activity:"+args[0]+"
    // Session:"+args[1]+" SubActivity:"+args[2]+" Param 2:"+args[3]+" AI:"+args[4]+"
    // Restart:"+args[5]+" LANGUAGE:"+args[6]);
    Parameters p= new Parameters();
    p.param0= Integer.parseInt(args[0]);
    p.param1= Integer.parseInt(args[2]);
    p.param2= Integer.parseInt(args[3]);
    p.session= Integer.parseInt(args[1]);
    p.ai= Integer.parseInt(args[4]);
    p.restart= Integer.parseInt(args[5]);
    p.lang= Integer.parseInt(args[6]);

    setupLogging(p);
    
    FeedbackModuleTable.startModulesFromParameter(p);
  }

  private static void setupLogging(Parameters p)
  {
    Logger rootLogger= (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    LoggerContext loggerContext= rootLogger.getLoggerContext();
    // we are not interested in auto-configuration
    loggerContext.reset();
    
    LoggerLayout layout= new LoggerLayout();
    layout.setContext(loggerContext);
    layout.setSession(p.session);
    layout.start();
    
    ConsoleAppender<ILoggingEvent> appender= new ConsoleAppender<ILoggingEvent>();
    appender.setContext(loggerContext);
    appender.setLayout(layout);
    appender.start();
    
    FileAppender<ILoggingEvent> fappender= new FileAppender<ILoggingEvent>();
    fappender.setContext(loggerContext);
    fappender.setFile(Config.LOG_FILE);
    fappender.setPrudent(true);
    fappender.setAppend(true);
    fappender.setLayout(layout);
    fappender.start();
    
    rootLogger.addAppender(appender);
    rootLogger.addAppender(fappender);
  }
  
}
