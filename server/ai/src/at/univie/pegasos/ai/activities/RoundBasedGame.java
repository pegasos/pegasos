package at.univie.pegasos.ai.activities;

import at.univie.mma.ai.Commands;
import at.univie.mma.ai.Parameters;
import at.univie.pegasos.ai.GroupActivity;

public abstract class RoundBasedGame extends GroupActivity {

  protected int round_nr;
  
  protected long round_starttime;
  protected long round_endtime;

  public RoundBasedGame(Parameters p)
  {
    super(p);
    round_nr= 0;
  }

  protected void startRound()
  {
    fetchAndStartUsers();
    
    round_nr++;
    String[] data= {"5", Long.toString(group_id), Integer.toString(super.a)}; // "start round", group_id, "activity ID"
    round_starttime= send_command(session, Commands.Codes.GroupActivity, Commands.OK, (Object[]) data);
  }
  
  protected void endRound(String... data)
  {
    if( data.length == 0 )
    {
      String[] send_data= {"6", Long.toString(group_id), Integer.toString(super.a)}; // "end round", group_id, "activity ID"
      round_endtime= send_command(session, Commands.Codes.GroupActivity, Commands.OK, (Object[]) send_data);
    }
    else
    {
      String[] send_data= new String[3 + data.length];
      send_data[0]= "6";
      send_data[1]= Long.toString(group_id);
      send_data[2]= Integer.toString(super.a); // "end round", group_id, "activity ID"
      for(int i= 0; i < data.length; i++)
        send_data[i+3]= data[i];
      log("" + send_data.length);
      round_endtime= send_command(session, Commands.Codes.GroupActivity, Commands.OK, (Object[]) send_data);
    }
  }
}
