package at.univie.pegasos.ai;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import activities.AI_module;
import at.univie.mma.ai.Commands;
import at.univie.mma.ai.Parameters;

public abstract class GroupActivity extends AI_module {
  
  /**
   * Check current status every [x] ms
   */
  private static final int TIMER_INTERVAL= 1000;
  
  /**
   * Maximum time between <now> and session_update allowed for a session to be
   * started/considered to be alive.
   */
  private static final int TIMEOUT_INTERVAL= 30000;
  
  private ResultSet res= null;
  protected long session;
  private long user_id;
  protected long group_id;
  
  /**
   * Number of users for which the activity has been started
   */
  private int started= 0;
  
  /**
   * IDs of the users for which the activity was started
   */
  protected long[] user_ids;
  
  /**
   * IDs of the sessions
   */
  protected long[] session_ids;
  
  /**
   * Param2 of the sessions for which the activity was started
   */
  private int[] param2s;
  
  private String msg= "";
  protected int a;
  int param2;
  
  private PreparedStatement stmt_user;

  protected MessageQueue messages;
  
  public GroupActivity(Parameters p)
  {
    super(p);
    this.session= p.session;
    this.group_id= p.ai;
    this.a= p.param1;
    
    log("GroupActivity s:" + session + " l:" + p.lang + " g:" + group_id + " a:" + a);
    log(p.toString());
    
    // restart != 2: Normal user
    if( p.restart != 2 )
    {
      try
      {
        res= db.sql("select userid,teamid,groupid from session where session_id=" + session, 0);
        if( res.next() )
        {
          user_id= res.getLong(1);
          // team_id = res.getLong(2);
          group_id= res.getInt(3);
          
          log("GroupActivity for user " + user_id + " started!");
          
          if( p.lang == 0 )
            msg= " ";
          else
            msg= "Please wait for the start signal";
            
          db.sql("insert into feedback (session_id,message) values (" + session + ",'" + msg + "')", 1);
        }
        else
        {
          log("There was an error selecting user-info from session");
        }
        close_ai_no_post();
      }
      catch (SQLException e)
      {
        log(e.getMessage());
        e.printStackTrace();
      }
    }
    else
    {
      log("Starting GroupActivity Module");
      try
      {
        start();
      }
      catch( SQLException e )
      {
        log(e.getMessage());
        e.printStackTrace();
        logStackTrace(e);
      }
    }
  }
  
  private void start() throws SQLException
  {
    stmt_user= db.createStatement("insert into in_group_session(controller_session_id, participant_session_id) values (?,?)");
    
    messages= new MessageQueue(db, session);
    
    timer= new Timer();
    timer.schedule(task, 0, TIMER_INTERVAL);
  }
  
  private TimerTask task= new TimerTask() {
    @Override
    public void run()
    {
      try
      {
        // TODO: DO NOT WAIT FOR EVER!!!!!!
        if( started == 0 ) // -> start, iff users are waiting
        {
          fetchAndStartUsers();
            
          onStart();
        }
        else
        { // started=1
          // Alive?! --> stay alive as long someone is running!
          
          res= db.sql("select last_update from session where activity=8 and param1=" + a + " and groupid=" + group_id
              + " and last_update > " + (System.currentTimeMillis() - TIMEOUT_INTERVAL), 0);
              
          if( !res.next() )
          {
            log("Group-Activity for Team beendet!");
            stop_timer();
            
            sql= "update session set endtime=" + System.currentTimeMillis() + ", last_update=" + System.currentTimeMillis()
                + " where session_id=" + session;
            log(sql);
            ResultSet res = db.sql(sql, 1);
            if( (res != null) && (res.getWarnings() != null) )
              log("SQL WARNINGS!!:" + res.getWarnings());
            
            onEnd();
            
            close_ai();
          }
          else
          {
            messages.fetchMessages();
            onLoop();
          }
        }
      }
      catch (SQLException e)
      {
        log("SQLException " + e.getMessage());
      }
      catch (Exception e)
      {
        log("Exception " + e.getMessage());
        logStackTrace(e);
      }
    } // end task
  };
  
  /**
   * Fetch all users waiting for this group session and start their sessions (i.e. set v10 = 1)
   */
  protected void fetchAndStartUsers()
  {
    try
    {
      started= 0;
      
      // Start feedback synchronously for all users!
      String q= "select count(session_id) from session where activity=8 and param1=" + a + " and groupid=" + group_id
          + " and endtime = 0 AND last_update>" + (System.currentTimeMillis() - TIMEOUT_INTERVAL) + " and ai=1";
      
      res= db.sql(q, 0);
      res.next();
      int count= res.getInt(1);
      if( count <= 0 )
        return;
      
      user_ids= new long[count];
      session_ids= new long[count];
      param2s= new int[count];
      
      q= "select s.session_id,s.userid,s.param2,s.v10,i.controller_session_id from session s "
          + "LEFT JOIN in_group_session i ON s.session_id = i.participant_session_id WHERE activity=8 and param1=" + a + " and groupid="
          + group_id + " and endtime = 0 AND last_update>" + (System.currentTimeMillis() - TIMEOUT_INTERVAL) + " and ai=1";
      res= db.sql(q, 0);
      
      long start_time= System.currentTimeMillis();
      int csa= 0; // controller sessions added
      while( res.next() ) // Loop over all users
      {
        long a_session= res.getLong(1);
        long a_user_id= res.getLong(2);
        int a_param2= res.getInt(3);
        
        log("Start for Group-Activity User_id:" + a_user_id);
        
        send_command(a_session, Commands.Codes.GroupActivity, Commands.OK, "4;" + session);
        
        // only start users which have not been started yet
        if( res.getInt(4) == 0 )
          db.sql("update session set v10=1,starttime=" + start_time + ",last_update=" + start_time + " where session_id=" + a_session, 1);
        
        user_ids[started]= a_user_id;
        session_ids[started]= a_session;
        param2s[started]= a_param2;
        
        if( res.getInt(5) == 0 )
        {
          stmt_user.setLong(1, session);
          stmt_user.setLong(2, a_session);
          stmt_user.addBatch();
          csa++;
        }
        else
          log("There is something completely wrong and the user in our group was not started by us");
        
        started++;
      }
      
      if( csa > 0 )
        stmt_user.executeBatch();
      
      if( started != count )
        log("Error starting Group activity. Only " + started + " sessions started (instead of " + count + ")");
    }
    catch( SQLException e )
    {
      log("SQLException " + e.getMessage() + ", " + e.getClass());
      logStackTrace(e);
    }
    catch( Exception e )
    {
      log("Exception " + e.getMessage());
      logStackTrace(e);
    }
  }
  
  private void stop_timer()
  {
    try
    {
      timer.cancel();
    }
    catch( Exception e )
    {
    }
  }
  
  /**
   * This method is called once the activity received the start signal
   */
  protected abstract void onStart();
  
  /**
   * This method is called after all participants have finished their activity
   */
  protected abstract void onEnd();
  
  /**
   * This method is called every ~TIMER_INTERVAL ms if the activity is started and users are
   * active
   */
  protected void onLoop()
  {
    
  }
  
  protected void send_feedback_session(long session, String text, int stop)
  {
    log("Feedback:" + session + ":" + text+ " ," + stop);
    
    sql= "insert into feedback(session_id,message,stop_session,itime) values (" + session + ",'" + text + "'," + stop + ","
        + System.currentTimeMillis() + ")";
    
    db.sql(sql, 1);
  }
  
  /**
   * Send a message to all active sessions
   * 
   * @param text
   *          text which is sent
   * @param stop
   *          stop=1 signals clients to stop the activity
   */
  protected void sendAll(String text, int stop)
  {
    int l= session_ids.length;
    for(int i= 0; i < l; i++)
    {
      send_feedback_session(session_ids[i], text, stop);
    }
  }
  
  /**
   * Send a command to all active sessions
   * 
   * @param command
   *          command to be sent
   */
  protected void sendAll(Command command)
  {
    int l= session_ids.length;
    for(int i= 0; i < l; i++)
    {
      send_command(session_ids[i], command);
    }
  }
}
