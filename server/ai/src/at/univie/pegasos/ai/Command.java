package at.univie.pegasos.ai;

public abstract class Command {
  public int status; 
  public int code;
  
  public Object[] data;
}
