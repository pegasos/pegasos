package at.univie.pegasos.ai;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import activities.Db_connector;

/**
 * 
 * TODO: make this thread-safe
 */
public class MessageQueue {
  public class Message {
    long message_id;
    long itime;
    String[] message;
    boolean stop;
    
    public Message(long message_id, long itime, String[] message, boolean stop)
    {
      super();
      this.message_id= message_id;
      this.itime= itime;
      this.message= message;
      this.stop= stop;
    }
    
    public long getID()
    {
      return message_id;
    }
    
    public String[] getMessage()
    {
      return message;
    }
    
    public boolean isStop()
    {
      return stop;
    }
    
    public void markread() throws SQLException
    {
      stmt_mr.setLong(1, System.currentTimeMillis());
      stmt_mr.setLong(2, message_id);
      stmt_mr.executeUpdate();
    }
  }
  
  private PreparedStatement stmt;
  private PreparedStatement stmt_mr;
  
  private Queue<Message> messages;
  private Set<Long> message_ids;
  
  public MessageQueue(Db_connector db, long session_id) throws SQLException
  {
    stmt= db.createStatementRO("select message_id, itime, message, stop_session from ai_message where session_id = ? and rtime is null order by itime ASC");
    stmt.setLong(1, session_id);
    
    stmt_mr= db.createStatement("update ai_message set rtime = ? where message_id = ?");
    
    messages= new LinkedBlockingQueue<Message>();
    message_ids= new HashSet<Long>(20);
  }
  
  public void fetchMessages() throws SQLException
  {
    ResultSet res= stmt.executeQuery();
    while( res.next() )
    {
      long id= res.getLong(1);
      
      // skip message if allready in queue
      if( message_ids.contains(id) )
        continue;
      
      message_ids.add(id);
      Message m= new Message(id, res.getLong(2), res.getString(3).split(";"), (res.getInt(4) == 0 ? false : true));
      messages.add(m);
    }
  }
  
  /**
   * Retrieve the first message from the queue and mark the message read. 
   * @return the message or null if there is none
   * @throws SQLException 
   */
  public Message poll() throws SQLException
  {
    Message ret= messages.poll();
    
    if( ret != null )
    {
      ret.markread();
      message_ids.remove(ret.getID());
    }
    
    return ret;
  }
}
