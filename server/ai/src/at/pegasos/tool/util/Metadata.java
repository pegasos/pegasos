package at.pegasos.tool.util;

import java.util.*;

public class Metadata {
  Map<String, String> kv;

  public Metadata()
  {
    kv = new HashMap<String, String>();
  }

  public boolean hasKey(String key)
  {
    return kv.containsKey(key);
  }

  public String getValue(String key)
  {
    return kv.get(key);
  }
}