package at.pegasos.tool.util;

public class Util {
  public static boolean isNumericWhole(String strNum)
  {
    if (strNum == null)
    {
      return false;
    }
    try
    {
      Long.parseLong(strNum);
    }
    catch (NumberFormatException nfe)
    {
      return false;
    }
    return true;
  }
}
