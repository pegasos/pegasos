package at.pegasos.ai;

import java.sql.*;
import java.util.*;

public class SessionTools
{
  public static class Params {
    public int param0;
    public int param1;
    public int param2;
  }

  private final long session_id;
  private long userid;
  private long groupid;
  private long starttime;
  private long team;
  private Params params;
  private int tz_server;
  private int tz_local;
  private final List<Long> groups;
  private final List<Long> teams;
  
  public static SessionTools create(Database db, long session_id) throws SQLException
  {
    return new SessionTools(session_id, db);
  }
  
  private SessionTools(long session_id, Database db) throws SQLException
  {
    this.session_id= session_id;
    PreparedStatement select= db.prepareStatement("SELECT starttime, userid, teamid, groupid, activity, param1, param2, tz_server, tz_local FROM session WHERE session_id = ?");
    select.setLong(1, session_id);

    groups= new ArrayList<Long>();
    teams= new ArrayList<Long>();

    ResultSet res= select.executeQuery();
    if( !res.next() )
    {
      // TODO: react to error
      System.out.println("Error: No result? (StartTime)");
    }
    else
    {
      starttime= res.getLong(1);
      userid= res.getLong(2);
      team= res.getLong(3);
      groupid= res.getLong(4);
      params = new Params();
      params.param0 = res.getInt(5);
      params.param1 = res.getInt(6);
      params.param2 = res.getInt(7);
      tz_server= res.getInt(8);
      tz_local= res.getInt(9);

      select= db.prepareStatement("select tm.teamid, tm.groupid from teams_memberships tm where tm.userid = ? and tm.deleted = false");
      select.setLong(1, userid);
      res= select.executeQuery();
      while( res.next() )
      {
        teams.add(res.getLong(1));
        groups.add(res.getLong(2));
      }
    }
  }

  public long getStart()
  {
    return starttime;
  }

  public Calendar getStartDate()
  {
    Calendar ret= Calendar.getInstance();
    ret.setTimeInMillis(starttime);
    return ret;
  }

  public long getUser()
  {
    return userid;
  }

  public long getGroup()
  {
    return groupid;
  }

  public int getTimeZoneServer()
  {
    return tz_server;
  }

  public int getTimeZoneLocal()
  {
    return tz_local;
  }

  public List<Long> getUserGroups()
  {
    return groups;
  }

  public List<Long> getUserTeams()
  {
    return teams;
  }

  public Params getParams()
  {
    return params;
  }
}
