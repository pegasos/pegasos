package at.pegasos.ai.util.log;

import org.slf4j.*;

import ch.qos.logback.classic.Logger;

public class Stacktrace {
  /**
   * Log an exception to a specific logger
   *
   * @param logger logger to be used
   * @param e      exception to be logged
   */
  public static void logStackTrace(Logger logger, Exception e)
  {
    StringBuilder builder = new StringBuilder();
    StackTraceElement[] trace = e.getStackTrace();
    for(StackTraceElement traceElement : trace)
    {
      builder
              .append("\tat ")
              .append(traceElement).append("\n");
    }
    logger.error(builder.toString());
  }

  /**
   * Log an exception. This is a convenience method and will log to the root logger
   *
   * @param e exception to be logged
   */
  public static void logStackTrace(Exception e)
  {
    logStackTrace((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME), e);
  }
}
