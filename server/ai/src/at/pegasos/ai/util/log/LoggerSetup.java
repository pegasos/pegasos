package at.pegasos.ai.util.log;

import at.univie.mma.ai.Config;
import at.univie.mma.ai.Parameters;
import ch.qos.logback.classic.*;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import org.slf4j.LoggerFactory;

public class LoggerSetup {
  private static boolean setup = false;
  public static void setupLogging(Parameters parameters)
  {
    setupLogging(parameters.session);
  }

  public static boolean isSetup()
  {
    return setup;
  }

  public static void setupLogging(long sessionId)
  {
    setup = true;

    Logger rootLogger= (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    LoggerContext loggerContext= rootLogger.getLoggerContext();
    // we are not interested in auto-configuration
    loggerContext.reset();

    LoggerLayout layout= new LoggerLayout();
    layout.setContext(loggerContext);
    layout.setSession(sessionId);
    layout.start();

    ConsoleAppender<ILoggingEvent> appender= new ConsoleAppender<ILoggingEvent>();
    appender.setContext(loggerContext);
    appender.setLayout(layout);
    appender.start();

    FileAppender<ILoggingEvent> fappender= new FileAppender<ILoggingEvent>();
    fappender.setContext(loggerContext);
    fappender.setFile(Config.LOG_FILE);
    fappender.setPrudent(true);
    fappender.setAppend(true);
    fappender.setLayout(layout);
    fappender.start();

    rootLogger.addAppender(appender);
    rootLogger.addAppender(fappender);
  }
}
