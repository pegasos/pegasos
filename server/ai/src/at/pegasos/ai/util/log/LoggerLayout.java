package at.pegasos.ai.util.log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.LayoutBase;

public class LoggerLayout extends LayoutBase<ILoggingEvent> {
  
  long session;
  private SimpleDateFormat sdf= new SimpleDateFormat("dd.MM.yy HH:mm:ss");
  private Calendar cal;
  
  public void setSession(long session)
  {
    this.session= session;
  }
  
  public String doLayout(ILoggingEvent event)
  {
    StringBuffer sbuf= new StringBuffer(128);
    
    cal= Calendar.getInstance();
    sbuf.append(sdf.format(cal.getTime()));
    sbuf.append(" [");
    sbuf.append(session);
    sbuf.append("]: ");
    if( !event.getLoggerName().equals(Logger.ROOT_LOGGER_NAME) )
    {
      sbuf.append(event.getLoggerName());
      sbuf.append(" - ");
    }
    sbuf.append(event.getFormattedMessage());
    sbuf.append(System.lineSeparator());
    return sbuf.toString();
  }
}