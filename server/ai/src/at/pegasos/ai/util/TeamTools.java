package at.pegasos.ai.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import at.pegasos.ai.Database;

public class TeamTools
{
  
  public class Group {
    /**
     * ID of the group
     */
    public long group_id;
    
    /**
     * ID of the team it belongs to
     */
    public long team_id;
  }
  
  public static TeamTools create(Database db) throws SQLException
  {
    return new TeamTools(db);
  }

  private PreparedStatement fetchTeamGroup;
  // private final PreparedStatement fetchTeamDefaultGroup;
  private final PreparedStatement insertTeam;

  private TeamTools(Database db) throws SQLException
  {
    fetchTeamGroup= db.createStatementRO("select userid, teamid, groupid from teams_memberships where userid = ? and deleted = false");
    // fetchTeamDefaultGroup= db.createStatementRO("select id from teams_groups where teamid = ? and deleted = false");
    insertTeam = db.prepareStatement("insert into teams_memberships(userid, teamid, groupid, crtime) values (?, ?, ?, ?)");
  }

  public long getDefaultGroupId(long userid) throws SQLException
  {
    fetchTeamGroup.setLong(1, userid);
    fetchTeamGroup.execute();
    ResultSet res= fetchTeamGroup.getResultSet();
    if( res.next() )
    {
      return res.getLong(3);
    }
    else
      return 0;
  }

  public Group getDefaultGroup(long userid) throws SQLException
  {
    fetchTeamGroup.setLong(1, userid);
    fetchTeamGroup.execute();
    ResultSet res= fetchTeamGroup.getResultSet();
    if( res.next() )
    {
      Group ret= new Group();
      ret.team_id= res.getLong(2);
      ret.group_id= res.getLong(3);

      return ret;
    }
    else
      return null;
  }

  public void addUserToTeam(long user_id, long team_id, long group_id) throws SQLException
  {
    // Group group = getDefaultGroupId(userid)
    insertTeam.setLong(1, user_id);
    insertTeam.setLong(2, team_id);
    insertTeam.setLong(3, group_id);
    insertTeam.setLong(4, System.currentTimeMillis() / 1000);
    insertTeam.executeUpdate();
  }
}
