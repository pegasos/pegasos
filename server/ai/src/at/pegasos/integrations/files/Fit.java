package at.pegasos.integrations.files;

import com.beust.jcommander.*;
import com.garmin.fit.Sport;
import com.garmin.fit.*;
import com.garmin.fit.util.*;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.util.Date;
import java.util.TimeZone;
import java.util.*;

import at.pegasos.integrations.files.data.*;
import lombok.extern.slf4j.*;

@Slf4j
@SuppressWarnings("SimpleDateFormat")
public class Fit extends FileImportTool {
  private static final double HALFHOUR = 1000 * 60 * 30;

  private Data data;

  public Fit() throws SQLException
  {
    super.setup();
  }

  public static void main(String[] args) throws ImportFailedException, SQLException
  {
    Fit t = new Fit();

    t.parameters = new Parameters();
    JCommander.newBuilder()
            .addObject(t.parameters)
            .build()
            .parse(args);

    System.out.println(t.parameters);
    t.importFile(t.parameters.input);

    /*
    t.parameters.userId = 1L;

    t.parameters.input = "/tmp/yzvx3Mb6.fit";
    t.importFile(t.parameters.input);
    t.data = null;

    t.parameters.input = "/tmp/y7BpQjxD.fit";
    t.importFile(t.parameters.input);
    System.out.println("----");
    t.data = null;

    t.parameters.input = "/tmp/ywzAbNOG.fit";
    t.importFile(t.parameters.input);
    System.out.println("----");
    t.data = null;
     */
  }

  private void decodeFile() throws ImportFailedException
  {
    try
    {
      System.out.printf("FIT Decode Example Application - Protocol %d.%d Profile %.2f %s\n",
              com.garmin.fit.Fit.PROTOCOL_VERSION_MAJOR, com.garmin.fit.Fit.PROTOCOL_VERSION_MINOR,
              com.garmin.fit.Fit.PROFILE_VERSION / 100.0, com.garmin.fit.Fit.PROFILE_TYPE);
      System.out.println(this.parameters.input);

      Decode decoder = new Decode();
      InputStream inputStream = Files.newInputStream(Paths.get(this.parameters.input));

      if (!decoder.checkFileIntegrity(inputStream))
      {
        throw new RuntimeException("FIT file integrity failed.");
      }
      inputStream.close();

      fetchFileInfo();

      Listener listener = new Listener(new Data());
      MesgBroadcaster mesgBroadcaster = new MesgBroadcaster(decoder);
      mesgBroadcaster.addListener((FileIdMesgListener)listener);
      mesgBroadcaster.addListener((UserProfileMesgListener)listener);
      mesgBroadcaster.addListener((DeviceInfoMesgListener)listener);
      mesgBroadcaster.addListener((MonitoringMesgListener)listener);
      mesgBroadcaster.addListener((RecordMesgListener)listener);
      mesgBroadcaster.addListener((ActivityMesgListener)listener);
      mesgBroadcaster.addListener((CapabilitiesMesgListener)listener);
      mesgBroadcaster.addListener((ConnectivityMesgListener)listener);
      mesgBroadcaster.addListener((DeviceSettingsMesgListener)listener);
      mesgBroadcaster.addListener((DeviceInfoMesgListener)listener);
      mesgBroadcaster.addListener((FieldCapabilitiesMesgListener)listener);
      mesgBroadcaster.addListener((GpsMetadataMesgListener)listener);
      mesgBroadcaster.addListener((LapMesgListener)listener);
      mesgBroadcaster.addListener((WorkoutMesgListener) listener);
      mesgBroadcaster.addListener((WorkoutSessionMesgListener) listener);
      mesgBroadcaster.addListener((SessionMesgListener) listener);
      mesgBroadcaster.addListener((SportMesgListener) listener);
      mesgBroadcaster.addListener((TotalsMesgListener) listener);

      decoder.addListener((DeveloperFieldDescriptionListener)listener);

//      decodePass(Files.newInputStream(Paths.get(this.parameters.input)), decoder, mesgBroadcaster);

      // System.out.println(data);
      // System.out.println(data.dataEntries);
    }
    catch( IOException e )
    {
      throw new ImportFailedException(e);
    }
  }

  private void fetchFileInfo() throws ImportFailedException
  {
    try
    {
      Decode decoder = new Decode();
      InputStream inputStream = Files.newInputStream(Paths.get(this.parameters.input));
      Data data = new Data();
      data.timeStart = Long.MAX_VALUE;

      if (!decoder.checkFileIntegrity(inputStream))
      {
        throw new RuntimeException("FIT file integrity failed.");
      }
      inputStream.close();

      SessionInfoExtractor sessionInfoExtractor = new SessionInfoExtractor(data);
      Decode sessionInfoDecoder = new Decode();
      MesgBroadcaster mesgBroadcaster2 = new MesgBroadcaster(sessionInfoDecoder);
      mesgBroadcaster2.addListener((SessionMesgListener) sessionInfoExtractor);
      mesgBroadcaster2.addListener((RecordMesgListener) sessionInfoExtractor);

      decodePass(Files.newInputStream(Paths.get(this.parameters.input)), decoder, mesgBroadcaster2);

      // Now calculate the correct time
//      data.timeStart = (long) (data.timeStart - data.tzServer*HALFHOUR);

      if( sessionInfoExtractor.sport == Sport.CYCLING )
      {
        data.sport = at.pegasos.integrations.files.Sport.CYCLING;
        if( sessionInfoExtractor.subSport == SubSport.INDOOR_CYCLING )
          data.isIndoor = true;
      }
      else if( sessionInfoExtractor.sport == Sport.RUNNING )
      {
        data.sport = at.pegasos.integrations.files.Sport.RUNNING;
      }
      this.data = data;
    }
    catch( IOException e )
    {
      throw new ImportFailedException(e);
    }
  }

  private void extractData() throws IOException, ImportFailedException
  {
    DataExtractor dataExtractor = new DataExtractor(data);
    Decode dataDecoder = new Decode();
    MesgBroadcaster mesgBroadcaster3 = new MesgBroadcaster(dataDecoder);
    mesgBroadcaster3.addListener((RecordMesgListener) dataExtractor);
    mesgBroadcaster3.addListener((LapMesgListener) dataExtractor);

    decodePass(Files.newInputStream(Paths.get(this.parameters.input)), dataDecoder, mesgBroadcaster3);
    System.out.println("After extractData " + new Date((long) (data.timeLastEntry)));
  }

  private static void decodePass(InputStream inputStream, Decode decoder, MesgBroadcaster mesgBroadcaster) throws IOException, ImportFailedException
  {
    try {
      decoder.read(inputStream, mesgBroadcaster, mesgBroadcaster);
    }
    catch (FitRuntimeException e)
    {
      // If a file with 0 data size in it's header  has been encountered,
      // attempt to keep processing the file
      if (decoder.getInvalidFileDataSize())
      {
        decoder.nextFile();
        decoder.read(inputStream, mesgBroadcaster, mesgBroadcaster);
      }
      else
      {
        System.err.print("Exception decoding file: ");
        System.err.println(e.getMessage());

        inputStream.close();

        throw new ImportFailedException(e);
      }
    }

    inputStream.close();
  }

  public void importFile(String fn) throws ImportFailedException
  {
    try
    {
      if( data == null )
        fetchFileInfo();
      extractData();

      long end = data.timeLastEntry + (long) (data.tzServer * HALFHOUR);
      System.out.println("check " + new Date(data.timeStart) + " " + new Date(data.timeLastEntry) + " " + new Date(end));
      System.out.println(data.dataEntries.get(data.dataEntries.size() - 1));
      System.out.println(new Date(data.dataEntries.get(data.dataEntries.size() - 1).rec_time));
      if( checkCreateSession(data.timeStart, data.timeLastEntry) )
      {
        doImport();
        if( !parameters.noPost )
        {
          runPost();
        }
      }
      else
        log.debug("Session already inserted");
    }
    catch( Exception e )
    {
      throw new ImportFailedException(e);
    }
  }

  private void doImport() throws SQLException
  {
    if( data.sport != null )
    {
      insertSetting(data.timeStart, "Sport", data.sportToSetting());
    }

    for(DataEntry entry : this.data.markers)
    {
      if( entry.rec_time > this.data.timeStart && entry.rec_time < this.data.timeLastEntry )
      {
        insertEntry(entry);
      }
    }
    for(DataEntry entry : data.dataEntries)
    {
      insertEntry(entry);
    }

    performPendingInserts();

    insertImport();

    persist();
  }

  public Long getStartDate()
  {
    if ( data == null )
    {
      try
      {
        fetchFileInfo();
        if( data != null )
          return data.timeStart;
        else
          throw new ImportFailedException(new IllegalStateException());
      }
      catch (ImportFailedException e)
      {
        throw new RuntimeException(e);
      }
    }
    return data.timeStart;
  }

  private static class Listener implements FileIdMesgListener, UserProfileMesgListener, MonitoringMesgListener,
          RecordMesgListener, DeveloperFieldDescriptionListener, ActivityMesgListener, CapabilitiesMesgListener,
          ConnectivityMesgListener, DeviceSettingsMesgListener, DeviceInfoMesgListener, FieldCapabilitiesMesgListener,
          GpsMetadataMesgListener, LapMesgListener, WorkoutMesgListener, WorkoutSessionMesgListener, SessionMesgListener,
          SportMesgListener, TotalsMesgListener {

    private final Data data;
    public Listener(Data data)
    {
      this.data = data;
    }

    @Override
    public void onMesg(FileIdMesg mesg) {
      System.out.println("File ID:");

      if (mesg.getType() != null) {
        System.out.print("   Type: ");
        System.out.println(mesg.getType().getValue());
      }

      if (mesg.getManufacturer() != null ) {
        System.out.print("   Manufacturer: ");
        System.out.println(mesg.getManufacturer());
      }

      if (mesg.getProduct() != null) {
        System.out.print("   Product: ");
        System.out.println(mesg.getProduct());
      }

      if (mesg.getSerialNumber() != null) {
        System.out.print("   Serial Number: ");
        System.out.println(mesg.getSerialNumber());
      }

      if (mesg.getNumber() != null) {
        System.out.print("   Number: ");
        System.out.println(mesg.getNumber());
      }
    }

    @Override
    public void onMesg(UserProfileMesg mesg) {
      System.out.println("User profile:");

      if (mesg.getFriendlyName() != null) {
        System.out.print("   Friendly Name: ");
        System.out.println(mesg.getFriendlyName());
      }

      if (mesg.getGender() != null) {
        if (mesg.getGender() == Gender.MALE) {
          System.out.println("   Gender: Male");
        } else if (mesg.getGender() == Gender.FEMALE) {
          System.out.println("   Gender: Female");
        }
      }

      if (mesg.getAge() != null) {
        System.out.print("   Age [years]: ");
        System.out.println(mesg.getAge());
      }

      if (mesg.getWeight() != null) {
        System.out.print("   Weight [kg]: ");
        System.out.println(mesg.getWeight());
      }
    }

    @Override
    public void onMesg(DeviceInfoMesg mesg) {
      System.out.println("Device info:");

      if (mesg.getTimestamp() != null)
      {
        System.out.print("   Timestamp: ");
        System.out.println(mesg.getTimestamp());
      }

      if (mesg.getBatteryStatus() != null) {
        System.out.print("   Battery status: ");

        switch (mesg.getBatteryStatus()) {
          case BatteryStatus.CRITICAL:
            System.out.println("Critical");
            break;
          case BatteryStatus.GOOD:
            System.out.println("Good");
            break;
          case BatteryStatus.LOW:
            System.out.println("Low");
            break;
          case BatteryStatus.NEW:
            System.out.println("New");
            break;
          case BatteryStatus.OK:
            System.out.println("OK");
            break;
          default:
            System.out.println("Invalid");
            break;
        }
      }

      for(Field field : mesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(MonitoringMesg mesg) {
      System.out.println("Monitoring:");

      if (mesg.getTimestamp() != null) {
        System.out.print("   Timestamp: ");
        System.out.println( mesg.getTimestamp());
      }

      if (mesg.getActivityType() != null) {
        System.out.print("   Activity Type: ");
        System.out.println(mesg.getActivityType());
      }

      // Depending on the ActivityType, there may be Steps, Strokes, or Cycles present in the file
      if (mesg.getSteps() != null) {
        System.out.print("   Steps: ");
        System.out.println( mesg.getSteps());
      } else if (mesg.getStrokes() != null) {
        System.out.print("   Strokes: ");
        System.out.println(mesg.getStrokes());
      } else if (mesg.getCycles() != null) {
        System.out.print("   Cycles: ");
        System.out.println(mesg.getCycles());
      }

      printDeveloperData(mesg);
    }

    @Override
    public void onMesg(RecordMesg mesg) {
      System.out.println("Record:" + mesg.getTimestamp());

      printValues(mesg, RecordMesg.PositionLatFieldNum);
      printValues(mesg, RecordMesg.PositionLongFieldNum);
      printValues(mesg, RecordMesg.HeartRateFieldNum);
      printValues(mesg, RecordMesg.CadenceFieldNum);
      printValues(mesg, RecordMesg.DistanceFieldNum);
      printValues(mesg, RecordMesg.SpeedFieldNum);
      printValues(mesg, RecordMesg.GradeFieldNum);

      printDeveloperData(mesg);

      for(Field field : mesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    private void printDeveloperData(Mesg mesg) {
      for (DeveloperField field : mesg.getDeveloperFields()) {
        if (field.getNumValues() < 1) {
          continue;
        }

        if (field.isDefined()) {
          System.out.print("   " + field.getName());

          if (field.getUnits() != null) {
            System.out.print(" [" + field.getUnits() + "]");
          }

          System.out.print(": ");
        } else {
          System.out.print("   Undefined Field: ");
        }

        System.out.print(field.getValue( 0 ));
        for (int i = 1; i < field.getNumValues(); i++) {
          System.out.print("," + field.getValue(i));
        }

        System.out.println();
      }
    }

    @Override
    public void onDescription(DeveloperFieldDescription desc) {
      System.out.println("New Developer Field Description");
      System.out.println("   App Id: " + desc.getApplicationId());
      System.out.println("   App Version: " + desc.getApplicationVersion());
      System.out.println("   Field Num: " + desc.getFieldDefinitionNumber());
    }

    private void printValues(Mesg mesg, int fieldNum) {
      Iterable<FieldBase> fields = mesg.getOverrideField((short) fieldNum);
      Field profileField = Factory.createField(mesg.getNum(), fieldNum);
      boolean namePrinted = false;

      if (profileField == null) {
        return;
      }

      for (FieldBase field : fields) {
        if (!namePrinted) {
          System.out.println("   " + profileField.getName() + ":");
          namePrinted = true;
        }

        if (field instanceof Field) {
          System.out.println("      native: " + field.getValue());
        } else {
          System.out.println("      override: " + field.getValue());
        }
      }
    }

    @Override
    public void onMesg(ActivityMesg activityMesg) {
      System.out.println(activityMesg + " " + activityMesg.getTimestamp());
      System.out.println(" " + activityMesg.getName());
      System.out.println(" " + activityMesg.getType());
      System.out.println(" " + activityMesg.getEvent());
      System.out.println(" " + activityMesg.getEvent().getValue());
      System.out.println(" " + activityMesg.getEventGroup());
      System.out.println(" " + activityMesg.getEventType());
      System.out.println(" " + activityMesg.getLocalTimestamp());
      System.out.println(" " + activityMesg.getNumSessions());
      System.out.println(" " + activityMesg.getNum());
      System.out.println(" " + activityMesg.getNumFields());
      for(Field field : activityMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(CapabilitiesMesg capabilitiesMesg) {
      System.out.println(capabilitiesMesg);
    }

    @Override
    public void onMesg(ConnectivityMesg connectivityMesg) {
      System.out.println(connectivityMesg);
    }

    @Override
    public void onMesg(DeviceSettingsMesg deviceSettingsMesg) {
      System.out.println(deviceSettingsMesg);
    }

    @Override
    public void onMesg(FieldCapabilitiesMesg fieldCapabilitiesMesg) {
      System.out.println(fieldCapabilitiesMesg);
    }

    @Override
    public void onMesg(GpsMetadataMesg gpsMetadataMesg) {
      System.out.println(gpsMetadataMesg);
    }

    @Override
    public void onMesg(LapMesg lapMesg) {
      System.out.println(lapMesg);
      for(Field field : lapMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(WorkoutMesg workoutMesg) {
      System.out.println(workoutMesg);
      System.out.println(" " + workoutMesg.getName());
      System.out.println(" " + workoutMesg.getNum());
      System.out.println(" " + workoutMesg.getNumFields());
      for(Field field : workoutMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(WorkoutSessionMesg workoutSessionMesg) {
      System.out.println(workoutSessionMesg);
      System.out.println(" " + workoutSessionMesg.getName());
      System.out.println(" " + workoutSessionMesg.getNum());
      System.out.println(" " + workoutSessionMesg.getNumFields());
      for(Field field : workoutSessionMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(TotalsMesg totalsMesg) {
      System.out.println(totalsMesg);
      System.out.println(" " + totalsMesg.getName());
      System.out.println(" " + totalsMesg.getNum());
      System.out.println(" " + totalsMesg.getNumFields());
      for(Field field : totalsMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(SportMesg sportMesg) {
      System.out.println(sportMesg);
      System.out.println(" " + sportMesg.getName());
      System.out.println(" " + sportMesg.getNum());
      System.out.println(" " + sportMesg.getNumFields());
      for(Field field : sportMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }

    @Override
    public void onMesg(SessionMesg sessionMesg) {
      System.out.println(sessionMesg + " " + sessionMesg.getTimestamp());
      System.out.println(" " + sessionMesg.getName());
      System.out.println(" " + sessionMesg.getNum());
      System.out.println(" " + sessionMesg.getNumFields());
      for(Field field : sessionMesg.getFields())
      {
        System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
      }
    }
  }

  private static class SessionInfoExtractor implements SessionMesgListener, RecordMesgListener {

    private final Data data;

    Sport sport;
    SubSport subSport;

    public SessionInfoExtractor(Data data)
    {
      this.data = data;
    }

    @Override
    public void onMesg(SessionMesg sessionMesg)
    {
      /* DateTimeFormatter dateTimeZoneDtf = DateTimeFormatter.ofPattern("e MM dd HH:mm:ss z yyyy");
        // parse them to objects that consider time zones
        ZonedDateTime delZdt = ZonedDateTime.parse(mesg.getTimestamp(), dateTimeZoneDtf); */

      long ts = Data.fitToTimestamp(sessionMesg.getTimestamp().getTimestamp());
      int tzServer = TimeZone.getDefault().getOffset(ts) / 1000 / 60 / 60 * 2;
      this.data.tzLocal = tzServer;
      double HALFHOUR =  1000 * 60 * 30;
      this.data.tzServer = tzServer;

      this.sport = sessionMesg.getSport();
      this.subSport = sessionMesg.getSubSport();

      this.data.dataEntries = new ArrayList<>((int) (sessionMesg.getTotalElapsedTime() * 2));
      this.data.markers = new ArrayList<>();
//      this.data.dataEntries = new ArrayList<>();
    }
    @Override
    public void onMesg(RecordMesg recordMesg)
    {
      long ts = Data.fitToTimestamp(recordMesg.getTimestamp().getTimestamp());
      if (ts < data.timeStart)
      {
        data.timeStart = ts;
      }
    }
  }

  private static class DataExtractor implements RecordMesgListener, LapMesgListener {
    private final Data data;
    public DataExtractor(Data data)
    {
      this.data = data;
    }

    @Override
    public void onMesg(LapMesg lapMesg)
    {
      long timestamp = Data.fitToTimestamp(lapMesg.getTimestamp().getTimestamp());
      this.data.markers.add(new Marker(timestamp));
    }

    @Override
    public void onMesg(RecordMesg recordMesg)
    {
//      System.out.println(recordMesg + " " + recordMesg.getNumFields() + " " + recordMesg.hasField(RecordMesg.PositionLatFieldNum));

      /*
      heart_rate bpm 107
  distance m 72270.7
  speed m/s 6.139
  cadence rpm 27
  altitude m 191.79999999999995
  temperature C 29
  power watts 150
  position_lat semicircles 574813170
  position_long semicircles 195057431
  timestamp s 1030614921
  enhanced_speed m/s 6.139
  enhanced_altitude m 191.79999999999995
       */

      long ts = Data.fitToTimestamp(recordMesg.getTimestamp().getTimestamp());
      if( ts > data.timeLastEntry )
      {
        data.timeLastEntry = ts;
        /*System.out.println("Update last ts to " + ts);
        for(Field field : recordMesg.getFields())
        {
          System.out.println("  " + field.getName() + " " + field.getUnits() + " " + field.getValue());
        }*/
      }

      if( recordMesg.hasField(RecordMesg.HeartRateFieldNum) )
      {
        this.data.dataEntries.add(new Heartrate(ts, recordMesg.getHeartRate()));
      }

      if( recordMesg.hasField(RecordMesg.PositionLatFieldNum) && recordMesg.hasField(RecordMesg.PositionLongFieldNum) )
      {
        Position position = new Position(ts,
                SemicirclesConverter.semicirclesToDegrees(recordMesg.getPositionLat()),
                SemicirclesConverter.semicirclesToDegrees(recordMesg.getPositionLong()));
        this.data.dataEntries.add(position);
        if( recordMesg.hasField(RecordMesg.AltitudeFieldNum) )
          position.alt_cm = (int) (recordMesg.getAltitude() * 100);
        if( recordMesg.hasField(RecordMesg.SpeedFieldNum) )
          position.speed_cm_s = (int) (recordMesg.getSpeed() * 100);
      }
      Ambience ambience = null;
      if( recordMesg.hasField(RecordMesg.TemperatureFieldNum) )
      {
        ambience = new Ambience(ts);
        ambience.temperature = recordMesg.getTemperature();
        this.data.dataEntries.add(ambience);
      }
      if( recordMesg.hasField(RecordMesg.AbsolutePressureFieldNum) )
      {
        if( ambience == null )
        {
          ambience = new Ambience(ts);
          this.data.dataEntries.add(ambience);
        }
        ambience.pressure = Math.toIntExact(recordMesg.getAbsolutePressure());
      }

      if( data.sport != null )
      switch(data.sport)
      {
        case RUNNING:
          if( recordMesg.hasField(RecordMesg.CadenceFieldNum) )
          {
            Running running = new Running(ts,
                    // speed
                    (int) (recordMesg.getSpeed() * 1000),
                    // distance
                    recordMesg.getDistance().intValue(),
                    recordMesg.getCadence());
            this.data.dataEntries.add(running);
          }
          break;
        case CYCLING:
          Bike bike = new Bike(ts);
          boolean used = false;
          if( recordMesg.hasField(RecordMesg.LeftRightBalanceFieldNum) )
          {
            System.out.println(recordMesg.getLeftRightBalance());
          }
          if( recordMesg.hasField(RecordMesg.PowerFieldNum) )
          {
            used = true;
            bike.power = recordMesg.getPower().shortValue();
          }
          if( recordMesg.hasField(RecordMesg.CadenceFieldNum) )
          {
            used = true;
            bike.cadence = recordMesg.getCadence();
          }
          if( recordMesg.hasField(RecordMesg.DistanceFieldNum) )
          {
            used = true;
            bike.distance_m = recordMesg.getDistance().intValue();
          }
          if( recordMesg.hasField(RecordMesg.SpeedFieldNum) )
          {
            used = true;
            bike.speed_mm_s = (int) (recordMesg.getSpeed() * 1000);
          }
          if( used )
            this.data.dataEntries.add(bike);
          break;
      }
    }
  }
}
