package at.pegasos.integrations.files;

import at.pegasos.integrations.files.ImportTool.Parameters;
import at.pegasos.tool.util.*;
import com.beust.jcommander.*;
import lombok.extern.slf4j.*;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;

@Slf4j
public class ImportFolder {

  private final Parameters parameters;
  private final Ser importSer;
  private final Tcx importTcx;
  private final Pwx importPwx;
  private final Fit importFit;
  protected List<Entry> files = new ArrayList<>();

  public ImportFolder(ImportTool.Parameters parameters) throws MalformedURLException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, SQLException
  {
    this.parameters = parameters;

    this.importSer = new Ser();
    this.importTcx = new Tcx();
    this.importPwx = new Pwx();
    this.importFit = new Fit();
  }

  public static void main(String[] args) throws ImportFailedException, MalformedURLException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, SQLException
  {
    Parameters parameters = new ImportTool.Parameters();
    JCommander.newBuilder()
            .addObject(parameters)
            .build()
            .parse(args);

    new ImportFolder(parameters).runImport();
  }

  static boolean checkMeta(Metadata meta)
  {
    return meta.hasKey("Param0") && meta.hasKey("Param1") && meta.hasKey("Param2") && meta.hasKey("User");
  }

  public void runImport() throws ImportFailedException
  {
    log.debug(parameters.toString());
    fetchFiles();

    files.sort(Comparator.comparing(Entry::getStartDate));

    log.info("Start importing {} files", files.size());
    for(Entry file : files)
    {
      try
      {
        importFile(file);
        log.info("Importing {} finished", file);
      } catch( ImportFailedException e )
      {
        e.printStackTrace();
      }
    }
  }

  private void importFile(Entry file) throws ImportFailedException
  {
    Parameters copy = parameters.copy();
    copy.input = file.file.toAbsolutePath().toString();
    file.importer.setParameters(copy);
    file.importer.importFile(file.file.toString());
  }

  private void fetchFilesFromDir(File folder) throws ImportFailedException
  {
    log.debug("listFiles: {}", Arrays.toString(Objects.requireNonNull(folder.listFiles())));
    for(final File fileEntry : Objects.requireNonNull(folder.listFiles()))
    {
      log.debug("fileEntry: {} {}", fileEntry, fileEntry.canRead());
      if( !fileEntry.isDirectory() )
      {
        if( fileEntry.getName().toLowerCase().endsWith(".ser") )
        {
          Path path = Paths.get(fileEntry.getAbsolutePath());
          Path parent = path.toAbsolutePath().getParent();
          String fn = path.getFileName().toString();
          fn = fn.substring(0, fn.length() - 4);

          if( Files.exists(parent.resolve(fn + ".meta")) )
          {
            // System.out.println(fileEntry + " has meta");
            try
            {
              Metadata meta = MetadataParser.parse(Files.readAllLines(parent.resolve(fn + ".meta")));
              if( checkMeta(meta) )
                addFile(path, meta);
              else
                log.error("Could not add file " + fileEntry + " due to error in metadata");
            } catch( IOException e )
            {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
          else
            log.warn("{} has no meta", fileEntry);
        }
        else if( fileEntry.getName().toLowerCase().endsWith(".tcx") )
        {
          Path path = Paths.get(fileEntry.getAbsolutePath());
          addFileTcx(path);
        }
        else if( fileEntry.getName().toLowerCase().endsWith(".pwx") )
        {
          Path path = Paths.get(fileEntry.getAbsolutePath());
          addFilePwx(path);
        }
        else if( fileEntry.getName().toLowerCase().endsWith(".fit") )
        {
          Path path = Paths.get(fileEntry.getAbsolutePath());
          addFileFit(path);
        }
      }
      else if( fileEntry.canRead() )
      {
        fetchFilesFromDir(fileEntry);
      }
    }
  }

  private void fetchFiles() throws ImportFailedException
  {
    // TODO: nullpointer exception when parameters.dir does not exist
    final File folder = new File(parameters.input);
    log.debug("Folder {}", folder);
    fetchFilesFromDir(folder);
  }

  private void addFile(Path path, Metadata meta)
  {
    SerEntry e = new SerEntry();
    e.importer = importSer;
    e.file = path;
    e.startdate = Long.parseLong(meta.getValue("Starttime"));
    e.meta = meta;
    files.add(e);
  }

  private void addFileTcx(Path path) throws ImportFailedException
  {
    TcxEntry e = new TcxEntry();
    e.importer = importTcx;
    e.file = path;
    e.startdate = Tcx.startTime(path.toString());
    files.add(e);
  }

  private void addFilePwx(Path path) throws ImportFailedException
  {
    PwxEntry e = new PwxEntry();
    e.importer = importPwx;
    e.file = path;
    e.startdate = Pwx.startTime(path.toString());
    files.add(e);
  }

  private void addFileFit(Path path) throws ImportFailedException
  {
    FitEntry e = new FitEntry();
    try
    {
      // here the implementation is different to the other imports. Fit import is stateful
      e.importer = new Fit();
      e.importer.setParameters(parameters);
    }
    catch (SQLException ex)
    {
      throw new ImportFailedException(ex);
    }
    e.file = path;
    files.add(e);
  }

  private static abstract class Entry {
    public Path file;
    public abstract Long getStartDate();
    public FileImportTool importer;
  }

  private static abstract class EntryWithSimpleStartDate extends Entry {
    public Long startdate;
    public Long getStartDate()
    {
      return startdate;
    }
  }

  private static class SerEntry extends EntryWithSimpleStartDate {
    public Metadata meta;
  }

  private static class TcxEntry extends EntryWithSimpleStartDate {
  }

  private static class PwxEntry extends EntryWithSimpleStartDate {
  }
  private static class FitEntry extends Entry {
    @Override
    public Long getStartDate()
    {
      return ((Fit) importer).getStartDate();
    }
  }
}
