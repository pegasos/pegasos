package at.pegasos.integrations.files;

public class ImportFailedException extends Exception {

  public ImportFailedException(Exception e)
  {
    super(e);
  }
}
