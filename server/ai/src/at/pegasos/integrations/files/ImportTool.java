package at.pegasos.integrations.files;

import com.beust.jcommander.*;

import org.slf4j.*;

import java.sql.*;

import activities.*;
import at.pegasos.ai.*;
import at.pegasos.ai.util.*;
import at.pegasos.ai.util.log.*;
import at.pegasos.integrations.files.data.*;
import at.pegasos.integrations.files.data.Marker;
import at.univie.mma.ai.postprocessing.*;
import lombok.*;

public class ImportTool {

  @ToString
  public static class Parameters {
    @Parameter(names = "--help", help = true)
    public boolean help;

    @Parameter(names = {"-u", "--user"}, description = "User (id) to be used during the imported (forced for all files). Some tools might require this setting", required = false)
    public Long userId;

    @Parameter(names = {"--deter-group"}, description = "Determine the group of the user to be used for the input", required = false)
    public boolean determineGroup;

    @Parameter(names = "--no-post", description = "Do not run post-processor after import")
    public boolean noPost = false;

    @Parameter(names = "--no-save", description = "Do not store information about the import i.e. do not create an import entry")
    public boolean noSave = false;

    @Parameter(description = "Input (what to import)")
    public String input;

    public Parameters copy()
    {
      Parameters copy = new Parameters();
      copy.userId = this.userId;
      copy.determineGroup = this.determineGroup;
      copy.noPost = this.noPost;
      copy.noSave = this.noSave;
      copy.input = this.input;
      return copy;
    }
  }

  protected Database db;
  /**
   * Team tools for this import.
   */
  protected TeamTools teamTools;

  protected Parameters parameters;

  private PreparedStatement ambience_insert;
  private PreparedStatement bike_insert;
  private PreparedStatement gps_insert;
  private PreparedStatement hr_insert;
  //  private PreparedStatement temp_insert;
  private PreparedStatement setting_insert;
  private PreparedStatement marker_insert;
  private long sessionId;
  private PreparedStatement session_insert;
  protected PreparedStatement import_data;
  private PreparedStatement fp_insert;

  public void setup() throws SQLException
  {
    if( !LoggerSetup.isSetup() )
    {
      LoggerSetup.setupLogging(-1);
      LoggerFactory.getLogger(this.getClass().getName()).info("Starting post processor from main / commandline");
    }

    db = Database.getInstance();
    db.setAutoCommit(false);

    teamTools = TeamTools.create(db);

    this.session_insert = this.db.prepareStatement("insert into session(userid,teamid,groupid,starttime,endtime,last_update, activity, param1, param2, ai) values (?,?,?,?,?,?, 1, 0, 0, 0)", Statement.RETURN_GENERATED_KEYS);
    this.import_data = this.db.prepareStatement("insert into imported_session(session_id,rec_time,type,data) values (?,?,?,?)");
    this.ambience_insert = this.db.prepareStatement("insert into ambience(session_id,rec_time,temperature,pressure) values (?,?,?,?)");
    this.bike_insert = this.db.prepareStatement("insert into bike(session_id,rec_time,speed_mm_s,distance_m,cadence,power,sensor_nr) values (?,?,?,?,?,?,?)");
    this.fp_insert = this.db.prepareStatement("insert into foot_pod(session_id,rec_time,speed_mm_s,distance_m,strides_255,calories, cadence) values (?,?,?,?,?,0,?)");
    this.gps_insert = this.db.prepareStatement("insert into gps(session_id,rec_time,lat,lon,alt_cm,speed_cm_s,acc,bearing) values (?,?,?,?,?,?,?,?)");
    this.hr_insert = this.db.prepareStatement("insert into hr(session_id,rec_time,hr, hr_var) values (?,?,?, 0)");
//    this.temp_insert= this.db.prepareStatement("insert into ambience(session_id,rec_time,temperature,pressure) values (?,?,?,?)");
    this.setting_insert = this.db.prepareStatement("insert into setting(session_id,rec_time,name,value) values (?,?,?,?)");
    this.marker_insert = this.db.prepareStatement("insert into marker(session_id,rec_time) values (?,?)");
  }

  /**
   * Check if a session with the given parameters (userId, start, end) exists. A delta of +/- one
   * minute s applied for the check.
   *
   * @param userId user for whom the session should exist
   * @param start  start time (as unix time stamp)
   * @param end    end time as unix time stamp
   * @return true if session exists
   * @throws SQLException when database problems occur
   */
  public boolean isInserted(long userId, long start, long end) throws SQLException
  {
    // check if we have a matching session
    PreparedStatement findSession = db.createStatementRO("select session_id from session where userid = ? and ABS(starttime - ?) < 60000 AND ABS(endtime - ?) < 60000");

    findSession.setLong(1, userId);
    findSession.setLong(2, start);
    findSession.setLong(3, end);
    findSession.execute();

    // if we have it --> use the sessionId
    ResultSet res = findSession.getResultSet();
    if( res.first() )
    {
      sessionId = res.getLong(1);

      return true;
    }
    else
      return false;
  }

  /**
   * Check if a corresponding session exists. If not a new session is created.
   * This method respects the parameters (userId, determineGroup)
   *
   * @param start star of the session
   * @param end   end time of the session
   * @return true if a new session was created
   * @throws SQLException when something goes wrong with the db
   */
  protected boolean checkCreateSession(long start, long end) throws SQLException
  {
    if( !isInserted(parameters.userId, start, end) )
    {
      if( parameters.determineGroup )
      {
        TeamTools.Group group = teamTools.getDefaultGroup(parameters.userId);
        createSession(parameters.userId, group.team_id, group.group_id, start, end);
      }
      else
      {
        createSession(parameters.userId, null, null, start, end);
      }

      return true;
    }
    return false;
  }

  /**
   * Create a new session
   *
   * @param userId  user for whom the session should be created
   * @param teamid  can be 0 for no team
   * @param groupid can be 0 for no group
   * @param start   timestamp for the start of the session
   * @param end     timestamp for the end of the session
   * @throws SQLException when something goes wrong with the db
   */
  public void createSession(long userId, Long teamid, Long groupid, long start, long end) throws SQLException
  {
    session_insert.setLong(1, userId);
    if( teamid != null )
      session_insert.setLong(2, teamid);
    else
      session_insert.setNull(2, Types.BIGINT);
    if( groupid != null )
      session_insert.setLong(3, groupid);
    else
      session_insert.setNull(3, Types.BIGINT);
    session_insert.setLong(4, start);
    session_insert.setLong(5, end);
    session_insert.setLong(6, end);

    session_insert.executeUpdate();
    ResultSet res = session_insert.getGeneratedKeys();
    res.next();
    sessionId = res.getLong(1);
  }

  protected void insertEntry(DataEntry entry) throws SQLException
  {
    if( entry instanceof Ambience )
      insertAmbience((Ambience) entry);
    else if( entry instanceof Bike)
      insertBike((Bike) entry);
    else if( entry instanceof Heartrate)
      insertHr((Heartrate) entry);
    else if( entry instanceof Marker)
      insertMarker((Marker) entry);
    else if( entry instanceof Position)
      insertPosition((Position) entry);
    else if( entry instanceof Running)
      insertFootpod((Running) entry);
  }

  protected void insertAmbience(Ambience ambience) throws SQLException
  {
    ambience_insert.setLong(1, sessionId);
    ambience_insert.setLong(2, ambience.rec_time);
    // TODO:
    /*if( ambience.temperature != 0 )
      ambience_insert.setInt(3, ambience.temperature);
    else
      ambience_insert.setNull(3, Types.INTEGER);
    if( ambience.distance_m != null )
      ambience_insert.setInt(4, ambience.distance_m);
    else
      ambience_insert.setNull(4, Types.INTEGER);*/
    ambience_insert.setInt(3, ambience.temperature);
    ambience_insert.setInt(4, ambience.pressure);
  }

  public void insertBike(long time, Integer speed, Integer distance, Integer cadence, Short power) throws SQLException
  {
    bike_insert.setLong(1, sessionId);
    bike_insert.setLong(2, time);
    if( speed != null )
      bike_insert.setInt(3, speed);
    else
      bike_insert.setNull(3, Types.INTEGER);
    if( distance != null )
      bike_insert.setInt(4, distance);
    else
      bike_insert.setNull(4, Types.INTEGER);
    if( cadence != null )
      bike_insert.setInt(5, cadence);
    else
      bike_insert.setNull(5, Types.INTEGER);
    if( power != null )
      bike_insert.setInt(6, power);
    else
      bike_insert.setNull(6, Types.SMALLINT);

    // set sensor number to null
    bike_insert.setNull(7, Types.INTEGER);

    if( speed != null || distance != null || cadence != null || power != null )
      bike_insert.addBatch();
  }

  public void insertBike(Bike bike) throws SQLException
  {
    bike_insert.setLong(1, sessionId);
    bike_insert.setLong(2, bike.rec_time);
    if( bike.speed_mm_s != null )
      bike_insert.setInt(3, bike.speed_mm_s);
    else
      bike_insert.setNull(3, Types.INTEGER);
    if( bike.distance_m != null )
      bike_insert.setInt(4, bike.distance_m);
    else
      bike_insert.setNull(4, Types.INTEGER);
    if( bike.cadence != null )
      bike_insert.setInt(5, bike.cadence);
    else
      bike_insert.setNull(5, Types.INTEGER);
    if( bike.power != null )
      bike_insert.setInt(6, bike.power);
    else
      bike_insert.setNull(6, Types.SMALLINT);

    // set sensor number to null
    bike_insert.setNull(7, Types.INTEGER);

    bike_insert.addBatch();
  }

  public void insertFootpod(long time, Integer speed, Integer distance, Integer strides, Short cadence) throws SQLException
  {
    fp_insert.setLong(1, sessionId);
    fp_insert.setLong(2, time);
    if( speed != null )
      fp_insert.setInt(3, speed);
    else
      fp_insert.setNull(3, Types.INTEGER);
    if( distance != null )
      fp_insert.setInt(4, distance);
    else
      fp_insert.setNull(4, Types.INTEGER);
    if( strides != null )
      fp_insert.setInt(5, strides);
    else
      fp_insert.setNull(5, Types.INTEGER);
    if( cadence != null )
      fp_insert.setInt(6, cadence);
    else
      fp_insert.setNull(6, Types.INTEGER);

    if( speed != null || distance != null || strides != null || cadence != null )
      fp_insert.addBatch();
  }

  public void insertFootpod(Running running) throws SQLException
  {
    fp_insert.setLong(1, sessionId);
    fp_insert.setLong(2, running.rec_time);
    if( running.speed_mm_s != null )
      fp_insert.setInt(3, running.speed_mm_s);
    else
      fp_insert.setNull(3, Types.INTEGER);
    if( running.distance_m != null )
      fp_insert.setInt(4, running.distance_m);
    else
      fp_insert.setNull(4, Types.INTEGER);
    if( running.strides_255 != null )
      fp_insert.setInt(5, running.strides_255);
    else
      fp_insert.setNull(5, Types.INTEGER);
    if( running.cadence != null )
      fp_insert.setInt(6, running.cadence);
    else
      fp_insert.setNull(6, Types.INTEGER);

    fp_insert.addBatch();
  }

  public void insertSetting(long time, String setting, String value) throws SQLException
  {
    setting_insert.setLong(1, sessionId);
    setting_insert.setLong(2, time);
    setting_insert.setString(3, setting);
    setting_insert.setString(4, value);
    setting_insert.executeUpdate();
  }

  public void insertHr(long time, int hr) throws SQLException
  {
    hr_insert.setLong(1, sessionId);
    hr_insert.setLong(2, time);
    hr_insert.setInt(3, hr);
    hr_insert.addBatch();
  }

  public void insertHr(Heartrate heartrate) throws SQLException
  {
    hr_insert.setLong(1, sessionId);
    hr_insert.setLong(2, heartrate.rec_time);
    hr_insert.setInt(3, heartrate.hr);
    hr_insert.addBatch();
  }

  public void insertPosition(long time, double lat, double lon, int alt, int speed_cm_s) throws SQLException
  {
    // gps(session_id,rec_time,lat,lon,alt_cm,speed_cm_s,acc,bearing) values (?,?,?,?,?,?,?,?)");

    gps_insert.setLong(1, sessionId);
    gps_insert.setLong(2, time);
    gps_insert.setDouble(3, lat);
    gps_insert.setDouble(4, lon);
    gps_insert.setInt(5, alt);
    gps_insert.setInt(6, speed_cm_s);
    gps_insert.setInt(7, 0);
    gps_insert.setInt(8, 0);
    gps_insert.addBatch();
  }

  public void insertPosition(Position position) throws SQLException
  {
    // gps(session_id,rec_time,lat,lon,alt_cm,speed_cm_s,acc,bearing) values (?,?,?,?,?,?,?,?)");

    gps_insert.setLong(1, sessionId);
    gps_insert.setLong(2, position.rec_time);
    gps_insert.setDouble(3, position.lat);
    gps_insert.setDouble(4, position.lon);
    gps_insert.setInt(5, position.alt_cm);
    gps_insert.setInt(6, position.speed_cm_s);
    gps_insert.setInt(7, 0);
    gps_insert.setInt(8, 0);
    gps_insert.addBatch();
  }

  public void insertMarker(long time) throws SQLException
  {
    System.out.println("Marker: " + time);
    marker_insert.setLong(1, sessionId);
    marker_insert.setLong(2, time);
    marker_insert.addBatch();
  }

  public void insertMarker(Marker marker) throws SQLException
  {
    System.out.println("Marker: " + marker.rec_time);
    marker_insert.setLong(1, sessionId);
    marker_insert.setLong(2, marker.rec_time);
    marker_insert.addBatch();
  }

  public void performPendingInserts() throws SQLException
  {
    bike_insert.executeBatch();
    fp_insert.executeBatch();
    hr_insert.executeBatch();
    gps_insert.executeBatch();
    marker_insert.executeBatch();
  }

  public void persist() throws SQLException
  {
    db.commit();
  }

  /**
   * Get the ID of the last created session or ID of the matching session from a previous action
   *
   * @return session ID
   */
  public long getSessionId()
  {
    return sessionId;
  }

  protected void insertImport() throws SQLException
  {
    if( parameters.noSave )
      return;

    import_data.setLong(1, sessionId);
    import_data.setLong(2, System.currentTimeMillis());
    import_data.setString(3, getClass().getSimpleName());
    import_data.setString(4, this.parameters.input);
    import_data.executeUpdate();
  }

  protected void runPost() throws SQLException
  {
    at.univie.mma.ai.Parameters p = new at.univie.mma.ai.Parameters();
    p.param0 = 1;
    p.param1 = 0;
    p.param2 = 0;
    p.session = sessionId;
    p.ai = 0;
    p.restart = 0;
    p.lang = 0;

    Database.getInstance().setAutoCommit(true);
    new PostProcessor(new NoPost(p), p).run();
  }
}
