package at.pegasos.integrations.files;

import com.beust.jcommander.*;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;

import at.pegasos.ai.util.*;
import at.pegasos.ai.util.TeamTools.*;
import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import at.pegasos.server.data.repository.*;
import at.univie.mma.ai.*;
import lombok.extern.slf4j.*;

@Slf4j
public class Ser extends FileImportTool {

  private final URLClassLoader child;
  private final Class<?> cMetadata;
  private final Class<?> cSession;
  private String fn;
  private Object dbParser;
  private Class<?> cDb_connector;

  public Ser() throws MalformedURLException, ClassNotFoundException, SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
  {
    child = new URLClassLoader(
            new URL[]{new File(Config.BACKEND_JAR).toURI().toURL()},
            this.getClass().getClassLoader()
    );

    cMetadata = Class.forName("at.pegasos.tool.util.Metadata", true, child);
    // cSession = Class.forName("at.univie.mma.server.data.Session", true, child);
    cSession = Class.forName("at.pegasos.server.data.Session", true, child);

    super.setup();
    HibernateUtil.setup();

    createParserDb();
  }

  public static void main(String[] args) throws ImportFailedException, MalformedURLException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, SQLException
  {
    Ser t = new Ser();

    t.parameters = new ImportTool.Parameters();
    JCommander.newBuilder()
            .addObject(t.parameters)
            .build()
            .parse(args);

    t.importFile(t.parameters.input);
  }

  private Object parseMeta() throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException
  {
    // Metadata meta= MetadataParser.parse(Files.readAllLines(MetadataParser.metaFileFromFile(fn)));
    Class<?> cMetadataParser = Class.forName("at.pegasos.tool.util.MetadataParser", true, child);
    Path fnMeta = (Path) cMetadataParser.getDeclaredMethod("metaFileFromFile", String.class).invoke(null, fn);

    return cMetadataParser.getDeclaredMethod("parse", List.class).invoke(null, Files.readAllLines(fnMeta));
  }

  public void importFile(String fn) throws ImportFailedException
  {
    log.info("Start import: {} [{}]", fn, parameters);
    this.fn = fn;

    long userId;

    try
    {
      Object meta = parseMeta();

      BufferedWriter f_out = new BufferedWriter(new FileWriter(Files.createTempFile(Paths.get("/tmp"), "readlog_" + Paths.get(fn).getFileName(), ".txt").toFile()));

      // MMAProtocolParser parser= new MMAProtocolParser(db, f_out);
      // Class<?> cMMAProtocolParser = Class.forName("at.univie.mma.server.parser.MMAProtocolParser", true, child);
      Class<?> cMMAProtocolParser = Class.forName("at.pegasos.server.parser.MMAProtocolParser", true, child);
      Object parser = cMMAProtocolParser.getConstructor(Class.forName("database.Db_connector", true, child), BufferedWriter.class).newInstance(dbParser, f_out);

      Object session = cMMAProtocolParser.getMethod("getSession").invoke(parser);
      if( this.parameters.userId == null )
      {
        // getValue(String key)
        String email = (String) cMetadata.getDeclaredMethod("getValue", String.class).invoke(meta, "User");

        User user = new UserRepository().findByEmail(email);

        // TODO: check for 'club' or 'group'
        Group group = TeamTools.create(db).getDefaultGroup(user.id);
        if( group != null )
        {
          cSession.getDeclaredField("group_id").set(session, (int) group.group_id);
          cSession.getDeclaredField("team_id").set(session, (int) group.team_id);
        }

        // parser.getSession().user_id= data.user_id;
        cSession.getDeclaredField("user_id").set(session, user.id);

        userId = user.id;
      }
      else
      {
        // parser.getSession().user_id= data.user_id;
        cSession.getDeclaredField("user_id").set(session, parameters.userId);
        userId = parameters.userId;

        if( parameters.determineGroup )
        {
          Group group = TeamTools.create(db).getDefaultGroup(userId);
          if( group != null )
          {
            cSession.getDeclaredField("group_id").set(session, (int) group.group_id);
            cSession.getDeclaredField("team_id").set(session, (int) group.team_id);
          }
        }
      }

      try
      {
        // parser.readFile(file, meta);
        long start = Long.parseLong((String) cMetadata.getDeclaredMethod("getValue", String.class).invoke(meta, "Starttime"));
        long duration = (long) cMMAProtocolParser.getMethod("getLastTimestamp", File.class).invoke(parser, Paths.get(fn).toAbsolutePath().toFile());
        long end = start + duration;
        log.debug("Start: {}, end: {}", start, end);
        // if( checkCreateSession(start, start + end) )
        if( !isInserted(userId, start, end) )
        {
          log.debug("Should import {}", fn);
          cMMAProtocolParser.getMethod("readFile", File.class, cMetadata).invoke(parser, Paths.get(fn).toAbsolutePath().toFile(), meta);
        }
        else
        {
          log.debug("{} already imported", fn);
        }
      }
      catch( Exception e )
      {
        e.printStackTrace();
        log.error(e.getMessage(), e);
      }

      f_out.close();
      log.debug("done");
    }
    catch( ClassNotFoundException | IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | NoSuchMethodException | SecurityException | NoSuchFieldException | SQLException e )
    {
      throw new ImportFailedException(e);
    }
  }

  private void createParserDb() throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
  {
    // Db_connector db= Db_connector.open_db();
    cDb_connector = Class.forName("database.Db_connector", true, child);
    dbParser = cDb_connector.getDeclaredMethod("open_db").invoke(null);
    // db.setAutoCommit(false);
    // cDb_connector.getDeclaredMethod("setAutoCommit", boolean.class).invoke(dbParser, false);
  }
}
