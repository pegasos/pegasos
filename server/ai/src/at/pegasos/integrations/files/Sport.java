package at.pegasos.integrations.files;

public enum Sport {
  RUNNING,
  CYCLING
}
