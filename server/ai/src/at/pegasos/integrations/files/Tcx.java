package at.pegasos.integrations.files;

import com.beust.jcommander.*;

import org.w3c.dom.*;
import org.xml.sax.*;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import javax.xml.parsers.*;

import lombok.extern.slf4j.*;

@Slf4j
@SuppressWarnings("SimpleDateFormat")
public class Tcx extends FileImportTool {

  private static final DateFormat sdf;
  static {
    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT-0"));
  }
  double last_dist_m = 0;
  long last_dist_t = -1;
  double speed_mm_s;
  private double lastStrides;
  private Document doc;
  private long start;
  private long end;
  private String sport;
  private Sport eSport;
  private long currentTime;
  private long lastTime = -1;

  public Tcx() throws SQLException
  {
    super.setup();
  }

  public static void main(String[] args) throws ImportFailedException, SQLException
  {
    // String d1= "2020-02-05T16:25:17.909Z";
    // System.out.println(sdf.parse(d1));

    Tcx t = new Tcx();
    /*Parameters p = new Parameters();
    p.userId = Long.parseLong(args[0]);
    p.input = args[1];
    t.setParameters(p);
    t.importFile(args[1]);*/

    t.parameters = new ImportTool.Parameters();
    JCommander.newBuilder()
            .addObject(t.parameters)
            .build()
            .parse(args);

    t.importFile(t.parameters.input);
  }

  /**
   * Get the start time of the activity in a file
   * @param fn filename
   * @return timestamp (GMT-0)
   * @throws ImportFailedException if parsing / opening of the file fails
   */
  public static Long startTime(String fn) throws ImportFailedException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new File(fn));
      doc.getDocumentElement().normalize();

      // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

      NodeList nList = doc.getElementsByTagName("Id");
      // System.out.println("----------------------------");

      assert (nList.getLength() == 1);

      String x = nList.item(0).getChildNodes().item(0).getNodeValue();

      // System.out.println(x);
      // "2016-08-15T14:43:59.000Z"
      Date d = sdf.parse(x);

      return d.getTime();
    }
    catch( ParserConfigurationException | SAXException | IOException | ParseException e )
    {
      throw new ImportFailedException(e);
    }
  }

  public void importFile(String fn) throws ImportFailedException
  {
    try
    {
      openFile(fn);

      if( checkCreateSession(start, end) )
      {
        doImport();
        if( !parameters.noPost )
        {
          runPost();
        }
      }
      else
        log.debug("Session already inserted");
    }
    catch( ParserConfigurationException | SAXException | IOException | ParseException | SQLException e )
    {
      throw new ImportFailedException(e);
    }
  }

  public void openFile(String fn) throws ParserConfigurationException, SAXException, IOException, ParseException
  {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    doc = dBuilder.parse(new File(fn));
    doc.getDocumentElement().normalize();

    // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

    NodeList nList = doc.getElementsByTagName("Id");
    // System.out.println("----------------------------");

    assert (nList.getLength() == 1);

    String x = nList.item(0).getChildNodes().item(0).getNodeValue();

    // System.out.println(x);
    // "2016-08-15T14:43:59.000Z"
    Date d = sdf.parse(x);
    // System.out.println(d);

    start = d.getTime();

    nList = doc.getElementsByTagName("Time");
    d = sdf.parse(nList.item(nList.getLength() - 1).getChildNodes().item(0).getNodeValue());
    end = d.getTime();

    // output("    <Activity Sport=\"" + sport + "\">");
    nList = doc.getElementsByTagName("Activity");
    if( nList.getLength() > 1 )
      throw new UnsupportedOperationException("Cannot import file with more than one activity");

    sport = sportToSetting(nList.item(0).getAttributes().getNamedItem("Sport").getNodeValue());
    switch(sport)
    {
      case "Cycling":
      case "Indoor Cycling":
        eSport = Sport.CYCLING;
        break;
      case "Running":
        eSport = Sport.RUNNING;
        break;
      default:
        eSport = null;
    }
  }

  private void doImport() throws SQLException, ParseException
  {
    if( sport != null )
    {
      insertSetting(start, "Sport", sport);
    }

    readFile();
    performPendingInserts();

    insertImport();

    persist();
  }

  public String sportToSetting(String sport)
  {
    if( sport.equals("Biking") )
    {
      if( doc.getElementsByTagName("Position").getLength() > 0 )
      {
        return "Cycling";
      }
      else
      {
        return "Indoor Cycling";
      }
    }
    else if( sport.equals("Running") )
    {
      return "Running";
    }

    return null;
  }

  public void readFile() throws ParseException, SQLException
  {
    NodeList laps = doc.getElementsByTagName("Lap");
    // <Lap StartTime="2020-06-01T14:55:00.000Z">
    for(int lapIdx = 0; lapIdx < laps.getLength(); lapIdx++)
    {
      if( lapIdx > 0 )
      {
        long startTime;
        Date d = sdf.parse(laps.item(lapIdx).getAttributes().getNamedItem("StartTime").getNodeValue());
        startTime = d.getTime();

        insertMarker(startTime);
      }
    }

    NodeList points = doc.getElementsByTagName("Trackpoint");
    for(int i = 0; i < points.getLength(); i++)
    {
      Element point = (Element) points.item(i);

      parseTrackpoint(point);
    }
  }

  private void parseTrackpoint(Element point) throws ParseException, SQLException
  {
    // Time
    String x = point.getElementsByTagName("Time").item(0).getChildNodes().item(0).getNodeValue();
    Date d = sdf.parse(x);
    // System.out.println(d + " " + d.getTime());
    currentTime = d.getTime();

    parsePosition(point);

    parseHr(point);

    parseDistance(point);

    if( eSport != null )
    {
      if( eSport == Sport.CYCLING )
        parseBikeData(point);
      else if( eSport == Sport.RUNNING )
        parseRunningData(point);
    }
    lastTime = currentTime;

    /*
     * <Time>2016-08-15T14:44:01.000Z</Time> <Position><LatitudeDegrees>48.18989</LatitudeDegrees>
     * <LongitudeDegrees>16.35253</LongitudeDegrees></Position>
     * <AltitudeMeters>205.285</AltitudeMeters> <DistanceMeters>9.137747022840712</DistanceMeters>
     * <HeartRateBpm><Value>126</Value></HeartRateBpm>
     */
  }

  private void parseDistance(Element point)
  {
    String x;

    if( point.getElementsByTagName("DistanceMeters").getLength() > 0 )
    {
      // <DistanceMeters>9.137747022840712</DistanceMeters>
      x = point.getElementsByTagName("DistanceMeters").item(0).getChildNodes().item(0).getNodeValue();
      double dist = Double.parseDouble(x);
      if( last_dist_t != -1 )
        speed_mm_s = (dist - last_dist_m) * 1000 / ((double) (currentTime - last_dist_t)) * 1000;
      else
        speed_mm_s = (dist - last_dist_m) * 1000;
      // System.out.println("has dist speed_mm_s:" + speed_mm_s + " dist:" + dist + " " + (dist - last_dist_m) + " " + (currentTime - last_dist_t));
      last_dist_m = dist;
      last_dist_t = currentTime;

      // sendFootPodData(d.getTime(), d.getTime(), speed, dist, 0, 0);
    }
    // else
    //  System.out.println("No dist");
  }

  private void parsePosition(Element point) throws SQLException, NumberFormatException
  {
    String x;
    if( point.getElementsByTagName("Position").getLength() > 0 )
    {
      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
      x = point.getElementsByTagName("LatitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
      double lat = Double.parseDouble(x);
      x = point.getElementsByTagName("LongitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
      double lon = Double.parseDouble(x);
      // AltitudeMeters
      int alt = 0;
      if( point.getElementsByTagName("AltitudeMeters").getLength() > 0 )
      {
        x = point.getElementsByTagName("AltitudeMeters").item(0).getChildNodes().item(0).getNodeValue();
        alt = (int) (Double.parseDouble(x) * 100);
      }
      else
        System.out.println("Alt missing: " + point + " " + lat + " " + lon);

      // System.out.println("has position " + x);

      insertPosition(currentTime, lat, lon, alt, (int) (speed_mm_s / 10.0));
    }
    // else
    //  System.out.println("No position");
  }

  private void parseHr(Element point) throws NumberFormatException, SQLException
  {
    String x;
    if( point.getElementsByTagName("HeartRateBpm").getLength() > 0 )
    {
      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
      x = point.getElementsByTagName("Value").item(0).getChildNodes().item(0).getNodeValue();
      // System.out.println("has hr " + x);

      insertHr(currentTime, Integer.parseInt(x));
    }
    // else
    //  System.out.println("No hr");
  }

  private void parseRunningData(Element point) throws SQLException
  {
    Short cadence = null;
    Integer strides = null;

    if( point.getElementsByTagName("Cadence").getLength() > 0 )
    {
      cadence = Short.parseShort(point.getElementsByTagName("Cadence").item(0).getTextContent());

      if( lastTime != -1 )
      {
        long elapsed = currentTime - lastTime;
        lastStrides = (cadence * elapsed) / 1000.0 / 60.0 + lastStrides;
      }
      else
        lastStrides = 0;
    }

    if( last_dist_t == currentTime )
      insertFootpod(currentTime, (int) speed_mm_s, (int) last_dist_m, strides, cadence);
    else
      insertFootpod(currentTime, null, null, strides, cadence);
  }

  private void parseBikeData(Element point) throws SQLException
  {
    Short watts = null;
    Integer cadence = null;

    if( point.getElementsByTagName("Watts").getLength() > 0 )
    {
      watts = Short.parseShort(point.getElementsByTagName("Watts").item(0).getTextContent());

      // System.out.println("Watts present " + watts);
    }
    if( point.getElementsByTagName("Cadence").getLength() > 0 )
    {
      cadence = Integer.parseInt(point.getElementsByTagName("Cadence").item(0).getTextContent());

      // System.out.println("Cadence present " + cadence);
    }
    // <Extensions><TPX xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2"><Watts>302</Watts></TPX></Extensions>
    // <Cadence>90</Cadence>

    if( last_dist_t == currentTime )
      insertBike(currentTime, (int) speed_mm_s, (int) last_dist_m, cadence, watts);
    else
      insertBike(currentTime, null, null, cadence, watts);
  }
}
