package at.pegasos.integrations.files.data;

import java.nio.*;
import java.security.*;
import java.util.*;

public class TestDecode {
  public static void main(String[] args) throws NoSuchAlgorithmException {
    // target yzvx3Mb6

    long id = 7472790125L;
    ByteBuffer buffer = ByteBuffer.allocate(8);
    buffer.putLong(id);

    System.out.println(Arrays.toString(buffer.array()));
    System.out.println(Arrays.toString(Base64.getEncoder().encode(buffer.array())));
    System.out.println(new String(Base64.getEncoder().encode(buffer.flip().array())));

    id = 7473996162L;
    buffer = ByteBuffer.allocate(8);
    buffer.putLong(id);

    // y7BpQjxD
    System.out.println(Arrays.toString(Base64.getEncoder().encode(buffer.array())));
    System.out.println(new String("A").getBytes()[0]);
    byte b = 61;

    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(new String("" + id).getBytes());
    byte[] digest = md.digest();
    System.out.println(new String(digest));

    System.out.println(Long.valueOf(7473996162L).hashCode());
    buffer = ByteBuffer.allocate(4);
    buffer.putInt(Long.valueOf(7473996162L).hashCode());
    // y7BpQjxD
    System.out.println(Arrays.toString(Base64.getEncoder().encode(buffer.array())));
    System.out.println(new String(Base64.getEncoder().encode(buffer.array())));

    buffer = ByteBuffer.allocate(4);
    buffer.putInt(Long.valueOf(7472790125L).hashCode());
    // y7BpQjxD
    System.out.println(Arrays.toString(Base64.getEncoder().encode(buffer.array())));
    System.out.println(new String(Base64.getEncoder().encode(buffer.array())));

    System.out.println("");

    buffer = ByteBuffer.allocate(5);
    buffer.put((byte) 1);
    buffer.put((byte) -67);
    buffer.put((byte) 124);
    buffer.put((byte) 35);
    buffer.put((byte) -126);

    System.out.println(Arrays.toString(buffer.array()));
    System.out.println(Arrays.toString(Base64.getEncoder().encode(buffer.array())));
    System.out.println(new String(Base64.getEncoder().encode(buffer.array())));
  }
}
