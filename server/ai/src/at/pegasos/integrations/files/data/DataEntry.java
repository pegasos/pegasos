package at.pegasos.integrations.files.data;

import lombok.*;

@ToString
public class DataEntry {
  public long rec_time;

  public DataEntry() {}

  public DataEntry(long timestamp)
  {
    this.rec_time = timestamp;
  }
}
