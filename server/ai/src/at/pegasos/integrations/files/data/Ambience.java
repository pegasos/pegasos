package at.pegasos.integrations.files.data;

import lombok.*;

@ToString(callSuper = true)
public class Ambience extends DataEntry {
  public int temperature;
  public int pressure;

  public Ambience(long timestamp)
  {
    super(timestamp);
  }
}
