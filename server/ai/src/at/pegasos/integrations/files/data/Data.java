package at.pegasos.integrations.files.data;

import at.pegasos.integrations.files.*;
import lombok.*;

import java.time.*;
import java.util.*;

@ToString
public class Data {
  static final long FIT_EPOCH_MS = 631065600000L;

  public List<DataEntry> dataEntries;
  public List<DataEntry> markers;

  public int tzLocal;
  public int tzServer;
  public long timeStart;
  public long timeLastEntry = 0;

  public Sport sport;
  public boolean isIndoor;

  public String sportToSetting()
  {
    if( sport == Sport.CYCLING )
    {
      return isIndoor ? "Indoor Cycling" : "Cycling";
    }
    else if( sport == Sport.RUNNING )
    {
      return "Running";
    }

    return null;
  }


  public static long fitToTimestamp(long timestamp)
  {
    Instant instant = Instant.ofEpochMilli(timestamp * 1000 + FIT_EPOCH_MS);

    return instant.toEpochMilli();
  }
}