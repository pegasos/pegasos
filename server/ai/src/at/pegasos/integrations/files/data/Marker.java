package at.pegasos.integrations.files.data;

import lombok.*;

@ToString(callSuper = true)
public class Marker extends DataEntry{
  public Marker(long timestamp)
  {
    super(timestamp);
  }
}
