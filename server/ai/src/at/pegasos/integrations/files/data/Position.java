package at.pegasos.integrations.files.data;

import lombok.*;

@ToString
public class Position extends DataEntry {
  public double lat;
  public double lon;
  public int alt_cm;
  public int speed_cm_s;

  public Position(long timestamp, double lat, double lon)
  {
    super(timestamp);
    this.lat = lat;
    this.lon = lon;
  }
}
