package at.pegasos.integrations.files.data;

import lombok.*;

@ToString(callSuper = true)
public class Bike extends DataEntry {
  public Integer speed_mm_s;
  public Integer distance_m;
  public Short cadence;
  public Short power;

  public Bike(long ts)
  {
    super(ts);
  }
}
