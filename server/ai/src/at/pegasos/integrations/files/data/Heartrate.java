package at.pegasos.integrations.files.data;

import lombok.*;

@ToString(callSuper = true)
public class Heartrate extends DataEntry {
  public short hr;

  public Heartrate(long timestamp, short hr)
  {
    super(timestamp);
    this.hr = hr;
  }
}
