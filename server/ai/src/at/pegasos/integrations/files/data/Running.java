package at.pegasos.integrations.files.data;

import lombok.*;

@ToString(callSuper = true)
public class Running extends DataEntry {
  public Integer speed_mm_s;
  public Integer distance_m;
  public Integer strides_255;
  public Short cadence;

  public Running(long timestamp, int speed_mm_s, int distance_m, Short cadence)
  {
    super(timestamp);
    this.speed_mm_s = speed_mm_s;
    this.distance_m = distance_m;
    this.cadence = cadence;
  }
}
