package at.pegasos.integrations.files;

public abstract class FileImportTool extends ImportTool {

  public void setParameters(Parameters parameters)
  {
    this.parameters = parameters;
  }

  abstract public void importFile(String fn) throws ImportFailedException;
}
