package at.pegasos.integrations.files;

import com.beust.jcommander.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import javax.xml.parsers.*;

import lombok.extern.slf4j.*;

@Slf4j
@SuppressWarnings("SimpleDateFormat")
public class Pwx extends FileImportTool {
  //                                                                2020-02-03T10:31:00
  private static final DateFormat sdf;
  static {
    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT-0"));
  }
  double last_dist_m = 0;
  long last_dist_t = -1;
  double speed_mm_s;
  private Document doc;
  private long start;
  private long end;
  private String sport;
  private String title;
  private long currentTime;

  public Pwx() throws SQLException
  {
    super.setup();
  }

  public static void main(String[] args) throws ImportFailedException, SQLException
  {
    Pwx t = new Pwx();

    t.parameters = new ImportTool.Parameters();
    JCommander.newBuilder()
        .addObject(t.parameters)
        .build()
        .parse(args);

    t.importFile(t.parameters.input);
  }

  public void importFile(String fn) throws ImportFailedException
  {
    try
    {
      openFile(fn);

      if( checkCreateSession(start, end) )
      {
        doImport();
        if( !parameters.noPost )
        {
          runPost();
        }
      }
      else
        log.debug("Session already inserted");
    }
    catch( ParserConfigurationException | SAXException | IOException | ParseException | SQLException e)
    {
      e.printStackTrace();
      throw new ImportFailedException(e);
    }
  }

  private void doImport() throws SQLException
  {
    if( sport != null )
    {
      insertSetting(start, "Sport", sport);
    }
    if( title != null )
    {
      insertSetting(start, "title", title);
    }

    readFile();
    performPendingInserts();

    insertImport();

    persist();
  }

  /**
   * Get the start time of the activity in a file
   * @param fn filename
   * @return timestamp (GMT-0)
   * @throws ImportFailedException if parsing / opening of the file fails
   */
  public static Long startTime(String fn) throws ImportFailedException
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new File(fn));
      doc.getDocumentElement().normalize();

      // extract start time. Encoded in tag <time>
      NodeList nList = doc.getElementsByTagName("time");
      assert (nList.getLength() == 1);
      String x = nList.item(0).getChildNodes().item(0).getNodeValue();

      Date d = sdf.parse(x);

      return d.getTime();
    }
    catch( ParserConfigurationException | IOException | ParseException | SAXException e )
    {
      throw new ImportFailedException(e);
    }

  }

  public void openFile(String fn) throws ParserConfigurationException, SAXException, IOException, ParseException
  {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    doc = dBuilder.parse(new File(fn));
    doc.getDocumentElement().normalize();

    // extract start time. Encoded in tag <time>
    NodeList nList = doc.getElementsByTagName("time");
    assert (nList.getLength() == 1);
    String x = nList.item(0).getChildNodes().item(0).getNodeValue();

    Date d = sdf.parse(x);
    start = d.getTime();

    // we need to calculate the end by extracting the duration
    x = ((Element) doc.getElementsByTagName("summarydata").item(0)).getElementsByTagName("duration").item(0).getTextContent();
    double h = Double.parseDouble(x) * 1000;
    end = start + (long) h;

    nList = doc.getElementsByTagName("sportType");
    if( nList.getLength() > 0 )
      sport = sportToSetting(nList.item(0).getTextContent());

    nList = doc.getElementsByTagName("title");
    if( nList.getLength() > 0 )
      title = sportToSetting(nList.item(0).getTextContent());
  }

  public String sportToSetting(String sport)
  {
    if( sport.equals("Bike") )
    {
      if( doc.getElementsByTagName("lat").getLength() > 0 )
      {
        return "Cycling";
      }
      else
      {
        return "Indoor Cycling";
      }
    }
    else if( sport.equals("Run") )
    {
      return "Running";
    }

    return null;
  }

  public void readFile() throws SQLException
  {
    NodeList laps = doc.getElementsByTagName("segment");
    // <Lap StartTime="2020-06-01T14:55:00.000Z">
    for(int lapIdx = 0; lapIdx < laps.getLength(); lapIdx++)
    {
      if( lapIdx > 0 )
      {
        long startTime;
        /*Date d= sdf.parse(laps.item(lapIdx).getAttributes().getNamedItem("StartTime").getNodeValue());
        startTime= d.getTime();*/
        startTime = (long) (Double.parseDouble(((Element) laps.item(lapIdx)).getElementsByTagName("beginning").item(0).getTextContent()) * 1000);

        // System.out.println(startTime);

        insertMarker(startTime + start);
      }
    }

    NodeList points = doc.getElementsByTagName("sample");
    for(int i = 0; i < points.getLength(); i++)
    {
      Element point = (Element) points.item(i);

      parseSample(point);
    }
  }

  private void parseSample(Element point) throws SQLException
  {
    // Time
    long h = (long) (Double.parseDouble(point.getElementsByTagName("timeoffset").item(0).getTextContent()) * 1000);
    currentTime = start + h;

    parseDistance(point);

    parsePosition(point);

    parseHr(point);

    parseBikeData(point);
  }

  private void parseDistance(Element point)
  {
    String x;

    if( point.getElementsByTagName("dist").getLength() > 0 )
    {
      // <DistanceMeters>9.137747022840712</DistanceMeters>
      x = point.getElementsByTagName("dist").item(0).getTextContent();
      double dist = Double.parseDouble(x);
      if( last_dist_t != -1 )
        speed_mm_s = (dist - last_dist_m) * 1000 / ((double) (currentTime - last_dist_t)) * 1000;
      else
        speed_mm_s = (dist - last_dist_m) * 1000;
      // System.out.println("has dist speed_mm_s:" + speed_mm_s + " dist:" + dist + " " + (dist - last_dist_m) + " " + (currentTime - last_dist_t));
      last_dist_m = dist;
      last_dist_t = currentTime;

      if( point.getElementsByTagName("spd").getLength() > 0 )
      {
        speed_mm_s = Double.parseDouble(point.getElementsByTagName("spd").item(0).getTextContent()) * 1000;
      }
    }
    // else
    //  System.out.println("No dist");
  }

  private void parsePosition(Element point) throws SQLException
  {
    // <lat>42.7784606</lat>
    // <lon>11.1239249</lon>

    String x;
    if( point.getElementsByTagName("lat").getLength() > 0 )
    {
      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
      x = point.getElementsByTagName("lat").item(0).getTextContent();
      double lat = Double.parseDouble(x);
      x = point.getElementsByTagName("lon").item(0).getTextContent();
      double lon = Double.parseDouble(x);
      // AltitudeMeters
      x = point.getElementsByTagName("alt").item(0).getTextContent();
      int alt = (int) (Double.parseDouble(x) * 100);

      // System.out.println("has position " + x);

      double speed_m_s = 0;
      if( point.getElementsByTagName("spd").getLength() > 0 )
      {
        speed_m_s = Double.parseDouble(point.getElementsByTagName("spd").item(0).getTextContent());
      }

      // System.out.println(lat + " " + lon + " " + alt + " " + speed_m_s);
      insertPosition(currentTime, lat, lon, alt, (int) (speed_m_s * 10.0));
    }
    // else
    //  System.out.println("No position");
  }

  private void parseHr(Element point) throws NumberFormatException, SQLException
  {
    String x;
    if( point.getElementsByTagName("hr").getLength() > 0 )
    {
      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
      x = point.getElementsByTagName("hr").item(0).getTextContent();
      // System.out.println("hr " + x);

      insertHr(currentTime, Integer.parseInt(x));
    }
    // else
    //  System.out.println("No hr");
  }

  private void parseBikeData(Element point) throws SQLException
  {
    Short watts = null;
    Integer cadence = null;

    if( point.getElementsByTagName("pwr").getLength() > 0 )
    {
      watts = Short.parseShort(point.getElementsByTagName("pwr").item(0).getTextContent());

      // System.out.println("Watts present " + watts);
    }
    if( point.getElementsByTagName("cad").getLength() > 0 )
    {
      cadence = Integer.parseInt(point.getElementsByTagName("cad").item(0).getTextContent());

      // System.out.println("Cadence present " + cadence);
    }
    // <Extensions><TPX xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2"><Watts>302</Watts></TPX></Extensions>
    // <Cadence>90</Cadence>

    // System.out.println(speed_mm_s + " " + last_dist_m + " " + cadence + " " + watts);
    if( last_dist_t == currentTime )
      insertBike(currentTime, (int) speed_mm_s, (int) last_dist_m, cadence, watts);
    else
      insertBike(currentTime, null, null, cadence, watts);
  }

  public void setParameters(Parameters parameters)
  {
    this.parameters = parameters;
  }
}
