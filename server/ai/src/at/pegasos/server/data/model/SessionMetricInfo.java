package at.pegasos.server.data.model;

import javax.persistence.*;

@Entity
@Table(name="metric")
public class SessionMetricInfo {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  public long ID;

  public String name;

  public String unit;
}