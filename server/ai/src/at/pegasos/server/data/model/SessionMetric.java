package at.pegasos.server.data.model;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name="session_metric")
@Getter @Setter @ToString
public class SessionMetric {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long ID;

  private long session_id;

  @ManyToOne()
  @JoinColumn(name="metric")
  private SessionMetricInfo metric;

  @Column(name="`value`")
  private double value;

  private Integer segmentnr;

  private String param;
}
