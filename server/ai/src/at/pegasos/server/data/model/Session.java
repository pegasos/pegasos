package at.pegasos.server.data.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.*;

@Entity
@Table(name="`session`")
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Session {

  /**
   * ID of the session
   */
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name="session_id")
  public long sessionId;

  /**
   * ID of the user who performed this activity
   */
  /*@Column(name="userid")
  public int userId;*/

  @Column(name="teamid")
  public Long teamid;

  @Column(name="groupid")
  public Long groupid;

  @ManyToOne()
  @JoinColumn(name="userid")
  public User user;

  /*@OneToOne(optional = true)
  @JoinColumn(name="teamid")
  @NotFound(action = NotFoundAction.IGNORE)
  public Team team;

  @OneToOne(optional = true)
  @JoinColumn(name="groupid")
  @NotFound(action = NotFoundAction.IGNORE)
  public Group group;*/

  @Column(name="activity")
  public int param0;
  public int param1;
  public int param2;

  /**
   * Timestamp when the activity was started
   */
  public long starttime;

  /**
   * Timestamp when the activity was last updated
   */
  public long last_update;

  public long endtime;

  public boolean ai;
}
