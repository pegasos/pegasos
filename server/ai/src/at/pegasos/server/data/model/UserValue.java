package at.pegasos.server.data.model;

import java.util.*;

import javax.persistence.*;

@MappedSuperclass
public abstract class UserValue {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name="ID")
  public long id;

  @OneToOne()
  @JoinColumn(name="value_id", insertable= false, updatable = false)
  private UserValueInfo value;

  @Column(name="value_id")
  private long value_id;

  public long user_id;

  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_start;

  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_end;

  public void setValue(UserValueInfo value)
  {
    this.value= value;
    value_id= value.id;
  }

  public UserValueInfo getValue()
  {
    return this.value;
  }

  public abstract double getDoubleValue();
}
