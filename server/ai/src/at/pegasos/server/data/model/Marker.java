package at.pegasos.server.data.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="marker")
@Getter @Setter @ToString
public class Marker {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "marker_id")
  private long ID;

  private long session_id;

  private long rec_time;
}
