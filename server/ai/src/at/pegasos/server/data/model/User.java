package at.pegasos.server.data.model;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name= "`user`")
@Data
public class User {
  
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  // @GeneratedValue(strategy=GenerationType.AUTO)
  public long id;

  private int type = 1;
  
  /**
   * Firstname of the user
   */
  @Column(nullable= false, name="forename")
  public String firstname;
  
  /**
   * Lastname of the user
   */
  @Column(nullable= false, name="surname")
  public String lastname;
  
  @Column(nullable= false, unique= true)
  public String email;

  public String password_1250; // ` varchar(255) NOT NULL,
  public String salt_0954; // ` varchar(255) NOT NULL,

  
  public long birthday;
  
  public double height;
  
  public double weight;
  
  public int gender;
  
  @Column(name="data1")
  public Double vo2max;
  
  @Column(name="data2")
  public Long hrmax;
  
  @Column(name="data4")
  public Long hrmin;

  @Column(name="data3")
  public Long fpcalib;

  @Column(name="data1_crtime")
  public Double vo2max_time;
  
  @Column(name="data2_crtime")
  public Long hrmax_time;
  
  @Column(name="data4_crtime")
  public Long hrmin_time;

  @Column(name="data3_crtime")
  public Long fpcalib_time;

  @Override
  public String toString()
  {
    return "User{" +
            "id=" + id +
            ", firstname='" + firstname + '\'' +
            ", lastname='" + lastname + '\'' +
            ", email='" + email + '\'' +
            ", birthday=" + birthday +
            ", height=" + height +
            ", weight=" + weight +
            ", gender=" + gender +
            ", vo2max=" + vo2max +
            ", hrmax=" + hrmax +
            ", hrmin=" + hrmin +
            ", fpcalib=" + fpcalib +
            ", vo2max_time=" + vo2max_time +
            ", hrmax_time=" + hrmax_time +
            ", hrmin_time=" + hrmin_time +
            ", fpcalib_time=" + fpcalib_time +
            '}';
  }

  /*@ManyToMany(fetch = FetchType.EAGER)
  @WhereJoinTable(clause="date_end is null")
  @JoinTable(
          name = "user_has_role",
          joinColumns = { @JoinColumn(name = "user_id", referencedColumnName= "id") },
          inverseJoinColumns = { @JoinColumn(name = "role_id") }
  )
  public Collection<Role> roles;*/
}
