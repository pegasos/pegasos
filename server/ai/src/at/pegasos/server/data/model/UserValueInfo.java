package at.pegasos.server.data.model;

import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@ToString
@Entity
@Table(name= "`value`")
public class UserValueInfo {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name="ID")
  public long id;

  public enum ValueType {
    INTEGER, DECIMAL, TEXT;
  }

  @NaturalId
  public String name;

  private String type;

  public ValueType getType()
  {
    switch(type)
    {
      case "int":
        return ValueType.INTEGER;
      case "dec":
        return ValueType.DECIMAL;
      case "text":
        return ValueType.TEXT;
      default:
        throw new IllegalStateException();
    }
  }

  public void setType(ValueType type)
  {
    switch(type)
    {
      case INTEGER:
        this.type = "int";
        break;
      case DECIMAL:
        this.type = "dec";
        break;
      case TEXT:
        this.type = "text";
        break;
    }
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    UserValueInfo that=(UserValueInfo) o;
    return id == that.id &&
            name.equals(that.name) &&
            type.equals(that.type);
  }
}