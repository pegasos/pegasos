package at.pegasos.server.data.model;

import java.text.*;

import javax.persistence.*;

@Entity
@Table(name= "user_value_dec")
public class UserValueDec extends UserValue {

  @SuppressWarnings("SimpleDateFormat")
  private static final SimpleDateFormat format1= new SimpleDateFormat("yyyy-MM-dd");

  @Override
  public String toString()
  {
    return "UserValueDec [internal=" + internal + ", id=" + id + ", value=" + getValue() + ", user_id=" + user_id + ", date_start="
            + (date_start == null ? "null" : format1.format(date_start.getTime())) + ", date_end="
            + (date_end == null ? "null" : format1.format(date_end.getTime())) + "]";
  }

  @Column(name= "`value`")
  public double internal;

  public double getDoubleValue()
  {
    return internal;
  }
}
