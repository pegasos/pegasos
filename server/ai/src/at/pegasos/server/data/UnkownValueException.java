package at.pegasos.server.data;

public class UnkownValueException extends Exception {

  public UnkownValueException(String name)
  {
    super(name);
  }
}
