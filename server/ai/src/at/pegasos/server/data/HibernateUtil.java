package at.pegasos.server.data;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.slf4j.*;

import java.util.*;

import javax.persistence.*;

import at.univie.mma.ai.*;
import ch.qos.logback.classic.*;

public class HibernateUtil {

  public static HibernateUtil instance;
  private static EntityManager em;
  private final SessionFactory factory;
  private final EntityManagerFactory emf;

  protected Configuration cfg;

  private Session lastSession;

  protected HibernateUtil()
  {
    setupLogging();
    configure();
    factory = cfg.buildSessionFactory();

    Properties p = cfg.getProperties();

    // convert to Map
    Map<String, String> pMap = new HashMap<>();
    Enumeration<?> e = p.propertyNames();
    while (e.hasMoreElements()) {
      String s = (String) e.nextElement();
      pMap.put(s, p.getProperty(s));
    }

    // create EntityManagerFactory
    // EntityManagerFactory emf = Persistence.createEntityManagerFactory("some persistence unit", pMap);
    // emf = Persistence.createEntityManagerFactory(getClass().getCanonicalName());
    emf = Persistence.createEntityManagerFactory("some persistence unit", pMap);
  }

  private void setupLogging()
  {
    ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.hibernate")).setLevel(Level.INFO);
    ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.hibernate.SQL")).setLevel(Level.INFO);
    ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.hibernate.type.descriptor.sql.BasicBinder")).setLevel(Level.INFO);
    ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.hibernate.type.BasicTypeRegistry")).setLevel(Level.INFO);
    // ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("net.sf.ehcache")).setLevel(Level. INFO);
    ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.jboss")).setLevel(Level.INFO);
  }

  protected void configure()
  {
    cfg = new Configuration()
      .setProperty("hibernate.connection.driver_class", Config.DB_DRIVER)
      .setProperty("hibernate.connection.url", Config.DB_URL)
      .setProperty("hibernate.connection.username", Config.DB_USER)
      .setProperty("hibernate.connection.password", Config.DB_PASSWORD)
      .setProperty("hibernate.show_sql", "true")
      .addAnnotatedClass(at.pegasos.server.data.model.Session.class)
      .addAnnotatedClass(at.pegasos.server.data.model.SessionMetric.class)
      .addAnnotatedClass(at.pegasos.server.data.model.SessionMetricInfo.class)
      .addAnnotatedClass(at.pegasos.server.data.model.User.class)
      .addAnnotatedClass(at.pegasos.server.data.model.UserValue.class)
      .addAnnotatedClass(at.pegasos.server.data.model.UserValueInfo.class)
      .addAnnotatedClass(at.pegasos.server.data.model.UserValueInt.class)
      .addAnnotatedClass(at.pegasos.server.data.model.UserValueDec.class)
    ;
  }

  public static void setup()
  {
    if (instance == null)
      instance = new HibernateUtil();
  }

  public static Session openSessionSafe()
  {
    if (instance == null)
    {
      instance = new HibernateUtil();
    }
    return openSession();
  }

  public static Session openSession()
  {
    instance.lastSession = instance.factory.openSession();
    return instance.lastSession;
  }

  public static Session getSession()
  {
    return instance.lastSession;
  }

  public static void closeSession()
  {
    if( instance.lastSession != null )
    {
      instance.lastSession.close();
      instance.lastSession = null;
    }
  }

  public static Session OpenOrGetSession()
  {
    if( instance == null )
    {
      instance = new HibernateUtil();
    }

    if( instance.lastSession == null )
    {
      return openSessionSafe();
    }
    else
    {
      return instance.lastSession;
    }
  }

  public static EntityManager getEntityManager()
  {
    if( em == null )
    {
      em = instance.emf.createEntityManager();
    }
    return em;
  }
}
