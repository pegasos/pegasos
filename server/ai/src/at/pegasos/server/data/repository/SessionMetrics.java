package at.pegasos.server.data.repository;

import org.hibernate.Session;

import java.util.*;

import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;

public class SessionMetrics {
  private static final String hqlString = "From SessionMetric s where s.session_id = :sessionId";
  private final org.hibernate.Session hibernateSession;

  public SessionMetrics()
  {
    hibernateSession = HibernateUtil.OpenOrGetSession();
  }

  public SessionMetrics(Session session)
  {
    hibernateSession = session;
  }

  public Collection<SessionMetric> findById(long sessionId)
  {
    List<SessionMetric> list = hibernateSession
            .createQuery(hqlString, SessionMetric.class)
            .setParameter("sessionId", sessionId)
            .list();
    return list;
  }
}
