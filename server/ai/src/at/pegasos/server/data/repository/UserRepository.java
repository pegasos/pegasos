package at.pegasos.server.data.repository;

import java.util.*;

import javax.persistence.*;

import at.pegasos.server.data.model.*;

public class UserRepository extends JpaRepository<User, Long> {

  public User findById(long ID)
  {
    return em.find(User.class, ID);
  }

  public User findByEmail(String email)
  {
    /*CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<User> criteria = builder.createQuery(User.class);
    Root<User> from = criteria.from(User.class);
    criteria.select(from);
    criteria.where(builder.equal(from.get(User.email), email));
    TypedQuery<User> typed = em.createQuery(criteria);
    try {
        return typed.getSingleResult();
    } catch (final NoResultException nre) {
        return null;
    }*/
    try
    {
      return em.createQuery(
              "SELECT u from User u WHERE u.email = :email", User.class)
              .setParameter("email", email).getSingleResult();
    }
    catch( final NoResultException nre )
    {
      return null;
    }
  }

  public List<User> findByFirstname(String name)
  {
    try
    {
      return em.createQuery(
              "SELECT u from User u WHERE u.firstname = :firstname", User.class)
              .setParameter("firstname", name).getResultList();
    }
    catch( final NoResultException nre )
    {
      return null;
    }
  }

  public void deleteAll()
  {
    if( !em.getTransaction().isActive() )
      em.getTransaction().begin();
    em.createQuery("DELETE FROM User").executeUpdate();
    em.getTransaction().commit();
  }
}
