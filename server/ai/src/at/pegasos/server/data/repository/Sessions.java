package at.pegasos.server.data.repository;

import java.util.*;

import javax.persistence.*;

import at.pegasos.server.data.model.*;

public class Sessions extends JpaRepository<Session, Long> {
  public Session findById(long ID)
  {
    return em.find(Session.class, ID);
  }

  @Override
  public void deleteAll()
  {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Session").executeUpdate();
    em.getTransaction().commit();
  }

  public List<Session> findByUserid(long userId)
  {
    /*Criteria criteria = em.getCriteriaBuilder().
    return (List<Session>) criteria.add(Restrictions.eq("userid", userId)).list();*/
    /*CriteriaQuery<Session> cr = em.getCriteriaBuilder().createQuery(Session.class);
    Root<Session> root = cr.from(Session.class);
    cr.select(root).where(em.getCriteriaBuilder().equal(root.get("user.id"), userId));

    return HibernateUtil.getSession().createQuery(cr).getResultList();*/
    TypedQuery<Session> q = em.createQuery("FROM Session where user.id = :userid", Session.class);
    q.setParameter("userid", userId);
    return q.getResultList();
  }
}
