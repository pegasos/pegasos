package at.pegasos.server.data.repository;

import javax.persistence.*;

import at.pegasos.server.data.model.*;

class UserValueInfoRepository extends JpaRepository<UserValueInfo, Long> {

  @Override public void delete(UserValueInfo userValueInfo)
  {
    // TODO: document why this is unsupported
    throw new UnsupportedOperationException();
  }

  @Override
  public void deleteAll()
  {
    // TODO: document why this is unsupported
    throw new UnsupportedOperationException();
  }

  public UserValueInfo findByName(String name)
  {
    TypedQuery<UserValueInfo> q = em.createQuery(
            "SELECT v FROM UserValueInfo v WHERE v.name = :name",
            UserValueInfo.class);

    q.setParameter("name", name);

    return q.getSingleResult();
  }

  public UserValueInfo findById(long id)
  {
    return em.find(UserValueInfo.class, id);
  }
}
