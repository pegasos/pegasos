package at.pegasos.server.data.repository;

import java.util.*;

import javax.persistence.*;

import at.pegasos.server.data.model.*;

class UserValueDecRepository extends JpaRepository<UserValueDec, Long> {

  /**
   * Get all entries of a user for a value newest first i.e. in descending order based on the start
   * date
   *
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @return entries
   */
  public List<UserValueDec> findByNameAndUserId(String name, long userid)
  {
    TypedQuery<UserValueDec> q = em.createQuery(
            "SELECT a FROM UserValueDec a JOIN a.value v WHERE v.name = :name AND a.user_id = :userid",
            UserValueDec.class);

    q.setParameter("name", name);
    q.setParameter("userid", userid);

    return q.getResultList();
  }

  @SuppressWarnings("unchecked")
  public Collection<UserValueDec> findByUserDate(Long userid, long timestamp)
  {
    Calendar date= Calendar.getInstance();
    date.setTimeInMillis(timestamp);

    Query q = em.createNativeQuery(
            "SELECT t.* " +
                    "FROM user_value_dec AS t " +
                    "INNER JOIN (" +
                    "   SELECT value_id, MAX(date_start) AS highestValue" +
                    "   FROM user_value_dec" +
                    "   WHERE user_id = :userid AND date_start <= :date AND (date_end >= :date OR date_end is NULL)" +
                    "   GROUP BY value_id" +
                    ") AS rIdent ON rIdent.value_id = t.value_id AND rIdent.highestValue= t.date_start",
            UserValueDec.class);

    q.setParameter("userid", userid);
    q.setParameter("date", date);

    return q.getResultList();
  }

  @Override
  public void deleteAll()
  {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM UserValueInt").executeUpdate();
    em.getTransaction().commit();
  }
}
