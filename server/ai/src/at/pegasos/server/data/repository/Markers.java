package at.pegasos.server.data.repository;

import at.pegasos.server.data.model.*;

import javax.persistence.*;
import java.util.*;

public class Markers extends JpaRepository<Marker, Long> {
  public Marker findById(long ID)
  {
    return em.find(Marker.class, ID);
  }

  @Override
  public void deleteAll()
  {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Marker").executeUpdate();
    em.getTransaction().commit();
  }

  public List<Marker> findBySessionid(long sessionId)
  {
    TypedQuery<Marker> q = em.createQuery("FROM Marker where session_id = :sessionid order by rec_time ASC", Marker.class);
    q.setParameter("sessionid", sessionId);
    return q.getResultList();
  }
}
