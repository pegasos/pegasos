package at.pegasos.server.data.repository;

import javax.persistence.*;

import at.pegasos.server.data.*;

public abstract class JpaRepository<Value, Key> {
  protected final EntityManager em;

  public JpaRepository()
  {
    em = HibernateUtil.getEntityManager();
  }

  public void save(Value value)
  {
    if( !em.getTransaction().isActive() )
      em.getTransaction().begin();
    em.persist(value);
    em.getTransaction().commit();
  }

  public void delete(Value value)
  {
    if( !em.getTransaction().isActive() )
      em.getTransaction().begin();
    em.remove(value);
    em.getTransaction().commit();
  }

  public abstract void deleteAll();
}
