package at.pegasos.server.data.repository;

import org.slf4j.*;

import java.util.*;

import javax.persistence.*;
import javax.persistence.Query;

import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import ch.qos.logback.classic.Logger;

public class UserValues {

  private static UserValues instance;

  private final EntityManager em;

  private final static String findValue = "select v from UserValueInfo v WHERE v.name = :name";
  private final static String avail_valsX =
          "SELECT vi FROM %s vi WHERE vi.user_id = :user AND (vi.date_end is null AND :date >= vi.date_start OR vi.date_end is not null AND :date between vi.date_start AND vi.date_end) AND vi.value.name like :name";

  UserRepository users = new UserRepository();

  UserValueIntRepository ints= new UserValueIntRepository();

  UserValueDecRepository decs= new UserValueDecRepository();

  UserValueInfoRepository infos= new UserValueInfoRepository();

  private UserValues()
  {
    em = HibernateUtil.getEntityManager();
  }

  private final Logger logger= ((Logger) LoggerFactory.getLogger(getClass()));

  public static UserValues getInstance()
  {
    if( instance == null )
      instance= new UserValues();
    
    return instance;
  }

  public UserValue[] getAvailableValues(long user, String pattern, long timestamp)
  {
    Calendar date= Calendar.getInstance();
    date.setTimeInMillis(timestamp);

    List<UserValue> vals = em
            .createQuery(String.format(avail_valsX,"UserValueInt"), UserValue.class)
            .setParameter("user", user)
            .setParameter("name", pattern)
            .setParameter("date", date)
            .getResultList();

    vals.addAll(em
            .createQuery(String.format(avail_valsX,"UserValueDec"), UserValue.class)
            .setParameter("user", user)
            .setParameter("name", pattern)
            .setParameter("date", date)
            .getResultList());

    return vals.toArray(new UserValue[0]);
  }

  public static long getValueInt(long user, String name, long timestamp)
  {
    // return 0;
    throw new UnsupportedOperationException();
  }

  /**
   * Set or update a value for a user for a specific time span
   *
   * @param user
   *          ID of the user
   * @param name
   *          name of the value
   * @param value
   *          actual value
   * @param start
   *          start time of the value
   * @param end
   *          end time of the value
   * @return true if the value was changed
   * @throws UnkownValueException if value info does not exist
   */
  public boolean setValue(long user, String name, double value, long start, Long end) throws UnkownValueException
  {
    // perform update
    boolean legacy= updateLegacy(user, name, value);

    UserValueInfo val= findInfoByName(name);
    if( val == null && !legacy )
      throw new UnkownValueException(name);
    else if( val == null )
    {
      // logger.debug("Changing only legacy value " + name);
      return true;
    }
    else
    {
      logger.debug("Changing value type: {} / {}", val.getType(), name);

      switch( val.getType() )
      {
        case DECIMAL:
          return setValueForUserDec(name, user, value, start, end);
        case INTEGER:
          return setValueForUserInt(name, user, (int) value, start);
        case TEXT:
          // TODO: implement text values
          throw new IllegalArgumentException(val.getType().toString());
        default:
          throw new IllegalArgumentException(val.getType().toString());
      }
    }
  }
  
  /**
   * Set or update a value for a user starting from a specific time
   * 
   * @param user
   *          ID of the user
   * @param name
   *          name of the value
   * @param value
   *          actual value
   * @param start
   *          start time of the value
   * @return true if the value was changed
   * @throws UnkownValueException if value info does not exist
   */
  public boolean setValue(long user, String name, double value, long start) throws UnkownValueException
  {
    return setValue(user, name, value, start, null);
  }

  /**
   * Find the information about a value by its name
   * @param name name of the value to be found
   * @return the value or null if it does not exist
   */
  public UserValueInfo findInfoByName(String name)
  {
    try
    {
      return (UserValueInfo) em
              .createQuery(findValue)
              .setParameter("name", name)
              .getSingleResult();
    }
    catch(NoResultException e)
    {
      logger.debug("No value with name '{}' found", name);
      return null;
    }
  }

  private boolean updateLegacy(long user, String name, double value)
  {
    // Legacy changes
    if( name.equals("WEIGHT") )
    {
      changeWeight(user, value);
      return true;
    }
    else if( name.equals("HEIGHT") )
    {
      changeHeight(user, value);
      return true;
    }
    else if( name.equals("VO2MAX") || name.toUpperCase().equals("VO2MAX") )
    {
      changeData(1, user, value);
      return true;
    }
    else if( name.equals("MAXHR") || name.toUpperCase().equals("MAX_HR") )
    {
      changeData(2, user, value);
      return true;
    }
    else if( name.equals("FP_CALIB") )
    {
      changeData(3, user, value);
      return true;
    }
    else if( name.equals("RESTHR") )
    {
      changeData(4, user, value);
      return true;
    }
    return false;
  }

  public boolean setValueForUserDec(String name, long userid, double value, long start, Long end)
  {
    try
    {
      Calendar cstart = Calendar.getInstance();
      cstart.setTimeInMillis(start);
      Calendar cend = null;
      if( end != null )
      {
        cend = Calendar.getInstance();
        cend.setTimeInMillis(end);
      }

      List<UserValueDec> oldvalues= decs.findByNameAndUserId(name, userid);

      logger.debug("#old values for {} and user {} @start: {}: {}", name, userid, start, oldvalues.size());

      if( oldvalues.size() > 0 )
      {
        // now we need to find the value that is just before our start date
        UserValueDec cur= null;
        // TODO: after has been >= before
        while( oldvalues.size() > 0 &&
            ( oldvalues.get(0).date_end == null && oldvalues.get(0).date_start.after(start) ||
                oldvalues.get(0).date_end != null && oldvalues.get(0).date_start.after(start) ) )
        {
          cur= oldvalues.remove(0);
        }

        UserValueDec old= null;
        if( oldvalues.size() > 0 )
          old= oldvalues.get(0);

        if( old != null && (old.date_end == null || old.date_end.after(start) ) )
        {
          Calendar x= Calendar.getInstance();
          x.setTimeInMillis(start - 1);
          logger.debug("changing old value: {} {}", old, x.getTime());

          old.date_end= x;

          logger.debug("Update: {}", old);
          decs.save(old);
        }
        else
          logger.debug("No need to update the old value");

        // do we have to update an 'old' value that is in the future (for the current operation)?
        if( cur == null )
        {
          // no. Create a new value
          logger.debug("No value in the future. No changes. Just insert");
          return insertXXX(old != null ? old.getValue() : infos.findByName(name), value, userid, cstart, cend);
        }
        else
        {
          logger.debug("Have a cur value. start {} end {}, cur.start {} cur.end {}", start, end, cur.date_start, cur.date_end);
          // Is this operation actually an update operation? (ie we are changing something we have set before)
          if( cur.date_start.compareTo(cstart) == 0 && (end == null && cur.date_end == null || end != null && cur.date_end.compareTo(cend) == 0 ) )
          {
            logger.debug("Updating old = current value");
            cur.internal= value;
            decs.save(cur);
            return true;
          }
          else
          {
            // there is a value that could be us or in the future
            if( end == null )
            {
              // We are open ended
              logger.debug("Value in the future. Insert with modified enddate");
              Calendar x= Calendar.getInstance();
              x.setTimeInMillis(cur.date_start.getTimeInMillis() + 1);
              return insertXXX(cur.getValue(), value, userid, cstart, x);
            }
            else
            {
              // we have an end date. Are we overlapping?
              if( cend.after(cur.date_start) )
              {
                if( cend.after(cur.date_end) )
                {
                  decs.delete(cur);
                }
                else
                {
                  Calendar x= Calendar.getInstance();
                  x.setTimeInMillis(end + 1);
                  cur.date_start= x;
                  decs.save(cur);
                }
                return insertXXX(cur.getValue(), value, userid, cstart, cend);
              }
              else
              {
                // we do not overlap
                logger.debug("Value in the future. But no conflicts");
                return insertXXX(cur.getValue(), value, userid, cstart, cend);
              }
            }
          }
        }
      }
      else
      {
        return insertXXX(infos.findByName(name), value, userid, cstart, cend);
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      return false;
    }
  }

  private boolean insertXXX(UserValueInfo val, double value, long userid, Calendar start, Calendar end)
  {
    UserValueDec newval= new UserValueDec();
    newval.date_start= start;
    newval.date_end= end;
    newval.internal= value;
    newval.user_id= userid;
    newval.setValue(val);

    logger.debug("Persit: {}", newval);
    decs.save(newval);

    return true;
  }

  public boolean setValueForUserInt(String name, long userid, int value)
  {
    return setValueForUserInt(name, userid, value, Calendar.getInstance().getTimeInMillis());
  }
  
  public boolean setValueForUserInt(String name, long userid, int value, long start)
  {
    List<UserValueInt> oldvalues= ints.findByNameAndUserId(name, userid);
    UserValueInfo val;
    
    logger.debug("#old values for " + name + " and user " + userid + ": " + oldvalues.size());
    
    if( oldvalues.size() > 0 )
    {
      UserValueInt old= oldvalues.get(0);
      
      logger.debug("#old values enddate: " + old.date_end);
      if( old.date_end == null )
      {
        Calendar x= Calendar.getInstance();
        x.setTimeInMillis(start - 1);
        old.date_end= x;

        ints.save(old);
      }

      val= old.getValue();
    }
    else
    {
      val= infos.findByName(name);
    }

    Calendar x= Calendar.getInstance();
    x.setTimeInMillis(start);

    UserValueInt newval= new UserValueInt();
    newval.date_start= x;
    newval.internal= value;
    newval.user_id= userid;
    newval.setValue(val);

    ints.save(newval);

    return true;
  }
  
  /**
   * Get all current values for a user
   * 
   * @param user
   *          ID of the user
   * @return all values for the user
   */
  public List<UserValue> getForUserDate(Long user, long date)
  {
    Collection<UserValueDec> d= decs.findByUserDate(user, date);
    Collection<UserValueInt> i= ints.findByUserDate(user, date);
    
    List<UserValue> ret= new ArrayList<UserValue>(d.size() + i.size());
    ret.addAll(d);
    ret.addAll(i);
    
    return ret;
  }
  
  /**
   * Change legacy data
   * <ul><li>1: VO2Max</li><li>2: Max Heartrate</li><li>3: Footpod calibration factor</li><li>4: resting heart rate</li></ul>
   * @param idx index / value number to be changed
   * @param userid user
   * @param value value
   */
  private void changeData(int idx, long userid, double value)
  {
    long time= System.currentTimeMillis() / 1000;

    em.getTransaction().begin();
    @SuppressWarnings("DefaultLocale")
    String qr= String.format("UPDATE user set data%d = :value, data%d_crtime = :time where id = :user", idx, idx);
    Query query = em.createNativeQuery(qr);
    query.setParameter("value", value);
    query.setParameter("time", time);
    query.setParameter("userid", userid);
    query.executeUpdate();
    em.getTransaction().commit();
  }
  
  /**
   * Change legacy weight
   * 
   * @param userid id of the user for which the weight will be performed
   * @param weight weight
   */
  private void changeWeight(long userid, double weight)
  {
    User u = users.findById(userid);
    u.weight = weight;
    users.save(u);
  }
  
  /**
   * Change legacy weight
   * 
   * @param userid id
   * @param height value
   */
  private void changeHeight(long userid, double height)
  {
    User u = users.findById(userid);
    u.height = height;
    users.save(u);
    System.out.println(users.findById(userid));
  }

  /**
   * Invalidate (i.e.) delete a value for a user
   * @param name name of the value to be invalidated 
   * @param userid id
   * @param date date on which the operation should take place
   * @throws UnkownValueException if value info does not exist
   */
  public void invalidateValueForUser(String name, long userid, Calendar date) throws UnkownValueException
  {
    UserValueInfo val= infos.findByName(name);
    if( val == null )
      throw new UnkownValueException(name);
    else
    {
      logger.debug("Changing value type: {} / {}", val.getType(), name);
      
      switch( val.getType() )
      {
        case DECIMAL:
        {
          List<UserValueDec> oldvalues= decs.findByNameAndUserId(name, userid);
          
          logger.debug("#old values for " + name + " and user " + userid + ": " + oldvalues.size());
          
          if( oldvalues.size() > 0 )
          {
            UserValueDec old= oldvalues.get(0);
            
            logger.debug("#old values enddate: " + old.date_end);
            if( old.date_end == null )
            {
              Calendar x= Calendar.getInstance();
              x.setTimeInMillis(date.getTimeInMillis() - 1);
              old.date_end= x;

              decs.save(old);
            }
          }
          break;
        }
          
        case INTEGER:
        {
          List<UserValueInt> oldvalues= ints.findByNameAndUserId(name, userid);
          
          logger.debug("#old values for " + name + " and user " + userid + ": " + oldvalues.size());
          
          if( oldvalues.size() > 0 )
          {
            UserValueInt old= oldvalues.get(0);
            
            logger.debug("#old values enddate: " + old.date_end);
            if( old.date_end == null )
            {
              Calendar x= Calendar.getInstance();
              x.setTimeInMillis(date.getTimeInMillis() - 1);
              old.date_end= x;

              ints.save(old);
            }
          }
          break;
        }
        case TEXT:
          // TODO: implement text values
          throw new IllegalArgumentException(val.getType().toString());
        default:
          throw new IllegalArgumentException(val.getType().toString());
      }
    }
  }

  public void save(UserValueInfo valTest)
  {
    infos.save(valTest);
  }
}
