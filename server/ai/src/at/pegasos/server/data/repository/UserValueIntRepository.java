package at.pegasos.server.data.repository;

import java.util.*;

import javax.persistence.*;

import at.pegasos.server.data.model.*;

class UserValueIntRepository extends JpaRepository<UserValueInt, Long> {

  /**
   * Get all entries of a user for a value newest first i.e. in descending order based on the start
   * date
   * 
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @return entries
   */
  public List<UserValueInt> findByNameAndUserId(String name, long userid)
  {
    TypedQuery<UserValueInt> q = em.createQuery(
            "SELECT a FROM UserValueInt a JOIN a.value v WHERE v.name = :name AND a.user_id = :userid",
            UserValueInt.class);

    q.setParameter("name", name);
    q.setParameter("userid", userid);

    return q.getResultList();
  }

  @SuppressWarnings("unchecked")
  public Collection<UserValueInt> findByUserDate(Long userid, long timestamp)
  {
    Calendar date= Calendar.getInstance();
    date.setTimeInMillis(timestamp);

    Query q = em.createNativeQuery(
            "SELECT t.* " +
                    "FROM user_value_int AS t " +
                    "INNER JOIN (" +
                    "   SELECT value_id, MAX(date_start) AS highestValue" +
                    "   FROM user_value_int" +
                    "   WHERE user_id = :userid AND date_start <= :date AND (date_end >= :date OR date_end is NULL)" +
                    "   GROUP BY value_id" +
                    ") AS rIdent ON rIdent.value_id = t.value_id AND rIdent.highestValue= t.date_start",
            UserValueInt.class);

    q.setParameter("userid", userid);
    q.setParameter("date", date);

    return q.getResultList();
  }

  @Override
  public void deleteAll()
  {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM UserValueInt").executeUpdate();
    em.getTransaction().commit();
  }
}
