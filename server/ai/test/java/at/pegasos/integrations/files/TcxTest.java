package at.pegasos.integrations.files;

import at.pegasos.ai.*;
import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import at.pegasos.server.data.repository.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.*;

import javax.persistence.*;
import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TcxTest {

  private static User user;

  @BeforeAll public static void setUp() throws SQLException
  {
    TestDatabase.setup();
    HibernateUtilTest.setup();
    user = UserRepositoryTest.createTestUser();
  }

  @AfterAll public static void tearDown()
  {
    new UserRepository().deleteAll();
    new Sessions().deleteAll();
  }

  @Test void testRunning() throws SQLException, ImportFailedException
  {
    Tcx tcxImport = new Tcx();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("running.TCX").getFile();
    p.noPost = true;

    System.out.println(user);

    tcxImport.setParameters(p);
    tcxImport.importFile(p.input);

    long sessionId = tcxImport.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();

    assertEquals(before + 1, after);

    Query q = HibernateUtil.OpenOrGetSession().createNativeQuery("SELECT count(*), avg(hr), min(hr), max(hr) FROM hr WHERE session_id = :sid");
    q.setParameter("sid", sessionId);
    Object[] o = (Object[]) q.getSingleResult();
    int count = ((BigInteger) o[0]).intValue();
    // System.out.println(o[0] + " " + o[1] + " " + o[2] + " " + o[3] + " " + o[0].getClass() + " " + o[1].getClass() + " " + o[2].getClass() + " " + o[3].getClass());
    double avg = (Short) o[1];
    double min = (Short) o[2];
    double max = (Short) o[3];

    assertEquals(390, count);
    assertEquals(114, avg);
    assertEquals(71, min);
    assertEquals(133, max);

    q= HibernateUtil.OpenOrGetSession().createNativeQuery("SELECT count(*), avg(speed_mm_s), min(speed_mm_s), max(speed_mm_s) FROM foot_pod WHERE session_id = :sid");
    q.setParameter("sid", sessionId);
    o = (Object[]) q.getSingleResult();
    count = ((BigInteger) o[0]).intValue();
    // System.out.println(o[0] + " " + o[1] + " " + o[2] + " " + o[3] + " " + o[0].getClass() + " " + o[1].getClass() + " " + o[2].getClass() + " " + o[3].getClass());
    avg = (Short) o[1];
    min = (Short) o[2];
    max = (Short) o[3];

    assertEquals(390, count);
    assertEquals(2046, avg);
    assertEquals(0, min);
    assertEquals(4700, max);

    new Sessions().delete(new Sessions().findById(sessionId));
  }

  @Test void importing_twice_does_not_create_new_session() throws SQLException, ImportFailedException
  {
    Tcx tcxImport = new Tcx();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("running.TCX").getFile();
    p.noPost = true;

    tcxImport.setParameters(p);

    // first import should create a session
    tcxImport.importFile(p.input);
    long sessionId1 = tcxImport.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);

    // second import should not create a new session
    tcxImport.importFile(p.input);
    long sessionId2 = tcxImport.getSessionId();

    after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);
    assertEquals(sessionId1, sessionId2);

    new Sessions().delete(new Sessions().findById(sessionId1));
  }

  @Test public void failing_import(@TempDir Path tempDir) throws IOException, SQLException
  {
    BufferedWriter writer = Files.newBufferedWriter(tempDir.resolve("test.xml"));
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<TrainingCenterDatabase xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\">\n" +
            "<Activities>\n" +
            "    <Activity Sport=\"Running\">\n" +
            "        <Id>2018-07-30T12:22:21.000Z</Id>\n" +
            "        <Lap StartTime=\"2018-07-30T12:22:22.000Z\">\n" +
            "            <TotalTimeSeconds>154.0</TotalTimeSeconds>\n" +
            "            <DistanceMeters>416.3999938964844</DistanceMeters>\n" +
            "            <MaximumSpeed>3.4999998410542803</MaximumSpeed>\n" +
            "            <Calories>66</Calories>\n" +
            "            <AverageHeartRateBpm><Value>119</Value></AverageHeartRateBpm>\n" +
            "            <MaximumHeartRateBpm><Value>133</Value></MaximumHeartRateBpm>\n" +
            "            <Intensity>Active</Intensity>\n" +
            "            <Cadence>74</Cadence>\n" +
            "            <TriggerMethod>Manual</TriggerMethod>\n" +
            "            <Track>\n" +
            "                <Trackpoint><Time>2018-07-30T12:22:22.000Z</Time><Position><LatitudeDegrees>48.205255</LatitudeDegrees><LongitudeDegrees>16.31761667</LongitudeDegrees></Position><AltitudeMeters>x</AltitudeMeters><DistanceMeters>0.0</DistanceMeters><HeartRateBpm><Value>71</Value></HeartRateBpm><Cadence>B</Cadence><SensorState>Present</SensorState></Trackpoint>\n"+
            "            </Track>\n" +
            "        </Lap>\n" +
            "    </Activity>\n" +
            "</Activities>\n" +
            "</TrainingCenterDatabase>");
    writer.close();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = tempDir.resolve("test.xml").toString();
    p.noPost = true;

    Tcx tcxImport = new Tcx();
    tcxImport.setParameters(p);

    try
    {
      tcxImport.importFile(p.input);
      fail();
    }
    catch( ImportFailedException | NumberFormatException e )
    {
      assertEquals(before, new Sessions().findByUserid(user.id).size());
    }
  }

  @Test
  void startTime() throws ImportFailedException
  {
    assertEquals(1532953341000L, Tcx.startTime(this.getClass().getClassLoader().getResource("running.TCX").getFile()));
  }
}
