package at.pegasos.integrations.files;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.*;

import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.sql.*;
import java.time.*;
import java.util.*;

import javax.persistence.*;

import at.pegasos.ai.*;
import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import at.pegasos.server.data.repository.*;

import static org.junit.jupiter.api.Assertions.*;

class PwxTest {

  private static User user;

  @BeforeAll public static void setUp() throws SQLException
  {
    HibernateUtilTest.setup();
    TestDatabase.setup();

    user = UserRepositoryTest.createTestUser();
  }

  @AfterAll public static void tearDown()
  {
    new UserRepository().deleteAll();
    new Sessions().deleteAll();
  }

  @Test void testSample() throws SQLException, ImportFailedException
  {
    Pwx pwxImport = new Pwx();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("test.pwx").getFile();
    p.noPost = true;

    pwxImport.setParameters(p);
    pwxImport.importFile(p.input);

    long sessionId = pwxImport.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();

    assertEquals(before + 1, after);

    new Sessions().delete(new Sessions().findById(sessionId));
  }

  @Test void importing_twice_does_not_create_new_session() throws SQLException, ImportFailedException
  {
    Pwx pwxImport = new Pwx();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("test.pwx").getFile();
    p.noPost = true;

    pwxImport.setParameters(p);

    // first import should create a session
    pwxImport.importFile(p.input);
    long sessionId1 = pwxImport.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);

    // second import should not create a new session
    pwxImport.importFile(p.input);
    long sessionId2 = pwxImport.getSessionId();

    after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);
    assertEquals(sessionId1, sessionId2);

    new Sessions().delete(new Sessions().findById(sessionId1));
  }

  @Test
  void startTime() throws ImportFailedException
  {
    assertEquals(1580725860000L, Pwx.startTime(this.getClass().getClassLoader().getResource("test.pwx").getFile()));
  }
}
