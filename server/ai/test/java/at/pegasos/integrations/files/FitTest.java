package at.pegasos.integrations.files;

import at.pegasos.ai.*;
import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import at.pegasos.server.data.repository.*;
import org.junit.jupiter.api.*;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

class FitTest {

  private static User user;

  @BeforeAll public static void setUp() throws SQLException
  {
    HibernateUtilTest.setup();
    TestDatabase.setup();

    user = UserRepositoryTest.createTestUser();
  }

  @AfterAll public static void tearDown()
  {
    new UserRepository().deleteAll();
    new Sessions().deleteAll();
  }

  @Test void testSample() throws SQLException, ImportFailedException
  {
    System.out.println("testSample start user:" + user);
    Fit fit = new Fit();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("PogMV6mK.fit").getFile();
    p.noPost = true;

    fit.setParameters(p);
    fit.importFile(p.input);

    long sessionId = fit.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();

    assertEquals(before + 1, after);

    new Sessions().delete(new Sessions().findById(sessionId));
    System.out.println("testSample end " + sessionId);
    new Sessions().deleteAll();
  }

  @Test void importing_twice_does_not_create_new_session() throws SQLException, ImportFailedException
  {
    System.out.println("importing_twice_does_not_create_new_session start");
    Fit fit = new Fit();

    int before = new Sessions().findByUserid(user.id).size();

    ImportTool.Parameters p = new ImportTool.Parameters();
    p.userId = user.id;
    p.input = this.getClass().getClassLoader().getResource("PogMV6mK.fit").getFile();
    p.noPost = true;

    fit.setParameters(p);

    System.out.println("Run import for " + new UserRepository().findById(1));
    // first import should create a session
    fit.importFile(p.input);
    long sessionId1 = fit.getSessionId();

    int after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);

    // second import should not create a new session
    fit.importFile(p.input);
    long sessionId2 = fit.getSessionId();

    after = new Sessions().findByUserid(user.id).size();
    assertEquals(before + 1, after);
    assertEquals(sessionId1, sessionId2);

    new Sessions().delete(new Sessions().findById(sessionId1));
    System.out.println("importing_twice_does_not_create_new_session end");
  }

  @Test
  void startTime() throws SQLException
  {
    System.out.println("startTime start");
    Fit fit = new Fit();
    fit.parameters = new ImportTool.Parameters();
    fit.parameters.input = (this.getClass().getClassLoader().getResource("PogMV6mK.fit").getFile());
    assertEquals(1662027085000L, fit.getStartDate());
    System.out.println("startTime end");
    new Sessions().deleteAll();
  }
}
