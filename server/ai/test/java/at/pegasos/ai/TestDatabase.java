package at.pegasos.ai;

import org.apache.ibatis.jdbc.*;

import java.io.*;
import java.sql.*;

import at.pegasos.ai.util.log.*;
import at.univie.mma.ai.*;

/**
 * Database access. This can be a singleton for AI modules as each module is currently running in an isolated jvm
 */
public class TestDatabase extends Database {

  public static void setup() throws SQLException
  {
    inst = new TestDatabase();
    ((TestDatabase) inst).open_db();

    ScriptRunner sr = null;
    try
    {
      sr = new ScriptRunner(inst.cn);
      //Creating a reader object
      Reader reader = new BufferedReader(new FileReader("../model-ansi.sql"));
      //Running the script
      sr.runScript(reader);

      sr = new ScriptRunner(inst.cn);
      //Creating a reader object
      reader = new BufferedReader(new FileReader(new File(sr.getClass().getClassLoader().getResource("data-tables-ansi.sql").getFile())));
      //Running the script
      sr.runScript(reader);
    }
    catch( FileNotFoundException throwables )
    {
      throwables.printStackTrace();
    }
  }

  @Override
  protected Database newInstance()
  {
    return new TestDatabase();
  }

  @Override
  protected void open_db() throws SQLException
  {
    cn = DriverManager.getConnection("jdbc:h2:mem:myDb;DB_CLOSE_DELAY=-1");
  }
}
