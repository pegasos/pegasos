package at.pegasos.server.data.repository;

import org.junit.jupiter.api.*;

import java.nio.charset.*;
import java.util.*;

import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;

import static org.junit.jupiter.api.Assertions.*;

public class UserRepositoryTest {

  @BeforeAll
  public static void setUp()
  {
    HibernateUtilTest.setup();
  }

  @AfterEach
  public void afterTestCase()
  {
    // new Sessions().deleteAll();
    new UserRepository().deleteAll();
  }

  public static User createTestUser()
  {
    User user = new User();

    byte[] array = new byte[7]; // length is bounded by 7
    new Random().nextBytes(array);
    String generatedString = new String(array, StandardCharsets.UTF_8);

    user.email = generatedString + "@a.at";
    user.firstname = user.lastname = generatedString;
    user.password_1250 = "1234";
    user.salt_0954 = "5678";

    UserRepository rep = new UserRepository();
    rep.save(user);

    return user;
  }

  @Test
  public void findByEmail_retrieves_correct()
  {
    User u = new User();
    UserRepository rep = new UserRepository();
    u.firstname = u.lastname = "a";
    u.email = "a@a1.at";
    u.password_1250 = "1234";
    u.salt_0954 = "5678";

    rep.save(u);
    System.out.println("Saved u");

    User u2 = new User();
    u2.firstname = u2.lastname = "b";
    u2.email = "a@b1.at";
    u2.password_1250 = "1234";
    u2.salt_0954 = "5678";

    rep.save(u2);

    assertEquals(u, rep.findByEmail("a@a1.at"));
  }

  @Test
  public void email_unique_respected()
  {
    User u = new User();
    UserRepository rep = new UserRepository();
    u.firstname = u.lastname = "a";
    u.email = "a@c.at";
    u.password_1250 = "1234";
    u.salt_0954 = "5678";
    rep.save(u);

    User u2 = new User();
    u2.firstname = u2.lastname = "b";
    u2.email = "a@c.at";
    u2.password_1250 = "1234";
    u2.salt_0954 = "5678";

    Assertions.assertThrows(javax.persistence.PersistenceException.class, () -> rep.save(u2));
    HibernateUtil.getEntityManager().getTransaction().rollback();
  }

  @Test
  public void findByFirstname()
  {
    System.out.println("findByFirstname");
    User u = new User();
    UserRepository rep = new UserRepository();
    System.out.println(rep.findByFirstname("a"));
    u.firstname = u.lastname = "a";
    u.email = "a@d.at";
    u.password_1250 = "1234";
    u.salt_0954 = "5678";
    rep.save(u);

    User u2 = new User();
    u2.firstname = u2.lastname = "a";
    u2.email = "a@e.at";
    u2.password_1250 = "1234";
    u2.salt_0954 = "5678";
    rep.save(u2);

    assertEquals(2, rep.findByFirstname("a").size());
    rep.delete(u);
    rep.delete(u2);
  }

  @Test public void delete_works()
  {
    User u = createTestUser();
    String mail = u.email;
    UserRepository rep = new UserRepository();
    Assertions.assertNotNull(rep.findByEmail(mail));

    rep.delete(u);

    Assertions.assertNull(rep.findByEmail(mail));
  }

  @Test public void deleteAll_works()
  {
    User u = createTestUser();
    String mail = u.email;
    UserRepository rep = new UserRepository();
    Assertions.assertNotNull(rep.findByEmail(mail));

    rep.deleteAll();

    Assertions.assertNull(rep.findByEmail(mail));
  }
}
