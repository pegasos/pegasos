package at.pegasos.server.data;

import org.hibernate.cfg.Configuration;

public class HibernateUtilTest extends HibernateUtil {

  @Override  protected void configure()
  {
    cfg = new Configuration()
            .setProperty("hibernate.connection.driver_class", "org.h2.Driver")
            .setProperty("hibernate.connection.url", "jdbc:h2:mem:myDb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false")
            .setProperty("javax.persistence.schema-generation.database.action", "none")
            .setProperty("hibernate.show_sql", "true")
            .setProperty("org.hibernate.type.descriptor.sql","trace")
            .addAnnotatedClass(at.pegasos.server.data.model.Session.class)
            .addAnnotatedClass(at.pegasos.server.data.model.SessionMetric.class)
            .addAnnotatedClass(at.pegasos.server.data.model.SessionMetricInfo.class)
            .addAnnotatedClass(at.pegasos.server.data.model.User.class)
            .addAnnotatedClass(at.pegasos.server.data.model.UserValueInfo.class)
            .addAnnotatedClass(at.pegasos.server.data.model.UserValueInt.class)
            .addAnnotatedClass(at.pegasos.server.data.model.UserValueDec.class)
            .addAnnotatedClass(at.pegasos.server.data.model.UserValue.class)
    ;
  }

  public static void setup()
  {
    if( instance == null )
      instance = new HibernateUtilTest();
  }
}
