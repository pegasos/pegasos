package at.pegasos.server.data.repository;

import at.pegasos.server.data.*;
import at.pegasos.server.data.model.*;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class UserValuesTest {

  @BeforeAll
  public static void setUp()
  {
    HibernateUtilTest.setup();
  }

  @AfterEach
  public void afterTestCase()
  {
    new UserRepository().deleteAll();
  }

  @Test
  public void saveAndRetrieveValueInfo()
  {
    UserValueInfo valTest = createTestValue();
    assertEquals(valTest, UserValues.getInstance().findInfoByName("test" + (idx - 1)));
  }

  static int idx = 1;
  private UserValueInfo createTestValue()
  {
    UserValueInfo valTest = new UserValueInfo();
    valTest.name = "test" + idx++;
    valTest.setType(UserValueInfo.ValueType.INTEGER);
    UserValues.getInstance().save(valTest);
    return valTest;
  }

  private User createTestUser()
  {
    User u = new User();
    UserRepository rep = new UserRepository();
    u.firstname = u.lastname = u.email = "";
    rep.save(u);
    return u;
  }

  @Test
  public void addForUser_addsValue()
  {
    User u = UserRepositoryTest.createTestUser();
    UserValueInfo valTest = createTestValue();

    try
    {
      UserValues.getInstance().setValue(u.id, valTest.name, 1, System.currentTimeMillis() - 100);

      UserValue[] vals = UserValues.getInstance().getAvailableValues(u.id, valTest.name, System.currentTimeMillis());
      assertEquals(1, vals.length, "One value expected");
      assertEquals(valTest, vals[0].getValue(), "Is test value");
      assertEquals(u.id, vals[0].user_id, "Value is set for user 1");
      assertNull(vals[0].date_end, "No end date");

      List<UserValue> vals2 = UserValues.getInstance().getForUserDate(u.id, System.currentTimeMillis());
      assertEquals(1, vals2.size(), "One value expected");
      assertEquals(valTest, vals2.get(0).getValue(), "Is test value");
      assertEquals(u.id, vals2.get(0).user_id, "Value is set for user 1");
      assertNull(vals2.get(0).date_end, "No end date");

    }
    catch (UnkownValueException e)
    {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void deleteForUser_deletesValue()
  {
    User u = UserRepositoryTest.createTestUser();

    // create two values
    UserValueInfo valTest1 = createTestValue();
    UserValueInfo valTest2 = createTestValue();

    try
    {
      // set values for the two values
      UserValues.getInstance().setValue(u.id, valTest1.name, 1, System.currentTimeMillis() - 1);
      UserValues.getInstance().setValue(u.id, valTest2.name, 2, System.currentTimeMillis() - 1);

      // safety check ...
      UserValue[] vals = UserValues.getInstance().getAvailableValues(u.id, "%", System.currentTimeMillis());
      assertEquals(2, vals.length, "Two values expected");

      // delete value1
      UserValues.getInstance().invalidateValueForUser(valTest1.name, u.id, Calendar.getInstance());

      vals = UserValues.getInstance().getAvailableValues(u.id, "%", System.currentTimeMillis());

      assertEquals(1, vals.length, "One value expected");
      assertEquals(valTest2, vals[0].getValue(), "Is test value");
      assertEquals(u.id, vals[0].user_id, "Value is set for user 1");
      assertNull(vals[0].date_end, "No end date");
    }
    catch (UnkownValueException e)
    {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void changingWeight_works()
  {
    User u = UserRepositoryTest.createTestUser();
    UserRepository rep = new UserRepository();
    final double weight = 75;

    try
    {
      UserValues.getInstance().setValue(u.id, "WEIGHT", weight, System.currentTimeMillis());
      u = rep.findById(u.id);

      assertEquals(weight, u.weight, "weight does not match");
    }
    catch (UnkownValueException e)
    {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void changingHeight_works()
  {
    User u = UserRepositoryTest.createTestUser();
    long id = u.id;
    UserRepository rep = new UserRepository();
    final double weight = 75;

    try
    {
      UserValues.getInstance().setValue(u.id, "HEIGHT", weight, System.currentTimeMillis());
      u = rep.findById(id);

      assertEquals(weight, u.height, "height does not match");
    }
    catch (UnkownValueException e)
    {
      e.printStackTrace();
      fail();
    }
  }



  @Test
  public void changingWeightHeight_works()
  {
    User u = UserRepositoryTest.createTestUser();
    UserRepository rep = new UserRepository();
    final double weight = 75;
    final double height = 175;

    try
    {
      UserValues.getInstance().setValue(u.id, "WEIGHT", weight, System.currentTimeMillis());
      u = rep.findById(u.id);

      assertEquals(weight, u.weight, "weight does not match");

      UserValues.getInstance().setValue(u.id, "HEIGHT", height, System.currentTimeMillis());
      u = rep.findById(u.id);

      assertEquals(height, u.height, "height does not match");
    }
    catch (UnkownValueException e)
    {
      e.printStackTrace();
      fail();
    }
  }
}
