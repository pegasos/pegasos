CREATE TABLE `bike` (
  `bike_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `sensor_nr` smallint(6) DEFAULT NULL,
  `rec_time` bigint(13) NOT NULL,
  `speed_mm_s` int(11) DEFAULT NULL,
  `distance_m` int(11) DEFAULT NULL,
  `cadence` int(11) DEFAULT NULL,
  `power` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`bike_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `foot_pod` (
  `foot_pod_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `sensor_nr` smallint(6) NOT NULL DEFAULT 1,
  `rec_time` bigint(13) NOT NULL,
  `speed_mm_s` smallint(6) NOT NULL,
  `distance_m` int(11) NOT NULL,
  `strides_255` smallint(6) DEFAULT NULL,
  `calories` int(11) NOT NULL,
  `cadence` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`foot_pod_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `gps` (
  `gps_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `sensor_nr` smallint(6) DEFAULT NULL,
  `rec_time` bigint(13) NOT NULL,
  `alt_cm` mediumint(9) NOT NULL,
  `speed_cm_s` smallint(6) NOT NULL,
  `acc` smallint(6) NOT NULL,
  `bearing` smallint(6) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `distance_m` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`gps_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `hr` (
  `hr_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint(11) unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  `hr` smallint(6) NOT NULL,
  `hr_var` smallint(6) NOT NULL,
  `v1` smallint(5) unsigned DEFAULT NULL,
  `v2` smallint(5) unsigned DEFAULT NULL,
  `v3` smallint(5) unsigned DEFAULT NULL,
  `v4` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`hr_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `marker`;
CREATE TABLE `marker` (
  `marker_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  PRIMARY KEY (`marker_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `ambience`;
CREATE TABLE `ambience` (
  `ambience_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  `temperature` int NOT NULL,
  `pressure` int NOT NULL,
  UNIQUE KEY `ambience_id` (`ambience_id`),
  FOREIGN KEY (`session_id`) REFERENCES session (session_id) ON DELETE CASCADE ON UPDATE CASCADE
);

