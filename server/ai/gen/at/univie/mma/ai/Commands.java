package at.univie.mma.ai;


public class Commands {
  public static final int OK= 1;
  public static final int FAIL= 0;
  public static class Codes {
    public static final int GroupActivity= 6;
    public static final int TimeDiff= 7;
    public static final int AIMessage= 8;
    public static final int SessionFeedback= 10;
  };
}
// End of generated class

