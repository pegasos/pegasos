package at.univie.mma.ai;

public class Config {
  public final static String LOG_FILE = "ai_pegasos.log";
  public static final String DB_DRIVER = "org.mariadb.jdbc.Driver";
  public static final String DB_URL = "jdbc:mariadb://localhost:3306/pegasos?useJvmCharsetConverters=true";
  public static final String DB_USER = "write";
  public static final String DB_PASSWORD = "password";
  public static final String BACKEND_JAR = "server_pegasos.jar";
}
// End of generated class
