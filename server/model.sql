--
-- Table structure for table `club_codes`
--

DROP TABLE IF EXISTS `club_codes`;
CREATE TABLE `club_codes` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `used` boolean NOT NULL DEFAULT false,
  `usetime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint NOT NULL,
  `forename` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` int(11) NOT NULL DEFAULT '0',
  `height` varchar(255) NOT NULL DEFAULT '0',
  `weight` varchar(255) NOT NULL DEFAULT '0',
  `gender` int(11) NOT NULL DEFAULT '1',
  `nationality` varchar(3),
  `hidden` int(11) NOT NULL DEFAULT '0',
  `password_1250` varchar(255) NOT NULL,
  `salt_0954` varchar(255) NOT NULL,
  `data1` varchar(255) NOT NULL DEFAULT '0',
  `data2` varchar(255) NOT NULL DEFAULT '0',
  `data3` varchar(255) NOT NULL DEFAULT '1000',
  `data4` varchar(255) NOT NULL DEFAULT '0',
  `base_heart_rate` smallint(3) NOT NULL DEFAULT '0',
  `data5` varchar(255) NOT NULL DEFAULT '0',
  `data1_crtime` int(11) NOT NULL default 0,
  `data2_crtime` int(11) NOT NULL default 0,
  `data3_crtime` int(11) NOT NULL default 0,
  `data4_crtime` int(11) NOT NULL default 0,
  `data5_crtime` int(11) NOT NULL default 0,
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `numberoflogins` int(10) NOT NULL DEFAULT '0',
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_id` (`id`)
);

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `userid` BIGINT UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_type` (`type`),
  KEY `mc_userid_2` (`userid`),
  CONSTRAINT `mc_teams_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `teams_assignments`
--

/*DROP TABLE IF EXISTS `teams_assignments`;
CREATE TABLE `teams_assignments` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `teamid` int(11) DEFAULT NULL,
  `msg` text NOT NULL,
  `crtime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_teamid` (`teamid`),
  CONSTRAINT `mc_teams_assignments_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_teams_assignments_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/
--
-- Table structure for table `teams_groups`
--

DROP TABLE IF EXISTS `teams_groups`;
CREATE TABLE `teams_groups` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED DEFAULT NULL,
  `teamid` INT UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_teamid` (`teamid`),
  CONSTRAINT `mc_teams_groups_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_teams_groups_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `teams_invites`
--

DROP TABLE IF EXISTS `teams_invites`;
CREATE TABLE `teams_invites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED DEFAULT NULL,
  `teamid` INT UNSIGNED DEFAULT NULL,
  `invited_userid` int(10) NOT NULL,
  `accepted` boolean NOT NULL DEFAULT false,
  `declined` boolean NOT NULL DEFAULT false,
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_teamid` (`teamid`),
  CONSTRAINT `mc_teams_invites_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_teams_invites_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `teams_memberships`
--

DROP TABLE IF EXISTS `teams_memberships`;
CREATE TABLE `teams_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED DEFAULT NULL,
  `teamid` INT UNSIGNED NOT NULL,
  `groupid` INT UNSIGNED NOT NULL DEFAULT '0',
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_teamid` (`teamid`),
  KEY `mc_groupid` (`groupid`),
  CONSTRAINT `mc_teams_memberships_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_teams_memberships_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_teams_memberships_ibfk_3` FOREIGN KEY (`groupid`) REFERENCES `teams_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `teams_requests`
--

DROP TABLE IF EXISTS `teams_requests`;
CREATE TABLE `teams_requests` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED DEFAULT NULL,
  `teamid` INT UNSIGNED DEFAULT NULL,
  `requested_userid` int(10) NOT NULL,
  `accepted` boolean NOT NULL DEFAULT false,
  `declined` boolean NOT NULL DEFAULT false,
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  KEY `mc_teamid` (`teamid`),
  CONSTRAINT `teams_requests_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `teams_requests_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `manages_team`;
create TABLE manages_team (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED NOT NULL,
  `teamid` INT UNSIGNED NOT NULL,
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOt NULL DEFAULT false,
  
  PRIMARY KEY (`id`),
  FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED NOT NULL,
  `teamid` INT UNSIGNED NULL DEFAULT NULL,
  `groupid` INT UNSIGNED NULL DEFAULT NULL,
  `activity` smallint(6) NOT NULL,
  `param1` smallint(6) NOT NULL,
  `param2` int(11) NOT NULL,
  `starttime` bigint(20) NOT NULL,
  `endtime` bigint(20) NOT NULL DEFAULT 0,
  `last_update` bigint(20) NOT NULL,
  `tz_server` tinyint(4) DEFAULT 4 COMMENT 'Server Timezone. Offset from GMT in half hours',
  `tz_local` tinyint(4) DEFAULT 4 COMMENT 'Local Timezone. Offset from GMT in half hours',
  `ai` tinyint(4) NOT NULL,
  `ai_started` smallint(6) NOT NULL DEFAULT 0,
  `last_gps_alt_cm` int(11) DEFAULT NULL,
  `last_gps_speed_cms` int(11) DEFAULT NULL,
  `last_gps_accur` int(11) DEFAULT NULL,
  `last_gps_bearing` int(11) DEFAULT NULL,
  `last_gps_lat` double DEFAULT NULL,
  `last_gps_lon` double DEFAULT NULL,
  `v1` int(11) DEFAULT NULL,
  `v2` int(11) DEFAULT NULL,
  `v3` int(11) DEFAULT NULL,
  `v4` int(11) DEFAULT NULL,
  `v5` int(11) DEFAULT NULL,
  `v6` int(11) DEFAULT NULL,
  `v7` int(11) DEFAULT NULL,
  `v8` int(11) DEFAULT NULL,
  `v9` int(11) DEFAULT NULL,
  `v10` int(11) DEFAULT NULL,
  `f1` int(11) NOT NULL DEFAULT 0,
  `f2` int(11) NOT NULL DEFAULT 0,
  `f3` int(11) NOT NULL DEFAULT 0,
  `f4` int(11) NOT NULL DEFAULT 0,
  `f5` int(11) NOT NULL DEFAULT 0,

  PRIMARY KEY (`session_id`),
  CONSTRAINT `mc_session_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_session_ibfk_2` FOREIGN KEY (`teamid`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mc_session_ibfk_3` FOREIGN KEY (`groupid`) REFERENCES `teams_groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `ai_message`
--

DROP TABLE IF EXISTS `ai_message`;
CREATE TABLE `ai_message` (
  `message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint unsigned NOT NULL NULL,
  `itime` bigint(13) DEFAULT NULL,
  `rtime` bigint(13) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `stop_session` boolean NOT NULL DEFAULT false,
  UNIQUE KEY `mc_ai_message_id` (`message_id`),
  KEY `aim_session_id` (`session_id`),
  CONSTRAINT `mc_ai_message_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `feedback_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint unsigned NOT NULL,
  `ttime` bigint(13) DEFAULT NULL,
  `itime` bigint(13) DEFAULT NULL,
  `message` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `stop_session` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `mc_feedback_id` (`feedback_id`),
  KEY `mc_session_id` (`session_id`),
  CONSTRAINT `mc_feedback_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `in_group_session`
--

DROP TABLE IF EXISTS `in_group_session`;
CREATE TABLE `in_group_session` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `controller_session_id` bigint unsigned DEFAULT NULL,
  `participant_session_id` bigint unsigned DEFAULT NULL,
  UNIQUE KEY `ings_id` (`id`),
  KEY `ings_controller_session_id` (`controller_session_id`),
  KEY `ings_participant_session_id` (`participant_session_id`),
  CONSTRAINT `ings_field_ibfk_1` FOREIGN KEY (`controller_session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ings_field_ibfk_2` FOREIGN KEY (`participant_session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `notification_types`
--

DROP TABLE IF EXISTS `notification_types`;
CREATE TABLE `notification_types` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY(`type`)
);

--
-- Table structure for table `notification_actions`
--

DROP TABLE IF EXISTS `notification_actions`;
CREATE TABLE `notification_actions` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` INT UNSIGNED NOT NULL,
  `description` varchar(30) NOT NULL,
  `action` varchar(100) NOT NULL,
  `order` tinyint DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `mc_notification_actions_ibfk_1` FOREIGN KEY (`type`) REFERENCES `notification_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED NOT NULL,
  `type` INT UNSIGNED NOT NULL,
  `msg` text NOT NULL,
  `data` varchar(255),
  `crtime` int(11) NOT NULL DEFAULT '0',
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  CONSTRAINT `mc_notifications_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mc_notifications_ibfk_2` FOREIGN KEY (`type`) REFERENCES `notification_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `setting_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  `name` varchar(30) NOT NULL,
  `value` varchar(60) NOT NULL,
  UNIQUE KEY `mc_setting_id` (`setting_id`),
  KEY `mc_session_id` (`session_id`),
  CONSTRAINT `mc_setting_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `marker`;
CREATE TABLE `marker` (
  `marker_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint unsigned NOT NULL,
  `rec_time` bigint(13) NOT NULL,
  PRIMARY KEY `mc_marker_id` (`marker_id`),
  KEY `mc_session_id` (`session_id`),
  CONSTRAINT `mc_marker_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `oauth2_authorization` (
  `id` varchar(100) NOT NULL,
  `registered_client_id` varchar(100) NOT NULL,
  `principal_name` varchar(200) NOT NULL,
  `authorization_grant_type` varchar(100) NOT NULL,
  `attributes` blob,
  `state` varchar(500) DEFAULT NULL,
  `authorization_code_value` blob,
  `authorization_code_issued_at` timestamp NULL DEFAULT NULL,
  `authorization_code_expires_at` timestamp NULL DEFAULT NULL,
  `authorization_code_metadata` blob,
  `access_token_value` blob,
  `access_token_issued_at` timestamp NULL DEFAULT NULL,
  `access_token_expires_at` timestamp NULL DEFAULT NULL,
  `access_token_metadata` blob,
  `access_token_type` varchar(100) DEFAULT NULL,
  `access_token_scopes` varchar(1000) DEFAULT NULL,
  `oidc_id_token_value` blob,
  `oidc_id_token_issued_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_expires_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_metadata` blob,
  `refresh_token_value` blob,
  `refresh_token_issued_at` timestamp NULL DEFAULT NULL,
  `refresh_token_expires_at` timestamp NULL DEFAULT NULL,
  `refresh_token_metadata` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE oauth2_authorization_consent (
    registered_client_id varchar(100) NOT NULL,
    principal_name varchar(200) NOT NULL,
    authorities varchar(1000) NOT NULL,
    PRIMARY KEY (registered_client_id, principal_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `value` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` enum('int','dec','text') NOT NULL,
  
  PRIMARY KEY (`ID`),
  UNIQUE KEY `value_UN` (`name`)
);

CREATE TABLE `user_value_int` (
  `ID` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `value_id` BIGINT UNSIGNED NOT NULL,
  `date_start` timestamp NOT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `value` BIGINT NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_value_int_user_FK` (`user_id`),
  KEY `user_value_int_value_FK` (`value_id`),
  CONSTRAINT `user_value_int_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_value_int_value_FK` FOREIGN KEY (`value_id`) REFERENCES `value` (`ID`)
);

CREATE TABLE `user_value_dec` (
  `ID` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `value_id` BIGINT UNSIGNED NOT NULL,
  `date_start` timestamp NOT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_value_dec_user_FK` (`user_id`),
  KEY `user_value_dec_value_FK` (`value_id`),
  CONSTRAINT `user_value_dec_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_value_dec_value_FK` FOREIGN KEY (`value_id`) REFERENCES `value` (`ID`)
);

CREATE TABLE `user_value_text` (
  `ID` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `value_id` BIGINT UNSIGNED NOT NULL,
  `date_start` timestamp NOT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_value_text_user_FK` (`user_id`),
  KEY `user_value_text_value_FK` (`value_id`),
  CONSTRAINT `user_value_text_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_value_text_value_FK` FOREIGN KEY (`value_id`) REFERENCES `value` (`ID`)
);

CREATE TABLE roles (
	id BIGINT unsigned NOT NULL AUTO_INCREMENT,
	name varchar(100) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY (name)
);

CREATE TABLE `user_has_role` (
  `id` BIGINT UNSIGNED auto_increment NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `role_id` BIGINT UNSIGNED NOT NULL,
  `date_start` timestamp NOT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (role_id) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (user_id) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Table structure for table `temp_changeemail`
--

DROP TABLE IF EXISTS `temp_changeemail`;
CREATE TABLE `temp_changeemail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` BIGINT UNSIGNED NOT NULL,
  `oldemail` varchar(255) NOT NULL,
  `newemail` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `crtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mc_userid` (`userid`),
  CONSTRAINT `mc_temp_changeemail_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `temp_passwordreset`
--

DROP TABLE IF EXISTS `temp_passwordreset`;
CREATE TABLE `temp_passwordreset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password_9048` varchar(255) NOT NULL,
  `password_8334` varchar(255) NOT NULL,
  `salt_0129` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `crtime` int(11) NOT NULL,
  `deleted` boolean NOT NULL DEFAULT false,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `temp_registration`
--

DROP TABLE IF EXISTS `temp_registration`;
CREATE TABLE `temp_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_code` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `forename` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `password_2896` varchar(255) NOT NULL,
  `salt_7863` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `crtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table structure for table `message_templates`
--

DROP TABLE IF EXISTS `message_templates`;
CREATE TABLE `message_templates` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `templatename` varchar(100) NOT NULL,
  `lang` char(6) NOT NULL,
  `template` text,

  PRIMARY KEY (`id`),
  UNIQUE KEY (`templatename`, `lang`)
);

CREATE TABLE `imported_session` (
  `import_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `session_id` BIGINT UNSIGNED NOT NULL,
  `rec_time` bigint unsigned NOT NULL,
  `type` varchar(20) NOT NULL,
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`import_id`),
  FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO roles (id, name) VALUES (1, 'ADMIN');

insert into notification_types values (1, 'team_invite');
insert into notification_types values (2, 'team_invite_accepted');
insert into notification_types values (3, 'team_invite_declined');
insert into notification_actions(`type`, `description`, `action`, `order`) values
  (1, 'accept', 'v1/teams/acceptinvite', 1),
  (1, 'decline', 'v1/teams/declineinvite', 2)
  ;

insert into message_templates (templatename, lang, template) values
  ('team_invite', 'de', '${person} hat Sie in das Team ${teamname} eingeladen'),
  ('team_invite', 'en', '${person} has invited to join team ${teamname}'),
  ('team_invite_accepted', 'de', '${person} hat die Einladung in Ihr Team ${teamname} angenommen'),
  ('team_invite_accepted', 'en', '${person} has accepted your invite to join team ${teamname}'),
  ('team_invite_declined', 'de', '${person} hat die Einladung in Ihr Team ${teamname} abgelehnt'),
  ('team_invite_declined', 'en', '${person} has declined your invite to join team ${teamname}'),
  ('registration_created', 'de', 'Vielen Dank für Ihre Registrierung auf der Pegasos Web Platform!<br /><br />Klicken Sie auf den folgenden Link, um die Registrierung abzuschließen:<br /><a href=\\"${link}\\" title=\\"Registrierung abschließen\\">Registrierung abschließen</a><br /><br />Sollten Sie den Link oben nicht sehen können, kopieren sie den nachfolgenden Link einfach direkt in Ihren Browser:<br />${link}<br /><br /><br />Diese E-Mail wurde automatisch zugestellt. Bitte antworten Sie nicht auf diese Nachricht'),
  ('registration_created', 'en', 'Thank you for your registration on the Pegasos Web Platform!<br/><br/Activate your account by visiting the following link:<br/><a href=\\"${link}\\" title=\\"Finish registration\\">Finish registration</a><br/><br/>If this link does not work please copy the following link into your address bar of your browser:<br/>${link}<br/><br/>This email was generated automatically. Please do not reply to it.'),
  ('mailchange_requested', 'de', 'Es wurde eine Anfrage zur Änderung Ihrer E-Mail Adresse auf der Pegasos Web Platform durchgeführt.<br/>
Wenn Sie Ihre E-Mail Adresse nicht ändern wollen, ignorieren Sie einfach diese Nachricht.<br/><br/>
Wenn Sie Ihre E-Mail Adresse ändern wollen, klicken Sie auf den nachfolgenden Link:<br/>
<a href=\\"${link}\\" title=\\"E-Mail Adresse ändern\\">E-Mail Adresse ändern</a><br/><br/>
Sollten Sie den Link oben nicht sehen können, kopieren sie den nachfolgenden Link einfach direkt in Ihren Browser:<br />
${link}<br/><br/><br/>
Diese E-Mail wurde automatisch zugestellt. Bitte antworten Sie nicht auf diese Nachricht.')
;

