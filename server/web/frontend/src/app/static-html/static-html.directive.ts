import {
  Directive, TemplateRef, ViewContainerRef, Input, Renderer2,
  OnChanges, SimpleChanges, OnInit, Compiler, Injector, NgModuleRef, ComponentRef
} from '@angular/core';
import '@angular/compiler';
import {StaticHtmlService} from './static-html.service';
import { StaticHtml } from './static-html';

@Directive({
  selector: '[appStaticHtmlTrustedRender]'
})
export class StaticHtmlTrustedRenderDirective implements OnInit, OnChanges {

  @Input() public appStaticHtmlTrustedRender!: string;

  private cmpRef!: ComponentRef<any>;

  private html: StaticHtml;

  constructor(
    protected renderer: Renderer2,
    protected templateRef: TemplateRef<any>,
    protected viewContainer: ViewContainerRef,
    protected compiler: Compiler,
    protected injector: Injector,
    protected moduleRef: NgModuleRef<any>,
    protected staticHtmlService: StaticHtmlService,
  ) {
    this.html = new StaticHtml(renderer, templateRef, viewContainer, compiler, injector, moduleRef, staticHtmlService);
   }

  ngOnInit(): void {
    // this.insertStaticView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.appStaticHtmlTrustedRender) {
      return;
    }
    this.html.insertStaticView(this.appStaticHtmlTrustedRender, true, true);
  }

  // Cleanup properly. You can add more cleanup-related stuff here.
  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }
}

@Directive({
  selector: '[appStaticHtml]'
})
export class StaticHtmlDirective implements OnInit, OnChanges {

  @Input() public appStaticHtml!: string;

  private cmpRef!: ComponentRef<any>;

  private html: StaticHtml;

  constructor(
    protected renderer: Renderer2,
    protected templateRef: TemplateRef<any>,
    protected viewContainer: ViewContainerRef,
    protected compiler: Compiler,
    protected injector: Injector,
    protected moduleRef: NgModuleRef<any>,
    protected staticHtmlService: StaticHtmlService,
  ) {
    this.html = new StaticHtml(renderer, templateRef, viewContainer, compiler, injector, moduleRef, staticHtmlService);
   }

  ngOnInit(): void {
    // this.insertStaticView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.appStaticHtml) {
      return;
    }
    this.html.insertStaticView(this.appStaticHtml, false, false);
  }

  // Cleanup properly. You can add more cleanup-related stuff here.
  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }
}

@Directive({
  selector: '[appStaticHtmlTrusted]'
})
export class StaticHtmlTrustedDirective implements OnInit, OnChanges {

  @Input() public appStaticHtmlTrusted!: string;

  private cmpRef!: ComponentRef<any>;

  private html: StaticHtml;

  constructor(
    protected renderer: Renderer2,
    protected templateRef: TemplateRef<any>,
    protected viewContainer: ViewContainerRef,
    protected compiler: Compiler,
    protected injector: Injector,
    protected moduleRef: NgModuleRef<any>,
    protected staticHtmlService: StaticHtmlService,
  ) {
    this.html = new StaticHtml(renderer, templateRef, viewContainer, compiler, injector, moduleRef, staticHtmlService);
   }

  ngOnInit(): void {
    // this.insertStaticView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.appStaticHtmlTrusted) {
      return;
    }
    this.html.insertStaticView(this.appStaticHtmlTrusted, true, false);
  }

  // Cleanup properly. You can add more cleanup-related stuff here.
  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }
}
