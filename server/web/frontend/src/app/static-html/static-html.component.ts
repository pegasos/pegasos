import {
  Component, ViewContainerRef, Input, Renderer2,
  OnChanges, SimpleChanges, OnInit, Compiler, Injector, NgModuleRef, ComponentRef
} from '@angular/core';
import { StaticHtmlService } from './static-html.service';
// import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { StaticHtml } from './static-html';

@Component({
  selector: 'app-static-html',
  // template: '<div *appStaticHtml=""></div>',
  template: '<ng-template></ng-template>',
  // styleUrls: ['./static-html.component.css']
})
export class StaticHtmlComponent implements OnInit, OnChanges {

  // innerHtml: SafeHtml;

  @Input() source!: string;
  @Input() trusted!: boolean;

  private cmpRef!: ComponentRef<any>;

  private html: StaticHtml;

  constructor(
    renderer: Renderer2,
    // templateRef: TemplateRef<any>,
    viewContainer: ViewContainerRef,
    compiler: Compiler,
    injector: Injector,
    moduleRef: NgModuleRef<any>,
    staticHtmlService: StaticHtmlService
  ) {
    this.html = new StaticHtml(renderer, null, viewContainer, compiler, injector, moduleRef, staticHtmlService);
  }

  ngOnInit(): void {
    this.insertStaticView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.source) {
      return;
    }
    this.insertStaticView();
  }

  // Cleanup properly. You can add more cleanup-related stuff here.
  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }

  private insertStaticView(): void {
    this.html.insertStaticView(this.source, this.trusted);
  }
}
