import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StaticHtmlDirective, StaticHtmlTrustedDirective, StaticHtmlTrustedRenderDirective } from './static-html.directive';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        StaticHtmlTrustedRenderDirective,
        StaticHtmlDirective,
        StaticHtmlTrustedDirective,
    ],
    exports: [
        StaticHtmlTrustedRenderDirective,
        StaticHtmlDirective,
        StaticHtmlTrustedDirective,
    ]
})
export class StaticHtmlModule {
}
