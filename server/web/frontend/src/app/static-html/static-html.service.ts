import { DEFAULT_LANGUAGE } from '../shared/language';
import { catchError } from 'rxjs/operators';
import { Injectable, SecurityContext } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { map } from 'rxjs/operators';
import { AppService } from '../services/app.service';

@Injectable({
  providedIn: 'root'
})
export class StaticHtmlService {

  constructor(
    private httpClient: HttpClient,
    private domSanitizer: DomSanitizer,
    private app: AppService,
  ) { }

  getStaticHTML(url: string): Observable<string|null> {
    console.log('staticHtmlservie', {url}, this.app.getCurrentLanguage().lang);

    return this.httpClient.get('assets/static/' + url + '_' + this.app.getCurrentLanguage().lang + '.html', {
      responseType: 'text'
    }).pipe(
      map(response => this.mapStaticHtml(response)),
      catchError(_ => {
        return this.httpClient.get('assets/static/' + url + '_' + DEFAULT_LANGUAGE.lang + '.html', {
          responseType: 'text'
        }).pipe(
          map(response => this.mapStaticHtml(response)),
          catchError(_ => {
            return this.httpClient.get(url, {
              responseType: 'text'
            }).pipe(
              map(response => this.mapStaticHtml(response)),
              catchError(_ => {
                return of('');
              })
            );
          })
        );
      })
    );
  }

  getStaticHTMLTrusted(url: string): Observable<string> {
    console.log('staticHtmlservie trusted', {url}, this.app.getCurrentLanguage().lang);

    return this.httpClient.get('assets/static/' + url + '_' + this.app.getCurrentLanguage().lang + '.html', {
      responseType: 'text'
    }).pipe(
      map(response => response),
      catchError(_ => {
        return this.httpClient.get('assets/static/' + url + '_' + DEFAULT_LANGUAGE.lang + '.html', {
          responseType: 'text'
        }).pipe(
          map(response => response),
          catchError(_ => {
            return this.httpClient.get(url, {
              responseType: 'text'
            }).pipe(
              map(response => response),
              catchError(_ => {
                return of('');
              })
            );
          })
        );
      })
    );
  }

  private mapStaticHtml(htmlString: string): string | null {
    return this.domSanitizer.sanitize(SecurityContext.HTML, htmlString);
    // return htmlString;
  }
}
