import { CommonModule } from '@angular/common';
import {
  Compiler, Component, ComponentRef, Directive, Injector, NgModule, NgModuleRef, OnChanges, OnInit, Renderer2, SimpleChanges,
  TemplateRef, ViewContainerRef
} from '@angular/core';
import { RouterModule } from '@angular/router';
import '@angular/compiler';
import { ActionComponentsModule } from '../actioncomponents/action.components.module';
import { PegasosCommonModule } from './../common/common-module';
import { StaticHtmlService } from './static-html.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

export class StaticHtml {

  protected appStaticHtml!: string;

  protected appStaticHtmlTrusted!: boolean;

  protected appStaticHtmlRender!: boolean;

  private cmpRef!: ComponentRef<any>;

  constructor(
    private renderer: Renderer2,
    private templateRef: TemplateRef<any> | null,
    private viewContainer: ViewContainerRef,
    private compiler: Compiler,
    private injector: Injector,
    private moduleRef: NgModuleRef<any>,
    private staticHtmlService: StaticHtmlService
  ) { }

  public insertStaticView(appStaticHtml: string, appStaticHtmlTrusted?: boolean, appStaticHtmlRender?: boolean): void {
    this.appStaticHtml = appStaticHtml;
    if (appStaticHtmlTrusted)
      this.appStaticHtmlTrusted = appStaticHtmlTrusted;
    if (appStaticHtmlRender)
      this.appStaticHtmlRender = appStaticHtmlRender;

    console.log('insertStaticView', this.appStaticHtml, this.appStaticHtmlRender, this.appStaticHtmlTrusted);

    if ( this.appStaticHtmlTrusted ) {
      this.staticHtmlService
        .getStaticHTMLTrusted(this.appStaticHtml)
        .subscribe(response => { this.replaceHtml(response); });
    } else {
      this.staticHtmlService
        .getStaticHTML(this.appStaticHtml)
        .subscribe(response => { if (response) this.replaceHtml(response); });
    }
  }

  private replaceHtml(innerHTML: string): void {
    this.viewContainer.remove();
    if ( this.appStaticHtmlRender ) {
      this.createComponentFromRaw(innerHTML);
    } else {
      if ( this.templateRef ) {
        const embeddedViewRef = this.viewContainer.createEmbeddedView(this.templateRef);
        this.renderer.setProperty(embeddedViewRef.rootNodes[0], 'innerHTML', innerHTML);
      }
    }
  }

  // Here we create the component.
  private createComponentFromRaw(template: string) {
    // Let's say your template looks like `<h2><some-component [data]="data"></some-component>`
    // As you see, it has an (existing) angular component `some-component` and it injects it [data]

    // Now we create a new component. It has that template, and we can even give it data.
    const tmpCmp = Component({ template })(class {
      // the class is anonymous. But it's a quite regular angular class. You could add @Inputs,
      // @Outputs, inject stuff etc.
      // data: { some: 'data' };
      // ngOnInit() { /* do stuff here in the dynamic component */ }
    });

    // Now, also create a dynamic module.
    const tmpModule = NgModule({
      imports: [CommonModule, RouterModule, ActionComponentsModule, FontAwesomeModule, PegasosCommonModule],
      declarations: [tmpCmp],
      // providers: [] - e.g. if your dynamic component needs any service, provide it here.
    })(class {});

    // Now compile this module and component, and inject it into that #vc in your current component template.
    this.compiler.compileModuleAndAllComponentsAsync(tmpModule)
      .then((factories) => {
        const f = factories.componentFactories[0];
        this.cmpRef = f.create(this.injector, [], null, this.moduleRef);
        this.cmpRef.instance.name = 'my-dynamic-component';
        this.viewContainer.insert(this.cmpRef.hostView);
      });
  }
}
