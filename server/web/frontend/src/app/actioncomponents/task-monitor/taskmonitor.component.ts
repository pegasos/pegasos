import { AppService } from './../../services/app.service';
import { Component, OnInit, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { TeamWithGroups, TeamWithGroupsWithUsers, TUser, Group as TGroup, GroupWithUsers, TaskStatus } from '../../shared/sample';
import { Group, Team, Person, PersonSelectEntry } from '../../shared/person';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TeamService } from '../../services/team.service';
import { ParamMap } from '@angular/router';
import { interval } from 'rxjs';

@Component({
  selector: 'app-taskmonitor',
  templateUrl: `./taskmonitor.component.html`,
  styleUrls: [`./taskmonitor.component.scss`],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TaskMonitorComponent),
      multi: true
    }
  ]
})
export class TaskMonitorComponent /*implements OnInit, ControlValueAccessor*/ {

  @Input() taskId!: number;
  @Input() updateInterval = 5000;
  @Output() taskFinished = new EventEmitter<void>();

  completion!: number;
  messages: string[] = [];
  subscr: any;

  public static Destory(monitor: TaskMonitorComponent): void {
    if ( monitor !== null && monitor !== undefined ) {
      if ( monitor.subscr !== undefined ) {
        monitor.subscr.unsubscribe();
      }
    }
  }

  public constructor(private app: AppService) {}

  public monitor(task: TaskStatus): void {
    this.taskId = task.id;
    this.startCheck();
  }

  /**
   * Start checking / updating task status. This method requires that `taskId` is set
   */
  public startCheck(): void {
    const fetcher = interval(this.updateInterval);
    this.subscr = fetcher.subscribe(a => {
      this.app.getResource('v1/tasks/status/' + this.taskId).subscribe(res => this.onTaskData(res));
    });
  }

  private onTaskData(task: TaskStatus) {
    this.completion = Math.round((task.completion + Number.EPSILON) * 10000) / 100;
    task.messages.forEach(m => this.messages.push(m));
    if ( task.completed ) {
      this.subscr.unsubscribe();
      this.taskFinished.emit();
    }
  }
}
