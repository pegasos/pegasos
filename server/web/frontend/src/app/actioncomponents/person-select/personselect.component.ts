import { Component, OnInit, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { TeamWithGroups, TeamWithGroupsWithUsers, TUser, Group as TGroup, GroupWithUsers } from '../../shared/sample';
import { Group, Team, Person, PersonSelectEntry } from '../../shared/person';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TeamService } from '../../services/team.service';
import { ParamMap } from '@angular/router';

@Component({
  selector: 'app-person-select',
  templateUrl: `./personselect.component.html`,
  styleUrls: [`./personselect.component.scss`],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PersonSelectComponent),
      multi: true
    }
  ]
})
export class PersonSelectComponent implements OnInit, ControlValueAccessor {

  @Input() singleSelection = false;
  @Input() formText = '(group)';
  @Input() selectionLevel: 'Person' | 'Group' | 'Team' = 'Person';
  @Input() personsAvailable: Array<any> = [];
  @Input() personsMap: {[key:string]: Person | Group | Team} = {};
  @Input() request = false;
  @Input() disabled = false;
  @Output() personsReceived = new EventEmitter<void>();

  /**
   * internal: when single selection is true this is set to 0
   */
  protected selectionLimit!: number;

  personsdSettings: any = {};

  selectedItems: Array<any> = [];

  teams: any;

  private personsLookup: Map<number, Person>;

  public static setDataTG(data: TeamWithGroups[] | TeamWithGroupsWithUsers[]): { 'available': PersonSelectEntry[], 'map': {[key:string]: Person | Group | Team}} {
    const teams = data;
    let personsAvailable: PersonSelectEntry[] = [];
    const pt: {[key:string]: Person | Group | Team} = {};
    (teams as any[]).forEach((team: TeamWithGroups | TeamWithGroupsWithUsers) => {
      const t: Team = new Team();
      pt['t_' + team.teamid] = t;
      t.teamid = team.teamid;
      t.name = team.name;

      (team.groups as any[]).forEach((group: TGroup | GroupWithUsers) => {
        const g = new Group();
        g.groupid = group.groupid;
        g.name = group.name;
        (<Team>pt['t_' + team.teamid]).groups.push(g);
        pt['g_' + group.groupid] = g;

        if ( 'members' in group ) {
          group.members.forEach((person: TUser) => {
            const p = new Person();
            p.id = person.id;
            p.name = person.firstname + ' ' + person.lastname;
            (<Group>pt['g_' + group.groupid]).members.push(p);
          });
        }
      });
      personsAvailable = personsAvailable.concat(pt['t_' + team.teamid].flatten());
    });

    return {
      'available': personsAvailable,
      'map': pt
    };
  }

  constructor(
    private teamService: TeamService,
  ) {
    this.personsLookup = new Map<number, Person>();
  }

  ngOnInit(): void {
    if( this.singleSelection ) {
      this.selectionLimit = 1;
    }

    if (this.request) {
      this.teamService.getMyGroupMembers().subscribe(data => this.onPersonsReceived(data));
    }
  }

  get inputData() {
    console.log('inputData()');
    return this.selectedItems;
  }

  writeValue(value: any): void {
    console.log('writeValue', value);
    this.selectedItems = value;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  setDisabledState(isDisabled: boolean): void {
    console.log('PersonSelectComponent setDisabledState:', isDisabled);
    // this.personsdSettings = {...this.personsdSettings, disabled: isDisabled};
    this.disabled = isDisabled;
  }

  private onPersonsReceived(data: TeamWithGroupsWithUsers[]): void {
    const h = PersonSelectComponent.setDataTG(data);
    this.personsAvailable = h.available;
    this.personsMap = h.map;

    data.forEach(team => {
      team.groups.forEach(group => {
        if ( 'members' in group ) {
          group.members.forEach(person => {
            const p = new Person();
            p.id = person.id;
            p.name = person.firstname + ' ' + person.lastname;
            this.personsLookup.set(p.id, p);
          });
        }
      });
    });

    this.personsReceived.emit();
  }

  public onElementChange(event: any): void {
    const item = event.itemValue;
    const select = this.selectedItems.includes(item);
    // console.log('onelementchange', event, select);

    if ( select ) {
      this.onPersonSelect(item);
    } else {
      this.onPersonDeSelect(item);
    }
    console.log(this.selectedItems);
  }

  private onPersonSelect(item: any) {
    // console.log('onPersonSelect', item, this.singleSelection, 'selectionLevel:', this.selectionLevel);

    if (this.selectionLevel !== 'Person') {
      if (this.selectionLevel === 'Group' && item.item_type !== 'group') {
        this.selectedItems = this.selectedItems.filter(i => i.id !== item.id);
        this.propagateChange(this.selectedItems);
        return;
      } else if (this.selectionLevel === 'Team' && item.item_type !== 'team') {
        this.selectedItems = this.selectedItems.filter(i => i.id !== item.id);
        this.propagateChange(this.selectedItems);
        return;
      }
    }

    if (item.id.startsWith('t_') || item.id.startsWith('g_')) {
      // all the old select values + newly selected
      let persons = this.selectedItems;
      const allnew = (<Group|Team>this.personsMap[item.id]).flatten();
      // get the ids of all newly selected
      const ids = allnew.map(i => i.id);

      // now unselect them. This avoids a wrong count
      // console.log({ item }, { allnew }, { ids }, { persons });
      persons = persons.filter(i => ids.indexOf(i.id) === -1);

      // add newly selected (all of team / group)
      persons = persons.concat(allnew);

      // OR: Add only the one
      // persons = persons.concat(item);

      this.selectedItems = persons;
    }
    // console.log(this.selectedItems);
    this.propagateChange(this.selectedItems);
  }

  private onPersonDeSelect(item: any) {
    // console.log('onPersonDeSelect', item);
    if ( item.id.startsWith('t_') || item.id.startsWith('g_') ) {
      let uns = this.selectedItems;
      const ids = (<Group|Team>this.personsMap[item.id]).flatten().slice(1).map(i => i.id);
      uns = uns.filter(i => ids.indexOf(i.id) === -1);

      this.selectedItems = uns;
    }
    // console.log(this.selectedItems);
    this.propagateChange(this.selectedItems);
  }

  /**
   * Check whether this dropdown has items in it
   */
  isReady(): boolean {
    return this.personsAvailable.length > 0;
  }

  public hasSelectedPersons(): boolean {
    return this.selectedItems !== undefined && this.selectedItems.length > 0;
  }

  /**
   * Update the selection based on the route and set the corresponding parameter in a request (formData)
   * @param params
   * @param formData
   */
  updateRouteForm(params: ParamMap, formData: {[key:string]: any} | null = null) {
    if (params.has('pers')) {
      // console.log('has pers', 'personSelect:', this);

      const pers = params.get('pers')?.split(',');
      this.selectedItems = [];
      // console.log('setFromRoute', params.get('pers'), {...this.personsMap});
      pers?.forEach(p => {
        if (p.startsWith('t_') || p.startsWith('g_')) {
          // get the ids of all newly selected
          const ids = (<Group|Team>this.personsMap[p]).flatten().map(i => i.id);

          // for each id find the corresponding entry in personsAvailable
          const entries = ids.map(id => this.personsAvailable.filter(pp => pp.id === id)[0]);

          this.selectedItems = this.selectedItems.concat(entries);
        } else {
          // Assume that is is person. If not it should not be in the list anyway (if statement below)
          const h = this.personsAvailable.filter(pp => pp.id === p);
          if (h.length > 0) {
            this.selectedItems.push(h[0]);
          }
        }
      });

      // formData['persons'] = params.get('pers').split(',');
      // do not set the params as formData but use the actual items to correct for possible problems
      if ( formData !== null ) {
        formData['persons'] = this.selectedItems.map(p => p.id);
      }
    } else if ( formData !== null ) {
      formData['persons'] = [];
    }
  }

  getRouteParam(): string {
    if ( this.selectedItems !== undefined ) {
      const pers = this.selectedItems.map(p => p.id);
      return pers.join(',');
    } else {
      return '';
    }
  }

  public updateRouteParams(params: {[key:string]: any}): void {
    console.log('PersonSelect::updateRouteParams', {params}, {...this.selectedItems});
    if (this.selectedItems !== undefined && this.selectedItems !== null &&
        this.selectedItems.length > 0 ) {
      let route = this.selectedItems.map(p => p.id);
      const teams = route.filter(id => id.startsWith('t_'));
      teams.forEach(team => {
        const children = (<Team>this.personsMap[team]).flatten().map(pp => pp.id).slice(1);

        // keep only elements which are not children of this element
        route = route.filter(entry => children.indexOf(entry) === -1);
      });
      const groups = route.filter(id => id.startsWith('g_'));
      groups.forEach(tg => {
        const children = (<Group>this.personsMap[tg]).flatten().map(pp => pp.id).slice(1);

        // keep only elements which are not children of this element
        route = route.filter(entry => children.indexOf(entry) === -1);
      });
      params['pers'] = route;
    } else {
      delete params['pers'];
    }
  }

  /**
   * Get a person in this dropdown
   * @param id ID of the person
   */
  public getPerson(id: number): Person | undefined {
    return this.personsLookup.get(id);
  }
}
