import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ParamMap } from '@angular/router';

@Component({
  selector: 'app-time-period',
  templateUrl: `./timeperiod.component.html`,
})
export class TimePeriodComponent {

  @Input() yearRange: string = (new Date().getFullYear() - 10) + ':' + (new Date().getFullYear() + 1);

  @Input() public formGroup!: FormGroup;

  @Input() start: Date = new Date();

  @Input() end: Date = new Date();

  @Input() monthMode: boolean = false;

  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    const now = new Date();
    this.formGroup.setControl('timeperiod', this.fb.group({
      'startdate': [this.start],
      'enddate': [this.end]
    }));
  }

  /**
   * Check if start date was touched
   * @returns true if start date was touched
   */
  public touched(): boolean {
    if (this.formGroup.get('timeperiod.startdate')?.touched) {
      return true;
    }
    return false;
  }

  public setStart(start: Date): void {
    this.start = start;
    this.formGroup.get('timeperiod.startdate')?.setValue(this.start);
  }

  /**
   * Retrieve the start date
   *
   * @returns start date as string
   */
  getStart(): string {
    const date = this.formGroup.get('timeperiod.startdate')?.value;
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  /**
   * Retrieve the start date only if it was touched.
   *
   * @returns start date as string if touched, undefined otherwise
   */
  getStartIfTouched(): string | undefined {
    if (this.formGroup.get('timeperiod.startdate')?.touched) {
      const date = this.formGroup.get('timeperiod.startdate')?.value;

      return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }
    return undefined;
  }

  public setEnd(end: Date): void {
    this.end = end;
    this.formGroup.get('timeperiod.enddate')?.setValue(this.end);
  }

  /**
   * Retrieve the end date
   *
   * @returns end date as string
   */
  getEnd(): string {
    const date = this.formGroup.get('timeperiod.enddate')?.value;

    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  /**
   * Retrieve the end date only if it was touched.
   *
   * @returns end date as string if touched, undefined otherwise
   */
  getEndIfTouched(): string | undefined {
    if (this.formGroup.get('timeperiod.enddate')?.touched) {
      const date = this.formGroup.get('timeperiod.enddate')?.value;

      return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }
    return undefined;
  }

  /**
   * Update the display based on the route's params. If no formData is given this parameter will be ignored
   * @param params params of the current route
   * @param formData form data to be updated based on the route
   */
  public update(params: ParamMap, formData: {[key:string]: any} | null = null): Date {
    if (this.monthMode ) {
      return this.formUpdateMonth(params, formData);
    }

    if (params.has('end')) {
      console.log('has end');
      const parts = params.get('end')?.split('-') as string[];
      this.formGroup.get('timeperiod.enddate')?.setValue(new Date(+parts[0], +parts[1] - 1, +parts[2]));
      this.formGroup.get('timeperiod.enddate')?.markAsTouched();

      if (formData !== null) {
        formData['enddate'] = {
          'year': parts[0],
          'month': parts[1],
          'day': parts[2]
        };
      }
    } else if (formData !== null) {
      formData['enddate'] = {
        'year': this.formGroup.get('timeperiod.enddate')?.value.getFullYear(),
        'month': this.formGroup.get('timeperiod.enddate')?.value.getMonth() + 1,
        'day': this.formGroup.get('timeperiod.enddate')?.value.getDate()
      };
    }

    if (params.has('start')) {
      const parts = params.get('start')?.split('-') as string[];
      const date = new Date(+parts[0], +parts[1] - 1, +parts[2]);
      this.formGroup.get('timeperiod.startdate')?.setValue(date);
      this.formGroup.get('timeperiod.startdate')?.markAsTouched();
      if (formData != null) {
        formData['startdate'] = {
          'year': parts[0],
          'month': parts[1],
          'day': parts[2]
        };
      }

      return date;
    } else {
      if (formData !== null) {
        formData['startdate'] = {
          'year': this.formGroup.get('timeperiod.startdate')?.value.getFullYear(),
          'month': this.formGroup.get('timeperiod.startdate')?.value.getMonth() + 1,
          'day': this.formGroup.get('timeperiod.startdate')?.value.getDate()
        };
      }

      return this.formGroup.get('timeperiod.enddate')?.value;
    }
  }

  /**
   * Update the display and request based on a 'month' parameter in the route
   * @param params ParamMap needs to contain a 'month' parameter
   * @param formData request payload updated by this method
   */
  formUpdateMonth(params: ParamMap, formData: {[key:string]: any} | null = null): Date {
    let date;
    if( params.has('month') ) {
      const parts = params.get('month')?.split('-') as string[];

      date = new Date(+parts[0], +parts[1] - 1, 1);
    } else {
      date = new Date();
      date.setDate(1);
      console.log('No month', {date});
    }
    const beginMonth = this.getMonthBegin(date);

    this.formGroup.get('timeperiod.startdate')?.setValue(date);
    this.formGroup.get('timeperiod.startdate')?.markAsTouched();

    if (formData !== null) {
      formData['startdate'] = {
        'year': beginMonth.getFullYear(),
        'month': beginMonth.getMonth() + 1,
        'day': beginMonth.getDate()
      };
    }

    const lastDayOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    this.formGroup.get('timeperiod.enddate')?.setValue(lastDayOfMonth);
    this.formGroup.get('timeperiod.enddate')?.markAsTouched();

    const endMonth = this.getMonthEnd(lastDayOfMonth);

    if (formData !== null) {
      formData['enddate'] = {
        'year': endMonth.getFullYear(),
        'month': endMonth.getMonth() + 1,
        'day': endMonth.getDate()
      };
    }

    return date;
  }

  public updateRouteParams(params: {[key:string]: any}): void {
    if (this.formGroup.get('timeperiod.startdate')?.touched) {
      const date = this.formGroup.get('timeperiod.startdate')?.value;
      params['start'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }

    if (this.formGroup.get('timeperiod.enddate')?.touched) {
      const date = this.formGroup.get('timeperiod.enddate')?.value;
      params['end'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }
  }

  /**
   * utility function used for setting start date when in calendar mode
   * Return the first day of the first week in the month (which can be from
   * the previous month)
   */
  getMonthBegin(date: Date): Date {
    const beginMonth = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    beginMonth.setDate(1);
    // Is this month starting on a day different from Monday?
    // console.log({beginMonth}, beginMonth.getDay());
    // Is this month starting on a sunday?
    if (beginMonth.getDay() === 0 ) {
      beginMonth.setDate(beginMonth.getDate() - 6 );
    } else if (beginMonth.getDay() > 1 ) {
      // Is this month starting on a day different from Monday?
      beginMonth.setDate(beginMonth.getDate() - beginMonth.getDay() + 1 );
    }
    return beginMonth;
  }

  /**
   * utility function used for setting end date when in calendar mode
   * Return the last day of the last week in the month (which can be from
   * the next month)
   */
  getMonthEnd(date: Date): Date {
    const lastDayOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    // Is this month ending on a day different from Sunday?
    // console.log({lastDayOfMonth}, lastDayOfMonth.getDay());
    if (lastDayOfMonth.getDay() !== 6 ) {
      lastDayOfMonth.setDate(lastDayOfMonth.getDate() + 7 - lastDayOfMonth.getDay());
    }
    return lastDayOfMonth;
  }
}
