import { InfoButtonComponent } from './info-button/info-button.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { ProgressBarModule } from 'primeng/progressbar';
import { MultiSelectModule } from 'primeng/multiselect';
import { TimePeriodComponent } from './time-period/timeperiod.component';
import { PersonSelectComponent } from './person-select/personselect.component';
import { EditButtonComponent } from './edit-button/edit-button.component';
import { DateFormatPipe } from './pipes/dateformat';
import { HostDirective } from './../shared/host.directive';
import { ExpandButtonComponent } from './expand/expand.button.component';
import { ActivitySelectComponent } from './activity-select/activityselect.component';
import { TaskMonitorComponent } from './task-monitor/taskmonitor.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        FontAwesomeModule,
        CalendarModule, ProgressBarModule, MultiSelectModule,
    ],
    declarations: [
        HostDirective,
        DateFormatPipe,
        EditButtonComponent,
        InfoButtonComponent,
        ActivitySelectComponent,
        PersonSelectComponent,
        TimePeriodComponent,
        TaskMonitorComponent,
        ExpandButtonComponent,
    ],
    exports: [
        HostDirective,
        DateFormatPipe,
        EditButtonComponent,
        InfoButtonComponent,
        ActivitySelectComponent,
        PersonSelectComponent,
        TimePeriodComponent,
        TaskMonitorComponent,
        ExpandButtonComponent,
    ]
})
export class ActionComponentsModule {
}
