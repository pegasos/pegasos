import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';
import { AppService } from '../../services/app.service';

@Pipe({
  name: 'mydate'
})
export class DateFormatPipe /* extends DatePipe*/ implements PipeTransform {
  constructor(private app: AppService) {
  }

  transform(value: any, args?: any): any {
    return formatDate(value, 'short', this.app.getLocale());
  }
}
