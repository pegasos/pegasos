import { Component, OnInit } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-edit-button',
  template: `<div class="edit-btn"></div>`,
  // <fa-icon icon="pencil-alt" class="delete-icon trn-children" title="diary_delete" data-trn-children="title"
  //  (click)="onSessionDelete(idx, value.id)" size="lg"></fa-icon>
  styles: [`
    .edit-btn {
      background-size: 1.4em 1.4em;
      background-repeat: no-repeat;

      height: 1.37em;
      width: 1.37em;
      vertical-align: -0.225em;

      background-image: url('/assets/img/edit.png');

      display: inline-block;

      cursor: pointer;
    }

    .edit-btn:hover {
      background-image: url('/assets/img/edit_hover.png');
    }`
  ],
  // encapsulation: ViewEncapsulation.Native
})
export class EditButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
