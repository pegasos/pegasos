import { Component, Input, OnInit } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-info-button',
  template: `<div class="info trn-attr" data-trn-attr="title" [title]="text"></div>`,
  styles: [`
    .info {
      background-image: url('/assets/img/info.png');
      background-position: 50% 50%;
      background-position-x: 50%;
      background-position-y: 50%;
      background-repeat: no-repeat;
      color: rgb(37, 37, 37);
      cursor: pointer;

      float: left;
      text-align: right;

      height: 20px;
      margin-left: 3px;
      width: 20px;
    }

    .info:hover {
        background-image: url('/assets/img/info_hover.png');
    }`]
  // encapsulation: ViewEncapsulation.Native
})
export class InfoButtonComponent implements OnInit {

  @Input() text!: string;

  constructor() { }

  ngOnInit(): void {

  }

}
