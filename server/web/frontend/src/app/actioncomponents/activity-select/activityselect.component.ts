import { Component, OnInit, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ParamMap } from '@angular/router';
import { activitytypes } from 'src/app/shared/activities';

@Component({
  selector: 'app-activity-select',
  templateUrl: `./activityselect.component.html`,
  styleUrls: [`./activityselect.component.scss`],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ActivitySelectComponent),
      multi: true
    }
  ]
})
export class ActivitySelectComponent implements OnInit, ControlValueAccessor {

  @Input() singleSelection = false;
  @Input() formText = '(activity)';
  @Input() disabled = false;

  public activitytypes = activitytypes;

  personsdSettings: any = {};
  options: any = {};

  selectionLimit!: number;

  selectedItems: Array<any> = [];

  ngOnInit(): void {
    if( this.singleSelection ) {
      this.selectionLimit = 1;
    }
  }

  get inputData() {
    return this.selectedItems;
  }

  writeValue(value: any): void {
    this.selectedItems = value;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public onElementChange(event: any): void {
    const item = event.itemValue;
    const select = this.selectedItems.includes(item);
    // console.log('onelementchange', event, select);

    if ( select ) {
      this.onActivityTypeSelect(item);
    } else {
      this.onActivityTypeDeSelect(item);
    }
  }

  onActivityTypeSelect(item: any) {
    if ( item.level < 3 ) {
      const selected = this.selectedItems;
      // console.log('ATs', item, selected);

      // ensure that newly selected item is not in the 'selected' set
      // selected= selected.filter(s => s.id !== item.id);

      const ids = selected.map(s => s.id);

      this.activitytypes.forEach(a => {
        if ( ids.indexOf(a.id) === -1 && a.level > item.level ) {
          if ( item.level === 2 && a.param0 === item.param0 && a.param1 === item.param1 ) {
            selected.push(a);
          } else if ( item.level === 1 && a.param0 === item.param0 ) {
            selected.push(a);
          }
        }
      });

      // this.myForm.get('activitytypes').setValue(selected);
    }

    this.propagateChange(this.selectedItems);
  }

  onActivityTypeDeSelect(item: any) {
    if ( item.level < 3 ) {
      let selected = this.selectedItems;
      // console.log('ATds', item, selected);

      const ids = selected.map(s => s.id);
      const toRemove: Array<number> = [];

      this.activitytypes.forEach(a => {
        if ( ids.indexOf(a.id) !== -1 && a.level > item.level ) {
          if ( item.level === 2 && a.param0 === item.param0 && a.param1 === item.param1 ) {
            toRemove.push(a.id);
          } else if ( item.level === 1 && a.param0 === item.param0 ) {
            toRemove.push(a.id);
          }
        }
      });

      selected = selected.filter(a => toRemove.indexOf(a.id) === -1);

      this.selectedItems = selected;
    }
    this.propagateChange(this.selectedItems);
  }

  /**
   * Check whether this dropdown has items in it

  isReady(): boolean {
    return this.personsAvailable.length > 0;
  }*/

  public hasSelectedActivities(): boolean {
    return this.selectedItems !== undefined && this.selectedItems.length > 0;
  }

  /**
   * Update the selection based on the route and set the corresponding parameter in a request (formData)
   * @param params
   * @param formData
   */
  updateRouteForm(params: ParamMap, formData: {[key:string]: any} | null = null): void {
    if ( params.has('activities') ) {
      const data = JSON.parse(params.get('activities') || '{}');

      this.selectedItems = [];
      // { 'id': 2, 'itemName': 'Training', 'param0': 1, 'level': 1}
      data.forEach((element:any) => {
        switch ( element.level ) {
          case 1:
            this.selectedItems.push(this.activitytypes.filter(t => t.level === 1 && t.param0 === element.param0 )[0]);
            break;
          case 2:
            this.selectedItems.push(this.activitytypes.filter(t => t.level === 2 && t.param0 === element.param0 &&
              t.param1 === element.param1 )[0]);
            break;
          case 3:
            this.selectedItems.push(this.activitytypes.filter(t => t.level === 3 && t.param0 === element.param0 &&
              t.param1 === element.param1 && t.param2 === element.param2 )[0]);
            break;
        }
      });

      if ( formData !== null ) {
        formData['activitytypes'] = data;
      }
    } else if ( formData !== null ) {
      formData['activitytypes'] = [];
    }
  }

  /**
   * Get data for a request (formData)
   * @param formData if != null this data will be used as basis for the returned value
   */
   public getFormData(formData: {[key:string]: any} | null = null): {[key:string]:any} {
     if ( formData === null ) {
      formData = {};
     }

     if ( this.selectedItems !== null && this.selectedItems.length > 0 ) {
      formData['activitytypes'] = this.selectedItems.map(element => {
        switch ( element.level ) {
          case 1:
            return {'len': 1, 'param0': element.param0};
          case 2:
            return {'len': 1, 'param0': element.param0, 'param1': element.param1};
          case 3:
            return {'len': 1, 'param0': element.param0, 'param1': element.param1, 'param2': element.param2};
        }
        return undefined;
       });
     }
    return formData;
  }

  public updateRouteParams(params: {[key:string]: any}): void {
    console.log('ActivitySelectComponent::updateRouteParams', {params}, {...this.selectedItems});
    console.log('selectedItems', this.selectedItems);

    if (this.selectedItems == null ) {
      delete params['activities'];
    } else {
      const types = this.selectedItems.map(
        a => ({'level': a.level, 'param0': a.param0, 'param1': a.param1, 'param2': a.param2}));

      if (types.length > 0 ) {
        params['activities'] = JSON.stringify(types);
      } else {
        delete params['activities'];
      }
    }
  }
}
