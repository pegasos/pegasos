import { Component, OnInit, Input, ChangeDetectorRef, Renderer2, ElementRef, AfterViewInit } from '@angular/core';
import { faCaretSquareRight, faCaretSquareDown } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-expand-button',
  template: `
  <!-- <button (click)="onClick()" (keydown)="onKeydown($event)" (focus)="onFocus($event)" (blur)="onBlur($event)">
      <ng-content></ng-content>
  </button> -->
  <fa-icon [icon]="icon" (click)="onClick()" (keydown)="onKeydown($event)" (focus)="onFocus($event)" (blur)="onBlur($event)" style="padding-right: 5px">
  `,
})
export class ExpandButtonComponent implements OnInit, AfterViewInit {

  @Input() content!: string;

  @Input() expanded = false;

  private controlledNode!: HTMLElement;

  public icon = faCaretSquareRight;

  constructor(private renderer: Renderer2, private hostElement: ElementRef, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.content ) {
      const tmp = document.getElementById(this.content);
      if (tmp)
        this.controlledNode = tmp;
    }
    // console.log('ngOnInit', this.expanded, this.content, this.controlledNode, this.hostElement);
  }


  ngAfterViewInit(): void {
    if ( this.expanded ) {
      this.showContent();
    } else {
      this.hideContent();
    }
  }

  public onClick(): void {
    // console.log('onClick', this.expanded, this.content, this.controlledNode);
    this.toggleExpand();
  }

  private toggleExpand(): void {
    if ( this.expanded ) {
      this.hideContent();
    } else {
      this.showContent();
    }
    // this.cd.detectChanges();
  }

  private hideContent(): void {
    this.expanded = false;
    this.icon = faCaretSquareRight;

    if (this.controlledNode) {
      this.renderer.setStyle(this.controlledNode, 'display', 'none');
      // this.controlledNode.style.display = 'none';
    }
  }

  private showContent(): void {
    this.expanded = true;
    this.icon = faCaretSquareDown;

    if (this.controlledNode) {
      this.renderer.setStyle(this.controlledNode, 'display', 'block');
      // this.controlledNode.style.display = 'block';
    }
  }

  public onKeydown(event: any): void {
    console.log('keydown', event.key);

    switch (event.keyCode) {
      case 'Return':
      case 'Enter':
        this.toggleExpand();

        event.stopPropagation();
        event.preventDefault();
        break;

      default:
        break;
    }
  }

  public onFocus(event: any): void {
    // console.log('onFocus');
    // this.domNode.classList.add('focus');
    this.renderer.addClass(this.hostElement.nativeElement, 'focus');
  }

  public onBlur(event: any): void {
    // console.log('onBlur');
    // this.domNode.classList.remove('focus');
    this.renderer.removeClass(this.hostElement.nativeElement, 'focus');
  }
}
