import { Component, OnInit, ComponentFactoryResolver, ChangeDetectorRef, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ChartConfigurer } from './../charts/ChartConfigurer';
import { ChartFactory } from './../charts/ChartFactory';
import { ChartConfig } from './../shared/chartconfig';
import { ChartsHostDirective } from './../charts/chartshost.directive';
import { ChartComponent, ChartHost } from './../charts/chart.component';

import { AppService } from './../services/app.service';

import { Activity, ChartInfo, GenericResponse } from '../shared/sample';
import { interval } from 'rxjs';
import { MenuItem } from 'primeng/api';
import { saveAs } from 'file-saver';

class ChartO {
  config!: ChartConfig;
  component!: ChartComponent;
  finished!: boolean;
}

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.scss']/*,
  entryComponents: [ChartComponent],
  changeDetection: ChangeDetectionStrategy.OnPush*/
})
export class ActivityDetailComponent implements OnInit, ChartHost {

  public sessionId: any;

  public sessionInfo!: Activity;

  private chartinfos!: ChartInfo[];
  public charts: ChartO[] = [];

  private dataXmin = Number.MAX_VALUE;
  private dataXmax = Number.MIN_VALUE;

  private finished = false;

  protected downloads!: MenuItem[];

  /**
   * Host of the charts
   */
  @ViewChildren(ChartsHostDirective) chartsHosts!: any[];

  constructor(private app: AppService,
    private route: ActivatedRoute,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef) {
      this.route.params.subscribe( params => this.sessionId = params['id']);
      // this.route.params.pipe(switchMap((params: Params) => this.sessionId= params['id']));
  }

  public ngOnInit(): void {
    console.log('Displaying session', this.sessionId, this.chartsHosts);
    this.app.setTitle(this.app.translateString('activity'));

    this.app.startLoading();

    this.app.getResource('v1/activities/summary/' + this.sessionId)
      .subscribe(data => this.onSessionInfoReceived(data));
    this.app.getResource('v1/charts/getavailable/' + this.sessionId).subscribe(data => this.onChartsReceived(data));
  }

  private onDataReceived(): void {
    if ( this.sessionInfo && this.chartinfos ) {
      // console.log('Data loaded. Waiting for finish');
      if ( this.finished ) {
        this.app.stopLoading();
      } else {
        const fetcher = interval(5000);
        const subscr = fetcher.subscribe(a => {
          if (!this.finished) {
            console.log('Timeout', this.sessionInfo, this.chartinfos);
            alert('Loading charts failed');
            this.app.stopLoading();
          }
          subscr.unsubscribe();
        });
      }
    }
  }

  private onSessionInfoReceived(data: Activity): any {
    // console.log('session info received', data);
    this.sessionInfo = data;

    const down = this.sessionInfo.downloads ? 
              this.sessionInfo.downloads.map(type => {
                return {
                  label: type.type,
                  command: () => {this.downloadSession(type.type)}
                }
              }) :
              [];
    this.downloads = down;

    this.onDataReceived();
  }

  private onChartsReceived(charts: ChartInfo[]) {
    console.log('Received available charts', charts, this.chartsHosts);

    this.chartinfos = charts;

    // when we receive charts this will always be false --> charts need to be loaded
    // if we don't have charts to display we are already finished.
    this.finished = charts.length === 0;

    // Create the chart hosts by adding to the charts array
    this.chartinfos.forEach((chart) => {
      const charto: ChartO = new ChartO();
      charto.config = ChartConfigurer.getChartConfig(chart);
      charto.config.sessionId = this.sessionId;
      charto.finished = false;
      this.charts.push(charto);
    });

    this.cd.detectChanges();
    this.renderChildren();

    this.onDataReceived();
  }

  /**
   * Create the actual charts. For each chart-host a chart will be created using the factory
   */
  private renderChildren() {
    console.log('renderChildren ', this.chartinfos, this.chartsHosts);

    this.chartsHosts.forEach((chartHost, index) => {
      const containerRef = chartHost.viewContainerRef;
      containerRef.clear();
      if ( this.chartinfos ) {
        const newComponent = containerRef.createComponent(
          ChartFactory.factoryForType(this.componentFactoryResolver, this.chartinfos[index].type));
          (<ChartComponent> newComponent.instance).config = this.charts[index].config;
          (<ChartComponent> newComponent.instance).host = this;
        this.charts[index].component = newComponent.instance;
        this.charts[index].finished = false;
      }
    });
  }

  onSessionDelete(sessionid: number): void {
    console.log('Open session delete', sessionid);
    if ( confirm(this.app.translateString('diary_deleteSession')) ) {
      this.app.startLoading();
      this.app.deleteResource('v1/activities/' + sessionid).subscribe(
        (res: GenericResponse) => {
          if ( res.message === 'OK' ) {
            this.app.goHome();
          } else {
            // we assume that res.message == 'Fail'
            if ( res.error === 'notmanager' ) {
              alert(this.app.translateString('diary_cannotDeleteNotOwner'));
            }
          }
          // alert(res.message + " " + res.error);
          this.app.stopLoading();
        },
        () => this.app.stopLoading());
    }
  }

  onSessionPost(sessionid: number): void {
    console.log('Open session post', sessionid);
    if ( confirm(this.app.translateString('activities_runPostProcessor')) ) {
      this.app.startLoading();
      this.app.putResource('v1/activities/postprocessor/' + sessionid).subscribe(
        (res: GenericResponse) => {
          if ( res.message === 'OK' ) {
            // this.app.goHome();
            // TODO: better notification. Reload?
            alert('Success');
          } else {
            // we assume that res.message == 'Fail'
            if ( res.error === 'notmanager' ) {
              // TODO: error message is wrong
              alert(this.app.translateString('diary_cannotDeleteNotOwner'));
            }
          }
          // alert(res.message + " " + res.error);
          this.app.stopLoading();
        },
        () => this.app.stopLoading());
    }
  }

  protected downloadSession(downloadType: string): void {
    this.app.startLoading();
    this.app.getResourceBlob('v1/activities/' + this.sessionId + '/' + downloadType)
      .subscribe((response) => {
        this.app.stopLoading();
        console.log({response});
        let fileName;
        const contentDisposition = response.headers.get('Content-Disposition');
        if (contentDisposition) {
          const fileNameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
          const matches = fileNameRegex.exec(contentDisposition);
          if (matches != null && matches[1]) {
            fileName = matches[1].replace(/['"]/g, '');
          }
        }
        const type = response.headers.get('Content-Type');
        const fileContent = response.body;
        const blob = new Blob([fileContent], { type: type });
        if( fileName ) {
          saveAs(blob, fileName);
        } else {
          saveAs(blob);
        }
      });
  }

  public OnChartLoaded(who: ChartComponent) {
    // mark this chart as finished
    const chart: ChartO = this.charts.find(c => c.component === who) as ChartO;
    chart.finished = true;

    const finished = this.charts.filter(c => c.finished === false).length === 0;

    const ext = who.getTimeExtend();
    if ( ext ) {
      this.dataXmin = Math.min(this.dataXmin, ext[0]);
      this.dataXmax = Math.max(this.dataXmax, ext[1]);
      console.log(ext, this.dataXmin, this.dataXmax);
    }

    if ( finished ) {
      this.finished = true;

      this.charts.forEach(c => {
        console.log(c, this.dataXmin, this.dataXmax);
        c.component.setGlobalTimeExtent(this.dataXmin, this.dataXmax);
      });

      this.app.stopLoading();
    }
  }

  RequestTimeRestrict(from: number, to: number, extra: number): void {
    this.charts.forEach(chart => chart.component.showTime(from, to, extra));
  }

  RequestNoTimeRestrict(): void {
    this.charts.forEach(chart => chart.component.showAllTime());
  }
}
