import { Component, Input } from '@angular/core';
import { Activity } from '../shared/sample';

@Component({
  selector: 'app-activity-summary',
  templateUrl: './activity-summary.component.html',
  styleUrls: ['./activity-summary.component.css']
})
export class ActivitySummaryComponent {

  @Input() data!: Activity;

  constructor() { }

  ngOnInit() {
  }
}
