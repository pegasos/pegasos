import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityCalendarComponent } from './activity-calendar.component';
import { ActivityDetailComponent } from './activity-detail.component';
import { ActivityListComponent } from './activity-list.component';

export const routes: Routes = [
  { path: '', component: ActivityListComponent },
  { path: 'calendar/view', component: ActivityCalendarComponent },
  { path: 'actd/:id', component: ActivityDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivitiesRoutingModule { }
