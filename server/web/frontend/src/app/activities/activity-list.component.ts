import { Component, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { saveAs } from 'file-saver';

import { AppService } from './../services/app.service';

import { Activity } from '../shared/sample';
import { ActivitiesListComponent } from './activities-list.component';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activities-list.component.scss', './activity-list.component.scss']
})
export class ActivityListComponent extends ActivitiesListComponent {

  constructor(protected override app: AppService,
    protected override route: ActivatedRoute,
    protected override router: Router,
    protected override cd: ChangeDetectorRef,
    protected override fb: FormBuilder) {
    super(app, route, router, cd, fb);
  }

  protected override onDataReceived(data: Activity[]): void {
    super.onDataReceived(data);
    this.onAllDataRecieved();
  }

  private onAllDataRecieved(): void {
    // console.log('onAllDataReceived', this.received_act, this.personSelect.isReady());
    if ( this.hasReceivedActivities() && this.personSelect.isReady() ) {
      this.app.setTitle(this.app.translateString('mainMenu_diary'));

      this.cd.detectChanges();
      this.app.translateContent();
      this.app.stopLoading();
    }
  }

  protected downloadSession(sessionId: number, downloadType: string): void {
    this.app.getResourceBlob('v1/activities/' + sessionId + '/' + downloadType)
      .subscribe((response) => {
        console.log({response});
        let fileName;
        const contentDisposition = response.headers.get('Content-Disposition');
        if (contentDisposition) {
          const fileNameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
          const matches = fileNameRegex.exec(contentDisposition);
          if (matches != null && matches[1]) {
            fileName = matches[1].replace(/['"]/g, '');
          }
        }
        const type = response.headers.get('Content-Type');
        const fileContent = response.body;
        const blob = new Blob([fileContent], { type: type });
        if( fileName ) {
          saveAs(blob, fileName);
        } else {
          saveAs(blob);
        }
      });
  }
}
