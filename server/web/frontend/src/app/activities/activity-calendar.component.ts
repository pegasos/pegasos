import { Component, OnInit, ChangeDetectorRef, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { FullCalendarComponent } from '@fullcalendar/angular';
import { CalendarOptions } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import allLocales from '@fullcalendar/core/locales-all';

import { AppService } from './../services/app.service';
import { Activity, Download } from '../shared/sample';

import { ActivitiesListComponent } from './activities-list.component';

class CalendarActivity implements Activity {
  user_id!: number;
  user_firstname!: string;
  user_lastname!: string;
  team_name!: string;
  team_id!: number;
  group_name!: string;
  group_id!: number;
  session_id!: number;
  activity_name!: string;
  date_time!: number;
  duration!: number;
  tzServer!: number;
  tzLocal!: number;
  list_summary!: { [index: string]: string; };
  downloads!: Download[];

  public id!: string;
  public allDay = false;
  public start!: Date;
  public end!: Date;
  // public startStr: String;
  // public endStr: String;
  public title!: string;
  // public url: String;
  // public classNames = [];
  // public editable = false;
  // public display; The rendering type of this event.
  //  Can be 'auto', 'block', 'list-item', 'background', 'inverse-background', or 'none'. See eventDisplay.
  public backgroundColor = '#f00';
  // public borderColor;
  public textColor = '#f00';

  public static fromActivity(a: Activity): CalendarActivity {
    const ret = a as CalendarActivity;

    ret.id = '' + ret.session_id;
    ret.start = new Date(ret.date_time);

    ret.end = new Date(ret.date_time + ret.duration * 1000);

    if ( ret.activity_name !== undefined && ret.activity_name !== '' ) {
      ret.title = ret.activity_name;
    }

    if ( a.user_id === 2 ) {
      ret.backgroundColor = '#0f0';
    } else {
      ret.backgroundColor = '#00f';
    }

    ret.textColor = '#00FF00';

    return ret;
  }
}

@Component({
  selector: 'app-activity-calendar',
  templateUrl: './activity-calendar.component.html',
  styleUrls: ['./activities-list.component.scss', './activity-calendar.component.scss']
})
export class ActivityCalendarComponent extends ActivitiesListComponent implements OnInit, AfterViewInit {

  public activitiesCalendar!: CalendarActivity[];

  @ViewChild('calendar') calendar!: FullCalendarComponent;

  /**
   * Options for the calendar module
   */
  optionsCalendar!: CalendarOptions;

  routeParams!: ParamMap;

  constructor(protected override app: AppService,
    protected elRef: ElementRef,
    protected override route: ActivatedRoute,
    protected override router: Router,
    protected override cd: ChangeDetectorRef,
    protected override fb: FormBuilder) {
    super(app, route, router, cd, fb);
  }

  override ngOnInit() {
    super.ngOnInit();

    this.optionsCalendar = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      // defaultDate: '2020-07-01',
      headerToolbar: {
          start: 'prev,next',
          center: 'title',
          // right: 'dayGridMonth,timeGridWeek,timeGridDay'
          end: 'dayGridMonth'
      },
      locales: allLocales,
      // TODO: react to dynamic language changes
      locale: this.app.getCurrentLanguage().locale,
      // Monday is always the first day ...
      firstDay: 1,
      eventClick: (info: any) => this.onSessionDetails(info.event.extendedProps.session_id),
    };
  }

  ngAfterViewInit(): void {
    // this.calendar.getCalendar().gotoDate(new Date('2020-08-01'));
    // aria-label="prev"
    // .fc_today-button
    // fc-next-button
    this.elRef.nativeElement.querySelector('button.fc-prev-button').addEventListener('click', () => this.onPreviousMonth());
    this.elRef.nativeElement.querySelector('button.fc-next-button').addEventListener('click', () => this.onNextMonth());
  }

  protected override onDataReceived(data: Activity[]): void {
    super.onDataReceived(data);
    this.activitiesCalendar = this.activities.map(act => CalendarActivity.fromActivity(act));
    console.log('onDataReceived', this.activitiesCalendar);
    this.onAllDataRecieved();
  }

  private onAllDataRecieved(): void {
    console.log('onAllDataReceived', this.hasReceivedActivities() /*, this.personsf_navailable*/);
    if ( this.hasReceivedActivities() /* && this.personsAvailable !== undefined */ ) {
      this.app.setTitle(this.app.translateString('mainMenu_diary'));

      this.cd.detectChanges();
      this.app.translateContent();
      console.log('stop loading');
      this.app.stopLoading();
    }
  }

  protected override formDataFromRoute(params: ParamMap): {} {

    const formData = super.formDataFromRoute(params);

    console.log('routeUpdate', {params});
    const date = this.timePeriod.update(params, formData);
    // console.log('Form update', date);
    this.gotoDate(date);

    return formData;
  }

  protected override routeUpdate(params: ParamMap): void {
    this.routeParams = params;

    super.routeUpdate(params);
  }

  gotoDate(date: Date) {
    this.calendar.getApi().gotoDate(date);
  }

  onNextMonth() {
    const curMonth: Date = this.calendar.getApi().getDate();

    this.app.startLoading();
    this.addRouteParams({'month': curMonth.getFullYear() + '-' + (curMonth.getMonth() + 1)});
  }

  onPreviousMonth() {
    const curMonth: Date = this.calendar.getApi().getDate();

    this.app.startLoading();
    this.addRouteParams({'month': curMonth.getFullYear() + '-' + (curMonth.getMonth() + 1)});
  }

  private addRouteParams(params: { }) {
    let newParams: {[key:string]: string | null} = {};
    this.routeParams.keys.forEach(key => {
      newParams[key] = this.routeParams.get(key);
    });
    if ( 'month' in params ) {
      delete newParams['start'];
      delete newParams['end'];
    }
    newParams = {...newParams, ...params};

    const urlTree = this.router.parseUrl(this.router.url);
    const urlWithoutParams = urlTree.root.children['primary'].segments.map(it => it.path).join('/');
    this.router.navigate(['/' + urlWithoutParams, newParams]);
  }
}
