import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { FullCalendarModule } from '@fullcalendar/angular';
import { SplitButtonModule } from 'primeng/splitbutton';

import { ActionComponentsModule } from '../actioncomponents/action.components.module';

import { ActivityCalendarComponent } from './activity-calendar.component';
import { ActivityListComponent } from './activity-list.component';
import { ActivityDetailComponent } from './activity-detail.component';
import { ActivitySummaryComponent } from './activity-summary.component';
import { ChartsModule } from '../charts/charts.module';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule, ReactiveFormsModule,
        TableModule, CalendarModule,
        SplitButtonModule,
        FullCalendarModule,
        FontAwesomeModule,
        ActionComponentsModule,
        ChartsModule,
    ],
    declarations: [
        ActivityCalendarComponent,
        ActivityListComponent,
        ActivityDetailComponent,
        ActivitySummaryComponent,
    ],
    exports: [],
    providers: [],
    bootstrap: []
})
export class ActivitiesCommonModule {
}
