import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, PRIMARY_OUTLET, Router, UrlSegment } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { AppService } from './../services/app.service';
import { TimePeriodComponent } from './../actioncomponents/time-period/timeperiod.component';
import { PersonSelectComponent } from '../actioncomponents/person-select/personselect.component';

import { activitytypes } from '../shared/activities';
import { GenericResponse } from './../shared/sample';
import { Activity } from '../shared/sample';
import { ActivitySelectComponent } from '../actioncomponents/activity-select/activityselect.component';

@Component({template: './activity-calendar.component.html'})
export abstract class ActivitiesListComponent implements OnInit {

  public yearRange: string = (new Date().getFullYear() - 10) + ':' + (new Date().getFullYear() + 1);

  public activities!: Activity[];

  @ViewChild('activitiesSelect') protected activitiesSelect!: ActivitySelectComponent;

  @ViewChild('personSelect') protected personSelect!: PersonSelectComponent;

  @ViewChild('timePeriod') public timePeriod!: TimePeriodComponent;

  activitesSettings: any = {};

  myForm!: FormGroup;

  private received_act = false;

  tWa = new Date();

  constructor(protected app: AppService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cd: ChangeDetectorRef,
    protected fb: FormBuilder) {
    this.tWa.setDate(this.tWa.getDate() - 14);
  }

  ngOnInit() {
    this.app.startLoading();

    this.myForm = this.fb.group({
      activities: new FormControl(),
      persons: new FormControl({value: [], disabled: true})
    });
  }

  public hasReceivedActivities(): boolean {
    return this.received_act;
  }

  protected onDataReceived(data: Activity[]): void {
    console.log('onDataReceived');
    this.activities = data;
    this.received_act = true;
  }

  public onPersonsReady(): void {
    // console.log('onPersonsReady');
    this.myForm.get('persons')?.enable();
    this.cd.detectChanges();
    this.route.paramMap.subscribe((params: ParamMap) => this.routeUpdate(params));
  }

  public onSessionDetails(sessionid: number): void {
    const url = this.router.serializeUrl(this.router.createUrlTree([{}, 'actd', sessionid]));

    console.log('Open session details', sessionid, url);
    // window.open(this.app.baseURL + '/tagebuch/actd/' + sessionid);
    window.open(url);
  }

  public onSessionDelete(idx: number, sessionid: number): void {
    console.log('Open session delete', sessionid, idx);
    if ( confirm(this.app.translateString('diary_deleteSession')) ) {
      this.app.startLoading();
      this.app.deleteResource('v1/activities/' + sessionid).subscribe(
        (res: GenericResponse) => {
          if ( res.message === 'OK' ) {
            this.activities.splice(idx, 1);
          } else {
            // we assume that res.message == 'Fail'
            if ( res.error === 'notmanager' ) {
              alert(this.app.translateString('diary_cannotDeleteNotOwner'));
            }
          }
          // alert(res.message + " " + res.error);
          this.app.stopLoading();
        },
        () => this.app.stopLoading());
    }
  }

  protected updateRouteParams(params: {[key:string]: any}): void {
    this.activitiesSelect.updateRouteParams(params);
  }

  public filter(): void {
    const params = {};
    this.personSelect.updateRouteParams(params);
    this.timePeriod.updateRouteParams(params);
    this.updateRouteParams(params);

    // TODO: the user might want to re-request the data.
    // If the parameters haven't change we are not requesting data

    const urlTree = this.router.parseUrl(this.router.url);
    const urlWithoutParams = urlTree.root.children['primary'].segments.map(it => it.path).join('/');
    this.router.navigate(['/' + urlWithoutParams, params]).then((val: boolean) => {
      console.log('Routing result:', val);
    });
  }

  protected formDataFromRoute(params: ParamMap): {} {
    const formData: {[key:string]: any} = {};
    this.activitiesSelect.updateRouteForm(params, formData);

    // console.log('routeUpdate', {params});
    this.timePeriod.update(params, formData);

    this.personSelect.updateRouteForm(params, formData);

    return formData;
  }

  protected routeUpdate(params: ParamMap): void {
    const formData = this.formDataFromRoute(params);

    this.app.startLoading();
    this.app.getResourcePost('v1/activities/get', formData).subscribe(data => this.onDataReceived(data));
  }

  /**
   * Download activities as CSV file
   */
  public downloadFile(): void {
    let data = 'session_id,user_firstname,user_lastname,team_name,group_name,date_time,duration,activity_name\n';
    this.activities.forEach(activity => {
      data += activity.session_id + ',' + activity.user_firstname + ',' + activity.user_lastname + ',' + activity.team_name + ',' +
        activity.group_name + ',' + activity.date_time + ',' + activity.duration + ',' + activity.activity_name + '\n';
    });
    const blob = new Blob([data], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}
