import { NgModule } from '@angular/core';
import { ActivitiesCommonModule } from './activities-common.module';
import { ActivitiesRoutingModule } from './activities-routing.module';

@NgModule({
    imports: [
        ActivitiesRoutingModule,
        ActivitiesCommonModule,
    ],
    declarations: [],
    providers: [],
    bootstrap: []
})
export class ActivitiesModule {
}
