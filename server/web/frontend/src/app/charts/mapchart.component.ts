import { AppService } from './../services/app.service';
import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import OlMap from 'ol/Map';
import OlView from 'ol/View';
import OSM from 'ol/source/OSM';
import Feature from 'ol/Feature';
import Geometry from 'ol/geom/Geometry';
import TileLayer from 'ol/layer/Tile';
import TileSource from 'ol/source/Tile';
import OlLineString from 'ol/geom/LineString';
import OlStyle from 'ol/style/Style';
import OlStroke from 'ol/style/Stroke';
import { transform } from 'ol/proj';

import { ChartComponent } from './chart.component';
import { MapChart, GeoPoint } from '../shared/sample';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Coordinate } from 'ol/coordinate';

@Component({
  template: `
    <div class="chart" *ngIf="config">
      <div class="mapcontainercontainer">
      <div [id]='config.svgid' class="mapcontainer">
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
      </div>
    </div>
  `,
  styleUrls: ['./chart.component.scss']
})
export class MapChartComponent extends ChartComponent implements OnInit, AfterViewInit {
  // config: ChartConfig= null;
  data!: MapChart;

  private map!: OlMap;
  private source!: OSM;
  private layer!: TileLayer<TileSource>;
  feature!: Feature<Geometry>;
  featureMarked!: Feature<Geometry>;

  constructor(app: AppService,
    private cd: ChangeDetectorRef) {
    super(app);
    this.margin = {top: 20, right: 20, bottom: 30, left: 40};
  }

  ngAfterViewInit(): void {
    console.log('Chartcomponent::ngAfterViewInit');
    // this.cd.detectChanges();

    this.initSvg();
    this.app.getResource('v1/charts/getgps/' + this.config.displayname + '/session/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
    // this.whatever();
  }

  ngOnInit() {
    // this.cd.detectChanges();
    console.log('ChartComponent::OnInit');
  }

  private onChartDataReceived(data: MapChart): void {
    // console.log("map received:", data);
    this.data = data;
    this.dataToScreen();

    this.host.OnChartLoaded(this);

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);
  }

  protected override initSvg() {
    const el = document.getElementById(this.config.svgid);
    if (el) {
      this.width = el.getBoundingClientRect().width - this.margin.left - this.margin.right;
      this.height = el.getBoundingClientRect().height - this.margin.top - this.margin.bottom;
    } else {
      console.error('Suprious initialisation error: Could not get map area from document');
    }

    this.source = new OSM();

    this.layer = new TileLayer({
      source: this.source
    });

    this.map = new OlMap({
      target: this.config.svgid,
      layers: [this.layer],
      view: new OlView({
        zoom: 14
      })
    });
  }

  private dataToScreen(): void {
    // console.log([this.data.points[0].lat, this.data.points[0].lon]);
    // this.view.centerOn([this.data.points[0].lon, this.data.points[0].lat], 10, 10);
    // this.view.setCenter(fromLonLat([this.data.points[0].lon, this.data.points[0].lat]));
    // this.map.getView().setCenter(transform([this.data.points[0].lon, this.data.points[0].lat], 'EPSG:4326', 'EPSG:3857'));
    const points: Coordinate[] = [];
    this.data.points.forEach((p: GeoPoint) => {
      points.push(transform([p.lon, p.lat], 'EPSG:4326',   'EPSG:3857'));
    });

    const lineStyle = new OlStyle({stroke: new OlStroke({color: '#FF0011', width: 3})});

    this.feature = new Feature({
      geometry: new OlLineString(points, 'XY'),
      name: 'Line'
    });
    this.feature.setStyle(lineStyle);

    const lineStyle2 = new OlStyle({stroke: new OlStroke({color: '#1100FF', width: 3})});

    this.featureMarked = new Feature({
      // geometry: new OlLineString(points, 'XY'),
      name: 'LineMarked'
    });
    this.featureMarked.setStyle(lineStyle2);
    // const ext= new OlExtent(feature.getGeometry().getExtent());
    // console.log("Center?", ext.getCenter());

    const layerLines = new VectorLayer({
      source: new VectorSource({
          features: [this.feature, this.featureMarked]
      })
      // , style: {strokeColor: "green", strokeWidth: 5}
    });

    this.map.addLayer(layerLines);

    this.fit();
  }

  private fit(what?: Feature<Geometry>): void {
    const fitTo = what ? what : this.feature;
    const extend = fitTo.getGeometry()?.getExtent();
    if ( extend )
      this.map.getView().fit(extend, {padding: [10, 10, 10, 10]});
    else
      console.error('Could not get extend of', fitTo);
  }

  public override showTime(from: number, to: number, extra: number): void {
    const points2: Coordinate[] = [];
    this.data.points.filter(p => p.time >= from && p.time <= to)
      .forEach((p: GeoPoint) => {
        points2.push(transform([p.lon, p.lat], 'EPSG:4326',   'EPSG:3857'));
    });

    this.featureMarked.setGeometry(new OlLineString(points2, 'XY'));

    this.fit(this.featureMarked);
  }

  public override showAllTime(): void {
    const points: Coordinate[] = [];
    this.featureMarked.setGeometry(new OlLineString(points, 'XY'));

    this.fit();
  }
}
