import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[charts-host]',
})
export class ChartsHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
