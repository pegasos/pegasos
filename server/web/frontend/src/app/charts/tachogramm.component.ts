import { Component } from '@angular/core';

import { extent } from 'd3-array';

import { SimpleChart } from './../shared/sample';
import { ChartAnimations, Point } from './chart.component';
import { LineChartComponent } from './linechart.component';

@Component({
  animations: ChartAnimations,
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid' [@AvailOrNot]="AvailStatus">
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
})
export class TachogrammChartComponent extends LineChartComponent {
  /**
   * Data as received from the backend
   */
  override data!: SimpleChart;
  /**
   * Data to display
   */
  override dataPoints!: Point[];

  AvailStatus = 'available';

  protected override setData(data: any): void {
    this.data = data;
    this.dataPoints = data.points;

    this.xExtent = extent(this.dataPoints, function(d: Point) { return d.x; }) as [number, number];
    const yExtent = extent(this.dataPoints, function(d: Point) { return d.y; }) as [number, number];
    this.yExtent = [Math.floor(yExtent[0] / 50)*50, Math.ceil(yExtent[1] / 50)*50];
  }

  override ngAfterViewInit(): void {
    this.width = this.config.width;
    this.height = this.config.height;

    this.initSvg();
    this.app.getResource('v1/charts/hrvList/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
  }

  protected override onChartDataReceived(data: SimpleChart) {
    // We now need to 'correct' hrv data as it several R-R ints will have the same time stamp
    // Data with higher indices are older
    for (let i = 0; i < data.points.length - 1; i++) {
      if ( data.points[i].x === data.points[i + 1].x ) {
        if ( i + 2 < data.points.length && data.points[i].x === data.points[i + 2].x) {
          if ( i + 3 < data.points.length && data.points[i].x === data.points[i + 3].x) {
            data.points[i].x -= 750;
            data.points[i + 1].x -= 500;
            data.points[i + 2].x -= 250;
          } else {
            data.points[i].x -= 666;
            data.points[i + 1].x -= 333;
          }
        } else {
          data.points[i].x -= 500;
        }
      }
    }
    super.onChartDataReceived(data);
  }

  protected override scaleData(): void {
    throw Error('should not happen. Why would you scale a tachogram?');
  }
}
