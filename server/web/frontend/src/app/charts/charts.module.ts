import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CHARTCOMPONENTS } from './ChartFactory';
import { ChartsHostDirective } from './chartshost.directive';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { TableModule } from 'primeng/table';

@NgModule({
    declarations: [
        ChartsHostDirective,
        CHARTCOMPONENTS,
    ],
    imports: [
        CommonModule,
        NgxUiLoaderModule,
        TableModule,
    ],
    exports: [
        ChartsHostDirective,
        CHARTCOMPONENTS,
    ]
})
export class ChartsModule {

}
