import { AppService } from './../services/app.service';
import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Format from 'd3-format';

import { ChartConfig } from './../shared/chartconfig';

import { ChartAnimations, ChartComponent, Point } from './chart.component';

@Component({
  animations: ChartAnimations,
  template: `
    <div class="chart" *ngIf="config">
      <h4 class="trn">{{config.displayname}}</h4>

      <svg width="590" height="590" style="background-color: #FFFFFF" [id]='config.svgid' [@AvailOrNot]="AvailStatus">
      </svg>
    </div>
  `,
  styleUrls: ['./chart.component.scss']
})
export class PoincareChartComponent extends ChartComponent implements AfterViewInit {
  override config!: ChartConfig;
  data: any;

  AvailStatus = 'available';

  constructor(app: AppService,
    private cd: ChangeDetectorRef) {
    super(app);
    this.margin = {top: 20, right: 20, bottom: 30, left: 40};
    this.width = 590;
    this.height = 590;
    // console.log('Chartcomponent::ChartComponent');
  }

  ngAfterViewInit(): void {
    // console.log('Chartcomponent::ngAfterViewInit');
    // this.cd.detectChanges();

    this.initSvg();
    this.app.getResource('v1/charts/poincare?session=' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
  }

  private onChartDataReceived(data: any) {
    this.data = data;

    this.displayData();

    this.host.OnChartLoaded(this);

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);
  }

  private displayData() {
    const maxRR = d3Array.max(this.data.points, function(d: Point) { return Math.max(d.x, d.y); } ) as number;
    const minRR = d3Array.min(this.data.points, function(d: Point) { return Math.min(d.x, d.y); } ) as number;

    const x = d3Scale.scaleLinear()
      .domain([Math.floor(minRR / 50)*50, Math.ceil(maxRR / 50)*50])
      .rangeRound([0, this.innerWidth]);
    const y = d3Scale.scaleLinear()
      .domain([Math.floor(minRR / 50)*50, Math.ceil(maxRR / 50)*50])
      .rangeRound([this.innerHeight, 0]);

    const xAxis = d3Axis.axisBottom(x).tickPadding(10).tickFormat(d3Format.format(".0f"));
    const yAxis = d3Axis.axisLeft(y).tickFormat(d3Format.format(".0f"));

    this.svg.append('g')
      .attr('transform', 'translate(0,' + this.innerHeight + ')')
      .call(xAxis)
      .append('text')
        .attr('fill', '#000')
        .attr('x', this.innerHeight - 6)
        .attr('y', '6')
        .attr('dy', '-0.71em')
        .attr('text-anchor', 'end')
        .attr('class', 'trn')
        .style('font-size', '15px')
        .text('charts_rr_interval_axis');

    this.svg.append('g')
      .call(yAxis)
      .append('text')
        .attr('fill', '#000')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .attr('class', 'trn')
        .style('font-size', '15px')
        .text('charts_rr_interval_axis');

    this.svg.selectAll('.dot')
      .data(this.data.points)
      .enter().append('circle')
      .attr('class', 'dot')
      .attr('r', 2.5)
      // .attr("realvalue", function(d) { return "" + d.x + "/" + d.y; })
      .attr('cx', function (d: Point) {return x(d.x); })
      .attr('cy', function (d: Point) { return y(d.y); });
  }

  public override showTime(from: number, to: number, extra: number): void {
    this.AvailStatus = 'notavailable';
  }

  public override showAllTime(): void {
    this.AvailStatus = 'available';
  }
}
