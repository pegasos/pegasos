import { AppService } from './../services/app.service';
import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';

import { responsivfy } from '../shared/responsify';

import { SimpleChart } from './../shared/sample';
import { ChartConfig, LineChartConfig } from './../shared/chartconfig';
import { animate, state, style, transition, trigger } from '@angular/animations';

export class Point {
  x: number;
  y: number;

  constructor(x: number, y: number)
  {
    this.x= x;
    this.y= y;
  }
}

export const ChartAnimations = [
  trigger('AvailOrNot', [
    state('notavailable', style({
     opacity: 0.1
    })),
    state('available', style({
      opacity: 1,
    })),
    transition('available => notavailable', [
      animate('300ms ease-out')
    ]),
    transition('notavailable => available', [
      animate('300ms ease-in')
    ])
  ]),
];

export interface ChartHost {
  RequestNoTimeRestrict(): void;
  /**
   * Callback when a chart has been loaded, i.e. data was received
   * @param who reference to chart which was loaded
   */
  OnChartLoaded(who: ChartComponent): void;

  RequestTimeRestrict(from: number, to: number, extra: number): void;
}

export class ChartComponent {

  public config!: ChartConfig;

  public host!: ChartHost;

  protected margin = {top: 20, right: 40, bottom: 30, left: 40};
  /**
   * Total width of the report svg element
   */
  protected width!: number;
  /**
   * Total height of the report svg element
   */
  protected height!: number;
  /**
   * Available drawing space width
   */
  protected innerWidth!: number;
  /**
   * Available drawing space height
   */
  protected innerHeight!: number;

  protected svg!: any;

  constructor(protected app: AppService) {
    console.log('Chartcomponent::ChartComponent');
  }

  protected initSvg() {
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.innerWidth = this.width - this.margin.left - this.margin.right;

    this.svg = d3.select('#' + this.config.svgid)
      .append('svg')
      .attr('class', 'ng-trigger ng-trigger-AvailOrNot')
      .attr('width', this.width)
      .attr('height', this.height)
      .call(responsivfy) // this is all it takes to make the chart responsive
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.svg.append('rect')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('fill', 'white');
  }

  public getTimeExtend(): number[] | undefined {
    return undefined;
  }

  public setGlobalTimeExtent(xmin: number, xmax: number): void {
  }

  public showTime(from: number, to: number, extra: number): void {
  }

  public showAllTime(): void {
  }
}

@Component({
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid'>
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
})
export class D3ChartComponent extends ChartComponent implements OnInit, AfterViewInit {
  override config!: LineChartConfig;
  data!: SimpleChart;

  constructor(app: AppService,
    protected cd: ChangeDetectorRef) {
    super(app);
    console.log('Chartcomponent::ChartComponent');

    this.width = 800;
    this.height = 300;
  }

  ngAfterViewInit(): void {
    console.log('Chartcomponent::ngAfterViewInit');
    // this.cd.detectChanges();

    this.initSvg();
    this.app.getResource('v1/charts/getchart/' + this.config.displayname + '/session/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
    // this.whatever();
  }

  ngOnInit() {
    // this.cd.detectChanges();
    console.log('ChartComponent::OnInit');
  }

  private onChartDataReceived(data: any)
  {
    this.data= data;
    this.whatever();

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);
  }

  private whatever() {
    // this.data.points.forEach(function(d) {d.x= new Date(d.x);});

    const x = d3Scale.scaleTime()
                     .rangeRound([0, this.innerWidth]);

    const y = d3Scale.scaleLinear()
                     .rangeRound([this.innerHeight, 0]);

    const line = d3Shape.line()
                        .x(function(d: any) { return x(d.x); })
                        .y(function(d: any) { return y(d.y); });

    console.log(this.config.displayname + ' ' + this.config.filterymax);
    let data;
    if( this.config.filterymin !== undefined || this.config.filterymax !== undefined )
    {
      if( this.config.filterymin !== undefined && this.config.filterymax !== undefined )
      {
        let ymin= this.config.filterymin;
        let ymax= this.config.filterymax;
        data= this.data.points.filter(function(d, ) {
          return d.y > ymin && d.y < ymax;
        });
      }
      else if( this.config.filterymin !== undefined && this.config.filterymax === undefined )
      {
        let ymin= this.config.filterymin;
        data= this.data.points.filter(function(d) {
          return d.y > ymin;
        });
      }
      else
      {
        let ymax= this.config.filterymax;
        data= this.data.points.filter(function(d) {
          return d.y < ymax;
        });
      }
    }
    else
    {
      data= this.data.points;
    }

    x.domain(d3Array.extent(data, (d: Point) => d.x) as [number, number]);
    y.domain(d3Array.extent(data, (d: Point) => d.y) as [number, number]);

    this.svg.append("g")
          .attr("transform", "translate(0," + this.innerHeight + ")")
          .call(d3Axis.axisBottom(x))
          .select(".domain")
          .remove();

    this.svg.append("g")
          .call(d3Axis.axisLeft(y))
          .append("text")
          .attr("fill", "#000")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "0.71em")
          .attr("text-anchor", "end")
          .attr('class', 'trn')
          .text(this.config.yaxislabel);

    console.log('Chart ', this.config.displayname, this.config.linecolor);
    this.svg.append("path")
          .datum(this.data.points)
          .attr("fill", "none")
          .attr("stroke", this.config.linecolor)
          .attr("stroke-linejoin", "round")
          .attr("stroke-linecap", "round")
          .attr("stroke-width", 1.5)
          .attr("d", line);

    // this.svg.style("background-color", '#FF0000');
  }
}
