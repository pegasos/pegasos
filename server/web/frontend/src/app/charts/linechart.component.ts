import { faCogs, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { AppService } from './../services/app.service';

import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Format from 'd3-format';

import { SimpleChart } from './../shared/sample';
import { LineChartConfig } from './../shared/chartconfig';
import { ChartComponent, Point } from './chart.component';

@Component({template: ``})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class ChartWithLines extends ChartComponent implements AfterViewInit {
  override config!: LineChartConfig;

  x: any;
  xAxis: any;
  y: any;
  /**
   * Extent of the data of this chart
   */
  xExtent!: number[];

  /**
   * Extent of the data of this chart
   */
  yExtent!: number[];

  /**
   * Extent of the data of all charts of this context
   */
  xExtentGlob!: number[];

  lineStart: any;
  lineEnd: any;

  constructor(app: AppService,
    protected cd: ChangeDetectorRef) {
    super(app);
  }

  ngAfterViewInit(): void {
    this.width = this.config.width;
    this.height = this.config.height;

    this.initSvg();
    this.app.getResource('v1/charts/getchart/' + this.config.displayname + '/session/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
  }

  protected onChartDataReceived(data: any) {
    this.setData(data);

    if ( this.config.scaley !== undefined && this.config.scaley !== null ) {
      this.scaleData();
    }

    this.setupChart();
    this.dataToScreen();

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);

    this.host.OnChartLoaded(this);
  }

  protected setData(data: any) {}

  protected scaleData() {}

  protected dataToScreen(): void {
    console.log('Error error');
  }

  protected setupChart(): void {
    this.x = d3Scale.scaleTime()
                     .rangeRound([20, this.innerWidth]);

    this.y = d3Scale.scaleLinear()
                     .rangeRound([this.innerHeight, 0]);

    const x = this.x;
    const y = this.y;

    let line;
    if ( this.config.linetype === 'Line') {
      line = d3Shape.line<Point>()
                        .x(function(d: Point) { return x(d.x); })
                        .y(function(d: Point) { return y(d.y); });
    } else {
      line = d3Shape.line().curve(d3Shape.curveCatmullRom.alpha(0.5))
      .x(function(d: any) { return x(d.x); })
      .y(function(d: any) { return y(d.y); });
    }

    this.x.domain(this.xExtent);
    if ( this.config.y1min !== undefined ) {
      this.yExtent[0] = this.config.y1min;
    }
    if ( this.config.y1max !== undefined ) {
      this.yExtent[1] = this.config.y1max;
    }
    y.domain(this.yExtent);

    // this.addFaIcon(faCogs);

    this.xAxis = d3Axis.axisBottom(this.x)
      /*.tickFormat(function(d) { return d; })*/
      ;

    const axisLabelFontSize = '11px';

    const axx = this.svg.append('g')
      .attr('transform', 'translate(20,' + this.innerHeight + ')')
      .attr('class', 'xAxis')
      .call(this.xAxis);

    axx.append('text')
      .attr('fill', '#000')
      .attr('x', this.innerWidth / 3)
      // .attr('y', '6')
      .attr('dy', '2.5em')
      .attr('text-anchor', 'end')
      .attr('font-size', axisLabelFontSize)
      .attr('class', 'trn')
      .text('Time');

    axx.select('.domain').remove();

    this.svg.append('g')
          .call(d3Axis.axisLeft<number>(y).tickFormat(d3Format.format(".0f")))
          .append('text')
          .attr('fill', '#000')
          .attr('transform', 'rotate(-90)')
          .attr('y', 6)
          .attr('dy', '0.71em')
          .attr('text-anchor', 'end')
          .attr('font-size', axisLabelFontSize)
          .attr('class', 'trn')
          .text(this.config.yaxislabel);
  }

  public override getTimeExtend(): number[] {
    return [this.xExtent[0], this.xExtent[1]];
  }

  public override setGlobalTimeExtent(xmin: number, xmax: number): void {
    this.xExtentGlob = [xmin, xmax];

    this.displayDataTime(xmin, xmax);
  }

  public override showTime(from: number, to: number, extra: number): void {
    if ( this.lineStart ) {
      this.lineStart.remove();
    }
    if ( this.lineEnd ) {
      this.lineEnd.remove();
    }

    this.displayDataTime(Math.max(this.xExtentGlob[0],
      from - extra), to + extra);

    this.lineStart = this.addMarker(from);
    this.lineEnd = this.addMarker(to);
  }

  public override showAllTime(): void {
    if ( this.lineStart ) {
      this.lineStart.remove();
    }
    if ( this.lineEnd ) {
      this.lineEnd.remove();
    }

    this.displayDataTime(this.xExtentGlob[0], this.xExtentGlob[1]);
  }

  public displayDataTime(xmin: number, xmax: number) {
    this.x.domain([xmin, xmax]);
    const svg = this.svg;
    this.svg.selectAll('.myXaxis')
      .transition()
        .duration(1000)
        .call(this.xAxis)
        .on('start', function() {
          svg.select('.myXaxis .domain').remove();
        });
  }

  private addMarker(xpoint: number): any {
    return this.svg.append('line')
      .style('stroke', 'black')
      .style('stroke-dasharray', '3, 3')
      .attr('x1', this.x(xpoint))
      .attr('y1', 1)
      .attr('x2', this.x(xpoint))
      .attr('y2', this.innerHeight - 1);
  }
}


@Component({
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid'>
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
})
export class BkLineChartComponent extends ChartComponent implements AfterViewInit {
  override config!: LineChartConfig;
  /**
   * Data as received from the backend
   */
  data!: SimpleChart;
  /**
   * Data to display
   */
  dataPoints!: Point[];

  x: any;
  xAxis: any;
  y: any;
  /**
   * Extent of the data of this chart
   */
  xExtent!: number[];

  /**
   * Extent of the data of all charts of this context
   */
  xExtentGlob!: number[];

  constructor(app: AppService,
    protected cd: ChangeDetectorRef) {
    super(app);
  }

  ngAfterViewInit(): void {
    this.width = this.config.width;
    this.height = this.config.height;

    this.initSvg();
    this.app.getResource('v1/charts/getchart/' + this.config.displayname + '/session/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
  }

  protected onChartDataReceived(data: any) {
    this.data = data;

    if ( this.config.scaley !== undefined && this.config.scaley !== null ) {
      this.data.points = this.data.points.map((point: Point) => {
        point.y = point.y * this.config.scaley;
        return point;
      });
    }

    this.dataToScreen();

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);

    this.host.OnChartLoaded(this);
  }

  private preprocessData(): void {
    console.log(this.config.displayname + ' ' + this.config.filterymax);
    let data: Point[];
    if ( this.config.filterymin !== undefined || this.config.filterymax !== undefined ) {
      if ( this.config.filterymin !== undefined && this.config.filterymax !== undefined ) {
        const ymin = this.config.filterymin;
        const ymax = this.config.filterymax;
        data = this.data.points.filter(function(d, ) {
          return d.y > ymin && d.y < ymax;
        });
      } else if ( this.config.filterymin !== undefined && this.config.filterymax === undefined ) {
        const ymin = this.config.filterymin;
        data = this.data.points.filter(function(d) {
          return d.y > ymin;
        });
      } else {
        const ymax = this.config.filterymax;
        data = this.data.points.filter(function(d) {
          return d.y < ymax;
        });
      }
    } else {
      data = this.data.points;
    }

    if ( this.config.kernel !== undefined && this.config.kernel === 'RollMean_5' ) {
      /*data.reduce(function(previousValue, currentValue, currentIndex, arr) {
        return null;
      });*/
      const delta = 5000;

      console.log('Applying filter');

      const help: Point[] = [];
      const tmp: Point[] = [];
      let accum = 0.0;
      let count = 0;
      data.forEach((point, idx) => {
        accum += point.y;
        count++;
        help.push(point);
        while ( help.length > 0 && point.x - help[0].x > delta ) {
          const h = help.shift();
          if( !h ) continue;
          accum -= h.y;
          count--;
        }
        tmp.push(new Point(point.x, accum / count));
      });

      data = tmp;
    }

    this.dataPoints = data;
  }

  private dataToScreen() {
    this.preprocessData();

    this.x = d3Scale.scaleTime()
                     .rangeRound([20, this.innerWidth]);

    this.y = d3Scale.scaleLinear()
                     .rangeRound([this.innerHeight, 0]);

    const x = this.x;
    const y = this.y;

    let line;
    if ( this.config.linetype === 'Line') {
      line = d3Shape.line<Point>()
                        .x(function(d: any) { return x(d.x); })
                        .y(function(d: any) { return y(d.y); });
    } else {
      line = d3Shape.line<Point>().curve(d3Shape.curveCatmullRom.alpha(0.5))
      .x(function(d: any) { return x(d.x); })
      .y(function(d: any) { return y(d.y); });
    }

    this.xExtent = d3Array.extent(this.dataPoints, function(d: Point) { return d.x; }) as [number, number];
    this.x.domain(this.xExtent);
    const ext = d3Array.extent(this.dataPoints, function(d: Point) { return d.y; });
    if ( this.config.y1min !== undefined ) {
      ext[0] = this.config.y1min;
    }
    if ( this.config.y1max !== undefined ) {
      ext[1] = this.config.y1max;
    }
    y.domain(ext);

    // this.addFaIcon(faCogs);

    this.xAxis = d3Axis.axisBottom(this.x)
      /*.tickFormat(function(d) { return d; })*/
      ;

    this.svg.append('g')
          .attr('transform', 'translate(20,' + this.innerHeight + ')')
          .attr('class', 'myXaxis')
          .call(this.xAxis)
          .select('.domain')
          .remove();

    this.svg.append('g')
          .call(d3Axis.axisLeft(y))
          .append('text')
          .attr('fill', '#000')
          .attr('transform', 'rotate(-90)')
          .attr('y', 6)
          .attr('dy', '0.71em')
          .attr('text-anchor', 'end')
          .attr('class', 'trn')
          .text(this.config.yaxislabel);

    this.svg.append('path')
          // .datum(this.dataPoints)
          .attr('class', 'data-line')
          .attr('fill', 'none')
          .attr('stroke', this.config.linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', 1.5)
          .attr('d', line(this.dataPoints));

    // this.svg.style("background-color", '#FF0000');
  }

  public override getTimeExtend(): number[] {
    return [this.xExtent[0], this.xExtent[1]];
  }

  public override setGlobalTimeExtent(xmin: number, xmax: number): void {
    this.xExtentGlob = [xmin, xmax];

    this.displayDataTime(xmin, xmax);
  }

  public override showTime(from: number, to: number, extra: number): void {
    this.displayDataTime(Math.max(this.xExtentGlob[0],
      from - extra), to + extra);
  }

  public override showAllTime(): void {
    this.displayDataTime(this.xExtentGlob[0], this.xExtentGlob[1]);
  }

  public displayDataTime(xmin: number, xmax: number) {
    this.x.domain([xmin, xmax]);
    const svg = this.svg;
    this.svg.selectAll('.myXaxis')
      .transition()
        .duration(1000)
        .call(this.xAxis)
        .on('start', function() {
          svg.select('.myXaxis .domain').remove();
        });

    // create local references for the line function
    const x = this.x;
    const y = this.y;

    const line = d3Shape.line<Point>()
      .x(function(d: Point) { return x(d.x); })
      .y(function(d: Point) { return y(d.y); });

    this.svg.select('.data-line')
       .transition()
         .duration(1000)
         .attr('d', line(this.dataPoints));
  }

  protected addFaIcon(icon: IconDefinition, maxSize = 30): void {
    const x = +icon.icon[0];
    const y = +icon.icon[0];
    const scale = maxSize / Math.max(x, y);
    this.svg.append('path')
      .attr('d', icon.icon[4])
      .attr('transform', 'scale(' + scale + ')');
    // console.log(faCogs, faCogs.icon, faCogs.prefix);
  }
}


@Component({
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid'>
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
})
export class LineChartComponent extends ChartWithLines {
  /**
   * Data as received from the backend
   */
  data!: SimpleChart;
  /**
   * Data to display
   */
  dataPoints!: Point[];

  protected override setData(data: any): void {
    this.data = data;
    this.dataPoints = data.points;

    this.xExtent = d3Array.extent(this.dataPoints, function(d: Point) { return d.x; }) as [number, number];
    this.yExtent = d3Array.extent(this.dataPoints, function(d: Point) { return d.y; }) as [number, number];
  }

  protected override scaleData(): void {
    this.data.points = this.data.points.map((point: Point) => {
      point.y = point.y * this.config.scaley;
      return point;
    });

    this.dataPoints = this.data.points;
    this.yExtent = d3Array.extent(this.dataPoints, function(d: Point) { return d.y; }) as [number, number];
  }

  private preprocessData(): void {
    console.log(this.config.displayname + ' ' + this.config.filterymax);
    let data: Point[];
    if ( this.config.filterymin !== undefined || this.config.filterymax !== undefined ) {
      if ( this.config.filterymin !== undefined && this.config.filterymax !== undefined ) {
        const ymin = this.config.filterymin;
        const ymax = this.config.filterymax;
        data = this.data.points.filter(function(d, ) {
          return d.y > ymin && d.y < ymax;
        });
      } else if ( this.config.filterymin !== undefined && this.config.filterymax === undefined ) {
        const ymin = this.config.filterymin;
        data = this.data.points.filter(function(d) {
          return d.y > ymin;
        });
      } else {
        const ymax = this.config.filterymax;
        data = this.data.points.filter(function(d) {
          return d.y < ymax;
        });
      }
    } else {
      data = this.data.points;
    }

    if ( this.config.kernel !== undefined && this.config.kernel === 'RollMean_5' ) {
      /*data.reduce(function(previousValue, currentValue, currentIndex, arr) {
        return null;
      });*/
      const delta = 5000;

      console.log('Applying filter');

      const help: Point[] = [];
      const tmp: Point[] = [];
      let accum = 0.0;
      let count = 0;
      data.forEach((point, idx) => {
        accum += point.y;
        count++;
        help.push(point);
        while ( help.length > 0 && point.x - help[0].x > delta ) {
          const h = help.shift();
          if( !h ) continue;
          accum -= h.y;
          count--;
        }
        tmp.push(new Point(point.x, accum / count));
      });

      data = tmp;
    }

    this.dataPoints = data;
  }

  protected override dataToScreen(): void {
    this.preprocessData();

    const x = this.x;
    const y = this.y;

    let line;
    if ( this.config.linetype === 'Line') {
      line = d3Shape.line<Point>()
                        .x(function(d: any) { return x(d.x); })
                        .y(function(d: any) { return y(d.y); });
    } else {
      line = d3Shape.line<Point>().curve(d3Shape.curveCatmullRom.alpha(0.5))
      .x(function(d: any) { return x(d.x); })
      .y(function(d: any) { return y(d.y); });
    }

    this.svg.append('path')
          // .datum(this.dataPoints)
          .attr('class', 'data-line')
          .attr('fill', 'none')
          .attr('stroke', this.config.linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', 1.5)
          .attr('d', line(this.dataPoints));

    // this.svg.style("background-color", '#FF0000');
  }

  public override displayDataTime(xmin: number, xmax: number) {
    super.displayDataTime(xmin, xmax);

    // create local references for the line function
    const x = this.x;
    const y = this.y;

    const line = d3Shape.line<Point>()
      .x(function(d: Point) { return x(d.x); })
      .y(function(d: Point) { return y(d.y); });

    this.svg.select('.data-line')
       .transition()
         .duration(1000)
         .attr('d', line(this.dataPoints.filter(point => point.x >= xmin && point.x <= xmax)));
  }

  protected addFaIcon(icon: IconDefinition, maxSize = 30): void {
    const x = +icon.icon[0];
    const y = +icon.icon[0];
    const scale = maxSize / Math.max(x, y);
    this.svg.append('path')
      .attr('d', icon.icon[4])
      .attr('transform', 'scale(' + scale + ')');
    // console.log(faCogs, faCogs.icon, faCogs.prefix);
  }
}
