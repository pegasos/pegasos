import { Component } from '@angular/core';

import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import 'd3-transition';

import { ChartWithLines } from './linechart.component';
import { MultiLineChart, DataSeries } from './../shared/sample';
import { MultiLineChartConfig } from './../shared/chartconfig';
import { ChartAnimations, Point } from './chart.component';

@Component({
  animations: ChartAnimations,
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid'>
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
})
export class MultiLineChartComponent extends ChartWithLines {
  override config!: MultiLineChartConfig;
  /**
   * Data as received from the backend
   */
  data!: MultiLineChart;

  origData: Map<string, Point[]> = new Map();

  series: any[] = [];

  protected override setData(data: any): void {
    this.data = data;

    this.xExtent = [Number.MAX_VALUE, Number.MIN_VALUE];
    this.yExtent = [Number.MAX_VALUE, Number.MIN_VALUE];
    this.data.dataSeries.forEach(series => {
      const exty = d3Array.extent(series.data, function(d: Point) { return d.y; }) as [number, number];
      const extx = d3Array.extent(series.data, function(d: Point) { return d.x; }) as [number, number];
      this.xExtent[0] = Math.min(extx[0], this.xExtent[0]);
      this.xExtent[1] = Math.max(extx[1], this.xExtent[1]);
      this.yExtent[0] = Math.min(exty[0], this.yExtent[0]);
      this.yExtent[1] = Math.max(exty[1], this.yExtent[1]);
    });
  }

  private filterData() {
    if( this.config.filterymin !== undefined || this.config.filterymax !== undefined )
    {
      if( this.config.filterymin !== undefined && this.config.filterymax !== undefined )
      {
        const ymin= this.config.filterymin;
        const ymax= this.config.filterymax;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d, ) {
            return d.y > ymin && d.y < ymax;
          });
        });
      }
      else if( this.config.filterymin !== undefined && this.config.filterymax === undefined )
      {
        const ymin= this.config.filterymin;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d) {
            return d.y > ymin;
          });
        });
      }
      else
      {
        const ymax= this.config.filterymax;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d) {
            return d.y < ymax;
          });
        });
      }
    }
  }

  protected override dataToScreen() {
    this.filterData();

    let i = 0;
    this.data.dataSeries.forEach(series => this.addSeries(series, this.config.linecolors[(i++) % this.config.linecolors.length]));

    /*console.log('Data loaded. Waiting for available change');
    const fetcher = interval(5000);
    const subscr = fetcher.subscribe(a => {
      console.log('available change');
      this.AvailStatus = 'available';
      subscr.unsubscribe();
      this.updateXX();
    });*/
  }

  private addSeries(series: DataSeries<Point>, linecolor: string): void {
    console.log('Adding series', series.name);

    // create local references for the line function
    const x = this.x;
    const y = this.y;

    const line = d3Shape.line<Point>()
      .x(function(d: Point) { return x(d.x); })
      .y(function(d: Point) { return y(d.y); });

    this.origData.set(series.name, series.data);

    this.series.push(
    this.svg.append('path')
          // .datum(series.data)
          .attr('class', 'data-' + series.name)
          .attr('fill', 'none')
          .attr('stroke', linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', 1.5)
          // .attr('d', line)
          .attr('d', line(series.data))
    );
  }

  public override displayDataTime(xmin: number, xmax: number) {
    super.displayDataTime(xmin, xmax);

    // create local references for the line function
    const x = this.x;
    const y = this.y;

    const line = d3Shape.line<Point>()
      .x(function(d: Point) { return x(d.x); })
      .y(function(d: Point) { return y(d.y); });

    this.origData.forEach((series, key) => {
      this.svg.select('.data-' + key)
       .transition()
         .duration(1000)
         .attr('d', line(series.filter(point => point.x >= xmin && point.x <= xmax)));
    });
  }
}
