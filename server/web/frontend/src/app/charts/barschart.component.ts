import { AppService } from './../services/app.service';
import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import {select as d3Select } from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { format } from 'd3-format';

import { ChartWithPoints } from './../shared/sample';
import { BarsChartConfig, ChartConfig } from './../shared/chartconfig';

import { ChartAnimations, ChartComponent } from './chart.component';

export interface BarsChart extends ChartWithPoints {
  dataSeries: DataSeries[];
}

export interface DataSeries {
  name: string;
  data: Point[];
  cumsum: number;
}

export interface Point {
  x: number;
  y: number;
  label: string;
  cumsum: number;
}

function myhmsformat (num: number) {
  const h = Math.floor( num / 3600 );
  const m = Math.floor((num - h * 3600) / 60 );
  const s = num - (h * 3600 + m * 60);
  // return (h > 0 ? (( h < 10 ? "0" + h : h ) + ":") : "") + ( m < 10 ? "0" + m : m ) + ":" + ( s < 10 ? "0" + s : s ); }
  return (h > 0 ? h + ':' : '') + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s ); }

@Component({
  animations: ChartAnimations,
  template: `
    <div class="chart" *ngIf="config">
      <div [id]='config.svgid' [@AvailOrNot]="AvailStatus">
        <h4 class='trn'>{{config.displayname}}</h4>
      </div>
    </div>
  `,
  styleUrls: ['./chart.component.scss']
})
export class BarsChartComponent extends ChartComponent implements AfterViewInit {
  override config!: BarsChartConfig;
  data!: BarsChart;

  private x: any;
  private y: any;
  private Tooltip: any;

  AvailStatus = 'available';

  constructor(app: AppService,
    private cd: ChangeDetectorRef) {
    super(app);
  }

  ngAfterViewInit(): void {
    this.margin = {top: 10, right: 5, bottom: 20, left: 50};

    this.width = this.config.width;
    this.height = this.config.height;

    this.initSvg();

    // create the tooltip
    this.Tooltip =
      d3Select('body').append('div')
      .attr('id', 'tooltip' + this.config.svgid)
      .style('opacity', 0)
      .attr('class', 'tooltip')
      .style('position', 'absolute')
      .style('background-color', this.config.tooltip_background)
      .style('font-size', this.config.tooltip_fontsize)
      .style('pointer-events', 'none')
      .style('border', 'solid')
      .style('border-width', '2px')
      .style('border-radius', '5px')
      .style('padding', '5px')
      ;

    this.app.getResource('v1/charts/getchart/' + this.config.displayname + '/session/' + this.config.sessionId)
      .subscribe(data => this.onChartDataReceived(data));
  }

  private onChartDataReceived(data: any) {
    this.data = data;

    this.dataToScreen();

    this.host.OnChartLoaded(this);

    this.cd.detectChanges();
    this.app.translateTree(this.config.svgid);
  }

  private filterData() {
    if( this.config.filterymin !== undefined || this.config.filterymax !== undefined )
    {
      if( this.config.filterymin !== undefined && this.config.filterymax !== undefined )
      {
        const ymin= this.config.filterymin;
        const ymax= this.config.filterymax;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d, ) {
            return d.y > ymin && d.y < ymax;
          });
        });
      }
      else if( this.config.filterymin !== undefined && this.config.filterymax === undefined )
      {
        const ymin= this.config.filterymin;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d) {
            return d.y > ymin;
          });
        });
      }
      else
      {
        const ymax= this.config.filterymax;
        this.data.dataSeries.forEach(series => {
          series.data= series.data.filter(function(d) {
            return d.y < ymax;
          });
        });
      }
    }
  }

  /**
   *
   * @param max time in seconds
   * @param count number of ticks
   */
  private createTimeTicks(max: number, count: number) {
    /**
     * breaks in minutes
     */
    const breaks = [60, 30, 20, 15, 10, 5, 4, 3, 2, 1];

    const quot = breaks.map(b => Math.floor(max / 60 / count / b));
    // console.log({quot, count});
    let num = breaks[breaks.length - 1] * 60;
    for ( const idx in breaks ) {
      if ( quot[idx] > 0 ) {
        num = breaks[idx] * 60;
        break;
      }
    }
    // console.log({num});

    const ret = [];
    let i = 0;
    while ( i < max + num ) {
      ret.push(i);
      i += num;
    }
    return ret;
  }

  private dataToScreen() {
    this.filterData();

    this.x = d3Scale.scaleBand()
                    .range([ 20, Math.min(this.data.dataSeries.length * 25, this.innerWidth) ])
                    .padding(0.2);

    // let data_ymin= Number.MAX_VALUE;
    let data_ymax = Number.MIN_VALUE;

    this.data.dataSeries.forEach((series) => {
      // series.data.map(p => p.label= myhmsformat(p.y));
      let acum = 0;
      series.data.forEach(p => {
        p.label = myhmsformat(p.y);
        p.cumsum = acum;
        acum += p.y;
      });
      series.cumsum = acum;
      // series.points.reduce((a, b) => {p.cumsum= a + b});
    });
    // console.log(this.data.dataSeries);

    this.data.dataSeries.forEach(series => {
      data_ymax = d3Array.max([series.cumsum, data_ymax]) as number;
    });

    this.x.domain(this.data.dataSeries.map(function(d) { return d.name; }))
      .padding(0.1);


    let xAxis;
    switch( this.config.axisType )
    {
      case 'Time':
        const ticks = this.createTimeTicks(data_ymax, Math.floor(this.height / 20));
        this.y = d3Scale.scaleTime()
                  .rangeRound([this.innerHeight, 0])
                  .domain([0, ticks[ticks.length - 1]]);
        xAxis = d3Axis.axisLeft<number>(this.y)
                  .tickFormat(myhmsformat)
                  .tickValues(ticks)
        break;
      case 'Percent':
        this.y = d3Scale.scaleLinear()
                  .rangeRound([this.innerHeight, 0])
                  .domain([0, data_ymax])
                  .nice();
        xAxis = d3Axis.axisLeft<number>(this.y)
                  .tickFormat(format('~%'));
        break;
      case 'Number':
        this.y = d3Scale.scaleLinear()
                  .rangeRound([this.innerHeight, 0])
                  .domain([0, data_ymax])
                  .nice();
        xAxis = d3Axis.axisLeft<number>(this.y);
        break;
    }

    this.svg.append('g')
          .attr('transform', 'translate(0,' + this.innerHeight + ')')
          .call(d3Axis.axisBottom(this.x))
          .select('.domain')
          .remove();

    // add y-axis label
    this.svg.append('g')
          .call(xAxis)
          .append('text')
          .attr('fill', '#000')
          .attr('transform', 'rotate(-90)')
          .attr('y', 6)
          .attr('dy', '0.71em')
          .attr('text-anchor', 'end')
          .attr('class', 'trn')
          .text(this.config.yaxislabel);

    this.data.dataSeries.forEach(series => this.addSeries(series));
  }

  private addSeries(series: DataSeries): void {
    console.log('Adding series', series.name, series.data);

    // create local references for the line function
    const x = this.x;
    const y = this.y;
    const height = this.innerHeight;
    const Tooltip = this.Tooltip;
    const config = this.config;

    this.svg.selectAll('.mybar')
      .data(series.data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('x', function(d: any) { return x(series.name); })
        .attr('width', x.bandwidth())
        .attr('fill', function(d: any, i: number) {return config.colors[i]; } )
        .style('opacity', 0.8)
        .attr('height', function(d: Point) { return height - y(d.y); })
        .attr('y', function(d: Point) { return y(d.cumsum + d.y); })
        .on('mouseover', (d:Point) => {
            Tooltip
              .style('opacity', 1);
            d3Select(d as any)
              .style('stroke', 'black')
              .style('opacity', 1);
          })
        .on('mousemove', function(event: any, d: Point, _:number) {
            Tooltip
              .html(config.tooltip_seriespre + series.name + config.tooltip_seriespost + '<br>' +
                config.tooltip_itempre + d.label + config.tooltip_itempost)
              .style('left', (event.pageX + 9) + 'px')
              .style('top', (event.pageY) + 'px')
          })
        .on('mouseleave', (d: Point) => {
            Tooltip
              .style('opacity', 0);
            d3Select(d as any)
              .style('stroke', 'none')
              .style('opacity', 0.8);
          })
        ;
  }

  public override showTime(from: number, to: number, extra: number): void {
    this.AvailStatus = 'notavailable';
  }

  public override showAllTime(): void {
    this.AvailStatus = 'available';
  }
}
