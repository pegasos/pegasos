import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {
  constructor(private loginService: LoginService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const userInfo = this.loginService.getLoggedInUser();
    if ( userInfo === undefined ) {
      return false;
    } else {
      return true;
    }
  }
}
