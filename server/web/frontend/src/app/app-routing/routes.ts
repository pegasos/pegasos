// Folder: app-routing;
// Generated file. Do not edit


import { Routes } from '@angular/router';

import { MenuGuard } from './menu.guard';
import { LoggedInGuard } from './loggedin.guard';

import { LoginComponent } from './../login/login.component';
import { HomeComponent } from '../home/home.component';
import { LiveComponent } from './../live/live.component';
import { OAuthComponent } from '../oauth/oauth.component';

export const routes: Routes = [
  { path: 'home', loadChildren: () => import('../home/home.module').then(m => m.HomeModule) },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'register', component: LoginComponent },
  { path: 'finishregistration', component: LoginComponent },
  { path: 'team', loadChildren: () => import('../team/team.module').then(m => m.TeamModule) },
  { path: 'tagebuch', loadChildren: () => import('../activities/activities.module').then(m => m.ActivitiesModule) },
  { path: 'reports', loadChildren: () => import('../reports/reports.module').then(m => m.ReportsModule) },
  { path: 'live', loadChildren: () => import('../live/live.module').then(m => m.LiveModule) },
  { path: 'user', loadChildren: () => import('../user/user.module').then(m => m.UserModule) },
  { path: 'admin', loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule), canActivate: [MenuGuard]},

  { path: 'powerprof', loadChildren:
    () => import('../powerprof/power.profile.module').then(m => m.PowerprofModule), canActivate: [MenuGuard] },

  { path: 'hilfe', loadChildren: () => import('../help/help.module').then(m => m.HelpModule) },

  { path: 'oauth2_callback', component: OAuthComponent, canActivate: [LoggedInGuard] },
  { path: 'oauth2_callback/:partnerid', component: OAuthComponent, canActivate: [LoggedInGuard] },

  { path: '', redirectTo: '/home', pathMatch: 'full' }
];
