import { MenuService } from '../services/menu.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { MENUS } from '../shared/menus';
import { LoginService } from '../login/login.service';
import { LoggedInUser } from '../shared/loggedinuser';
import { Role } from '../shared/sample';

function includesAll(inc: Role[], what: string[]): boolean {
  return what.map(x => inc.map(r => r.name).includes(x)).every(check => check === true);
}

@Injectable({
  providedIn: 'root'
})
export class MenuGuard implements CanActivate {
  private userInfo: LoggedInUser | undefined | null = null;

  constructor(private loginService: LoginService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const x = route.routeConfig?.path;
    if( !x ) return false;
    if ( !this.canAccessMenu(x) ) {
      return false;
    }
    return true;
  }

  private async canAccessMenu(action: string): Promise<boolean> {
    if( this.userInfo == null ) {
      this.userInfo = await this.loginService.getLoggedInUser();
    }

    // console.debug('MenuGuard::canAccessMenu', action, this.userInfo);
    const menus = MENUS.filter(men => men.action === action);
    if ( menus.length === 0 ) {
      return false;
    }
    const menu = menus[0];
    if ( this.userInfo !== undefined ) {
      return menu.requiredroles.length === 0 || includesAll(this.userInfo.roles, menu.requiredroles);
    } else {
      return menu.isPublic === true && menu.requiredroles.length === 0;
    }
  }
}
