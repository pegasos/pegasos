import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ActionComponentsModule } from './../actioncomponents/action.components.module';
import { PegasosCommonModule } from './../common/common-module';

import { UserValueComponent } from './user.value.component';
import { UserComponent } from './user.component';
import { AddedComponentsModule } from '../app.module';

const routes: Routes = [
  { path: 'profile', component: UserComponent },
  { path: 'finishmailchange', component: UserComponent },
  { path: 'values/:vid', component: UserValueComponent },
  { path: 'values/:uid/:vid', component: UserValueComponent },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule, ReactiveFormsModule,
        FontAwesomeModule,
        TableModule, CalendarModule,
        ActionComponentsModule,
        PegasosCommonModule,
        AddedComponentsModule,
    ],
    declarations: [
        UserComponent,
        UserValueComponent,
    ],
    exports: [RouterModule]
})
export class UserModule {

}
