import { ValuesService } from './../services/values.service';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit, Optional } from '@angular/core';
import { Params, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { DialogConfig } from './../dialog/dialog-config';

import { AppService } from './../services/app.service';

import { UserDetails, GenericResponse, UserValueInfo, UserValue, Change, ValueChanges } from './../shared/sample';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit/*, AfterViewInit*/ {

  public LEGACY_VALUES: Map<string, string>;

  data!: UserDetails;

  public isDialog: boolean;

  private userId!: number;

  protected changePwForm!: FormGroup;
  protected changeEmailForm!: FormGroup;

  /**
   * From with all the basic user data
   */
  protected userDataForm!: FormGroup;

  /**
   * Whether the user has clicked on an action and its currently pending
   */
  public actionPending = false;

  /**
   * If the user has submitted the password change form
   */
  protected submitted = false;

  /**
   * Whether the user has submitted the email change form
   */
  protected submittedE = false;

  protected message!: string;

  protected changeEmailFormError!: string;

  /**
   * Defined values
   */
  private avail_values!: UserValueInfo[];
  public avail_valuesd!: UserValueInfo[];

  /**
   * Whether the values / information about the values can be shown
   */
  public showValues = false;

  /**
   * Values for which a value is known for the user
   */
  private userValues!: Map<string, UserValue>;

  constructor(private app: AppService,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private valservice: ValuesService,
    @Optional() private config: DialogConfig) {

    this.LEGACY_VALUES = new Map<string, string>();
    this.LEGACY_VALUES.set('HEIGHT', 'height');
    this.LEGACY_VALUES.set('WEIGHT', 'weight');
    this.LEGACY_VALUES.set('VO2MAX', 'vo2max');
    this.LEGACY_VALUES.set('MAXHR', 'hrmax');
    this.LEGACY_VALUES.set('RESTHR', 'hrmin');
    this.LEGACY_VALUES.set('FP_CALIB', 'fpcalib');

    console.log('UserComponent config:', config);
    if ( config !== null ) {
      this.userId = config.data.userId;
      this.isDialog = true;
    } else {
      this.isDialog = false;
    }
    console.log('constr fertig');
  }

  ngOnInit() {
    // check if we are regular and not embedded and whether a mailchange has been requested
    if ( !this.isDialog && this.route.snapshot.routeConfig?.path === 'finishmailchange' ) {
      if ( this.route.snapshot.queryParamMap.has('hash') ) {
        console.log('finishmailchange');
        const hash = this.route.snapshot.queryParamMap.get('hash');
        this.app.getPublicResource('v1/user/finishmailchange/' + hash)
            .subscribe(
                (data: GenericResponse) => {
                  if ( data.message === 'success' ) {
                    // TODO: success notification
                  } else {
                    this.message = this.app.translateString('personalData_' + data.error);
                  }
                }
            );
        }
    }

    console.log('onInit');

    this.userDataForm = this.formBuilder.group({
      firstname: [],
      lastname: [],
      birthday: [],
      height: [],
      weight: [],
      gender: [],
      hrmin: [],
      hrmax: [],
      fpcalib: [],
      vo2max: [],
    });

    this.changePwForm = this.formBuilder.group({
      curpassword: ['', [Validators.required]],
      newpassword: ['', [Validators.required]],
      newpasswordConfirm: ['', [Validators.required]]
    }, { validator: this.checkPasswordsChange });
    this.changeEmailForm = this.formBuilder.group({
      newemail: ['', [Validators.required, Validators.email]],
      newemailagain: ['', [Validators.required, Validators.email]]
    }, { validator: this.checkEmailChange });

    // query the values of the user
    // if userId is set we are in dialog mode (most likely editing someoneelse's data)
    if ( this.userId !== undefined ) {
      this.app.getResource('v1/user/get/' + this.userId).subscribe(data => this.onDataReceived(data));
      this.valservice.getUserValues(this.userId).then(uservalues => {
        this.userValues = uservalues;
        this.OnUserValueUpdate();
      });
    } else {
      this.app.startLoading();
      this.app.getResource('v1/user/get').subscribe(data => this.onDataReceived(data));
      this.valservice.getMyUserValues().then(uservalues => {
        this.userValues = uservalues;
        this.OnUserValueUpdate();
      });
    }

    // Get all available values
    this.valservice.getAvailableValues().then(avail_values => {
      this.avail_values = avail_values;
      this.avail_valuesd = avail_values.filter(val => !this.LEGACY_VALUES.has(val.name));
      // this.avail_valuesd= avail_values;
      this.OnUserValueUpdate();
    });

    console.log('onInit done');
  }

  private OnUserValueUpdate(): void {
    if ( this.userValues !== undefined && this.avail_values !== undefined ) {
      // console.log({'userValues': this.userValues});
      this.avail_values.forEach(value => {
        // skip legacy values
        if ( this.LEGACY_VALUES.has(value.name) ) {
          // continue;
        } else {
          // console.log(this.userValues);
          const val = this.userValues.has(value.name) ? this.userValues.get(value.name)?.doubleValue : '';
          this.userDataForm.addControl(value.name, this.formBuilder.control(val));
        }
      });
      this.showValues = true;

      this.onAllDataReceived();
    }
  }

  private onDataReceived(data: UserDetails): void {
    this.app.stopLoading();

    this.data = data;
    for(let [attr, value] of Object.entries(this.data)) {
      // console.log(attr);
      if ( attr !== 'id' && attr !== 'email' && attr !== 'birthday' && !attr.endsWith('_time') ) {
        // some values such as language are not part of the form
        if (this.userDataForm.get(attr) !== null ) {
          this.userDataForm.get(attr)?.setValue(value);
        } else {
          console.log(attr, 'not in form');
        }
      }
    };
    const date = new Date(this.data.birthday * 1000);
    this.userDataForm.get('birthday')?.setValue(new Date('' + date));
    // this.userDataForm.clearValidators();

    this.onAllDataReceived();
  }

  private onAllDataReceived(): void {
    if ( this.data !== undefined && this.userValues !== undefined && this.avail_values !== undefined ) {

      this.cd.detectChanges();
      this.app.translateTree('user-component');
    }
  }

  private checkPasswordsChange(group: FormGroup) {
    const pass = group.controls['newpassword'].value;
    const confirmPass = group.controls['newpasswordConfirm'].value;

    return pass === confirmPass ? null : { 'notSame': true };
  }

  private checkEmailChange(group: FormGroup) {
    const email = group.controls['newemail'].value;
    const confirmEmail = group.controls['newemailagain'].value;

    // console.log({email, confirmEmail});

    return email === confirmEmail ? null : { 'notSame': true };
  }

  public changeData(): void {
    // console.log(this.userDataForm);
    if ( this.userDataForm.invalid ) {
      const keys = ['firstname', 'lastname', 'birthday'];
      this.avail_values.forEach(value => {
        if ( !this.LEGACY_VALUES.has(value.name) ) {
          keys.push(value.name);
        }
      });
      keys.forEach(k => {
        if ( this.userDataForm.get(k)?.errors ) {
          console.log(k, 'has errors');
        }
      });

      return;
    }

    // check if user basic data has changed ...
    if ( this.userDataForm.get('firstname')?.touched || this.userDataForm.get('lastname')?.touched ||
           this.userDataForm.get('birthday')?.touched || this.userDataForm.get('gender')?.touched ) {
      if ( this.userDataForm.get('firstname')?.value !== this.data.firstname ||
             this.userDataForm.get('lastname')?.value !== this.data.lastname ||
             this.userDataForm.get('gender')?.value !== this.data.gender ||
             this.userDataForm.get('birthday')?.touched ) {
        this.data.firstname = this.userDataForm.get('firstname')?.value;
        this.data.lastname = this.userDataForm.get('lastname')?.value;
        this.data.gender = this.userDataForm.get('gender')?.value;
        this.data.birthday = this.userDataForm.get('birthday')?.value.getTime() / 1000;

        console.log(this.data);

        if ( this.userId !== undefined ) {
          this.actionPending = true;
          // TODO: update global data
          this.app.putResource('v1/user/update/' + this.userId, this.data).subscribe(res => {
            this.actionPending = false;
            this.userDataForm.get('firstname')?.markAsUntouched();
            this.userDataForm.get('lastname')?.markAsUntouched();
            this.userDataForm.get('birthday')?.markAsUntouched();
          });
        } else {
          this.actionPending = true;
          this.app.putResource('v1/user/update', this.data).subscribe(res => {
            this.actionPending = false;
            this.userDataForm.get('firstname')?.markAsUntouched();
            this.userDataForm.get('lastname')?.markAsUntouched();
            this.userDataForm.get('birthday')?.markAsUntouched();
          });
        }
      }
    }

    // check if values have changed
    const changes: ValueChanges = {
      changes: [],
    };
    this.avail_values.forEach(value => {
      if ( !this.LEGACY_VALUES.has(value.name) && this.userDataForm.get(value.name)?.touched ) {
        if ( !this.userValues.has(value.name) || this.userDataForm.get(value.name)?.value !== this.userValues.get(value.name) ) {
          const change: Change = {name: value.name, value: this.userDataForm.get(value.name)?.value};
          changes.changes.push(change);
        }
      }
    });
    // check legacy values
    // console.log(this.userDataForm);
    /*console.log(this.userDataForm.get('weight').touched, this.userValues.has('WEIGHT'), this.userDataForm.get('weight').value,
      this.userValues.get('WEIGHT'));*/
    this.LEGACY_VALUES.forEach((formv, userv) => {
      // console.log(formv, userv, this.userDataForm.get(formv).touched, this.userDataForm.get(formv).value, this.data[formv]);
      if ( this.userDataForm.get(formv)?.touched &&
        this.userDataForm.get(formv)?.value !== (this.data as {[key:string]:any})[formv] ) {
        const change: Change = {name: userv, value: this.userDataForm.get(formv)?.value};
        changes.changes.push(change);
      }
    });
    /*if( this.userDataForm.get('weight').touched && this.userValues.has('WEIGHT') &&
      this.userDataForm.get('weight').value !== this.userValues.get('WEIGHT') )
    {
      const change: Change= {name: 'WEIGHT', value: this.userDataForm.get('weight').value};
      changes.changes.push(change);
    }
    if( this.userDataForm.get('height').touched && this.userValues.has('HEIGHT') &&
      this.userDataForm.get('height').value !== this.userValues.get('HEIGHT') )
    {
      const change: Change= {name: 'HEIGHT', value: this.userDataForm.get('height').value};
      changes.changes.push(change);
    }*/
    if ( changes.changes.length > 0 ) {
      // send to server
      if ( this.userId !== undefined ) {
        this.actionPending = true;
        const me = this;
        this.valservice.sendChanges(changes, this.userId).subscribe(res => this.OnValuesChangeResponse(me, res, changes));
      } else {
        this.actionPending = true;
        const me = this;
        this.valservice.sendChangesForMe(changes).subscribe(res => this.OnValuesChangeResponse(me, res, changes));
      }
    }
  }

  private OnValuesChangeResponse(that: UserComponent, res: GenericResponse, changes: ValueChanges): void {
    that.actionPending = false;
    if ( res.message === 'OK' ) {
      changes.changes.forEach((change: Change) => {
        // again, also here we have to check for legacy values
        if ( that.LEGACY_VALUES.has(change.name) ) {
          that.userDataForm.get(that.LEGACY_VALUES.get(change.name) as string)?.markAsUntouched();
        } else {
          that.userDataForm.get(change.name)?.markAsUntouched();
        }
      });
    } else {
      // TODO: translate
      alert('Chaning values failed: ' + res.message + '(' + res.error + ')');
    }
    // that.changes= undefined;
  }

  public changePassword(): void {
    this.submitted = true;

    if ( this.changePwForm.invalid ) {
      // console.log('form invalid', this.changePwForm, this.changePwForm.errors.notSame);
      return;
    }

    // this.actionPending= true;
    const pw = this.changePwForm.value['curpassword'];
    const npw = this.changePwForm.value['newpassword'];

    const dd = `password=${npw}&oldpassword=${pw}`;

    this.app.getResourcePostForm('v1/user/updatePassword', dd).subscribe(data => alert('Done'));

    // this._service.register(this.registerForm.value, this);
  }

  public changeEmail(): void {
    this.submittedE = true;

    if ( this.changeEmailForm.invalid ) {
      // console.log('form invalid', this.changePwForm, this.changePwForm.errors.notSame);
      return;
    }

    this.actionPending = true;
    const pw = this.data.email;
    const npw = this.changeEmailForm.value['newemailagain'];

    const dd = `email=${npw}&oldemail=${pw}`;

    this.app.getResourcePostForm('v1/user/changeEmail', dd).subscribe((data: GenericResponse) => {
      if ( data.message === 'success' ) {
        alert('Done');
        this.submittedE = false;
        this.actionPending = false;
      } else {
        // alert(data.error);
        this.changeEmailFormError = this.app.translateString('personalData_' + data.error);
        this.submittedE = false;
        this.actionPending = false;
      }
    });
  }

  public getUserValue(name: string): UserValue | undefined {
    if ( this.userValues.has(name) ) {
      return this.userValues.get(name);
    } else {
      return undefined;
    }
  }

  public onValueDetails(id: number): void {
    if ( this.isDialog ) {
      // TODO: this works only if no one changed the url of the user module
      window.location.href = this.app.baseURL + '/user/values/' + (this.userId !== undefined ? this.userId : '') + '/' + id;
    } else {
      if ( this.userId !== undefined ) {
        this.router.navigate(['./values', this.userId, id], { relativeTo: this.route.parent });
      } else {
        this.router.navigate(['./values', id], { relativeTo: this.route.parent });
      }
    }
  }
}
