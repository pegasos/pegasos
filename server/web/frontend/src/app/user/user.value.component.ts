import { Component, OnInit, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ValuesService, ValueUpdate, ValueCreate } from './../services/values.service';

import { AppService } from './../services/app.service';

import { GenericResponse, UserValueInfo, Change, ValueChanges } from './../shared/sample';
import { Table } from 'primeng/table';
import { of, Observable } from 'rxjs';

export interface UserValue {
  id: number;
  value: UserValueInfo | null;
  user_id: number;
  date_start: Date | null;
  date_end: Date | null;
  doubleValue: number | null;
}

@Component({
  selector: 'app-user-value',
  templateUrl: './user.value.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserValueComponent implements OnInit {

  private userId!: number;

  private valueId!: number;

  /**
   * Whether the user has clicked on an action and its currently pending
   */
  public actionPending = false;

  /**
   * Information about the value we are displaying / editing
   */
  public valueInfo!: UserValueInfo;

  public history!: UserValue[];

  public dateStart: Map<number, Date> = new Map<number, Date>();

  public dateEnd: Map<number, Date | null> = new Map<number, Date>();

  public historyClone: {[s: number]: UserValue} = {};

  public changedValues: {[s: number]: boolean} = {};

  @ViewChildren(Table) private table!: QueryList<Table>;

  private newValueId = -1;

  constructor(private app: AppService,
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private valservice: ValuesService) {

    this.route.params.subscribe( params => {
      this.valueId = params['vid'];
      this.userId = params['uid'];
    });
  }

  ngOnInit() {
    // this.app.setTitle('Pegasos - Loading');
    this.valservice.getInfo(this.valueId).then(value => {
      this.valueInfo = value;
      this.OnUserValueUpdate();
    });

    if ( this.userId !== undefined ) {
      this.valservice.getHistory(this.userId, this.valueId).then(values => {
        this.history = values;
        this.OnUserValueUpdate();
      });
    } else {
      this.valservice.getMyHistory(this.valueId).then(values => {
        this.history = values;
        this.OnUserValueUpdate();
      });
    }

    this.app.startLoading();
  }

  private OnUserValueUpdate(): void {
    if ( this.valueInfo !== undefined && this.history !== undefined ) {
      this.history.forEach(value => {
        this.dateStart.set(value.id, new Date('' + value.date_start));
        this.dateEnd.set(value.id, value.date_end !== null ? new Date('' + value.date_end) : null);
      });
      // console.log(this.dateStart);

      this.app.stopLoading();

      this.app.setTitle('Change Values for ' + this.valueInfo.name);

      // Now that we have the values and they are on display they need to be translaged
      this.cd.detectChanges();
      this.app.translateContent();
    }
  }

  public onStartEdit(idx: number, value: UserValue): void {
    // console.log(value);

    this.table.first.initRowEdit(value);

    // unpack the value and thus store a copy of its current value
    this.historyClone[value.id] = {...value};
    this.changedValues[value.id] = true;

    const sf = document.getElementById('start-fixed-' + value.id);
    if (sf) sf.hidden = true;
    const si = document.getElementById('start-input-' + value.id);
    if (si) si.hidden = false;
    const ef = document.getElementById('end-fixed-' + value.id);
    if (ef) ef.hidden = true;
    const ei = document.getElementById('end-input-' + value.id);
    if (ei) ei.hidden = false;
  }

  public onAbortEdit(idx: number, value: UserValue): void {
    this.history[idx] = this.historyClone[value.id];
    delete this.historyClone[value.id];
    delete this.changedValues[value.id];
    this.dateStart.set(value.id, new Date('' + this.history[idx].date_start));
    this.dateEnd.set(value.id, new Date('' + this.history[idx].date_end));

    this.hideDateEdits(value);
  }

  private hideDateEdits(value: UserValue): void {
    const sf = document.getElementById('start-fixed-' + value.id);
    if (sf) sf.hidden = false;
    const si = document.getElementById('start-input-' + value.id);
    if (si) si.hidden = true;
    const ef = document.getElementById('end-fixed-' + value.id);
    if (ef) ef.hidden = false;
    const ei = document.getElementById('end-input-' + value.id);
    if (ei) ei.hidden = true;
  }

  public onStartClear(value: UserValue): void {
    console.log('onStartClear', {value}, this.dateStart.get(value.id));
    // this.dateStart.set(value.id, this.historyClone[value.id].date_start);
  }

  public onStartChanged(value: UserValue, newval: Date): void {
    console.log('onStartChanged', {value, newval});
    if ( newval !== null ) {
      this.dateStart.set(value.id, newval);
      value.date_start = newval;
    }
  }

  public onEndClear(value: UserValue): void {
    value.date_end = null;
    this.dateEnd.set(value.id, null);
    console.log('onEndClear', {value}, this.dateEnd.get(value.id));
  }

  public onEndChanged(value: UserValue, newval: Date): void {
    console.log('onEndChanged', {value, newval});
    if ( newval !== null ) {
      this.dateEnd.set(value.id, newval);
      value.date_end = newval;
    }
  }

  public onValueDelete(idx: number, value: UserValue): void {
    // if value is a temporary element we can just remove it from the list
    if ( +value.id < 0 ) {
      delete this.changedValues[value.id];
      this.history.splice(idx, 1);
    } else {
      if ( confirm(this.app.translateString('value_deleteValueConfirm')) ) {
        this.actionPending = true;
        this.valservice.deleteValue(value as any).subscribe(
          (res: GenericResponse) => {
            if ( res.message === 'OK' ) {
              delete this.changedValues[value.id];
              this.history.splice(idx, 1);
            } else {
              alert(this.app.translateString('value_deleteValueFailed'));
            }
            this.actionPending = false;
          },
          () => this.actionPending = false);
      }
    }
  }

  public addEntry(): void {
    let added = false;

    if ( this.history.length > 0 ) {
      if ( this.history[this.history.length - 1].date_end === null ) {
        const lastEl = this.history[this.history.length - 1];

        // end last element
        const value: UserValue = {
          id: (this.newValueId--),
          date_start: new Date(),
          date_end: null,
          doubleValue: null,
          value: null,
          user_id: this.userId
        };

        // last row: change date to current time and put into edit mode
        this.onStartEdit(this.history.length - 1, lastEl);

        lastEl.date_end = new Date(new Date('' + value.date_start).getTime() - 1);
        this.dateEnd.set(lastEl.id, new Date('' + lastEl.date_end));

        // add new row to data
        this.changedValues[value.id] = true;
        this.dateStart.set(value.id, new Date('' + value.date_start));
        this.history.push(value);
        // this.table.first.initRowEdit(value);
        this.cd.detectChanges();
        this.onStartEdit(this.history.length - 1, value);

        added = true;
      }
    }

    if ( !added ) {
      // end last element
      const value: UserValue = {
        id: (this.newValueId--),
        date_start: new Date(),
        date_end: null,
        doubleValue: null,
        value: null,
        user_id: this.userId
      };

      // add new row to data
      this.changedValues[value.id] = true;
      this.dateStart.set(value.id, new Date('' + value.date_start));
      this.history.push(value);
      this.cd.detectChanges();
      this.onStartEdit(this.history.length - 1, value);
    }
  }

  public saveChanges(): void {
    console.log(this.changedValues);
    const changes: UserValue[] = [];
    const news: UserValue[] = [];

    for (const key in this.changedValues) {
      if (this.changedValues.hasOwnProperty(key)) {
        const value = this.history.filter(val => val.id === +key)[0];
        if ( value.id > 0 ) {
          const old = this.historyClone[key];
          if ( this.valuesDiffer(value, old) ) {
            changes.push(value);
          }
        } else {
          news.push(value);
        }
      }
    }

    console.log({changes, news});

    if ( changes.length > 0 && news.length > 0 ) {
      this.sendChanges(changes).subscribe(
        () => this.sendCreates(news).subscribe(
          (createdValues) => this.changesDone(createdValues),
          (err) => alert(err)
        )
      );
    } else if ( changes.length > 0 ) {
      this.sendChanges(changes).subscribe(
        () => {
          this.changesDone();
          this.actionPending = false; },
        (err) => alert(err)
      );
    } else if ( news.length > 0 ) {
      this.sendCreates(news).subscribe(
        (createdValues) => this.changesDone(createdValues),
        (err) => alert(err)
      );
    }
  }

  private sendChanges(changes: UserValue[]): Observable<GenericResponse> {
    if ( this.userId !== undefined ) {
      return this.valservice.sendUpdates(this.valueId, this.userId, changes.map(value => {
        const change: ValueUpdate = {
          id: value.id,
          date_start: value.date_start,
          date_end: value.date_end,
          value: value.doubleValue,
        };
        return change;
      }));
    } else {
      return this.valservice.sendUpdatesForMe(this.valueId, changes.map(value => {
        const change: ValueUpdate = {
          id: value.id,
          date_start: value.date_start,
          date_end: value.date_end,
          value: value.doubleValue,
        };
        return change;
      }));
    }
  }

  private sendCreates(creates: UserValue[]): Observable<UserValue[]> {
    if ( this.userId !== undefined ) {
      return this.valservice.sendCreate(this.valueId, this.userId, creates.map(value => {
        const n: ValueCreate = {
          date_start: value.date_start,
          date_end: value.date_end !== null ? value.date_end : null,
          value: value.doubleValue
        };
        return n;
      }));
    } else {
      return this.valservice.sendCreateForMe(this.valueId, creates.map(value => {
        const n: ValueCreate = {
          date_start: value.date_start,
          date_end: value.date_end !== null ? value.date_end : null,
          value: value.doubleValue
        };
        return n;
      }));
    }
  }

  private valuesDiffer(a: UserValue, b: UserValue): boolean {
    // console.log('different?', a, b);
    if ( a.doubleValue !== b.doubleValue ) {
      if ( a.value !== null && b.value !== null && +a.value !== + b.value ) {
        // console.log('value different');
        return true;
      }
    }

    if ( new Date('' + a.date_start).getTime() !== new Date('' + b.date_start).getTime() ) {
      // console.log('start different');
      return true;
    }

    if ( a.date_end !== null && b.date_end === null ||
         a.date_end === null && b.date_end !== null ) {
          // console.log('e different');
      return true;
    }

    if ( a.date_end !== null && b.date_end !== null ) {
      if ( new Date('' + a.date_end).getTime() !== new Date('' + b.date_end).getTime() ) {
        // console.log('end different');
        return true;
      }
    }

    return false;
  }

  private changesDone(createdValues?: UserValue[]): void {
    let idx = 0;
    for (const key in this.changedValues) {
      if (this.changedValues.hasOwnProperty(key)) {
        if ( +key < 0 ) {
          const value = this.history.filter(val => val.id === +key)[0];
          this.table.first.cancelRowEdit(value);
          delete this.changedValues[key];
          if( createdValues )
            this.history[this.history.indexOf(value)] = createdValues[idx];
          idx += 1;
        } else {
          const value = this.history.filter(val => val.id === +key)[0];
          this.table.first.cancelRowEdit(value);
          delete this.changedValues[key];
          this.hideDateEdits(value);
        }
      }
    }
  }
}
