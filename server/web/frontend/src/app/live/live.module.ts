import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';

import { ActionComponentsModule } from './../actioncomponents/action.components.module';

import { ActiveUserComponent } from './activeuser.component';
import { LiveComponent } from './live.component';

const routes: Routes = [
  { path: '', component: LiveComponent },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        TableModule, CalendarModule,
        ActionComponentsModule,
    ],
    declarations: [
        LiveComponent,
        ActiveUserComponent,
    ],
    exports: [RouterModule]
})
export class LiveModule {

}
