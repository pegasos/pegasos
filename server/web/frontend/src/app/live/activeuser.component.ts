import { UserDetails } from './../shared/sample';
import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { LiveComponent } from './live.component';

@Component({
  template: `
<div class="member_line_wrap" *ngIf="userdata && user">
  <div class="member_line radius" title="Charts von {{user.firstname}} {{user.lastname}} anzeigen">
      <a class="chart_show" [style.color]="color">&nbsp;&nbsp;{{user.firstname}} {{user.lastname}}</a>
      <div class="aktuell_member_details">
        <span class="aktuell_member_detailinfo" *ngIf="userdata.hr"><img src="assets/img/pulse.png" title="Puls" class="pulse">{{userdata.hr}} 1/min</span>
        <span class="aktuell_member_detailinfo" *ngIf="userdata.speed_mm_s"><img src="assets/img/speed.png" title="Geschwindigkeit" class="speed" />{{((userdata.speed_mm_s / 1000) * 3.6) | number: '1.2'}} km/h</span>
        <span class="aktuell_member_detailinfo" *ngIf="userdata.distance_m"><img src="assets/img/distance.png" title="Distance" class="distance">{{userdata.distance_m}} m</span>
        <span class="aktuell_member_detailinfo" *ngIf="userdata.strides"><img src="assets/img/strides" title="Strides" class="strides">{{userdata.strides}}</span>
        <span class="aktuell_member_detailinfo"><img src="assets/img/time.png" title="Zeit" class="time">{{userdata.last_update - userdata.starttime | date: 'HH:mm:ss' : 'GMT-0'}}</span></div>
  </div>
  <img src="assets/img/crosshair.png" title="Person verfolgen" data-memberid="2" class="crosshair" (click)="onFollow()">
</div>`,
  styleUrls: ['./live.component.scss']
})
export class ActiveUserComponent implements OnInit {
  public userdata: any;
  private userid!: number;
  color: any;
  public user!: UserDetails;
  private ref!: LiveComponent;

  constructor(private usrService: UserService) { }

  ngOnInit() {
  }

  public setLiveComponent(ref: LiveComponent) {
    this.ref = ref;
  }

  public setUserId(userid: number) {
    this.userid = userid;
    this.usrService.getUser(userid).then(usr => {
      // console.log('User received', userid, usr);
      this.user = usr;
    });
  }

  public getUserId(): number {
    return this.userid;
  }

  public setData(userdata: any): void {
    // console.log('setting userdata for', this.userid, userdata);
    this.userdata = userdata;
  }

  public onFollow(): void {
    this.ref.setFollowing(this.userid);
  }
}
