import { Component, OnInit, AfterViewInit, ViewChildren, ChangeDetectorRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';

import { ActiveUserComponent } from './activeuser.component';
import { AppService } from './../services/app.service';
import { HostDirective } from '../shared/host.directive';

import OlMap from 'ol/Map';
import OlView from 'ol/View';
import Point from 'ol/geom/Point';
import OSM from 'ol/source/OSM';
import { Vector as VectorSource } from 'ol/source';
import { Vector as VectorLayer } from 'ol/layer';
import TileLayer from 'ol/layer/Tile';
import { fromLonLat } from 'ol/proj';
import OlStyle from 'ol/style/Style';
import OlIcon from 'ol/style/Icon';
import TileSource from 'ol/source/Tile';
import Feature from 'ol/Feature';
import Geometry from 'ol/geom/Geometry';
import { Extent } from 'ol/extent';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit, AfterViewInit, OnDestroy {

  /**
   * Hosts for the active users
   */
  @ViewChildren(HostDirective) private userHosts!: any;

  public activeUsers: Set<number>;

  public activeUsersCount: number;

  public userData: Map<number, any>;

  components!: ActiveUserComponent[];

  private colors = ['b79200', 'b80000', '2a4b94', '71A164', '997A37', '43879C', '88bf75',
  '2eb990', 'd03d44', '88a100', '909090', 'A96DE7', 'd2795b', 'E77B8F', 'A69A7F', 'AC19B4',
   'A69A7F', 'DC1D46', '26236A', '9F7E90', '678386', '8CEF54', '2AEB1B'];

  private map!: OlMap;
  private source!: OSM;
  private layer!: TileLayer<TileSource>;
  private layerMarker!: VectorLayer<VectorSource<Geometry>>;
  private markerSource!: VectorSource<Geometry>;

  /**
   * Positions of the users on the map
   */
  private positions: Map<number, Feature<Geometry>>;

  /**
   * Color for a person
   */
  private personColor: Map<number, string>;

  private following: number | undefined;

  /**
   * Helper field for color assignment
   */
  private idx = 0;

  subscr!: Subscription;

  constructor(private app: AppService,
    private cd: ChangeDetectorRef,
    private componentFactoryResolver: ComponentFactoryResolver) {
      this.activeUsers = new Set<number>();
      this.activeUsersCount = 0;
      this.userData = new Map<number, any>();
      this.positions = new Map<number, Feature<Geometry>>();
      this.personColor = new Map<number, string>();
    }

  ngOnInit() {
    this.app.getResource('v1/live/get').subscribe(res => this.onLiveData(res));

    const fetcher = interval(5000);
    this.subscr = fetcher.subscribe(a => {
      this.app.getResource('v1/live/get').subscribe(res => this.onLiveData(res));
    });
    this.app.setTitle(this.app.translateString('mainMenu_currentEvents'));
    // this.app.translateTree('aktuell_left');
  }

  ngOnDestroy(): void {
    this.subscr.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.source = new OSM();

    this.layer = new TileLayer({
      source: this.source
    });

    this.map = new OlMap({
      target: 'svgid',
      layers: [this.layer],
      view: new OlView({
        zoom: 14
      })
    });

    this.markerSource = new VectorSource;
    this.layerMarker = new VectorLayer({
      source: this.markerSource,
    });
    this.map.addLayer(this.layerMarker);

    // this.map.getView().fit(this.layerMarker.getSource().getExtent(), {size: this.map.getSize(), maxZoom: 8});

    this.app.translateContent();
  }

  private onLiveData(res: any[]): void {
    const activeUsers: Set<number> = new Set<number>(res.map(user => user.user_id));

    const wasEmpty = this.activeUsers.size === 0;
    this.setActiveUsersComponents(activeUsers);

    let followPos;

    res.forEach(data => {
      if ( data.user_id === this.following ) {
        followPos = fromLonLat([data.last_gps_lon, data.last_gps_lat]);
      }
      // this.positions.get(data.user_id).getGeometry().setCoordinates(fromLonLat([data.last_gps_lon, data.last_gps_lat]));
      // const p: Point = this.positions.get(data.user_id).getGeometry();
      // p.setCoordinates(fromLonLat([data.last_gps_lon, data.last_gps_lat]));
      this.positions.get(data.user_id)?.set('geometry', new Point(fromLonLat([data.last_gps_lon, data.last_gps_lat])));
      this.userData.set(data.user_id, data);
    });

    this.components.forEach(comp => comp.setData(this.userData.get(comp.getUserId())));

    if ( wasEmpty && this.activeUsers.size > 0 ) {
      const ext = this.layerMarker.getSource()?.getExtent() as Extent;
      this.map.getView().fit(ext, {size: this.map.getSize(), maxZoom: 14});
    }

    if ( this.following && followPos ) {
      const size = this.map.getSize();
      if (size)
        this.map.getView().centerOn(followPos, size, [200, 200]);
    }

    this.cd.detectChanges();
  }

  /**
   * Add the components for the active users
   */
  private setActiveUsersComponents(activeUsers: Set<number>): void {
    // console.log('Add new users', activeUsers);
    let changed = false;

    /*
    // now check whether there are other users and we have to update our view
    if( activeUsers.size !== this.activeUsers.size )
    {
      // console.log('Size mismatch');
      this.setActiveUsersComponents(activeUsers);
    }
    else
    {
      // check if all new elements are contained in old
      const eq= Array.from(activeUsers).every(user => {
        if( !this.activeUsers.has(user) ) {
          // console.log('We are missing', user, this.activeUsers);
          return false;
        }
        return true;
      });
      if( !eq )
      {
        // console.log('Array missmatch');
        this.setActiveUsersComponents(activeUsers);
      }
    }
    */

    // find users which are new and add them to the map
    activeUsers.forEach(userId => {
      if ( !this.activeUsers.has(userId) ) {
        changed = true;
        this.assignColor(userId);
        this.addMarker(userId);
      }
    });
    // find users which have been on the map and are no longer active
    this.activeUsers.forEach(userId => {
      if ( !activeUsers.has(userId) ) {
        changed = true;
        this.removeMarker(userId);
        if ( userId === this.following ) {
          this.following = undefined;
        }
      }
    });

    this.activeUsers = activeUsers;
    this.cd.detectChanges();
    console.log('changed', changed, 'activeUsers', this.activeUsers, this.userHosts);

    this.renderUsers();
  }

  private renderUsers(): void {
    this.components = [];
    const aui = this.activeUsers.keys();
    this.userHosts.forEach((userHost: any, index: number) => {
      const containerRef = userHost.viewContainerRef;
      containerRef.clear();

      const newComponent = containerRef.createComponent(
        this.componentFactoryResolver.resolveComponentFactory(ActiveUserComponent));
      const inst: ActiveUserComponent = (<ActiveUserComponent> newComponent.instance);
      inst.setUserId(aui.next().value);
      inst.setLiveComponent(this);
      inst.color = this.personColor.get(inst.getUserId());
      this.components.push(inst);

      // console.log('added component', newComponent);
    });
    this.cd.detectChanges();

    /*if( this.activeUsers.size === 1 )
    {
      this.following= this.activeUsers.values()[0];
    }*/
  }

  private addMarker(userId: number): void {
    console.log('add marker', userId);
    const iconStyle = new OlStyle({
      image: new OlIcon({
        /*anchor: [0.5, 100],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 1.0,*/
        color: this.personColor.get(userId),
        src: 'assets/img/dot.png'
      })
    });

    const marker = new Feature({
      id: userId,
      geometry: new Point(
        fromLonLat([16.270168567539717, 48.045985599720815])
      ),
    });
    marker.setStyle(iconStyle);

    this.markerSource.addFeature(marker);

    this.positions.set(userId, marker);
  }

  private removeMarker(userId: number): void {
    this.markerSource.removeFeature(this.positions.get(userId) as Feature<Geometry>);
    this.positions.delete(userId);
  }

  public assignColor(userId: number) {
    if ( !this.personColor.has(userId) ) {
      this.personColor.set(userId, '#' + this.colors[this.idx++ % this.colors.length]);
    }
  }

  public setFollowing(userId: number) {
    // console.log('Follow', userId);
    this.following = userId;
  }
}
