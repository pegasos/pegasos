export interface PersonSelectEntry {
  id: string;
  itemName: string;
  item_type: string;
}

export class Person {
  public id!: number;
  public name!: string;

  public flatten(): PersonSelectEntry {
    return {id: 'p_' + this.id, itemName: this.name, item_type: 'person'};
  }
}

export class Group {
  public groupid!: number;
  public name!: string;
  public members: Array<Person> = [];

  public flatten(): PersonSelectEntry[] {
    // return [];
    return [{id: 'g_' + this.groupid, itemName: this.name, item_type: 'group'}].concat(this.members.map(g => g.flatten()));
  }

  public entry(): PersonSelectEntry {
    return {id: 'g_' + this.groupid, itemName: this.name, item_type: 'group'};
  }
}

export class Team {
  public teamid!: number;
  public name!: string;
  public groups: Array<Group> = [];

  public flatten(): PersonSelectEntry[] {
    return [{id: 't_' + this.teamid, itemName: this.name, item_type: 'team'}].concat(
      this.groups.map(g => g.flatten()).
        reduce((r, f) => r.concat(f), [])
      );
  }

  public entry(): PersonSelectEntry {
    return {id: 'g_' + this.teamid, itemName: this.name, item_type: 'team'};
  }
}
