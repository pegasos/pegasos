import { TufteColorBlind } from './colors';

export type ReportType = 'Simple' | 'SessionbasedLines' | 'Multiline' | 'Bars' | 'Curve';

export class DataTransformation {
  name: string;
  fun: ((a: number) => number) | undefined;
  constructor(name: string, fun: ((a: number) => number) | undefined) {
    this.name = name;
    this.fun = fun;
  }
}

export const HumanPercent = new DataTransformation('Human Percent', (a) => Math.round(a * 10000) / 100);
export const DataTransformations: Array<DataTransformation> = [
  new DataTransformation('', undefined),
  new DataTransformation('log 1digit', (a) => Math.round(Math.log10(a) * 10) / 10),
  new DataTransformation('log 2digits', (a) => Math.round(Math.log10(a) * 100) / 100),
  new DataTransformation('ln 1digit', (a) => Math.round(Math.log(a) * 10) / 10),
  new DataTransformation('ln 2digits', (a) => Math.round(Math.log(a) * 100) / 100),
  HumanPercent,
];

export type AxisTimeScale = 'None' | 'Day' | 'Week' | 'Month' /*| 'Year'*/;

export type VarType = 'string' | 'number' | 'list' | 'DataTransformation' | 'AxisTimeScale';

export class ConfigVar {
  name: string;
  type: VarType;

  constructor (name: string, type: VarType) {
    this.name = name;
    this.type = type;
  }
}

const ReportConfigVars = [new ConfigVar('yaxislabel', 'string')];

export class ReportConfig {
  /**
   * Action to be performed on server
   */
  name: string;
  id: string;

  /**
   * If true data should be fetched from the server. Currently its not intended to
   * modify this as part of any configuration but only as a hack in order to push
   * data to a report.
   */
  fetchData = true;

  start!: string | undefined;
  end!: string | undefined;

  persons!: string;

  toolTip!: string;
  // tooltip_background= 'white';
  // tooltip_fontsize= '10px';

  colors = TufteColorBlind;

  seriesNameLookup: Map<string, string> = new Map<string, string>();

  yaxislabel: string = '';

  constructor(name: string) {
    this.name = name;
    this.id = 'rep';
  }

  public exposedVars: ConfigVar[] = [...ReportConfigVars];
}

const ReportWithLinesConfigVars = [...ReportConfigVars,
  new ConfigVar('axis_operation', 'DataTransformation'),
  new ConfigVar('y1min', 'number'), new ConfigVar('y1max', 'number'),
  new ConfigVar('secondaryseries', 'list'),
  new ConfigVar('secondary_yaxsislabel', 'string'),
  new ConfigVar('secondary_axis_operation', 'DataTransformation'),
  new ConfigVar('y2min', 'number'), new ConfigVar('y2max', 'number')];

export class ReportWithLinesConfig extends ReportConfig {
  /**
   * Offset [px] between y-axis and first data point on x-axis
   */
  xOffset = 15;

  axis_operation: ((a: number) => number) | undefined = undefined;
  // axis_operation: any = undefined;

  secondaryseries: string[] = [];

  patterns = ['1, 0', '3, 3', '2, 1.75', '1, 2'];

  secondary_yaxsislabel: string | undefined = undefined;

  secondary_axis_operation: ((a: number) => number) | undefined = undefined;

  y1min!: number;
  y1max!: number;
  y2min!: number;
  y2max!: number;
  yaxisInt!: boolean;

  public override exposedVars: ConfigVar[] = [...ReportWithLinesConfigVars];
}

export class MultiLineReportConfig extends ReportWithLinesConfig {
  // '`${personName}<BR/>${new Date(d.x)} ${series.name} ${d.y}`';
  // override toolTip = '`${personName}: ${series.params[i]}: ${series.values[i]}`';
  override toolTip = '`${personName}<BR/>${new Date(value.x)} ${series.name.split("#")[1]} ${value.y}`';
}

export class SessionbasedLineReportConfig extends ReportWithLinesConfig {
  override toolTip = '`${personName}: ${series.name}: ${value.value}`';
}

export class CurvedLineReportConfig extends ReportWithLinesConfig {
  xmin: number = Number.MAX_VALUE;
  xmax: number = Number.MIN_VALUE;

  override y1min: number = Number.MAX_VALUE;

  /**
   * Whether the domain / ticks / positions on x axis should be selected automatically
   */
  auto_stops = true;

  /**
   * Positions on x asis. Only considered if auto_stops = false.
   */
  stops: Array<number> | undefined = undefined;

  /**
   * Default size for points
   */
  radius = 3;
  lineWidth = 1.5;

  override toolTip = '';
}

export type AxisType = 'Number' | 'Time';

const BarReportConfigVars = [...ReportConfigVars,
  new ConfigVar('axis_operation', 'DataTransformation'),
  new ConfigVar('xAxisScale', 'AxisTimeScale'),
];

export class BarsReportConfig extends ReportConfig {
  yaxis_type: AxisType = 'Number';

  xAxisScale: AxisTimeScale = 'None';

  override toolTip = '`${personName}: ${series.params[i]}: ${series.values[i].toFixed()}`';

  patterns = ['', 'url(#mask-stripe1)', 'url(#mask-stripe2)', 'url(#mask-stripe3)', 'url(#mask-stripe4)', 'url(#mask-stripe5)',
    'url(#mask-stripe6)'];

  /**
   * Desired with of the bars. If not enough space is availabe this will be scaled down.
   * If more space is available at most this width will be used
   */
  desiredBarWith = 50;

  /**
   * When true the series will appear in a sorted order
   */
  sortseries = true;

  axis_operation = (function (value: number) { return value; });

  public override exposedVars: ConfigVar[] = [...BarReportConfigVars];
}

export class ReportInfo {
  public translatedName!: string;

  constructor(
    /**
     * Action / name in url
     */
    public action: string,
    /**
     * Name which is displayed in the select
     */
    public name: string,
    public type: ReportType,
    public defaultWeeks: number,
    public config: ReportConfig) { }
}
