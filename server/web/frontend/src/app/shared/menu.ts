export class Menu {
  /**
   * action aka route to be performed by this menu
   */
  public action: string;
  /**
   * Name to be displayed in the menu (translateable string)
   */
  public name: string;
  /**
   * Whether or not this menu should be accessible for users currently not logged in
   */
  public isPublic: boolean;
  public requiredroles: string[];

  public sub: Menu[] | undefined;

  constructor(
      /**
       * action aka route to be performed by this menu
       */
      action: string,
      /**
      * Name to be displayed in the menu (translateable string)
   *  */
      name: string,
      isPublic?: boolean,
      requiredroles?: string[],
      sub?: Menu[]) {

    this.action = action;
    this.name = name;
    this.sub = sub;

    if ( isPublic === undefined ) {
      this.isPublic = true;
    } else {
      this.isPublic = isPublic;
    }

    if ( requiredroles !== undefined ) {
      this.requiredroles = requiredroles;
    } else {
      this.requiredroles = [];
    }
  }
}
