import { Person } from './person';

export interface PersonResolver {
  getPerson(id: number): Person | undefined;
}
