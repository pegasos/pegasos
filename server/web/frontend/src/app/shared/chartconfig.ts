import { Type, ChartInfo } from './sample';

let chartId = 0;

export class ChartConfig {
  displayname: string;
  type: Type;
  svgid: string;
  sessionId!: number;
  width = 800;
  height = 200;

  constructor(info: ChartInfo) {
    this.displayname = info.name;
    this.svgid = 'svgchart' + (chartId++);
    this.type = info.type;
  }
}

export class LineChartConfig extends ChartConfig {
  linecolor: string = 'steelblue';
  yaxislabel!: string;
  filterymin!: number;
  filterymax!: number;
  /**
   * Scaling of y axis. If undefined no scaling will be applied otherwise scaley will be applied as linear factor
   */
  scaley!: number;
  linetype = 'Line';

  kernel! :string;

  y1min!: number;
  y1max!: number;
}

export class MultiLineChartConfig extends LineChartConfig {
  linecolors = ['steelblue', 'red', 'black', 'green', 'orange'];
}

export class BarsChartConfig extends ChartConfig {
    colors = ['steelblue', 'red', 'black', 'green', 'orange'];
    yaxislabel!: string;
    filterymin!: number;
    filterymax!: number;
    axisType: 'Time' | 'Percent' | 'Number' = 'Time';

    tooltip_background= 'white';
    tooltip_fontsize= '10px';
    tooltip_seriespre: string= '';
    tooltip_seriespost: string= '';
    tooltip_itempre: string= '';
    tooltip_itempost: string= '';
}
