import { ReportInfo, ReportConfig, MultiLineReportConfig, BarsReportConfig, CurvedLineReportConfig, SessionbasedLineReportConfig } from './reportconfig';
import * as transform from './datatransformation';

const c0 = new SessionbasedLineReportConfig('lsct');
const c1 = new SessionbasedLineReportConfig('lscthrr');
const c2 = new SessionbasedLineReportConfig('lsctrpe');
const c3 = new MultiLineReportConfig('test');
const c4= new BarsReportConfig('energyweek');
const c5= new BarsReportConfig('testt');
const c6= new CurvedLineReportConfig('testm');
const c8 = new BarsReportConfig('timeinz');
const c9 = new BarsReportConfig('einz');
const c10 = new MultiLineReportConfig('pmc');

export const REPORTS: ReportInfo[] = [new ReportInfo('lsct', 'lsct_power', 'SessionbasedLines', 16, c0),
    new ReportInfo('lscthrr', 'lscthrr', 'SessionbasedLines', 16, c1),
    new ReportInfo('lsctrpe', 'lsctrpe', 'SessionbasedLines', 16, c2),
    new ReportInfo('test', 'test', 'Multiline', 52, c3),
    new ReportInfo('energy', 'energy', 'Bars', 4, c4),
    new ReportInfo('tinz', 'tinz', 'Bars', 4, c5),
    new ReportInfo('powerp', 'powerp', 'Curve', 52, c6),
    new ReportInfo('timeinz', 'timeinz', 'Bars', 4, c8),
    new ReportInfo('einz', 'einz', 'Bars', 4, c9),
    new ReportInfo('pmc', 'pmc', 'Multiline', 52, c10),
];

// c0.toolTip = '`${personName} ${series.name}: ${d.value}`';
c0.colors = ['steelblue', 'red', 'black', 'green', 'orange'];
c0.yaxislabel = 'lsct_stage_power';

// c3.toolTip = '`${personName}<BR/>${new Date(d.x)} ${series.name} ${d.y}`';
c3.secondaryseries = ['BODYFAT'];
c3.secondary_yaxsislabel = '[%]';
c3.secondary_axis_operation = transform.HumanPercent.fun;

c4.toolTip = '`${series.name}: ${series.values[i].toFixed()}kJ`';
c4.toolTip = '`${personName}: ${series.date} ${series.params[i]}: ${series.values[i].toFixed()}`';
c4.yaxislabel = 'Energy [kJ]';

c5.yaxis_type= 'Time';
c5.toolTip = '`${personName}: ${series.params[i]}: ${myhmsformat(series.values[i])}`';
c5.yaxislabel= 'Time in Zone [h:m:s]';
c5.seriesNameLookup = new Map([
    ['TINZ-1', 'Time in Zone 1'],
    ['TINZ-2', 'Time in Zone 2'],
    ['TINZ-3', 'Time in Zone 3'],
    ['TINZ-4', 'Time in Zone 4'],
    ['TINZ-5', 'Time in Zone 5'],
    ['TINZ-6', 'Time in Zone 6'],
    ['TINZ-7', 'Time in Zone 7']]);
c6.toolTip = '`${personName}: ${d.x}s ${Math.round(d.y)}W`';
c6.xmin= 1;
c8.yaxis_type = 'Time';
c8.toolTip = '`${series.person}: ${series.params[i]}: ${myhmsformat(series.values[i])}`';
c8.yaxislabel = 'Time in Zone [h:m:s]';
c8.seriesNameLookup = new Map([
    ['TIME_IN_POWER_ZONE-1', 'Time in Zone 1'],
    ['TIME_IN_POWER_ZONE-2', 'Time in Zone 2'],
    ['TIME_IN_POWER_ZONE-3', 'Time in Zone 3'],
    ['TIME_IN_POWER_ZONE-4', 'Time in Zone 4'],
    ['TIME_IN_POWER_ZONE-5', 'Time in Zone 5'],
    ['TIME_IN_POWER_ZONE-6', 'Time in Zone 6'],
    ['TIME_IN_POWER_ZONE-7', 'Time in Zone 7']]);
c9.toolTip = '`${series.person}: ${series.params[i]}: ${series.values[i]}`';
c9.yaxislabel = 'Energy in Zone [J]';
c9.seriesNameLookup = new Map([
    ['WSUM_IN_POWER_ZONE-1', 'kJ in Zone 1'],
    ['WSUM_IN_POWER_ZONE-2', 'kJ in Zone 2'],
    ['WSUM_IN_POWER_ZONE-3', 'kJ in Zone 3'],
    ['WSUM_IN_POWER_ZONE-4', 'kJ in Zone 4'],
    ['WSUM_IN_POWER_ZONE-5', 'kJ in Zone 5'],
    ['WSUM_IN_POWER_ZONE-6', 'kJ in Zone 6'],
    ['WSUM_IN_POWER_ZONE-7', 'kJ in Zone 7']]);
c9.axis_operation = function (value: number) { return value / 1000.0; }
c10.toolTip = '`${series.person}: ${series.params[i]}: ${series.values[i]}`';
c10.xOffset = 0;

