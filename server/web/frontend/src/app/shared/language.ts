
export class Language {
    flag: string;
    lang: string;
    language: string;
    locale: any;

    constructor(lang: string, flag: string, language: string, localeId: string)
    {
        this.lang= lang;
        this.flag= flag;
        this.language= language;
        this.locale= localeId;
    }
}

export const DEFAULT_LANGUAGE= new Language('de', 'assets/lang/at.png', 'Deutsch', 'de');

export const LANGUAGES: Language[] = [
    DEFAULT_LANGUAGE,
    new Language('en', 'assets/lang/en.png', 'English', 'en'),
    new Language('hu', 'assets/lang/hu.png', 'Magyar', 'hu'),
    new Language('is', 'assets/lang/is.png', 'Íslenska', 'is'),
    new Language('mk', 'assets/lang/mk.png', 'македонски', 'mk'),
    new Language('bg', 'assets/lang/bg.png', 'български', 'bg')
];
