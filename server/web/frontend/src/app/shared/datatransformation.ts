export class DataTransformation {
  name: string;
  fun: ((a: number) => number) | undefined;
  constructor(name: string, fun: ((a: number) => number) | undefined) {
    this.name = name;
    this.fun = fun;
  }
}

export const HumanPercent = new DataTransformation('Human Percent', (a) => Math.round(a * 10000) / 100);
export const DataTransformations: Array<DataTransformation> = [
  new DataTransformation('', undefined),
  new DataTransformation('log 1digit', (a) => Math.round(Math.log10(a) * 10) / 10),
  new DataTransformation('log 2digits', (a) => Math.round(Math.log10(a) * 100) / 100),
  new DataTransformation('ln 1digit', (a) => Math.round(Math.log(a) * 10) / 10),
  new DataTransformation('ln 2digits', (a) => Math.round(Math.log(a) * 100) / 100),
  HumanPercent,
];

