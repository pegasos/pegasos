// generated file: Will be overwritten during build

import {Menu} from './menu';

export const MENUS: Menu[] = [
    new Menu('user/profile', 'mainMenu_personalData', false),
    new Menu('team', 'mainMenu_team', false),
    new Menu('live', 'mainMenu_currentEvents', false),
    new Menu('tagebuch', 'mainMenu_diary', false),
    new Menu('reports', 'mainMenu_reports', false),
//    new Menu('rankings', 'mainMenu_rankings'),
    new Menu('hilfe', 'mainMenu_help'),
    new Menu('impressum', 'mainMenu_imprint'),
    new Menu('admin', 'mainMenu_admin', false, ['ADMIN']),
];
