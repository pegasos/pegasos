import { AppService } from './../services/app.service';
import { Component, ChangeDetectorRef } from '@angular/core';

import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';

import { SessionbasedLineReportConfig } from '../shared/reportconfig';
import { Point, ReportSessionData, SessionInfo } from '../shared/sample';
import { LinebasedReport } from './linebased.report';

class SeriesPoint {
  session!: SessionInfo;
  value!: number;
}

class UserSeries {
  color!: string;
  points: Array<SeriesPoint>;
  name!: string;
  user!: number;

  constructor() {
    this.points = new Array<SeriesPoint>();
  }
}

@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class SessionbasedLineReportComponent extends LinebasedReport {
  override config!: SessionbasedLineReportConfig;
  override data!: ReportSessionData;

  private userSeries: Map<number, Map<string, UserSeries>>;

  protected override margin = {top: 10, right: 180, bottom: 70, left: 50};

  constructor(app: AppService,
    private cd: ChangeDetectorRef) {
    super(app);
    this.personColors = new Map<string, string>();
    this.userSeries = new Map<number, Map<string, UserSeries>>();
  }

  public onDataReceived(data: any) {
    this.data = data;
    console.log('config', {...this.config});

    // this.data.sessions.forEach(session => this.persons.add(session.userId));

    // construct the entries in the userSeries map
    this.data.users.forEach(user => this.userSeries.set(user, new Map<string, UserSeries>()));

    this.seriesNames = new Set(this.data.data.map(series => series.name));
    this.seriesPatterns = new Map<string, string>();
    let colorIdx = 0;
    // create patterns (= colors) for the series
    this.seriesNames.forEach(seriesName =>
      this.seriesPatterns.set(seriesName, this.config.colors[(colorIdx++) % this.config.colors.length]));

    // foreach user and series create a list
    this.data.data.forEach(series => {
      const sec = this.config.secondaryseries.includes(series.name);

      // now split the series up into the series of the users
      series.data.forEach((dataPoint, index) => {
        const user = this.data.sessions[index].userId;

        if ( !this.userSeries.get(user)?.has(series.name) ) {
          const s = new UserSeries();
          s.name = series.name;
          s.user = user;
          s.color = this.seriesPatterns.get(series.name) as string;
          this.userSeries.get(user)?.set(series.name, s);
        }

        if ( dataPoint != null ) {
          const p = new SeriesPoint();
          p.session = this.data.sessions[index];
          if ( !sec ) {
            p.value = !this.config.axis_operation ? dataPoint : this.config.axis_operation(dataPoint);
          } else {
            p.value = !this.config.secondary_axis_operation ? dataPoint : this.config.secondary_axis_operation(dataPoint);
          }

          this.userSeries.get(user)?.get(series.name)?.points.push(p);
        }
      });

      this.multiname = this.seriesNames.size > 1;
    });

    this.dataToScreen();

    this.cd.detectChanges();
    this.app.translateTree(this.config.id);
  }

  private dataToScreen(): void {

    // console.log([this.config.xOffset, this.innerWidth]);

    super.configureX(this.data.sessions.map(s => s.starttime));

    let data_ymin = Number.MAX_VALUE;
    let data_ymax = Number.MIN_VALUE;
    let data_y2min = Number.MAX_VALUE;
    let data_y2max = Number.MIN_VALUE;

    // which series are secondary axis
    const secondary = new Map<string, boolean>();

    this.data.data.forEach(series => {
      let sec = false;
      // Determine whether this entry should be displayed on the secondary axis
      if ( this.config.secondaryseries.includes(series.name) ) {
        sec = true;
        this.secondary_used = true;
      }
      const fun = sec ?
        (!this.config.secondary_axis_operation ? (p: number) => p : this.config.secondary_axis_operation) :
        (!this.config.axis_operation ? (p: number) => p : this.config.axis_operation);
      const exty = d3Array.extent(series.data, fun) as [number, number];
      if ( sec ) {
        data_y2min = d3Array.min([exty[0], data_y2min]) as number;
        data_y2max = d3Array.max([exty[1], data_y2max]) as number;
      } else {
        data_ymin = d3Array.min([exty[0], data_ymin]) as number;
        data_ymax = d3Array.max([exty[1], data_ymax]) as number;
      }
      secondary.set(series.name, sec);
    });

    super.configureYAxis(data_ymin, data_ymax, data_y2min, data_y2max);

    super.setup();

    this.userSeries.forEach((series, user) => series.forEach(s => this.addSeriesNew(s, user)));

    this.addLegendRight('Color');
  }

  private addSeriesNew(series: UserSeries, personId: number): void {
    // console.log('Adding series', series.name, personId, {...series});
    // console.log({...series.points});

    this.lineShowing.set(series.name, true);

    let isSecondary = false;
    // Determine whether this entry should be displayed on the secondary axis
    if ( this.config.secondaryseries.includes(series.name) ) {
      isSecondary = true;
    }

    // create local references for the line function
    const xAxis = this.x;
    const yAxis = isSecondary ? this.y2 : this.y1;

    const line = d3Shape.line<SeriesPoint>()
      .x(function(d: SeriesPoint) { return xAxis(d.session.starttime); })
      .y(function(d: SeriesPoint) { return yAxis(d.value); });

    const hash = this.nameHash(series.name);
    this.svg.append('path')
      // .datum(series.points)
      .attr('class', 'line-' + hash)
      .attr('fill', 'none')
      .attr('stroke', series.color)
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr('stroke-width', 1.5)
      .attr('d', line(series.points));

    const helper = this.getHelper(this.svg, xAxis, yAxis, this.config, this.personResolver);

    this.svg.selectAll('myCircle')
      .data(series.points)
      .enter()
      .append('circle')
        // .attr("class", "dot") // Assign a class for styling
        .attr('class', 'point-' + hash)
        .attr('fill', series.color)
        .attr('stroke', 'none')
        .attr('cx', (d: SeriesPoint, i: number) => xAxis(d.session.starttime))
        .attr('cy', (d: SeriesPoint) => yAxis(d.value))
        .attr('r', helper.radius)
        .on('click', (_: any, d: SeriesPoint) => { helper.onSessionDetails(d.session.session_id); })
        .on('mouseover', function (event: any, datum: any) { helper.handleMouseOver(datum, series.points.indexOf(datum), personId, series, event.target, event); })
        .on('mouseout', function (event: any, datum: any) { helper.handleMouseOut(datum, series.points.indexOf(datum), event.target); });
  }

  protected getDates(): number[] {
    return this.data.sessions.map(s => s.starttime);
  }

  protected getValues() {
    if (!this.multiPerson) {
      const values: Array<Array<Point>> = [];
      // get the one and only series
      const series = this.userSeries.get(this.data.users[0]) as Map<String,UserSeries>;
      this.seriesNames.forEach(name => {
        values.push((series.get(name) as UserSeries).points.map(p => {return {x: p.session.starttime, y: p.value};}));
      });
      return values;
    } else {
      const values: Array<Array<Point>> = [];
      this.data.users.forEach(userId => {
        const series = this.userSeries.get(userId) as Map<String,UserSeries>;
        this.seriesNames.forEach(name => {
          values.push((series.get(name) as UserSeries).points.map(p => {return {x: p.session.starttime, y: p.value};}));
        });
      });
      return values;
    }
  }
}
