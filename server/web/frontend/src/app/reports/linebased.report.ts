import { AppService } from './../services/app.service';
import { AfterViewInit, Component } from '@angular/core';
import { ReportComponent } from './report.component';
import { ReportWithLinesConfig } from '../shared/reportconfig';

import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3TimeFormat from 'd3-time-format';
import { format } from 'd3-format';
import { DataSeries, Point } from '../shared/sample';

@Component({template: 'report.component.html'})
export abstract class LinebasedReport extends ReportComponent implements AfterViewInit {

  public override config!: ReportWithLinesConfig;
  data: any;

  /**
   * Colors for persons.
   */
  protected personColors!: Map<string, string>;

  /**
   * Patterns for series
   */
  protected seriesPatterns!: Map<string, string>;

  /**
   * Name of the data series
   */
  protected seriesNames!: Set<string>;

  protected multiPerson!: boolean;

  /**
   * Users for which we have at least one data point
   */
  // protected persons: Set<number>;

  /**
   * Whether this report has different data series, i.e., more than one
   */
  protected multiname = false;

  protected secondary_used = false;

  /**
  * Status of all lines. key: name of the seris.
  */
  protected lineShowing: Map<string, boolean>;

  private timeout: any = null;

  x: any;
  y1: any;
  y2: any;

  constructor(protected override app: AppService) {
    super(app);
    // this.persons = new Set<number>();
    this.lineShowing = new Map<string, boolean>();
  }

  override ngAfterViewInit() {
    console.log('LinebasedReport::ngAfterViewInit', this.config.name, {config: this.config});

    if (this.config.secondaryseries.length > 0 ) {
      this.margin.left += 60;
    }
    super.ngAfterViewInit();
  }

  public abstract override onDataReceived(data: any): void;

  protected preprocessDataUserDates(): void {
    // pre processing of data. Determining type and computing names
    this.seriesNames = new Set<string>();
    this.seriesPatterns = new Map<string, string>();
    let patternidx = 0;
    this.data.series.forEach((series:DataSeries<number>) => {
      const sp = series.name.split('#');
      const seriesName = sp[1];
      this.seriesNames.add(seriesName);

      if ( !this.seriesPatterns.has(seriesName) ) {
        this.seriesPatterns.set(seriesName, this.config.patterns[patternidx++ % this.config.patterns.length]);
      }
    });

    this.multiname = this.seriesNames.size > 1;

    this.multiPerson = this.data.users.length > 1;
  }

  protected preprocessDataMetricUserParam(): void {
    // pre processing of data. Determining type and computing names
    this.seriesNames = new Set<string>();
    this.seriesPatterns = new Map<string, string>();
    let patternidx = 0;
    this.data.series.forEach((series:DataSeries<number>) => {
      const sp = series.name.split('#');
      const seriesName = sp[0];
      this.seriesNames.add(seriesName);

      if ( !this.seriesPatterns.has(seriesName) ) {
        this.seriesPatterns.set(seriesName, this.config.patterns[patternidx++ % this.config.patterns.length]);
      }
    });

    this.multiname = this.seriesNames.size > 1;

    this.multiPerson = this.data.users.length > 1;
  }

  /**
   * Configure x axis of the report i.e., set the domain for x
   * @param dates for which data is available
   */
  protected configureX(dates: number[]): void {
    this.x = d3Scale.scaleTime()
                     .rangeRound([this.config.xOffset, this.innerWidth]);

    let data_xmin = Number.MAX_VALUE;
    let data_xmax = Number.MIN_VALUE;

    const extx = d3Array.extent(dates, function(d) { return d; }) as [number, number];
    data_xmin = d3Array.min([extx[0], data_xmin]) as number;
    data_xmax = d3Array.max([extx[1], data_xmax]) as number;
    data_xmin = d3Array.min([extx[0], data_xmin]) as number;
    data_xmax = d3Array.max([extx[1], data_xmax]) as number;

    this.x.domain([data_xmin, data_xmax]);
  }

  protected configureYAxis(data_ymin: number, data_ymax: number, data_y2min: number, data_y2max: number): void {
    this.y1 = d3Scale.scaleLinear()
                    .rangeRound([this.innerHeight, 0]);

    this.y2 = d3Scale.scaleLinear()
                    .rangeRound([this.innerHeight, 0]);

    this.y1.domain([
      this.config.y1min ? this.config.y1min : data_ymin,
      this.config.y1max ? this.config.y1max : data_ymax]);
    this.y2.domain([
      this.config.y2min ? this.config.y2min : data_y2min,
      this.config.y2max ? this.config.y2max : data_y2max]);
  }

  /**
   * Create the svg area in which the report will be rendered. This will also create the axes.
   */
  protected setup(): void {
    this.svg.append('g')
      .attr('transform', 'translate(0,' + this.innerHeight + ')')
      .call(d3Axis.axisBottom<Date>(this.x).tickFormat(d3TimeFormat.timeFormat('%Y-%m-%d')))
      .attr('class', 'xAxis')
      .selectAll('text')
        .style('text-anchor', 'end')
        .attr('dx', '-.8em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(-65)')
        .select('.domain')
        .remove();

    const yAxis = d3Axis.axisLeft<number>(this.y1);
    if( this.config.yaxisInt ) {
      const yAxisTicks = this.y1.ticks()
        .filter((tick:any) => Number.isInteger(tick));
      yAxis
        .tickFormat(format('d'))
        .tickValues(yAxisTicks)
    }
    this.svg.append('g')
      .call(yAxis)
      .attr('class', 'yAxis')
      .append('text')
        .attr('fill', '#000')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .attr('class', 'trn')
        .text(this.config.yaxislabel);

    if ( this.secondary_used ) {
      const t = this.svg.append('g')
          .call(d3Axis.axisRight(this.y2))
          .attr('class', 'yAxis')
          .attr("transform", "translate(" + (this.config.xOffset + this.innerWidth) + " ,0)");
      if ( this.config.secondary_yaxsislabel ) {
        t.append('text')
          .attr('fill', '#000')
          .attr('transform', 'rotate(-90)')
          .attr('y', 35)
          // .attr('dy', '0.71em')
          .attr('text-anchor', 'end')
          .attr('class', 'trn')
          .text(this.config.secondary_yaxsislabel);
      }
    }
  }

  private addPersonLegend(type: 'Pattern' | 'Color' = 'Pattern', size: number, spacing: number, marginLeft: number): void {
    const legend = this.svg.select(function(this:any) {return this.parentNode;})
    .append('g')
      .attr('id', 'legend')
      .attr('transform', 'translate(' + marginLeft + ', ' + (this.margin.top + 20) + ')')
      .attr('class', 'Legend')
      .append('g')
        .attr('id', 'persons')
        .selectAll('g')
        .data(this.data.users);

    if ( type === 'Pattern' ) {
      legend
        .enter()
        .append('circle')
          .attr('cx', 0)
          .attr('cy', function(_:any, i:number) { return i * (size + spacing); })
          .attr('r', size)
          .attr('stroke', 'none')
          .style('fill', (d:any) => this.personColors.get('' + d) )
          .style('mask', (_:any) => '' )
          ;
    } else {
      legend
        .enter()
        .append('line')
          .attr('x1', 0)
          .attr('y1', (_:any, i:number) => i * (size + spacing))
          .attr('x2', size * 3)
          .attr('y2', (_:any, i:number) => i * (size + spacing))
          .style('stroke', 'black')
          .style('stroke-dasharray', (d:any) => this.personColors.get(d));
    }

    // const names: string[] = this.data.users.map(u => this.personResolver.getPerson(u).name);

    legend
      .enter()
      .append('text')
        .attr('x', type === 'Pattern' ? size * 1.2 : size * 3 + spacing)
        .attr('y', function(_:any, i:number) { return (i + 0.25) * (size + spacing); } )
        .style('fill', (d:any) => this.personColors.get('' + d) )
        .text((d:any) => this.personResolver.getPerson(d)?.name)
        .attr('text-anchor', 'left')
        // .style('alignment-baseline', 'central')
        ;
  }

  private addSeriesLegend(legend: string, type: string, seriesNames: string[], size: number, spacing: number) {
    const l2 = this.svg.select(function(this:any) {return this.parentNode;}).selectAll(legend)
        .append('g')
          .attr('id', 'dataSeries')
          .attr('transform', 'translate(0, ' + (this.data.users.length * (size + spacing) + 20) + ')')
          .selectAll('g')
          .data(seriesNames);

      if ( type === 'Pattern' ) {
        l2
          .enter()
          .append('line')
            .attr('x1', 0)
            .attr('y1', (_:any, i:number) => i * (size + spacing))
            .attr('x2', size * 3)
            .attr('y2', (_:any, i:number) => i * (size + spacing))
            .style('stroke', 'black')
            .style('stroke-dasharray', (d:any) => this.seriesPatterns.get(d))
            .attr('class', (d:any) => 'legend-' + this.nameHash(d))
            .on('click', (_:any,d:any) => this.onLegendClicked(d))
            .on('dblclick', (_:any,d:any) => this.onLegendDblClicked(d))
            ;
      } else {
        l2
          .enter()
          .append('circle')
            .attr('cx', 0)
            .attr('cy', (_:any, i:number) => i * (size + spacing))
            .attr('r', size)
            .attr('class', (d:any) => 'legend-' + this.nameHash(d))
            .attr('stroke', 'none')
            .style('fill', (d:any) => this.seriesPatterns.get('' + d) )
            .style('mask', (_:any) => '' )
            .on('click', (_:any,d:any) => this.onLegendClicked(d))
            .on('dblclick', (_:any,d:any) => this.onLegendDblClicked(d))
          ;
      }

      l2
        .enter()
        .append('text')
          .attr('class', 'LegendText')
          .attr('class', (d:any) => 'LegendText legend-' + this.nameHash(d))
          .on('click', (_:any, d:any) => this.onToggleLine(d))
          .attr('x', type === 'Pattern' ? size * 3 + spacing : size * 1.2)
          .attr('y', (_:any, i:number) => (i + 0.25) * (size + spacing) )
          .text((d:any) => this.config.seriesNameLookup.has(d) ? this.config.seriesNameLookup.get(d) : d)
          .attr('text-anchor', 'left');
  }

  private onLegendClicked(lineName: string): void {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => this.onToggleLine(lineName), 300);
  }

  private onLegendDblClicked(lineName: string): void {
    clearTimeout(this.timeout);

    this.onToggleAllLines(lineName);
  }

  protected onToggleLine(lineName: string): void {
    const newState = !this.lineShowing.get(lineName);
    const newOpacity = newState ? 1 : 0;
    const newSemiOpacity = newState ? 1 : 0.2;
    const hash = this.nameHash(lineName);
    this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.line-' + hash).style('opacity', newOpacity);
    this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.point-' + hash).style('opacity', newOpacity);
    this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.legend-' + hash).style('opacity', newSemiOpacity);
    this.lineShowing.set(lineName, newState);
  }

  protected onToggleAllLines(lineName: string): void {
    const semiOpaque = 0.2;
    this.lineShowing.forEach((_,name) => {
      if ( name === lineName ) {
        const hash = this.nameHash(lineName);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.line-' + hash).style('opacity', 1);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.point-' + hash).style('opacity', 1);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.legend-' + hash).style('opacity', 1);
        this.lineShowing.set(lineName, true);
      } else {
        const hash = this.nameHash(name);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.line-' + hash).style('opacity', 0);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.point-' + hash).style('opacity', 0);
        this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.legend-' + hash).style('opacity', semiOpaque);
        this.lineShowing.set(name, false);
      }
    })
  }

  protected addLegendRight(type: 'Pattern' | 'Color' = 'Pattern'): void {
    /**
     * Size of the circle
     */
    const size = 5;
    /**
     * Spacing between the entries
     */
    const spacing = 10;

    const spaceLegendRight = this.secondary_used ? 30 : 0;

    /**
     * Space between left border and the legend
     */
    const marginLeft = Math.min(this.margin.left + this.x.range()[this.x.range().length - 1] + this.config.xOffset + spaceLegendRight,
      (this.margin.left + this.innerWidth + this.config.xOffset + spaceLegendRight));
    this.addPersonLegend(type, size, spacing, marginLeft)

    if ( this.secondary_used ) {
      this.svg.select(function(this:any) {return this.parentNode;})
      .append('g')
        .attr('id', 'legendLeft')
        .attr('transform', 'translate(' + (10) + ', ' + (this.margin.top + 20) + ')')
        .attr('class', 'Legend')

      // For some reason .data does not accept a set so we need to convert it to an array first
      const seriesNames = Array.from(this.seriesNames).filter(name => !this.config.secondaryseries.includes(name));
      this.addSeriesLegend('#legendLeft', type, seriesNames, size, spacing);

      const seriesNamesSec = Array.from(this.seriesNames).filter(name => this.config.secondaryseries.includes(name));
      this.addSeriesLegend('#legend', type, seriesNamesSec, size, spacing);
    } else {
      // We have only primary data series entries and can add them on the right
      const seriesNames = Array.from(this.seriesNames);
      this.addSeriesLegend('#legend', type, seriesNames, size, spacing);
    }
  }

  protected getDownloadData(): any[][] {
    const data: any[][] = [];
    if ( this.multiPerson ) {
      const header: any[] = [''];
      this.data.users.forEach((userId:number) => {
        header.push(this.personResolver.getPerson(userId)?.name);
        this.seriesNames.forEach(_ =>  header.push(''));
      });
      data.push(header);
    }
    const header = ['Date'];
    this.data.users.forEach((_:any) => {
      if (this.multiPerson ) {
        header.push('');
      }
      // this.seriesNames.forEach(name =>  header.push(name));
      Array.from(this.seriesNames)
        // check if have a 'pretty' name for this series
        .map(n => this.config.seriesNameLookup.has(n) ? this.config.seriesNameLookup.get(n) : n)
        // add it
        .forEach(name =>  header.push(name as string));
    });
    data.push(header);

    const dates: number[] = this.getDates();
    const values: Array<Array<Point>> = this.getValues();
    const idxs: Array<number> = values.map(_ => 0);
    // console.log({dates}, {values}, {idxs});

    dates.forEach(date => {
      const line: Array<any> = [date];
      values.forEach( (val,idx) => {
        if ( val[idxs[idx]] && val[idxs[idx]].x === date ) {
          line.push(val[idxs[idx]].y);
          idxs[idx]++;
        } else {
          line.push('');
        }
      });
      data.push(line);
    });

    return data;
  }

  /**
   * TODO
   */
  protected abstract getValues(): Point[][];

  /**
   * TODO
   */
  protected abstract getDates(): number[];
}
