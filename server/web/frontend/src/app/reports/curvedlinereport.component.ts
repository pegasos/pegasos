import { Component } from '@angular/core';

import { AppService } from './../services/app.service';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3TimeFormat from 'd3-time-format';

import { Point, ReportUserDatesSingle } from './../shared/sample';
import { CurvedLineReportConfig } from '../shared/reportconfig';
import { LinebasedReport } from './linebased.report';

@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
// TODO: rename to ParamBased report?
export class CurvedLineReportComponent extends LinebasedReport {
  /**
   * Configuration of this report
   */
  public override config!: CurvedLineReportConfig;

  // TODO: wrong data type --> why do you think this is a wrong datatype? --> because it might / should not be oriented around dates
  override data!: ReportUserDatesSingle;

  /**
   * Params contained for a series in this report
   */
  private params: Map<string, Set<number>>;

  /**
   * Map of actual data series to be used for display. key is name-user.
   * For each key a set of <param,value> tuples will exist
   */
  private series: Map<string, Map<number, number>>;

  /**
   * which of the constructed series are to be displayed on the secondary axis
   */
  private secondary: Map<string, boolean>;

  protected override margin = {top: 10, right: 180, bottom: 70, left: 50};

  constructor(app: AppService) {
    super(app);
    this.params = new Map<string, Set<number>>();
    this.personColors = new Map<string, string>();
    this.series = new Map<string, Map<number, number>>();
    this.secondary = new Map<string, boolean>();
  }

  public onDataReceived(data: ReportUserDatesSingle) {
    this.data = data;
    this.preprocessDataMetricUserParam();
    this.dataToScreen();
  }

  private dataToScreen(): void {
    if ( this.config.auto_stops ) {
      this.x = d3Scale.scaleLog()
        .rangeRound([this.config.xOffset, this.innerWidth]);
    } else {
      this.x = d3Scale.scaleLinear();
      if ( this.config.stops !== undefined ) {
        this.x = this.x.domain(this.config.stops);
        const range: number[] = [];
        const step = this.innerWidth / this.config.stops.length;
        this.config.stops.forEach((stop, idx) => {
          range.push(idx * step + this.config.xOffset);
        });
        this.x = this.x.range(range);
      }
    }

    this.y1 = d3Scale.scaleLinear()
                     .rangeRound([this.innerHeight, 0]);

    this.y2 = d3Scale.scaleLinear()
                     .rangeRound([this.innerHeight, 0]);

    let data_y1min = Number.MAX_VALUE;
    let data_y1max = Number.MIN_VALUE;
    let data_y2min = Number.MAX_VALUE;
    let data_y2max = Number.MIN_VALUE;
    let data_xmin = this.config.xmin;
    let data_xmax = this.config.xmax;

    // this.data.dates= this.data.dates.map(d => Date.parse('' + d));

    let colidx = 0;

    // build the actual data series
    this.data.series.forEach(dseries => {
      let sec = false;
      const sp = dseries.name.split('#');
      const sn = sp[0];
      const param = sp.length === 3 ? parseInt(sp[1], 10) : 0;
      const pn = sp.length === 3 ? sp[2] : sp[1];

      data_xmin = d3Array.min([param, data_xmin]) as number;
      data_xmax = d3Array.max([param, data_xmax]) as number;

      let series;
      if ( this.series.has(sn + '#' + pn) ) {
        series = this.series.get(sn + '#' + pn);
      } else {
        series = new Map<number, number>();
        this.series.set(sn + '#' + pn, series);
      }

      if ( !this.params.has(sn) ) {
        this.params.set(sn, new Set<number>());
      }
      this.params.get(sn)?.add(param);

      // Determine whether this entry should be displayed on the secondary axis
      let i = 0;
      for (i = 0; i < this.config.secondaryseries.length; i++) {
        if ( this.config.secondaryseries[i] === sn ) {
          sec = true;
          this.secondary_used = true;
          break;
        }
      }
      this.secondary.set(sn + '#' + pn, sec);

      // use data for determining min / max of corresponding axis
      if ( !sec ) {
        const exty = d3Array.extent(dseries.data, function(d) { return d; }) as [number, number];
        data_y1min = d3Array.min([exty[0], data_y1min]) as number;
        data_y1max = d3Array.max([exty[1], data_y1max]) as number;
      } else {
        if( this.config.secondary_axis_operation ) {
          const op = this.config.secondary_axis_operation;
          dseries.data = dseries.data.map(p => p === null ? 0 : op(p));
        }
        const exty = d3Array.extent(dseries.data, function(d) { return d; }) as [number, number];
        data_y2min = d3Array.min([exty[0], data_y2min]) as number;
        data_y2max = d3Array.max([exty[1], data_y2max]) as number;
      }

      // build up actual series
      series?.set(param, dseries.data[0]);

      if ( !this.personColors.has(pn) ) {
        this.personColors.set(pn, this.config.colors[colidx++ % this.config.colors.length]);
      }
    });

    if ( this.config.auto_stops ) {
      this.x.domain([data_xmin, data_xmax]);
    }
    this.y1.domain([Math.min(this.config.y1min, data_y1min), data_y1max]);
    this.y2.domain([data_y2min, data_y2max]);
    console.log({data_xmin, data_xmax}, {data_y1min, data_y1max}, {data_y2min, data_y2max}, this.x, this.x(10));

    let xAxis = d3Axis.axisBottom(this.x)
      .tickFormat(function(d:any) { return d; });
    if ( !this.config.auto_stops && this.config.stops !== undefined ) {
      xAxis = xAxis.tickFormat(function(d:any) { return d; }).tickValues(this.config.stops);
    }
    this.svg.append('g')
      .attr('transform', 'translate(0,' + this.innerHeight + ')')
      // .call(d3Axis.axisBottom(this.x).tickFormat(function(d) { return d; }).tickValues(this.config.stops))
      .call(xAxis)
      .attr('class', 'xAxis')
      .selectAll('text')
        .style('text-anchor', 'end')
        .attr('dx', '-.8em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(-65)')
        .select('.domain')
        .remove();

    this.svg.append('g')
      .call(d3Axis.axisLeft(this.y1))
      .attr('class', 'yAxis')
      .append('text')
        .attr('fill', '#000')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .attr('class', 'trn')
        .text(this.config.yaxislabel);

    if ( this.secondary_used ) {
      this.svg.append('g')
          .call(d3Axis.axisRight(this.y2));
    }

    // let i= 0;
    this.series.forEach((series, key) => {
      this.addSeries(key, series, this.secondary.get(key) as boolean);
    });
    // this.data.series.forEach(series => this.addSeries(series, secondary[i++]));

    this.addLegendRight();
  }

  private addSeries(name: string, series: Map<number, number>, secondary: boolean): void {
    const seriesName = name.split('#')[0];
    const personId = +name.split('#')[1];
    const linecolor = this.personColors.get('' + personId);
    // const linecolor = this.personColors.get(series.name.split('#')[0]);
    const linepattern = this.seriesPatterns.get(name.split('#')[0]);
    console.log('Adding series', name, linecolor, linepattern, secondary);

    this.lineShowing.set(seriesName, true);

    // create local references for the line function
    const x = this.x;
    const y = secondary ? this.y2 : this.y1;

    const line = d3Shape.line<any>().curve(d3Shape.curveCatmullRom.alpha(0.5))
      .x(function(d) { return x(d.x); })
      .y(function(d) { return y(d.y); });

    const points: any[] = [];
    const keys: number[] = Array.from(series.keys()).sort((a: number, b: number) => a - b);
    keys.forEach(key => points.push({'x': key, 'y': series.get(key)}));
    console.log('Points', points);

    const hash = this.nameHash(seriesName);
    /*if ( secondary ) {
      this.svg.append('path')
          .datum(points)
          .attr('fill', 'none')
          .attr('stroke', linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', this.config.lineWidth)
          .style('stroke-dasharray', d => this.seriesPatterns.get(d))
          .attr('d', line);
    } else {*/
      this.svg.append('path')
          .datum(points)
          .attr('fill', 'none')
          .attr('class', 'line-' + hash)
          .attr('stroke', linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', this.config.lineWidth)
          .style('stroke-dasharray', (linepattern))
          .attr('d', line);
    // }

    const helper = this.getHelper(this.svg, x, y, this.config, this.personResolver, this.config.radius);

    this.svg.selectAll('myCircle')
          .data(points)
          .enter().append('circle') // Uses the enter().append() method
            // .attr("class", "dot") // Assign a class for styling
            .attr('class', 'point-' + hash)
            .attr('fill', linecolor)
            .attr('stroke', 'none')
            .attr('cx', function(d: any) { return x(d.x); })
            .attr('cy', function(d: any) { return y(d.y); })
            .attr('r', helper.radius)
            /*
            function(event:any, datum:any) {
          // datum == point
          const index = points.indexOf(datum);
          console.log(event,datum,index);
            */
            .on('mouseover', function(this:any, event:any,d:any, i:number) { helper.handleMouseOver(d, i, personId, series, d, event); })
            .on('mouseout', function (d:any, i:number) { helper.handleMouseOut(d, i, d); })
            ;
  }

  protected getDates(): number[] {
    const keys: Set<number> = new Set<number>();
    this.series.forEach(series => Array.from(series.keys()).forEach(k => keys.add(k)));
    return Array.from(keys).sort((a: number, b: number) => a - b);
  }

  protected getValues(): Point[][] {
    const values: Point[][] = [];
    this.data.users.forEach(userId => {
      this.seriesNames.forEach(name => {
        const series: Map<number, number> = this.series.get(name + '#' + userId) as Map<number, number>;
        const points: Array<Point> = [];
        if( series ) {
          // add all points
          const keys: number[] = Array.from(series.keys()).sort((a: number, b: number) => a - b);
          keys.forEach(key => points.push({'x': key, 'y': series.get(key) as number}));
        }
        values.push(points);
      });
    });
    return values;
  }
}
