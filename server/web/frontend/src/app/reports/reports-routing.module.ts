import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsMainComponent } from './reports-main.component';

export const routes: Routes = [
  // { path: 'reports', component: ReportsMainComponent },
  // { path: 'reports/:report', component: ReportsMainComponent },
  { path: '', component: ReportsMainComponent },
  { path: ':report', component: ReportsMainComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
