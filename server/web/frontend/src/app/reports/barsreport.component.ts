import { Component } from '@angular/core';

import * as d3Scale from 'd3-scale';
import * as d3Axis from 'd3-axis';
import * as d3TimeFormat from 'd3-time-format';

import { AppService } from './../services/app.service';
import { ReportUserDatesSingle, DataSeries } from './../shared/sample';
import { BarsReportConfig } from '../shared/reportconfig';
import { ReportComponent } from './report.component';

interface Point {
  x: Date;
  y: number;
}

function myhmsformat (num: number) {
  const h = Math.floor( num / 3600 );
  const m = Math.floor((num - h * 3600) / 60 );
  const s = num - (h * 3600 + m * 60);
  return (h > 0 ? (( h < 10 ? '0' + h : h ) + ':') : '') + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s ); }

const DateDiff = {

  inDays: function(d1: number, d2: number) {
      return Math.floor((d2 - d1) / (24 * 3600 * 1000));
  },

  inWeeks: function(d1: number, d2: number) {
    return Math.floor( (d2 - d1) / (24 * 3600 * 1000 * 7));
  },

  inMonths: function(d1: Date, d2: Date) {
      const d1Y = d1.getFullYear();
      const d2Y = d2.getFullYear();
      const d1M = d1.getMonth();
      const d2M = d2.getMonth();

      return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
  },

  inYears: function(d1: Date, d2: Date) {
      return d2.getFullYear() - d1.getFullYear();
  }
};

/**
 * One column / bar in the chart. One column presents one person and contains all date for the given time instance
 */
class Column {
  public date!: Date;
  public params: string[] = [];
  public start: number[] = [];
  public end: number[] = [];
  public values: number[] = [];
  public person!: string;
  public name!: string;
}

@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class BarsReportComponent extends ReportComponent {
  public override config!: BarsReportConfig;

  data!: ReportUserDatesSingle;

  protected override margin = {top: 10, right: 180, bottom: 75, left: 50};

  x: any;
  y: any;

  /**
   * Whether this report has data series with parameters/names (e.g. Time in Zone where for each zone a series exists)
   */
  private multiname = false;

  private multiPerson!: boolean;

  /**
   * Accumulated sums for series. Only used when `multiname = true` see above.
   */
  private cumSums: Map<string, number>;

  /**
   * Colors for persons. config.colors is used when `multiname = false`. Otherwise config.patterns will be used
   */
  private personColors: Map<string, string> = new Map<string, string>();

  /**
   * Colors for the different sereis.
   */
  private seriesColors: Map<string, string> = new Map<string, string>();

  /**
   * Name of the data series
   */
  private seriesNames!: Set<string>;

  /**
   * Existing params for each series
   */
  private seriesParams: Map<string,Set<string>> = new Map<string,Set<string>>();

  /**
   * Dates on the x-axis
   */
  private dates!: Date[];

  protected seriesShowing!: Map<string,boolean>;

  constructor(app: AppService) {
    super(app);

    this.cumSums = new Map<string, number>();
    this.seriesColors = new Map<string, string>();
  }

  public onDataReceived(data: ReportUserDatesSingle): void {
    this.data = data;
    this.seriesNames = new Set<string>();

    // pre processing of data. Determining type and computing names
    this.preprocessData();

    this.addDefs();
    this.dataToScreen();
    this.app.translateTree(this.config.id);
  }

  private preprocessData(): void {
    this.data.series.forEach(series => {
      const sp = series.name.split('#');
      const seriesName = sp[0]; // eg TINZ
      const seriesParam = sp.length === 3 ? sp[1] : ''; // eg 3 (zone 3)
      if ( sp.length === 3 ) {
        this.multiname = true;
        this.seriesNames.add(seriesName + '-' + seriesParam);
        if (!this.seriesParams.has(seriesName)) {
          this.seriesParams.set(seriesName, new Set<string>());
        }
        this.seriesParams.get(seriesName)?.add(seriesParam);
      } else {
        this.seriesNames.add(seriesName);
      }
    });

    this.multiPerson = this.data.users.length > 1;

    if (this.config.sortseries ) {
      this.data.series.sort((a: DataSeries<number>, b: DataSeries<number>) => {
        const spA = a.name.split('#');
        const seriesNameA = spA[0]; // eg TINZ
        const seriesParamA = spA.length === 3 ? spA[1] : ''; // eg 3 (zone 3)
        const aN = seriesNameA + '-' + seriesParamA;

        const spB = b.name.split('#');
        const seriesNameB = spB[0]; // eg TINZ
        const seriesParamB = spB.length === 3 ? spB[1] : ''; // eg 3 (zone 3)
        const bN = seriesNameB + '-' + seriesParamB;

        return aN.localeCompare(bN);
      });
      this.seriesNames = new Set(Array.from(this.seriesNames).sort());
    }

    this.dates = this.data.dates;

    this.seriesShowing = new Map<string,boolean>();
    this.seriesNames.forEach(n => this.seriesShowing.set(n, true));
  }

  private dataToScreen(): void {

    const acumulator = new Map<string, number[]>();
    const transformedData = new Map<string, Column>();

    let data_ymax = Number.MIN_VALUE;

    this.data.series.forEach( series => {
      const sp = series.name.split('#');
      const seriesName = sp[0]; // eg TINZ
      const seriesParam = sp.length === 3 ? sp[1] : ''; // eg 3 (zone 3)
      const seriesPerson = sp.length === 3 ? sp[2] : sp[1]; // eg 1 (user id 1)

      series.data.forEach( (d, idx) => {
        const key = this.data.dates[idx] + '-' + seriesPerson;
        let acum = 0;

        if ( acumulator.has(key) ) {
          const tmp = acumulator.get(key) as number[];
          acum = tmp[tmp.length - 1];
        } else {
          acumulator.set(key, [0]);
        }

        if ( !transformedData.has(key) ) {
          const s = new Column();
          s.date = this.data.dates[idx];
          s.person = seriesPerson;
          transformedData.set(key, s);
        }

        const td = transformedData.get(key) as Column;
        td.start.push(acum);

        const value = this.config.axis_operation(d);
        acum += value;
        // console.log(value, d);

        td.name = seriesName;
        td.values.push(value);
        td.end.push(acum);
        td.params.push(seriesParam);

        acumulator.get(key)?.push(acum);

        if ( acum > data_ymax ) {
          data_ymax = acum;
        }
      });
    });

    console.log(acumulator, transformedData, this.seriesNames);

    // construct domain
    const xDomain: string[] = [];
    this.data.dates.forEach( date => {
      this.data.users.forEach( user => {
        xDomain.push(date + '-' + user);
      });
    });

    this.x = d3Scale.scaleBand()
                     .rangeRound([10, Math.min(xDomain.length * this.config.desiredBarWith, this.innerWidth)])
                     .domain(xDomain)
                     // How much space between bars
                     .paddingInner(0.05)
                     // How much space between border and first/last bar
                     .paddingOuter(0.1);

    const timeTicks = this.config.yaxis_type === 'Time' ? this.createTimeTicks(data_ymax, Math.floor(this.height / 20)) : [];

    if ( this.config.yaxis_type === 'Time') {
      this.y = d3Scale.scaleTime()
        .rangeRound([this.innerHeight, 0])
        .domain([0, timeTicks[timeTicks.length - 1]]);
      // if ( data_ymax > 3600 ) {
      //   this.y = this.y.nice(d3Time.timeMinute);
      // }
    } else {
      this.y = d3Scale.scaleLinear()
        .rangeRound([this.innerHeight, 0])
        .domain([0, data_ymax])
        .nice();
    }

    const formatWeek = d3TimeFormat.timeFormat('%Y, %b %d'); // '%Y-%m-%d'
    const xAxis = d3Axis.axisBottom<string>(this.x)
      .tickFormat(d => formatWeek(new Date(d.split('T')[0])));
      // .ticks(d3Time.timeWeek, xcount);

    let yAxis = d3Axis.axisLeft<number>(this.y);
    if ( this.config.yaxis_type === 'Time' ) {
      yAxis = yAxis.tickFormat(myhmsformat)
        .tickValues(timeTicks);
    }

    /*
    const axx = this.svg.append('g')
      .attr('transform', 'translate(20,' + this.innerHeight + ')')
      .attr('class', 'xAxis')
      .call(this.xAxis);

    axx.append('text')
      .attr('fill', '#000')
      .attr('x', this.innerWidth / 3)
      // .attr('y', '6')
      .attr('dy', '2.5em')
      .attr('text-anchor', 'end')
      .attr('font-size', axisLabelFontSize)
      .attr('class', 'trn')
      .text('Time');

    axx.select('.domain').remove();
    */

    const axx = this.svg.append('g')
        .attr('transform', 'translate(0,' + this.innerHeight + ')')
        .attr('class', 'xAxis')
        .call(xAxis);

    axx.selectAll('text')
      .style('text-anchor', 'end')
      .attr('dx', '-.8em')
      .attr('dy', '.15em')
      .attr('transform', 'rotate(-65)');
        // .style('font', font_y)

    if( this.config.xAxisScale != 'None' ) {
      axx.append('text')
        .attr('fill', '#000')
        .attr('x', this.innerWidth / 3)
        // .attr('y', '6')
        .attr('dy', '6em')
        .attr('text-anchor', 'end')
        .attr('font-size', '11px')
        .attr('class', 'trn')
        // .text('Week');
        .text('reports_axis_' + this.config.xAxisScale)
    }

    axx.select('.domain').remove();

    this.svg.append('g')
          .call(yAxis)
          // .style('font', font_y)
          .attr('class', 'yAxis')
          .append('text')
            .attr('fill', '#000')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('dy', '0.71em')
            .attr('text-anchor', 'end')
            .attr('class', 'trn')
            .text(this.config.yaxislabel);

    // if we have multiple series ==> color for series, mask for persons
    if ( this.multiname ) {
      const personColors = this.personColors;
      const seriesColors = this.seriesColors;

      this.data.users.forEach((person, idx) => {
        personColors.set('' + person, this.config.patterns[idx % this.config.patterns.length]);
      });

      let ind = 0;
      this.seriesNames.forEach( seriesName => {
        seriesColors.set(seriesName, this.config.colors[ind++ % this.config.colors.length]);
      });
    } else {
      // we have only on series ==> color for persons
      const personColors = this.personColors;
      const seriesColors = this.seriesColors;

      this.data.users.forEach((person, idx) => {
        personColors.set('' + person, this.config.colors[idx % this.config.colors.length]);
      });

      let ind = 0;
      this.seriesNames.forEach( seriesName => {
        seriesColors.set(seriesName, this.config.patterns[ind++ % this.config.patterns.length]);
      });
    }

    transformedData.forEach((value) => {
      this.addSeries(value);
    });

    this.addLegend();
  }

  private addLegend(): void {
    const size = 15;

    const marginLeft = Math.min(this.margin.left + this.x.range()[1] + 10, (this.margin.left + this.innerWidth + 10));

    const legend = this.svg.select(function(this:any) {return this.parentNode;})
      .append('g')
        .attr('id', 'legend')
        .attr('transform', 'translate(' + marginLeft + ', ' + (this.margin.top + 20) + ')')
        .attr('class', 'Legend')
        .append('g')
          .attr('id', 'persons')
          .selectAll('g')
          .data(this.data.users);

    legend
      .enter()
      .append('rect')
        .attr('x', 0)
        .attr('y', function(_:any, i:number) { return i * (size + 5); }) // 100 is where the first dot appears. 25 is the distance between dots
        .attr('width', size)
        .attr('height', size)
        // .style("fill", d => this.personColors.get('' + d))
        .style('fill', (d:any) => this.multiname ? '' : this.personColors.get('' + d) )
        .style('mask', (d:any) => this.multiname ? this.personColors.get('' + d) : '' )
        ;

    // console.log(this.personResolver);
    // const names: string[] = this.data.users.map(u => this.personResolver.getPerson(u)?.name as string);
    // console.log(names);

    legend
      .enter()
      .append('text')
        .attr('x', size * 1.2)
        // 100 is where the first dot appears. 25 is the distance between dots
        .attr('y', function(_:any, i:number) { return (i + 0.55) * (size + 5); } )
        .style('fill', (d:any) => this.multiname ? '' : this.personColors.get('' + d) )
        .text((d:any) => this.personResolver.getPerson(d)?.name)
        .attr('text-anchor', 'left')
        // .style('alignment-baseline', 'central')
        ;

    console.log('Is multiname?', this.multiname, this.seriesNames, this.seriesColors);
    if ( this.multiname ) {
      // For some reason .data does not accept a set so we need to convert it to an array first
      const seriesNames = Array.from(this.seriesNames);

      const l2 = this.svg.select(function(this:any) {return this.parentNode;}).selectAll('#legend')
        // .selectAll('#dataSeries')
        .append('g')
          .attr('id', 'dataSeries')
          .attr('transform', 'translate(0, ' + (this.data.users.length * (size + 5) + 20) + ')')
          .selectAll('g')
          .data(seriesNames);

      l2
        .enter()
        .append('rect')
          .attr('x', 0)
          .attr('y', (_:any, i:number) => i * (size + 5))
          .attr('width', size)
          .attr('height', size)
          .attr('class', (d:any) => 'legend-' + this.nameHash(d))
          .style('fill', (d:any) => this.seriesColors.get(d) )
          .on('click', (_:any,d:any) => this.onLegendSeriesClicked(d))
          ;

      l2
        .enter()
        .append('text')
          .attr('x', size * 1.2)
          .attr('y', (_:any, i:number) => (i + 0.55) * (size + 5) )
          .style('fill', (d:any) => this.seriesColors.get(d) )
          .text((d:any) => this.config.seriesNameLookup.has(d) ? this.config.seriesNameLookup.get(d) : d)
          .attr('class', (d:any) => 'legend-' + this.nameHash(d))
          .attr('text-anchor', 'left');
    }
  }

  private addSeries(series: Column): void {
    // create local references
    const x = this.x;
    const y = this.y;
    const height = this.innerHeight;

    const helper = this.getHelper(this.svg, x, y, this.config, this.personResolver);

    const bar = this.svg.selectAll('.mybar')
      .data(series.start)
      .enter().append('rect')
        .attr('class', (_:any, i:number) => 'data-bar box-person-' + series.person + ' box-series-' + this.nameHash(series.name + '-' + series.params[i]))
        .attr('id', 'data-bar')
        .attr('x', () => x(series.date + '-' + series.person))
        .attr('y', (_:any, i:number) => y(series.end[i]))
        .attr('height', (_:any, i:number) => height - y(series.values[i]))
        .attr('width', x.bandwidth() )
        .style('fill', (_:any, i:number) => this.multiname ?
          this.seriesColors.get(series.name + '-' + series.params[i]) :
          this.personColors.get(series.person) )
        .style('mask', (_:any, i:number) => this.multiname ? this.personColors.get(series.person) : this.seriesColors.get(series.params[i]) )
        .style('opacity', 0.8)
        // datum == start
        .on('mouseover', function(event:any, datum:any) {
          const index = series.start.indexOf(datum);
          helper.handleMouseOver(datum, index, +series.person, series, event.target, event);
        })
        // datum == start
        .on('mouseleave', function(event:any, datum:any) {
          const index = series.start.indexOf(datum);
          helper.handleMouseOut(datum, index, event.target); })
        .on('mousemove', function(event:any, datum:any) {
          // datum == start
          const index = series.start.indexOf(datum);
          helper.handleMouseMove(event, event.target, datum, index);
        });

    const timeAxis = this.config.yaxis_type === 'Time';
    this.svg.selectAll('.barText')
      .data(series.start)
        .enter()
          .append('text')
            .attr('class', 'yAxsis')
            .attr('x', function(d:any) { return x(series.date + '-' + series.person); })
            .attr('y', function(d:any) { return y(series.end[series.end.length - 1]); })
            .attr('dy', '-.35em')
            .attr('dx', x.bandwidth() / 4)
            .text(function(d:any) { return timeAxis ?
                myhmsformat(series.end[series.end.length - 1]) :
                Math.round(series.end[series.end.length - 1]); })
            .style('text-anchor', 'start')
            .style('font-family', 'sans-serif')
            .style('font-size', '0.5em')
      ;
  }

  private onLegendSeriesClicked(lineName: string): void {
    /*clearTimeout(this.timeout);

    this.timeout = setTimeout(() => this.onToggleLine(lineName), 300);*/

    // .attr('class', (_:any, i:number) => 'data-bar box-person-' + series.person + ' box-series-' + hash + '-' + series.params[i])
    const newState = !this.seriesShowing.get(lineName);
    const newOpacity = newState ? 1 : 0;
    const newSemiOpacity = newState ? 1 : 0.2;
    const hash = this.nameHash(lineName);
    this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.box-series-' + hash).style('opacity', newOpacity);
    this.svg.select(function(this:any) {return this.parentNode;}).selectAll('.legend-' + hash).style('opacity', newSemiOpacity);
    this.seriesShowing.set(lineName, newState);
  }

  /**
   * add pattern definitions
   */
  private addDefs(): void {
    const defs = this.svg.select(function(this:any) {return this.parentNode;})
      .append('defs');

    defs
      .append('pattern')
        .attr('id', 'pattern-stripe15')
        .attr('width', 4)
        .attr('height', 4)
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('patternTransform', 'rotate(15)')
        .append('rect')
          .attr('width', 2)
          .attr('height', 4)
          .attr('transform', 'translate(0,0)')
          .attr('fill', 'white')
          ;
    defs
      .append('pattern')
        .attr('id', 'pattern-stripe45')
        .attr('width', 4)
        .attr('height', 4)
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('patternTransform', 'rotate(45)')
        .append('rect')
          .attr('width', 2)
          .attr('height', 4)
          .attr('transform', 'translate(0,0)')
          .attr('fill', 'white')
          ;

    defs
    .append('pattern')
      .attr('id', 'pattern-stripe90')
      .attr('width', 4)
      .attr('height', 4)
      .attr('patternUnits', 'userSpaceOnUse')
      .attr('patternTransform', 'rotate(90)')
      .append('rect')
        .attr('width', 2)
        .attr('height', 4)
        .attr('transform', 'translate(0,0)')
        .attr('fill', 'white')
        ;
    defs
      .append('pattern')
        .attr('id', 'pattern-stripe-45')
        .attr('width', 4)
        .attr('height', 4)
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('patternTransform', 'rotate(-45)')
        .append('rect')
          .attr('width', 2)
          .attr('height', 4)
          .attr('transform', 'translate(0,0)')
          .attr('fill', 'white')
          ;

    defs
      .append('mask')
        .attr('id', 'mask-stripe1')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe15)');

      defs
      .append('mask')
        .attr('id', 'mask-stripe2')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe90)');

    defs
      .append('mask')
        .attr('id', 'mask-stripe3')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe-45)');

    defs
      .append('mask')
        .attr('id', 'mask-stripe4')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe45)');

    const h = defs
      .append('mask');
    h.attr('id', 'mask-stripe5')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe15)');
    h.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe-45)');

    const h2 = defs
      .append('mask');
    h2.attr('id', 'mask-stripe6')
        .append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe90)');
    h2.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', '100%')
          .attr('height', '100%')
          .attr('fill', 'url(#pattern-stripe-45)');
  }

  /**
   * direct copy of barschart.components.ts
   * @param max time in seconds
   * @param count number of ticks
   */
   private createTimeTicks(max: number, count: number) {
    /**
     * breaks in minutes
     */
    const breaks = [60, 30, 20, 15, 10, 5, 4, 3, 2, 1];

    const quot = breaks.map(b => Math.floor(max / 60 / count / b));
    // console.log({quot, count});
    let num = breaks[breaks.length - 1] * 60;
    for ( const idx in breaks ) {
      if ( quot[idx] > 0 ) {
        num = breaks[idx] * 60;
        break;
      }
    }
    // console.log({num});

    const ret = [];
    let i = 0;
    while ( i < max + num ) {
      ret.push(i);
      i += num;
    }
    return ret;
  }

  protected getDownloadData(): any[][] {
    const data: any[][] = [];
    if ( this.multiPerson ) {
      const header: any[] = [''];
      this.data.users.forEach(userId => {
        header.push(this.personResolver.getPerson(userId)?.name);
        this.seriesNames.forEach(_ =>  header.push(''));
      });
      data.push(header);
    }
    const header = ['Date'];
    this.data.users.forEach(_ => {
      if (this.multiPerson ) {
        header.push('');
      }
      Array.from(this.seriesNames)
        // check if have a 'pretty' name for this series
        .map(n => this.config.seriesNameLookup.has(n) ? this.config.seriesNameLookup.get(n) : n)
        // add all names
        .forEach(name =>  header.push(name as string));
    });
    data.push(header);

    const values: Array<Array<Point>> = [];// this.getValues();
    // was hilft: this.multiname
    if ( this.multiname ) {
      // const seriesNames = Array.from(new Set(Array.from(this.seriesNames).map(name => name.substring(0, name.lastIndexOf('-')))));
      const seriesNames = this.seriesParams.keys();
      console.log(seriesNames);
      this.data.users.map(userId => {
        Array.from(seriesNames).forEach(name => {
          this.seriesParams.get(name)?.forEach(param => {
            const lookupname = name + '#' + param + '#' + userId;
            const series = this.data.series.find(series => series.name === lookupname);

            const points: Array<Point> = [];
            if (series) {
              series.data.forEach((value,index) => points.push({x: this.data.dates[index], y: value}));
            }
            values.push(points);
          });
        });
      });
    }
    else {
      // If we have no params for the series names, we go over all users and names
      // we find the coresponding series and add all points
      this.data.users.map(userId => {
        Array.from(this.seriesNames).map(name => {
          const lookupname = name + '#' + userId;
          const series = this.data.series.find(series => series.name === lookupname);

          const points: Array<Point> = [];
          if (series) {
            series.data.forEach((value,index) => points.push({x: this.data.dates[index], y: value}));
          }
          values.push(points);
        });
      });
    }

    const idxs: Array<number> = values.map(_ => 0);
    // console.log({dates}, {values}, {idxs});

    this.dates.forEach(date => {
      const line: Array<any> = [date];
      values.forEach( (val,idx) => {
        if ( val[idxs[idx]] && val[idxs[idx]].x === date ) {
          line.push(val[idxs[idx]].y);
          idxs[idx]++;
        } else {
          line.push('');
        }
      });
      data.push(line);
    });

    return data;
  }
}
