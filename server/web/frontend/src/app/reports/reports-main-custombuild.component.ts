import { Component, OnInit, ChangeDetectorRef, ViewChild, Input, Directive, ViewContainerRef, ViewChildren, ComponentFactoryResolver } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { AppService } from './../services/app.service';
import { ActivitySelectComponent } from '../actioncomponents/activity-select/activityselect.component';
import { ReportsMainComponent } from './reports-main.component';
import { BarsReportConfig, ConfigVar, CurvedLineReportConfig, DataTransformations, MultiLineReportConfig,
  ReportConfig, ReportInfo, ReportType, SessionbasedLineReportConfig} from '../shared/reportconfig';


@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[configHost]',
})
export class ConfigHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

interface ConfigComponent {
  name: string;
  configName: string;
  parent: CustomReportBuilderComponent;
}

@Component({
  template: `
<div class="col-xs-10 col-sm-4 col-md-4 col-lg-2 header-column" [formGroup]="parent.customReportForm">
  <div class="filterForm_label">{{name}}</div>
  <p-inputNumber [formControlName]="configName" mode="decimal" [size]="10"></p-inputNumber>
</div>`
})
export class NumberConfigComponent implements ConfigComponent {
  name!: string;
  configName!: string;
  parent!: CustomReportBuilderComponent;
}

@Component({
  template: `
<div class="col-xs-10 col-sm-4 col-md-4 col-lg-2 header-column" [formGroup]="parent.customReportForm">
  <div class="filterForm_label">{{name}}</div>
  <input type="text" [formControlName]="configName"  class="form-control"/>
</div>`
})
export class TextConfigComponent implements ConfigComponent {
  name!: string;
  configName!: string;
  parent!: CustomReportBuilderComponent;
}

@Component({
  template: `
<div class="col-xs-10 col-sm-4 col-md-4 col-lg-2 header-column" [formGroup]="parent.customReportForm">
  <div class="filterForm_label">{{name}}</div>
  <p-chips styleClass="test" [formControlName]="configName" separator=","></p-chips>
</div>`
})
export class ListConfigComponent implements ConfigComponent {
  name!: string;
  configName!: string;
  parent!: CustomReportBuilderComponent;
}

@Component({
  template: `
<div class="col-xs-10 col-sm-4 col-md-4 col-lg-2 header-column" [formGroup]="parent.customReportForm">
  <div class="filterForm_label">{{name}}</div>
  <p-dropdown [options]="parent.Transformations" optionLabel="name" [formControlName]="configName" [style]="{'width':'200px'}" placeholder="select"></p-dropdown>
</div>`
})
export class DataTransformationConfigComponent implements ConfigComponent {
  name!: string;
  configName!: string;
  parent!: CustomReportBuilderComponent;
}

@Component({
  template: `
<div class="col-xs-10 col-sm-4 col-md-4 col-lg-2 header-column" [formGroup]="parent.customReportForm">
  <div class="filterForm_label">{{name}}</div>
  <p-dropdown [options]="AxsisTimeScaleTypes" [formControlName]="configName"></p-dropdown>
</div>`
})
export class AxisTimeScaleConfigComponent implements ConfigComponent {
  name!: string;
  configName!: string;
  parent!: CustomReportBuilderComponent;
  AxsisTimeScaleTypes = ['None', 'Day', 'Week', 'Month'];
}

function reportConfigFromType(type: ReportType): ReportConfig {
  console.log('config from Type: ', type);
  switch ( type ) {
    case 'Bars':
      return new BarsReportConfig('custom');
    case 'Curve':
      return new CurvedLineReportConfig('custom');
    case 'Multiline':
      return new MultiLineReportConfig('custom');
    case 'SessionbasedLines':
      return new SessionbasedLineReportConfig('custom');
  }
  return new ReportConfig('_error_');
}

@Component({
  selector: 'app-reports-custombuild',
  templateUrl: './reports-main-custombuild.component.html',
  styleUrls: ['./reports-main-custombuild.component.scss']}
)
export class CustomReportBuilderComponent implements OnInit {

  @Input() host!: ReportsMainComponent;

  @ViewChild('activitiesSelect') protected activitiesSelect!: ActivitySelectComponent;

  customReportForm!: FormGroup;

  selectedType: any;
  selectedAggregate: any;

  types = [{name: 'User Value', type: 'UserValue'},
  {name: 'Metric', type: 'Metric'},
  {name: 'Metrics accumulate', type: 'MetricAccumulateSimple'},
  {name: 'Metrics accumulate (param)', type: 'MetricAccumulateParam'}];
  GroupBys = [{name: 'DAY', type: 'DAY'},
    {name: 'WEEK', type: 'WEEK'},
    {name: 'MONTH', type: 'MONTH'},
    {name: 'ALL', type: 'ALL'}];
  Aggregates = [{name: 'SUM', type: 'SUM'},
  {name: 'MAX', type: 'MAX'},
  {name: 'MIN', type: 'MIN'},
  {name: 'MEAN', type: 'MEAN'},
  {name: 'FIRST', type: 'FIRST'},
  {name: 'LAST', type: 'LAST'}];

  // 'Simple' | 'SessionbasedLines' | 'Multiline' | 'Bars' | 'Curve'
  Displays /*: [{name: string, type: ReportType}]*/ = [
    {name: 'Session Lines', type: 'SessionbasedLines'},
    {name: 'User Lines', type: 'Multiline'},
    {name: 'Bars', type: 'Bars'},
    {name: 'Curve', type: 'Curve'}
  ];

  /**
   * Data to be displayed on the screen
   */
  data: any;

  configReady = false;

  Transformations = DataTransformations;

  /**
   * Hosts for the configurations
   */
  @ViewChildren(ConfigHostDirective) configHosts!: any;

  configVars: Array<string> = [];
  reportConfig!: ReportConfig;

  constructor(protected app: AppService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cd: ChangeDetectorRef,
    protected componentFactoryResolver: ComponentFactoryResolver,
    protected fb: FormBuilder) { }

  ngOnInit() {
    this.customReportForm = this.fb.group({
      selectedType: new FormControl({value: this.types[0], disabled: false}),
      activities: new FormControl({value: [], disabled: true}),
      values: [],
      groupBy: new FormControl({value: this.GroupBys[0], disabled: true}),
      aggregate: new FormControl({value: this.Aggregates[0], disabled: true}),
      selectedDisplay: new FormControl({value: this.Displays[0], disabled: false}),
    });

    if( this.reportConfig === undefined ) {
      this.reportConfig = reportConfigFromType(this.Displays[0].type as ReportType);
      this.prepareConfigForm();
    }
  }

  public onTypeSelect(originalEvent: any): void {
    const item = originalEvent.value;
    console.log(item.type, this.customReportForm);
    console.log(originalEvent);
    if ( item.type === 'UserValue' ) {
      this.customReportForm.get('activities')?.disable();
    } else {
      this.customReportForm.get('activities')?.enable();
    }

    if ( item.type === 'MetricAccumulateSimple' || item.type === 'MetricAccumulateParam') {
      this.customReportForm.get('groupBy')?.enable();
      this.customReportForm.get('aggregate')?.enable();
    } else {
      this.customReportForm.get('groupBy')?.disable();
      this.customReportForm.get('aggregate')?.disable();
    }
    this.selectedType = item;
  }

  public onDisplaySelect(originalEvent: any): void {
    const item = originalEvent.value;
    // console.log(item.type, this.customReportForm);

    // TODO: convert config
    this.reportConfig = reportConfigFromType(item.type as ReportType);
    this.prepareConfigForm();
  }

  /**
   * Invoked when the user clicks on the 'Load' button on the ui
   */
  public onLoad(): void {
    const params: {[key:string]: string} = {};
    params['type'] = this.selectedType.type;
    params['values'] = this.customReportForm.get('values')?.value;
    if ( this.selectedType.type === 'MetricAccumulateSimple' || this.selectedType.type === 'MetricAccumulateParam') {
      params['groupby'] = this.customReportForm.get('groupBy')?.value.type;
      params['aggregate'] = this.customReportForm.get('aggregate')?.value.type;
    }
    const type: ReportType = this.customReportForm.get('selectedDisplay')?.value.type;
    this.activitiesSelect.updateRouteParams(params);

    const c1 = new SessionbasedLineReportConfig('lsct');
    c1.toolTip = '`${personName} ${series.name}: ${d.value}`';
    c1.colors = ['steelblue', 'red', 'black', 'green', 'orange'];
    c1.yaxislabel = 'lsct_stage_power';
    c1.secondaryseries = ['BODYFAT'];

    const config = c1;
    params['display'] = this.customReportForm.get('selectedDisplay')?.value.type;

    this.configToRouteParams(params);

    this.host.customReport(params, type, config);
  }

  /**
   * Update the form based on the parameters from the route.
   * Also update `report´ based on route
   * @param params parameters from the route
   */
  public updateRouteForm(params: ParamMap, report: ReportInfo): void {
    const formData = {};

    this.activitiesSelect.updateRouteForm(params, formData);

    if ( params.has('type') ) {
      const ts = params.get('type');
      // TODO: make safe agains missing types / wrong parameters
      const type = this.types.filter(t => t.type === ts)[0];
      this.selectedType = type;
      this.customReportForm.get('selectedType')?.setValue(type);
    }

    if ( params.has('values') ) {
      this.customReportForm.get('values')?.setValue(params.get('values'));
    }

    if ( params.has('display') ) {
      const ts = params.get('display');
      const type = this.Displays.filter(t => t.type === ts)[0];
      this.customReportForm.get('selectedDisplay')?.setValue(type);

      report.type = type.type as ReportType;
    }

    // return formData;
  }

  private configToRouteParams(params: {[key:string]: string}): void {
    this.reportConfig.exposedVars.forEach(configVar => {
      const name = 'config_' + configVar.name;
      if ( this.customReportForm.get(name) != null ) {
        if ( this.customReportForm.get(name)?.value !== undefined ) {
          if ( configVar.type === 'DataTransformation' ) {
            if ( this.customReportForm.get(name)?.value != null) {
              console.log('Value for', configVar, this.customReportForm.get(name)?.value);
              params[name] = this.customReportForm.get(name)?.value.name;
            } else {
              console.log('No value for', name);
              delete params[name];
            }
          } else if ( this.customReportForm.get(name)?.value != null) {
            console.log('Value for', configVar, this.customReportForm.get(name)?.value);
            params[name] = this.customReportForm.get(name)?.value;
          } else {
            console.log('No value for', name);
            delete params[name];
          }
        } else {
          console.log('Not set', name, 'in form');
          delete params[name];
        }
      } else {
        console.log('No', name, 'in form');
        delete params[name];
      }
    });
  }

  /**
   * Fetch report config from route and set it in the form
   * @param params Route parameters
   * @returns config contained in the route
   */
  configFromRoute(params: ParamMap): ReportConfig {
    // Fetch display type from route
    const ts = params.get('display');
    // TODO: make code safe agains 'wrong' parameters
    const type = this.Displays.filter(t => t.type === ts)[0];

    console.log('creating report config for', {type});

    this.reportConfig = reportConfigFromType(type.type as ReportType);
    this.prepareConfigForm();

    params.keys.forEach(key => {
      if ( key.startsWith('config_') ) {
        const field = key.substring(7);
        console.log('Setting value for', field);

        const varType: ConfigVar | undefined = this.reportConfig.exposedVars.find(v => v.name === field);

        if ( varType !== undefined ) {
          switch ( varType.type ) {
            case 'string':
            case 'number':
              // console.log(field, 'is simple', typeof this.reportConfig[field]);
              (this.reportConfig as {[key:string]:any})[field] = params.get(key);
              this.customReportForm.get('config_' + field)?.setValue((this.reportConfig as {[key:string]:any})[field]);
              break;

            case 'list':
              // console.log(field, 'is array');
              (this.reportConfig as {[key:string]:any})[field] = params.get(key)?.split(',');
              this.customReportForm.get('config_' + field)?.setValue((this.reportConfig as {[key:string]:any})[field]);
              break;

            case 'DataTransformation':
              // console.log(field, 'is function');
              const name = params.get(key);
              const trans = DataTransformations.find(t => t.name === name);
              if ( trans ) {
                (this.reportConfig as {[key:string]:any})[field] = trans.fun;
              }
              // console.log({name}, {trans}, this.reportConfig[field]);
              this.customReportForm.get('config_' + field)?.setValue(trans);
              break;
          }
        } else {
          console.log(field, 'not in config');
        }
      }
    });

    return this.reportConfig;
  }

  private prepareConfigForm() {
    // set number of vars
    this.configVars = this.reportConfig.exposedVars.map(v => v.name);

    this.configReady = true;
    this.cd.detectChanges();

    console.log('renderChildren ', this.configVars, this.configHosts);
    this.configHosts.forEach((configHost: any, index: number) => {
      // get config variable
      const configVar = this.reportConfig.exposedVars[index];

      // create controll in form and set value
      let cvalue;
      let componentType;
      switch ( configVar.type ) {
        case 'string':
          componentType = TextConfigComponent;
          cvalue = (this.reportConfig as {[key:string]:any})[configVar.name];
          break;

        case 'number':
          componentType = NumberConfigComponent;
          cvalue =  (this.reportConfig as {[key:string]:any})[configVar.name];
          break;

        case 'list':
          componentType = ListConfigComponent;
          cvalue =  (this.reportConfig as {[key:string]:any})[configVar.name];
          break;

        case 'DataTransformation':
          componentType = DataTransformationConfigComponent;
          const fun =  (this.reportConfig as {[key:string]:any})[configVar.name];
          cvalue = DataTransformations.find(t => t.fun === fun);
          break;

        case 'AxisTimeScale':
          componentType = AxisTimeScaleConfigComponent;
          cvalue =  (this.reportConfig as {[key:string]:any})[configVar.name];
          break;

        default:
          console.log('Error no config var type', configVar.type, configVar.name);
      }
      this.customReportForm.addControl('config_' + configVar.name, new FormControl(cvalue));

      // create component
      const containerRef = configHost.viewContainerRef;
      containerRef.clear();
      const comp: ConfigComponent = containerRef.createComponent(componentType).instance;
      comp.name = configVar.name;
      comp.configName = 'config_' + comp.name;
      comp.parent = this;
      console.log(comp);
    });

    this.cd.detectChanges();
  }

  public getActivitiesFormData(): {} {
    return this.activitiesSelect.getFormData();
  }
}
