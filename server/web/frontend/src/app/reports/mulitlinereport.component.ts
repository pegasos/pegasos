import { Component } from '@angular/core';

import { AppService } from './../services/app.service';

import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';

import { ReportUserDatesSingle, DataSeries, Point } from './../shared/sample';
import { MultiLineReportConfig } from '../shared/reportconfig';
import { LinebasedReport } from './linebased.report';

@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class MultilineReportComponent extends LinebasedReport {
  public override config!: MultiLineReportConfig;
  override data!: ReportUserDatesSingle;

  protected override margin = {top: 10, right: 180, bottom: 70, left: 50};
  dates!: number[];

  constructor(app: AppService) {
    super(app);
  }

  public onDataReceived(data: ReportUserDatesSingle) {
    this.data = data;

    this.preprocessDataUserDates();

    this.dataToScreen();
  }

  private dataToScreen(): void {
    this.dates = this.data.dates.map(d => Date.parse('' + d));
    super.configureX(this.dates);

    let data_y1min = Number.MAX_VALUE;
    let data_y1max = Number.MIN_VALUE;
    let data_y2min = Number.MAX_VALUE;
    let data_y2max = Number.MIN_VALUE;

    // which series are secondary axis
    const secondary: boolean[] = [];

    const personColors = new Map<string, string>();
    let colidx = 0;
    this.data.series.forEach(series => {
      let sec = false;

      const sp = series.name.split('#');
      const seriesPerson = sp[0];
      const seriesName = sp[1];

      // check if this series should appear on secondary axis
      let idx = 0;
      for (idx = 0; idx < this.config.secondaryseries.length; idx++) {
        if ( this.config.secondaryseries[idx] === seriesName ) {
          sec = true;
          this.secondary_used = true;
          break;
        }
      }
      secondary.push(sec);

      // compute maxima / minima for axis
      if ( !sec ) {
        if ( this.config.axis_operation ) {
          const op = this.config.axis_operation;
          series.data = series.data.map(p => p === null ? 0 : op(p));
        }
        const exty = d3Array.extent(series.data, function(d) { return d; }) as [number, number];
        data_y1min = d3Array.min([exty[0], data_y1min]) as number;
        data_y1max = d3Array.max([exty[1], data_y1max]) as number;
      } else {
        if ( this.config.secondary_axis_operation ) {
          const op = this.config.secondary_axis_operation;
          series.data = series.data.map(p => p === null ? 0 : op(p));
        }
        const exty = d3Array.extent(series.data, function(d) { return d; }) as [number, number];
        data_y2min = d3Array.min([exty[0], data_y2min]) as number;
        data_y2max = d3Array.max([exty[1], data_y2max]) as number;
      }

      if ( !personColors.has(seriesPerson) ) {
        personColors.set(seriesPerson, this.config.colors[colidx++ % this.config.colors.length]);
      }
    });
    this.personColors = personColors;
    console.log({'secondary': secondary});

    super.configureYAxis(data_y1min, data_y1max, data_y2min, data_y2max);
    // console.log({data_xmin, data_xmax}, {data_y1min, data_y1max}, {data_y2min, data_y2max});

    super.setup();

    let i = 0;
    this.data.series.forEach(series => this.addSeries(series, secondary[i++]));

    this.addLegendRight();
  }

  private addSeries(series: DataSeries<number>, secondary: boolean): void {
    const person = series.name.split('#')[0];
    const seriesName = series.name.split('#')[1];
    const linecolor = this.personColors.get(person);
    const linepattern = this.seriesPatterns.get(seriesName);
    console.log('Adding series', series.name, linecolor, linepattern, secondary);

    this.lineShowing.set(seriesName, true);

    // create local references for the line function
    const x = this.x;
    const y = secondary ? this.y2 : this.y1;

    const line = d3Shape.line()
      .x(function(d: any) { return x(d.x); })
      .y(function(d: any) { return y(d.y); });

    let id = 1;

    const points: any[] = [];
    this.dates.forEach((date: number, index: number) => {
      // result[series.data[index]] = field;
      if ( series.data[index] != null ) {
        points.push({'x': date, 'y': series.data[index], 'id': id});
        id++;
      }
    });

    const hash = this.nameHash(seriesName);
    this.svg.append('path')
          //.datum(points)
          .attr('class', 'line-' + hash)
          .attr('fill', 'none')
          .attr('stroke', linecolor)
          .attr('stroke-linejoin', 'round')
          .attr('stroke-linecap', 'round')
          .attr('stroke-width', 1.5)
          .style('stroke-dasharray', (linepattern))
          .attr('d', line(points));

    const helper = this.getHelper(this.svg, x, y, this.config, this.personResolver);
    console.log(helper);

    this.svg.selectAll('myCircle')
          .data(points)
          .enter().append('circle') // Uses the enter().append() method
            .attr('class', 'point-' + hash)
            .attr('fill', linecolor)
            .attr('stroke', 'none')
            .attr('cx', function(d: any) { return x(d.x); })
            .attr('cy', function(d: any) { return y(d.y); })
            .attr('r', helper.radius)
            // .on('click', function(d, i) {
            //   helper.onSessionDetails(d.session_id);
            // })
            .on('mouseover', function(event:any, datum:any) { helper.handleMouseOver(datum, points.indexOf(datum), +person, series, event.target, event); })
            .on('mouseout', function(event:any, datum:any) { helper.handleMouseOut(datum, points.indexOf(datum), event.target); });

           /*.on('mouseover', function(event:any, datum:any) {
              const index = series.start.indexOf(datum);
              helper.handleMouseOver(datum, index, +series.person, series, event.target, event);
            })
            // datum == start
            .on('mouseleave', function(event:any, datum:any) {
              const index = series.start.indexOf(datum);
              helper.handleMouseOut(datum, index, event.target); })
            .on('mousemove', function(event:any, datum:any) {
              // datum == start
              const index = series.start.indexOf(datum);
              helper.handleMouseMove(event, event.target, datum, index);
            });*/
  }

  protected getDates(): number[] {
    return this.dates;
  }

  protected getValues() {
    // this.seriesNames.forEach(name =>  header.push(name));
    if (!this.multiPerson) {
      const values: Array<Array<Point>> = [];
      // for all our series names
      this.seriesNames.forEach(name => {
        // find the series of our one user
        const series = this.data.series.find(s => s.name === this.data.users[0] + '#' + name) as DataSeries<number>;
        const points: Array<Point> = []
        // and add all points
        this.dates.forEach((date: number, index: number) => {
          if ( series.data[index] != null ) {
            points.push({x: date, y: series.data[index]});
          }
        })
        values.push(points);
      });
      return values;
    } else {
      const values: Array<Array<Point>> = [];
      // for each user
      this.data.users.forEach(userId => {
        // and all series names
        this.seriesNames.forEach(name => {
          const series = this.data.series.find(s => s.name === userId + '#' + name) as DataSeries<number>;
          const points: Array<Point> = []
          // add all points
          this.dates.forEach((date: number, index: number) => {
            if ( series.data[index] != null ) {
              points.push({x: date, y: series.data[index]});
            }
          })
          values.push(points);
        });
      });
      return values;
    }
  }
}
