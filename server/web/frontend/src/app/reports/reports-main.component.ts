import { HttpParams } from '@angular/common/http';
import { PersonResolver } from './../shared/personresolver';
import { Component, OnInit, ChangeDetectorRef, ViewChild, ComponentFactoryResolver,
  ComponentFactory, ViewChildren, AfterViewInit, QueryList, Type } from '@angular/core';
import { ActivatedRoute, Router, ParamMap, convertToParamMap } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { faFileCsv, faFilePdf } from '@fortawesome/free-solid-svg-icons';

import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas';

import { AppService } from './../services/app.service';

import { Person } from '../shared/person';
import { ReportFilter } from './../shared/sample';

import { ReportHostDirective } from './reporthost.directive';
import { ReportInfo, ReportType, ReportConfig } from './../shared/reportconfig';
import { REPORTS } from './../shared/reports';
import { ReportComponent } from './report.component';
import { MultilineReportComponent } from './mulitlinereport.component';
import { BarsReportComponent } from './barsreport.component';
import { CurvedLineReportComponent } from './curvedlinereport.component';
import { SessionbasedLineReportComponent } from './sessionbasedline.component';
import { CustomReportBuilderComponent } from './reports-main-custombuild.component';
import { PersonSelectComponent } from '../actioncomponents/person-select/personselect.component';
import { TimePeriodComponent } from '../actioncomponents/time-period/timeperiod.component';

class ReportFactory {
  public static factoryForType(type: ReportType): Type<ReportComponent> {
    switch ( type ) {
      case 'Bars':
        return BarsReportComponent;
      case 'Curve':
        return CurvedLineReportComponent;
      case 'Multiline':
        return MultilineReportComponent;
      case 'SessionbasedLines':
        return SessionbasedLineReportComponent;
      default:
        return SessionbasedLineReportComponent;
    }
  }
}

export const REPORTCOMPONENTS = [MultilineReportComponent, BarsReportComponent, CurvedLineReportComponent,
  SessionbasedLineReportComponent];

const custom = new ReportInfo('_custom_', '_custom_', 'Multiline', 0, new ReportConfig('_custom_fillme_'));
custom.translatedName = 'Custom';

@Component({
  selector: 'app-reports-main',
  templateUrl: './reports-main.component.html',
  styleUrls: ['./reports-main.component.scss']
})
export class ReportsMainComponent implements OnInit, AfterViewInit, PersonResolver {

  public yearRange: string = (new Date().getFullYear() - 10) + ':' + (new Date().getFullYear() + 1);

  public pdfIcon = faFilePdf;
  public csvIcon = faFileCsv;

  public reports: ReportInfo[] = REPORTS;

  private report!: ReportInfo;
  protected hasReport = false;

  public myForm!: FormGroup;

  @ViewChild('timePeriod') protected timePeriod!: TimePeriodComponent;
  tWa = new Date();

  @ViewChild('personSelect') protected personSelect!: PersonSelectComponent;

  /**
   * Host of the report
   */
  @ViewChild(ReportHostDirective) reportHost!: any;
  @ViewChildren(ReportHostDirective) reportHosts!: QueryList<ReportHostDirective>;

  private params!: {[key:string]: any};

  /**
   * indicator If the form for custom reports should be shown
   */
  showCustom!: boolean;
  @ViewChild('customBuilder') protected customBuilder!: CustomReportBuilderComponent;
  reportInstance!: ReportComponent;

  constructor(private app: AppService,
    private route: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    /*private fileSaverService: FileSaverService*/
    ) {
    this.reports.forEach(report => report.translatedName = this.app.translateString(report.name));
    this.reports.push(custom);
  }

  ngOnInit() {
    this.report = this.reports[0];
    this.tWa = new Date();
    this.tWa.setDate(this.tWa.getDate() - this.report.defaultWeeks * 7);

    this.myForm = this.fb.group({
      reportselect: new FormControl({value: this.report, disabled: false}),
      persons: new FormControl({value: [], disabled: true}),
    });

    this.app.startLoading();
  }

  ngAfterViewInit() {
  }

  private updateReport(params: ParamMap): void {
    console.log('updateReport', {params}, this.report);

    this.timePeriod.update(params);
    this.personSelect.updateRouteForm(params);

    if ( params.has('report') ) {
      this.hasReport = true;
      this.reportHosts.first?.viewContainerRef.detach();
      this.reportFromName(params.get('report') as string);
      if ( this.report !== custom ) {
        this.updateReportConfig();
      }

      // set the selected the report
      this.myForm.get('reportselect')?.setValue(this.report);

      if ( this.report === custom ) {
        this.showCustom = true;
        this.cd.detectChanges();
        this.customBuilder.updateRouteForm(params, this.report);
        this.report.config = this.customBuilder.configFromRoute(params);
      }

      this.params = {};
      params.keys.forEach(key => (this.params as {[key:string]:any})[key] = params.get(key));

      this.reportToScreen();
    }
  }

  public onPersonsReceived(): void {
    this.cd.detectChanges();
    this.personSelect.setDisabledState(false);

    this.onAllDataRecieved();
  }

  private onAllDataRecieved(): void {
    this.route.paramMap.subscribe((params: ParamMap) => this.updateReport(params));
    this.cd.detectChanges();
    this.app.translateTree('reportpage');
    this.app.stopLoading();
  }

  /**
   * Callback when the user selects a report from the menu. If start date has not been changed by the user it will be
   * adjusted according to the configured default period
   * @param event event
   */
  onSelectReport(event: any) {
    const item: ReportInfo = event.value;
    console.log('onSelectReport', item);
    if ( !this.timePeriod.touched() ) {
      const tWa = new Date();
      tWa.setDate(tWa.getDate() - item.defaultWeeks * 7);
      this.timePeriod.setStart(tWa)
      this.report = item;
      this.updateReportConfig();
    } else {
      // In this case we do not want to update the UI based on previous values as this might confuse the user.
      // rather we erase the old config
      this.report = item;

      // check if there is a config (currently only _custom_ does not have a config )
      if ( this.report.config !== undefined ) {
        this.report.config.end = undefined;
        this.report.config.start = undefined;
      }
    }

    if ( this.report === custom ) {
      this.showCustom = true;
    } else {
      this.showCustom = false;
    }
  }

  /**
   * Show the report as configured by the form. Encode selections as url so that it can be refreshed or bookmarked
   */
  public OnShowReport(): void {
    if ( this.report !== undefined ) {
      this.params = {};
      this.setParamsFromForm();
      // this.updateReportConfig();
      // console.log({params});
      this.router.navigate(['/reports', this.report.action, this.params]);
      // console.log('after navigate');
    }
  }

  private setParamsFromForm(): void {
    this.timePeriod.updateRouteParams(this.params);
    this.personSelect.updateRouteParams(this.params);
  }

  private updateReportConfig(): void {
    this.report.config.start = this.timePeriod.getStartIfTouched();
    this.report.config.end = this.timePeriod.getEndIfTouched();

    this.report.config.persons = this.personSelect.getRouteParam();
  }

  private reportFromName(reportName: string): void {
    const report = this.reports.filter(r => r.action === reportName)[0];
    console.log('reportFromName', report);
    this.report = report;
  }

  /**
   * Put the report on the screen
   */
  private reportToScreen(): void {
    this.cd.detectChanges();
    console.log(this.reportHosts, this.reportHost, this.report);
    this.reportHosts.forEach((reportHost, index) => {
      const containerRef = reportHost.viewContainerRef;
      containerRef.clear();
      console.log(this.report.type);
      const newComponent = containerRef.createComponent(ReportFactory.factoryForType(this.report.type));
      (<ReportComponent> newComponent.instance).config = this.report.config;
      (<ReportComponent> newComponent.instance).personResolver = this;
      if ( this.report === custom ) {
        this.customReportToScreen((<ReportComponent> newComponent.instance));
      }
      console.log('added component', newComponent);
      this.reportInstance = (<ReportComponent> newComponent.instance);
    });
    this.app.setTitle(this.report.translatedName);
    this.cd.detectChanges();
  }

  private customReportToScreen(report: ReportComponent) {
    report.config.fetchData = false;
    let pars = new HttpParams();
    const keys = ['type', 'values', 'groupby', 'aggregate', 'start', 'end'];
    keys.forEach(key => {
      if ( key in this.params ) {
        pars = pars.append(key, (this.params as {[key:string]:any})[key]);
      }
    });

    // We need always a start and end date
    if ( !('start' in this.params) ) {
      pars = pars.append('start', this.timePeriod.getStart());
    }
    if ( !('end' in this.params ) ) {
      pars = pars.append('end', this.timePeriod.getEnd());
    }

    const subscr = ('activities' in this.params) ?
      // extract selected activities and send them as body of the request
      this.app.getResourcePost('v1/reports/get/custom', {
        'sessionFilter': (this.customBuilder.getActivitiesFormData() as {[key:string]:any})['activitytypes']
      } as ReportFilter, pars) :
      // request without a body
      this.app.getResource('v1/reports/get/custom', pars);

    subscr.subscribe(data => {
        if ( report !== null ) {
          report.onDataReceived(data);
          this.cd.detectChanges();
        }

        this.app.stopLoading();
      },
      () => this.app.stopLoading());
  }

  /**
   * user has clicked on 'custom report'
   * @param params configuration (i.e., display type, selected values, aggregation, ...) of the report
   * @param type Type of the selected display (i.e., Uservalues, session lines, ...)
   * @param config initial configuration for the report
   */
  public customReport(params: {}, type: ReportType, config: ReportConfig): void {
    this.params = params;
    this.setParamsFromForm();
    console.log({params});

    custom.config = config;
    custom.type = type;

    this.router.navigate(['/reports', this.report.action, params]);
  }

  /**
   * Get a person (recieved from server)
   * @param id ID of the person
   */
  public getPerson(id: number): Person | undefined {
    return this.personSelect.getPerson(id);
  }

  public onSave(): void {
    this.app.startLoading();
    console.log('onSave');
    const A4Width = 295;
    const margin = { left: 15, right : 15 }

    const data = document.getElementById('report');
    if (!data) return;
    html2canvas(data, {
      allowTaint: true,
        useCORS: true}).then(canvas => {
      // Few necessary setting options
      const imgWidth = A4Width - margin.left - margin.right;
      const pageHeight = 208;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jsPDF('l', 'mm', 'a4'); // A4 size page of PDF
      pdf.addImage(contentDataURL, 'PNG',
        // xOffset
        margin.left,
        // yOffset
        10,
        // width // xEnd
        imgWidth, // imgWidth + margin.left,
        // height // yEnd
        imgHeight, // imgHeight + 10,
        undefined, 'FAST', 0);
      this.app.stopLoading();
      pdf.save('Report.pdf'); // Generated PDF
    });
  }

  public onSaveData(): void {
    this.reportInstance.downloadFile();
  }
}
