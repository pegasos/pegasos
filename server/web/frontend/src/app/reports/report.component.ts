import { PersonResolver } from './../shared/personresolver';
import { HttpParams } from '@angular/common/http';

import { AppService } from './../services/app.service';
import { Component, AfterViewInit } from '@angular/core';

import * as d3 from 'd3-selection';

import {responsivfy} from '../shared/responsify';
import { ReportConfig } from '../shared/reportconfig';

@Component({template: 'report.component.html'})
export abstract class ReportComponent implements AfterViewInit {
  // data: any;
  config!: ReportConfig;

  protected svg!: any;

  protected margin = {top: 10, right: 60, bottom: 70, left: 50};
  /**
   * Total width of the report svg element
   */
  protected width!: number;
  /**
   * Total height of the report svg element
   */
  protected height!: number;
  /**
   * Available drawing space width
   */
  protected innerWidth!: number;
  /**
   * Available drawing space height
   */
  protected innerHeight!: number;

  /**
   * Host of the component. ie reportsmaincomponent in which this report is placed
   */
  public personResolver!: PersonResolver;

  constructor(protected app: AppService) {
  }

  ngAfterViewInit(): void {
    /*console.log('ReportComponent::ngAfterViewInit', this.config.name,
      {config: this.config}, this.config.end, this.config.end !== undefined);*/

    this.initSvg();
    let params: HttpParams = new HttpParams();
    if ( this.config.start !== undefined ) {
      params = params.set('start', this.config.start);
    }
    if ( this.config.end !== undefined ) {
      // console.log('setting end', params.set('end', this.config.end), params);
      params = params.set('end', this.config.end);
      console.log(params);
    }
    if ( this.config.persons !== undefined && this.config.persons !== '' ) {
      params = params.set('pers', this.config.persons);
    }
    if ( this.config.fetchData ) {
      this.app.getResource('v1/reports/get/' + this.config.name, params)
        .subscribe(data => this.onDataReceived(data));
    }
  }

  private initSvg() {
    this.height = 300;
    this.width = 800;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.innerWidth = this.width - this.margin.left - this.margin.right;

    this.svg = d3.select('#' + this.config.id)
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .call(responsivfy) // this is all it takes to make the chart responsive
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.svg.append('rect')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('fill', 'white');
  }

  public abstract onDataReceived(data: any): void;

  /**
   * TODO: write documentation
   */
  protected abstract getDownloadData(): any[][];

  public downloadFile(): void {
    const data: any[][] = this.getDownloadData();

    const blob = new Blob([data.join('\r\n')], { type: 'text/csv' });
    // console.log({data});
    const url = window.URL.createObjectURL(blob);

    // create a fake element
    var anchor = document.createElement("a");
    anchor.download = "ReportData.csv";
    anchor.href = url;
    anchor.click();
    anchor.remove();
  }

  protected getHelper(svg: any, x: number, y: number, config: ReportConfig, personResolver: PersonResolver, radius = 3) {
    const h = {
      myhmsformat: function (num: number) {
        const hour = Math.floor( num / 3600 );
        const m = Math.floor((num - hour * 3600) / 60 );
        const s = num - (hour * 3600 + m * 60);
        return (hour > 0 ? (( hour < 10 ? '0' + hour : hour ) + ':') : '') + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s );
      },

      onSessionDetails: (sessionid: Number) => {
        // console.log('Open session details', sessionid);
        window.open(this.app.baseURL + '/tagebuch/actd/' + sessionid);
      },

      // Create Event Handlers for mouse
      handleMouseOver: function(value: any, i: number, personId: number, series: any, element: any, event: any) {  // Add interactivity
        const myhmsformat = function (num: number) {
          const hour = Math.floor( num / 3600 );
          const m = Math.floor((num - hour * 3600) / 60 );
          const s = num - (hour * 3600 + m * 60);
          return (hour > 0 ? (( hour < 10 ? '0' + hour : hour ) + ':') : '') + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s );
        };
        // Use D3 to select element, change color and size
        d3.select(element).attr('r', this.radius * 1.5);
        // console.log(element);

        // eslint-disable-next-line unused
        const personName = personResolver.getPerson(personId)?.name;
        // eslint-disable-next-line no-eval
        const text = eval(config.toolTip);

        // console.log('Create tooltip:', text, 't' + element.id + '-' + i);
        if ( text !== undefined ) {
        // Specify where to put label of text
        d3.select('body').append('div')
          .attr('id', 't' + element.id + '-' + i)  // Create an id for text so we can select it later for removing on mouseout
          .attr('class', 'tooltip')
          .style('position', 'absolute')
          .style('left', (event.pageX + 9) + 'px')
          .style('top', (event.pageY) + 'px')
          .style('background-color', 'white')
          .style('font-size', '10px')
          .style('pointer-events', 'none')
          .style('border', 'solid')
          .style('border-width', '2px')
          .style('border-radius', '5px')
          .style('opacity', 1)
          .style('padding', '5px')
          .html(text);
          /*.text(function() {
            return [d.x, d.y];  // Value of the text
          });*/
        }
      },

      handleMouseOut: function(d: any, i: number, element: any) {
        // Use D3 to select element, change color back to normal
        d3.select(element).attr('r', this.radius);

        // Select text by id and then remove
        d3.select('#t' + element.id + '-' + i).remove();  // Remove text location
      },

      handleMouseMove: function(event: any, element: any, d: any, i: number) {
        d3.select('#t' + element.id + '-' + i)
          .style('left', (event.pageX + 9) + 'px')
          .style('top', (event.pageY) + 'px');
      },

      app: this.app,
      radius: radius,
      svg: null,
      x: x,
      y: y,
      config: config,
      personResolver: personResolver
    };
    h.app = this.app;
    h.svg = svg;
    h.x = x;
    h.y = y;
    h.config = config;
    h.personResolver = personResolver;
    return h;
  }

  protected nameHash(name: string): number {
    var hash = 0,
    i, chr;
    if (name.length === 0) return hash;
    for (i = 0; i < name.length; i++) {
      chr = name.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }
}

export class Point {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
