import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { ChipsModule } from 'primeng/chips';
import { InputNumberModule } from 'primeng/inputnumber';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ActionComponentsModule } from '../actioncomponents/action.components.module';

import { REPORTCOMPONENTS, ReportsMainComponent } from './reports-main.component';
import { ReportHostDirective } from './reporthost.directive';
import { AxisTimeScaleConfigComponent, ConfigHostDirective, CustomReportBuilderComponent, DataTransformationConfigComponent, ListConfigComponent, NumberConfigComponent, TextConfigComponent } from './reports-main-custombuild.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        TableModule, CalendarModule, DropdownModule, ChipsModule, InputNumberModule,
        FontAwesomeModule,
        ActionComponentsModule,
    ],
    declarations: [
        ReportsMainComponent,
        CustomReportBuilderComponent, NumberConfigComponent, TextConfigComponent, ListConfigComponent, DataTransformationConfigComponent, AxisTimeScaleConfigComponent,
        ConfigHostDirective,
        ReportHostDirective,
        REPORTCOMPONENTS,
    ],
    exports: [
        ReportHostDirective,
    ],
    providers: [],
    bootstrap: []
})
export class ReportsCommonModule {
}
