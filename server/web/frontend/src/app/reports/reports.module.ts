import { NgModule } from '@angular/core';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsCommonModule } from './reports-common.module';

@NgModule({
    imports: [
        ReportsRoutingModule,
        ReportsCommonModule,
    ],
    declarations: [],
    providers: [],
    bootstrap: []
})
export class ReportsModule {
}
