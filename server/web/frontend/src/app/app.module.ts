import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxUiLoaderConfig, NgxUiLoaderModule } from 'ngx-ui-loader';
import { MenubarModule } from 'primeng/menubar';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';

import { DialogModule } from './dialog/dialog.module';
import { ActionComponentsModule } from './actioncomponents/action.components.module';
import { PegasosCommonModule } from './common/common-module';

import { AppService } from './services/app.service';
import { MenuService } from './services/menu.service';
import { TranslationService } from './services/translation.service';
import { LoginService } from './login/login.service';

import { AppRoutingModule } from './app-routing/app-routing.module';

import { LoginInterceptor } from './loggedin.interceptor';
import { UserService } from './services/user.service';

import { DECLARATIONS, ENTRYCOMPONENTS } from './addedcomponents';

@NgModule({
    imports: [
        CommonModule,
        ActionComponentsModule
    ],
    declarations: [
        DECLARATIONS,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [
        DECLARATIONS,
    ]
})
export class AddedComponentsModule {
}

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  // fgsType: SPINNER.chasingDots, // foreground spinner type
  pbColor: 'red',
  pbThickness: 10,    // progress bar thickness
  hasProgressBar: true,

  blur: 10,
  delay: 0,          // Show loader immediately
  fastFadeOut: true, // Faster fade out
  fgsColor: 'red',   // Foreground spinner color
  logoSize: 420,
  logoUrl: 'assets/img/splash.png',
};

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        LoginComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule, ReactiveFormsModule,
        NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
        MenubarModule,
        DialogModule,
        ActionComponentsModule,
        AddedComponentsModule,
        PegasosCommonModule,
    ],
    providers: [AppService, LoginService, MenuService, TranslationService, UserService,
        { provide: HTTP_INTERCEPTORS, useClass: LoginInterceptor, multi: true }],
    bootstrap: [AppComponent]
})
export class AppModule {
}
