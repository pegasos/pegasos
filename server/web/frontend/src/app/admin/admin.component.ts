import {
  OnInit, Component, ChangeDetectorRef, ComponentRef, QueryList, ViewChildren, Directive, ViewContainerRef,
  ComponentFactoryResolver,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AppService } from './../services/app.service';
import { AdminToolComponent } from './admin.tool.component';

import { getMenu } from './menu';
import { ToolFactory } from './ToolFactory';

@Directive({
  selector: '[appToolHost]',
})
export class ToolsHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminPanelComponent implements OnInit {
  menuItems!: MenuItem[];

  /**
   * True if an action is currently running
   */
  public loading!: boolean;

  /**
   * Wheter or not a tool is currently shown, i.e. in the url
   */
  private toolShown = false;

  /**
   * Host of the tool
   */
  @ViewChildren(ToolsHostDirective) private toolHost!: QueryList<ToolsHostDirective>;
  tool!: AdminToolComponent;

  constructor(
    private app: AppService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private route: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.menuItems = getMenu(this);

    this.cd.detectChanges();
    this.app.translateContent();

    this.route.paramMap.subscribe((params: ParamMap) => this.showToolFromRoute(params));
  }

  /**
   * Callback for the menu. This method will update the route
   * @param name tool which should be displayed (as per ToolFactory)
   */
  public showTool(name: string): void {
    const urlTree = this.router.parseUrl(this.router.url);
    const urlWithoutParams = this.toolShown ?
      urlTree.root.children['primary'].segments.slice(0, -1).map(it => it.path).join('/') :
      urlTree.root.children['primary'].segments.map(it => it.path).join('/');

    this.router.navigate([urlWithoutParams, name]);
  }

  private showToolFromRoute(params: ParamMap): void {
    if (params.has('tool')) {
      this.toolShown = true;
      this.toolToScreen(params.get('tool'));
    } else {
      this.toolShown = false;
    }
  }

  /**
   * Put a tool to the screen. Does not update the route
   * @param name tool to display (as per ToolFactory)
   */
  public toolToScreen(name: string|null): void {
    if( name === null ) return;
    const toolHost = this.toolHost.first;
    const containerRef = toolHost.viewContainerRef;
    containerRef.clear();

    const newComponent: ComponentRef<AdminToolComponent> = containerRef.createComponent(
      ToolFactory.factoryForType(this.componentFactoryResolver, name));

    this.tool = (<AdminToolComponent>newComponent.instance);

    this.cd.detectChanges();
  }
}
