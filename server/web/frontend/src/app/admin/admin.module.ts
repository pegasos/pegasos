import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MenubarModule } from 'primeng/menubar';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';
import { DropdownModule } from 'primeng/dropdown';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ActionComponentsModule } from '../actioncomponents/action.components.module';
import { SystemPersonSelectComponent } from './system-person-select.component';
import { AdminPanelComponent, ToolsHostDirective } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ADMINCOMPONENTS } from './ToolFactory';

@NgModule({
    imports: [
        AdminRoutingModule,
        CommonModule,
        FormsModule, ReactiveFormsModule,
        TableModule, MenubarModule, FileUploadModule, ProgressBarModule, DropdownModule,
        FontAwesomeModule,
        ActionComponentsModule,
    ],
    declarations: [
        AdminPanelComponent,
        ToolsHostDirective,
        SystemPersonSelectComponent,
        ADMINCOMPONENTS,
    ],
    providers: [],
    bootstrap: []
})
export class AdminModule {
  public constructor(library: FaIconLibrary) {
    library.addIcons(faTrash);
    library.addIcons(faTimes);
  }
}
