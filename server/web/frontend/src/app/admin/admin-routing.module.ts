import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuGuard } from '../app-routing/menu.guard';
import { AdminPanelComponent } from './admin.component';

export const routes: Routes = [
  { path: '', component: AdminPanelComponent },
  { path: ':tool', component: AdminPanelComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
