// Folder: admin;
// Generated file. Do not edit


import { RunPostProcessorComponent } from '../admin/tools/run.postprocessor.component';
import { AddUserComponent } from '../admin/tools/add.user.component';
import { RoleEditComponent } from '../admin/tools/role.edit.component';
import { AdminToolComponent } from './admin.tool.component';
import { ComponentFactoryResolver, ComponentFactory } from '@angular/core';
import { FilesImportComponent } from '../admin/tools/files.import.component';
import { FixEndTimeComponent } from './tools/fixendtime.component';

export const ADMINCOMPONENTS = [RunPostProcessorComponent, AddUserComponent, RoleEditComponent, FilesImportComponent, FixEndTimeComponent];

export class ToolFactory {
  public static factoryForType(componentFactoryResolver: ComponentFactoryResolver, name: string): ComponentFactory<AdminToolComponent> {
    switch ( name ) {
      case 'RunPostProcessorComponent':
        return componentFactoryResolver.resolveComponentFactory(RunPostProcessorComponent);
      case 'AddUserComponent':
        return componentFactoryResolver.resolveComponentFactory(AddUserComponent);
      case 'RoleEditComponent':
        return componentFactoryResolver.resolveComponentFactory(RoleEditComponent);
      case 'FilesImportComponent':
        return componentFactoryResolver.resolveComponentFactory(FilesImportComponent);
      case 'FixEndTime':
        return componentFactoryResolver.resolveComponentFactory(FixEndTimeComponent);
      default:
        return componentFactoryResolver.resolveComponentFactory(RunPostProcessorComponent);
    }
  }
}
// End of generated class
