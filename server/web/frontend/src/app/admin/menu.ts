// Folder: admin;
// Generated file. Do not edit


import { AdminPanelComponent } from './admin.component';

export function getMenu(panel: AdminPanelComponent) {
  return [
  {
    label: 'User',
    items : [
    {
      label: 'Add',
      command: () => panel.showTool('AddUserComponent'),
    },
    {
      label: 'Edit Roles',
      command: () => panel.showTool('RoleEditComponent'),
    },
    {
      label: 'Upload Files',
      command: () => panel.showTool('FilesImportComponent'),
    },
    ],
  },
  {
    label: 'Activities',
    items : [
    {
      label: 'Run PostProcessor',
      command: () => panel.showTool('RunPostProcessorComponent'),
    },
    {
      label: 'Fix End-Time',
      command: () => panel.showTool('FixEndTime'),
    },
    ],
  },
];
}
// End of generated class
