import { AppService } from './../services/app.service';
import { Component, OnInit, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UserDetails } from '../shared/sample';

@Component({
  selector: 'app-system-person-select',
  templateUrl: `./system-person-select.component.html`,
  styleUrls: [`./system-person-select.component.scss`],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SystemPersonSelectComponent),
      multi: true
    }
  ]
})
export class SystemPersonSelectComponent implements OnInit, ControlValueAccessor {
  @Input() request = true;
  @Output() personsReceived = new EventEmitter<UserDetails[]>();
  @Output() onChange = new EventEmitter<UserDetails>();

  protected users!: UserDetails[];

  protected selectedUser!: any;

  constructor(private app: AppService) {}

  ngOnInit(): void {
    if (this.request) {
      this.app.getResource('v1/user/all').subscribe(
        data => this.onUsersReceived(data),
        err => this.app.handleReceiveError(err));
    }
  }

  protected onUsersReceived(data: UserDetails[]): void {
    this.users = data;

    this.personsReceived.emit(this.users);
  }

  protected valueChanged(event: any): void {
    this.onChange.emit(event.value);
  }

  writeValue(value: UserDetails): void {
    console.log('writeValue', value);
    this.selectedUser = value;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  getPerson() {
    return this.selectedUser;
  }
}
