// admin-module: entry: 'User' -> 'Add'

import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PersonSelectComponent } from '../../actioncomponents/person-select/personselect.component';
import { AppService } from '../../services/app.service';
import { TeamService } from '../../services/team.service';
import { PersonSelectEntry } from '../../shared/person';
import { TeamWithGroups } from '../../shared/sample';
import { AdminToolComponent } from '../admin.tool.component';

@Component({
  selector: 'app-admin-add-user',
  templateUrl: './add.user.component.html',
  // styleUrls: ['./admin.component.scss']
})
export class AddUserComponent extends AdminToolComponent implements OnInit {
  registerForm!: FormGroup;

  /**
   * True if an action is currently running
   */
  public loading: boolean;

  /**
   * whether the form has been submitted
   */
  public submitted: boolean;
  personsAvailable: PersonSelectEntry[] = [];
  personsMap: any;

  constructor(
    private app: AppService,
    private formBuilder: FormBuilder,
    private teamsService: TeamService,
    private cd: ChangeDetectorRef) {
    super();
    this.loading = false;
    this.submitted = false;
  }

  ngOnInit(): void {
    this.app.startLoading();

    this.registerForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      groupSelect: new FormControl(),
    });

    this.app.getResource('v1/teams/allgroups').subscribe(data => this.onGroupsReceived(data), err => this.app.handleReceiveError(err));

    this.cd.detectChanges();
    this.app.translateContent();
  }

  private onGroupsReceived(data: TeamWithGroups[]): void {
    this.app.stopLoading();
    const h = PersonSelectComponent.setDataTG(data);
    this.personsAvailable = h.available;
    this.personsMap = h.map;
    this.cd.detectChanges();
  }

  public register(): void {
    this.submitted = true;

    if (this.registerForm.invalid) {
      this.cd.detectChanges();
      this.app.translateContent();
      return;
    } else {
      this.loading = true;

      const x = this.registerForm.value;
      const xx = this.registerForm.get('groupSelect')?.value;
      if ( xx !== undefined && xx !== null && xx.length > 0 ) {
        x['groupId'] = xx[0].id.substring(2);
      }

      this.app.getResourcePost('v1/users/add', x).subscribe(
        (data) => {
          if ( data.message === 'Created' ) {
            this.teamsService.invalidateMyGroupMemebers();
            alert('Created');
          } else if ( data.message === 'Fail' ) {
            if ( data.error === 'user_exists' ) {
              alert('User already in db or in temp registration');
            }
          }
          this.loading = false;
        },
        (err: HttpErrorResponse) => {
          if ( err.message === 'user_exists' ) {
            alert('User already in db or in temp registration');
          } else {
            alert('Other error' + err.message);
          }
          this.loading = false;
          // this.submitted = false;
        }
      );
    }
  }
}
