// admin-module: entry: 'User' -> 'Upload Files', order: 3

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { concat, Observable } from 'rxjs';
import { FileUpload } from 'primeng/fileupload';
import { AdminToolComponent } from '../admin.tool.component';
import { TaskMonitorComponent } from './../../actioncomponents/task-monitor/taskmonitor.component';
import { TaskStatus, UserDetails } from '../../shared/sample';
import { AppService, Upload } from '../../services/app.service';
import { GenericResponse } from './../../shared/sample';
import { SystemPersonSelectComponent } from '../system-person-select.component';

@Component({
  selector: 'app-admin-import-files',
  templateUrl: './files.import.component.html',
  // styleUrls: ['./admin.component.scss']
})
export class FilesImportComponent extends AdminToolComponent implements OnInit, OnDestroy {
  selectedUser!: UserDetails;

  public currentUser!: UserDetails;
  @ViewChild('persons') protected personSelect!: SystemPersonSelectComponent;

  // @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files: Upload[] = [];

  private importId!: string;
  subscr: any;

  @ViewChild('taskMonitor') taskMonitor!: TaskMonitorComponent;
  importRunning = false;

  constructor(
    private app: AppService,
    private cd: ChangeDetectorRef) {
    super();
    // this.loading = false;
  }

  ngOnInit(): void {
    this.app.startLoading();

    this.cd.detectChanges();
    this.app.translateContent();
  }

  public onUsersReceived(data: UserDetails[]): void {
    this.app.stopLoading();
  }

  /**
   * User has selected a person from the list
   * @param user selected person
   */
  protected onUserSelect(user: UserDetails): void {
    this.selectedUser = user;
  }

  /**
   * Callback when user clicks button to use the selected user
   */
  public onSelectPerson(): void {
    this.currentUser = this.selectedUser;
  }

  onUpload(event: any, uploader: FileUpload) {
    this.app.putResource('v1/user/createImport/' + this.currentUser.id).subscribe((res: GenericResponse) => {
      if ( res.message === 'Fail' ) {
        alert('Creating import failed');
      } else {
        this.importId = res.message;
        this.uploadFiles(event.files);
        uploader.clear();
        // this.cd.detectChanges();
      }
    });
  }

  private uploadFiles(files: any): void {
    let res = concat() as Observable<Upload>;

    for (const file of files) {
      // console.log('UploadFile', file);
      const formData = new FormData();
      formData.append('file', file);

      const upload: Upload = { state: 'PENDING', progress: 0, data: {name: file.name, size: file.size} };
      this.files.push(upload);

      res = concat(res, this.app.uploadFile('v1/user/importfile/' + this.importId, formData, upload));
    }

    const total = this.files.length;
    let finished = 0;
    res.subscribe((event: Upload) => {
      if ( event.state === 'DONE' ) {
        finished += 1;
        if ( finished === total ) {
          // console.log('Finished');
          this.app.putResource('v1/user/finishImport/' + this.currentUser.id + '/' + this.importId)
            .subscribe((task: TaskStatus) => {
              if ( task.messages.length > 0 && task.messages[0] === 'Could not create import ') {
                alert('Could not create import. Insufficient rights?');
              } else {
                this.importRunning = true;
                // need to update so that taskMonitor is defined
                this.cd.detectChanges();
                this.taskMonitor.monitor(task);
              }
            });
        }
      }
    });
  }

  removeFile(event: Event, file: File, uploader: FileUpload) {
    const index = uploader.files.indexOf(file);
    uploader.remove(event, index);
  }

  ngOnDestroy(): void {
    TaskMonitorComponent.Destory(this.taskMonitor);
  }
}
