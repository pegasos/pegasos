// admin-module: entry: 'Activities' -> 'Fix EndTime'

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminToolComponent } from '../admin.tool.component';
import { AppService } from '../../services/app.service';
import { GenericResponse } from './../../shared/sample';

@Component({
  selector: 'app-admin-fix-endtime',
  templateUrl: './fixendtime.component.html',
  // styleUrls: ['./admin.component.scss']
})
export class FixEndTimeComponent extends AdminToolComponent implements OnInit {

  myForm!: FormGroup;

  public running = false;

  constructor(
    private app: AppService,
    protected fb: FormBuilder,
    private cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      activityId: ['', [Validators.required]],
    });
    this.app.translateContent();
  }

  public onPersonsReady(): void {
    this.app.stopLoading();
  }

  public OnRun(): void {
    const activityId = this.myForm.get('activityId')?.value;
    
    const request = 'v1/tools/fixend/' + activityId;

    this.running = true;
    this.app.putResource(request).subscribe((response: GenericResponse) => {
      this.running = false;
      alert(response.message);
    });
  }
}
