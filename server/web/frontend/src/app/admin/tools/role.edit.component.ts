// admin-module: entry: 'User' -> 'Edit Roles', order: 2

import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { AdminToolComponent } from '../admin.tool.component';
import { Role, UserDetails } from '../../shared/sample';
import { AppService } from '../../services/app.service';
import { SystemPersonSelectComponent } from './../system-person-select.component';

@Component({
  selector: 'app-admin-edit-user-role',
  templateUrl: './role.edit.component.html',
  // styleUrls: ['./admin.component.scss']
})
export class RoleEditComponent extends AdminToolComponent implements OnInit {
  protected usersReceived = false;
  selectedUser!: UserDetails;

  @ViewChild('persons') protected personSelect!: SystemPersonSelectComponent;

  /**
   * Roles of the user
   */
  userRoles: any;

  /**
   * All roles available
   */
  roles!: Role[];
  selectedRole: Role | undefined;

  currentUser!: UserDetails;

  constructor(
    private app: AppService,
    private cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.app.startLoading();
    this.app.getResource('v1/roles').subscribe(data => this.onRolesReceived(data), err => this.app.handleReceiveError(err));

    this.cd.detectChanges();
    this.app.translateContent();
  }

  public onUsersReceived(data: UserDetails[]): void {
    this.usersReceived = true;
    if ( this.roles !== undefined ) {
      this.app.stopLoading();
    }
  }

  public onRolesReceived(data: Role[]): void {
    this.roles = data;
    if ( this.usersReceived ) {
      this.app.stopLoading();
    }
  }

  public onUserRolesReceived(user: UserDetails, data: []): void {
    this.userRoles = data;
    this.currentUser = user;
    this.app.stopLoading();
  }

  protected onUserSelect(user: UserDetails): void {
    this.selectedUser = user;
  }

  public onEdit(): void {
    if (this.selectedUser !== undefined ) {
      this.app.startLoading();
      this.app.getResource('v1/roles/user/' + this.selectedUser.id)
        .subscribe(data => this.onUserRolesReceived(this.selectedUser, data), err => this.app.handleReceiveError(err));
    }
  }

  public onRoleSelected(event: any) {
    this.selectedRole = event.value;
  }

  public onRoleAdd(): void {
    // assume nothing weired has happend and user & role are selected
    this.app.startLoading();
    this.app.putResource('v1/roles/user/' + this.currentUser.id + '/' + this.selectedRole?.id).subscribe((data) => {
      if ( data.message === 'OK' ) {
        this.userRoles.push(this.selectedRole?.name);
        this.selectedRole = undefined;
      } else if ( data.message === 'Fail' ) {
        alert('Adding failed');
      } else {
        alert(data.message);
      }
    },
    (error: any) => alert(error),
    () => this.app.stopLoading());
  }

  public onRoleDelete(role: string): void {
    // assume nothing bad happens ...
    const id = this.roles.filter(r => r.name === role)[0].id;
    this.app.startLoading();
    this.app.deleteResource('v1/roles/user/' + this.currentUser.id + '/' + id).subscribe((data) => {
      if ( data.message === 'OK' ) {
        this.userRoles = this.userRoles.filter((r: string) => r !== role);

        this.app.getResource('v1/roles/user/' + this.currentUser.id)
          .subscribe(dd => console.log('User roles', dd));
      } else if ( data.message === 'Fail' ) {
        alert('Delete failed');
      }
    },
    (error: any) => alert(error),
    () => this.app.stopLoading());
  }
}
