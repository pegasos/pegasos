// admin-module: entry: 'Activities' -> 'Run PostProcessor'

import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AdminToolComponent } from '../admin.tool.component';
import { ActivitySelectComponent } from './../../actioncomponents/activity-select/activityselect.component';
import { PersonSelectComponent } from './../../actioncomponents/person-select/personselect.component';
import { TimePeriodComponent } from './../../actioncomponents/time-period/timeperiod.component';
import { AppService } from '../../services/app.service';
import { TaskMonitorComponent } from './../../actioncomponents/task-monitor/taskmonitor.component';
import { TaskStatus } from './../../shared/sample';

@Component({
  selector: 'app-admin-run-postprocessor',
  templateUrl: './run.postprocessor.component.html',
  // styleUrls: ['./admin.component.scss']
})
export class RunPostProcessorComponent extends AdminToolComponent implements OnInit {

  @ViewChild('personSelect') protected personSelect!: PersonSelectComponent;

  @ViewChild('timePeriod') public timePeriod!: TimePeriodComponent;

  @ViewChild('activitiesSelect') public activitiesSelect!: ActivitySelectComponent;

  @ViewChild('taskMonitor') taskMonitor!: TaskMonitorComponent;

  taskRunning = false;

  myForm!: FormGroup;

  constructor(
    private app: AppService,
    protected fb: FormBuilder,
    private cd: ChangeDetectorRef) {
    super();
    this.app.startLoading();
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      activities: new FormControl(),
      persons: new FormControl(),
    });
    this.app.translateContent();
  }

  public onPersonsReady(): void {
    this.app.stopLoading();
  }

  public OnRun(): void {
    const params = {};
    this.personSelect.updateRouteParams(params);
    this.timePeriod.updateRouteParams(params);

    const activities = this.activitiesSelect.getFormData({});
    const formData = 'activitytypes' in activities ? activities['activitytypes'] : [];

    let request = 'v1/activities/runpost?';
    Object.entries(params).forEach(
      ([key, value]) => {
        console.log(key, value);
        request += key + '=' + value + '&';
      }
    );

    this.app.putResource(request, formData).subscribe((task: TaskStatus) => {
      this.taskRunning = true;
      // need to update so that taskMonitor is defined
      this.cd.detectChanges();
      // this.app.translateTree('task-display');
      this.app.translateContent();
      this.taskMonitor.monitor(task);
    });
  }

  ngOnDestroy(): void {
    TaskMonitorComponent.Destory(this.taskMonitor);
  }
}
