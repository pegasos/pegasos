export var lang_dict={
  "AVG_POWER": {
    "de": "durchschn. Leistung [W]",
    "en": "avg Power [W]"
  },
  "Poincare": {
    "en": "Poincar\u00e9"
  },
  "Tachogram": {
    "en": "Tachogram"
  },
  "activities_doRunPostProcessor": {
    "de": "Ergebnisse neu berechnen",
    "en": "Recalculate results"
  },
  "activities_runPostProcessor": {
    "de": "Wollen Sie die Ergebnisse der Aktivit\u00e4t wirklich neu berechnen? Diese Aktion kann nicht r\u00fcckg\u00e4ngig gemacht werden.",
    "en": "Are you sure? Recalculating results cannot be undone."
  },
  "activity": {
    "bg": "\u0414\u0435\u0439\u043d\u043e\u0441\u0442",
    "de": "Aktivit\u00e4t",
    "en": "Activity",
    "hu": "Aktivit\u00e1s",
    "is": "A\u00f0ger\u00f0",
    "mk": "\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442"
  },
  "admin_addToGroup": {
    "de": "Zu Gruppe hinzuf\u00fcgen",
    "en": "add to group"
  },
  "admin_createUser": {
    "de": "Neuen User anlegen",
    "en": "Create new user"
  },
  "admin_postprocessor_running": {
    "de": "Berechnung l\u00e4uft",
    "en": "Processing"
  },
  "admin_run_postprocessor": {
    "de": "Run",
    "en": "Run"
  },
  "ajax_error": {
    "bg": "\u0413\u0440\u0435\u0448\u043a\u0430 \u043f\u0440\u0438 \u0437\u0430\u043f\u0430\u0437\u0432\u0430\u043d\u0435 \u043d\u0430 \u0434\u0430\u043d\u043d\u0438\u0442\u0435.",
    "de": "Beim Speichern der Daten ist ein Fehler aufgetreten.",
    "en": "An error occurred while saving your data.",
    "hu": "Hiba t\u00f6rt\u00e9nt az adatok ment\u00e9se k\u00f6zbe.",
    "is": "Upp kom villa \u00feegar vistun f\u00f3r fram.",
    "mk": "\u041d\u0430\u0441\u0442\u0430\u043d\u0430 \u0433\u0440\u0435\u0448\u043a\u0430 \u043f\u0440\u0438 \u0437\u0430\u0447\u0443\u0432\u0443\u0432\u0430\u045a\u0435 \u043d\u0430 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438\u0442\u0435"
  },
  "ajax_saved": {
    "bg": "\u041d\u043e\u0432\u0438\u0442\u0435 \u0432\u0438 \u0434\u0430\u043d\u043d\u0438 \u0431\u044f\u0445\u0430 \u0437\u0430\u043f\u0430\u0437\u0435\u043d\u0438.",
    "de": "Ihre neuen Daten wurden gespeichert.",
    "en": "Your new data has been saved.",
    "hu": "Az \u00faj adaqtok ment\u00e9sre ker\u00fcltek.",
    "is": "N\u00fdju g\u00f6gnin \u00fe\u00edn hafa veri\u00f0 vistu\u00f0.",
    "mk": "\u0412\u0430\u0448\u0438\u0442\u0435 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438 \u0441\u0435 \u0437\u0430\u0447\u0443\u0432\u0430\u043d\u0438"
  },
  "all": {
    "bg": "\u0432\u0441\u0438\u0447\u043a\u043e",
    "de": "alle",
    "en": "all",
    "hu": "mind",
    "is": "allir",
    "mk": "\u0441\u0438\u0442\u0435"
  },
  "bikecadence_rpm": {
    "de": "Trittfrequenz [rpm]",
    "en": "Cadence [rpm]"
  },
  "bikepower_w": {
    "de": "Leistung [W]",
    "en": "Power [W]"
  },
  "chart_BikeCadence": {
    "de": "Trittfrequenz",
    "en": "Cadence"
  },
  "chart_BikePower": {
    "de": "Leistung",
    "en": "Power"
  },
  "chart_BikePowerTinZ": {
    "de": "Zeit in Power Zone",
    "en": "Time in power zone"
  },
  "chart_BikePowerTinZ2": {
    "de": "Zeit in Power Zone",
    "en": "Time in power zone"
  },
  "chart_BikeSpeed": {
    "de": "Geschwindigkeit (Fahrrad)",
    "en": "Speed (Bike)"
  },
  "chart_HR": {
    "de": "Herzfrequenz",
    "en": "Heartrate"
  },
  "chart_Laps": {
    "de": "Runden",
    "en": "Laps"
  },
  "chart_Map": {
    "de": "Karte",
    "en": "Map"
  },
  "chart_Metrics": {
    "de": "Metriken",
    "en": "Metrics"
  },
  "chart_Poincare": {
    "de": "Poincar\u00e9"
  },
  "chart_Tachogram": {
    "de": "Tachogram"
  },
  "charts_lap_lapnr": {
    "de": "#",
    "en": "#"
  },
  "charts_rr_interval_axis": {
    "de": "RR Abstand [ms]",
    "en": "RR interval [ms]"
  },
  "contactInfo_institute": {
    "de": "Zentrum f\u00fcr Sportwissenschaft und Universit\u00e4tssport",
    "en": "Centre for Sport Science and University Sports",
    "hu": "Centre for Sport Science and University Sports",
    "is": "Centre for Sport Science and University Sports"
  },
  "contactInfo_street": {
    "de": "Auf der Schmelz 6",
    "en": "Auf der Schmelz 6",
    "hu": "Auf der Schmelz 6",
    "is": "Auf der Schmelz 6"
  },
  "contactInfo_university": {
    "de": "Universit\u00e4t Wien",
    "en": "University of Vienna",
    "hu": "University of Vienna",
    "is": "University of Vienna"
  },
  "contactInfo_zip": {
    "de": "1150 Vienna",
    "en": "1150 Vienna",
    "hu": "1150 Vienna",
    "is": "1150 Vienna"
  },
  "create": {
    "bg": "\u0441\u044a\u0437\u0434\u0430\u0432\u0430\u043c",
    "de": "Erstellen",
    "en": "Create",
    "hu": "L\u00e9trehoz\u00e1s",
    "is": "Skapa",
    "mk": "\u041a\u0440\u0435\u0438\u0440\u0430\u0458"
  },
  "currentEvents_activeMembers": {
    "bg": "\u0410\u043a\u0442\u0438\u0432\u043d\u0438 \u0447\u043b\u0435\u043d\u043e\u0432\u0435 \u043d\u0430 \u043e\u0442\u0431\u043e\u0440\u0430",
    "de": "Aktive Teammitglieder",
    "en": "Active team members",
    "hu": "Akt\u00edv csapattagok",
    "is": "Virkir a\u00f0ilar a\u00f0 teyminu",
    "mk": "\u0410\u043a\u0442\u0432\u043d\u0438 \u0447\u043b\u0435\u043d\u043e\u0432\u0438 \u043d\u0430 \u0442\u0438\u043c\u043e\u0442"
  },
  "currentEvents_chartsDescription": {
    "bg": "\u0420\u0430\u0437\u0433\u043b\u0435\u0434\u0430\u0439\u0442\u0435 \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u0438 \u0434\u0438\u0430\u0433\u0440\u0430\u043c\u0438",
    "de": "Sehen Sie sich verschiedene Charts an",
    "en": "Take a look at various charts",
    "hu": "K\u00fcl\u00f6nf\u00e9e \u00e1b\u00e1rk megtekint\u00e9se",
    "is": "L\u00edtum \u00e1 nokkur kort",
    "mk": "\u0440\u0430\u0437\u0433\u043b\u0435\u0434\u0430\u0458\u0442\u0435 \u0433\u0438 \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u0438\u0442\u0435 \u043f\u043e\u0433\u043b\u0430\u0432\u0458\u0430"
  },
  "currentEvents_chooseActivity": {
    "bg": "\u041c\u043e\u043b\u044f, \u0438\u0437\u0431\u0435\u0440\u0435\u0442\u0435 \u0434\u0435\u0439\u043d\u043e\u0441\u0442 \u0432 \u0433\u043e\u0440\u043d\u0438\u044f \u0434\u0435\u0441\u0435\u043d \u044a\u0433\u044a\u043b, \u0437\u0430 \u0434\u0430 \u043f\u043e\u043b\u0443\u0447\u0438\u0442\u0435 \u043f\u043e\u0434\u0440\u043e\u0431\u043d\u0430 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u0437\u0430 \u0447\u043b\u0435\u043d\u043e\u0432\u0435\u0442\u0435 \u043d\u0430 \u0412\u0430\u0448\u0438\u044f \u043e\u0442\u0431\u043e\u0440.",
    "de": "Bitte w\u00e4hlen Sie rechts oben eine Activity aus, um Detailinformationen \u00fcber Ihre Teammitglieder zu erhalten.",
    "en": "Please choose an activity in the upper right corner to receive detailed informations about your team members.",
    "hu": "K\u00e9rj\u00fck, v\u00e1lasszon egy tev\u00e9kenys\u00e9get a jobb fels\u0151 sarokban, hogy r\u00e9szletes inform\u00e1ci\u00f3kat kapjon a csapat tagjair\u00f3l.",
    "is": "Vinsamlega veldu a\u00f0ger\u00f0 efst \u00ed h\u00e6gra horni til a\u00f0 f\u00e1 n\u00e1kv\u00e6mar uppl\u00fdsingar um \u00fe\u00e1 sem eru \u00ed \u00fe\u00ednu teymi.",
    "mk": "\u0412\u0435 \u043c\u043e\u043b\u0438\u043c\u0435 \u043e\u0434\u0431\u0435\u0440\u0435\u0442\u0435 \u0430\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442 \u0432\u043e \u0433\u043e\u0440\u043d\u0438\u043e\u0442 \u0434\u0435\u0441\u0435\u043d \u0430\u0433\u043e\u043b \u0437\u0430 \u0434\u0430 \u0434\u043e\u0431\u0438\u0432\u0430\u0442\u0435 \u0434\u0435\u0442\u0430\u043b\u043d\u0438 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0438 \u0437\u0430 \u0447\u043b\u0435\u043d\u043e\u0432\u0438\u0442\u0435 \u043d\u0430 \u0412\u0430\u0448\u0438\u043e\u0442 \u0442\u0438\u043c"
  },
  "currentEvents_message": {
    "bg": "\u0441\u044a\u043e\u0431\u0449\u0435\u043d\u0438\u0435",
    "de": "Nachricht",
    "en": "Message",
    "hu": "\u00dczenet",
    "is": "Skilabo\u00f0",
    "mk": "\u041f\u043e\u0440\u0430\u043a\u0430"
  },
  "currentEvents_noMembersActive": {
    "bg": "\u041d\u0438\u043a\u043e\u0439 \u043e\u0442 \u0447\u043b\u0435\u043d\u043e\u0432\u0435\u0442\u0435 \u043d\u0430 \u0412\u0430\u0448\u0438\u044f \u043e\u0442\u0431\u043e\u0440 \u043d\u0435 \u0435 \u0430\u043a\u0442\u0438\u0432\u0435\u043d \u0432 \u043c\u043e\u043c\u0435\u043d\u0442\u0430.",
    "de": "Keine Teammitglieder sind im Moment aktiv.",
    "en": "None of your team members is active at the moment.",
    "hu": "Jelenleg egyetlen csapattag sem akt\u00edv.",
    "is": "Enginn \u00ed \u00fe\u00ednu teymi er virkur n\u00fana.",
    "mk": "\u041d\u0438\u043a\u043e\u0458 \u043e\u0434 \u0447\u043b\u0435\u043d\u043e\u0432\u0438\u0442\u0435 \u043d\u0430 \u0432\u0430\u0448\u0438\u043e\u0442 \u0442\u0438\u043c \u043d\u0435 \u0435 \u0430\u043a\u0442\u0438\u0432\u0435\u043d \u0432\u043e \u043e\u0432\u043e\u0458 \u043c\u043e\u043c\u0435\u043d\u0442"
  },
  "currentEvents_noTeam": {
    "bg": "\u041d\u0435 \u043f\u0440\u0438\u0442\u0435\u0436\u0430\u0432\u0430\u0442\u0435 \u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d \u043e\u0442\u0431\u043e\u0440.",
    "de": "Sie besitzen kein Team.",
    "en": "You do not own a team.",
    "hu": "\u00d6nnek nincsen csapata.",
    "is": "Ert \u00fe\u00fa ekki hluti af teymi.",
    "mk": "\u041d\u0435\u043c\u0430\u0442 \u0441\u043e\u043f\u0441\u0442\u0432\u0435\u043d \u0442\u0438\u043c"
  },
  "currentEvents_overview": {
    "bg": "\u043f\u0440\u0435\u0433\u043b\u0435\u0434",
    "de": "\u00dcbersicht",
    "en": "overview",
    "hu": "\u00c1ttekint\u00e9s",
    "is": "yfirlit",
    "mk": "\u043f\u0440\u0435\u0433\u043b\u0435\u0434\u0438"
  },
  "currentEvents_sendMessage": {
    "bg": "\u0418\u0437\u043f\u0440\u0430\u0442\u0438 \u0441\u044a\u043e\u0431\u0449\u0435\u043d\u0438\u0435",
    "de": "Nachricht senden",
    "en": "Send Message",
    "hu": "\u00dczenet k\u00fcld\u00e9se",
    "is": "Sendu skilabo\u00f0",
    "mk": "\u041f\u0440\u0430\u0442\u0438 \u043f\u043e\u0440\u0430\u043a\u0430"
  },
  "currentEvents_sendMessageTo": {
    "bg": "\u0418\u0437\u043f\u0440\u0430\u0442\u0435\u0442\u0435 \u0441\u044a\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u0434\u043e \u0441\u044a\u0441\u0442\u0435\u0437\u0430\u0442\u0435\u043b\u044f",
    "de": "Senden Sie einem/einer Athleten/Athletin eine Nachricht",
    "en": "Send a message to the athlete",
    "hu": "\u00dczenet k\u00fcld\u00e9se a sportol\u00f3nak",
    "is": "Sendu skilabo\u00f0 til \u00ed\u00fer\u00f3ttamanns",
    "mk": "\u041f\u0440\u0430\u0442\u0438 \u043f\u043e\u0440\u0430\u043a\u0430 \u043d\u0430 \u0441\u043f\u043e\u0440\u0442\u0438\u0441\u0442"
  },
  "currentEvents_showCharts": {
    "bg": "\u041f\u043e\u043a\u0430\u0437\u0432\u0430\u043d\u0435 \u043d\u0430 \u0434\u0438\u0430\u0433\u0440\u0430\u043c\u0438",
    "de": "Charts anzeigen",
    "en": "Show Charts",
    "hu": "\u00c1br\u00e1k mutat\u00e1sa",
    "is": "S\u00fdni\u00f0 kort",
    "mk": "\u041f\u043e\u043a\u0430\u0436\u0438 \u043f\u043e\u0433\u043b\u0430\u0432\u0458\u0430"
  },
  "currentEvents_title": {
    "bg": "\u0422\u0435\u043a\u0443\u0449\u0438 \u0441\u044a\u0431\u0438\u0442\u0438\u044f",
    "de": "Aktuell",
    "en": "Current Events",
    "hu": "Aktu\u00e1lis esem\u00e9nyek",
    "is": "N\u00favernadi vi\u00f0bur\u00f0ur",
    "mk": "\u041c\u043e\u043c\u0435\u043d\u0442\u0430\u043b\u043d\u0438 \u043d\u0430\u0441\u0442\u0430\u043d\u0438"
  },
  "data": {
    "bg": "\u0414\u0430\u043d\u043d\u0438",
    "de": "Daten",
    "en": "Data",
    "hu": "Adat",
    "is": "G\u00f6gn",
    "mk": "\u041f\u043e\u0434\u0430\u0442\u043e\u0446\u0438"
  },
  "diary_activity": {
    "bg": "\u0434\u0435\u0439\u043d\u043e\u0441\u0442",
    "de": "Aktivit\u00e4t",
    "en": "activity",
    "hu": "aktivit\u00e1s",
    "is": "a\u00f0ger\u00f0",
    "mk": "\u0430\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442"
  },
  "diary_cannotDeleteNotOwner": {
    "de": "Aktivit\u00e4t kann nicht gel\u00f6scht werden, da Sie kein Manager der Gruppe sind",
    "en": "Could not delete activity since you are not a manager of the group."
  },
  "diary_clubSessionHint": {
    "bg": "\u0422\u043e\u0432\u0430 \u0435 \u043a\u043b\u0443\u0431\u043d\u0430 \u0441\u0435\u0441\u0438\u044f",
    "de": "Dies ist eine Club-Session",
    "en": "This is a club session",
    "hu": "Ez egy klub\u00fcl\u00e9s",
    "is": "\u00deetta er \u00e6fing kl\u00fabbsins",
    "mk": "\u041e\u0432\u0430 \u0435 \u0441\u0435\u0441\u0438\u0458\u0430 \u043d\u0430 \u0442\u0438\u043c\u043e\u0442"
  },
  "diary_completeStages": {
    "bg": "\u0418\u0437\u043f\u044a\u043b\u043d\u0435\u043d\u0438 \u0435\u0442\u0430\u043f\u0438: ",
    "de": "Komplette Stufen: ",
    "en": "Complete Stages: ",
    "hu": "Elk\u00e9sz\u00fclt szakaszok: ",
    "is": "Skrefum loki: ",
    "mk": "\u0417\u0430\u0432\u0440\u0448\u0435\u043d\u043e \u043d\u0438\u0432\u043e: "
  },
  "diary_date": {
    "bg": "\u0434\u0430\u0442\u0430",
    "de": "Datum",
    "en": "date",
    "hu": "d\u00e1tum",
    "is": "dagsetning",
    "mk": "\u0434\u0430\u0442\u0443\u043c"
  },
  "diary_delete": {
    "de": "L\u00f6schen",
    "en": "Delete"
  },
  "diary_deleteSession": {
    "de": "Sind Sie sicher? Das L\u00f6schen einer Aktivit\u00e4t kann nicht r\u00fcckg\u00e4ngig gemacht werden.",
    "en": "Are you sure? Deleting an activity cannot be undone."
  },
  "diary_distance": {
    "bg": "\u0440\u0430\u0437\u0441\u0442\u043e\u044f\u043d\u0438\u0435 [m]",
    "de": "Distanz [m]",
    "en": "distance [m]",
    "hu": "t\u00e1vols\u00e1g [m]",
    "is": "fjarl\u00e6g\u00f0 [m]",
    "mk": "\u0440\u0430\u0441\u0442\u043e\u0458\u0430\u043d\u0438\u0435 [\u043c]"
  },
  "diary_downloadsDetails": {
    "bg": "\u0438\u0437\u0442\u0435\u0433\u043b\u044f\u043d\u0438\u044f \u0438 \u043f\u043e\u0434\u0440\u043e\u0431\u043d\u043e\u0441\u0442\u0438",
    "de": "Downloads & Details",
    "en": "downloads & details",
    "hu": "let\u00f6lt\u00e9sek \u00e9s hozz\u00e1val\u00f3k",
    "is": "ni\u00f0urhal og uppl\u00fdsingar",
    "mk": "\u0441\u0438\u043c\u043d\u0443\u0432\u0430\u045a\u0430 & \u0434\u0435\u0442\u0430\u043b\u0438"
  },
  "diary_duration": {
    "bg": "\u043f\u0440\u043e\u0434\u044a\u043b\u0436\u0438\u0442\u0435\u043b\u043d\u043e\u0441\u0442 [\u0447:\u043c:\u0441]",
    "de": "Dauer [h:m:s]",
    "en": "duration [h:m:s]",
    "hu": "id\u0151tartam [h:m:s]",
    "is": "t\u00edmalengd [h:m:s]",
    "mk": "\u0442\u0440\u0430\u0435\u045a\u0435 [h:m:s]"
  },
  "diary_entries": {
    "bg": "\u0412\u043f\u0438\u0441\u0432\u0430\u043d\u0438\u044f",
    "de": "Eintr\u00e4ge",
    "en": "entries",
    "hu": "bejegyz\u00e9s",
    "is": "insl\u00e1ttur",
    "mk": "\u0412\u043b\u0435\u0433\u0443\u0432\u0430\u045a\u0430"
  },
  "diary_filterOptions": {
    "bg": "\u041e\u043f\u0446\u0438\u0438 \u0437\u0430 \u0444\u0438\u043b\u0442\u0440\u0438\u0440\u0430\u043d\u0435",
    "de": "Filteroptionen",
    "en": "Filter Options",
    "hu": "Opci\u00f3k sz\u0171r\u00e9s",
    "is": "Valkostir s\u00edu",
    "mk": "\u041e\u043f\u0446\u0438\u0438 \u0437\u0430 \u0444\u0438\u043b\u0442\u0440\u0438\u0440\u0430\u045a\u0435"
  },
  "diary_group": {
    "bg": "\u0433\u0440\u0443\u043f\u0430",
    "de": "Gruppe",
    "en": "group",
    "hu": "csoport",
    "is": "h\u00f3pur",
    "mk": "\u0433\u0440\u0443\u043f\u0430"
  },
  "diary_maxHr": {
    "bg": "\u041c\u0430\u043a\u0441. \u0421\u044a\u0440\u0434\u0435\u0447\u0435\u043d \u0440\u0438\u0442\u044a\u043c [min-&sup1;]",
    "de": "Max. Hr [min-&sup1;]",
    "en": "Max. Hr [min-&sup1;]",
    "hu": "Max. Hr [min-&sup1;]",
    "is": "Mesti Hr [m\u00edn-&sup1;]",
    "mk": "Max. Hr \u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u0435\u043d \u043f\u0443\u043b\u0441 [min-&sup1;]"
  },
  "diary_name": {
    "bg": "\u0438\u043c\u0435",
    "de": "Name",
    "en": "name",
    "hu": "n\u00e9v",
    "is": "nafn",
    "mk": "\u0438\u043c\u0435 "
  },
  "diary_next": {
    "bg": "\u0421\u043b\u0435\u0434\u0432\u0430\u0449",
    "de": "Weiter",
    "en": "Next",
    "hu": "k\u00f6vetkez\u0151",
    "is": "N\u00e6sta",
    "mk": "\u0421\u043b\u0435\u0434\u043d\u043e"
  },
  "diary_noDataFound": {
    "bg": "\u041d\u044f\u043c\u0430 \u043d\u0430\u043c\u0435\u0440\u0435\u043d\u0438 \u0434\u0430\u043d\u043d\u0438.",
    "de": "Keine Daten gefunden.",
    "en": "No data found.",
    "hu": "Az adatok nem tal\u00e1lhat\u00f3ak.",
    "is": "Fann engin g\u00f6gn.",
    "mk": "\u041d\u0435 \u0441\u0435 \u043d\u0430\u0458\u0434\u0435\u043d\u0438 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438"
  },
  "diary_overview": {
    "bg": "\u041f\u0440\u0435\u0433\u043b\u0435\u0434",
    "de": "\u00dcbersicht",
    "en": "Overview",
    "hu": "\u00c1ttekint\u00e9s",
    "is": "Yfirlit",
    "mk": "\u041f\u0440\u0435\u0433\u043b\u0435\u0434"
  },
  "diary_place": {
    "bg": "\u043c\u044f\u0441\u0442\u043e",
    "de": "Platz",
    "en": "place",
    "hu": "hely",
    "is": "sta\u00f0ur",
    "mk": "\u043c\u0435\u0441\u0442\u043e"
  },
  "diary_points": {
    "bg": "\u0442\u043e\u0447\u043a\u0438",
    "de": "Punkte",
    "en": "points",
    "hu": "pontok",
    "is": "p\u00fanktar",
    "mk": "\u043f\u043e\u0435\u043d\u0438"
  },
  "diary_previous": {
    "bg": "\u041f",
    "de": "Zur\u00fcck",
    "en": "Previous",
    "hu": "el\u0151z\u0151",
    "is": "Fyrri",
    "mk": "\u041f\u0440\u0435\u0442\u0445\u043e\u0434\u043d\u043e"
  },
  "diary_pulse": {
    "bg": "&#216; \u043f\u0443\u043b\u0441 [min-&sup1;]",
    "de": "&#216; Puls [min-&sup1;]",
    "en": "&#216; pulse [min-&sup1;]",
    "hu": "&#216; pulzus[min-&sup1;]",
    "is": "&#216; p\u00fals [m\u00edn-&sup1;]",
    "mk": "&#216; pulse [min-&sup1;]"
  },
  "diary_rank": {
    "bg": "\u041c\u044f\u0441\u0442\u043e \u0432\u043a\u043b\u0430\u0441\u0438\u0440\u0430\u043d\u0435\u0442\u043e",
    "de": "Rang",
    "en": "Rank",
    "hu": "helyez\u00e9s",
    "is": "R\u00f6\u00f0un",
    "mk": "\u0420\u0430\u043d\u0433"
  },
  "diary_result": {
    "bg": "\u0440\u0435\u0437\u0443\u043b\u0442\u0430\u0442",
    "de": "Ergebnis",
    "en": "result",
    "hu": "eredm\u00e9ny",
    "is": "ni\u00f0ursta\u00f0a",
    "mk": "\u0440\u0435\u0437\u0443\u043b\u0442\u0430\u0442"
  },
  "diary_sId": {
    "de": "S-ID",
    "en": "S-ID"
  },
  "diary_score": {
    "bg": "\u0440\u0435\u0437\u0443\u043b\u0442\u0430\u0442",
    "de": "Punkte",
    "en": "Score",
    "hu": "pontsz\u00e1m",
    "is": "Sta\u00f0a",
    "mk": "\u0420\u0435\u0437\u0443\u043b\u0442\u0430\u0442"
  },
  "diary_search": {
    "bg": "\u0422\u044a\u0440\u0441\u0435\u043d\u0435",
    "de": "Suche",
    "en": "Search",
    "hu": "keres\u00e9s",
    "is": "Leit",
    "mk": "\u0411\u0430\u0440\u0430\u0458"
  },
  "diary_sessionDetails": {
    "bg": "\u0414\u0435\u0442\u0430\u0439\u043b\u0438 \u0437\u0430 \u0441\u0435\u0441\u0438\u044f\u0442\u0430",
    "de": "Details zur Session",
    "en": "Session Details",
    "hu": "id\u0151szaki r\u00e9szletek",
    "is": "Uppl\u00fdsingar um \u00e6fingu",
    "mk": "\u0414\u0435\u0442\u0430\u043b\u0438 \u0437\u0430 \u0441\u0435\u0441\u0438\u0458\u0430\u0442\u0430"
  },
  "diary_sessionDetailsDescription": {
    "bg": "\u0422\u0443\u043a \u043c\u043e\u0436\u0435\u0442\u0435 \u0434\u0430 \u0440\u0430\u0437\u0433\u043b\u0435\u0434\u0430\u0442\u0435 \u043f\u043e\u0434\u0440\u043e\u0431\u043d\u043e \u0437\u0430\u0432\u044a\u0440\u0448\u0435\u043d\u0430\u0442\u0430 \u0442\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u044a\u0447\u043d\u0430 \u0441\u0435\u0441\u0438\u044f.",
    "de": "Hier k\u00f6nnen Sie sich eine abgeschlossene Trainingsession im Detail ansehen.",
    "en": "Here you can take a look at a finished training session in detail.",
    "hu": "Itt r\u00e9szletesen megtekintheti a befejezett edz\u00e9st.",
    "is": "H\u00e9r getur \u00fe\u00fa sko\u00f0a\u00f0 uppl\u00fdsingar fr\u00e1 lokinni \u00e6fingu.",
    "mk": "\u041e\u0432\u0434\u0435 \u043c\u043e\u0436\u0435\u0442\u0435 \u0434\u0430 \u0433\u0438 \u0432\u0438\u0434\u0438\u0442\u0435 \u0437\u0430\u0432\u0440\u0448\u0435\u043d\u0438\u0442\u0435 \u0441\u0435\u0441\u0438\u0438 \u0432\u043e \u0434\u0435\u0442\u0430\u043b\u0438"
  },
  "diary_show": {
    "bg": "\u043f\u043e\u043a\u0430\u0437\u0432\u0430\u043d\u0435",
    "de": "Anzeigen",
    "en": "Show",
    "hu": "mutat",
    "is": "S\u00fdna",
    "mk": "\u041f\u043e\u043a\u0430\u0436\u0438"
  },
  "diary_speed": {
    "bg": "&#216;  \u0441\u043a\u043e\u0440\u043e\u0441\u0442",
    "de": "&#216; Geschwindigkeit",
    "en": "&#216; speed",
    "hu": "&#216; sebess\u00e9g",
    "is": "&#216; hra\u00f0i",
    "mk": "&#216; \u0431\u0440\u0437\u0438\u043d\u0430"
  },
  "diary_steps": {
    "bg": "\u0441\u0442\u044a\u043f\u043a\u0438",
    "de": "Stufen",
    "en": "steps",
    "hu": "l\u00e9p\u00e9sek",
    "is": "skref",
    "mk": "\u0447\u0435\u043a\u043e\u0440\u0438"
  },
  "diary_strokes": {
    "bg": "\u0443\u0434\u0430\u0440\u0438",
    "de": "Schlagzahl",
    "en": "strokes",
    "hu": "\u00e9rver\u00e9s",
    "is": "h\u00f6gg",
    "mk": "\u043f\u043e\u0442\u0435\u0437\u0438"
  },
  "diary_testNotCompleted": {
    "bg": "\u0422\u0435\u0441\u0442\u044a\u0442 \u043d\u0435 \u0435 \u0437\u0430\u0432\u044a\u0440\u0448\u0435\u043d ",
    "de": "Der Test ist nicht beendet ",
    "en": "Test is not completed ",
    "hu": "A test m\u00e9g nem fejez\u0151d\u0151tt be ",
    "is": "\u00c6fingunni er ekki loki\u00f0",
    "mk": "\u0422\u0435\u0441\u0442\u043e\u0442 \u043d\u0435 \u0435 \u0437\u0430\u0432\u0440\u0448\u0435\u043d "
  },
  "diary_time": {
    "bg": "\u0432\u0440\u0435\u043c\u0435 [\u0447:\u043c:\u0441]",
    "de": "Zeit [h:m:s]",
    "en": "time [h:m:s]",
    "hu": "id\u0151 [h:m:s]",
    "is": "t\u00edmi [h:m:s]",
    "mk": "\u0432\u0440\u0435\u043c\u0435 [h:m:s]"
  },
  "diary_title": {
    "bg": "\u0434\u043d\u0435\u0432\u043d\u0438\u043a",
    "de": "Tagebuch",
    "en": "Diary",
    "hu": "Napl\u00f3",
    "is": "Dagb\u00f3k",
    "mk": "\u0434\u043d\u0435\u0432\u043d\u0438\u043a"
  },
  "diary_umttDetails": {
    "bg": "\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u043e\u0441\u0442\u0438 \u0437\u0430 UM-TT",
    "de": "Details zum UM-TT",
    "en": "UM-TT Details",
    "hu": "UM-TT r\u00e9szlet",
    "is": "UM-TT Uppl\u00fdsingar",
    "mk": "UM-TT \u0414\u0435\u0442\u0430\u043b\u0438"
  },
  "diary_vo2max": {
    "bg": "Vo2Max",
    "de": "Vo2Max",
    "en": "Vo2Max",
    "hu": "Vo2Max",
    "is": "Vo2Max",
    "mk": "Vo2Max"
  },
  "downloadTables": {
    "bg": "\u0418\u0437\u0442\u0435\u0433\u043b\u0435\u0442\u0435 \u0442\u0435\u043a\u0443\u0449\u0438\u0442\u0435 \u0442\u0430\u0431\u043b\u0438\u0446\u0438 \u0432\u044a\u0432 \u0444\u043e\u0440\u043c\u0430\u0442 CSV",
    "de": "Aktuelle Tabellen im CSV-Format downloaden",
    "en": "Download current tables in CSV format",
    "hu": "Let\u00f6lt\u00e9s CSV form\u00e1tumba",
    "is": "Hla\u00f0i\u00f0 ni\u00f0ur t\u00f6flunni \u00ed CSV ger\u00f0",
    "mk": "\u0421\u0438\u043c\u043d\u0443\u0432\u0430\u045a\u0435 \u043d\u0430 \u0441\u0438\u0442\u0435 \u043c\u043e\u043c\u0435\u043d\u0442\u0430\u043b\u043d\u0438 \u0442\u0430\u0431\u0435\u043b\u0438 \u0432\u043e CSV \u0444\u043e\u0440\u043c\u0430\u0442"
  },
  "einz": {
    "de": "Energie in Power Zone"
  },
  "email": {
    "bg": "\u0415\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430 \u043f\u043e\u0449\u0430",
    "de": "E-Mail",
    "en": "E-mail",
    "hu": "E-Mail",
    "is": "T\u00f6lvup\u00f3stur",
    "mk": "\u0435 - \u043c\u0430\u0438\u043b"
  },
  "energy": {
    "de": "Energie"
  },
  "filter": {
    "bg": "\u0444\u0438\u043b\u0442\u044a\u0440",
    "de": "filtern",
    "en": "Filter",
    "hu": "Sz\u0171r\u0151",
    "is": "S\u00eda",
    "mk": "\u0424\u0438\u043b\u0442\u0435\u0440"
  },
  "footer_contact": {
    "bg": "\u043a\u043e\u043d\u0442\u0430\u043a\u0442",
    "de": "Kontakt",
    "en": "Contact",
    "hu": "Kapcsolat",
    "is": "Tengsl",
    "mk": "\u041a\u043e\u043d\u0442\u0430\u043a\u0442"
  },
  "fpCalibrationFactor": {
    "bg": "FP \u043a\u043e\u0435\u0444\u0438\u0446\u0438\u0435\u043d\u0442 \u043d\u0430 \u043a\u0430\u043b\u0438\u0431\u0440\u0438\u0440\u0430\u043d\u0435",
    "de": "FP-Kalibrierungsfaktor",
    "en": "FP calibration factor",
    "hu": "FP kalibr\u00e1l\u00e1sit\u00e9nyez\u0151",
    "is": "FP kv\u00f6r\u00f0unar\u00fe\u00e1ttur",
    "mk": "FP \u043a\u0430\u043b\u0438\u0431\u0440\u0430\u0446\u0438\u0441\u043a\u0438 \u0444\u0430\u043a\u0442\u043e\u0440"
  },
  "from": {
    "bg": "\u043e\u0442",
    "de": "von",
    "en": "from",
    "hu": "-b\u00f3l",
    "is": "fr\u00e1",
    "mk": "\u043e\u0434"
  },
  "group": {
    "bg": "\u0433\u0440\u0443\u043f\u0430",
    "de": "Gruppe",
    "en": "Group",
    "hu": "Csoport",
    "is": "H'opur",
    "mk": "\u0413\u0440\u0443\u043f\u0430"
  },
  "header_slogan": {
    "bg": "\u0421\u043f\u043e\u0440\u0442\u043d\u0438\u044f\u0442 \u0430\u043d\u0430\u043b\u0438\u0437\u0430\u0442\u043e\u0440",
    "de": "Der Sport Analyser",
    "en": "The Sports Analyser",
    "hu": "A sportelemz\u0151",
    "is": "The Sports Analyser",
    "mk": "\u0421\u043f\u043e\u0440\u0442\u0441\u043a\u0438 \u0430\u043d\u0430\u043b\u0438\u0437\u0430\u0442\u043e\u0440"
  },
  "header_title": {
    "bg": "\u041c\u043e\u0431\u0438\u043b\u0435\u043d \u0441\u044a\u0432\u0435\u0442\u043d\u0438\u043a \u0434\u0432\u0438\u0436\u0435\u043d\u0438\u0435",
    "de": "Mobile Motion Advisor",
    "en": "Mobile Motion Advisor",
    "hu": "Mobiltelefon haszn\u00e1lati tan\u00e1csad\u00f3",
    "is": "Mobile Motion Advisor",
    "mk": "\u041c\u043e\u0431\u0438\u043b\u0435\u043d \u0441\u043e\u0432\u0435\u0442\u043d\u0438\u043a \u0437\u0430 \u0434\u0432\u0438\u0436\u0435\u045a\u0435\u0442\u043e"
  },
  "help_title": {
    "bg": "\u041f\u043e\u043c\u043e\u0449",
    "de": "Hilfe",
    "en": "Help",
    "hu": "Seg\u00edts\u00e9g",
    "is": "Hj\u00e1lp",
    "mk": "\u041f\u043e\u043c\u043e\u0448"
  },
  "home_intro": {
    "bg": "\u041d\u0430\u0447\u0430\u043b\u043e \u0432\u044a\u0432\u0435\u0436\u0434\u0430\u0449 \u0442\u0435\u043a\u0441\u0442: \u0430\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u0438",
    "de": "Home Intro Text: Deutsch",
    "en": "Home Intro Text: English",
    "hu": "Hazai bevezet\u0151 sz\u00f6veg: Magyar",
    "is": "Kynning \u00e1 heimasv\u00e6\u00f0i: Enska",
    "mk": "\u0412\u043e\u0432\u0435\u0434\u0435\u043d \u0442\u0435\u043a\u0441\u0442: \u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438"
  },
  "home_title": {
    "bg": "\u041d\u043e\u0432\u0438\u043d\u0438",
    "de": "Neuigkeiten",
    "en": "News",
    "hu": "\u00dajdons\u00e1gok",
    "is": "Fr\u00e9ttir",
    "mk": "\u0412\u0435\u0441\u0442\u0438"
  },
  "hrMax": {
    "bg": "\u0441\u044a\u0440\u0434\u0435\u0447\u0435\u043d \u0440\u0438\u0442\u044a\u043c \u043c\u0430\u043a\u0441.",
    "de": "HR-MAX",
    "en": "HR-MAX",
    "hu": "HR-MAX",
    "is": "HR-MAX",
    "mk": "HR-MAX ,\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u0435\u043d \u043f\u0443\u043b\u0441"
  },
  "hrMin": {
    "bg": "\u0441\u044a\u0440\u0434\u0435\u0447\u0435\u043d \u0440\u0438\u0442\u044a\u043c \u043c\u0438\u043d.",
    "de": "HR-MIN",
    "en": "HR-MIN",
    "hu": "HR-MIN",
    "is": "HR-MIN",
    "mk": "HR-MIN \u043c\u0438\u043d\u0438\u043c\u0430\u043c\u043b\u0435\u043d \u043f\u0443\u043b\u0441"
  },
  "hrThreshold": {
    "bg": "\u0441\u044a\u0440\u0434\u0435\u0447\u0435\u043d \u0440\u0438\u0442\u044a\u043c \u043f\u0440\u0430\u0433",
    "de": "HR-Threshold",
    "en": "HR threshold",
    "hu": "HR k\u00fcsz\u00f6b\u00e9rt\u00e9k",
    "is": "HR \u00fer\u00f6skuldur",
    "mk": "HR \u043f\u0440\u0430\u0433 \u043d\u0430 \u043f\u0443\u043b\u0441"
  },
  "hr_bpm": {
    "de": "Herzfrequenz [bpm]",
    "en": "Heartrate [bpm]"
  },
  "information": {
    "de": "Information",
    "en": "Information"
  },
  "legend": {
    "bg": "\u043b\u0435\u0433\u0435\u043d\u0434\u0430",
    "de": "Legende",
    "en": "Legend",
    "hu": "Felirat",
    "is": "Listi",
    "mk": "\u041b\u0435\u0433\u0435\u043d\u0434\u0430"
  },
  "login": {
    "bg": "\u0412\u043b\u0435\u0437 \u0432 \u043f\u0440\u043e\u0444\u0438\u043b\u0430",
    "de": "Login",
    "en": "Login",
    "hu": "Bejelentkez\u00e9s",
    "is": "Innskr\u00e1",
    "mk": "\u041b\u043e\u0433\u0438\u0440\u0430\u045a\u0435"
  },
  "loginBox_email_required": {
    "bg": "\u0418\u043c\u0435\u0439\u043b\u044a\u0442 \u0435 \u0437\u0430\u0434\u044a\u043b\u0436\u0438\u0442\u0435\u043b\u0435\u043d",
    "de": "Email ist erforderlich",
    "en": "Email is required",
    "hu": "E-mail sz\u00fcks\u00e9ges"
  },
  "loginBox_noAcc": {
    "bg": "\u041d\u044f\u043c\u0430 \u043f\u0440\u043e\u0444\u0438\u043b?",
    "de": "Kein Account?",
    "en": "No Account?",
    "hu": "Nincs felhaszn\u00e1l\u00f3i fi\u00f3kja?",
    "is": "Enginn reikningur?",
    "mk": "\u041d\u0435\u043c\u0430\u0442\u0435 \u043f\u0440\u043e\u0444\u0438\u043b?"
  },
  "loginBox_pw": {
    "bg": "\u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Passwort",
    "en": "Password",
    "hu": "Jelsz\u00f3",
    "is": "Lykilor\u00f0",
    "mk": "\u041b\u043e\u0437\u0438\u043d\u043a\u0430"
  },
  "loginBox_pw_required": {
    "bg": "\u0438\u0437\u0438\u0441\u043a\u0432\u0430 \u0441\u0435 \u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Password ist erforderlich",
    "en": "Password is required",
    "hu": "Jelsz\u00f3 sz\u00fcks\u00e9ges"
  },
  "loginBox_regNow": {
    "bg": "\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0430\u0439 \u0441\u0435 \u0441\u0435\u0433\u0430!",
    "de": "Jetzt registrieren!",
    "en": "Register now!",
    "hu": "Regisztr\u00e1ljon most!",
    "is": "Skr\u00e1 n\u00fana!",
    "mk": "\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0430\u0458\u0442\u0435 \u0441\u0435 \u0441\u0435\u0433\u0430!"
  },
  "loginBox_toReg": {
    "bg": "\u041a\u044a\u043c \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044f\u0442\u0430",
    "de": "Zur Registrierung",
    "en": "To the registration",
    "hu": "A regisztr\u00e1ci\u00f3ba",
    "is": "Til skr\u00e1ningar",
    "mk": "\u0417\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0430\u045a\u0435"
  },
  "loginBox_wrong_login": {
    "bg": "\u041f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u0441\u043a\u043e\u0442\u043e \u0438\u043c\u0435 \u0438\u043b\u0438 \u043f\u0430\u0440\u043e\u043b\u0430\u0442\u0430 \u043d\u0435 \u0441\u044a\u0432\u043f\u0430\u0434\u0430\u0442. \u041c\u043e\u043b\u044f, \u043e\u043f\u0438\u0442\u0430\u0439\u0442\u0435 \u043e\u0442\u043d\u043e\u0432\u043e.",
    "de": "Benutzername und Passwort stimmen nicht \u00fcberein. Bitte versuchen Sie es nocheinmal.",
    "en": "The username or password did not match. Please try again.",
    "hu": "A felhaszn\u00e1l\u00f3n\u00e9v vagy a jelsz\u00f3 nem egyezett. K\u00e9rlek pr\u00f3b\u00e1ld \u00fajra."
  },
  "logout": {
    "bg": "\u0418\u0437\u043b\u0435\u0437 \u043e\u0442 \u043f\u0440\u043e\u0444\u0438\u043b\u0430",
    "de": "Logout",
    "en": "Logout",
    "hu": "Kijelentkez\u00e9s",
    "is": "\u00datskr\u00e1",
    "mk": "\u041e\u0434\u043b\u043e\u0433\u0438\u0440\u0430\u045a\u0435"
  },
  "lsct_power": {
    "de": "LSCT (Power)"
  },
  "lsct_stage_power": {
    "de": "Leistung [W]"
  },
  "lscthrr": {
    "de": "LSCT (HRR)"
  },
  "lsctrpe": {
    "de": "LSCT (RPE)"
  },
  "mainMenu_admin": {
    "de": "Admin",
    "en": "Admin"
  },
  "mainMenu_currentEvents": {
    "bg": "\u0422\u0435\u043a\u0443\u0449\u0438 \u0441\u044a\u0431\u0438\u0442\u0438\u044f",
    "de": "Aktuelle Events",
    "en": "Current Events",
    "hu": "Aktu\u00e1lis esem\u00e9nyek",
    "is": "N\u00faverandi vi\u00f0bur\u00f0ir",
    "mk": "\u041c\u043e\u043c\u0435\u043d\u0442\u0430\u043b\u043d\u0438 \u043d\u0430\u0441\u0442\u0430\u043d\u0438"
  },
  "mainMenu_diary": {
    "bg": "\u0434\u043d\u0435\u0432\u043d\u0438\u043a",
    "de": "Tagebuch",
    "en": "Diary",
    "hu": "Napl\u00f3",
    "is": "T\u00f6lfr\u00e6\u00f0i",
    "mk": "\u0421\u0442\u0430\u0442\u0438\u0441\u0442\u0438\u043a\u0430"
  },
  "mainMenu_help": {
    "bg": "\u041f\u043e\u043c\u043e\u0449",
    "de": "Hilfe",
    "en": "Help",
    "hu": "Seg\u00edts\u00e9g",
    "is": "Hj\u00e1lp",
    "mk": "\u041f\u043e\u043c\u043e\u0448"
  },
  "mainMenu_home": {
    "bg": "\u041d\u0430\u0447\u0430\u043b\u043e",
    "de": "Home",
    "en": "Home",
    "hu": "Lakhely Haza",
    "is": "Heim",
    "mk": "\u0414\u043e\u043c\u0430"
  },
  "mainMenu_imprint": {
    "bg": "\u043e\u0442\u043f\u0435\u0447\u0430\u0442\u044a\u043a",
    "de": "Impressum",
    "en": "Imprint",
    "hu": "Impresszum",
    "is": "\u00c1letrun",
    "mk": "\u043e\u0442\u043f\u0435\u0447\u0430\u0442\u043e\u043a"
  },
  "mainMenu_loggedInAs": {
    "bg": "\u0412\u043b\u0435\u0437\u043b\u0438 \u0441\u0442\u0435 \u043a\u0430\u0442\u043e ",
    "de": "Eingeloggt als ",
    "en": "Logged in as ",
    "hu": "Bel\u00e9p\u00e9s m\u00e1sk\u00e9nt ",
    "is": "Skr\u00e1\u00f0ur inn sem ",
    "mk": "\u041b\u043e\u0433\u0438\u0440\u0430\u043d \u043a\u0430\u043a\u043e "
  },
  "mainMenu_personalData": {
    "bg": "\u041b\u0438\u0447\u043d\u0438 \u0434\u0430\u043d\u043d\u0438",
    "de": "Pers\u00f6nliche Daten",
    "en": "Personal Data",
    "hu": "Szem\u00e9lyes adat",
    "is": "Skr\u00e1 n\u00fana!",
    "mk": "\u041b\u0438\u0447\u043d\u0438 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438"
  },
  "mainMenu_rankings": {
    "bg": "\u041a\u043b\u0430\u0441\u0438\u0440\u0430\u043d\u0435",
    "de": "Ranglisten",
    "en": "Rankings",
    "hu": "Ranglist\u00e1k",
    "is": "R\u00f6\u00f0un",
    "mk": "\u0420\u0430\u043d\u0433\u0438\u0440\u0430\u045a\u0435"
  },
  "mainMenu_reports": {
    "bg": "\u0414\u043e\u043a\u043b\u0430\u0434\u0438",
    "de": "Reports",
    "en": "Reports",
    "hu": "Jelent\u00e9sek",
    "is": "Sk\u00fdrslur",
    "mk": "\u0418\u0437\u0432\u0435\u0448\u0442\u0430\u0438"
  },
  "mainMenu_team": {
    "bg": "\u0435\u043a\u0438\u043f",
    "de": "Team",
    "en": "Team",
    "hu": "Csapat",
    "is": "Teymi",
    "mk": "\u0422\u0438\u043c"
  },
  "mainMenu_tpm": {
    "bg": "\u0423\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b \u043d\u0430 \u0442\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u044a\u0447\u043d\u0430\u0442\u0430 \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u0430",
    "de": "Training Programme Manager",
    "en": "Training Programme Manager",
    "hu": "Edz\u00e9sprogram kezel\u0151",
    "is": "Training Programme Manager",
    "mk": "\u041f\u0440\u043e\u0433\u0440\u0430\u043c\u0441\u043a\u0438 \u043c\u0435\u043d\u0430\u045f\u0435\u0440 \u0437\u0430 \u0442\u0440\u0435\u043d\u0438\u043d\u0433"
  },
  "member_instructionsFor": {
    "bg": "\u0418\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u0438 \u0437\u0430 ",
    "de": "Anweisungen f\u00fcr ",
    "en": "Instructions for ",
    "hu": "utas\u00edt\u00e1sok ",
    "is": "Lei\u00f0beiningar fyrir ",
    "mk": "\u0418\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u0438 \u0437\u0430 "
  },
  "member_lastEdit": {
    "bg": "\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0430 \u0440\u0435\u0434\u0430\u043a\u0446\u0438\u044f: ",
    "de": "letzter Edit: ",
    "en": "last edit: ",
    "hu": "utols\u00f3 szerkeszt\u00e9s: ",
    "is": "s\u00ed\u00f0ast skr\u00e1\u00f0: ",
    "mk": "\u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0430 \u043f\u0440\u043e\u043c\u0435\u043d\u0430: "
  },
  "member_never": {
    "de": "nie",
    "en": "never",
    "hu": "soha",
    "is": "aldrei"
  },
  "message": {
    "bg": "\u0441\u044a\u043e\u0431\u0449\u0435\u043d\u0438\u0435",
    "de": "Nachricht",
    "en": "Message",
    "hu": "\u00dczenet",
    "is": "Skilabo\u00f0",
    "mk": "\u041f\u043e\u0440\u0430\u043a\u0430"
  },
  "n": {
    "bg": "\u041d\u0435",
    "de": "Nein",
    "en": "No",
    "hu": "Nem",
    "is": "Nei",
    "mk": "\u041d\u0435"
  },
  "name": {
    "bg": "\u0438\u043c\u0435",
    "de": "Name",
    "en": "Name",
    "hu": "N\u00e9v",
    "is": "Nafn",
    "mk": "\u0418\u043c\u0435"
  },
  "noIframes": {
    "bg": "\u0440\u0430\u043c\u043a\u0438 \u0437\u0430 \u0441\u044a\u0434\u044a\u0440\u0436\u0430\u043d\u0438\u0435 \u043d\u0435 \u0441\u0430 \u043d\u0430\u043b\u0438\u0446\u0435",
    "de": "iframes nicht verf\u00fcgbar",
    "en": "iframes not available",
    "hu": "A be\u00e1gyazott keret nem el\u00e9rhet\u0151",
    "is": "atri\u00f0i\u00f0 er ekki a\u00f0gengilegt",
    "mk": "iframes \u043d\u0435 \u0441\u0435 \u0434\u043e\u0441\u0442\u0430\u043f\u043d\u0438"
  },
  "oauth_access_denied": {
    "de": "Zugriff verweigert"
  },
  "oauth_not_connected": {
    "de": "Nicht verbunden"
  },
  "personalData_birthday": {
    "bg": "\u0440\u043e\u0436\u0434\u0435\u043d \u0434\u0435\u043d",
    "de": "Geburtsdatum",
    "en": "Birthday",
    "hu": "Sz\u00fclet\u00e9snap",
    "is": "F\u00e6\u00f0ingardagur",
    "mk": "\u0414\u0430\u0442\u0443\u043c \u043d\u0430 \u0440\u0430\u0453\u0430\u045a\u0435"
  },
  "personalData_curEmail": {
    "bg": "\u0410\u043a\u0442\u0443\u0430\u043b\u043d\u0430 \u0435\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430 \u043f\u043e\u0449\u0430",
    "de": "Aktuelle E-Mail",
    "en": "Current E-mail",
    "hu": "Aktu\u00e1lis E-mail",
    "is": "N\u00favernadi t\u00f6lvup\u00f3stfang",
    "mk": "\u041c\u043e\u043c\u0435\u043d\u0442\u0430\u043b\u043d\u0430\u0435 - \u043c\u0430\u0438\u043b \u0430\u0434\u0440\u0435\u0441\u0430"
  },
  "personalData_curPw": {
    "bg": "\u041d\u0430\u0441\u0442\u043e\u044f\u0449\u0430 \u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Aktuelles Passwort",
    "en": "Current password",
    "hu": "Aktu\u00e1lis jelsz\u00f3",
    "is": "N\u00favernadi lykilor\u00f0",
    "mk": "\u041c\u043e\u043c\u043d\u0435\u043d\u0442\u0430\u043b\u043d\u0430 \u043b\u043e\u0437\u0438\u043d\u043a\u0430"
  },
  "personalData_emailRepeat": {
    "bg": "\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u0442\u0435 \u043d\u043e\u0432\u0438\u044f \u0438\u043c\u0435\u0439\u043b",
    "de": "Neue E-Mail wiederholen",
    "en": "Repeat new E-mail",
    "hu": "Ism\u00e9telje meg az \u00faj E-mail c\u00edm\u00e9t",
    "is": "Endurtaka n\u00fdtt t\u00f6lvup\u00f3sfang",
    "mk": "\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u0442\u0435 \u0458\u0430 \u043d\u043e\u0432\u0430\u0442\u0430 \u0435 - \u043c\u0430\u0438\u043b \u0430\u0434\u0440\u0435\u0441\u0430"
  },
  "personalData_female": {
    "bg": "\u0416\u0435\u043d\u0430",
    "de": "Weiblich",
    "en": "Female",
    "hu": "N\u0151",
    "is": "Kona",
    "mk": "\u0416\u0435\u043d\u0441\u043a\u043e"
  },
  "personalData_forename": {
    "bg": "\u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043e \u0438\u043c\u0435",
    "de": "Vorname",
    "en": "Forename",
    "hu": "Keresztn\u00e9v",
    "is": "Nafn",
    "mk": "\u041f\u0440\u0432\u043e \u0438\u043c\u0435"
  },
  "personalData_gender": {
    "bg": "\u043f\u043e\u043b",
    "de": "Geschlecht",
    "en": "Gender",
    "hu": "Nem",
    "is": "Kyn",
    "mk": "\u041f\u043e\u043b"
  },
  "personalData_height": {
    "bg": "\u0412\u0438\u0441\u043e\u0447\u0438\u043d\u0430 [cm]",
    "de": "Gr\u00f6\u00dfe [cm]",
    "en": "Height [cm]",
    "hu": "Magass\u00e1g [cm]",
    "is": "H\u00e6\u00f0 [cm]",
    "mk": "\u0412\u0438\u0441\u0438\u043d\u0430 [\u0441\u043c]"
  },
  "personalData_hidden": {
    "bg": "\u0427\u0430\u0441\u0442\u043d\u0438\u0442\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u0438 \u0438\u043c\u0430\u0442 \u043b\u0438 \u0432\u044a\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442 \u0434\u0430 \u0432\u0438\u0436\u0434\u0430\u0442 \u043c\u043e\u0438\u0442\u0435 \u043a\u043b\u0443\u0431\u043d\u0438 \u0441\u0435\u0441\u0438\u0438?",
    "de": "Sollen private User meine Club-Sessions sehen k\u00f6nnen?",
    "en": "Are private Users allowed to see my club sessions?",
    "hu": "Lehets\u00e9ges-e, hogy a priv\u00e1t felhaszn\u00e1l\u00f3k l\u00e1thass\u00e1k a klubszekci\u00f3kat?",
    "is": "Eru einst\u00f6kum notendum heimilt a\u00f0 sj\u00e1 \u00e6fingar kl\u00fabbsins?",
    "mk": "\u0414\u0430\u043b\u0438 \u043d\u0430 \u043b\u0438\u0447\u043d\u0438\u0442\u0435 \u043a\u043e\u0440\u0438\u0441\u043d\u0438\u0446\u0438 \u0438\u043c \u0435 \u0434\u043e\u0437\u0432\u043e\u043b\u0435\u043d\u043e \u0434\u0430 \u0433\u0438 \u0433\u043b\u0435\u0434\u0430\u0430\u0441\u0442 \u043c\u043e\u0438\u0442\u0435 \u0441\u0435\u0441\u0438\u0438?"
  },
  "personalData_intro": {
    "bg": "\u0422\u0443\u043a \u043c\u043e\u0436\u0435\u0442\u0435 \u0434\u0430 \u043f\u0440\u043e\u043c\u0435\u043d\u0438\u0442\u0435 \u043b\u0438\u0447\u043d\u0438\u0442\u0435 \u0441\u0438 \u0434\u0430\u043d\u043d\u0438 - \u0438\u043c\u0435, \u0430\u0434\u0440\u0435\u0441, \u0440\u0430\u0437\u043c\u0435\u0440 \u0438 \u0442.\u043d., \u043a\u0430\u043a\u0442\u043e \u0438 \u043f\u0430\u0440\u043e\u043b\u0430\u0442\u0430 \u0438 \u043f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u0441\u043a\u043e\u0442\u043e \u0441\u0438 \u0438\u043c\u0435.",
    "de": "Hier k\u00f6nnen Sie ihre pers\u00f6nlichen Daten bearbeiten.",
    "en": "Here you can modify your personal details like name, address, size, etc. as well as your password and user name.",
    "hu": "Itt tudja m\u00f3dos\u00edtani a szem\u00e9lyes be\u00e1ll\u00edt\u00e1sait, nev\u00e9t, c\u00edm\u00e9t, m\u00e9ret\u00e9t, stb. ezen k\u00edv\u00fcl a jelszav\u00e1t \u00e9s a felhaszn\u00e1l\u00f3nev\u00e9t.",
    "is": "H\u00e9r getur \u00fe\u00fa laga\u00f0 \u00fe\u00ednar pers\u00f3nulegu uppl\u00fdsinar svo sem nafn, heimilsfang, st\u00e6r\u00f0 o.\u00fe.h en l\u00edka lykilor\u00f0 og notendanafn",
    "mk": "\u041e\u0432\u0434\u0435 \u043c\u043e\u0436\u0435 \u0434\u0430 \u0433\u0438 \u043c\u0435\u043d\u0443\u0432\u0430\u0442\u0435 \u0412\u0430\u0448\u0438\u0442\u0435 \u043b\u0438\u0447\u043d\u0438 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438 \u043a\u0430\u043a\u043e: \u0438\u043c\u0435, \u0430\u0434\u0440\u0435\u0441\u0430, \u0435 - \u043c\u0430\u0438\u043b, \u043b\u043e\u0437\u0438\u043d\u043a\u0430 \u0438 \u0441\u043b."
  },
  "personalData_mailchange_notfound": {
    "de": "Ein Fehler ist w\u00e4hrend der E-Mail-\u00c4nderung aufgetreten",
    "en": "An error has happened during your email change."
  },
  "personalData_mailchange_pending": {
    "de": "Sie haben bereits eine \u00c4nderung Ihrer E-Mail beantragt. Bitte versuchen Sie es sp\u00e4ter erneut.",
    "en": "You have requested an email change already. Please try again later."
  },
  "personalData_mailinuse": {
    "de": "Die eingegebene E-Mail Adresse wird bereits verwendet.",
    "en": "The address you have entered is already in use."
  },
  "personalData_mailsend_failed": {
    "de": "Ein Fehler ist w\u00e4hrend der E-Mail-\u00c4nderung aufgetreten",
    "en": "An error has happened during your email change."
  },
  "personalData_male": {
    "bg": "\u041c\u044a\u0436",
    "de": "M\u00e4nnlich",
    "en": "Male",
    "hu": "F\u00e9rfi",
    "is": "Karl",
    "mk": "\u041c\u0430\u0448\u043a\u043e"
  },
  "personalData_newEmail": {
    "bg": "\u041d\u043e\u0432 \u0438\u043c\u0435\u0439\u043b",
    "de": "Neue E-Mail",
    "en": "New E-mail",
    "hu": "\u00daj E-mail",
    "is": "N\u00fdtt t\u00f6lvup\u00f3stfang",
    "mk": "\u041d\u043e\u0432\u0430 \u0435 -\u043c\u0430\u0438\u043b \u0430\u0434\u0440\u0435\u0441\u0430"
  },
  "personalData_newPw": {
    "bg": "\u041d\u043e\u0432\u0430 \u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Neues Passwort",
    "en": "New password",
    "hu": "\u00daj jelsz\u00f3",
    "is": "N\u00fdtt lykilor\u00f0",
    "mk": "\u041d\u043e\u0432\u0430 \u043b\u043e\u0437\u0438\u043d\u043a\u0430"
  },
  "personalData_performance": {
    "bg": "\u0421\u043f\u0435\u0446\u0438\u0444\u0438\u0447\u043d\u0438 \u0434\u0430\u043d\u043d\u0438 \u0437\u0430 \u0440\u0435\u0437\u0443\u043b\u0442\u0430\u0442",
    "de": "Performance-spezifische Daten",
    "en": "Performance Specific Data",
    "hu": "V\u00e9grehajt\u00e1shoz sz\u00fcjks\u00e9ges adat",
    "is": "G\u00f6gn um s\u00e9rstakan \u00e1rangur",
    "mk": "\u0421\u043f\u0435\u0446\u0438\u0444\u0438\u0447\u043d\u0438 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438 \u0437\u0430 \u0438\u0437\u0432\u0435\u0434\u0431\u0430\u0442\u0430"
  },
  "personalData_pwRepeat": {
    "bg": "\u041f\u043e\u0432\u0442\u043e\u0440\u0438 \u043d\u043e\u0432\u0430\u0442\u0430 \u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Neues Passwort wiederholen",
    "en": "Repeat new password",
    "hu": "Ism\u00e9telje meg az \u00faj jelszav\u00e1t",
    "is": "Endurtaka n\u00fdtt lykilor\u00f0",
    "mk": "\u041f\u043e\u0432\u0442\u043e\u0440\u0435\u0442\u0435 \u0458\u0430 \u043d\u043e\u0432\u0430\u0442\u0430 \u043b\u043e\u0437\u0438\u043d\u043a\u0430"
  },
  "personalData_settings": {
    "bg": "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438",
    "de": "Einstellungen",
    "en": "Settings",
    "hu": "Be\u00e1ll\u00edt\u00e1sok",
    "is": "Stillingar",
    "mk": "\u041f\u043e\u0434\u0435\u0441\u0443\u0432\u0430\u045a\u0435"
  },
  "personalData_surname": {
    "bg": "\u0444\u0430\u043c\u0438\u043b\u043d\u043e \u0438\u043c\u0435",
    "de": "Nachname",
    "en": "Surname",
    "hu": "Vezet\u00e9kn\u00e9v",
    "is": "Eftirnafn",
    "mk": "\u041f\u0440\u0435\u0437\u0438\u043c\u0435"
  },
  "personalData_threshold": {
    "bg": "\u043f\u0440\u0430\u0433",
    "de": "Schwelle",
    "en": "Threshold",
    "hu": "Kiindul\u00f3pont",
    "is": "\u00der\u00f6skuldur",
    "mk": "\u043f\u0440\u0430\u0433"
  },
  "personalData_title": {
    "bg": "\u041b\u0438\u0447\u043d\u0438 \u0434\u0430\u043d\u043d\u0438",
    "de": "Pers\u00f6nliche Daten",
    "en": "Personal Data",
    "hu": "Szem\u00e9lyes adat",
    "is": "Pers\u00f3nuleg g\u00f6gn",
    "mk": "\u041b\u0438\u0447\u043d\u0438 \u043f\u043e\u0434\u0430\u0442\u043e\u0446\u0438"
  },
  "personalData_vo2max": {
    "bg": "VO2max",
    "de": "VO2Max",
    "en": "VO2max",
    "hu": "VO2max",
    "is": "VO2Max",
    "mk": "VO2max"
  },
  "personalData_weight": {
    "bg": "\u0422\u0435\u0433\u043b\u043e [kg]",
    "de": "Gewicht [kg]",
    "en": "Weight [kg]",
    "hu": "S\u00faly [kg]",
    "is": "\u00deyngd [kg]",
    "mk": "\u0422\u0435\u0436\u0438\u043d\u0430 [\u043a\u0433]"
  },
  "powerp": {
    "de": "Power Profile"
  },
  "registration_confirmPw": {
    "bg": "\u041f\u043e\u0442\u0432\u044a\u0440\u0434\u0438 \u043f\u0430\u0440\u043e\u043b\u0430",
    "de": "Passwort best\u00e4tigen",
    "en": "Confirm password",
    "hu": "Jelsz\u00f3 meger\u0151s\u00edt\u00e9se"
  },
  "registration_emailValid": {
    "bg": "\u0418\u0437\u0438\u0441\u043a\u0432\u0430 \u0441\u0435 \u0432\u0430\u043b\u0438\u0434\u0435\u043d \u0438\u043c\u0435\u0439\u043b",
    "de": "Eine g\u00fcltige Email ist erforderlich",
    "en": "A valid email is required",
    "hu": "\u00c9rv\u00e9nyes e-mail sz\u00fcks\u00e9ges"
  },
  "registration_firstName_required": {
    "bg": "\u0418\u043c\u0435\u0442\u043e \u0435 \u0437\u0430\u0434\u044a\u043b\u0436\u0438\u0442\u0435\u043b\u043d\u043e",
    "de": "Vorname ist erforderlich",
    "en": "First Name is required",
    "hu": "Vezet\u00e9kn\u00e9v sz\u00fcks\u00e9ges"
  },
  "registration_lastName_required": {
    "bg": "\u0424\u0430\u043c\u0438\u043b\u043d\u043e\u0442\u043e \u0438\u043c\u0435 \u0435 \u0437\u0430\u0434\u044a\u043b\u0436\u0438\u0442\u0435\u043b\u043d\u043e",
    "de": "Nachname ist erforderlich",
    "en": "Last Name is required",
    "hu": "Vezet\u00e9kn\u00e9v sz\u00fcks\u00e9ges"
  },
  "reports_axis_Day": {
    "de": "Tag",
    "en": "Day"
  },
  "reports_axis_Month": {
    "de": "Monat",
    "en": "Month"
  },
  "reports_axis_Week": {
    "de": "Woche",
    "en": "Week"
  },
  "reports_axis_Year": {
    "de": "Jahr",
    "en": "Year"
  },
  "reports_show_report": {
    "bg": "\u043f\u043e\u043a\u0430\u0437\u0432\u0430\u043d\u0435",
    "de": "Anzeigen",
    "en": "Show",
    "hu": "Mutat",
    "is": "S\u00fdna",
    "mk": "\u041f\u043e\u043a\u0430\u0436\u0438"
  },
  "reports_title": {
    "bg": "\u043e\u0442\u0447\u0435\u0442\u0438",
    "de": "Reports",
    "en": "Reports",
    "hu": "Jelent\u00e9sek",
    "is": "Sk\u00fdrslur",
    "mk": "\u0418\u0437\u0432\u0435\u0448\u0442\u0430\u0438"
  },
  "save": {
    "bg": "\u0417\u0430\u043f\u0430\u0437\u0438",
    "de": "Speichern",
    "en": "Save",
    "hu": "Ment\u00e9s",
    "is": "Vista",
    "mk": "\u0437\u0430\u0447\u0443\u0432\u0430\u0458"
  },
  "send": {
    "bg": "\u0438\u0437\u043f\u0440\u0430\u0449\u0430\u043c",
    "de": "Abschicken",
    "en": "Send",
    "hu": "K\u00fcld\u00e9s",
    "is": "Senda",
    "mk": "\u041f\u0440\u0430\u0442\u0438"
  },
  "speed_kmh": {
    "de": "Geschwindigkeit [km/h]",
    "en": "Speed [km/h]"
  },
  "team_add_group_member_failed": {
    "de": "Hinzuf\u00fcgen fehlgeschlagen"
  },
  "team_choose": {
    "bg": "\u041c\u043e\u0436\u0435\u0442\u0435 \u0434\u0430 \u0438\u0437\u0431\u0435\u0440\u0435\u0442\u0435 \u043e\u0442\u0431\u043e\u0440 \u0432 \u0433\u043e\u0440\u043d\u0438\u044f \u0434\u0435\u0441\u0435\u043d \u044a\u0433\u044a\u043b, \u0430\u043a\u043e \u0441\u0442\u0435 \u0447\u043b\u0435\u043d \u043d\u0430 \u043e\u0442\u0431\u043e\u0440\u0430.",
    "de": "Sobald Sie Mitglied in einem Team sind, k\u00f6nnen Sie hier rechts oben das jeweilige Team ausw\u00e4hlen.",
    "en": "You can choose a team in the upper right corner if you're a member of the team.",
    "hu": "Amint \u00d6n egy csapat tagja, kiv\u00e1laszthatja a megfelel\u0151 csapatot a jobb fels\u0151 sarokban.",
    "is": "You can choose a team in the upper right corner if you're a member of the team.",
    "mk": "\u0414\u043e\u043a\u043e\u043b\u043a\u0443 \u0441\u0442\u0435 \u0447\u043b\u0435\u043d \u043d\u0430 \u0442\u0438\u043c\u043e\u0442 \u043c\u043e\u0436\u0435\u0442\u0435 \u0434\u0430 \u043e\u0434\u0431\u0435\u0440\u0435\u0442\u0435 \u0442\u0438\u043c \u0432\u043e \u0433\u043e\u0440\u043d\u0438\u043e\u0442 \u0434\u0435\u0441\u0435\u043d \u0430\u0433\u043e\u043b"
  },
  "team_create": {
    "de": "Erstellen",
    "en": "Create"
  },
  "team_createText": {
    "de": "Hier k\u00f6nnen Sie ein neues Team erstellen",
    "en": "Here you can create a team"
  },
  "team_deleteGroup": {
    "de": "Gruppe l\u00f6schen",
    "en": "Delete group"
  },
  "team_deleteMemberTeam": {
    "de": "delete member from team",
    "en": "delete member from team"
  },
  "team_deleteUserFromGroup": {
    "de": "Mitglied aus Gruppe entfernen",
    "en": "remove member from group"
  },
  "team_edit": {
    "bg": "\u0417\u0430 \u0434\u0430 \u0440\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u0430\u0442\u0435 \u0433\u0440\u0443\u043f\u0430 \u0438\u043b\u0438 \u0438\u043c\u0435 \u043d\u0430 \u0433\u0440\u0443\u043f\u0430, \u0449\u0440\u0430\u043a\u043d\u0435\u0442\u0435 \u0434\u0432\u0443\u043a\u0440\u0430\u0442\u043d\u043e \u0432\u044a\u0440\u0445\u0443 \u0441\u044a\u043e\u0442\u0432\u0435\u0442\u043d\u043e\u0442\u043e \u0438\u043c\u0435.",
    "de": "Um einen Team- oder Gruppennamen zu bearbeiten, doppelklicken Sie einfach direkt auf den jeweiligen Namen.",
    "en": "To edit a team or group name  double click on the respective name.",
    "hu": "Csapat vagy csoportn\u00e9v szerkeszt\u00e9s\u00e9hez csak kattintson dupl\u00e1n a nev\u00e9re",
    "is": "To edit a team or group name  double click on the respective name.",
    "mk": "\u0417\u0430 \u0434\u0430 \u043d\u0430\u043f\u0440\u0430\u0432\u0438\u0442\u0435 \u043f\u0440\u043e\u043c\u0435\u043d\u0430 \u043d\u0430 \u0438\u043c\u0435\u0442\u043e \u043d\u0430 \u0433\u0440\u0443\u043f\u0430\u0442\u0430 \u043a\u043b\u0438\u043a\u043d\u0435\u0442\u0435 \u0434\u0432\u0430\u043f\u0430\u0442\u0438 \u043d\u0430 \u0441\u043e\u043e\u0434\u0432\u0435\u0442\u043d\u043e\u0442\u043e \u0438\u043c\u0435"
  },
  "team_editAssignments": {
    "de": "Aufgaben des Mitglieds bearbeiten",
    "en": "Edit member's assignments"
  },
  "team_group_member_added": {
    "de": "Person wurde zur Gruppe hinzugef\u00fcgt"
  },
  "team_group_member_removed": {
    "de": "Person wurde aus der Gruppe entfernt"
  },
  "team_groups": {
    "bg": "\u0413\u0440\u0443\u043f\u0438",
    "de": "Gruppen",
    "en": "Groups",
    "hu": "Csoport",
    "is": "Groups",
    "mk": "\u0413\u0440\u0443\u043f\u0438"
  },
  "team_info_groups": {
    "de": "Um eine neue Gruppe zu erstellen, geben Sie in in das Textfeld in dieser Spalte unten den gew\u00fcnschten Gruppennamen ein, und klicken sie anschlie\u00dfend auf den Button. Per Klick auf die Titelzeile einer Gruppe \u00f6ffnen sich die Eigenschaften der Gruppe. Darin sehen sie im Anschluss alle Mitglieder, die sie dieser Gruppe zugewiesen haben. Sie k\u00f6nnen nat\u00fcrlich auch wieder Mitglieder aus einer Gruppe entfernen, indem sie auf das Papierkorb-Symbol in den Gruppeneigenschaften klicken."
  },
  "team_info_members": {
    "de": "Um ein Mitglied einer Gruppe zuzuweisen, ziehen Sie die Person einfach mit der Maus in die entsprechende Gruppe. Ein Mitglied kann immer nur einer Gruppe zugewiesen sein. Um eine Person in dieses Team einzuladen, geben Sie in das Formular unten einfach die E-Mail Adresse des Mitglieds ein. Wird Ihre Einladung best\u00e4tigt, so erhalten Sie eine Benachrichtigung."
  },
  "team_invite": {
    "bg": "\u041f\u043e\u043a\u0430\u043d\u0438",
    "de": "Einladen",
    "en": "Invite",
    "hu": "Invit\u00e1l",
    "is": "Invite",
    "mk": "\u041f\u043e\u043a\u0430\u043d\u0438"
  },
  "team_inviteEmail": {
    "bg": "\u041f\u043e\u043a\u0430\u043d\u0438 \u043f\u043e \u0438\u043c\u0435\u0439\u043b",
    "de": "Via E-Mail Adresse einladen",
    "en": "Invite via E-mail address",
    "hu": "Invit\u00e1l E-mail c\u00edmmel",
    "is": "Invite via E-mail address",
    "mk": "\u041f\u043e\u043a\u0430\u043d\u0438 \u043f\u0440\u0435\u043a\u0443 \u0435 - \u043c\u0430\u0438\u043b \u0430\u0434\u0440\u0435\u0441\u0430"
  },
  "team_invite_accept": {
    "bg": "\u043f\u0440\u0438\u0435\u043c\u0430\u043c",
    "de": "Annehmen",
    "en": "Annehmen",
    "hu": "elfogad",
    "mk": "\u041f\u0440\u0438\u0444\u0430\u0442\u0438"
  },
  "team_invite_decline": {
    "bg": "\u043e\u0442\u043a\u0430\u0437\u0432\u0430\u043c",
    "de": "Ablehnen",
    "en": "Ablehnen",
    "hu": "visszautas\u00edt",
    "mk": "\u041f\u0430\u0434"
  },
  "team_member_removed": {
    "de": "Person wurde aus dem Team entfernt"
  },
  "team_members": {
    "bg": "\u0427\u043b\u0435\u043d\u043e\u0432\u0435",
    "de": "Mitglieder",
    "en": "Members",
    "hu": "Tagok",
    "is": "Members",
    "mk": "\u0427\u043b\u0435\u043d\u043e\u0432\u0438"
  },
  "team_name": {
    "de": "Name",
    "en": "Name"
  },
  "team_name_required": {
    "de": "Name ist erforderlich",
    "en": "Name is required"
  },
  "team_newGroup": {
    "bg": "\u041d\u043e\u0432\u0430 \u0433\u0440\u0443\u043f\u0430",
    "de": "Neue Gruppe",
    "en": "New group",
    "hu": "\u00daj csoport",
    "is": "New group",
    "mk": "\u041d\u043e\u0432\u0438 \u0433\u0440\u0443\u043f\u0438"
  },
  "team_noGroup": {
    "bg": "\u041d\u0435 \u0441\u0442\u0435 \u0441\u0435 \u0432\u043a\u043b\u044e\u0447\u0438\u043b\u0438 \u0432 \u0433\u0440\u0443\u043f\u0430 \u0432 \u0442\u043e\u0437\u0438 \u043e\u0442\u0431\u043e\u0440.",
    "de": "Sie sind keiner Gruppe im Team zugewiesen.",
    "en": "You are not assigned to a group in this team.",
    "hu": "\u00d6n nincs  a csoprthoz hozz\u00e1rendelve.",
    "is": "You are not assigned to a group in this team.",
    "mk": "\u041d\u0435 \u0441\u0442\u0435 \u043d\u0430\u0437\u043d\u0430\u0447\u0435\u043d\u0438 \u0432\u043e \u0433\u0440\u0443\u043f\u0430\u0442\u0430 \u043d\u0430 \u043e\u0432\u043e\u0458 \u0442\u0438\u043c"
  },
  "team_noMembers": {
    "bg": "\u0422\u043e\u0437\u0438 \u043e\u0442\u0431\u043e\u0440 \u043d\u044f\u043c\u0430 \u0447\u043b\u0435\u043d\u043e\u0432\u0435 \u0432 \u043c\u043e\u043c\u0435\u043d\u0442\u0430. \u0417\u0430\u0434\u0430\u0439\u0442\u0435 \u0447\u043b\u0435\u043d\u043e\u0432\u0435 \u0447\u0440\u0435\u0437 drag & drop.",
    "de": "Dieses Team hat noch keine Mitglieder. Weisen Sie Mitglieder per Drag & Drop zu.",
    "en": "This team does not have members at the  moment. Assign members by drag & drop.",
    "hu": "Ennek a csapatnak m\u00e9g nincs tagja. Tagok hozz\u00e1rendel\u00e9se a drag & drop seg\u00edts\u00e9g\u00e9vel.",
    "is": "This team does not have members at the  moment. Assign members by drag & drop.",
    "mk": "\u041e\u0432\u043e\u0458 \u0442\u0438\u043c \u043c\u043e\u043c\u0435\u043d\u0442\u0430\u043b\u043d\u043e \u043d\u0435\u043c\u0430 \u0447\u043b\u0435\u043d\u043e\u0432\u0438. \u0414\u043e\u0434\u0430\u0434\u0435\u0442\u0435 \u0447\u043b\u0435\u043d\u043e\u0432\u0438 \u0441\u043e \u043e\u0431\u0435\u043b\u0435\u0436\u0443\u0432\u0430\u045a\u0435 \u0438 \u043f\u043e\u0432\u043b\u0435\u043a\u0443\u0432\u0430\u045a\u0435"
  },
  "team_remove_member_failed": {
    "de": "Entfernen fehlgeschlagen"
  },
  "team_request": {
    "bg": "\u0418\u0441\u043a\u0430\u0442\u0435 \u043b\u0438 \u0434\u0430 \u0441\u0435 \u043f\u0440\u0438\u0441\u044a\u0435\u0434\u0438\u043d\u0438\u0442\u0435 \u043a\u044a\u043c \u043e\u0442\u0431\u043e\u0440? \u0412\u044a\u0432\u0435\u0434\u0435\u0442\u0435 \u0438\u043c\u0435\u0439\u043b \u0430\u0434\u0440\u0435\u0441 \u043d\u0430 \u043b\u0438\u0434\u0435\u0440\u0430 \u043d\u0430 \u0435\u043a\u0438\u043f\u0430, \u0437\u0430 \u0434\u0430 \u0438\u0437\u043f\u0440\u0430\u0442\u0438\u0442\u0435 \u0437\u0430\u044f\u0432\u043a\u0430.",
    "de": "Sie wollen dem Team eines Bekannten beitreten? Geben Sie hier einfach die E-Mail Adresse des Teamgr\u00fcnders ein, um ihm eine Anfrage zu schicken.",
    "en": "Do you want to join a team? Enter the team leader's email address to send a request.",
    "hu": "Szeretne csatlakozni egy ismer\u0151se csapat\u00e1hoz? Egyszer\u0171en csak \u00edrjon a csapat alap\u00edt\u00f3j\u00e1nak egy k\u00e9r\u00e9s\u00e9t e-mailben.",
    "is": "Do you want to join a team? Enter the team leader's email address to send a request.",
    "mk": "\u0414\u0430\u043b\u0438 \u0441\u0430\u043a\u0430\u0442\u0435 \u0434\u0430 \u0441\u0435 \u043f\u0440\u0438\u043a\u043b\u0443\u0447\u0438\u0442\u0435 \u043d\u0430 \u043d\u0435\u043a\u043e\u0458 \u0442\u0438\u043c? \u0412\u043d\u0435\u0441\u0435\u0442\u0435 \u0458\u0430 \u0435 - \u043c\u0430\u0438\u043b \u0430\u0434\u0440\u0435\u0441\u0430\u0442\u0430 \u043d\u0430 \u043b\u0438\u0434\u0435\u0440\u043e\u0442 \u043d\u0430 \u0442\u0438\u043c\u043e\u0442 \u0437\u0430 \u0434\u0430 \u043f\u0440\u0430\u0442\u0438\u0442\u0435 \u0431\u0430\u0440\u0430\u045a\u0435"
  },
  "team_requestTitle": {
    "bg": "\u0418\u0437\u043f\u0440\u0430\u0442\u0435\u0442\u0435 \u0437\u0430\u044f\u0432\u043a\u0430 \u0434\u043e \u043e\u0442\u0431\u043e\u0440\u0430",
    "de": "Anfrage an Team versenden",
    "en": "Send request to team",
    "hu": "K\u00e9r\u00e9s k\u00fcld\u00e9se a caspatnak.",
    "is": "Send request to team",
    "mk": "\u041f\u0440\u0430\u0442\u0435\u0442\u0435 \u0431\u0430\u0440\u0430\u045a\u0435 \u0434\u043e \u0442\u0438\u043c\u043e\u0442"
  },
  "team_showHideGroups": {
    "bg": "\u041f\u043e\u043a\u0430\u0437\u0432\u0430\u043d\u0435 / \u0441\u043a\u0440\u0438\u0432\u0430\u043d\u0435 \u043d\u0430 \u0432\u0441\u0438\u0447\u043a\u0438 \u0433\u0440\u0443\u043f\u0438",
    "de": "Zeige/verstecke alle Gruppen",
    "en": "Show/Hide all groups",
    "hu": "Mutassa/Rejtse el az \u00f6sszes csoportot",
    "is": "Show/Hide all groups",
    "mk": "\u041f\u043e\u043a\u0430\u0436\u0438/\u0441\u043a\u0440\u0438\u0458 \u0433\u0438 \u0441\u0438\u0442\u0435 \u0433\u0440\u0443\u043f\u0438"
  },
  "team_title": {
    "bg": "\u041e\u0442\u0431\u043e\u0440",
    "de": "Team",
    "en": "Team",
    "hu": "Csapat",
    "is": "Team",
    "mk": "\u0415\u043a\u0438\u043f\u0430"
  },
  "test": {
    "de": "Gewicht"
  },
  "timeinz": {
    "de": "Zeit in Zone"
  },
  "tinz": {
    "de": "Zeit in Power Zone"
  },
  "to": {
    "bg": "\u0434\u043e",
    "de": "bis",
    "en": "to",
    "hu": "-ba",
    "is": "til",
    "mk": "\u0434\u043e"
  },
  "value_BODYFAT": {
    "de": "K\u00f6rpferfett [%]"
  },
  "value_FTP": {
    "de": "FTP"
  },
  "value_POWER_ZONE_1": {
    "de": "Power Zone 1"
  },
  "value_POWER_ZONE_2": {
    "de": "Power Zone 2"
  },
  "value_POWER_ZONE_3": {
    "de": "Power Zone 3"
  },
  "value_POWER_ZONE_4": {
    "de": "Power Zone 4"
  },
  "value_POWER_ZONE_5": {
    "de": "Power Zone 5"
  },
  "value_POWER_ZONE_6": {
    "de": "Power Zone 6"
  },
  "value_POWER_ZONE_7": {
    "de": "Power Zone 7"
  },
  "value_add": {
    "de": "Neuen Eintrag f\u00fcr diesen Wert hinzuf\u00fcgen",
    "en": "Add new entry for this value"
  },
  "value_date_end": {
    "de": "Bis",
    "en": "End"
  },
  "value_date_start": {
    "de": "Von",
    "en": "Start"
  },
  "value_delete": {
    "de": "Diesen Eintrag l\u00f6schen",
    "en": "Delete this entry"
  },
  "value_deleteValueConfirm": {
    "de": "Sind Sie sicher? Das L\u00f6schen eines Wertes kann nicht r\u00fcckg\u00e4ngig gemacht werden.",
    "en": "Are you sure? Deleting a value cannot be undone."
  },
  "value_deleteValueFailed": {
    "de": "L\u00f6schen ist fehlgeschlagen",
    "en": "Deleting value failed"
  },
  "value_edit": {
    "de": "Diesen Eintrag bearbeiten",
    "en": "Edit this entry"
  },
  "value_edit_value": {
    "de": "Eintr\u00e4ge f\u00fcr diesen Wert bearbeiten",
    "en": "Edit entries for this value"
  },
  "value_value": {
    "de": "Wert",
    "en": "Value"
  },
  "value_warningConsistentState": {
    "de": "Achtung: Eintr\u00e4ge m\u00fcssen immer einen konsistenten Status haben (d.h. zB keine Eintr\u00e4ge mit \u00fcberlappenden Von / Bis daten.)!",
    "en": "Remember: Make sure the value history is in a consistent state after your changes!"
  },
  "vo2max": {
    "bg": "VO2max",
    "de": "VO2max",
    "en": "VO2max",
    "hu": "VO2max",
    "is": "VO2max",
    "mk": "VO2max"
  },
  "y": {
    "bg": "\u0434\u0430",
    "de": "Ja",
    "en": "Yes",
    "hu": "Igen",
    "is": "J\u00e1",
    "mk": "\u0414\u0430"
  }
}