import { ActionComponentsModule } from './../actioncomponents/action.components.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DragDropModule } from 'primeng/dragdrop';
import { UserModule } from '../user/user.module';

import { TeamComponent } from './team.component';

const routes: Routes = [
  { path: '', component: TeamComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    FontAwesomeModule,
    DragDropModule,
    ActionComponentsModule,
    UserModule,
  ],
  exports: [RouterModule],
  declarations: [
    TeamComponent
  ]
})
export class TeamModule {

}
