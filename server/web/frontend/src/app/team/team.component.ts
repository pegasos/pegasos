import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { HostBinding } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  AUTO_STYLE,
  // ...
} from '@angular/animations';

import { AppService } from './../services/app.service';

import { TeamWithGroups, TeamWithUsers, TeamWithGroupsWithUsers, TUser, Response, GroupWithUsers } from './../shared/sample';
import { DialogService } from '../dialog/dialog.service';
import { UserComponent } from '../user/user.component';
import { Group } from '../shared/person';

@Component({
  selector: 'app-team',
  animations: [
    trigger('openClose', [
      state('collapsed', style({
        height: 0,
        paddingTop: 0,
        paddingBottom: 0,
        opacity: 0
      })),
      state('expanded', style({
        // height: '*',          // Height depends upon the content. So Angular computes it dynamically
        height: 'auto',
        padding: '*',
        overflow: 'auto'
      })),
      transition('collapsed => expanded', [
        animate('100ms ease-out', style({
          height: '*',
          paddingTop: '*',
          paddingBottom: '*'
        })),
        animate('100ms', style({ opacity: 1 }))
      ]),
      transition('expanded => collapsed', [
        animate('300ms ease-in')
      ])
    ]),
  ],
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit/*, AfterViewInit*/ {

  teams!: TeamWithGroups[];

  /**
   * Currently selected team. Only accessed from html
   */
  protected selected_team!: TeamWithUsers | undefined;
  protected selected_teamgroups!: TeamWithGroupsWithUsers | undefined;

  private team_data = new Map<number, TeamWithUsers>();

  private group_data = new Map<number, TeamWithGroupsWithUsers>();

  public team_selected = false;

  private members_received = false;
  private groups_received = false;

  protected groupOpened: {[key:string]:'collapsed'|'expanded'};

  /**
   * Form for adding a team
   */
  createForm!: FormGroup;

  /**
   * Form for inviting a person to a team
   */
  inviteForm!: FormGroup;

  /**
   * True if an action is currently running
   */
  public loading!: boolean;

  public loadingInvite!: boolean;

  /**
   * whether the form has been submitted
   */
  public submitted!: boolean;
  submittedInvite!: boolean;
  inviteFormError!: string | undefined;
  draggedPerson!: TUser | null;

  constructor(private app: AppService,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    public ds: DialogService) {
    this.team_selected = false;
    this.groupOpened = {};
  }

  ngOnInit() {
    this.app.startLoading();
    this.createForm = this.formBuilder.group({
      name: ['', [Validators.required]],
    });

    this.inviteForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

    this.app.setTitle(this.app.translateString('mainMenu_team'));

    this.app.translateTree('team-create');

    this.app.getResource('v1/teams/myteams').subscribe(data => this.onDataReceived(data));
  }

  private onDataReceived(data: TeamWithGroups[]): void {
    this.app.stopLoading();

    this.teams = data;
  }

  public onSelectTeam(team: TeamWithGroups): void {
    if ( this.team_data.has(team.teamid) ) {
      this.team_selected = true;
      this.selected_team = this.team_data.get(team.teamid);
      this.selected_teamgroups = this.group_data.get(team.teamid);

      this.cd.detectChanges();
      this.app.translateTree('team-container');
    } else {
      this.app.startLoading();
      this.team_selected = false;
      this.members_received = this.groups_received = false;
      this.app.getResource('v1/teams/team/' + team.teamid).subscribe(data => this.onTeamReceived(data));
      this.app.getResource('v1/teams/teamgroups/' + team.teamid).subscribe(data => this.onTeamGroupsReceived(data));
    }
  }

  private onTeamReceived(team: TeamWithUsers[]): void {
    this.members_received = true;
    this.team_data.set(team[0].teamid, team[0]);
    this.selected_team = team[0];
    this.onTD();
  }

  private onTeamGroupsReceived(team: TeamWithGroupsWithUsers[]): void {
    this.groups_received = true;
    this.group_data.set(team[0].teamid, team[0]);
    this.selected_teamgroups = team[0];
    this.selected_teamgroups.groups.forEach(g => this.groupOpened[g.groupid] = 'collapsed');
    this.onTD();
  }

  private onTD(): void {
    if ( this.members_received && this.groups_received ) {
      this.team_selected = true;
      this.app.stopLoading();

      this.cd.detectChanges();
      // TODO: translate only team content?
      this.app.translateTree('team-container');
    }
  }

  openGroup(groupId: number): void {
    this.groupOpened[groupId] = this.groupOpened[groupId] === 'collapsed' ? 'expanded' : 'collapsed';
  }

  openAllGroups(): void {
    if (!this.selected_teamgroups)
      return;
    this.selected_teamgroups.groups.forEach(g => {
      this.groupOpened[g.groupid] = (this.groupOpened[g.groupid] === 'collapsed' ? 'expanded' : 'collapsed');
    });
  }

  /**
   * Edit a user
   * @param user user to be edited
   */
  editUser(user: TUser): void {
    console.log('edit user', user);
    const ref = this.ds.open(UserComponent, { data: { message: 'I am a dynamic component inside of a dialog!',
      userId: user.id } });

    ref.afterClosed.subscribe(result => {
      console.log('Dialog closed', result);
    });
  }

  public create(): void {
    this.submitted = true;

    if (this.createForm.invalid) {
      this.cd.detectChanges();
      this.app.translateContent();
      return;
    } else {
      this.loading = true;

      this.app.getResourcePost('v1/teams', this.createForm.value).subscribe(
        (data) => {
          if ( data.message === 'Created' ) {
            // alert('Created');
            const team: TeamWithGroups = {
              teamid: data.id,
              name: this.createForm.get('name')?.value,
              captains: [],
              groups: []
            };
            this.teams.push(team);
            this.createForm.reset();
            this.app.translateTree('team-create');
          } else if ( data.message === 'Fail' ) {
            alert('Fail: ' + data.error);
          }
          this.loading = false;
        },
        (err) => {
          if ( err.message === 'user_exists' ) {
            alert('User already in db or in temp registration');
          } else {
            alert('Other error' + err.message);
          }
          this.loading = false;
        }
      );
    }
  }

  deleteGroup(groupId: number): void {
    alert('Not implemented');
  }

  /**
   * Remove user from team
   * @param user user to be removed
   */
   removeMember(user: TUser): void {
    console.log('delete user', user);
    if (!this.selected_team) {
      //TODO: translate
      alert('No team selected. Cannot remove user from team');
      return;
    }

    const selected_team = this.selected_team;
    this.app.deleteResource('v1/teams/' + selected_team.teamid + '/members/' + user.id).subscribe((response: Response) => {
      if ( response.message === 'OK' ) {
        alert(this.app.translateString('team_member_removed'));
        const members = this.selected_team?.members.filter(p => p.id !== user.id);
        if (members)
          selected_team.members = members;
        else
        selected_team.members = [];
      } else {
        alert(this.app.translateString('team_remove_member_failed'));
      }
    },
    err => {
      alert(this.app.translateString('team_remove_member_failed'));
    });
  }

  /**
   * Remove user from group
   * @param user user to be removed
   * @param group group from which the user should be removed
   */
  removeUserGroup(user: TUser, group: GroupWithUsers): void {
    console.log('delete user group', user);
    this.app.deleteResource('v1/teams/' + this.selected_team?.teamid + '/group/' + group.groupid  + '/members/' +
        user.id).subscribe((response: Response) => {
      if ( response.message === 'OK' ) {
        alert(this.app.translateString('team_group_member_removed'));
        group.members = group.members.filter(p => p.id !== user.id);
      } else {
        alert(this.app.translateString('team_remove_member_failed'));
      }
    },
    err => {
      alert(this.app.translateString('team_remove_member_failed'));
    });
  }

  /**
   * Callback when user hits 'invite' button
   */
  public invite(): void {
    if ( this.inviteForm.invalid ) {
      this.submittedInvite = true;
      this.cd.detectChanges();
      this.app.translateContent();
      return;
    } else {
      this.loadingInvite = true;
      this.submittedInvite = false;

      this.app.putResource('v1/teams/invite/' + this.selected_team?.teamid, this.inviteForm.get('email')?.value).subscribe(
        (response: Response) => {
          this.loadingInvite = false;
          console.log(response);
          if ( response.message === 'Created' ) {
            this.inviteForm.reset();
            this.inviteFormError = undefined;
          } else if ( response.message === 'Fail' ) {
            this.submittedInvite = true;
            this.inviteForm.controls['email'].setErrors({'fail': true});
            this.inviteFormError = this.app.translateString('team_' + response.error);
          }
        },
        err => {
          this.loadingInvite = false;
          alert(err);
          console.log(err);
        }
      );
    }
  }

  public dragStart(person: TUser): void {
    console.log('dragStart', {person});
    this.draggedPerson = person;
  }

  public drop(group: GroupWithUsers): void {
    // better safe then sorry. We set draggedPerson == null --> keep a reference (because async)
    const person = this.draggedPerson;
    console.log('On Drop', {group, person});

    if ( this.draggedPerson ) {
      if ( group.members.filter(m => m.id === person?.id).length === 0 ) {
        this.app.putResource('v1/teams/' + this.selected_team?.teamid + '/group/' + group.groupid + '/members/',
          person?.id).subscribe((response: Response) => {
            if ( response.message === 'OK' ) {
              alert(this.app.translateString('team_group_member_added'));
              if (person) group.members.push(person);
            } else {
              alert(this.app.translateString('team_add_group_member_failed'));
            }
          },
          err => {
            alert(this.app.translateString('team_add_group_member_failed'));
          });
      } else {
        console.log('drop on member group -> ignore');
      }
      this.draggedPerson = null;
    }
  }

  public dragEnd(): void {
    this.draggedPerson = null;
  }
}
