import { Injectable } from '@angular/core';

import { AppService } from './app.service';
import { UserDetails } from '../shared/sample';


@Injectable({
    providedIn: 'root'
  })
export class UserService {

  private users: Map<number, UserDetails>;

  constructor(private app: AppService){
    this.users= new Map<number, UserDetails>();
  }

  public getUser(id: number): Promise<UserDetails> {
    if( this.users.has(id) ) {
      return Promise.resolve(this.users.get(id) as UserDetails);
    } else {
      console.log('Requesting user from server', id);
      return this.app.getPublicResource('v1/user/get/' + id).toPromise().then(res => {
        this.users.set(id, res);
        return this.users.get(id) as UserDetails;
      });
    }
  }
}
