import { LoginService } from './../login/login.service';
import { AppService } from './app.service';
import { Menu } from '../shared/menu';
import { Injectable, OnInit } from '@angular/core';
import { firstValueFrom, Observable } from 'rxjs';
import { of, lastValueFrom } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';

import { MENUS } from '../shared/menus';
import { LoggedInUser } from '../shared/loggedinuser';
import { Role } from '../shared/sample';

function includesAll(inc: Role[], what: string[]): boolean {
  console.log(inc, 'includes?', what, what.map(x => inc.map(r => r.name).includes(x)).every(check => check === true));
  return what.map(x => inc.map(r => r.name).includes(x)).every(check => check === true);
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private userInfo!: LoggedInUser | undefined;

  constructor(
    // private appService: AppService,
    private loginServer: LoginService) {
    // this.init();

    // this call is async and will await completion
    // lastValueFrom(this.appService.getLoggedInUser()).then((userInfo) => this.userInfo = userInfo);
    /* this.loginServer.getLoggedInUser().then((userInfo) => {
      console.log('MenuService::<init>', userInfo);
      this.userInfo = userInfo;}); */
  }

  /*private async init() {
    this.userInfo = await lastValueFrom(this.appService.getLoggedInUser());
  }*/

/*
  public getMenus(): Observable<Menu[]> {
    console.log('MenuService::getMenus', this.userInfo);
    /*if ( this.userInfo === undefined ) {
      this.userInfo = lastValueFrom(this.appService.getLoggedInUser()).then();
    }
    if ( this.userInfo !== undefined && this.userInfo !== null && this.loginServer.isLoggedIn() ) {
      return of(MENUS.filter(menu => menu.requiredroles.length === 0 || includesAll(this.userInfo.roles, menu.requiredroles)));
    } else {
      return of(MENUS.filter(menu => menu.isPublic === true && menu.requiredroles.length === 0));
    }*/
    // if ( this.userInfo !== undefined && this.userInfo !== null && this.loginServer.isLoggedIn() ) {
/*
    if (this.userInfo ) {
      const roles = this.userInfo.roles;
      return of(MENUS.filter(menu => menu.requiredroles.length === 0 || includesAll(roles, menu.requiredroles)));
    } /*if ( this.userInfo !== undefined && this.userInfo !== null && this.loginServer.isLoggedIn() ) {
      return of(MENUS.filter(menu => menu.requiredroles.length === 0 || includesAll(this.userInfo.roles, menu.requiredroles)));
    } */
/*
    else {
      return of(MENUS.filter(menu => menu.isPublic === true && menu.requiredroles.length === 0));
    }
  }
*/

  public async canAccessMenu(action: string): Promise<boolean> {
    this.userInfo = await this.loginServer.getLoggedInUser();

    console.debug('MenuService::canAccessMenu', action);
    const menus = MENUS.filter(men => men.action === action);
    if ( menus.length === 0 ) {
      return false;
    }
    const menu = menus[0];
    if ( this.userInfo !== undefined ) {
      return menu.requiredroles.length === 0 || includesAll(this.userInfo.roles, menu.requiredroles);
    } else {
      return menu.isPublic === true && menu.requiredroles.length === 0;
    }
  }

}
