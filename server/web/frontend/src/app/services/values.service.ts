import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AppService } from './app.service';
import { UserValueInfo, UserValue, ValueChanges, GenericResponse } from './../shared/sample';
import { HttpParams } from '@angular/common/http';

export interface ValueUpdate {
  id: number;
  date_start: Date | null;
  date_end: Date | null;
  value: any;
}

export interface ValueCreate {
  date_start: Date | null;
  date_end: Date | null;
  value: any;
}


@Injectable({
    providedIn: 'root'
  })
export class ValuesService {

  private values!: UserValueInfo[];

  private myValues!: Map<string, UserValue>;

  private otherValues: Map<number, Map<string, UserValue>>;

  constructor(private app: AppService) {
    // this.myValues= new Map<string, UserValue>();
    this.otherValues = new Map<number, Map<string, UserValue>>();
  }

  /**
   * Get the information about all available values.
   * This method can be used to get information about which values are available in theory.
   * It does not necessary mean that a particular user has a concrete value for this value instance.
   * Example user <Hugo> might not have values for <Vo2Max,MaxHr,...>
   */
  public getAvailableValues(): Promise<UserValueInfo[]> {
    if ( this.values !== undefined ) {
      return Promise.resolve(this.values);
    } else {
      return this.app.getPublicResource('v1/values/avail').toPromise().then(res => {
        this.values = res; return this.values;
      });
    }
  }

  /**
   * Get the current values for the logged in user
   */
  public getMyUserValues(): Promise<Map<string, UserValue>> {
    if ( this.myValues !== undefined ) {
      return Promise.resolve(this.myValues);
    } else {
      return this.app.getPublicResource('v1/user/values').toPromise().then((res: UserValue[]) => {
        this.myValues = new Map<string, UserValue>();
        res.forEach(value => {
          this.myValues.set(value.value.name, value);
        });
        return this.myValues;
      });
    }
  }

  /**
   * Get the current values for a user
   * @param userId id of the user
   */
  public getUserValues(userId: number): Promise<Map<string, UserValue>> {
    return this.app.getPublicResource('v1/values/get/' + userId).toPromise().then((res: UserValue[]) => {
      const values = new Map<string, UserValue>();
      res.forEach(value => {
        values.set(value.value.name, value);
      });
      return values;
    });
  }

  public getInfo(valueId: number): Promise<UserValueInfo> {
    return this.app.getPublicResource('v1/values/info/' + valueId).toPromise();
  }

  public getHistory(userId: number, valueId: number): Promise<UserValue[]> {
    return this.app.getPublicResource('v1/values/history/' + valueId + '/' + userId).toPromise();
  }

  public getMyHistory(valueId: number): Promise<UserValue[]> {
    return this.app.getPublicResource('v1/values/history/' + valueId).toPromise();
  }

  /**
   * Change a number of values. This method will update the value-instances to the provided values using the current date as start date
   * @param changes values changes
   * @param userId id of the user for which the action is performed
   */
  public sendChanges(changes: ValueChanges, userId: number): Observable<GenericResponse> {
    console.log('send changes', changes);
    return this.app.putResource('v1/values/set', changes, new HttpParams().set('user', '' + userId));
  }

  public sendChangesForMe(changes: ValueChanges): Observable<GenericResponse> {
    console.log('send changes', changes);
    return this.app.putResource('v1/user/values', changes);
  }

  public sendUpdates(valueId: number, userId: number, updates: ValueUpdate[]): Observable<GenericResponse> {
    return this.app.putResource('v1/values/update/' + valueId, updates, new HttpParams().set('user', '' + userId));
  }

  public sendUpdatesForMe(valueId: number, updates: ValueUpdate[]): Observable<GenericResponse> {
    // console.log('Update my value', valueId, updates);
    return this.app.putResource('v1/values/update/' + valueId, updates);
  }

  public sendCreate(valueId: number, userId: number, updates: ValueCreate[]): Observable<UserValue[]> {
    return this.app.getResourcePost('v1/values/' + valueId, updates, new HttpParams().set('user', '' + userId));
  }

  public sendCreateForMe(valueId: number, updates: ValueCreate[]): Observable<UserValue[]> {
    return this.app.getResourcePost('v1/values/' + valueId, updates);
  }

  public deleteValue(value: UserValue): Observable<GenericResponse> {
    return this.app.deleteResource('v1/values/' + value.value.id + '/' + value.id);
  }
}
