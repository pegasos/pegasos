import { Injectable } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeDe from '@angular/common/locales/de';
import localeDeat from '@angular/common/locales/de-AT';
import localeHu from '@angular/common/locales/hu';
import localeIs from '@angular/common/locales/is';
import localeMk from '@angular/common/locales/mk';
import localeBg from '@angular/common/locales/bg';

import { Language, LANGUAGES, DEFAULT_LANGUAGE } from './../shared/language';

// import * as lang from '../langdict.js';
const lang = require('../langdict.js');

@Injectable({
    providedIn: 'root'
})
export class TranslationService {

    // private translator;

    private currentLang= DEFAULT_LANGUAGE;

    private document: any;

    public constructor()
    {
      this.registerLocale();
    }

    public setDocument(document: any): void
    {
      this.document= document;
    }

    public translate(index: string): string
    {
      if( index in lang.lang_dict )
      {
        if( this.currentLang.lang in lang.lang_dict[index] )
        {
          return lang.lang_dict[index][this.currentLang.lang];
        }
        else if( DEFAULT_LANGUAGE.lang in lang.lang_dict[index] )
        {
          return lang.lang_dict[index][DEFAULT_LANGUAGE.lang];
        }
        else
        {
          return index;
        }
      }
      else
      {
        return index;
      }

    }

    public getLangDict()
    {
      return lang.lang_dict;
    }

    private translateElement(index: any, elem: any): void
    {
      let trn_key= elem.getAttribute('data-trn-key');
      if( !trn_key )
      {
        trn_key = elem.innerHTML;
        elem.setAttribute('data-trn-key', trn_key); // store key for next time
      }

      elem.innerHTML= this.translate(trn_key);
    }

  private translateElementAttr(index: any, elem: any): void {
    let trn_attr_names= elem.getAttribute('data-trn-attr');
    trn_attr_names= trn_attr_names.split(' ');

    // for each attribute
    trn_attr_names.forEach((trn_attr_name: string) => {
      let trn_key = elem.getAttribute('data-trn-key-' + trn_attr_name);
      if( !trn_key )
      {
        trn_key= elem.getAttribute(trn_attr_name);
        elem.setAttribute('data-trn-key-' + trn_attr_name, trn_key); // store key for next time
      }
      elem.setAttribute(trn_attr_name, this.translate(trn_key));
    });
  }

  private translateElementChildren(elem: HTMLElement): void {
    const tmp = elem.getAttribute('data-trn-children');
    if (!tmp ) {
      console.error('Could not get attribute data-trn-children from', elem);
      return;
    }
    const trn_children_names = tmp.split(' ');

    // for each child
    trn_children_names.forEach(trn_attr_name => {
      console.log('Trying to translate all children of type', trn_attr_name);
      let trn_key = elem.getAttribute('data-trn-key-' + trn_attr_name);
      if ( !trn_key ) {
        trn_key = elem.getAttribute(trn_attr_name) as string;
        elem.setAttribute('data-trn-key-' + trn_attr_name, trn_key); // store key for next time
      }

      Array.from(elem.getElementsByTagName(trn_attr_name)).forEach((telem: Element) => {
        telem.innerHTML = this.translate(trn_key as string);
      });
      elem.setAttribute(trn_attr_name, this.translate(trn_key));
    });
  }

  public translateSite(): void {
    Array.from(this.document.getElementsByClassName('trn')).forEach((elem, index) => this.translateElement(index, elem));

    Array.from(this.document.getElementsByClassName('trn-attr')).forEach((elem, index) => this.translateElementAttr(index, elem));

    Array.from(this.document.getElementsByClassName('trn-children')).forEach((elem: any) => this.translateElementChildren(elem));
  }

  public translateTree(id: string): void {
    Array.from(this.document.getElementById(id).getElementsByClassName('trn')).forEach(
      (elem, index) => this.translateElement(index, elem));

    Array.from(this.document.getElementById(id).getElementsByClassName('trn-attr')).forEach(
      (elem, index) => this.translateElementAttr(index, elem));

    Array.from(this.document.getElementsByClassName('trn-children')).forEach((elem:any) => this.translateElementChildren(elem));
  }

  public getLanguages(): Language[] {
    return LANGUAGES;
  }

  public getCurrentLanguage(): Language {
    return this.currentLang;
  }

    /**
     * Change the current language without translating something
     * @param Language 
     */
    public setLanguage(language: Language): void {
      this.currentLang= language;
      this.registerLocale();
    }

    /**
     * Change the current language. This operation will also translate the current site
     * @param language 
     */
    public changeLanguageL(language: Language): void
    {
      this.currentLang= language;
      this.registerLocale();
      this.translateSite();
    }

    private registerLocale(): void {
      /*if( this.currentLang.locale === 'de-AT' )
      {
        registerLocaleData(localeDe, 'de-AT');
      }*/
      if( this.currentLang.locale === 'de' )
      {
        // this.currentLang.locale= 'de-AT';
        registerLocaleData(localeDeat, 'de');
      }
      else if( this.currentLang.locale === 'en' )
      {
        registerLocaleData(localeEn);
      }
      else if( this.currentLang.locale === 'mk' )
      {
        registerLocaleData(localeMk);
      }
      else if( this.currentLang.locale === 'is' )
      {
        registerLocaleData(localeIs);
      }
      else if( this.currentLang.locale === 'hu' )
      {
        registerLocaleData(localeHu, 'HU');
      }
      else if( this.currentLang.locale === 'bg' )
      {
        registerLocaleData(localeBg);
      }
    }

    public getLocale(): string {
      return this.currentLang.locale;
    }
}
