import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';

import { AppService } from './app.service';
import { TeamWithGroupsWithUsers } from './../shared/sample';


@Injectable({
    providedIn: 'root'
  })
export class TeamService {

  constructor(private app: AppService) {
  }

  public getMyGroupMembers(): Observable<TeamWithGroupsWithUsers[]> {
    return from(this.app.cachedGetResource('v1/teams/mygroupmembers', '_mygroupmembers'));
  }

  public invalidateMyGroupMemebers(): void {
    sessionStorage.removeItem('_mygroupmembers');
  }
}
