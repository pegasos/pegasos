import { LoggedInUser } from './../shared/loggedinuser';
import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpEventType, HttpResponse, HttpProgressEvent, } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Title } from '@angular/platform-browser';

import { TranslationService } from './translation.service';

import { Config } from '../Config';
import { Language } from './../shared/language';
import { Notification } from './../shared/sample';
import { LoginService } from '../login/login.service';

const LOCALSTORAGE_ACCESS_TOKEN = 'oauth2authcodepkce-nekotssecca';

function isHttpResponse<T>(event: HttpEvent<T>): event is HttpResponse<T> {
  return event.type === HttpEventType.Response;
}

function isHttpProgressEvent(
  event: HttpEvent<unknown>
): event is HttpProgressEvent {
  return (
    event.type === HttpEventType.DownloadProgress ||
    event.type === HttpEventType.UploadProgress
  );
}

export interface Upload {
  progress: number;
  data: any;
  state: 'PENDING' | 'IN_PROGRESS' | 'DONE';
}

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public clientId = 'pegasos-web-client';
  public baseURL = Config.FRONTEND_URL;
  public redirectUri = Config.FRONTEND_URL + '/login';
  public backendBaseURL = Config.BACKENDBASEURL;
  public tokenPoint = this.backendBaseURL + '/oauth2/token';
  public loginUrl = this.backendBaseURL + '/oauth/authorize?response_type=code&client_id=' + this.clientId +
    '&redirect_uri=' + this.redirectUri;
  public loginUrlNative = this.backendBaseURL + '/oauth/token?grant_type=password';

  private lastTitle: string;

  constructor(
    private httpClient: HttpClient,
    private ngxService: NgxUiLoaderService,
    private translation: TranslationService,
    private titleService: Title,
    private inj: Injector,
    ) {
      if ( localStorage.getItem('LANGUAGE') != null ) {
        const language: Language = JSON.parse(localStorage.getItem('LANGUAGE') as string);
        this.translation.setLanguage(language);
      }
      this.lastTitle = titleService.getTitle();
  }

  getResource(resourceName: string, params?: HttpParams): Observable<any> {
    const headers = localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) != null ? new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)}) :
      new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});

    return this.httpClient.get(this.backendBaseURL + '/' + resourceName, { headers: headers, params: params });
  }

  /**
   * Get
   * @param resourceName 
   * @param params 
   * @returns 
   */
  public getResourceBlob(resourceName: string, params?: HttpParams): Observable<any> {
    const headers = localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) != null ? new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)}) :
      new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});

    return this.httpClient.get(this.backendBaseURL + '/' + resourceName, { headers: headers, params: params, responseType: 'blob', observe: 'response' });
  }

  public getResourcePost(resourceName: string, data: any, params?: HttpParams): Observable<any> {
    console.log(data);
    const headers= new HttpHeaders({
      'Content-type': 'application/json; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});

    return this.httpClient.post(this.backendBaseURL + '/' + resourceName, JSON.stringify(data), { headers: headers, params: params });
  }

  public cachedGetResource(resource: string, name: string): Promise<any> {
    if ( sessionStorage.getItem(name) != null ) {
      console.log(name, 'was cached', JSON.parse(sessionStorage.getItem(name) || '{}'))
      return Promise.resolve(JSON.parse(sessionStorage.getItem(name) || '{}'));
    } else {
      return new Promise((resolve, _) => {this.getResource(resource).subscribe(
        data => {
          console.log('Data received', name, data);
          sessionStorage.setItem(name, JSON.stringify(data));
          resolve(data)
        })});
    }
  }

  /**
   * Execute a put request on the server
   * @param resourceName name / url
   * @param data data to be sent (can be ommited)
   * @param params parameters for the request (can be ommited)
   */
  public putResource(resourceName: string, data?: any, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});

    if ( data !== undefined ) {
      return this.httpClient.put(this.backendBaseURL + '/' + resourceName, JSON.stringify(data), { headers: headers, params: params });
    } else {
      return this.httpClient.put(this.backendBaseURL + '/' + resourceName, {}, { headers: headers, params: params });
    }
  }

  /**
   * Post data to the server in form-urlencoded format
   * @param resourceName name of the endpoint
   * @param data formencoded data
   */
  public getResourcePostForm(resourceName: string, data: string): Observable<any> {
    console.log(data);
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});

    return this.httpClient.post(this.backendBaseURL + '/' + resourceName, data, { headers: headers });
  }

  public uploadFile(resourceName: string, data: any, upload: Upload): Observable<Upload> {
    const headers = new HttpHeaders({
      // 'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});

    return this.httpClient.post(this.backendBaseURL + '/' + resourceName, data, {
      headers: headers,
      reportProgress: true,
      observe: 'events',
    }).pipe(map(event => {
      // console.log(event);
      if (isHttpProgressEvent(event)) {
        const progress = event.total
          ? Math.round((100 * event.loaded) / event.total)
          : upload.progress;

        upload.progress = progress;
        upload.state = 'IN_PROGRESS';

        return {
          data: upload.data,
          progress: progress,
          state: 'IN_PROGRESS',
        };
      } else if (isHttpResponse(event)) {
        // console.log('done');

        upload.progress = 100;
        upload.state = 'DONE';

        return {
          data: upload.data,
          progress: 100,
          state: 'DONE',
        };
      } /* else {
        console.log('Don\'t know that event', event);
      } */
      return upload;
    }));
  }

  public getPublicResource(resourceName: string, credentials = true): Observable<any> {
    let headers;

    if ( credentials && localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) !== undefined ) {
      headers = new HttpHeaders({
        'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});
    } else {
      headers = new HttpHeaders({
        'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      });
    }

    return this.httpClient.get(this.backendBaseURL + '/' + resourceName, { headers: headers });
  }

  public deleteResource(resourceName: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string)});

    return this.httpClient.delete(this.backendBaseURL + '/' + resourceName, { headers: headers });
  }

  public setup(document: any): void {
    this.translation.setDocument(document);
  }

  public translateContent(): void {
    this.translateTree('maincontent');
  }

  /**
   * Translate part of the tree.
   *
   * @param id id in the html/doc tree of the element which should be translated.
   */
  public translateTree(id: string) {
    this.translation.translateTree(id);
  }

  public translateSite(): void {
    this.translation.translateSite();
  }

  public translateString(what: string): string {
    return this.translation.translate(what);
  }

  public getLanguages(): Language[] {
    return this.translation.getLanguages();
  }

  /**
   * Change the language of the website. Will also save this language for the future
   * @param language
   */
  public changeLanguage(language: Language): void {
    localStorage.setItem('LANGUAGE', JSON.stringify(language));
    this.translation.changeLanguageL(language);
  }

  public getCurrentLanguage(): Language {
    return this.translation.getCurrentLanguage();
  }

  public getLocale() {
    return this.translation.getLocale();
  }

  /**
   * Show loading symbol / spinner
   */
  public startLoading() {
    this.lastTitle = this.titleService.getTitle();
    this.titleService.setTitle( 'Loading ... | Pegasos');
    this.ngxService.start();
  }

  /**
   * Show loading symbol / spinner
   */
  public stopLoading() {
    this.titleService.setTitle(this.lastTitle);
    this.ngxService.stop();
  }

  public handleReceiveError(err: any): void {
    // TODO: implement a better error handling for now we will just stop loading. In the future we should investigate what is the cause
    console.log('handle error', err);
    this.ngxService.stop();
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle( newTitle + ' | Pegasos');
    this.lastTitle = this.titleService.getTitle();
  }

  public goHome(): void {
    window.location.href = this.baseURL;
  }

  public getNotifications(): Observable<Notification[]> {
    return this.getResource('v1/notifications/get');
  }

  /*
  getResource(resourceUrl): Observable<any> {
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + Cookie.get('access_token')});
    return this._http.get(resourceUrl, { headers: headers })
                   .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getResource2(resourceName): Observable<any> {
    let headers;
    if ( Cookie.check('access_token') ) {
      headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + Cookie.get('access_token')});
    }  else {
      headers = new HttpHeaders({
        'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
        });
    }
    return this._http.get('http://localhost:8081/spring-security-oauth-server' + resourceName, { headers: headers })
                   .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  checkCredentials() {
    return Cookie.check('access_token');
  }*/

  /*logout() {
    Cookie.delete('access_token');
    window.location.reload();
  }*/
}
