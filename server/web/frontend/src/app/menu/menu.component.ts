import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

import { MenuService } from '../services/menu.service';
import { AppService } from '../services/app.service';
import { LoginService } from './../login/login.service';
import { Menu } from './../shared/menu';
import { Language } from './../shared/language';
import { Notification, NotificationAction, Response, Role } from './../shared/sample';
import { LoggedInUser } from '../shared/loggedinuser';
import { MENUS } from '../shared/menus';

function includesAll(inc: Role[], what: string[]): boolean {
  return what.map(x => inc.map(r => r.name).includes(x)).every(check => check === true);
}

@Component({
  selector: 'app-navigation',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, AfterViewInit {
  public menus: Menu[] = [];
  public languages!: Language[];
  public currentLang!: Language;
  public items!: MenuItem[];
  public langmenu!: MenuItem[];
  private lang!: MenuItem;
  private userInfo!: LoggedInUser | undefined;

  /**
   * Menu item to be displayed for the notification
   */
  private notification: MenuItem = {
    id: 'notify',
    icon: 'pi pi-bell p-overlay-badge',
  };

  constructor(private _service: MenuService,
    private app: AppService,
    private loginService: LoginService) {}

  ngAfterViewInit() {
    // this._translation.translateTree('menus');
    this.app.translateTree('menus');
  }

  async ngOnInit() {
    this.languages = this.app.getLanguages();
    this.currentLang = this.app.getCurrentLanguage();
    this.setMenus(await this.getMenus());

    this.requestNotifications();
  }

  public async getMenus(): Promise<Menu[]> {
    this.userInfo = await this.loginService.getLoggedInUser();

    if (this.userInfo) {
      const roles = this.userInfo.roles;
      return (MENUS.filter(menu => menu.requiredroles.length === 0 || includesAll(roles, menu.requiredroles)));
    } else {
      return (MENUS.filter(menu => menu.isPublic === true && menu.requiredroles.length === 0));
    }
  }

  private requestNotifications(): void {
    if ( this.isLoggedIn() ) {
      this.app.getNotifications().subscribe((not: Notification[]) => this.setNotifications(not));
    }
  }

  private isLoggedIn(): boolean {
    return this.loginService.isLoggedIn();
  }

  public logout() {
    this.loginService.logout();
  }

  private setMenus(menus: Menu[]) {
    this.menus = menus;

    const mapper = (m: Menu) => {
      return {
        label: '<span class="trn">' + this.app.translateString(m.name) + '</span>',
        routerLink: m.action,
        escape: false,
      };
    };

    this.items = this.menus.map((m: Menu) => {
      return {
        label: '<span class="trn">' + this.app.translateString(m.name) + '</span>',
        routerLink: m.action,
        escape: false,
        items: (m.sub && m.sub.length > 0 ? m.sub.map(mapper): undefined),
      };
    });

   if ( !this.isLoggedIn() ) {
      this.items.push( {
        label: 'login',
        routerLink: '/login',
      });
    } else {
      this.items.push(this.notification);
      this.items.push( {
        label: 'logout',
        command: () => this.logout(),
      });
    }
    this.lang = {
      label: '<img src="' + this.currentLang.flag + '" /> ' + this.currentLang.language,
      // icon: this.currentLang.flag,
      escape: false,
      items: []
    };
    this.languagemenu();
    this.items.push(this.lang);
  }

  public setNotifications(notifcations: Notification[]): void {
    if ( notifcations.length > 0 ) {
      const mapper = (n: Notification): MenuItem => {
        const label = '<div id="notification-' + n.id + '">' + n.msg + '</div>';
        if ( n.type.actions.length > 0 ) {
          return {
            label: label,
            escape: false,
            items: n.type.actions.map((a: NotificationAction): MenuItem => {
              return {
                label: this.app.translateString(a.description),
                command: () => {
                  this.OnNotificationAction(n, a);
                }
              };
            })
          };
        } else {
          return {
            label: label,
            escape: false,
            command: () => {
              this.OnNotificationAccept(n);
            }
          };
        }
      };

      this.notification.items = notifcations.map((n: Notification) => mapper(n));

      this.addBadge(notifcations.length);
    } else {
      // this.notifications = [];
      this.notification.items = [];
      this.removeBadge();
    }
  }

  public OnNotificationAction(notification: Notification, action: NotificationAction): void {
    this.app.startLoading();
    this.app.getResource(action.action + '/' + notification.id).subscribe((response: Response) => {
      if ( response.message !== 'OK' ) {
        // TODO: better error handling
        alert(response.error);
      }
      this.app.stopLoading();
      this.requestNotifications();
    },
    (_) => {
      this.app.stopLoading();
      alert('Error');
    });
  }

  public OnNotificationAccept(n: Notification): void {
    this.app.getResource('v1/notifications/accept/' + n.id).subscribe((response: Response) => {
      if ( response.message !== 'OK' ) {
        alert(response.error);
      }
      this.requestNotifications();
    },
    (_) => {
      this.app.stopLoading();
      alert('Error');
    });
  }

  private addBadge(count: number): void {
    const notify = document.getElementById('notify')?.firstChild as ChildNode;
    const badge = document.createElement('span');
    badge.setAttribute('id', 'notify-count');
    // p-badge p-component p-badge-info p-badge-no-gutter
    badge.classList.add('p-badge');
    badge.classList.add('p-component');
    // badge.classList.add('p-badge-info');
    badge.classList.add('p-badge-no-gutter');
    // badge.classList.add('p-overlay-badge');
    badge.appendChild(document.createTextNode('' + count));
    notify.appendChild(badge);

    // find hamburger icon for the menu
    let bars: Element | undefined = undefined;
    const list = document.getElementsByClassName('pi pi-bars');
    for (let i = 0; i < list.length; i++) {
      if ( list[i].parentNode?.parentNode?.parentElement?.id === 'menus' ) {
        bars = list[i];
      }
    }

    // add badge to menu
    const badge2 = document.createElement('span');
    badge2.setAttribute('id', 'notify-count-bars');
    // p-badge p-component p-badge-info p-badge-no-gutter
    badge2.classList.add('p-badge');
    badge2.classList.add('p-component');
    // badge.classList.add('p-badge-info');
    badge2.classList.add('p-badge-no-gutter');
    bars?.classList.add('p-overlay-badge');
    badge2.appendChild(document.createTextNode('' + count));
    bars?.appendChild(badge2);
  }

  private removeBadge(): void {
    const count = document.getElementById('notify-count');
    if ( count !== null ) {
      document.getElementById('notify')?.firstChild?.removeChild(count);
    }

    const countBars = document.getElementById('notify-count-bars');
    if ( countBars !== null ) {
      // find hamburger icon for the menu
      let bars: Element | undefined = undefined;
      const list = document.getElementsByClassName('pi pi-bars');
      for (let i = 0; i < list.length; i++) {
        if ( list[i].parentNode?.parentNode?.parentElement?.id === 'menus' ) {
          bars = list[i];
        }
      }
      bars?.removeChild(countBars);
    }
  }

  public changeLanguage(language: Language) {
    this.app.changeLanguage(language);
    this.currentLang = this.app.getCurrentLanguage();
    this.lang.label = '<img src="' + this.currentLang.flag + '" /> ' + this.currentLang.language;

    this.app.putResource('v1/user/language', language.lang).subscribe((_) => {});

    this.languagemenu();
  }

  private languagemenu(): void {
    this.lang.items = this.languages.filter(l => l.lang !== this.currentLang.lang).map((l: Language): MenuItem => {
      return {
        // icon: l.flag,
        label: '<img src="' + l.flag + '" /> ' + l.language,
        escape: false,
        command: () => this.changeLanguage(l)
      };
    });
  }
}
