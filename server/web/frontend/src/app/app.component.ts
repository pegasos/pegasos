import { Component, Inject, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { DOCUMENT } from '@angular/common'; 

import { AppService } from './services/app.service';
import { TranslationService } from './services/translation.service';

@Component({
  selector: 'app-root',
  providers: [],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewChecked {
  title = 'app';

  public constructor(@Inject(DOCUMENT) private document: typeof DOCUMENT,
    private appService: AppService) {
  }

  ngOnInit() {
    // this.translationService.translateSite();
    this.appService.setup(this.document);
    this.appService.translateSite();
  }

  ngAfterViewChecked() {
    // this.translationService.translateTree('maincontent');
  }
}
