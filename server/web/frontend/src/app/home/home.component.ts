import { Component, OnInit } from '@angular/core';
import { isDevMode } from '@angular/core';
import { LoginService } from '../login/login.service';
import { AppService } from './../services/app.service';
import { UserService } from './../services/user.service';
import { UserDetails } from './../shared/sample';

@Component({
  selector: 'app-home',
  providers: [],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  user!: UserDetails;

  isDev: boolean;
  public pub!: string;
  public priv!: string;

  constructor(private app: AppService,
    private loginService: LoginService,
    private userService: UserService) {
    this.isDev = isDevMode();
  }

  ngOnInit() {
    // this.app.startLoading();

    this.loginService.getLoggedInUser().then(userDetails => {
      console.log('UserDetials', userDetails);
      if ( userDetails != undefined ) {
        this.userService.getUser(userDetails.user_id).then(u => {
          this.user = u;
        });
      }});
/*
    this.app.getPublicResource('v1/public/get').subscribe(
      data => {
        this.pub = data;
        this.app.stopLoading();
      },
      error =>  {
        this.pub = 'Error Public';
        this.app.stopLoading();
    });
*/
    this.app.translateContent();
  }
}
