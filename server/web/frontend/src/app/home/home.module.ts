import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ActionComponentsModule } from './../actioncomponents/action.components.module';

import { HomeComponent } from './home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ActionComponentsModule,
  ],
  declarations: [
    HomeComponent,
  ],
  exports: [RouterModule],
})
export class HomeModule {
}
