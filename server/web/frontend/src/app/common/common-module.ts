import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { CardModule } from 'primeng/card';

import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faInfo,
  faQuestion,
  faUserPlus,
  faTrash,
  faPlus,
  faUndo,
  faFileCsv,
  faCogs,
} from '@fortawesome/free-solid-svg-icons';

export const fontAwesomeIcons = [
  faUserPlus,
  faQuestion,
  faInfo,
  faTrash,
  faPlus,
  faUndo,
  faFileCsv,
  faCogs,
];

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    CardModule,
  ],
  declarations: [
  ],
  exports: [],
})
export class PegasosCommonModule {
  constructor(iconLibrary: FaIconLibrary) {
    console.log('Pegasos Common Module');
    iconLibrary.addIcons(...fontAwesomeIcons);
  }

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: PegasosCommonModule,
      providers: []
    };
  }
}
