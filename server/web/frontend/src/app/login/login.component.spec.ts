import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../services/app.service';

import { LoginComponent } from './login.component';
import { LoginService } from './login.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {
    const appServiceStub = {
      translateContent() {},

      /*translateString(what: string) {
        // return this.translation.translate(what);
        return what;
      },

      getCurrentLanguage(): Language {
        return DEFAULT_LANGUAGE;
      },

      getLoggedInUser(): LoggedInUser {
        /*const d = this._cookieService.get('loggedinuser');
        if ( d !== undefined ) {
          return JSON.parse(d);
        } else {
          return undefined;
        }*/
      //  return undefined;
      //}
    };

    const loginServiceStub = {
      isLoggedIn(): boolean {
        return false;
      }
    };

    let routeConfig = {
      path: '',
    }

    let paramMap = {
      get: (key: string) => {
      },
  
      has: (key: string) => {
        return false;
      }
    };
  
    let route = {
      snapshot: {
        routeConfig: routeConfig,
        paramMap: paramMap,
        queryParamMap: paramMap,
        queryParams: [],
      }
    };

    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],

      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: AppService, useValue: appServiceStub },
        { provide: LoginService, useValue: loginServiceStub },
        FormBuilder,
        ChangeDetectorRef,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
