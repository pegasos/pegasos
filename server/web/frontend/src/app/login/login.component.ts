import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Params, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AppService } from '../services/app.service';
import { LoginService } from './login.service';
import { Config } from '../Config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private isLoggedIn = false;

  public doLogin= false;
  public doRegister= false;

  protected loading= false;
  protected submitted= false;

  protected loginError = false;

  protected loginForm!: FormGroup;
  protected registerForm!: FormGroup;

  private returnUrl!: string;

  user = {username: '', password: '', remember: false};

  constructor(private route: ActivatedRoute,
    private _service: LoginService,
    private formBuilder: FormBuilder,
    private app: AppService,
    private cd: ChangeDetectorRef) {
      this._service.disableRefresh(true);
    }

  async ngOnInit() {
    this.isLoggedIn = this._service.isLoggedIn();
    const i = window.location.href.indexOf('code');

    // get return url from route parameters or default to '/'
    this.returnUrl = sessionStorage.getItem('returnURL') != null ?
      sessionStorage.getItem('returnURL') :
      this.route.snapshot.queryParams['returnUrl'] || '/';
    // console.log('LoginComponent::ngOnInit', sessionStorage.getItem('returnURL'), this.returnUrl, this.isLoggedIn, i);

    if ( this.route.snapshot.routeConfig?.path === 'logout' ) {
      this._service.logout();
    } else if ( this.route.snapshot.routeConfig?.path === 'register' ) {
      this.registerForm = this.formBuilder.group({
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        code: [''] // no validator for this one
      }, { validator: this.checkPasswords });
      this.doRegister = true;
    } else if ( this.route.snapshot.routeConfig?.path === 'finishregistration' ) {
      if ( this.route.snapshot.queryParamMap.has('hash') ) {
        // console.log('finishregistration');
        this._service.finishregistration(this.route.snapshot.queryParamMap.get('hash') as string, this);
      }
    } else {
      // console.log("login::onInit i:" + i);
      // are we not logged in but have a token?
      if ( !this.isLoggedIn && i !== -1) {
        // alert("Retrieve Token");
        const code = window.location.href.substring(i + 5);
        const andPos = code.indexOf('&');
        // console.log({code, andPos});
        if ( andPos === -1 ) {
          this._service.retrieveToken(code, this.returnUrl);
        } else {
          this._service.retrieveToken(code.substring(0, andPos), this.returnUrl);
        }
      } else if ( !this.isLoggedIn ) {
        const loginUrl: string = await this._service.generateLoginChallenge();
        if( this.returnUrl )
          sessionStorage.setItem('returnURL', this.returnUrl);

        window.location.href = loginUrl;
      } else if ( this.isLoggedIn && this.returnUrl !== undefined ) {
        // TODO: language
        sessionStorage.removeItem('returnURL');
        window.location.href = this.returnUrl;
      }
    }

    this.cd.detectChanges();
    this.app.translateContent();
  }

  public login(): void {
    this.submitted = true;
    this.loginError = false;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      this.cd.detectChanges();
      this.app.translateContent();
      return;
    }

    this.loading = true;
    this._service.loginNative(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value)
      .then(
        () => {
          if ( this.returnUrl !== undefined ) {
            window.location.href = this.returnUrl;
          }
        },
        (_) => {
          this.loading = false;
          this.loginForm.controls['username'].setErrors({'incorrect': true});
          this.loginForm.controls['password'].setErrors({'incorrect': true});
          this.loginError = true;
          this.cd.detectChanges();
          this.app.translateContent();
        });
  }

  private checkPasswords(group: FormGroup) {
    const pass = group.controls['password'].value;
    const confirmPass = group.controls['confirmPassword'].value;

    return pass === confirmPass ? null : { notSame: true };
  }

  public register(): void {
    this.submitted = true;

    if( this.registerForm.invalid )
    {
      this.cd.detectChanges();
      this.app.translateContent();
      return;
    }

    this.loading = true;

    // before submitting ensure we are using the current language setting
    this.registerForm.value.language = this.app.getCurrentLanguage().lang;

    this._service.register(this.registerForm.value, this);
  }

  public resgisterFinished(err?: any): void {
    if ( err !== null ) {
      alert('Registration submitted');
      this.submitted = false;
      this.registerForm.reset();
    } else {
      alert('Error: ' + err);
    }
    this.loading = false;
  }
}