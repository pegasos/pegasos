import { LoginComponent } from './login.component';
import { AppService } from './../services/app.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, firstValueFrom, lastValueFrom, throwError } from 'rxjs';
import { Config } from '../Config';
import { LoggedInUser } from '../shared/loggedinuser';

/**
 * The maximum length for a code verifier for the best security we can offer.
 * Please note the NOTE section of RFC 7636 § 4.1 - the length must be >= 43,
 * but <= 128, **after** base64 url encoding. This means 32 code verifier bytes
 * encoded will be 43 bytes, or 96 bytes encoded will be 128 bytes. So 96 bytes
 * is the highest valid value that can be used.
 */
export const RECOMMENDED_CODE_VERIFIER_LENGTH = 96;

/**
 * Character set to generate code verifier defined in rfc7636.
 */
const PKCE_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';

/**
 * Implements *base64url-encode* (RFC 4648 § 5) without padding, which is NOT
 * the same as regular base64 encoding.
 */
function base64urlEncode(value: string): string {
    let base64 = btoa(value);
    base64 = base64.replace(/\+/g, '-');
    base64 = base64.replace(/\//g, '_');
    base64 = base64.replace(/=/g, '');
    return base64;
}

export async function generatePKCECodeChallengeAndVerifier() {
    const output = new Uint32Array(RECOMMENDED_CODE_VERIFIER_LENGTH);
    crypto.getRandomValues(output);
    const codeVerifier = base64urlEncode(Array
        .from(output)
        .map((num: number) => PKCE_CHARSET[num % PKCE_CHARSET.length])
        .join(''));
    const buffer = await crypto
        .subtle
        .digest('SHA-256', (new TextEncoder()).encode(codeVerifier));
    const hash = new Uint8Array(buffer);
    let binary = '';
    const hashLength = hash.byteLength;
    for (let i: number = 0; i < hashLength; i++) {
        binary += String.fromCharCode(hash[i]);
    }
    const codeChallenge = base64urlEncode(binary);
    return { codeChallenge, codeVerifier };
}

/**
 * To store the OAuth client's data between websites due to redirection.
 */
const LOCALSTORAGE_STATE = 'oauth2authcodepkce-state';
/**
 * To store the OAuth client's data between websites due to redirection.
 */
const LOCALSTORAGE_ACCESS_TOKEN = 'oauth2authcodepkce-nekotssecca';

/**
 * To store the OAuth client's data between websites due to redirection.
 */
const LOCALSTORAGE_ACCESS_TOKEN_EXPIRY = 'oauth2authcodepkce-nekotssecca-expiry';
/**
 * To store the OAuth client's data between websites due to redirection.
 */
const LOCALSTORAGE_REFRESH_TOKEN = 'oauth2authcodepkce-nekothhserfer';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
  private codeVerifier: string;

  /**
   * Flag indicating whether a refresh operation (for refreshtoken) is currently running.
   * This is also hijaked to indicate whether we are on the login page (i.e. disableRefresh is used)
   */
  private refreshRunning: boolean = false;

  constructor(
    private httpClient: HttpClient,
    private app: AppService) {
    this.codeVerifier = JSON.parse(localStorage.getItem(LOCALSTORAGE_STATE) || '{}');
  }

  retrieveToken(code: string, redirectUrl?: string) {
    this.refreshRunning = true;
    const params = new URLSearchParams();
    params.append('grant_type', 'authorization_code');
    params.append('client_id', this.app.clientId);
    params.append('redirect_uri', this.app.redirectUri);
    params.append('code', code);
    params.append('code_verifier', this.codeVerifier);

    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.app.clientId + ':secret')
    });

    this.httpClient.post(this.app.tokenPoint, params.toString(), { headers: headers })
      .subscribe({
        next: (data:any) =>{
          localStorage.removeItem(LOCALSTORAGE_STATE);
          this.saveToken(data);
          this.refreshRunning = false;
          if ( redirectUrl ) {
            window.location.href = redirectUrl;
          }
        },
        error: (_) => alert('Could not obtain token')});
  }

  /**
   * Check if we can refresh the access token
   *
   * @returns true if a refresh token is available
   */
  public canRefresh(): boolean {
    // console.log('LoginService::canRefresh?', localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN) != undefined);
    return localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN) != undefined && !this.refreshRunning;
  }

  public isRefreshing(): boolean {
    // console.log('LoginService::isRefreshing?', this.refreshRunning)
    return this.refreshRunning;
  }

  /**
   * Try to refresh the access token
   */
  public async refreshToken(): Promise<void> {
    this.refreshRunning = true;

    // console.log('Refresh Token', localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN));
    if (!localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN)) {
      console.error('Tried to refresh token. But had no refresh token');
      throw new Error();
    }

    // For refreshing the token we do not need to pass any information about the client
    const params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', JSON.parse(localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN) as string));

    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.app.clientId + ':secret')});

    // console.log('Requesting refresh token', params);

    firstValueFrom(this.httpClient.post(this.app.backendBaseURL + '/oauth2/token', params.toString(), { headers: headers })).then(
      (data) => this.saveToken(data),
      (e) => {
        console.log('Caught error during token refresh', e, 'removing tokens');
        this.removeTokens();
        return throwError(() => e);
      });
    this.refreshRunning = false;
    return lastValueFrom(EMPTY);
  }

  private saveToken(token: any): void {
    // console.log('Token', token);
    // alert("Obtained Access Token " + token);
    const expireDate = new Date().getTime() + (1000 * token.expires_in);
    localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN, JSON.stringify(token.access_token));
    localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY, JSON.stringify(new Date(expireDate)));
    localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, JSON.stringify(token.refresh_token));
    // localStorage.setItem('loggedinuser', JSON.stringify(token.userinfo));
    console.log('Obtained Access token ' + token);
  }

  public saveVerifier(codeVerifier: string): void {
    // localStorage.setItem(LOCALSTORAGE_VERIFIER, JSON.stringify(state));
    this.codeVerifier = codeVerifier;
    localStorage.setItem(LOCALSTORAGE_STATE, JSON.stringify(codeVerifier));
  }

  public loginNative(user: string, password: string): Promise<void> {
    const formData = new HttpParams()
      .set('password', password)
      .set('username', user)
      .set('submit', 'Login');

    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.app.clientId + ':secret')
    });

    // return this.httpClient.post(this.app.loginUrlNative, formData.toString(), { headers: headers });
    firstValueFrom(this.httpClient.post(this.app.loginUrlNative, formData.toString(), { headers: headers })).then(
      (token) => this.saveToken(token),
      (e) => {
        console.log('Caught error during token refresh', e, 'removing tokens');
        this.removeTokens();
        return throwError(() => e);
      });
    return lastValueFrom(EMPTY);
  }

  public removeTokens(): void {
    localStorage.removeItem(LOCALSTORAGE_ACCESS_TOKEN);
    localStorage.removeItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY);
    localStorage.removeItem(LOCALSTORAGE_REFRESH_TOKEN);
    sessionStorage.removeItem('loggedinuser');
  }

  public logout(): void {
    // this.app.getResourcePost('logout', {})
    this.app.getResourcePost('logout', {}).subscribe(() => {
      this.app.getResource('oauth2/logout')
        .subscribe(
          data => {
            // alert(data);
            this.removeTokens();
            window.location.href = this.app.baseURL;
          }, err => {
            this.removeTokens();
            alert('Error logging out (oauth');
            window.location.href = this.app.baseURL;
          });
      }, err => alert('Error logging out'));
  }

  public isLoggedIn(): boolean {
    console.log('LoginService::isLoggedIn', localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN));
    // return this.checkCredentials();
    return localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) != null && this.checkCredentials();
  }

  private checkCredentials(): boolean {
    const now = new Date().getTime();
    const myaccess_token = JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN) as string);
    const access_token_ex = localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY) != null ? 
      new Date(JSON.parse(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY) as string)).getTime() : now;
    console.log('LoginService::checkCredentials ' + myaccess_token + ' ' + now + ' ' + access_token_ex + ' ' + (now < access_token_ex));

    return now < access_token_ex;
  }

  public register(user: any, comp: LoginComponent): void {
    const headers = new HttpHeaders({
      'Content-type': 'application/json; charset=utf-8'
    });

    firstValueFrom(this.httpClient.post(this.app.backendBaseURL + '/v1/users/register', JSON.stringify(user), { headers: headers })).then(
      _ => comp.resgisterFinished(),
      err => {console.log('Caught an error'); comp.resgisterFinished(err)}
    );
  }

    public finishregistration(hash: string, comp: LoginComponent): void {
        const headers = new HttpHeaders({
            'Content-type': 'application/json; charset=utf-8'
        });
        this.app.getPublicResource('v1/users/finishregistration/' + hash)
            .subscribe(
                data => alert(data)
            );
    }

  public disableRefresh(disable: boolean): void {
    // currently only refreshRunning is blocking is from being able to refresh
    if (!this.refreshRunning )
      this.refreshRunning = disable;
  }

  /**
   * Generade a code Challenge and verifier for the login / obtaining token process.
   * The code verifier will be stored in order to used it once the token has been obtained.
   * This also means that only one login process should take place!
   *
   * @returns codeChallenge to be used during login / token process
   */
  public async generateLoginChallenge(): Promise<string> {
    const { codeChallenge, codeVerifier } = await generatePKCECodeChallengeAndVerifier();
    this.saveVerifier(codeVerifier);

    return Config.BACKENDBASEURL +
      '/oauth2/authorize?response_type=code&client_id=' + this.app.clientId + '&scope=openid&code_challenge=' + codeChallenge +
      '&code_challenge_method=S256&state=foo&redirect_uri=' + Config.FRONTEND_URL + '/login';
  }

  /**
   * Get the current user
   *
   * @returns LoggedInUser or undefined if user is not logged in
   */
   public async getLoggedInUser(): Promise<LoggedInUser | undefined> {
    console.log('LoginService::getLoggedinUser', sessionStorage.getItem('loggedinuser'));
    // If everyting works correctly there is only a user in session storage when the user is logged in
    if ( sessionStorage.getItem('loggedinuser') != null ) {
      console.log('Return stored user', sessionStorage.getItem('loggedinuser'));
      return JSON.parse(sessionStorage.getItem('loggedinuser') as string);
    }
    console.log('LoginService::getLoggedinUser', this.isRefreshing(), this.isLoggedIn());
    if ( !this.isRefreshing() && !this.isLoggedIn() ) {
      // we are not logged in and there is currently no login running
      console.log('No refresh running. We are not loggedin');
      return undefined;
    } else {
      // wait if there is any existing operation retrying
      // ----------------------------------------------------------
      await new Promise(resolve => {
        // declare some global variable to check in while loop
        while(this.isRefreshing()) {
            setTimeout(()=> {
                // Just adding some delay
                // (you can remove this setTimeout block if you want)
            },50);
        }

        // when while-loop breaks, resolve the promise to continue
        resolve(true);
      });
      // Refresh has completed. Are we logged in?
      console.log('Awaited refresh.', this.isLoggedIn());
      if (this.isLoggedIn() ) {
        const loggedinUser = lastValueFrom(this.app.getResource('v1/user/getloggedinuser')).then((loggedinUser: LoggedInUser) => {
          sessionStorage.setItem('loggedinuser', JSON.stringify(loggedinUser));
          return loggedinUser;
        });
        return loggedinUser;
      } else {
        return undefined;
      }
    }
  }
}
