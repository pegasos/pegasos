import { AppService } from './services/app.service';
import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { LoginService } from './login/login.service';

@Injectable()
export class LoginInterceptor implements HttpInterceptor {
  constructor(private inj: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      // console.log('LoginInterceptor. 401?', err.status === 401);
      if ( err.status === 401 ) {
        const ls: LoginService = this.inj.get(LoginService);
        // console.log({ err }, err, ls.isRefreshing());
        if ( !ls.isRefreshing() ) {
          if ( ls.canRefresh() ) {
            // console.log('Interceptor:Try to refresh token');
            try {
              ls.refreshToken().then(
                () => window.location.reload(),
                (error) => {
                  // console.log('Interceptor:Error during refresh', error)
                  // At this point the login service has already taken care of everything necessary to prevent future problems
                  this.redirectLogin();
                }
              );
            } catch( e ) {
              // console.error('Interceptor:Error during token refresh', e, 'loggedin?', ls.isLoggedIn());
              if ( ls.isLoggedIn() ) {
                ls.logout();
                console.log(location);
              }
              this.redirectLogin();
            }
          } else {
            // console.log('Interceptor:Cannot refresh token -> if logged in -> logout -> redirect');
            if ( ls.isLoggedIn() ) {
              ls.logout();
            }
            this.redirectLogin();
          }
        } /* else {
          console.log('Caught a 401. But we are already taking care of this');
        } */
      } /* else {
        console.log('Interceptor:Other error', err, err.status);
      } */

      return throwError(() => err);
    }
    ));
  }

  private redirectLogin(): void {
    const currentLocation = location;
    // console.log('Interceptor:Redirect Login', currentLocation);
    location.assign(this.inj.get(AppService).baseURL + '/login?returnUrl=' + currentLocation);
  }
}
