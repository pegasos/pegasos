# -*- coding: utf-8 -*-
import json
import sys
from configparser import (ConfigParser, MissingSectionHeaderError,
                          ParsingError, DEFAULTSECT)
import codecs
import os
import json
import itertools

class FakeGlobalSectionHead(object):
    def __init__(self, fp):
        self.fp = fp
        self.sechead = '[global]\n'
    def readline(self):
        if self.sechead:
            try: return self.sechead
            finally: self.sechead = None
        else: return self.fp.readline()

# cp = ConfigParser()
# cp.readfp(FakeGlobalSectionHead(codecs.open(sys.argv[1], "r")))
# print(cp.items('global'))
# print(cp.sections())

def load_files(lang, files):
	for filex in files:
		cp= ConfigParser()
		cp.optionxform= str
		with codecs.open(filex, "r") as fp:
			cp.read_file(itertools.chain(['[global]'], fp))
		for section in cp.sections():
			if section in lang_dict[lang]:
				lang_dict[lang][section]+= cp.items(section)
			else:
				lang_dict[lang][section]= cp.items(section)

def to_langdict(language):
	lang= lang_dict[language]
	out= {}
	for group in lang.keys():
		if group == "global":
			pre= ""
		else:
			pre= group + "_"
		for (key, val) in lang[group]:
			out[pre+ key]= val[1:-1]
	
	return out

def to_outdict(language):
	lang= lang_dict[language]
	for group in lang.keys():
		if group == "global":
			pre= ""
		else:
			pre= group + "_"
		for (key, val) in lang[group]:
			if pre+ key not in out:
				out[pre+ key]= {}
			out[pre+ key][language]= val[1:-1]

files= {}

lang_dict= {}
out= {}

for dirname, dirnames, filenames in os.walk('.'):
    # print path to all filenames.
    for filename in filenames:
    	parts= filename.split(".")
    	if len(parts) >= 2 and parts[-1] == "ini":
    		x= filename[0:-4].split("_")
    		lang= x[-1]
    		f= os.path.join(dirname, filename)
    		if lang in files:
    			files[lang].append(f)
    		else:
    			files[lang]= [f]
    		# print(f, x)

print( files )

for lang in files.keys():
	print( lang )
	lang_dict[lang]= {}
	load_files(lang, files[lang])
	# out[lang]= to_langdict(lang)
	to_outdict(lang)

fp= codecs.open("langdict.js", "w") 
fp.write("export var lang_dict=" + json.dumps(out, sort_keys=True, indent=2))
fp.close()

