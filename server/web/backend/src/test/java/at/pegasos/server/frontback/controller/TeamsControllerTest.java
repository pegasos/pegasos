package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.auth.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.jdbc.*;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.annotation.*;
import org.springframework.test.annotation.DirtiesContext.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit.jupiter.*;

import javax.transaction.*;
import java.security.*;

import static at.pegasos.server.frontback.repositories.TeamWithGroupWithUsersRepositoryTest.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@ActiveProfiles(value = "test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = Replace.ANY)
class TeamsControllerTest {
  @Autowired TeamsController teamsController;

  @Autowired TeamRepository teamRepository;
  @Autowired UserRepository userRepository;
  @Autowired TUserRepository tuserRepository;
  @Autowired TeamWithGroupWithUsersRepository teamWithGroupWithUsersRepository;
  @Autowired AuthorisationUserDetailsRepository authorisationUserDetailsRepository;
  @Autowired GroupRepository groupRepository;
  @Autowired TeamMembershipRepository teamMembershipRepository;

  private User b;
  private User a;
  private User owner;
  private Group group;
  private Team team;

  static class PrincipalClass implements Principal {

    private final String name;

    public PrincipalClass(User u)
    {
      this.name = u.email;
    }

    @Override public String getName()
    {
      return name;
    }
  }

  @Test
  public void can_removePersonFromGroup() throws NoSuchAlgorithmException
  {
    // Set up
    ownerA_notB();

    GenericResponse response = teamsController.group_removeFromGroup(new PrincipalClass(owner), team.teamid, group.groupid, a.user_id);
    assertEquals(GenericResponse.OkResponse, response);
    // A is no longer a member of the group -> size = 0
    assertEquals(0,
        (int) teamMembershipRepository.findUserActive(a.user_id).stream().filter(tm -> tm.groupid != null && tm.groupid.equals(group.groupid)).count());
    // A is still in the team
    assertEquals(1,
        (int) teamMembershipRepository.findUserActive(a.user_id).stream().filter(tm -> tm.teamid == team.teamid).count());
    // and this is the only active membership
    assertEquals(1, teamMembershipRepository.findUserActive(a.user_id).size());
    // The group is now empty
    assertEquals(0, teamWithGroupWithUsersRepository.findByGroup(group.groupid).get(0).groups.get(0).getMembers().size());
  }

  @Test
  public void can_addPersonToGroup() throws NoSuchAlgorithmException
  {
    // Set up
    ownerA_notB();

    assertEquals(GenericResponse.OkResponse, teamsController.group_addMember(new PrincipalClass(owner), team.teamid, group.groupid, b.user_id));
    // B is now in the group
    assertEquals(1,
        (int) teamMembershipRepository.findUserActive(b.user_id).stream().filter(tm -> tm.groupid.equals(group.groupid)).count());
    // A is still in the group
    assertEquals(1,
        (int) teamMembershipRepository.findUserActive(a.user_id).stream().filter(tm -> tm.groupid.equals(group.groupid)).count());
    // The group contains now A & B
    assertEquals(2, teamWithGroupWithUsersRepository.findByGroup(group.groupid).get(0).groups.get(0).getMembers().size());
    // getTeamGroupMembers returns correct value = 2
    assertEquals(2, teamsController.getTeamGroupMembers(new PrincipalClass(owner), team.teamid).get(0).groups.get(0).getMembers().size());
  }

  @Test
  public void can_removePersonFromTeam() throws NoSuchAlgorithmException
  {
    // Set up
    ownerA_notB();

    GenericResponse response = teamsController.removeMember(new PrincipalClass(owner), team.teamid, a.user_id);
    assertEquals(GenericResponse.OkResponse, response);
    // A is no longer a member of the team -> size = 0
    assertEquals(0, teamMembershipRepository.findUserActive(a.user_id).size());
    // B still has now membership
    assertEquals(0, teamMembershipRepository.findUserActive(b.user_id).size());
    // The group is now empty
    assertEquals(0, teamWithGroupWithUsersRepository.findByGroup(group.groupid).get(0).groups.get(0).getMembers().size());
  }

  @Test
  public void can_removePersonFromGroupThenTeam() throws NoSuchAlgorithmException
  {
    // Set up
    ownerA_notB();

    // remove person from group. --> we should have an entry with group = null for that person
    GenericResponse response = teamsController.group_removeFromGroup(new PrincipalClass(owner), team.teamid, group.groupid, a.user_id);
    assertEquals(GenericResponse.OkResponse, response);

    response = teamsController.removeMember(new PrincipalClass(owner), team.teamid, a.user_id);
    assertEquals(GenericResponse.OkResponse, response);
    // A is no longer a member of the team -> size = 0
    assertEquals(0,
        (int) teamMembershipRepository.findUserActive(a.user_id).size());
    // B still has now membership
    assertEquals(0, teamMembershipRepository.findUserActive(b.user_id).size());
    // The group is now empty
    assertEquals(0, teamWithGroupWithUsersRepository.findByGroup(group.groupid).get(0).groups.get(0).getMembers().size());
  }

  private void ownerA_notB() throws NoSuchAlgorithmException
  {
    owner = createTestUser(userRepository, authorisationUserDetailsRepository);
    owner = userRepository.saveAndFlush(owner);
    team = new Team();
    team.name = "test";
    team.owner = owner;
    team = teamRepository.saveAndFlush(team);

    group = new Group();
    group.name = "Test";
    group.setTeam(team);
    group = groupRepository.saveAndFlush(group);

    teamWithGroupWithUsersRepository.findByTeamid(team.teamid).get(0).captains.add(tuserRepository.findById(owner.user_id).get());

    a = createTestUser(userRepository, authorisationUserDetailsRepository);
    b = createTestUser(userRepository, authorisationUserDetailsRepository);

    TeamMembership teamMembershipA = new TeamMembership();
    teamMembershipA.userid = a.user_id;
    teamMembershipA.teamid = team.teamid;
    teamMembershipA.groupid = group.groupid;
    teamMembershipA.deleted = false;
    teamMembershipRepository.saveAndFlush(teamMembershipA);

    TeamMembership teamMembershipB = new TeamMembership();
    teamMembershipB.userid = b.user_id;
    teamMembershipB.teamid = team.teamid;
    teamMembershipB.groupid = group.groupid;
    teamMembershipB.deleted = true;
    teamMembershipRepository.saveAndFlush(teamMembershipB);
  }
}
