package at.pegasos.server.frontback;

import at.pegasos.server.frontback.config.*;
import at.pegasos.server.frontback.controller.*;
import org.junit.jupiter.api.Test;

import org.rosuda.REngine.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class ApplicationTest {

  @Autowired ChartController chartsController;
  @Autowired LiveController liveController;
  @Autowired NotificationsController notificationsController;
  @Autowired RolesController rolesController;
  @Autowired TeamsController teamsController;
  @Autowired TrainingController trainingController;
  @Autowired UserController userController;
  @Autowired Values valuesController;

  // @MockBean REngine rEngine;

  @Test public void contextLoads()
  {
  }

  @Test public void no_smokeexists()
  {
    assertNotEquals(null, chartsController);
    assertNotEquals(null, liveController);
    assertNotEquals(null, notificationsController);
    assertNotEquals(null, rolesController);
    assertNotEquals(null, teamsController);
    assertNotEquals(null, trainingController);
    assertNotEquals(null, userController);
    assertNotEquals(null, valuesController);
  }
}