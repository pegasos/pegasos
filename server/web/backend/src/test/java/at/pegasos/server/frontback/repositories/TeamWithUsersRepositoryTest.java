package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.auth.Security;
import at.pegasos.server.frontback.auth.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import org.hibernate.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.junit.jupiter.*;

import javax.transaction.*;
import java.nio.charset.*;
import java.security.*;
import java.util.*;

import static at.pegasos.server.frontback.repositories.TeamWithGroupWithUsersRepositoryTest.createTestUser;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class TeamWithUsersRepositoryTest {
  @Autowired TeamWithUsersRepository repository;
  @Autowired TeamRepository teamRepository;
  @Autowired UserRepository userRepository;
  @Autowired TUserRepository tuserRepository;
  @Autowired AuthorisationUserDetailsRepository authorisationUserDetailsRepository;
  @Autowired GroupRepository groupRepository;
  @Autowired TeamMembershipRepository teamMembershipRepository;

  @Autowired SessionFactory sessionFactory;

  @Test
  public void getForManager_does_not_contain_deleted() throws NoSuchAlgorithmException
  {
    sessionFactory.openSession().beginTransaction();

    User owner = createTestUser(userRepository, authorisationUserDetailsRepository);
    owner = userRepository.saveAndFlush(owner);
    Team t = new Team();
    t.name = "test";
    t.owner = owner;
    t = teamRepository.saveAndFlush(t);

    Group g = new Group();
    g.name = "Test";
    g.setTeam(t);
    g = groupRepository.saveAndFlush(g);

    repository.findByTeamid(t.teamid).get(0).captains.add(tuserRepository.findById(owner.user_id).get());

    User a = createTestUser(userRepository, authorisationUserDetailsRepository);
    User b = createTestUser(userRepository, authorisationUserDetailsRepository);

    TeamMembership teamMembershipA = new TeamMembership();
    teamMembershipA.userid = a.user_id;
    teamMembershipA.teamid = t.teamid;
    teamMembershipA.groupid = g.groupid;
    teamMembershipA.deleted = true;
    teamMembershipRepository.saveAndFlush(teamMembershipA);

    TeamMembership teamMembershipB = new TeamMembership();
    teamMembershipB.userid = b.user_id;
    teamMembershipB.teamid = t.teamid;
    teamMembershipB.groupid = g.groupid;
    teamMembershipRepository.saveAndFlush(teamMembershipB);

    List<TeamWithUsers> result = repository.getForManager(owner.email);

    assertEquals(1, result.size());
    assertEquals(1, result.get(0).getMembers().size());
    assertFalse(result.get(0).getMembers().contains(tuserRepository.findById(a.user_id).get()));
    assertTrue(result.get(0).getMembers().contains(tuserRepository.findById(b.user_id).get()));
  }
}