package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.auth.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.repositories.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.jdbc.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit.jupiter.*;

import javax.transaction.*;

import java.security.*;
import java.util.*;

import static at.pegasos.server.frontback.repositories.TeamWithGroupWithUsersRepositoryTest.createTestUser;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@ActiveProfiles(value = "test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class ReportControllerTest {

  @Autowired ReportController reportController;

  @Autowired TeamRepository teamRepository;
  @Autowired UserRepository userRepository;
  // @Autowired TUserRepository tuserRepository;
  // @Autowired TeamWithGroupWithUsersRepository teamWithGroupWithUsersRepository;
  @Autowired AuthorisationUserDetailsRepository authorisationUserDetailsRepository;
  // @Autowired GroupRepository groupRepository;
  @Autowired TeamMembershipRepository teamMembershipRepository;

  // private User b;
  // private User a;
  private User owner;
  private Group group;
  private Team team;

  @Test void getReport()
  {
  }

  @Test void customReport_TypesWork() throws NoSuchAlgorithmException
  {
    setupTestUser();

    assertNotEquals(
        reportController.getCustomReport(new TeamsControllerTest.PrincipalClass(owner), "UserValue", null, null, List.of("DOES_NOT_MATTER"),
            "2020-1-1", "2020-12-31", null, null), null);
  }

  private void setupTestUser() throws NoSuchAlgorithmException
  {
    owner = createTestUser(userRepository, authorisationUserDetailsRepository);
    owner = userRepository.saveAndFlush(owner);
    team = new Team();
    team.name = "test";
    team.owner = owner;
    team = teamRepository.saveAndFlush(team);
  }
}