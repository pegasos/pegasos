package at.pegasos.server.frontback.data.chart;

import at.pegasos.server.frontback.data.*;

import java.util.*;

public class ChartWithRowData extends Chart {
  private final List<Map<String, Object>> points;

  public ChartWithRowData(Type type, List<Map<String, Object>> points)
  {
    super(type);
    this.points = points;
  }

  public List<Map<String, Object>> getPoints()
  {
    return points;
  }
}
