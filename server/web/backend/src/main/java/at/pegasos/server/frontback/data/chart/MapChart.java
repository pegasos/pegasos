package at.pegasos.server.frontback.data.chart;

import java.util.List;

import at.pegasos.server.frontback.data.*;

public class MapChart extends Chart {
  public static class GeoPoint {
    public long time;
    public double lat;
    public double lon;
    
    public GeoPoint(long time, double x, double y)
    {
      this.time= time;
      this.lat= x;
      this.lon= y;
    }
  }

  private final List<GeoPoint> points;
  
  public MapChart(List<GeoPoint> points)
  {
    super(Type.Map);
    this.points= points;
  }

  public List<GeoPoint> getPoints()
  {
    return points;
  }
}
