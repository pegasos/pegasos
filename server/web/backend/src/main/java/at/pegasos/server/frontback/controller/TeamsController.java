package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.exceptions.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.services.*;
import io.swagger.v3.oas.annotations.*;
import lombok.extern.slf4j.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.access.prepost.*;
import org.springframework.transaction.annotation.*;
import org.springframework.web.bind.annotation.*;

import javax.persistence.*;
import java.security.*;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/v1/teams")
public class TeamsController {

  @Entity(name="manages_team")
  public static class ManagesTeam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="userid")
    public User user;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="teamid")
    public Team team;

    public int crtime;

    // public boolean deleted;
  }

  // private Logger logger = LoggerFactory.getLogger(this.getClass());
  @Autowired
  TeamRepository teams;

  @Autowired
  TeamGroupsRepository repo;

  @Autowired
  TeamWithUsersRepository repotu;

  @Autowired
  TeamWithGroupWithUsersRepository repotgu;

  @Autowired
  TeamInvitesRepository teamInvites;

  @Autowired
  NotificationService notifier;

  @Autowired
  GroupRepository repog;

  @Autowired
  UserRepository userrepo;

  @Autowired private TUserRepository teamUserRepository;

  @Autowired private TeamMembershipRepository teamMemberships;

  @Autowired
  MgtRepo r;

  @Autowired
  private PrivilegeManager priv;

  @Autowired JdbcTemplate jdbcTemplate;
  @Autowired EntityManager entityManager;

  private final Logger logger= LoggerFactory.getLogger(this.getClass());

  /*@PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/myteams")
  public List<TeamGroups> getTeams(Principal p) throws Exception
  {
    assert( p != null );
    
    User me= userrepo.findByEmail(p.getName()).get(0);
    
    logger.error("" + repo.getForManager(me.user_id));
    return repo.getForManager(me.user_id);
  }*/
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/myteams")
  public List<TeamWithGroups> getTeams(Principal p) throws Exception
  {
    assert( p != null );

    return repo.getForManager(p.getName());
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/myteammembers")
  public List<TeamWithUsers> getMyTeamMembers(Principal p)
  {
    assert( p != null );

    return repotu.getForManager(p.getName());
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/mygroupmembers")
  public List<TeamWithGroupsWithUsers> getMyTeamGroupMembers(Principal p)
  {
    assert( p != null );

    return repotgu.getForManager(p.getName());
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/team/{teamid}")
  public List<TeamWithUsers> getTeamMembers(Principal p, @PathVariable("teamid") long teamid)
  {
    assert( p != null );

    return repotu.findByTeamid(teamid);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/teamgroups/{teamid}")
  public List<TeamWithGroupsWithUsers> getTeamGroupMembers(Principal p, @PathVariable("teamid") long teamid)
  {
    assert( p != null );

    log.debug("/teamsgroups/{}: {}", teamid, repotgu.findByTeamid(teamid));

    return repotgu.findByTeamid(teamid);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.POST)
  public Response create(Principal p, @RequestBody Team team, @PathVariable(required = false) Long managerId)
  {
    User me= userrepo.findByEmail(p.getName()).get(0);

    if( managerId != null )
    {
      Optional<User> u= userrepo.findById(managerId);
      team.owner= u.get();
    }
    else
      team.owner= me;

    team= teams.saveAndFlush(team);

    Group g= new Group();
    g.name= "Default";
    g.setTeam(team);
    g.crtime= System.currentTimeMillis() / 1000;
    g= repog.saveAndFlush(g);

    if( managerId != null )
    {
      Optional<User> u= userrepo.findById(managerId);
      ManagesTeam mgt= new ManagesTeam();
      mgt.user= u.get();
      mgt.team= team;
      mgt.crtime= (int) (System.currentTimeMillis() / 1000);
      mgt= r.saveAndFlush(mgt);
    }
    else
    {
      ManagesTeam mgt= new ManagesTeam();
      mgt.user= me;
      mgt.team= team;
      mgt.crtime= (int) (System.currentTimeMillis() / 1000);
      mgt= r.saveAndFlush(mgt);
    }

    return new CreatedResponse("" + team.teamid);
  }

  /**
   * Get all teams with all groups
   * 
   * @param p user performing the request
   * @return all teams on the server
   */
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/allgroups")
  public List<TeamWithGroups> getAll(Principal p)
  {
    assert( p != null );

    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User " + p + " is not admin (add)");
      throw new UnauthorizedUserException(p.toString());
    }

    // TODO: why does this also fetches deleted teams?
    return repo.findAll();
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/invite/{teamid}")
  @Transactional(rollbackFor = Exception.class)
  public Response invite(Principal p, @RequestBody String email, @PathVariable("teamid") long teamid)
  {
    assert( p != null );
    User me= userrepo.findByEmail(p.getName()).get(0);
    if( email.startsWith("\"") )
      email = email.substring(1);
    if( email.endsWith("\"") )
      email = email.substring(0, email.length() - 1);
    logger.debug("Inviter {}. Email: '{}'", me, email);

    Optional<Team> ot = teams.findById(teamid);
    if( ot.isPresent() )
    {
      Team t = ot.get();

      log.debug("Inviting {} to team {}", email, t);
      if( t.owner.user_id == me.user_id )
      {
        // who should be invited
        List<User> linvitee= userrepo.findByEmail(email);
        logger.debug("Found users for {}: {}", email, linvitee);
        if( linvitee.size() > 0 )
        {
          User invitee = linvitee.get(0);

          // is this user already invited
          if( teamInvites.findByInvitedPersonTeam(invitee.user_id, t.teamid).size() == 0 )
          {
            TeamInvite invite = new TeamInvite();
            invite.userid = me.user_id;
            invite.invitedPerson = invitee.user_id;
            invite.teamid = t.teamid;
            invite.crtime = System.currentTimeMillis() / 1000;
            invite = teamInvites.save(invite);

            notifier.notifyInvitedToTeam(invitee.language, invite);

            return new CreatedResponse();
          }
          else
            return new GenericResponse(GenericResponse.FAIL, "already_invited");
        }
        else
          return new GenericResponse(GenericResponse.FAIL, "no_person_mail");
      }
      else
      {
        return new GenericResponse(GenericResponse.FAIL, "not_owner");
      }
    }
    else
    {
      return new GenericResponse(GenericResponse.FAIL, "team_error");
    }
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/acceptinvite/{notificationid}")
  @Transactional(rollbackFor = Exception.class)
  public Response acceptInvite(Principal p, @PathVariable("notificationid") long id)
  {
    NotificationService.NotificationCheckResult res = notifier.check(p, id);

    if( res == NotificationService.NotificationCheckResult.OK )
    {
      User user = userrepo.findByEmail(p.getName()).get(0);
      // There is an invite so we can just get it
      @SuppressWarnings("OptionalGetWithoutIsPresent")
      TeamInvite invite = teamInvites.findById(notifier.getDataAsLong(id)).get();

      if( teamMemberships.findUserActive(user.user_id).size() > 0 )
      {
        return new GenericResponse(GenericResponse.FAIL, "team_membership_max");
      }

      /*
      //TODO: do we need 'verein'??
      // Ueberpruefen, ob der Invite fuer einen Verein ist, und ob man bereits in einem Verein Mitglied ist
      $query = db_query('
          SELECT *
          FROM teams
          WHERE id = "'.$row['teamid'].'" AND
          type = "1" AND
          deleted = "0"
      ', $GLOBALS['conn']);
      $inviteType = 0;
      if (mysqli_num_rows($query) > 0) {
          $inviteType = 1;
          $queryTest = db_query('
              SELECT *
              FROM teams_memberships tm
              JOIN teams t ON t.id = tm.teamid
              WHERE tm.userid = "'.$userID.'" AND
              t.type = "1" AND
              t.deleted = "0" AND
              tm.deleted = "0"
          ', $GLOBALS['conn']);
          if (mysqli_num_rows($queryTest) > 0) {
              $result = array('msg' => 'Sie sind bereits in einem Verein Mitglied.',
                              'action' => 'notification_acceptTeamInvite',
                              'error' => 1);
              return json_encode($result);
          }
      }
       */

      // Set the invite as accepted
      invite.accepted = true;
      invite.declined = false;
      invite.deleted = true;
      teamInvites.save(invite);

      Group g = getDefaultGroup(invite.teamid);

      // Create the membership
      TeamMembership membership = new TeamMembership();
      // membership.crtime = System.currentTimeMillis() / 1000;
      membership.deleted = false;
      membership.teamid = invite.teamid;
      membership.groupid = g.groupid;
      membership.userid = invite.invitedPerson;
      teamMemberships.save(membership);

      notifier.setNotificationRead(id);
      notifier.notifyTeamInviteAccepted(user.language, invite);

      return GenericResponse.OkResponse;
    }

    return GenericResponse.FailResponse;
  }

  private Group getDefaultGroup(long teamid)
  {
    String sql= "(select * FROM teams_groups WHERE name = 'Default' AND deleted = false AND teamid = " + teamid +
            ") UNION (select * from teams_groups WHERE deleted = false AND teamid = " + teamid + " ORDER BY id ASC )";
    List<Group> groups = jdbcTemplate.query(sql, (rs, rowNum) -> {
      Group g = new Group();
      g.groupid = rs.getLong("id");
      g.name = rs.getString("name");
      return g;
    });

    if( groups.size() > 0 )
    {
      return groups.get(0);
    }
    else
      throw new RuntimeException("Cannot find default group for team " + teamid);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/declineinvite/{notificationid}")
  public Response declineInvite(Principal p, @PathVariable("notificationid") long id)
  {
    NotificationService.NotificationCheckResult res = notifier.check(p, id);

    if( res == NotificationService.NotificationCheckResult.OK )
    {
      User user = userrepo.findByEmail(p.getName()).get(0);
      // There is an invite so we can just get it
      @SuppressWarnings("OptionalGetWithoutIsPresent")
      TeamInvite invite = teamInvites.findById(notifier.getDataAsLong(id)).get();

      // Decline invite
      invite.accepted = false;
      invite.declined = true;
      invite.deleted = true;
      teamInvites.save(invite);

      notifier.setNotificationRead(id);
      notifier.notifyTeamInviteDeclined(user.language, invite);

      return GenericResponse.OkResponse;
    }
    return GenericResponse.FailResponse;
  }

  @Operation(
      summary = "Add a person to a group",
      description = "Adds a person to a specified group. " +
          (Config.SINGLE_TEAM_MEMBERSHIP ?
            "If this person is a member of a different team or group all other memberships will be canceled.":
            "This operation does not alter other memberships")
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/{teamid}/group/{groupid}/members/")
  @Transactional(rollbackFor = Exception.class)
  public GenericResponse group_addMember(Principal p, @PathVariable("teamid") long teamID, @PathVariable("groupid") long groupID, @RequestBody long newUserID)
  {
    Optional<TeamWithGroupsWithUsers> oteam = repotgu.findById(teamID);
    Optional<Group> ogroup = repog.findById(groupID);
    TeamWithGroupsWithUsers team;
    TUser tuser = teamUserRepository.findByEmail(p.getName()).orElseThrow(() -> new RuntimeException("Principal not in db?"));

    // check if team exists
    if( !oteam.isPresent() )
    {
      return GenericResponse.FailResponse;
    }
    // check if group exists
    if( !ogroup.isPresent() )
    {
      return GenericResponse.FailResponse;
    }
    // check if user exists
    if( !teamUserRepository.findById(newUserID).isPresent() )
    {
      return GenericResponse.FailResponse;
    }

    team = oteam.get();

    // check if user can manage the team
    if( !team.captains.contains(tuser) )
    {
      return GenericResponse.FailResponse;
    }

    // If server is configured to have only one team / group per user -> remove all active memberships before creating a new one
    if( Config.SINGLE_TEAM_MEMBERSHIP )
    {
      /*teamMemberships.findUserActive(newUserID).forEach(tm -> {
        log.debug("{}", tm);
        tm.deleted = true;
        // teamMemberships.savetm);
        entityManager.createQuery("update TeamMembership set deleted = true where id = " + tm.id).executeUpdate();
      });*/
      entityManager.createQuery("update TeamMembership set deleted = true where userid = " + newUserID).executeUpdate();
    }
    // invalidate all 'dummy' memberships
    entityManager.createQuery("update TeamMembership set deleted = true where userid = " + newUserID +
          " AND teamid = " + teamID + " AND groupid is null").executeUpdate();
    // invalidate all other memberships in groups of this team
    entityManager.createQuery("update TeamMembership set deleted = true where userid = " + newUserID +
        " AND teamid = " + teamID).executeUpdate();

    TeamMembership tm = new TeamMembership();
    tm.userid = newUserID;
    tm.teamid = teamID;
    tm.groupid = groupID;
    tm.deleted = false;
    teamMemberships.save(tm);

    return GenericResponse.OkResponse;
  }

  /**
   * group_removeFromGroup
   * =====================
   *
   * Entfernt einen User aus einer Gruppe.
   *
   * @param p: User der den Loeschvorgang gestartet hat
   * @param teamID: ID des Teams
   * @param groupID: ID der Gruppe
   * @param removeUserID: ID des Users, der entfernt werden soll
   */
  @Operation(
      summary = "Remove a person from a group",
      description = "Removes a person from a specified group. The person is still considered as a member of the team after this operation"
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.DELETE, value = "/{teamid}/group/{groupid}/members/{userid}")
  @Transactional(rollbackFor = Exception.class)
  public GenericResponse group_removeFromGroup(Principal p, @PathVariable("teamid") long teamID, @PathVariable("groupid") long groupID, @PathVariable("userid") long removeUserID)
  {
    log.debug("removeFromGroup {} {} {} {}", p.getName(), teamID, groupID, removeUserID);
    Optional<TeamWithGroupsWithUsers> oteam = repotgu.findById(teamID);
    Optional<Group> ogroup = repog.findById(groupID);
    TeamWithGroupsWithUsers team;
    TUser tuser = teamUserRepository.findByEmail(p.getName()).orElseThrow(() -> new RuntimeException("Principal not in db?"));

    // check if team exists
    if( !oteam.isPresent() )
    {
      log.error("Trying to remove member from team {} failed: does not exist", teamID);
      return GenericResponse.FailResponse;
    }
    // check if group exists
    if( !ogroup.isPresent() )
    {
      log.error("Trying to remove member from group {} failed: does not exist", groupID);
      return GenericResponse.FailResponse;
    }
    // check if user exists
    if( !teamUserRepository.findById(removeUserID).isPresent() )
    {
      log.error("Trying to remove member {} from team failed: does not exist", removeUserID);
      return GenericResponse.FailResponse;
    }

    team = oteam.get();

    // check if user can manage the team
    if( !team.captains.contains(tuser) )
    {
      log.error("Trying to remove member from team failed: {} is not captain", tuser);
      return GenericResponse.FailResponse;
    }

    List<TeamMembership> memberships = teamMemberships.findActive(removeUserID, teamID, groupID);
    if( memberships.size() == 0 )
    {
      // If we end up here -> something is wrong we are deleting a membership that does not exist
      log.error("Trying to remove member {} from team failed: is not member", removeUserID);
      return GenericResponse.FailResponse;
    }
    memberships.forEach(m -> {
      m.deleted = true;
      teamMemberships.save(m);
    });

    // Person will not disappear from the team -> add to group 0 / null
    TeamMembership tm = new TeamMembership();
    tm.userid = removeUserID;
    tm.teamid = teamID;
    tm.groupid = null;
    tm.deleted = false;
    // teamMemberships.save(tm);

    entityManager.createNativeQuery("insert into teams_memberships(userid, teamid, groupid, deleted, crtime) values (" +
        removeUserID + "," + teamID + ", null, false, " + System.currentTimeMillis() / 1000 + ")").executeUpdate();
    /*Query q = entityManager.createNativeQuery("INSERT INTO teams_memberships (userid, teamid, groupid, deleted, crtime) values (?, ?, ?, ?, ?)");
    q.setParameter(1, removeUserID);
    q.setParameter(2, removeUserID);
    q.setParameter(3, teamID);
    q.setParameter(4, null);
    q.setParameter(5, System.currentTimeMillis() / 1000);
    q.executeUpdate();*/


    return GenericResponse.OkResponse;
  }

  /**
   * Remove a user from a team
   *
   * @param p: User der den Loeschvorgang gestartet hat
   * @param teamID: ID des Teams
   * @param removeUserID: ID des Users, der entfernt werden soll
   */
  @Operation(
      summary = "Remove a person from a team",
      description = "Removes a person from a specified team completely. This means that the membership of this person in the team is canceled."
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.DELETE, value = "/{teamid}/members/{userid}")
  @Transactional(rollbackFor = Exception.class)
  public GenericResponse removeMember(Principal p, @PathVariable("teamid") long teamID, @PathVariable("userid") long removeUserID)
  {
    Optional<TeamWithGroupsWithUsers> oteam = repotgu.findById(teamID);
    TeamWithGroupsWithUsers team;
    TUser tuser = teamUserRepository.findByEmail(p.getName()).orElseThrow(() -> new RuntimeException("Principal not in db?"));

    // check if team exists
    if( !oteam.isPresent() )
    {
      return GenericResponse.FailResponse;
    }
    // check if user exists
    if( !teamUserRepository.findById(removeUserID).isPresent() )
    {
      return GenericResponse.FailResponse;
    }

    team = oteam.get();

    // check if user can manage the team
    if( !team.captains.contains(tuser) )
    {
      return GenericResponse.FailResponse;
    }

    List<TeamMembership> memberships = teamMemberships.findActive(removeUserID, teamID);
    if( memberships.size() == 0 )
    {
      // If we end up here -> something is wrong we are deleting a membership that does not exist
      return GenericResponse.FailResponse;
    }
    memberships.forEach(m -> {
      /*m.deleted = true;
      teamMemberships.save(m);*/
      entityManager.createNativeQuery("update teams_memberships set deleted = true where id = " + m.id).executeUpdate();

    });

    return GenericResponse.OkResponse;
  }
}
