package at.pegasos.server.frontback.reports;

import java.util.*;
import java.util.stream.Collectors;

import lombok.extern.slf4j.*;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.repositories.UserValueService;

@Slf4j
public class UserValueBasedReportGenerator extends ReportGenerator {

  SortedSet<Long> dates;

  private Set<Long> users_with_values;

  private final String[] names;

  private final Set<String> namesWithValues;

  private final UserValueService valueservice;

  private final Map<Long, Map<String, List<UserValue>>> vals;

  public UserValueBasedReportGenerator(UserValueService valueservice, String[] names)
  {
    super();
    this.valueservice= valueservice;
    this.names= names;

    this.namesWithValues= new HashSet<String>(this.names.length);

    this.vals= new HashMap<Long, Map<String, List<UserValue>>>(names.length*2);

    this.dates= new TreeSet<Long>();
  }

  private void fetchValues()
  {
    // Get all users that have at least one value
    users_with_values= new HashSet<Long>(this.users.size());

    for(Long user : this.users)
    {
      log.debug("Get values for user:{} names:{} start:{} end:{}", user, Arrays.toString(names), start.getTime(), end.getTime());
      List<UserValue> values= valueservice.getForUserByNamesAfterBefore(user, names, start, end);
      log.debug("Values: {} {}", values, values.size());

      int h = values.size() / names.length;

      Map<String, List<UserValue>> uservals= new HashMap<String, List<UserValue>>();
      for(UserValue value : values)
      {
        if( !uservals.containsKey(value.getValue().name) )
        {
          uservals.put(value.getValue().name, new ArrayList<UserValue>(h));
        }
        uservals.get(value.getValue().name).add(value);
      }
      
      vals.put(user, uservals);

      dates.addAll(values.stream().map(v -> {
        Calendar c= Calendar.getInstance();
        c.clear();
        c.setTimeZone(TimeZone.getTimeZone("GMT-0"));
        c.set(v.date_start.get(Calendar.YEAR), v.date_start.get(Calendar.MONTH), v.date_start.get(Calendar.DAY_OF_MONTH));
        return c.getTimeInMillis();
      }).collect(Collectors.toSet()));
      log.debug("dates: {} {}", dates, dates.size());

      // add names to set of names which actually have values
      namesWithValues.addAll(values.stream().map(v -> v.getValue().name).collect(Collectors.toSet()));
      
      if( values.size() > 0 )
      {
        users_with_values.add(user);
      }
    }
  }

  public Report generateReport()
  {
    fetchValues();

    ReportUserDatesSingle report= new ReportUserDatesSingle();

    List<DataSeries<Double>> series= new ArrayList<DataSeries<Double>>(users_with_values.size()*names.length);

    List<Double> valSeries;

    // For each user
    for(Long user : this.users)
    {
      // and every variable / name
      for(String variable : namesWithValues)
      {
        // create a series
        valSeries = new ArrayList<Double>(dates.size());
        List<UserValue> valuesForVariable = vals.get(user).get(variable);

        // if there are no values for this variable we can just add nulls for this series
        if( valuesForVariable == null || valuesForVariable.size() == 0 )
        {
          for(int i= 0; i < dates.size(); i++)
          {
            valSeries.add(null);
          }
        }
        else
        {
//          log.debug("Have values of {} for {}", user, variable);
          UserValue current = null;
          Iterator<UserValue> valueIterator = valuesForVariable.iterator();
          for(Long date : dates)
          {
//            log.debug("Date {}", date);
            if( current == null && valueIterator.hasNext() )
              current = valueIterator.next();

            if( current != null )
            {
              // current date is before the next value in the list --> add missing
              if( date + 86400_000 < current.date_start.getTimeInMillis() )
              {
//                log.debug("No value for date {} (gap)", date);
                valSeries.add(null);
              }
              else
              {
                // now peak ahead and see if we have multiple values for this day
                if( valueIterator.hasNext() )
                {
                  UserValue tmp = current;
                  current = valueIterator.next();
                  if( matchDay(tmp.date_start, current.date_start) )
                  {
//                    log.debug("{} and {} have the same date", tmp, current);
                    while( valueIterator.hasNext() && matchDay(tmp.date_start, current.date_start) )
                    {
                      tmp = current;
                      current = valueIterator.next();
                    }
                    valSeries.add(tmp.getDoubleValue());
                  }
                  else
                  {
                    // next value in line is for a future date, add tmp. current will be used for next iteration
                    valSeries.add(tmp.getDoubleValue());
                  }
                }
                else
                {
                  // we do not have any remaining values and can just finish
                  valSeries.add(current.getDoubleValue());
                  current = null;
                }
              }
            }
            else
            {
//              log.debug("No value for date {}", date);
              valSeries.add(null);
            }
          }
        }

        series.add(new DataSeries<Double>(user + "#" + variable, valSeries));
      }
    }

    report.dates = dates.stream().map(d -> {
      Calendar c= Calendar.getInstance();
      c.setTimeInMillis(d);
      return c;
    }).collect(Collectors.toList());
    report.users = users_with_values;
    report.series = series;

    return report;
  }

  public static boolean matchDay(Calendar d1, Calendar d2)
  {
    if( d1.get(Calendar.YEAR) == d2.get(Calendar.YEAR) )
    {
      if( d1.get(Calendar.MONTH) == d2.get(Calendar.MONTH) )
      {
        return d1.get(Calendar.DAY_OF_MONTH) == d2.get(Calendar.DAY_OF_MONTH);
      }
      else
        return false;
    }
    else
      return false;
  }
}
