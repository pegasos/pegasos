package at.pegasos.server.frontback.controller;

import java.io.Serializable;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.ValueChanges.Change;
import at.pegasos.server.frontback.repositories.*;

@RestController
@RequestMapping("/v1/values/")
public class Values {

  public static class ValueUpdate implements Serializable {

    private static final long serialVersionUID= 1L;

    public Long id;
    public Date date_start;
    public Date date_end;
    public String value;
  }
  
  public static class ValueCreation implements Serializable {

    private static final long serialVersionUID= 1L;

    public Date date_start;
    public Date date_end;
    public String value;
  }

  @Autowired
  private UserValueService uservalues;
  
  @Autowired
  private UserDetailsRepository userrepo;

  @Autowired
  UserValueInfoRepository infos;

  @Autowired
  UserValueIntRepository ints;

  @Autowired
  UserValueDecRepository decs;

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  /**
   * Get all available values
   * @param p
   * @return
   */
  @RequestMapping(method = RequestMethod.GET, value = "/avail")
  public List<UserValueInfo> getAvail()
  {
    return uservalues.availableValues();
  }
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get/{user}")
  public List<UserValue> getValues(Principal p, @PathVariable("user") Long userid)
  {
    return uservalues.getForUser(userid);
  }
  
  /**
   * Retrieve the history of a value for a user
   * @param p
   * @param valueId ID of the value
   * @param userId ID of the user
   * @return all values in ascending (oldest to newest) order
   */
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = {"/history/{value}", "/history/{value}/{user}"})
  public List<UserValue> getHistory(Principal p, @PathVariable(name= "value", required= false) Long valueId,
      @PathVariable(name = "user", required= false) Long userId)
  {
    // TODO check if allowd to query
    if( userId != null )
    {
      return uservalues.getHistoryForUser(userId, valueId);
    }
    else
    {
      UserDetails me= userrepo.findByEmail(p.getName()).get(0);
      return uservalues.getHistoryForUser(me.id, valueId);
    }
  }

  /*@PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/value/{name}/{value}")
  public GenericResponse setValue(Principal p, @PathVariable("name") String name, @PathVariable("value") String value, 
      @RequestParam("user") Long userid, @RequestParam("start") String start)
  {
    long user;
    if( userid != null )
    {
      user= userid;
    }
    else
    {
      UserDetails me= userrepo.findByEmail(p.getName()).get(0);
      user= me.id;
    }
    
    if( start != null )
    {
      Calendar cal = Calendar.getInstance();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      try
      {
        cal.setTime(sdf.parse(start));
        uservalues.setValueForUser(name, user, value, cal);
      }
      catch( ParseException e )
      {
        e.printStackTrace();
        // TODO: exception
      }
    }
    else
    {
      uservalues.setValueForUser(name, user, value);
    }
    
    return new GenericResponse("OK");
  }*/
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/set")
  public GenericResponse setValues(Principal p, @RequestParam("user") Long userid, @RequestParam(required= false) String start,
      @RequestBody ValueChanges changes)
  {
    long user;
    if( userid != null )
    {
      user= userid;
    }
    else
    {
      UserDetails me= userrepo.findByEmail(p.getName()).get(0);
      user= me.id;
    }
    
    if( start != null )
    {
      Calendar cal = Calendar.getInstance();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      try
      {
        cal.setTime(sdf.parse(start));
        for(Change change : changes.changes)
        {
          if( !change.value.equals("") )
          {
            uservalues.setValueForUser(change.name, user, change.value, cal);
          }
          else
          {
            uservalues.invalidateValueForUser(change.name, user, cal);
          }
        }
      }
      catch( ParseException e )
      {
        e.printStackTrace();
        return new GenericResponse("Fail.Date");
      }
    }
    else
    {
      for(Change change : changes.changes)
      {
        uservalues.setValueForUser(change.name, user, change.value);
      }
    }
    
    return new GenericResponse(GenericResponse.OK);
  }

  /**
   * Get the value-information of a value
   *
   * @param valueId
   * @return
   */
  @RequestMapping(method = RequestMethod.GET, value = "/info/{value}")
  public UserValueInfo getInfo(@PathVariable("value") Long valueId)
  {
    Optional<UserValueInfo> o= infos.findById(valueId);
    if( o.isPresent() )
      return o.get();

    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value");
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/update/{value}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public GenericResponse updateValues(Principal p, @PathVariable("value") Long valueId, @RequestBody ValueUpdate[] changes)
  {
    logger.info(Arrays.toString(changes));

    // TODO: check if change is allowed

    if( uservalues.update(valueId, changes) )
      return GenericResponse.OkResponse;
    else
      return GenericResponse.FailResponse;
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.POST, value = "/{value}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public UserValue[] create(Principal p, @PathVariable("value") Long valueId, @RequestParam(required= false) Long user,
      @RequestBody ValueCreation[] create)
  {
    long userId;
    if( user != null )
    {
      userId= user;
    }
    else
    {
      UserDetails me= userrepo.findByEmail(p.getName()).get(0);
      userId= me.id;
    }

    return uservalues.create(valueId, userId, create);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.DELETE, value = "/{valueId}/{id}")
  public GenericResponse deleteValue(Principal p, @PathVariable("valueId") long valueId, @PathVariable("id") long id)
  {
    assert( p != null );

    // TODO: check if allowed

    logger.info("Delete " + valueId + "/" + id);

    if( uservalues.delete(valueId, id) )
    {
      return new GenericResponse(GenericResponse.OK);
    }
    else
    {
      return new GenericResponse(GenericResponse.OK);
    }
  }
}
