package at.pegasos.server.frontback.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.UserValueInfo;

@Repository
@Transactional(readOnly = true)
public interface UserValueInfoRepository extends JpaRepository<UserValueInfo, Long>  {
  
  UserValueInfo findByName(String name);
}
