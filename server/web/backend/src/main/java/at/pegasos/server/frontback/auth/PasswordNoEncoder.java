package at.pegasos.server.frontback.auth;

import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordNoEncoder implements PasswordEncoder {
  @Override
  public String encode(CharSequence rawPassword)
  {
    return rawPassword.toString();
  }
  
  @Override
  
  public boolean matches(CharSequence rawPassword, String encodedPassword)
  {
    return rawPassword.equals(encodedPassword);
  }
  
  /*private void trace()
  {
    StackTraceElement[] stackTrace= Thread.currentThread().getStackTrace();
    
    // Once you get StackTraceElement you can also print it to console
    for(StackTraceElement st : stackTrace)
    {
      // System.err.println(st);
      logger.error("\t" + st.getClassName() + "(" + st.getMethodName() + ":" + st.getLineNumber() + ")");
    }
  }*/
}