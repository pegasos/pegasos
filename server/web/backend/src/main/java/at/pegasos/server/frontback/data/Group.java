package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="teams_groups")
public class Group {
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public Long groupid;

  /**
   * Name of the group
   */
  @Column(name="name")
  public String name;

  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="teamid")
  private Team team;

  public long crtime;

  public void setTeam(Team team)
  {
    this.team= team;
  }
}
