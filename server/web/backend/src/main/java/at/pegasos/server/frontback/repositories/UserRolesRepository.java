package at.pegasos.server.frontback.repositories;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.Repository;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.UserHasRole;

@Repository
public interface UserRolesRepository extends JpaRepository<UserHasRole, Long> {

  @Query("SELECT r FROM UserHasRole r WHERE r.user_id = :#{#user.user_id} AND r.role_id = :#{#role.id} AND r.date_end is null")
  UserHasRole findActiveRole(User user, Role role);
}
