package at.pegasos.server.frontback.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PersonSelect {
  
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public static String buildQueryString(List<String> persons)
  {
    String query= "(";
    boolean or_needed= false;

    // teams
    String tQuery= persons.stream()
            .filter(tgp -> tgp.startsWith("t"))
            .map(tgp -> tgp.substring(2))
            .collect(Collectors.joining(","));
    if( tQuery.length() > 0 )
    {
      query+= "s.userid in (select _tm.userid from teams_memberships _tm where _tm.teamid in (" + tQuery + ") and _tm.deleted = false)";
      or_needed = true;
    }

    // groups
    String gQuery= persons.stream()
            .filter(tgp -> tgp.startsWith("g"))
            .map(tgp -> tgp.substring(2))
            .collect(Collectors.joining(","));
    if( gQuery.length() > 0 )
    {
      if( or_needed )
        query+= " OR ";
      query+= "s.userid in (select _tm.userid from teams_memberships _tm where _tm.groupid in (" + gQuery + ") and _tm.deleted = false)";
      or_needed = true;
    }

    // single persons
    String pQuery= persons.stream()
            .filter(tgp -> tgp.startsWith("p"))
            .map(tgp -> tgp.substring(2))
            .collect(Collectors.joining(","));
    if( pQuery.length() > 0 )
    {
      if( or_needed )
        query+= " OR ";
      query+= "s.userid in (" + pQuery + ")";
    }

    query+= ")";

    return query;
  }

  public List<Long> getUsers(List<String> persons)
  {
    String query = "";
    
    boolean union_needed= false;
    
    // teams
    String tQuery= persons.stream()
      .filter(tgp -> tgp.startsWith("t"))
      .map(tgp -> tgp.substring(2))
      .collect(Collectors.joining(","));
    if( tQuery.length() > 0 )
    {
      query+= "(select _tm.userid from teams_memberships _tm where _tm.teamid in (" + tQuery + ") and _tm.deleted = 0)";
      union_needed = true;
    }
    
    // groups
    String gQuery= persons.stream()
      .filter(tgp -> tgp.startsWith("g"))
      .map(tgp -> tgp.substring(2))
      .collect(Collectors.joining(","));
    if( gQuery.length() > 0 )
    {
      if( union_needed )
        query+= " UNION ";
      query+= "(select _tm.userid from teams_memberships _tm where _tm.groupid in (" + gQuery + ") and _tm.deleted = 0)";
      union_needed = true;
    }
    
    LoggerFactory.getLogger(this.getClass()).debug(query);
    // have we queried persons before?
    if( union_needed )
    {
      List<Map<String, Object>> ids= jdbcTemplate.queryForList(query);
    
      // add single persons
      Set<Long> tmp= ids.stream().map(map -> Long.parseLong(map.get("userid").toString())).collect(Collectors.toSet());
      tmp.addAll(persons.stream()
            .filter(tgp -> tgp.startsWith("p"))
            .map(tgp -> Long.parseLong(tgp.substring(2)))
            .collect(Collectors.toList()));
      return new ArrayList<Long>(tmp);
    }
    else
    {
      // single persons
      return persons.stream()
            .filter(tgp -> tgp.startsWith("p"))
            .map(tgp -> Long.parseLong(tgp.substring(2)))
            .collect(Collectors.toList());
    }
  }
}
