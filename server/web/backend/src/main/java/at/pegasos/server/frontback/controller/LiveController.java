package at.pegasos.server.frontback.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.pegasos.server.frontback.ActivityNames;
import at.pegasos.server.frontback.data.User;
import at.pegasos.server.frontback.repositories.UserRepository;

@RestController
@RequestMapping("/v1/live/")
public class LiveController {
  
  private final String query= "select u.id as userid, s.session_id, s.starttime, s.last_update, s.activity, s.param1, s.param2, " +
    "s.last_gps_alt_cm, s.last_gps_speed_cms, s.last_gps_accur, s.last_gps_bearing, s.last_gps_lat, s.last_gps_lon, " + 
    "s.v1, s.v2, s.v3, s.v4, s.v5, s.v6, s.v7, s.v8, s.v9, s.v10, s.f1, s.f2, s.f3, s.f4, s.f5 " + 
    "from session s left join user u on s.userid = u.id where u.id in " +
      "(select userid from teams_memberships tm where tm.teamid in (" + 
        "select team.id from teams team where team.id in (" +
          "select mgr.teamid from manages_team mgr where mgr.userid = %d AND deleted = false)" +
      ") and tm.deleted = false) " +
    "and s.endtime = 0 and s.last_update > %d";
  
  @Autowired
  JdbcTemplate jdbcTemplate;
  
  @Autowired
  UserRepository userrepo;
  
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get")
  public List<Map<String, Object>> getReport(Principal p)
  {
    User me= userrepo.findByEmail(p.getName()).get(0);
    
    List<Map<String, Object>> activities= jdbcTemplate.queryForList(String.format(query, me.user_id, System.currentTimeMillis() - 6000));
    
    /*
    data.v[1]=hr;
    data.v[2]=hr_var[0];
    data.v[3]= distance_m;
    data.v[4]= speed_mm_s;
    data.v[5]= (int) data.fp_strides_cul;
    data.v[6]=stroke_count;
    data.v[7]=stroke_avg;
     */
    
    ArrayList<Map<String, Object>> ret= new ArrayList<Map<String, Object>>(activities.size());
    
    for(Map<String, Object> session : activities)
    {
      Map<String, Object> r= new HashMap<String, Object>();
      r.put("session_id", (Long) session.get("session_id"));
      r.put("user_id", (Integer) session.get("userid"));
      r.put("starttime", (Long) session.get("starttime"));
      r.put("last_update", (Long) session.get("last_update"));
      
      int p0, p1, p2;
      r.put("activity_name", ActivityNames.getActivityName(p0= (Integer) session.get("activity"), 
          p1= (Integer) session.get("param1"), 
          p2= (Integer) session.get("param2")));
      r.put("param0", p0);
      r.put("param1", p1);
      r.put("param2", p2);
      
      Integer g;
      g= (Integer) session.get("last_gps_alt_cm");
      if( g != null )
        r.put("last_gps_alt_cm", g);
      g= (Integer) session.get("last_gps_speed_cms");
      if( g != null )
        r.put("last_gps_speed_cms", g);
      g= (Integer) session.get("last_gps_accur");
      if( g != null )
        r.put("last_gps_accur", g);
      g= (Integer) session.get("last_gps_bearing");
      if( g != null )
        r.put("last_gps_bearing", g);
      Double d= (Double) session.get("last_gps_lat");
      if( d != null )
      {
        r.put("last_gps_lat", d / 100000000);
        r.put("last_gps_lon", ((Double) session.get("last_gps_lon")) / 100000000);
      }
      
      Integer val;
      int i= 1;
      for(String map : MAPPING)
      {
        if( map != null )
        {
          val= (Integer) session.get("v" + i);
          if( val != null )
          {
            r.put(map, val);
          }
        }
        i++;
      }
      
      ret.add(r);
      
      logger.debug(r.toString());
    }
    
    return ret;
  }
  
  private final static String[] MAPPING = new String[] {"hr", null, null, "distance_m", "speed_mm_s", "strides", "stroke_count", "stroke_avg"};
}
