package at.pegasos.server.frontback.dbdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.pegasos.server.frontback.data.MetricInfo;
import at.pegasos.server.frontback.data.SessionInfo;

@Entity
@Table(name="session_metric")
public class Metric {
  @Override
  public String toString()
  {
    return "Metric [ID=" + ID + ", metricId=" + metricId + ", metric=" + metric + ", sessionId=" + sessionId + ", session=" + session
        + ", value=" + value + ", segmentnr=" + segmentnr + ", param=" + param + "]";
  }

  @GeneratedValue
  @Id
  public long ID;
  
  @Column(name="metric")
  public long metricId;
  
  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="metric", insertable=false, updatable= false)
  public MetricInfo metric;
  
  @Column(name="session_id")
  public long sessionId;
  
  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="session_id", insertable=false, updatable= false)
  public SessionInfo session;
  
  public double value;
  
  public Integer segmentnr;
  
  public String param;
}
