package at.pegasos.server.frontback.repositories;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import at.pegasos.server.frontback.controller.Values.ValueCreation;
import at.pegasos.server.frontback.controller.Values.ValueUpdate;
import at.pegasos.server.frontback.data.UserValue;
import at.pegasos.server.frontback.data.UserValueDec;
import at.pegasos.server.frontback.data.UserValueInfo;
import at.pegasos.server.frontback.data.UserValueInt;

@Service
@Transactional
public class UserValueService {
  
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public class UnkownValueException extends RuntimeException {

    public UnkownValueException(String name)
    {
      super(name);
    }

    /**
     * 
     */
    private static final long serialVersionUID= 3626239569553230108L;
    
  }
  
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  UserValueIntRepository ints;
  
  @Autowired
  UserValueDecRepository decs;
  
  @Autowired
  UserValueInfoRepository infos;
  
  @Autowired
  private EntityManager entityManager;

  private Session getSession() {
      return entityManager.unwrap(Session.class);
  }
  
  /**
   * Get all available values
   * 
   * @return
   */
  public List<UserValueInfo> availableValues()
  {
    return infos.findAll();
  }
  
  /**
   * Set the value for a user starting `now`. This method first checks the type of the value
   * 
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @param value
   *          actual value
   * @return if the value was changed
   */
  public boolean setValueForUser(String name, long userid, String value)
  {
    return setValueForUser(name, userid, value, Calendar.getInstance());
  }
  
  /**
   * Set the value for a user. This method first checks the type of the value
   * 
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @param value
   *          actual value
   * @param start
   *          date for which the value should be set
   * @return if the value was changed
   */
  public boolean setValueForUser(String name, long userid, String value, Calendar start)
  {
    boolean legacy= false;
    
    // Legacy changes
    if( name.equals("WEIGHT") )
    {
      changeWeight(userid, Double.parseDouble(value));
      legacy= true;
    }
    else if( name.equals("HEIGHT") )
    {
      changeHeight(userid, Double.parseDouble(value));
      legacy= true;
    }
    else if( name.equals("VO2MAX") )
    {
      changeData(1, userid, Double.parseDouble(value));
      legacy= true;
    }
    else if( name.equals("MAXHR") )
    {
      changeData(2, userid, Double.parseDouble(value));
      legacy= true;
    }
    else if( name.equals("FP_CALIB") )
    {
      changeData(3, userid, Double.parseDouble(value));
      legacy= true;
    }
    else if( name.equals("RESTHR") )
    {
      changeData(4, userid, Double.parseDouble(value));
      legacy= true;
    }
    
    UserValueInfo val= infos.findByName(name);
    if( val == null && !legacy )
      throw new UnkownValueException(name);
    else if( val == null )
    {
      logger.debug("Changing only legacy value " + name);
      return true;
    }
    else
    {
      logger.debug("Changing value type:" + val.getType() + "/" + name);
      
      switch( val.getType() )
      {
        case DECIMAL:
          return setValueForUserDec(name, userid, Double.parseDouble(value), start);
        case INTEGER:
          return setValueForUserInt(name, userid, (int) Double.parseDouble(value), start);
        case TEXT:
          // TODO: implement text values
        default:
          throw new IllegalArgumentException(val.getType().toString());
      }
    }
    // return false;
  }
  
  public boolean setValueForUserDec(String name, long userid, double value)
  {
    return setValueForUserDec(name, userid, value, Calendar.getInstance());
  }
  
  public boolean setValueForUserDec(String name, long userid, double value, Calendar start)
  {
    List<UserValueDec> oldvalues= decs.findByNameAndUserId(name, userid);
    UserValueInfo val;
    
    logger.debug("#old values for " + name + " and user " + userid + ": " + oldvalues.size());
    
    if( oldvalues.size() > 0 )
    {
      UserValueDec old= oldvalues.get(0);
      
      logger.debug("#old values enddate: " + old.date_end);
      if( old.date_end == null )
      {
        Calendar end= Calendar.getInstance();
        end.setTime(start.getTime());
        end.add(Calendar.MILLISECOND, -1);;
        old.date_end= end;
        decs.saveAndFlush(old);
      }
      
      val= old.getValue();
    }
    else
    {
      val= infos.findByName(name);
    }
    
    UserValueDec newval= new UserValueDec();
    newval.date_start= start;
    newval.internal= value;
    newval.user_id= userid;
    newval.setValue(val);
    
    decs.save(newval);
    
    // Legacy changes
    /*if( name.equals("WEIGHT") )
    {
      changeWeight(user, value);
    }
    else if( name.equals("VO2MAX") )
    {
      changeData(1, user, value);
    }
    else if( name.equals("MAXHR") )
    {
      changeData(2, user, value);
    }
    else if( name.equals("FP_CALIB") )
    {
      changeData(3, user, value);
    }
    else if( name.equals("RESTHR") )
    {
      changeData(4, user, value);
    }*/
    
    return true;
  }
  
  public boolean setValueForUserInt(String name, long userid, int value)
  {
    return setValueForUserInt(name, userid, value, Calendar.getInstance());
  }
  
  public boolean setValueForUserInt(String name, long userid, int value, Calendar start)
  {
    List<UserValueInt> oldvalues= ints.findByNameAndUserId(name, userid);
    UserValueInfo val;
    
    logger.debug("#old values for " + name + " and user " + userid + ": " + oldvalues.size());
    
    if( oldvalues.size() > 0 )
    {
      UserValueInt old= oldvalues.get(0);
      
      logger.debug("#old values enddate: " + old.date_end);
      if( old.date_end == null )
      {
        Calendar end= Calendar.getInstance();
        end.setTime(start.getTime());
        end.add(Calendar.MILLISECOND, -1);;
        old.date_end= end;
        ints.saveAndFlush(old);
      }
      
      val= old.getValue();
    }
    else
    {
      val= infos.findByName(name);
    }
    
    UserValueInt newval= new UserValueInt();
    newval.date_start= start;
    newval.internal= value;
    newval.user_id= userid;
    newval.setValue(val);
    
    ints.save(newval);
    
    return true;
  }
  
  public List<UserValue> getForUsersByNamesAfterBefore(Long[] users, Collection<String> names, Calendar start, Calendar end)
  {
    Set<Long> u= new HashSet<Long>();
    Collections.addAll(u, users);
    return getForUsersByNamesAfterBefore(u, names, start, end);
  }
  
  public List<UserValue> getForUsersByNamesAfterBefore(Collection<Long> users, String[] names, Calendar start, Calendar end)
  {
    Set<String> n= new HashSet<String>();
    Collections.addAll(n, names);
    return getForUsersByNamesAfterBefore(users, n, start, end);
  }
  
  public List<UserValue> getForUsersByNamesAfterBefore(Long[] users, String[] names, Calendar start, Calendar end)
  {
    Set<Long> u= new HashSet<Long>();
    Collections.addAll(u, users);
    Set<String> n= new HashSet<String>();
    Collections.addAll(n, names);
    return getForUsersByNamesAfterBefore(u, n, start, end);
  }

  public List<UserValue> getForUserByNamesAfterBefore(Long user, String[] names, Calendar start, Calendar end)
  {
    Set<Long> u= new HashSet<Long>();
    u.add(user);
    Set<String> n= new HashSet<String>();
    Collections.addAll(n, names);
    return getForUsersByNamesAfterBefore(u, n, start, end);
  }

  public List<UserValue> getForUsersByNamesAfterBefore(Collection<Long> users, Collection<String> names, Calendar start, Calendar end)
  {
    // String qd= "FROM UserValueDec v where v.value.name in (:names) AND v.user_id in :users AND DATE(v.date_start) >= DATE(:start) AND DATE(v.date_start) <= DATE(:end) ORDER BY v.value.name, v.user_id, v.date_start";
    String qd= "FROM UserValueDec v where v.value.name in (:names) AND v.user_id in :users AND DATE(v.date_start) between DATE(:start) AND DATE(:end) ORDER BY v.value.name, v.user_id, v.date_start";
    String qi= "FROM UserValueInt v where v.value.name in (:names) AND v.user_id in :users AND v.date_start between :start AND :end ORDER BY v.value.name, v.user_id, v.date_start";
    
    logger.debug(qd);

    Query<UserValueDec> queryd= getSession().createQuery(qd, UserValueDec.class);
    queryd.setParameter("start", start);
    queryd.setParameter("end", end);
    queryd.setParameterList("names", names);
    queryd.setParameterList("users", users);
    List<UserValueDec> d= queryd.list();
    
    Query<UserValueInt> queryi= getSession().createQuery(qi, UserValueInt.class);
    queryi.setParameter("start", start);
    queryi.setParameter("end", end);
    queryi.setParameterList("names", names);
    queryi.setParameterList("users", users);
    List<UserValueInt> i= queryi.list();
    
    List<UserValue> ret= new ArrayList<UserValue>(d.size() + i.size());
    ret.addAll(d);
    ret.addAll(i);
    
    return ret;
  }
  
  /**
   * Get all current values for a user
   * 
   * @param user
   *          ID of the user
   * @return
   */
  public List<UserValue> getForUser(Long user)
  {
    String qd= "FROM UserValueDec v where v.user_id = :user AND v.date_end is null ORDER BY v.value.name, v.user_id, v.date_start";
    String qi= "FROM UserValueInt v where v.user_id = :user AND v.date_end is null ORDER BY v.value.name, v.user_id, v.date_start";
    
    Query<UserValueDec> queryd= getSession().createQuery(qd, UserValueDec.class);
    queryd.setParameter("user", user);
    List<UserValueDec> d= queryd.list();
    
    Query<UserValueInt> queryi= getSession().createQuery(qi, UserValueInt.class);
    queryi.setParameter("user", user);
    List<UserValueInt> i= queryi.list();
    
    List<UserValue> ret= new ArrayList<UserValue>(d.size() + i.size());
    ret.addAll(d);
    ret.addAll(i);
    
    return ret;
  }

  /**
   * Get all values for a user for a given value
   * 
   * @param user
   *          ID of the user
   * @param valueName
   *          Name of the value
   * @return
   
  public List<UserValue> getHistoricForUser(Long user, String valueName)
  {
    UserValueInfo info= infos.findByName(valueName);
    if( info != null )
    {
      if( info.type.equals("int") )
      {
        List<UserValueInt> i= ints.findByValueIdAndUserId(info.id, user);
        List<UserValue> ret= new ArrayList<UserValue>(i.size());
        ret.addAll(i);
        return ret;
      }
      else if( info.type.equals("dec") )
      {
        List<UserValueDec> d= decs.findByValueIdAndUserId(info.id, user);
        List<UserValue> ret= new ArrayList<UserValue>(d.size());
        ret.addAll(d);
        return ret;
      }
      else
        throw new IllegalStateException("Not implemented: " + info.type);
    }
    else
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
  }*/

  /**
   * Get all value instances for a user for a given value
   *
   * @param user
   *          ID of the user
   * @param valueId
   *          ID of the value
   * @return
   */
  public List<UserValue> getHistoryForUser(Long user, Long valueId)
  {
    Optional<UserValueInfo> oinfo= infos.findById(valueId);
    if( oinfo.isPresent() )
    {
      UserValueInfo info= oinfo.get();
      switch( info.getType() )
      {
        case DECIMAL:
          List<UserValueDec> d= decs.findByValueIdAndUserId(info.id, user);
          List<UserValue> retd= new ArrayList<UserValue>(d.size());
          retd.addAll(d);
          return retd;

        case INTEGER:
          List<UserValueInt> i= ints.findByValueIdAndUserId(info.id, user);
          List<UserValue> ret= new ArrayList<UserValue>(i.size());
          ret.addAll(i);
          return ret;

        case TEXT:
        default:
          throw new IllegalStateException("Not implemented: " + info.getType());
      }
    }
    else
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
  }
  
  /**
   * Change legacy data
   * <ul><li>1: VO2Max</li><li>2: Max Heartrate</li><li>3: Footpod calibration factor</li><li>4: resting heart rate</li></ul>
   * @param idx
   * @param userid
   * @param value
   */
  private void changeData(int idx, long userid, double value)
  {
    long time= System.currentTimeMillis() / 1000;
    
    String qr= String.format("UPDATE UserDetails set data%d = :value, data%d_crtime = :time where id = :userid", idx, idx);
    Query<?> q= getSession().createQuery(qr);
    q.setParameter("userid", userid);
    q.setParameter("value", value);
    q.setParameter("time", time);
    q.executeUpdate();
  }
  
  /**
   * Change legacy weight
   * 
   * @param userid
   * @param weight
   */
  private void changeWeight(long userid, double weight)
  {
    Query<?> q= getSession().createQuery("UPDATE UserDetails set weight = :weight where id = :userid");
    q.setParameter("userid", userid);
    q.setParameter("weight", weight);
    q.executeUpdate();
  }
  
  /**
   * Change legacy weight
   * 
   * @param userid
   * @param weight
   */
  private void changeHeight(long userid, double height)
  {
    Query<?> q= getSession().createQuery("UPDATE UserDetails set height = :height where id = :userid");
    q.setParameter("userid", userid);
    q.setParameter("height", height);
    q.executeUpdate();
  }

  /**
   * Invalidate (i.e.) delete a value for a user
   * @param name name of the value to be invalidated 
   * @param userid
   * @param cal
   */
  public void invalidateValueForUser(String name, long userid, Calendar date)
  {
    UserValueInfo val= infos.findByName(name);
    if( val == null )
      throw new UnkownValueException(name);
    else
    {
      logger.debug("Changing value type:" + val.getType() + "/" + name);
      
      switch( val.getType() )
      {
        case DECIMAL:
          List<UserValueDec> olddvalues= decs.findByNameAndUserId(name, userid);
          
          logger.debug("#old values for " + name + " and user " + userid + ": " + olddvalues.size());

          if( olddvalues.size() > 0 )
          {
            UserValueDec old= olddvalues.get(0);

            logger.debug("#old values enddate: " + old.date_end);
            if( old.date_end == null )
            {
              Calendar end= Calendar.getInstance();
              end.setTime(date.getTime());
              end.add(Calendar.MILLISECOND, -1);;
              old.date_end= end;
              decs.saveAndFlush(old);
            }
          }
          break;
        
        case INTEGER:
          List<UserValueInt> oldivalues= ints.findByNameAndUserId(name, userid);

          logger.debug("#old values for " + name + " and user " + userid + ": " + oldivalues.size());

          if( oldivalues.size() > 0 )
          {
            UserValueInt old= oldivalues.get(0);

            logger.debug("#old values enddate: " + old.date_end);
            if( old.date_end == null )
            {
              Calendar end= Calendar.getInstance();
              end.setTime(date.getTime());
              end.add(Calendar.MILLISECOND, -1);;
              old.date_end= end;
              ints.saveAndFlush(old);
            }
          }
          break;

        case TEXT:
          // TODO: implement text values
        default:
          throw new IllegalArgumentException(val.getType().toString());
      }
    }
  }

  public boolean update(Long valueId, ValueUpdate[] changes)
  {
    Optional<UserValueInfo> ov= infos.findById(valueId);
    if( !ov.isPresent() )
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value");
    UserValueInfo value= ov.get();

    for(ValueUpdate change: changes)
    {
      Optional<? extends UserValue> ocv;
      UserValue cv= null;
      switch( value.getType() )
      {
        case DECIMAL:
          ocv= decs.findById(change.id);
          if( !ocv.isPresent() )
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value: " + change.id);
          cv= ocv.get();
          break;
        case INTEGER:
          ocv= ints.findById(change.id);
          if( !ocv.isPresent() )
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value: " + change.id);
          cv= ocv.get();
          break;
        case TEXT:
          break;
        default:
          break;
      }

      Calendar c= Calendar.getInstance();
      c.setTime(change.date_start);
      cv.date_start= c;
      if( change.date_end != null )
      {
        Calendar c2= Calendar.getInstance();
        c2.setTime(change.date_end);
        cv.date_end= c2;
      }
      else
        cv.date_end= null;

      switch( value.getType() )
      {
        case DECIMAL:
          ((UserValueDec) cv).internal= Double.parseDouble(change.value);
          decs.saveAndFlush((UserValueDec) cv);
          break;
        case INTEGER:
          ((UserValueInt) cv).internal= (long) Double.parseDouble(change.value);
          ints.saveAndFlush((UserValueInt) cv);
          break;
        case TEXT:
          break;
        default:
          break;
      }
    }

    return true;
  }

  public UserValue[] create(Long valueId, long userId, ValueCreation[] creates)
  {
    Optional<UserValueInfo> ov= infos.findById(valueId);
    if( !ov.isPresent() )
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value");
    UserValueInfo value= ov.get();

    UserValue[] ret= new UserValue[creates.length];
    int i= 0;

    for(ValueCreation create: creates)
    {
      UserValue cv= null;
      switch( value.getType() )
      {
        case DECIMAL:
          cv= new UserValueDec();
          ((UserValueDec) cv).internal= Double.parseDouble(create.value);
          break;
        case INTEGER:
          cv= new UserValueInt();
          ((UserValueInt) cv).internal= (long) Double.parseDouble(create.value);
          break;
        case TEXT:
          // TODO
          break;
        default:
          break;
      }

      cv.user_id= userId;
      cv.setValue(value);

      Calendar c= Calendar.getInstance();
      c.setTime(create.date_start);
      cv.date_start= c;
      if( create.date_end != null )
      {
        Calendar c2= Calendar.getInstance();
        c2.setTime(create.date_end);
        cv.date_end= c2;
      }

      switch( value.getType() )
      {
        case DECIMAL:
          ret[i]= decs.saveAndFlush((UserValueDec) cv);
          break;
        case INTEGER:
          ret[i]= ints.saveAndFlush((UserValueInt) cv);
          break;
        case TEXT:
          break;
        default:
          break;
      }
      i++;
    }

    return ret;
  }

  /**
   * Delete a value
   * 
   * @param valueId
   *          id of the value of which an instance should be delete
   * @param id
   *          id of the instance to be deleted
   * @return true if deleting was successful
   */
  public boolean delete(long valueId, long id)
  {
    Optional<UserValueInfo> ov= infos.findById(valueId);
    if( !ov.isPresent() )
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find value");
    UserValueInfo value= ov.get();

    // TODO: check privilege
    switch( value.getType() )
    {
      case DECIMAL:
        decs.deleteById(id);
        return true;
      case INTEGER:
        ints.deleteById(id);
        return true;
      case TEXT:
        // TODO
      default:
        return false;
    }
  }
}
