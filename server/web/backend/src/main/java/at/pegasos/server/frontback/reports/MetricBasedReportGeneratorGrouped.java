package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.controller.ReportController.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.services.*;
import org.springframework.lang.*;

import java.util.*;
import java.util.stream.*;

public class MetricBasedReportGeneratorGrouped extends MetricBasedReportGenerator {
  private HashMap<Long, Lap[]> laps;

  public static class Lap {
    public int number;
    /**
     * Duration of the Lap in ms
     */
    public long duration;
    public long start;
  }

  private final MarkerRepository markerRepository;

  final Map<String, List<Double>> valuesList = new HashMap<>();
  /**
   * metric-param combinations in this report
   */
  private final Set<String> metric_param = new HashSet<String>();
  /**
   * Dates for which data exists
   */
  private final Set<Long> dates = new HashSet<Long>();
  /**
   * users for which data exists
   */
  protected Set<Long> users_with_data = new HashSet<Long>();
  protected ReportUserDatesList report;

  private GroupBy grouping;

  private final LapFilters lapFilters;

  public MetricBasedReportGeneratorGrouped(RepositoryService service,
      String[] metric_names, @Nullable List<SessionTypeFilter> filter, @Nullable LapFilters lapFilters)
  {
    super(service.getMetricInfoRepository(), service.getMetricRepository(), service.getSessionInfoRepository(), metric_names, filter);
    this.markerRepository = service.getMarkerRepository();

    logger.debug("Start {}, end {}, lapFilters {}", start, end, lapFilters);
    this.lapFilters = lapFilters;
  }

  public Report generateReport()
  {
    fetchValues();
    fetchLaps();

    logger.debug("Laps: {}", laps);
    fetchMetrics();

    logger.debug("Dates: {}", dates);

    report = new ReportUserDatesList();

    report.dates = dates.stream().sorted().map(d -> {
      Calendar c = Calendar.getInstance();
      c.setTimeInMillis(d);
      return c;
    }).collect(Collectors.toList());
    report.users = new ArrayList<Long>(users_with_data);

    finishReport();

    logger.debug("finished report " + report);

    return report;
  }

  private void fetchMetrics()
  {
    logger.debug("Fetching metrics");
    // Get all the values we have for this report
    List<Metric> allMetrics = getMetrics();
    for(Metric m : allMetrics)
    {
      if( filter(m) )
      {
        logger.debug("Filtering metric {} from output", m);
        continue;
      }

      // when we do not group we do not have a date
      long date = ReportUtil.groupedDate(m.session.starttime, grouping, m);
      dates.add(date);
      users_with_data.add(m.session.userId);

      String key = getKey(m, date);

      // ensure that we have a list present for 'key'
      valuesList.computeIfAbsent(key, k -> new ArrayList<>());

      // Add current value to the values for this entry
      if( m.segmentnr != null )
      {
        valuesList.get(key).add((double) laps.get(m.sessionId)[m.segmentnr - 1].number);
        valuesList.get(key).add((double) laps.get(m.sessionId)[m.segmentnr - 1].start);
        valuesList.get(key).add((double) laps.get(m.sessionId)[m.segmentnr - 1].duration);
        valuesList.get(key).add(m.value);
      }
      else
      {
        valuesList.get(key).add(-1d);
        valuesList.get(key).add(-1d);
        valuesList.get(key).add(-1d);
        valuesList.get(key).add(m.value);
      }
    }
  }

  /**
   *
   * @param m Metric
   * @return true if metric should be excluded from the result
   */
  private boolean filter(Metric m)
  {
    if( lapFilters == null )
      return false;

    if( lapFilters.filters.size() > 1 )
    {
      if( lapFilters.conj == Conjunction.OR )
      {
        // If one of the filters matches (i.e. returns false) we know that we can use this metric -> return false
        for(LapFilter filter : lapFilters.filters)
        {
          if( !filter(filter, m) )
          {
            return false;
          }
        }
        return true;
      }
      else if( lapFilters.conj == Conjunction.AND )
      {
        // If one of the filters doesn't match (i.e. returns true) we know that we cannot use this metric -> return true
        for(LapFilter filter : lapFilters.filters)
        {
          if( filter(filter, m) )
          {
            logger.debug("Filter {} fired on {}", filter, m);
            return true;
          }
        }
        return false;
      }
      else
        throw new IllegalStateException("Unimplemented conjunction " + lapFilters.conj);
    }
    else
    {
      return filter(lapFilters.filters.get(0), m);
    }
  }

  /**
   * Check if a metric should be included
   * @param filter Filter to be applied
   * @param m metric
   * @return true if metric should be excluded from the result
   */
  private boolean filter(LapFilter filter, Metric m)
  {
    long value = referenceValue(filter, m);
    logger.debug("Filter {}, value {}, on {}", filter, value, m);

    switch( filter.comp )
    {
      case GT:
        return value <= filter.value;
      case GTE:
        return value < filter.value;
      case LT:
        return value >= filter.value;
      case LTE:
        return value > filter.value;
      case EQ:
        return value != filter.value;
      case NEQ:
        return value == filter.value;
    }
    throw new IllegalStateException("Unimplemented comparator type " + filter.comp);
  }

  private long referenceValue(LapFilter filter, Metric m)
  {
    if( m.segmentnr != null )
    {
      Lap lap = laps.get(m.sessionId)[m.segmentnr - 1];

      switch( filter.type )
      {
        case LapNr:
          return lap.number;
        case Duration:
          return lap.duration;
        case StartTime:
          return lap.start;
        default:
          throw new IllegalStateException("Unimplemented filter type " + filter.type);
      }
    }
    else
    {
      switch( filter.type )
      {
        case LapNr:
          return 0;
        case Duration:
          SessionInfo session = m.session;
          return session.endtime - session.starttime;
        case StartTime:
          return m.session.starttime;
        default:
          throw new IllegalStateException("Unimplemented filter type " + filter.type);
      }
    }
  }

  private void fetchLaps()
  {
    this.laps = new HashMap<Long, Lap[]>();
    data.sessions.forEach(sessionInfo -> {
      List<Marker> markers= markerRepository.findBySessionIdOrderByRecTimeAsc(sessionInfo.session_id);

      Lap[] sessionLaps = new Lap[markers.size()+1];

      long start = sessionInfo.starttime;
      int n= 1;
      // long duration;
      for(Marker m : markers)
      {
        Lap lap = new Lap();

        lap.duration = m.recTime - start;
        lap.number = n;
        lap.start = start;

        sessionLaps[n-1] = lap;

        start= m.recTime;
        n++;
      }
      // add Lap from last marker to end
      Lap lap = new Lap();
      lap.duration = sessionInfo.endtime - start;
      lap.number = n;
      lap.start = start;
      sessionLaps[n-1] = lap;

      laps.put(sessionInfo.session_id, sessionLaps);
    });
  }

  protected List<Metric> getMetrics()
  {
    return metrrep.findBySessionIdAndMetricIdOrdered(data.session_ids, data.metrics);
  }

  protected void finishReport()
  {
    report.series = new ArrayList<DataSeries<List<Double>>>(metric_param.size() * report.users.size());

    // Go over all metrics
    for(String mp : metric_param)
    {
      String[] mps = mp.split("-");
      long metricId = Long.parseLong(mps[0]);
      String param = mps[1];

      String metric = data.metric_map.get(metricId);

      // For each user
      for(Long userId : users_with_data)
      {
        // Create a series metric-user
        List<List<Double>> data = new ArrayList<List<Double>>(report.dates.size());
        for(Calendar d : report.dates)
        {
          data.add(valuesList.get(metricId + "-" + param + "-" + userId + "-" + d.getTimeInMillis()));
        }
        report.series.add(new DataSeries<List<Double>>(metric + "#" + param + "#" + userId, data));
      }
    }
  }

  public void setGroupBy(GroupBy grouping)
  {
    this.grouping = grouping;
  }

  /**
   * Get the key for the metric.
   *
   * @param m    Metric
   * @param date date
   * @return key
   */
  public String getKey(Metric m, long date)
  {
    String k = m.metricId + "-" + m.param;
    metric_param.add(k);
    return k + "-" + m.session.userId + "-" + date;
  }
}
