package at.pegasos.server.frontback.data;

import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.WhereJoinTable;

import lombok.ToString;

@ToString
@Entity
@Table(name="user")
public class User {
  /**
   * ID of the user
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public long user_id;
  
  /**
   * Firstname of the user
   */
  @Column(name="forename")
  public String firstname;
  
  /**
   * Lastname of the user
   */
  @Column(name="surname")
  public String lastname;

  public String email;

  public String language;

  @ManyToMany(fetch = FetchType.EAGER)
  @WhereJoinTable(clause="date_end is null")
  @JoinTable(
      name = "user_has_role",
      joinColumns = { @JoinColumn(name = "user_id", referencedColumnName= "id") },
      inverseJoinColumns = { @JoinColumn(name = "role_id") }
  )
  public Collection<Role> roles;

  private final long crtime = System.currentTimeMillis() / 1000;
}
