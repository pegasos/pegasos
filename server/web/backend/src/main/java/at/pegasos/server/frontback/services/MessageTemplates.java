package at.pegasos.server.frontback.services;

import at.pegasos.server.frontback.repositories.MessageTemplatesRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.Principal;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class MessageTemplates {

  @Entity
  @Table(name="message_templates")
  public static class MessageTemplate {

    @Id
    @GeneratedValue
    public long id;

    public String templatename;

    public String lang;

    public String template;
  }

  @Value("${default_language:en}")
  private String defaultLang;

  @Autowired
  MessageTemplatesRepository templates;

  /*@PostConstruct
  public void init()
  {

  }*/

  public String get(Principal p, String templateName, Map<String, String> valuesMap)
  {
    return get(templateName, defaultLang, valuesMap);
  }

  public String get(String templateName, String language, Map<String, String> valuesMap)
  {
    Optional<MessageTemplate> ot = templates.findByTemplatenameAndLang(templateName, language);
    if( ot.isPresent() )
    {
      StringSubstitutor sub = new StringSubstitutor(valuesMap);

      return sub.replace(ot.get().template);
    }
    else
    {
      ot = templates.findByTemplatenameAndLang(templateName, defaultLang);
      if( ot.isPresent() )
      {
        StringSubstitutor sub = new StringSubstitutor(valuesMap);

        return sub.replace(ot.get().template);
      }
      else
      {
        return null;
      }
    }
  }
}
