package at.pegasos.server.frontback.data;

import at.pegasos.server.frontback.data.MetricInfo;

public interface SessionMetric {
  
  public MetricInfo getMetric();
  
  public double getValue();
  
  public Integer getSegmentnr();
  
  public String getParam();
}
