package at.pegasos.server.frontback.data;

public class Chart {
  
  protected final Type type;
  
  public Chart(Type type)
  {
    this.type= type;
  }

  public Type getType()
  {
    return type;
  }
}
