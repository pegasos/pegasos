package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.dbdata.TeamMembership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamMembershipRepository extends JpaRepository<TeamMembership, Long> {

  /**
   * Find all active memeberships for a given user (i.e. all memberships which are not deleted)
   * @param userId user
   * @return List of memberships
   */
  @Query("FROM TeamMembership m WHERE m.userid = :userId AND deleted = false")
  List<TeamMembership> findUserActive(long userId);

  /**
   * Find all active memeberships for a given user (i.e. all memberships which are not deleted)
   * @param userId user
   * @param teamId team
   * @return List of memberships
   */
  @Query("FROM TeamMembership m WHERE m.userid = :userId AND m.teamid = :teamId AND deleted = false")
  List<TeamMembership> findActive(long userId, long teamId);

  /**
   * Find all active memeberships for a given user (i.e. all memberships which are not deleted)
   * @param userId user
   * @param teamId team
   * @param groupId group
   * @return List of memberships
   */
  @Query("FROM TeamMembership m WHERE m.userid = :userId AND m.teamid = :teamId AND m.groupid = :groupId AND deleted = false")
  List<TeamMembership> findActive(long userId, long teamId, Long groupId);
}
