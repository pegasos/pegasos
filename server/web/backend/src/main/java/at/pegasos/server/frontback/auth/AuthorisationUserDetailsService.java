package at.pegasos.server.frontback.auth;

import java.util.List;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthorisationUserDetailsService implements UserDetailsService {
  
  // private Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  private AuthorisationUserDetailsRepository repository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
  {
    // LOG.info("Current objects in DB: {}", service.countPersons());
    // logger.error("Getting user " + username);
    // return repository.findByEmail(username)
    List<AuthorisationUserDetails> users= repository.findByEmail(username);
    if( users.size() == 1 )
      return users.get(0);
    else
      throw new UsernameNotFoundException(username);
  }
  
}
