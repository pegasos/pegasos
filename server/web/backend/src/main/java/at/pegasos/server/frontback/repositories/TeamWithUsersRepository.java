package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TeamWithUsers;

@Repository
@Transactional(readOnly = true)
public interface TeamWithUsersRepository extends JpaRepository<TeamWithUsers, Long> {
  
  /*@Query(value = "select t from TeamUsers t where :userid in managers.user.id")
  List<TeamUsers> getForManager(@Param("userid") long userid);*/
  
  List<TeamWithUsers> findByTeamid(long teamid);
  
  // @Query(value = "select t from TeamWithUsers t join t.captains as manager with manager.email = :email")
  @Query(value = "select t from TeamWithUsers t join t.captains as manager where manager.email = :email")
  List<TeamWithUsers> getForManager(@Param("email") String email);
}
