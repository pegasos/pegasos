package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

@Repository
@Transactional(readOnly = true)
public interface MetricRepository extends JpaRepository<Metric, Long> {

  List<Metric> findBySessionIdInAndMetricIdIn(Collection<Long> session_ids, List<Long> metrics);

  @Query("select m from #{#entityName} m where m.sessionId in :sessions and m.metricId in :metrics ORDER BY session.starttime ASC, m.segmentnr ASC")
  List<Metric> findBySessionIdAndMetricIdOrdered(@Param("sessions") Collection<Long> session_ids, @Param("metrics") List<Long> metrics);

  List<SessionMetric> findBySessionId(long session_id);

  List<Metric> findBySessionIdAndMetricIdInOrderByMetricIdAsc(Long sessionId, List<Long> metrics);

  List<Metric> findBySessionIdInAndMetricIdInAndParamIsNullAndSegmentnrIsNull(Collection<Long> session_ids, List<Long> metrics);
}
