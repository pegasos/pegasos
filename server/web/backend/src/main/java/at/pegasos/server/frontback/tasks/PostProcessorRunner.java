package at.pegasos.server.frontback.tasks;

import at.pegasos.server.frontback.util.*;
import lombok.extern.slf4j.*;

import java.io.*;
import java.util.*;

@Slf4j
public class PostProcessorRunner implements Task {

  private static class Job {

    public long sessionId;
    public String action;
  }
  private final List<Job> jobList = new ArrayList<>();
  private int completed = 0;
  private boolean isCompleted = false;
  private final List<String> messages = new ArrayList<>();

  public void add(long sessionId, String action)
  {
    Job j = new Job();
    j.sessionId= sessionId;
    j.action = action;
    jobList.add(j);
  }

  public void add(long sessionId)
  {
    Job j = new Job();
    j.sessionId= sessionId;
    jobList.add(j);
  }

  public void run()
  {
    (new Thread(() -> {
      for(Job j : jobList)
      {
        try
        {
          if( j.action == null )
          {
            AiHelper.callBlocking(log, "at.univie.mma.ai.postprocessing.PostProcessor", j.sessionId);
          }
          else
          {
            AiHelper.callBlocking(log, j.action, j.sessionId);
          }
          messages.add("Processing " + j.sessionId + " completed");
        }
        catch( IOException e )
        {
          log.error("Error running", e);
          messages.add("Processing " + j.sessionId + " failed");
        }
        completed++;
      }
      isCompleted = true;
    })).start();
  }

  @Override public float completion()
  {
    return ((short)(completed / (float) jobList.size() * 10000)) / 10000f;
  }

  @Override public Collection<String> messages()
  {
    List<String> ret = new ArrayList<>(messages);
    messages.clear();
    return ret;
  }

  @Override public boolean completed()
  {
    return isCompleted;
  }
}
