package at.pegasos.server.frontback.data;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="metric")
@ToString
public class MetricInfo {
  @GeneratedValue
  @Id
  public long ID;
  
  public String name;
  
  public String unit;
  
  public long getID()
  {
    return ID;
  }
  
  public String getName()
  {
    return name;
  }
  
  public String getUnit()
  {
    return unit;
  }
}
