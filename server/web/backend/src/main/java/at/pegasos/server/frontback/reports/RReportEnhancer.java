package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.data.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.rosuda.REngine.*;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.*;
import java.util.*;

@Slf4j
public class RReportEnhancer implements ReportEnhancer {

  public REngine engine;

  private REXP env;

  public String[] code;

  public ExtractionDefinition[] extractions = new ExtractionDefinition[0];

  /*public RReportEnhancer(REngine engine)
  {
    this.engine = engine;
  }*/

  void createEngine() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
  {
    this.engine = REngine.engineForClass("org.rosuda.REngine.JRI.JRIEngine", new String[] {"--no-save"}, new REngineStdOutput(), false);
  }

  protected void loadReport(Report report) throws REngineException, REXPMismatchException
  {
    if( report instanceof ReportUserDatesSingle )
      loadReportDates((ReportUserDatesSingle) report);
    else if( report instanceof ReportUserDatesList )
      throw new IllegalArgumentException(); // TODO: implement this
    else
      throw new IllegalArgumentException();
  }

  protected void loadReportDates(ReportUserDatesSingle report) throws REngineException, REXPMismatchException
  {
    RList rr = new RList();
    // rr.put("users", new REXPDouble(new double [] {1L, 2L}));
    double[] users = new double[report.users.size()];
    int i = 0;
    for(Long v : report.users)
    {
      users[i++] = v.doubleValue();
    }
    rr.put("users", new REXPDouble(users));
    RList series = new RList();
    String[] series_names = new String[report.series.size()];
    double[] series_data = new double[report.series.size()];
    i = 0;
    for(DataSeries<Double> x : report.series)
    {
      series_names[i] = x.getName();
      series_data[i] = x.getData().get(0);
      i++;
    }
    series.put("names", new REXPString(series_names));
    series.put("data", new REXPDouble(series_data));
    rr.put("series", new REXPList(series));

    engine.assign("report", new REXPList(rr), env);
  }

  public void enhance(Report report)
  {
    try
    {
      this.env = engine.newEnvironment(null, false);
      loadReport(report);

      for(String x : code)
      {
        log.debug("Running {}", x);
        engine.parseAndEval(x, env, false);
      }

      for(ExtractionDefinition extract : extractions)
      {
        extract(extract, report);
      }
    }
    catch( REngineException | REXPMismatchException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static abstract class ExtractionDefinition {
    /**
     * Name to be used in the return report
     */
    public final String exportName;

    public ExtractionDefinition(String exString)
    {
      this.exportName = exString;
    }
  }

  public static class UserCoeffExtractionDefinition extends ExtractionDefinition {
    String modelName;
    String[] names;
    String listName;

    public UserCoeffExtractionDefinition(String exportName, String listName, String modelName, String[] names)
    {
      super(exportName);
      this.listName = listName;
      this.modelName = modelName;
      this.names = names;
    }
  }

  public static class UserModelExtractionDefinition extends ExtractionDefinition {
    String modelName;
    String[] names;
    String listName;

    public UserModelExtractionDefinition(String exportName, String listName, String modelName, String[] names)
    {
      super(exportName);
      this.listName = listName;
      this.modelName = modelName;
      this.names = names;
    }
  }

  protected void extract(ExtractionDefinition d, Report report) throws REngineException, REXPMismatchException
  {
    if( d instanceof UserModelExtractionDefinition )
    {
      for(Long user : report.users)
      {
        UserModelExtractionDefinition m = (UserModelExtractionDefinition) d;
        log.debug("Extracting summary {}", m.modelName);
        REXP mod = engine.parseAndEval("coef(" + m.listName + "[[\"" + m.modelName + "." + user + "\"]])", env, false);
        if( !mod.isNumeric() )
        {
          report.setExtra(d.exportName + "#" + user, "missing");
          continue;
        }
        mod = engine.parseAndEval("summary(" + m.listName + "[[\"" + m.modelName + "." + user + "\"]])", env, false);
        if( mod.isVector() )
        {
          REXPReference rnames = (REXPReference) engine
              .parseAndEval("row.names(summary(" + m.listName + "[[\"" + m.modelName + "." + user + "\"]])$coefficients)", env, false);
          String[] names = rnames.asStrings();

          RList list = mod.asList();
          REXPDouble coeffs = (REXPDouble) list.get("coefficients");

          double[][] values = coeffs.asDoubleMatrix();

          Map<String, Double> modelMap = new HashMap<String, Double>();
          for(int i = 0; i < m.names.length; i++)
          {
            // find row in the output
            int row = -1;
            for(int r = 0; r < names.length; r++)
            {
              if( names[r].equals(m.names[i]) )
              {
                row = r;
                break;
              }
            }
            if( row != -1 )
            {
              modelMap.put(m.names[i], values[row][0]);
              modelMap.put(m.names[i] + "_see", values[row][1]);
            }
            else
            {
              log.debug("Missing model parameter {} for person {}", m.names[i], user);
            }
          }
          report.setExtra(d.exportName + "#" + user, modelMap);
        }
        else
        {
          report.setExtra(d.exportName + "#" + user, "missing");
        }
      }
    }
    else if( d instanceof UserCoeffExtractionDefinition )
    {
      for(Long user : report.users)
      {
        UserCoeffExtractionDefinition m = (UserCoeffExtractionDefinition) d;
        System.out.println("Extracting coef " + m.modelName);
        REXP mod = engine.parseAndEval("coef(" + m.listName + "[[\"" + m.modelName + "." + user + "\"]])", env, false);
        if( mod.isNumeric() )
        {
          double[] values = mod.asDoubles();
          Map<String, Double> modelMap = new HashMap<String, Double>();
          for(int i = 0; i < m.names.length; i++)
          {
            modelMap.put(m.names[i], values[i]);
          }
          report.setExtra(d.exportName + "#" + user, modelMap);
        }
        else
        {
          report.setExtra(d.exportName + "#" + user, "missing");
        }
      }
    }
  }

  public static void main(String[] args)
      throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, REXPMismatchException,
      REngineException, IOException
  {

    ObjectMapper objectMapper = new ObjectMapper();
    ReportUserDatesSingle report = objectMapper.readValue(new File("/home/martin/projects/CyclingData/mmp.json"), ReportUserDatesSingle.class);

    RReportEnhancer e = new RReportEnhancer();

    // "/backend/src/main/resources/"
    
    /*String[] h= new String[] {
        "h<- data.frame(sapply(sapply(report$series$name,function(x) strsplit(x, '#')), function(y) as.numeric(y[[2]])))",
        "colnames(h)<- c(\"a\")",
        "x<- h$a",
        "##ExPD model fit\n",
        "form <- as.formula( y ~ W1/x * (1 - exp( -x / (W1 / (Pmax-CP)) )) + CP - A*(log(x/1800))*(x>1800) )",
        "\"mod.67\" <- nls(form, data=data.frame(y = report$series$data, x = x), start=list(W1=10000, Pmax=1000, CP=200, A=30))",
        "print(mod.67)"
    };
    e.code= h;*/
    // e.code= Files.readAllLines(Paths.get("/home/martin/projects/CyclingData/omnidata.R")).toArray(new String[0]);
    e.code = new String[] {String.join("\n", Files.readAllLines(Paths.get("/home/martin/projects/CyclingData/omnidata.R")))};

    e.extractions = new ExtractionDefinition[] {
        new UserCoeffExtractionDefinition("OmPD", "models", "OmPD", new String[] {"W1", "Pmax", "CP", "A"}),
        new UserCoeffExtractionDefinition("Om3CP", "models", "Om3CP", new String[] {"W1", "Pmax", "CP", "A"}),
        new UserModelExtractionDefinition("OmExp", "models", "OmExp", new String[] {"W1", "Pmax", "CP", "A"})};

    e.createEngine();
    e.enhance(report);
    e.engine.close();
    // mod <- nls(form, data=df, start=list(W1=startW, Pmax=startPmax, CP=startCP, A=startA))
    // mod
    System.out.println(report.extra.toString());

    //objectMapper.writeValue(System.out, report);
  }
}
