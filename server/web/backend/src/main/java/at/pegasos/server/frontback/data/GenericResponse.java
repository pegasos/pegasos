package at.pegasos.server.frontback.data;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GenericResponse extends Response {

  public static final GenericResponse OkResponse= new GenericResponse(OK);
  public static final GenericResponse FailResponse= new GenericResponse(FAIL);

  public GenericResponse(final String message)
  {
    super(message);
  }
  
  public GenericResponse(final String message, final String error)
  {
    super(message, error);
  }
}
