package at.pegasos.server.frontback.dbdata;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="teams_memberships")
@ToString
public class TeamMembership {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public long userid;

  public long teamid;

  public Long groupid = null;

  private final long crtime = System.currentTimeMillis() / 1000;

  public boolean deleted;
}
