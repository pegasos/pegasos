package at.pegasos.server.frontback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.pegasos.server.frontback.data.notification.NotificationType;

@Repository
public interface NotificationTypesRepository extends JpaRepository<NotificationType, Long> {

  NotificationType findByType(String string);

}
