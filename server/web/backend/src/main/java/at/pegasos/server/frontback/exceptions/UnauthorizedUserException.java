package at.pegasos.server.frontback.exceptions;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class UnauthorizedUserException extends RuntimeException {
  public UnauthorizedUserException(String msg)
  {
    super(msg);
  }
}
