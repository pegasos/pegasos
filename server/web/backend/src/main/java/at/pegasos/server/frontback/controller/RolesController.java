package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.exceptions.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.repositories.*;
import io.swagger.v3.oas.annotations.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;
import java.util.stream.*;

@RestController
@RequestMapping("/v1/roles")
public class RolesController {
  @Autowired
  private PrivilegeManager priv;

  @Autowired
  UserRepository userrepo;

  @Autowired
  UserRolesRepository userroles;

  @Autowired
  RoleRepository roles;

  private Logger logger= LoggerFactory.getLogger(this.getClass());

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/my")
  public Collection<String> myRoles(Principal p)
  {
    User me= userrepo.findByEmail(p.getName()).get(0);

    return me.roles.stream().map(u -> u.name).collect(Collectors.toList());
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "")
  public Collection<Role> all(Principal p)
  {
    return roles.findAll();
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/user/{userid}")
  public Collection<String> userRoles(Principal p, @PathVariable("userid") long userid)
  {
    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User " + p + " is not admin (add)");
      throw new UnauthorizedUserException(p.toString());
    }
    else
    {
      Optional<User> user= userrepo.findById(userid);
      if( user.isPresent() )
      {
        return user.get().roles.stream().map(u -> u.name).collect(Collectors.toList());
      }
      else
      {
        return Collections.emptyList();
      }
    }
  }

  @Operation(
      summary = "Add a role for a user",
      description = "Add a role for a user. Requires 'ADMIN' privilege of the logged in user. Returns ok if adding was successfull, fail otherwise (user does not exist, role does not exist, ...)"
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/user/{userid}/{roleid}")
  public GenericResponse addUserRole(Principal p, @PathVariable("userid") long userid, @PathVariable("roleid") long roleid)
  {
    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User " + p + " is not admin (add)");
      throw new UnauthorizedUserException(p.toString());
    }
    else
    {
      Optional<User> user= userrepo.findById(userid);
      if( user.isPresent() )
      {
        User u= user.get();
        // TODO: check if user does not have role already
        Optional<Role> r= roles.findById(roleid);
        if( r.isPresent() )
        {
          UserHasRole entry= UserHasRole.newRole(u, r.get());

          userroles.saveAndFlush(entry);

          logger.debug(userrepo.findById(userid).get().roles.toString());

          return GenericResponse.OkResponse;
        }
      }
      return GenericResponse.FailResponse;
    }
  }

  @Operation(
      summary = "Delete a role for a user",
      description = "Delete a role for a user. Requires 'ADMIN' privilege of the logged in user"
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.DELETE, value = "/user/{userid}/{roleid}")
  public GenericResponse deleteUserRole(Principal p, @PathVariable("userid") long userid, @PathVariable("roleid") long roleid)
  {
    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User " + p + " is not admin (add)");
      throw new UnauthorizedUserException(p.toString());
    }
    else
    {
      Optional<User> user= userrepo.findById(userid);
      // TODO: check if user does have role
      Optional<Role> r= roles.findById(roleid);

      if( user.isPresent() )
      {
        User u= user.get();
        UserHasRole activeRole= userroles.findActiveRole(u, r.get());
        activeRole.end();

        userroles.saveAndFlush(activeRole);

        logger.debug(userrepo.findById(userid).get().roles.toString());

        return GenericResponse.OkResponse;
      }
      return GenericResponse.FailResponse;
    }
  }
}
