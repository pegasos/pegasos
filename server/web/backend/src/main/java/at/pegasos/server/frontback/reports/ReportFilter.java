package at.pegasos.server.frontback.reports;

import lombok.*;

import java.util.*;

@ToString
public class ReportFilter {
  public List<SessionTypeFilter> sessionFilter;
  public LapFilters lapQuery;
  public List<SessionDataFilter> dataQuery;
}
