package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.authorization.*;
import org.springframework.security.oauth2.server.resource.authentication.*;
import org.springframework.security.web.authentication.logout.*;
import org.springframework.security.web.savedrequest.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.*;

import javax.servlet.http.*;
import java.io.*;
import java.security.*;

@Controller public class LoginPageController {
  private static final Logger log = LoggerFactory.getLogger(LoginPageController.class);
  @Autowired public OAuth2AuthorizationService authorizationService;
  @Value("${frontend.url:" + Config.FRONTEND_URL + "}") private String frontendUrl;

  @GetMapping("/login")
  public ModelAndView login(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView theModel = new ModelAndView("login");

    theModel.addObject("frontendUrl", frontendUrl);

    SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
    if( savedRequest != null )
    {
      String requestedUrl = savedRequest.getRedirectUrl();
      log.debug(requestedUrl);
      if( requestedUrl.contains("&show-logintype") )
        theModel.addObject("showLogintype", true);
      if( requestedUrl.contains("&show-stayloggedin") )
        theModel.addObject("showStayLoggedIn", true);
    }

    return theModel;
  }

  @GetMapping("/oauth2/logout")
  public void logout(Principal principal, HttpServletRequest request, HttpServletResponse response)
  {
    log.debug("{} {} {}", principal, request, response);

    /*OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) principal;
    OAuth2AccessToken accessToken = authorizationServerTokenServices.getAccessToken(oAuth2Authentication);
    consumerTokenServices.revokeToken(accessToken.getValue());*/
    JwtAuthenticationToken token = (JwtAuthenticationToken) principal;
    Jwt jwt = token.getToken();
    log.debug("Token: {}, id: {}, subject: {}, token: {}", token, jwt.getId(), jwt.getSubject(), jwt.getTokenValue());
    // OAuth2Authorization authorization = authorizationService.findByToken(token.getName(), );
    // authorizationService.remove(authorization);
    OAuth2Authorization authorization = authorizationService.findByToken(jwt.getTokenValue(), null);
    log.debug("Authorization: {}", authorization);
    authorizationService.remove(authorization);

    log.debug("Contextpath '{}'", request.getContextPath());
    String redirectUrl = /* getLocalContextPathUrl(request)+ */"/logout";
    log.debug("Redirect URL: {}", redirectUrl);

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if( auth != null )
    {
      log.debug("Auth: {}", auth);
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }
    SecurityContextHolder.getContext().setAuthentication(null);

    // response.sendRedirect(redirectUrl);
  }
}