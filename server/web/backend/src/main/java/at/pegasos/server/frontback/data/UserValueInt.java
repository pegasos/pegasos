package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="user_value_int")
public class UserValueInt extends UserValue {
  
  @Override
  public String toString()
  {
    return "UserValueInt [internal=" + internal + ", id=" + id + ", value=" + getValue() + ", user_id=" + user_id + ", date_start=" + date_start
        + ", date_end=" + date_end + "]";
  }

  @Column(name="value")
  public long internal;
  
  public double getDoubleValue()
  {
    return internal;
  }
}
