package at.pegasos.server.frontback.charts;

import at.pegasos.server.frontback.data.*;

import java.util.*;

/**
 * Internal type representing information about an available chart. This type is not exposed. Any
 * relevant information contained in this type should also be present in the frontend and accessible
 * via the unique `name`.
 */
public abstract class DBChart extends AvailableChart {

  public DBChart(String name, Type type)
  {
    super(name, type);
  }

  public abstract String getDataQuery();
  
  /**
   * Convert query result into 'chart object' i.e. data of the chart which is sent
   * 
   * @param rows
   *          query result
   * @return chart
   */
  public abstract Chart newResult(List<Map<String, Object>> rows);
}
