package at.pegasos.server.frontback.dbdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marker")
public class Marker {
  @Id
  public long marker_id;
  
  @Column(name="session_id")
  public long sessionId;
  
  @Column(name="rec_time")
  public long recTime;
}
