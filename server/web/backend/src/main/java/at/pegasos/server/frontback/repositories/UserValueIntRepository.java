package at.pegasos.server.frontback.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.UserValueInt;

@Repository
@Transactional(readOnly = true)
public interface UserValueIntRepository extends JpaRepository<UserValueInt, Long>  {

  /**
   * Get all entries of a user for a value newest first i.e. in descending order based on the start
   * date
   * 
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @return
   */
  @Query("FROM UserValueInt v where v.value.name = :name AND user_id = :userid ORDER BY date_start DESC")
  List<UserValueInt> findByNameAndUserId(String name, long userid);
  
  /**
   * Get all entries of a user for a value newest first i.e. in ascending order based on the start
   * date
   * 
   * @param name
   *          name of the value
   * @param userid
   *          id of the user
   * @return
   */
  @Query("FROM UserValueInt v where v.value_id = :valueId AND user_id = :userId ORDER BY date_start ASC")
  List<UserValueInt> findByValueIdAndUserId(long valueId, long userId);
}
