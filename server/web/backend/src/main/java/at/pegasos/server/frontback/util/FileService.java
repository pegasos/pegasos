package at.pegasos.server.frontback.util;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.util.*;
import org.springframework.web.multipart.*;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.zip.*;

import lombok.extern.slf4j.*;

@Service
@Slf4j
public class FileService {

  @Value("${app.upload.dir:${user.home}}")
  public String uploadDir;

  private final Map<Long, Path> uploads = new HashMap<>();

  private final AtomicLong nextId = new AtomicLong(Long.MAX_VALUE);

  public static void unzip(String fileZip, File destDir) throws IOException
  {
    final byte[] buffer = new byte[1024];
    final ZipInputStream zis = new ZipInputStream(Files.newInputStream(Paths.get(fileZip)));
    ZipEntry zipEntry = zis.getNextEntry();
    while( zipEntry != null )
    {
      final File newFile = newFile(destDir, zipEntry);
      if( zipEntry.isDirectory() )
      {
        if( !newFile.isDirectory() && !newFile.mkdirs() )
        {
          throw new IOException("Failed to create directory " + newFile);
        }
      }
      else
      {
        File parent = newFile.getParentFile();
        if( !parent.isDirectory() && !parent.mkdirs() )
        {
          throw new IOException("Failed to create directory " + parent);
        }

        final FileOutputStream fos = new FileOutputStream(newFile);
        int len;
        while( (len = zis.read(buffer)) > 0 )
        {
          fos.write(buffer, 0, len);
        }
        fos.close();
        log.debug("Extracted {} to {}", zipEntry.getName(), newFile);
      }
      zipEntry = zis.getNextEntry();
    }
    zis.closeEntry();
    zis.close();
  }

  /**
   * @see https://snyk.io/research/zip-slip-vulnerability
   */
  public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException
  {
    File destFile = new File(destinationDir, zipEntry.getName());

    String destDirPath = destinationDir.getCanonicalPath();
    String destFilePath = destFile.getCanonicalPath();

    if( !destFilePath.startsWith(destDirPath + File.separator) )
    {
      throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
    }

    return destFile;
  }

  private Path createTempDir()
  {
    try
    {
      return Files.createTempDirectory(Paths.get(uploadDir), "upload");
    }
    catch( IOException e )
    {
      e.printStackTrace();
      return null;
    }
  }

  public boolean removeTempDir(Path dir)
  {
    try
    {
      Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        {
          try
          {
            Files.delete(file);
          }
          catch( IOException e )
          {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir,
                                                 @SuppressWarnings("unused") BasicFileAttributes attrs)
        {
          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc)
        {
          // fix up modification time of directory when done
          if( exc == null )
          {
            try
            {
              Files.delete(dir);
            }
            catch( IOException x )
            {
              log.warn("Unable to delete {}", dir.toString());
            }
          }
          return FileVisitResult.CONTINUE;
        }
      });
    }
    catch( IOException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public void upload(MultipartFile file, Path dest, boolean unzip)
  {
    try
    {
      Path copyLocation = dest.resolve(StringUtils.cleanPath(file.getOriginalFilename()));
      Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
      log.debug("Copied file to {}", copyLocation);
      log.debug("Unzip? unzip: {} '{}' {}", unzip, copyLocation.toString(), copyLocation.toString().endsWith(".zip"));
      if( unzip && copyLocation.toString().endsWith(".zip") )
      {
        unzip(copyLocation.toString(), dest.toFile());
      }
    }
    catch( Exception e )
    {
      log.error("Something failed while processing {}", file);
      e.printStackTrace();
      throw new RuntimeException("Could not store file " + file.getOriginalFilename() + ". Please try again!");
    }
  }

  /**
   * Upload files to a temporary directory
   *
   * @param files files to upload
   * @param unzip if true all zip files will be extracted
   * @return temporary folder with all files
   */
  public Path uploadFiles(MultipartFile[] files, boolean unzip)
  {
    Path p = createTempDir();

    Arrays.stream(files)
            .forEach(file -> upload(file, p, unzip));

    return p;
  }

  public Path uploadFile(Long importid, MultipartFile file, boolean unzip)
  {
    Path dir = uploads.get(importid);
    if( dir != null )
    {
      upload(file, dir, unzip);
      return dir;
    }
    else
      return null;
  }

  /**
   * Create a new upload
   * @return ID for the upload
   */
  public Long createUpload()
  {
    long id = nextId.updateAndGet(i -> i > 0 ? i - 1 : Long.MAX_VALUE);;
    Path p = createTempDir();

    uploads.put(id, p);

    return id;
  }

  public boolean removeUpload(long id)
  {
    Path p = uploads.get(id);
    if( p != null )
    {
      if( removeTempDir(p) )
      {
        uploads.remove(id);
        return true;
      }
      else
        return false;
    }
    else
    {
      log.error("Tried to remove upload for id {}. which did not exist", id);
      return false;
    }
  }

  public Path getUploadDir(Long importId)
  {
    return uploads.get(importId);
  }

  /**
   *
   * @param url
   * @param accessToken
   * @throws IOException when url is malformed or the stream cannot be opened
   */
  public void downloadFile(String url, String outFile, String accessToken) throws IOException
  {
    // first check if we can write to the directory
    Path p = Paths.get(outFile);
    if( !Files.exists(p.getParent()) )
    {
      log.info("Creating directory {}", p.getParent());
      if( !p.getParent().toFile().mkdirs() )
      {
        log.error("Did not create parent directory {} for {}", p.getParent(), p);
      }
    }

    URLConnection c = new URL(url).openConnection();
    c.setRequestProperty("Authorization", "Bearer: " + accessToken);
    ReadableByteChannel readableByteChannel = Channels.newChannel(c.getInputStream());
    FileOutputStream fileOutputStream = new FileOutputStream(outFile);
    FileChannel fileChannel = fileOutputStream.getChannel();
    fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
    fileChannel.close();
  }
}
