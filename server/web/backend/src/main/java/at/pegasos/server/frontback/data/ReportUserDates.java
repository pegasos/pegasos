package at.pegasos.server.frontback.data;

import java.util.*;

/**
 * Report containing values for users on specific dates.
 *
 */
public class ReportUserDates extends Report {

  /**
   * Dates on which points for the data series are present
   */
  public List<Calendar> dates;

  /**
   * Construct a completely empty report
   */
  public ReportUserDates()
  {
    dates = Collections.emptyList();
    users = Collections.emptyList();
  }
}
