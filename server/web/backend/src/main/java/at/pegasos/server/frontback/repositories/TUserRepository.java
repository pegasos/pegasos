package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.data.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

@Repository
@Transactional(readOnly = true)
public interface TUserRepository extends JpaRepository<TUser, Long> {
  Optional<TUser> findByEmail(String name);

  // List<TUser> findById(long id);
}
