package at.pegasos.server.frontback.util;

import at.pegasos.server.frontback.reports.*;

import java.util.*;

public class SessionFilter {
  public static String filterToQuery(Collection<SessionTypeFilter> filters)
  {
    boolean or_needed = false;
    StringBuilder query = new StringBuilder();

    query.append("(");
    for(SessionTypeFilter a : filters)
    {
      if( or_needed )
        query.append("OR");
      or_needed = true;
      query.append("(");
      switch( a.len )
      {
        case 1:
          query.append("activity = ").append(a.param0);
          break;
        case 2:
          query.append("activity = ").append(a.param0).append(" AND param1 = ").append(a.param1);
          break;
        case 3:
          query.append("activity = ").append(a.param0).append(" AND param1 = ").append(a.param1).append(" AND param2 = ").append(a.param2);
          break;
      }
      query.append(")");
    }
    query.append(")");

    return query.toString();
  }
}
