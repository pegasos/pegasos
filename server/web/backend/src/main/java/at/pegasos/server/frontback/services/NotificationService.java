package at.pegasos.server.frontback.services;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.notification.*;
import at.pegasos.server.frontback.repositories.*;
import lombok.extern.slf4j.Slf4j;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class NotificationService {

  public enum NotificationCheckResult {
    OK,
    /**
     * Notification does not exist
     */
    NOT_FOUND,
    /**
     * General error e.g. someone trying to perform an action of a notification not addressed to him/her
     */
    ERROR
  }

  @Autowired NotificationsRepository notifications;
  @Autowired NotificationTypesRepository types;

  @Autowired UserRepository users;

  @Autowired MessageTemplates templates;

  @Autowired TeamRepository teams;

  private NotificationType team_invite;
  private NotificationType team_inviteAccepted;
  private NotificationType team_inviteDeclined;

  @PostConstruct
  public void init()
  {
    team_invite = types.findByType("team_invite");
    team_inviteAccepted = types.findByType("team_invite_accepted");
    team_inviteDeclined = types.findByType("team_invite_declined");
  }

  public void setNotificationRead(long id)
  {
    Optional<Notification> on = notifications.findById(id);
    if( on.isPresent() )
    {
      Notification n = on.get();
      n.deleted = true;
      notifications.save(n);
    }
  }

  public void notifyInvitedToTeam(String language, TeamInvite invite)
  {
    Notification notification = new Notification();
    notification.type = team_invite;
    notification.user = users.findById(invite.invitedPerson).get();
    notification.crtime = System.currentTimeMillis() / 1000;
    notification.data = String.valueOf(invite.id);

    User inviter = users.findById(invite.userid).get();
    Map<String, String> replace = new HashMap<>(2);
    replace.put("person", inviter.firstname + " " + inviter.lastname);
    replace.put("teamname", teams.findById(invite.teamid).get().name);
    notification.msg = templates.get("team_invite", language, replace);

    notifications.save(notification);
  }

  public void notifyTeamInviteAccepted(String language, TeamInvite invite)
  {
    Notification notification = new Notification();
    notification.type = team_inviteAccepted;
    notification.user = users.findById(invite.userid).get();

    User invitedPerson = users.findById(invite.invitedPerson).get();
    Map<String, String> replace = new HashMap<>(2);
    replace.put("person", invitedPerson.firstname + " " + invitedPerson.lastname);
    replace.put("teamname", teams.findById(invite.teamid).get().name);
    notification.msg = templates.get("team_invite_accepted", language, replace);

    notifications.save(notification);
  }

  public void notifyTeamInviteDeclined(String language, TeamInvite invite)
  {
    Notification notification = new Notification();
    notification.type = team_inviteDeclined;
    notification.user = users.findById(invite.userid).get();

    Map<String, String> replace = new HashMap<>(2);
    replace.put("person", notification.user.firstname + " " + notification.user.lastname);
    replace.put("teamname", teams.findById(invite.teamid).get().name);
    notification.msg = templates.get("team_invite_declined", language, replace);

    notifications.save(notification);
  }

  /**
   * Check if a user is allowed to use a notification
   *
   * @param p user performing an action on the notification
   * @param notificationId ID of the notification
   * @return OK if notification is for the requesting user, NOT_FOUND if the notification does not exist, ERROR if the notification is not for the user
   */
  public NotificationCheckResult check(@NotNull  Principal p, long notificationId)
  {
    User user = users.findByEmail(p.getName()).get(0);
    Optional<Notification> onotification = notifications.findById(notificationId);
    if( onotification.isPresent() )
    {
      if( onotification.get().user.user_id == user.user_id )
      {
        return NotificationCheckResult.OK;
      }
      else
      {
        return NotificationCheckResult.ERROR;
      }
    }
    else
    {
      return NotificationCheckResult.NOT_FOUND;
    }
  }

  /**
   * Get data of a notification as long
   *
   * @param id notification
   * @return data
   */
  public Long getDataAsLong(long id)
  {
    return Long.parseLong(notifications.findById(id).get().data);
  }
}
