package at.pegasos.server.frontback.data;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CREATED)
public class CreatedResponse extends Response {

  public String id= null;

  public CreatedResponse(String id)
  {
    super("Created");
    this.id= id;
  }

  public CreatedResponse()
  {
    super("Created");
  }  
}
