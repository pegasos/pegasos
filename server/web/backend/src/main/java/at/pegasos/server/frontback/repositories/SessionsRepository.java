package at.pegasos.server.frontback.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.dbdata.Session;

@Repository
@Transactional(readOnly = true)
public interface SessionsRepository extends JpaRepository<Session, Long> {
  
  // List<AuthorisationUserDetails> findByEmail(String email);
  
  List<Session> findByUserIdAndStarttimeAfterOrderByStarttimeDesc(long userid, long starttime);
  
  List<Session> findByStarttimeAfter(long starttime);
  
  List<Session> findByUserIdIn(Collection<Integer> userids);
}
