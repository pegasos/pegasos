package at.pegasos.server.frontback.data;

import java.util.*;

/**
 * Report containing values for users on specific dates.
 *
 */
public class ReportUserDatesSingle extends ReportUserDates {

  /**
   * Dates on which points for the data series are present
   */
  public List<Calendar> dates;

  /**
   * Data for users on the specific dates. For each user one data series exists. Each data series
   * has an entry (null or value) for each date
   */
  public List<DataSeries<Double>> series;

  /**
   * Construct a completely empty report
   */
  public ReportUserDatesSingle()
  {
    dates= Collections.emptyList();
    users= Collections.emptyList();
    series= Collections.emptyList();
  }
}
