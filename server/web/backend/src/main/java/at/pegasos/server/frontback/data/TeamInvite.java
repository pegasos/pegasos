package at.pegasos.server.frontback.data;

import javax.persistence.*;

@Entity
@Table(name="teams_invites")
public class TeamInvite {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  // `userid` int(11) DEFAULT NULL,
  public long userid;

  // `teamid` int(11) DEFAULT NULL,
  public long teamid;

  @Column(name = "invited_userid")
  public long invitedPerson;

  public boolean accepted;

  public boolean declined;

  public boolean deleted;

  public long crtime;
}
