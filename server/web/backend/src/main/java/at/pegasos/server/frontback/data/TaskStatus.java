package at.pegasos.server.frontback.data;

import at.pegasos.server.frontback.tasks.*;
import lombok.*;

import java.util.*;

@Data
public class TaskStatus {
  private final long ID;
  private final boolean completed;
  private final float completion;
  private final Collection<String> messages;

  public TaskStatus(long ID, Task task)
  {
    this.ID = ID;
    this.completed = task.completed();
    this.completion = task.completion();
    this.messages = task.messages();
  }
}
