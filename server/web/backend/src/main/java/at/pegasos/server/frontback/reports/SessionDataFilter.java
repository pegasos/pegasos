package at.pegasos.server.frontback.reports;

public class SessionDataFilter {
  /**
   * Which table to use
   */
  public final String table;
  /**
   * Which field to report
   */
  public final String field;
  /**
   * Name to use in the output
   */
  public final String name;
  /**
   * Additional filter to use. If null no additional filter is applied
   */
  public final String where;

  public SessionDataFilter(String table, String field, String name)
  {
    this.table = table;
    this.field = field;
    this.name = name;
    this.where = null;
  }

  public SessionDataFilter(String table, String field, String name, String where)
  {
    this.table = table;
    this.field = field;
    this.name = name;
    this.where = where;
  }
}
