package at.pegasos.server.frontback.tasks;

import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import at.pegasos.server.frontback.repositories.TempEmailChangeRepository;
import at.pegasos.server.frontback.repositories.TempRegistrationRepository;

@Component
public class CleanTemp extends TimerTask {
  
  private static final Logger logger= LoggerFactory.getLogger(CleanTemp.class);
  
  @Bean
  @Primary
  public CleanTemp bean()
  {
    return new CleanTemp();
  }
  
  @Autowired
  private TempRegistrationRepository temprepo;
  
  @Autowired
  private TempEmailChangeRepository ec;
  

  @Override
  public void run()
  {
    long now= System.currentTimeMillis() / 1000;
    //                  sec  min   h
    long dayago= now - (60 * 60 * 24);
    
    logger.info("Removing old entries from registration");
    temprepo.removeOlderThan(dayago);
    logger.info("Removing old entries from email change");
    ec.removeOlderThan(dayago);
  }
  
}
