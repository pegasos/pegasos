package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.ToString;

@ToString
@Entity
@Table(name="teams")
public class Team {
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public long teamid;
  
  /**
   * Name of the team
   */
  public String name;

  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="userid")
  public User owner;
}
