package at.pegasos.server.frontback.util;

import lombok.extern.slf4j.*;

@Slf4j
public class ObjectHelper {
  public static int getIntValue(Object o)
  {
    if( o instanceof Integer)
      return (Integer) o;
    else if( o instanceof Short)
      return (Short) o;
    else if( o instanceof Long)
    {
      log.warn("{} is possibly to large for int", o);
      return ((Long) o).intValue();
    }
    else
      throw new IllegalArgumentException(o.getClass() + " cannot be converted to int");
  }
}
