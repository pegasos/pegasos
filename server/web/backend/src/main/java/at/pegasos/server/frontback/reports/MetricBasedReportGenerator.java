package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.controller.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;
import org.slf4j.*;

import java.util.*;
import java.util.stream.*;

public class MetricBasedReportGenerator extends ReportGenerator {

  protected final MetricRepository metrrep;
  private final MetricInfoRepository metrirep;
  private final SessionInfoRepository sirepo;

  protected Logger logger = LoggerFactory.getLogger(this.getClass());

  protected MetricBasedReportGeneratorData data = new MetricBasedReportGeneratorData();

  public MetricBasedReportGenerator(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo,
      String[] metric_names)
  {
    super();

    this.data.metric_names = metric_names;

    this.metrirep = metrirep;
    this.metrrep = metrrep;
    this.sirepo = sirepo;

    init();
  }

  public MetricBasedReportGenerator(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo,
      String[] metric_names, List<SessionTypeFilter> filter)
  {
    super();

    this.data.metric_names = metric_names;
    this.data.sessions_filter = filter;

    this.metrirep = metrirep;
    this.metrrep = metrrep;
    this.sirepo = sirepo;

    init();
  }

  private void init()
  {
    Collection<MetricInfo> tmp = metrirep.findByNameInOrderByIDAsc(data.metric_names);
    data.metric_map = tmp.stream().collect(Collectors.toMap(MetricInfo::getID, MetricInfo::getName));
    data.metrics = tmp.stream().map(m -> m.ID).collect(Collectors.toList());
    logger.error("Metric ids resolved: {}", data.metrics);

    assert (data.metric_names.length == data.metrics.size());
  }

  protected void fetchValues()
  {
    logger.debug("Get values for {}, metrics: {}, start: {}, end: {} ", (me != null ? me.user_id : "null"),
        Arrays.toString(data.metric_names), (start != null ? start.getTimeInMillis() : null),
        (end != null ? end.getTimeInMillis() : "null"));
    // logger.debug(data.sessions_filter.toString());

    data.sessions = sirepo.findByUserIdInAndTime(users, start.getTimeInMillis(), end.getTimeInMillis());
    if( data.sessions_filter != null )
    {
      data.sessions = data.sessions.stream().filter(s -> {
        for(SessionTypeFilter f : data.sessions_filter)
        {
          if( f.len == 3 && f.param0 == s.param0 && f.param1 == s.param1 && f.param2 == s.param2 )
            return true;
          else if( f.len == 2 && f.param0 == s.param0 && f.param1 == s.param1 )
            return true;
          else if( f.len == 1 && f.param0 == s.param0 )
            return true;
        }
        return false;
      }).collect(Collectors.toList());
    }

    data.session_ids = data.sessions.stream().map(s -> s.session_id).collect(Collectors.toList());
    logger.debug("Found sessions: {}", data.session_ids);
  }

  public Report generateReport()
  {
    fetchValues();

    ReportSessionData report = new ReportSessionData();

    report.sessions = data.sessions;
    report.data = new MetricData[data.metrics.size()];
    report.users = report.sessions.stream().map(session -> session.userId).collect(Collectors.toSet());

    for(int i = 0; i < data.metrics.size(); i++)
    {
      report.data[i] = new MetricData();
      report.data[i].name = data.metric_map.get(data.metrics.get(i));
      report.data[i].data = new ArrayList<Object>(data.sessions.size());
    }

    int idx = 0;
    for(Long sessionId : data.session_ids)
    {
      List<Metric> metrics = metrrep.findBySessionIdAndMetricIdInOrderByMetricIdAsc(sessionId, data.metrics);
      boolean sessionHasMetric = false;

      logger.debug("sessionId {} {}", sessionId, report.sessions.get(idx).session_id);
      logger.debug("Fetching metrics for session: {} found {}", sessionId, metrics.toString());

      // and every variable / name
      int i = 0;
      for(Long metric : data.metrics)
      {
        if( metrics.size() > 0 && metrics.get(0).metricId == metric )
        {
          report.data[i].data.add(metrics.get(0).value);
          metrics.remove(0);
          sessionHasMetric = true;
        }
        else
        {
          report.data[i].data.add(null);
        }
        i++;
      }

      if( !sessionHasMetric )
      {
        report.sessions.remove(idx);
        int last = report.data[0].data.size() - 1;
        for(i = 0; i < data.metric_names.length; i++)
        {
          report.data[i].data.remove(last);
        }
      }
      else
      {
        idx++;
      }
    }

    return report;
  }
}
