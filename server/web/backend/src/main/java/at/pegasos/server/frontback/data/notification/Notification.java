package at.pegasos.server.frontback.data.notification;

import javax.persistence.*;

import at.pegasos.server.frontback.data.User;

@Entity
@Table(name="notifications")
public class Notification {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  /**
   * Who is addressed by this notofication
   */
  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="userid")
  public User user;

  @ManyToOne(fetch= FetchType.EAGER)
  @JoinColumn(name="type")
  public NotificationType type;

  public String msg;

  public String data;

  public long crtime;

  public boolean deleted;

}
