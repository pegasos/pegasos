package at.pegasos.server.frontback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.*;

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
@PropertySource(value="classpath:pegasos.properties")
public class Application {
  
  public static void main(String[] args)
  {
    SpringApplication.run(Application.class, args);
  }
}
