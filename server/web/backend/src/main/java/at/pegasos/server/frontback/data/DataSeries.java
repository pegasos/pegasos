package at.pegasos.server.frontback.data;

import lombok.*;

import java.util.List;

@ToString
public class DataSeries<T> {
  private final String name;
  private final List<T> data;

  public DataSeries()
  {
    name= null;
    data= null;
  }

  public DataSeries(String name, List<T> data)
  {
    this.name= name;
    this.data= data;
  }
  
  public String getName()
  {
    return name;
  }
  
  public List<T> getData()
  {
    return data;
  }
}
