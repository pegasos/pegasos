package at.pegasos.server.frontback.reports;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import at.pegasos.server.frontback.data.DataSeries;
import at.pegasos.server.frontback.dbdata.Metric;
import at.pegasos.server.frontback.repositories.MetricInfoRepository;
import at.pegasos.server.frontback.repositories.MetricRepository;
import at.pegasos.server.frontback.repositories.SessionInfoRepository;

/**
 * A simple report accumulates all metrics of a given type (the param field is not taken into account)
 */
public class MetricBasedAccumulatedReportGeneratorSimple extends MetricBasedAccumulatedReportGenerator {

  public MetricBasedAccumulatedReportGeneratorSimple(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names, List<SessionTypeFilter> filter)
  {
    super(metrirep, metrrep, sirepo, metric_names, filter);
  }

  @Override protected List<Metric> getMetrics()
  {
    return metrrep.findBySessionIdInAndMetricIdInAndParamIsNullAndSegmentnrIsNull(data.session_ids, data.metrics);
  }

  public MetricBasedAccumulatedReportGeneratorSimple(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names)
  {
    super(metrirep, metrrep, sirepo, metric_names);
  }

  @Override
  public String getKey(Metric m, long date)
  {
    return m.metricId + "-" + m.session.userId + "-" + date;
  }

  @Override
  protected void finishReport()
  {
    report.series= new ArrayList<DataSeries<Double>>(data.metrics.size() * report.users.size());
    
    // Go over all metrics
    for(Long metricId : data.metrics)
    {
      String metric= data.metric_map.get(metricId);
      // For each user
      for(Long userId : users_with_data)
      {
        // Create a series metric-user
        List<Double> data= new ArrayList<Double>(report.dates.size());
        for(Calendar d : report.dates)
        {
          data.add(values.get(metricId + "-" + userId + "-" + d.getTimeInMillis()));
        }
        report.series.add(new DataSeries<Double>(metric + "#" + userId, data));
      }
    }
  }
}
