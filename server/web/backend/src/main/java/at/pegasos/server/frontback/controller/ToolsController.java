package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.repositories.*;
import io.swagger.v3.oas.annotations.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@Slf4j
@RestController
@RestControllerAdvice
@RequestMapping("/v1/tools")
public class ToolsController {

  @Autowired private PrivilegeManager privilegeManager;
  @Autowired private SessionsRepository sessionsRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private JdbcTemplate jdbcTemplate;

  @Operation(description = "Fix the end-time of a session")
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/fixend/{sessionId}")
  public GenericResponse fixEndTime(Principal p,
                                    @Parameter(name = "sessionId", description = "ID of session which should be fixed")
                                    @PathVariable("sessionId") long sessionId) {
    assert p != null;

    Optional<Session> os = sessionsRepository.findById(sessionId);
    if( os.isEmpty() )
      throw new UnkownSessionException();

    Session session = os.get();

    long principalId = userRepository.findByEmail(p.getName()).get(0).user_id;

    if( session.userId == principalId || privilegeManager.hasPrivilegeActivity(p, "fixend", session) )
    {
      long maxEnd = Long.MIN_VALUE;

      for(TableDescription table : TableDescriptions.tables)
      {
        String query = "select max(rec_time) as rT from " + table.table + " where session_id = " + sessionId;
        log.debug(query);

        List<Map<String, Object>> recTime = jdbcTemplate.queryForList(query);

        if( recTime.size() > 0 && recTime.get(0).get("rT") != null )
        {
          Long rec_time = (Long) recTime.get(0).get("rT");
          maxEnd = Math.max(maxEnd, rec_time);
        }
      }

      if( maxEnd < session.endtime )
      {
        session.endtime = maxEnd;
        sessionsRepository.save(session);
        sessionsRepository.flush();
      }
      return GenericResponse.OkResponse;
    }
    else
    {
      return GenericResponse.FailResponse;
    }
  }
}
