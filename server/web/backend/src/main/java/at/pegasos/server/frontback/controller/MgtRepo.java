package at.pegasos.server.frontback.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.pegasos.server.frontback.controller.TeamsController.ManagesTeam;

@Repository
public interface MgtRepo extends JpaRepository<ManagesTeam, Long> {
  
}