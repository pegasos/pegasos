package at.pegasos.server.frontback.data;

import java.util.*;

public class Report {
  public Map<String, Object> extra;

  /**
   * IDs of the users in the report
   */
  public Collection<Long> users;

  public void setExtra(String key, Object value)
  {
    if( extra == null )
    {
      extra= new HashMap<String, Object>();
    }
    extra.put(key, value);
  }
}
