package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TeamInvite;

@Repository
public interface TeamInvitesRepository extends JpaRepository<TeamInvite, Long> {
  
  // List<TeamInvite> findByOldemail(String email);

  /**
   * Get all active invites for a person - team combination
   * @param personId person
   * @param teamid team
   * @return active invites
   */
  @Query("FROM TeamInvite i where i.invitedPerson = :personId AND teamid = :teamid AND deleted = 0")
  List<TeamInvite> findByInvitedPersonTeam(long personId, long teamid);
  
  @Transactional
  @Modifying
  @Query("DELETE FROM TeamInvite t WHERE t.crtime < :date")
  int removeOlderThan(@Param("date") long date);
}
