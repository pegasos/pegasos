package at.pegasos.server.frontback.auth;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings("serial")
@Entity
@Table(name="user")
public class AuthorisationUserDetails implements UserDetails {
  
  @Override
  public String toString()
  {
    return "AuthorisationUserDetails [id=" + id + ", type=" + type + ", forename=" + forename + ", surname=" + surname + ", email=" + email
        + ", hidden=" + hidden + ", db_password=" + db_password + ", salt=" + salt + ", deleted=" + deleted + "]";
  }

  public AuthorisationUserDetails()
  {
    
  }
  
  public long getId()
  {
    return id;
  }

  public void setId(long id)
  {
    this.id= id;
  }

  public short getType()
  {
    return type;
  }

  public void setType(short type)
  {
    this.type= type;
  }

  public String getForename()
  {
    return forename;
  }

  public void setForename(String forename)
  {
    this.forename= forename;
  }

  public String getSurname()
  {
    return surname;
  }

  public void setSurname(String surname)
  {
    this.surname= surname;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email= email;
  }

  public int getHidden()
  {
    return hidden;
  }

  public void setHidden(int hidden)
  {
    this.hidden= hidden;
  }

  public String getSalt()
  {
    return salt;
  }

  public void setSalt(String salt)
  {
    this.salt= salt;
  }

  public boolean getDeleted()
  {
    return deleted;
  }

  public void setDeleted(boolean deleted)
  {
    this.deleted= deleted;
  }

  public void setDbPassword(String password)
  {
    this.db_password= password;
  }
  
  public String getDbPassword()
  {
    return db_password;
  }
  
  public void setCrtime(long crtime)
  {
    this.crtime= crtime;
  }
  
  public long getCrtime()
  {
    return crtime;
  }
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  
  private short type;
  
  private String forename;
  
  private String surname;
  
  private String email;
  
  private long crtime;
  
  private int hidden;
  
  @Column(name="password_1250")
  private String db_password;
  
  @Column(name="salt_0954")
  private String salt;
  
  private boolean deleted;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities()
  {
    // TODO Auto-generated method stub
    return new ArrayList<GrantedAuthority>(0);
  }

  @Override
  public String getPassword()
  {
    return db_password;
  }

  @Override
  public String getUsername()
  {
    return email;
  }

  @Override
  public boolean isAccountNonExpired()
  {
    return !deleted;
  }

  @Override
  public boolean isAccountNonLocked()
  {
    return !deleted;
  }

  @Override
  public boolean isCredentialsNonExpired()
  {
    return !deleted;
  }

  @Override
  public boolean isEnabled()
  {
    return !deleted;
  }
  
}
