package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.exceptions.*;
import at.pegasos.server.frontback.services.*;
import at.pegasos.server.frontback.tasks.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

import java.io.*;
import java.nio.file.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import javax.mail.*;

import at.pegasos.server.frontback.*;
import at.pegasos.server.frontback.auth.Security;
import at.pegasos.server.frontback.auth.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.ValueChanges.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.util.*;
import io.swagger.v3.oas.annotations.*;

@RestController
@RequestMapping("/v1/user")
public class UserController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private UserDetailsRepository userrepo;

  @Autowired
  private UserRepository userPlainrepo;

  @Autowired
  private AuthorisationUserDetailsRepository auto;

  @Autowired
  private TempEmailChangeRepository ec;

  @Autowired
  private UserValueService uservalues;

  @Autowired
  private PrivilegeManager priv;

  @Autowired
  private FileService fileService;

  @Autowired private MessageTemplates templates;

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, /*value = "/get/{userid}"*/ path = {"/get", "/get/{userid}"})
  public UserDetails getUser(Principal p, @PathVariable("userid") Optional<Long> userid)
  {
    assert( p != null );

    if( userid.isPresent() )
    {
      return userrepo.findById(userid.get()).get();
    }
    else
    {
      return userrepo.findByEmail(p.getName()).get(0);
    }
  }

  @RequestMapping(method = RequestMethod.GET, path = "getloggedinuser")
  public User getUser(Principal p)
  {
    logger.debug("{}", p);
    if( p != null )
    {
      return userPlainrepo.findByEmail(p.getName()).get(0);
    }
    return null;
  }

  @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
  @PreAuthorize("isAuthenticated()")
  @ResponseBody
  public GenericResponse changeUserPassword(Principal p, @RequestParam("password") String password,
      @RequestParam("oldpassword") String oldPassword) throws DataAccessException, NoSuchAlgorithmException
  {
    AuthorisationUserDetails user= auto.findByEmail(p.getName()).get(0);

    if( !Security.checkPassword(user, oldPassword) )
    {
      throw new InvalidOldPasswordException();
    }
    
    Security.Password pw= Security.generatePassword(password);
    
    user.setDbPassword(pw.pw_hash);
    user.setSalt(pw.salt);
    
    auto.save(user);
    
    logger.debug(user.getEmail() + " changed password");
    
    return new GenericResponse("success");
  }
  
  @RequestMapping(value = "/changeEmail", method = RequestMethod.POST)
  @PreAuthorize("isAuthenticated()")
  @ResponseBody
  public GenericResponse changeUserEmail(Principal p, @RequestParam("email") String email,
      @RequestParam("oldemail") String oldemail) throws DataAccessException, NoSuchAlgorithmException
  {
    UserDetails user = userrepo.findByEmail(p.getName()).get(0);

    if( !user.email.equals(oldemail) )
    {
      throw new InvalidOldEmailException();
    }

    if( auto.findByEmail(email).size() > 0 )
    {
      return new GenericResponse("failed", "mailinuse");
      // TODO: notify user of that mail? (that someone requested to use their mail
    }

    // TODO: Test if mail is correct / valid

    // Do nothing if there is a mail-change pending already
    if( ec.findByOldemail(oldemail).size() > 0 )
    {
      return new GenericResponse("failed", "mailchange_pending");
    }

    // Now create the temp entry
    String hash = Security.generateRandomHash();

    TempEmailChange t= new TempEmailChange();
    t.userid = user.id;
    t.crtime = System.currentTimeMillis() / 1000;
    t.oldemail = oldemail;
    t.newemail = email;
    t.hash = hash;
    ec.save(t);

    String link = Config.FRONTEND_URL + "/user/finishmailchange?hash=" + hash;
    Map<String, String> replace = new HashMap<>(1);
    replace.put("link", link);
    String msgBody = templates.get("mailchange_requested", user.language, replace);
    try
    {
      Email.sendMail(email, user.firstname, user.lastname, "Finish mail change", msgBody);
    }
    catch( UnsupportedEncodingException | MessagingException e )
    {
      e.printStackTrace();
      return new GenericResponse("failed", "mailsend_failed");
    }

    logger.info("User {} request to change mail to {}", user.email, email);

    return new GenericResponse("success");
  }
  
  @RequestMapping(method = RequestMethod.GET, value = "/finishmailchange/{hash}")
  public GenericResponse finishMailChange(@PathVariable("hash") String hash)
  {
    if( ec.findByHash(hash).size() < 1 )
    {
      return new GenericResponse("failed", "mailchange_notfound");
    }
    
    // find temp object
    TempEmailChange t= ec.findByHash(hash).get(0);
    
    // update user
    UserDetails user= userrepo.findByEmail(t.oldemail).get(0);
    logger.info("Change email for user {} to {}", user, t.newemail);
    user.email= t.newemail;
    userrepo.save(user);

    // remove temp entry
    ec.delete(t);
    
    return new GenericResponse("success");
  }
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/values")
  public List<UserValue> getCurrentValues(Principal p)
  {
    UserDetails me= userrepo.findByEmail(p.getName()).get(0);
    return uservalues.getForUser(me.id);
  }
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/values")
  public GenericResponse setValues(Principal p, @RequestParam(required=false) String start, @RequestBody ValueChanges changes)
  {
    UserDetails me= userrepo.findByEmail(p.getName()).get(0);
    
    if( start != null )
    {
      Calendar cal = Calendar.getInstance();
      @SuppressWarnings("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      try
      {
        cal.setTime(sdf.parse(start));
        for(Change change : changes.changes)
        {
          if( !change.value.equals("") )
          {
            uservalues.setValueForUser(change.name, me.id, change.value, cal);
          }
          else
          {
            uservalues.invalidateValueForUser(change.name, me.id, cal);
          }
        }
      }
      catch( ParseException e )
      {
        e.printStackTrace();
        return new GenericResponse("Fail.Date");
      }
    }
    else
    {
      for(Change change : changes.changes)
      {
        uservalues.setValueForUser(change.name, me.id, change.value);
      }
    }
    
    return new GenericResponse(GenericResponse.OK);
  }
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, path = {"/update", "/update/{userid}"})
  public GenericResponse update(Principal p, @PathVariable Optional<Long> userid, @RequestBody UserDetails changed)
  {
    UserDetails user;
    if( userid.isPresent() )
    {
      user= userrepo.findById(userid.get()).get();
    }
    else
    {
      user= userrepo.findByEmail(p.getName()).get(0);
    }
    
    user.firstname= changed.firstname;
    user.lastname= changed.lastname;
    user.birthday= changed.birthday;
    user.gender= changed.gender;
    
    userrepo.save(user);
    
    return new GenericResponse(GenericResponse.OK);
  }

  @Operation(
      summary = "Get all users",
      description = "Get a list of all users. Requires 'ADMIN' privilege of the logged in user."
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/all")
  public List<UserDetails> getUserRoles(Principal p)
  {
    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User {} is not admin (add)", p);
      throw new UnauthorizedUserException(p.toString());
    }

    return userrepo.findAll();
  }

  @Operation(
          summary = "Set the language for the user",
          description = "Sets the language for the current user (to be used for e.g. notifications."
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/language")
  public Response setLanguage(Principal p, @RequestBody String language)
  {
    assert p != null;

    if( language.startsWith("\"") )
      language = language.substring(1);
    if( language.endsWith("\"") )
      language = language.substring(0, language.length() - 1);

    UserDetails me = userrepo.findByEmail(p.getName()).get(0);
    me.language = language.strip();
    userrepo.save(me);

    return GenericResponse.OkResponse;
  }

  /*@PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/value/{name}/{value}")
  public GenericResponse setValue(Principal p, @PathVariable("name") String name, @PathVariable("value") String value, 
      @RequestParam(required=false) String start)
  {
    long user;
    UserDetails me= userrepo.findByEmail(p.getName()).get(0);
    user= me.id;
    
    if( start != null )
    {
      Calendar cal = Calendar.getInstance();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      try
      {
        cal.setTime(sdf.parse(start));
        uservalues.setValueForUser(name, user, value, cal);
      }
      catch( ParseException e )
      {
        e.printStackTrace();
        // TODO: exception
      }
    }
    else
    {
      uservalues.setValueForUser(name, user, value);
    }
    
    return new GenericResponse("OK");
  }*/

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/createImport/{userid}", method = RequestMethod.PUT)
  public GenericResponse createImport(Principal p, @PathVariable Long userid)
  {
    if( priv.hasPrivilegeUser(p, "uploadFile", userid) )
    {
      long id = fileService.createUpload();
      return new GenericResponse(String.valueOf(id));
    }
    return GenericResponse.FailResponse;
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/importfile/{importid}", method = RequestMethod.POST)
  public GenericResponse addFileToImport(@PathVariable Long importid,
      @RequestParam("file")  MultipartFile file)
  {
    // TODO: we should do some security checks here.
    // in theory it would be possible that someone injects data into the upload
    logger.info("uploadFile {}", file);

    Path p = fileService.uploadFile(importid, file, true);
    if( p != null )
      return GenericResponse.OkResponse;
    else
      return GenericResponse.FailResponse;
  }

  @Autowired TaskService taskService;

  private class Importer extends Thread implements Task {

    private Path uploadDir;
    private Long userid;
    private int completed = 0;
    private boolean isCompleted = false;
    private final List<String> messages = new ArrayList<>();
    private int files = 1;

    public void setData(Path uploadDir, Long userid)
    {
      this.uploadDir = uploadDir;
      this.userid = userid;
    }

    @Override public void run()
    {
      if( uploadDir == null )
      {
        messages.add("Could not create import");
        completed = 1;
        isCompleted = true;
      }
      else
      {
        Pattern startImport = Pattern.compile(".*Start importing (\\d*) files.*");
        Pattern finishedFileImport = Pattern.compile(".*Importing .* finished.*");

        try
        {
          AiHelper.callBlocking(logger, "at.pegasos.integrations.files.ImportFolder",
              (String message) -> {
                Matcher m = startImport.matcher(message);
                if( m.matches() )
                {
                  files = Integer.parseInt(m.group(1));
                  messages.add("Found " + files);
                  logger.debug("Files {}", files);
                }
                else if( finishedFileImport.matcher(message).matches() )
                {
                  completed++;
                  logger.debug("File completed");
                }
                else
                  logger.debug("False");
              },
              "--user", userid, uploadDir.toString());
          fileService.removeTempDir(uploadDir);
          // completed = 1;
          isCompleted = true;
        }
        catch( IOException e )
        {
          e.printStackTrace();
        }
      }
    }

    @Override public float completion()
    {
      return completed / (float) files;
    }

    @Override public Collection<String> messages()
    {
      List<String> ret = new ArrayList<>(messages);
      messages.clear();
      return ret;
    }

    @Override public boolean completed()
    {
      return isCompleted;
    }
  };

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/finishImport/{userid}/{importid}", method = RequestMethod.PUT)
  public TaskStatus doImport(Principal p, @PathVariable Long userid, @PathVariable("importid") Long importId)
  {
    Importer runner = new Importer();

    if( priv.hasPrivilegeUser(p, "uploadFile", userid) )
    {
      runner.setData(fileService.getUploadDir(importId), userid);
      runner.start();
    }
    else
    {
      runner.messages.add("Could not create import");
      runner.completed = 1;
      runner.isCompleted = true;
    }
    long taskId = taskService.addTask(runner);

    return new TaskStatus(taskId, runner);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/importfiles/{userid}", method = RequestMethod.POST)
  public GenericResponse uploadFiles(@PathVariable Long userid, @RequestParam("files") MultipartFile[] files)
  {
    logger.info("uploadFiles {}", Arrays.toString(files));

    Path p = fileService.uploadFiles(files, true);

    AiHelper.callBackground(logger, "at.pegasos.integrations.files.ImportFolder", new Runnable() {

      @Override
      public void run() {
        fileService.removeTempDir(p);
      }
      }, "--user", userid, p.toString());

    /*redirectAttributes.addFlashAttribute("message",
        "You successfully uploaded all files!"); */

    return GenericResponse.OkResponse;
  }
}
