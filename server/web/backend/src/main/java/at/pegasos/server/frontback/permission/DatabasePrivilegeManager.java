package at.pegasos.server.frontback.permission;

import java.security.Principal;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import org.springframework.beans.factory.annotation.Autowired;

import at.pegasos.server.frontback.repositories.UserRepository;

public class DatabasePrivilegeManager implements PrivilegeManager {

  @Autowired
  UserRepository userrepo;

  @Override
  public boolean hasRole(Principal p, String rolename)
  {
    User user= userrepo.findByEmail(p.getName()).get(0);

    return user.roles.stream().anyMatch(r -> r.name.equals(rolename));
  }

  @Override
  public boolean hasRole(long user_id, String rolename)
  {
    User user= userrepo.findById(user_id).get();

    return user.roles.stream().anyMatch(r -> r.name.equals(rolename));
  }

  @Override
  public boolean hasRole(long user_id, long role_id)
  {
    User user= userrepo.findById(user_id).get();

    return user.roles.stream().anyMatch(r -> r.id == role_id);
  }

  @Override
  public boolean hasPrivilege(Principal p, String privilege)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean hasPrivilege(Principal p, String privilege, long user_id)
  {
    User user = userrepo.findByEmail(p.getName()).get(0);
    // check if we are the user, if not whether we are admin
    return user.user_id == user_id || hasRole(p, "ADMIN");
  }

  @Override public boolean hasPrivilegeActivity(Principal p, String privilege, long sessionId)
  {
    return hasRole(p, "ADMIN");
  }

  @Override public boolean hasPrivilegeActivity(Principal p, String privilege, Session session)
  {
    return hasRole(p, "ADMIN") || session.getUser().email.equals(p.getName());
  }

  @Override public boolean hasPrivilegeUser(Principal p, String privilege, long userId)
  {
    return hasRole(p, "ADMIN");
  }
}
