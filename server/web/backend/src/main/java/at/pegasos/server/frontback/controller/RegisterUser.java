package at.pegasos.server.frontback.controller;

import java.io.*;
import java.security.*;
import java.util.*;

import javax.mail.MessagingException;

import at.pegasos.server.frontback.exceptions.*;
import at.pegasos.server.frontback.services.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import at.pegasos.server.frontback.Config;
import at.pegasos.server.frontback.auth.*;
import at.pegasos.server.frontback.auth.Security;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.permission.PrivilegeManager;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.util.Email;

@RestController
@RequestMapping("/v1/users")
public class RegisterUser {
  
  @Autowired
  private TempRegistrationRepository temprepo;
  
  @Autowired
  private AuthorisationUserDetailsRepository repo;
  
  @Autowired
  private TUserRepository trepo;
  
  @Autowired
  private TeamWithUsersRepository turepo;
  
  @Autowired
  private PrivilegeManager priv;

  @Autowired
  TeamWithGroupWithUsersRepository repot;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired private MessageTemplates templates;

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  public static class RegistrationData implements Serializable {
    private static final long serialVersionUID= -1935178537646455474L;
    
    public String password;
    public String confirmPassword;
    public String firstName;
    public String lastName;
    public String email;
    public String code;
    public String language;
    public Long groupId;

    @Override
    public String toString()
    {
      return "RegistrationData [password=" + password + ", confirmPassword=" + confirmPassword + ", firstName=" + firstName + ", lastName="
          + lastName + ", email=" + email + ", code=" + code + ",groupId=" + groupId + ",language=" + language + "]";
    }
  }
  
  private boolean checkData(String email)
  {
    // Testen, ob die E-Mail Adresse bereits in User vorhanden ist
    if( repo.findByEmail(email).size() > 0 )
    {
      logger.error("Received registration from existing user: " + email);
      //TODO
        /*echo '
            Die von Ihnen eingegebene E-Mail Adresse ist bereits vorhanden. Bitte überprüfen Sie Ihre Eingaben.<br />
            <a href="index.php?site=registrierung" title="Zurück zur Registrierung">Zurück zur Registrierung</a>
        ';*/
        return false;
    }
    
    // Testen, ob die E-Mail Adresse bereits in der temporaeren Registrierung vorhanden ist
    if( temprepo.findByEmail(email).size() > 0 )
    {
      logger.error("Received registration from user in process: " + email);
      //TODO
        /*  echo '
            Die von Ihnen eingegebene E-Mail Adresse ist bereits vorhanden. Bitte überprüfen Sie Ihre Eingaben.<br />
            <a href="index.php?site=registrierung" title="Zurück zur Registrierung">Zurück zur Registrierung</a>
        ';*/
        return false;
    }
    
    // Vereinscode ueberpruefen (falls vorhanden)
    // TODO:
    /*
    if (!empty($code)) {
        $query = db_query('
            SELECT *
            FROM club_codes
            WHERE code = "'.$code.'" AND
            used = "0"
        ', $GLOBALS['conn']);
        
        if (mysql_num_rows($query) == 0) {
            echo '
                Der von Ihnen eingegeben Vereinscode wurde nicht gefunden. Bitte überprüfen Sie Ihre Eingaben.<br />
                <a href="index.php?site=registrierung" title="Zurück zur Registrierung">Zurück zur Registrierung</a>
            ';
            return;
        }
    }*/
    return true;
  }
  
  private static class PwData {
    String salt;
    String hash;
    String password_hash;
  }

  private PwData generatePassword(String password, String email, String firstname, String surname, String code) throws NoSuchAlgorithmException, DataAccessException
  {
    String salt= Security.salt(8);
    
    String password_hash = Security.sha1(salt + Security.sha1(password + salt));
    
    logger.debug("gP " + salt + " " + password_hash);
    
    String hash = Security.generateRandomHash();
    // logger.debug("gP " + salt + " " + password_hash + " " + hash);

    PwData data= new PwData();
    data.salt= salt;
    data.password_hash= password_hash;
    data.hash= hash;

    return data;
  }
  
  private class Register extends Thread {
    private final RegistrationData body;

    public Register(RegistrationData body)
    {
      this.body= body;
    }
    
    public void run()
    {
      if( !checkData(body.email) )
      {
        return;
      }

      try
      {
        PwData pw= generatePassword(body.password, body.email, body.firstName, body.lastName, body.code);
        TempRegistration t= new TempRegistration();
        t.club_code= body.code;
        t.crtime= System.currentTimeMillis() / 1000;
        t.email= body.email;
        t.forename= body.firstName;
        t.surname= body.lastName;
        t.password_2896= pw.password_hash;
        t.salt_7863= pw.salt;
        t.hash= pw.hash;

        temprepo.save(t);

        String link = Config.FRONTEND_URL + "/finishregistration?hash=" + pw.hash;
        Map<String, String> replace = new HashMap<>(1);
        replace.put("link", link);
        String msgBody = templates.get("registration_created", body.language, replace);

        Email.sendMail(body.email, body.firstName, body.lastName, "Complete registration", msgBody);
      }
      catch( NoSuchAlgorithmException | DataAccessException | UnsupportedEncodingException | MessagingException e )
      {
        e.printStackTrace();
      }
    }
  }
  
  @RequestMapping(method = RequestMethod.POST, value = "/register")
  public void register(@RequestBody RegistrationData body)
  {
    // logger.error("POST get " + body);
    new Register(body).start();
  }
  
  @RequestMapping(method = RequestMethod.GET, value = "/finishregistration/{hash}")
  public void finishRegistration(@PathVariable("hash") String hash)
  {
    /*
 // Registrierung nicht vorhanden
    if ($size == 0) {
        echo '
            Ihre Registrierung wurde nicht gefunden.<br />
            Die Registrierung wurde nicht abgeschlossen.<br />
            <a href="index.php" title="Zurück zur Website">Zurück zur Website</a>
        ';
    }
    */
    
    // Testen, ob die temporaere Registrierung vorhanden ist
    TempRegistration t= temprepo.findByHash(hash).get(0);
    
    // Vereinscode ueberpruefen (falls vorhanden)
    short type= 0;
    String label= "Mein Team";
    if( t.club_code != null )
    {
      /*
            // Ueberpruefen, ob der Code bereits benutzt wurde
            $queryC = db_query('
                SELECT *
                FROM club_codes
                WHERE code = "'.$row['club_code'].'"
                AND used = "0"
            ', $GLOBALS['conn']);
            
            // Registrierung loeschen, falls Code bereits benutzt wurde
            if (mysql_num_rows($queryC) == 0) {
                $queryC = db_query('
                    DELETE FROM temp_registration
                    WHERE hash = "'.$hash.'"
                ', $GLOBALS['conn']);
                
                echo '
                    Der von Ihnen bei der Registrierung eingegeben Vereinscode wurde bereits aktiviert, bzw. wurde nicht gefunden. Bitte registrieren Sie sich erneut.<br /
                    Die Registrierung wurde nicht abgeschlossen.<br />
                    <a href="index.php" title="Zurück zur Website">Zurück zur Website</a>
                ';
                return;
            }
            
            // Vereinscode auf "used" setzen
            $queryC = db_query('
                UPDATE club_codes
                SET used = "1", usetime = "'.time().'"
                WHERE code = "'.$row['club_code'].'"
                AND used = "0"
            ', $GLOBALS['conn']);
            
            */
            
            type = 1;
            label= "Mein Verein";
    }
        
    // Eintrag in der Usertabelle erstellen
    AuthorisationUserDetails user= new AuthorisationUserDetails();
    user.setType(type);
    user.setForename(t.forename);
    user.setSurname(t.surname);
    user.setEmail(t.email);
    user.setDbPassword(t.password_2896);
    user.setSalt(t.salt_7863);
    user.setCrtime(System.currentTimeMillis()/1000);
    
    logger.debug("Saving user " + user);
    user= repo.saveAndFlush(user);
    
    if( Config.AUTO_TEAM )
    {
      TeamWithUsers team= new TeamWithUsers();
      TUser tuser= trepo.findById(user.getId()).get();
      
      team.captains.add(tuser);
      team.name= label;
      team.crtime= System.currentTimeMillis() / 1000;
      team.type= 1;
      
      turepo.save(team);
    }
    
    // Temporaeren Eintrag loeschen
    temprepo.delete(t);
    
    /*
        // Alles OK
        echo '
            Die Registrierung wurde erfolgreich abgeschlossen!<br />
            Sie können sich nun in die Web-Platform einloggen.<br />
            <a href="index.php" title="Zurück zur Website">Zurück zur Website</a>
        ';
    */
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.POST, value = "/add")
  public Response add(Principal p, @RequestBody RegistrationData body)
  {
    if( !priv.hasRole(p, "ADMIN") )
    {
      logger.error("User " + p + " is not admin (add)");
      throw new UnauthorizedUserException(p.toString());
    }

    // check if user is already in database
    if( !checkData(body.email) )
    {
      return new GenericResponse(GenericResponse.FAIL, "user_exists");
    }

    PwData pw;
    try
    {
      pw= generatePassword(body.password, body.email, body.firstName, body.lastName, body.code);
    }
    catch( DataAccessException | NoSuchAlgorithmException e )
    {
      e.printStackTrace();
      return GenericResponse.FailResponse;
    }

    // Create entry in the user table
    AuthorisationUserDetails user= new AuthorisationUserDetails();
    user.setType((short) 0);
    user.setForename(body.firstName);
    user.setSurname(body.lastName);
    user.setEmail(body.email);
    user.setDbPassword(pw.password_hash);
    user.setSalt(pw.salt);
    user.setCrtime(System.currentTimeMillis()/1000);

    logger.debug("Saving user {}", user);
    user= repo.saveAndFlush(user);

    if( body.groupId != null )
    {
      List<TeamWithGroupsWithUsers> x= repot.findByGroup(body.groupId);
      if( x.size() > 0 )
      {
        String sql= "insert into teams_memberships(userid, teamid, groupid, crtime) values (" + user.getId() + "," + x.get(0).teamid + ","
            + body.groupId + "," + (System.currentTimeMillis() / 1000) + ")";
        jdbcTemplate.execute(sql);

        /*for(GroupWithUsers group : x.get(0).groups)
        {
          if( group.groupid == body.groupId )
          {
            group.members.add(e);
          }
        }*/
      }
      // TODO: else throw exception
    }

    return Response.CreatedResponse;
  }
}
