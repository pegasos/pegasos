package at.pegasos.server.frontback.data;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name="roles")
@ToString
public class Role {
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public long id;

  /**
   * Name of the team
   */
  public String name;
}
