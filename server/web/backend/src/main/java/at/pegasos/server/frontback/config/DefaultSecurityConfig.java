package at.pegasos.server.frontback.config;

import at.pegasos.server.frontback.auth.*;
import com.fasterxml.jackson.databind.*;
import lombok.extern.slf4j.*;
import org.springframework.context.annotation.*;
import org.springframework.http.*;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.web.*;
import org.springframework.security.web.authentication.*;
import org.springframework.security.web.authentication.logout.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

@EnableWebSecurity
@Slf4j
public class DefaultSecurityConfig {

  @Slf4j
  public static class SimpleAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException
    {
      if( !request.getMethod().equals("POST") )
      {
        throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
      }

      UsernamePasswordAuthenticationToken authRequest = getAuthRequest(request);
      setDetails(request, authRequest);
      UsernamePasswordAuthenticationToken ret = (UsernamePasswordAuthenticationToken) this.getAuthenticationManager().authenticate(authRequest);
      Map<String,String> details = new HashMap<>();
      if( request.getParameter("loginType") != null )
        details.put("login_type", request.getParameter("loginType"));
      if( request.getParameter("stayloggedin") != null )
        details.put("stayloggedin", request.getParameter("stayloggedin"));
      ret.setDetails(details);
      return ret;
    }

    private UsernamePasswordAuthenticationToken getAuthRequest(HttpServletRequest request)
    {
      String username = request.getParameter("email");
      String password = obtainPassword(request);

      return new UsernamePasswordAuthenticationToken(username, password);
    }
  }

  // @formatter:off
  @Bean
  SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
    http
        .addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
        .csrf().disable()
        .authorizeRequests(authorize -> authorize
            .antMatchers("/v1/**").permitAll()
            .antMatchers("/login").permitAll()
            .anyRequest().authenticated()
        )
        .logout(logout -> logout
            .logoutUrl("/logout")
            .clearAuthentication(true)
            .invalidateHttpSession(true)
            .deleteCookies("JSESSIONID")
            // oncce logged out we do not want to do much more. Its the responsibility of the client to perform further actions
            .logoutSuccessHandler((new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK)))
            .permitAll()
        )
        .formLogin()
        .loginPage("/login")
        .usernameParameter("email")
        .defaultSuccessUrl("/loggedin")
        // .failureUrl("/login?error=true")
        // .failureHandler(failureHandler())
        .permitAll()
        .and()
        .oauth2ResourceServer().jwt()
    ;

    return http.build();
  }
  // @formatter:on

  public SimpleAuthenticationFilter authenticationFilter()
  {
    SimpleAuthenticationFilter filter = new SimpleAuthenticationFilter();
    filter.setAuthenticationManager((AuthenticationManager) authenticationBean());
    filter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler() {
      private ObjectMapper objectMapper = new ObjectMapper();

      @Override public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
          AuthenticationException exception) throws IOException, ServletException
      {
        // log.error("failureHandle {} {} {}", request, response, exception);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        Map<String, Object> data = new HashMap<>();
        data.put("timestamp", Calendar.getInstance().getTime());
        data.put("exception", exception.getMessage());

        response.getOutputStream().println(objectMapper.writeValueAsString(data));

        request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", exception);

        getRedirectStrategy().sendRedirect(request, response, "/login?error=true");
      }
    });
    // filter.setAuthenticationFailureHandler(failureHandler());
    return filter;
  }

  @Bean
  UserDetailsService users() {
    return new AuthorisationUserDetailsService();
  }

  @Bean
  @Primary
  public AuthenticationProvider authenticationBean()
  {
    return new PegasosAuthenticationManager();
  }
}
