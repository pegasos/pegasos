package at.pegasos.server.frontback.data;

import java.util.*;

/**
 * Report containing values for users on specific dates.
 */
public class ReportUserDatesList extends ReportUserDates {
  public boolean hasLists = true;

  /**
   * Dates on which points for the data series are present
   */
  public List<Calendar> dates;

  /**
   * Data for users on the specific dates. For each user one data series exists. Each data series
   * has an entry (null or value) for each date
   */
  public List<DataSeries<List<Double>>> series;

  /**
   * Construct a completely empty report
   */
  public ReportUserDatesList()
  {
    dates = Collections.emptyList();
    users = Collections.emptyList();
    series = Collections.emptyList();
  }

  public ReportUserDatesList(ReportUserDatesSingle old)
  {
    this.dates = old.dates;
    this.users = old.users;
  }
}
