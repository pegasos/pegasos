package at.pegasos.server.frontback.util;

import org.slf4j.*;

import java.io.*;
import java.util.*;
import java.util.function.*;

import at.pegasos.server.frontback.*;
import lombok.extern.slf4j.*;

@Slf4j
public class AiHelper {
  
  /**
   * Run an AI action
   *
   * @param action
   *          action to be performed as fully classified class name
   * @param arguments
   *          arguments to the action. All arguments should be mappable using .toString()
   */
  public static boolean callBackground(String action, Object... arguments)
  {
    return callBackground(log, action, arguments);
  }

  /**
   * Run an AI action
   *
   * @param action
   *          action to be performed as fully classified class name
   * @param onComplete
   *          action to be run once the module has run
   * @param arguments
   *          arguments to the action. All arguments should be mappable using .toString()
   */
  public static boolean callBackground(String action, Runnable onComplete, Object... arguments)
  {
    return callBackground(log, action, arguments);
  }

  /**
   * Run an AI action
   *
   * @param logger
   *          logger to be used for
   * @param action
   *          action to be performed as fully classified class name
   * @param arguments
   *          arguments to the action. All arguments should be mappable using .toString()
   */
  public static boolean callBackground(Logger logger, String action, Object... arguments)
  {
    return callBackground(logger, action, null, arguments);
  }
  
  /**
   * Run an AI action
   *
   * @param logger
   *          logger to be used for
   * @param action
   *          action to be performed as fully classified class name
   * @param onComplete
   *          action to be run once the module has run
   * @param arguments
   *          arguments to the action. All arguments should be mappable using .toString()
   */
  public static boolean callBackground(Logger logger, String action, Runnable onComplete, Object... arguments)
  {
    final String[] cmd1= {"java", "-cp", Config.AI_FILE, action};
    
    final String[] cmd= new String[cmd1.length + arguments.length];
    System.arraycopy(cmd1, 0, cmd, 0, cmd1.length);
    if( arguments.length > 0 )
    {
      int i= cmd1.length;
      for(Object o : arguments)
      {
        cmd[i++]= o.toString();
      }
    }
    
    log.debug("Command to run: " + Arrays.toString(cmd));
    Thread runner= new Thread() {
      public void run()
      {
        try
        {
          Process pr= Runtime.getRuntime().exec(cmd);
          BufferedReader line_in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
          BufferedReader stdError = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
          String line_out;
          
          try
          {
            while( (line_out= line_in.readLine()) != null )
            {
              log.debug("AI({}): {}", action, line_out);
            }
          }
          catch( IOException e )
          {
            e.printStackTrace();
            log.error("Running command failed with " + e.getMessage());
          }

          try
          {
            while( (line_out= stdError.readLine()) != null )
            {
              log.error("AI({}): {}", action, line_out);
            }
          }
          catch( IOException e )
          {
            e.printStackTrace();
            log.error("Running command failed with " + e.getMessage());
          }
        }
        catch( IOException e )
        {
          e.printStackTrace();
          log.error("Running command failed with " + e.getMessage());
        }

        log.debug("AI({}) has finished", action);
        if( onComplete != null )
        {
          logger.debug("AI({}): running onComplete", action);
          onComplete.run();
        }
      }
    };
    runner.start();
    return true;
  }


  /**
   * Run an AI action
   *
   * @param logger
   *          logger to be used for
   * @param action
   *          action to be performed as fully classified class name
   * @param arguments
   *          arguments to the action. All arguments should be mappable using .toString()
   */
  public static void callBlocking(Logger logger, String action, Object... arguments) throws IOException
  {
    final String[] cmd1= {"java", "-cp", Config.AI_FILE, action};

    final String[] cmd= new String[cmd1.length + arguments.length];
    System.arraycopy(cmd1, 0, cmd, 0, cmd1.length);
    if( arguments.length > 0 )
    {
      int i= cmd1.length;
      for(Object o : arguments)
      {
        cmd[i++]= o.toString();
      }
    }

    log.debug("Command to run: " + Arrays.toString(cmd));

    Process pr= Runtime.getRuntime().exec(cmd);
    BufferedReader line_in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
    BufferedReader stdError = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
    String line_out;

    try
    {
      while( (line_out= line_in.readLine()) != null )
      {
        log.debug("AI({}): {}", action, line_out);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      log.error("Running command failed with " + e.getMessage());
    }

    try
    {
      while( (line_out= stdError.readLine()) != null )
      {
        log.error("AI({}): {}", action, line_out);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      log.error("Running command failed with " + e.getMessage());
    }

    log.debug("AI({}) has finished", action);
  }

  public static void callBlocking(Logger logger, String action, Consumer<String> listener, Object... arguments) throws IOException
  {
    final String[] cmd1= {"java", "-cp", Config.AI_FILE, action};

    final String[] cmd= new String[cmd1.length + arguments.length];
    System.arraycopy(cmd1, 0, cmd, 0, cmd1.length);
    if( arguments.length > 0 )
    {
      int i= cmd1.length;
      for(Object o : arguments)
      {
        cmd[i++]= o.toString();
      }
    }

    log.debug("Command to run: " + Arrays.toString(cmd));

    Process pr= Runtime.getRuntime().exec(cmd);
    BufferedReader line_in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
    BufferedReader stdError = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
    String line_out;

    try
    {
      while( (line_out= line_in.readLine()) != null )
      {
        // log.debug("AI({}): {}", action, line_out);
        listener.accept(line_out);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      log.error("Running command failed with " + e.getMessage());
    }

    try
    {
      while( (line_out= stdError.readLine()) != null )
      {
        log.error("AI({}): {}", action, line_out);
        listener.accept(line_out);
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      log.error("Running command failed with " + e.getMessage());
    }

    log.debug("AI({}) has finished", action);
  }
}
