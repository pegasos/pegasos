package at.pegasos.server.frontback.charts;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.chart.*;
import org.springframework.jdbc.core.*;

import java.util.*;

public class DBMultiColChart extends DBChart {
  private final String query;
  private final String avail_query;
  private final int colCount;

  public DBMultiColChart(String name, Type type, String tbl, String[] cols)
  {
    super(name, type);

    query = "SELECT rec_time, " + String.join(",", cols) + " FROM " + tbl + " WHERE session_id = ? ORDER BY rec_time ASC";
    StringBuilder builder = new StringBuilder("SELECT ");
    int i = 0;
    for(String col : cols)
    {
      builder.append("count(").append(col).append(") as c").append(i++);
      if( i < cols.length)
        builder.append(",");
    }
    builder.append(" FROM ").append(tbl).append(" WHERE session_id = ?");
    avail_query = builder.toString();
    colCount = cols.length;
  }

  public DBMultiColChart(String name, Type type, String tbl, String[] cols, String where)
  {
    super(name, type);

    query =
        "SELECT rec_time, " + String.join(",", cols) + " FROM " + tbl + " WHERE " + where + " AND session_id = ? ORDER BY rec_time ASC";
    StringBuilder builder = new StringBuilder("SELECT ");
    int i = 0;
    for(String col : cols)
    {
      builder.append("count(").append(col).append(") as c").append(i++);
      if( i < cols.length)
        builder.append(",");
    }
    builder.append(" FROM ").append(tbl).append(" WHERE ").append(where).append(" AND session_id = ?");
    avail_query = builder.toString();
    colCount = cols.length;
  }

  @Override public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    Map<String, Object> t = jdbcTemplate.queryForList(avail_query, new Object[] {session}).get(0);

    for(int i = 0; i < colCount; i++)
    {
      if( ((Long) t.get("c" + i)) > 0 )
        return true;
    }
    return false;
  }

  @Override public String getDataQuery()
  {
    return query;
  }

  @Override public ChartWithRowData newResult(List<Map<String, Object>> rows)
  {
    return new ChartWithRowData(type, rows);
  }
}
