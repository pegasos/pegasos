package at.pegasos.server.frontback.charts;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.chart.*;
import org.springframework.jdbc.core.*;

import java.util.*;

/**
 * Internal type representing information about an available chart. This type is not exposed. Any
 * relevant information contained in this type should also be present in the frontend and accessible
 * via the unique `name`.
 */
public class DBMapChart extends DBChart {
  private final String avail_query;
  private final String query;

  public DBMapChart(String name, Type type, String tbl, String collat, String collon)
  {
    super(name, type);

    this.query = "SELECT rec_time, " + collat + ", " + collon + " FROM " + tbl + " WHERE session_id = ? ORDER BY rec_time ASC";
    this.avail_query = "SELECT count(" + collat + ") as c from " + tbl + " where session_id = ?";
  }

  @Override public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    Map<String, Object> t = jdbcTemplate.queryForList(avail_query, new Object[] {session}).get(0);

    return ((Long) t.get("c")) > 0;
  }

  @Override public String getDataQuery()
  {
    return query;
  }

  public MapChart newResult(List<Map<String, Object>> rows)
  {
    ArrayList<at.pegasos.server.frontback.data.chart.MapChart.GeoPoint> points = new ArrayList<at.pegasos.server.frontback.data.chart.MapChart.GeoPoint>(
        rows.size());

    for(Map<String, Object> row : rows)
    {
      at.pegasos.server.frontback.data.chart.MapChart.GeoPoint p = new at.pegasos.server.frontback.data.chart.MapChart.GeoPoint(
          Long.parseLong(row.get("rec_time").toString()), Double.parseDouble(row.get("lat").toString()),
          Double.parseDouble(row.get("lon").toString()));
      points.add(p);
    }

    return new MapChart(points);
  }
}
