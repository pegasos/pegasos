package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.charts.DBMapChart;
import at.pegasos.server.frontback.charts.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.chart.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;
import io.swagger.v3.oas.annotations.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@RestController
@RequestMapping("/v1/charts")
public class ChartController {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired JdbcTemplate jdbcTemplate;

  @Autowired SessionsRepository repo;

  private List<Map<String, Object>> getHRV_row(long sessionID)
  {
    return jdbcTemplate.queryForList("SELECT rec_time, v1,v2,v3,v4 from hr where session_id = ? ORDER BY rec_time ASC", sessionID);
  }
  
  private List<Integer> getHRV(long sessionID)
  {
    final String[] cols = new String[] {"v1", "v2", "v3", "v4"};

    List<Integer> ret;

    List<Map<String, Object>> rows = jdbcTemplate.queryForList(
        "SELECT rec_time, v1,v2,v3,v4 from hr where session_id = ? ORDER BY rec_time ASC", sessionID);

    ret = new ArrayList<Integer>(rows.size() * 2);

    for(Map<String, Object> row : rows)
    {
      for(String col : cols)
      {
        if( row.get(col) != null )
        {
          Integer c = (Integer) row.get(col);
          ret.add(c);
        }
        else
          break;
      }
    }

    return ret;
  }

  @RequestMapping("/poincare")
  public Chart hrv(@RequestParam(value = "session") long session)
  {
    List<SimpleChart.Point> points = new ArrayList<SimpleChart.Point>();
    int last = 0;
    int c;
    SimpleChart.Point p;
    List<Map<String, Object>> rows = getHRV_row(session);
    String[] cols = new String[] {"v1", "v2", "v3", "v4"};
    for(Map<String, Object> row : rows)
    {
      for(String col : cols)
      {
        if( row.get(col) != null )
        {
          c = (Integer) row.get(col);
          if( last != 0 )
          {
            p = new SimpleChart.Point(last, c);
            points.add(p);
          }
          last = c;
        }
        else
          break;
      }
    }

    return new SimpleChart(Type.Poincare, points);
  }

  @RequestMapping("/hrvraw")
  public Chart hrvraw(@RequestParam(value = "session") long session)
  {
    List<SimpleChart.Point> points = new ArrayList<SimpleChart.Point>();
    SimpleChart.Point p;
    List<Integer> vals = getHRV(session);
    int i = 1;
    for(Integer v : vals)
    {
      p = new SimpleChart.Point(i++, v);
      points.add(p);
    }

    return new SimpleChart(Type.Line, points);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/hrvList/{session}")
  public Chart hrvList(Principal p, @PathVariable("session") long sessionId)
  {
    assert (p != null);

    final String[] cols = new String[] {"v1", "v2", "v3", "v4"};

    List<SimpleChart.Point> points = new ArrayList<SimpleChart.Point>();
    List<Map<String, Object>> rows = getHRV_row(sessionId);

    for(Map<String, Object> row : rows)
    {
      long time = (Long) row.get("rec_time");
      for(String col : cols)
      {
        if( row.get(col) != null )
        {
          int c = (Integer) row.get(col);
          points.add(new SimpleChart.Point(time, c));
        }
        else
          break;
      }
    }

    return new SimpleChart(Type.Tachogram, points);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping("/getavailable/{sessionId}")
  public List<ChartInfo> getAvailableCharts(Principal p, @PathVariable long sessionId)
  {
    Optional<Session> os= repo.findById(sessionId);
    if( os.isEmpty() )
      throw new UnkownSessionException();

    Session session= os.get();

    List<AvailableChart> avail= AvailableCharts.ForSession(session.param0, session.param1, session.param2);
    log.debug("Test available Charts for {}: {}", session.session_id, avail);

    ArrayList<ChartInfo> ret= new ArrayList<ChartInfo>(avail.size());
    for(AvailableChart chart : avail)
    {
      if( chart.isAvailable(jdbcTemplate, sessionId) )
      {
        ChartInfo c= new ChartInfo();
        c.name= chart.name;
        c.type= chart.type;
        ret.add(c);
      }
    }
    log.debug("available Charts for {}: {}", session.session_id, ret);

    return ret;
  }

  @Operation(description = "Get data of a session in the form of a simple database chart. Returns chart with points")
  @RequestMapping("/getchart/{id}/session/{session}")
  public Chart getDBChart(
      @Parameter(name = "session", description = "ID of the session to be queried") @PathVariable("session") long session,
      @Parameter(name = "id", description = "id/name of the chart") @PathVariable("id") String id)
  {
    // TODO: check privileges
    AvailableChart tmp= AvailableCharts.getByID(id);
    if( !(tmp instanceof DBChart) )
    {
      log.error("Queried chart {} = {} is not a dbchart", id, tmp);
      throw new IllegalArgumentException(id + " is not a dbchart");
    }

    DBChart chart = (DBChart) tmp;
    List<Map<String, Object>> rows= jdbcTemplate.queryForList(chart.getDataQuery(), session);

    return chart.newResult(rows);
  }

  @RequestMapping("/getgps/{id}/session/{session}")
  public MapChart getGPS(@PathVariable("id") String id, @PathVariable("session") long session)
  {
    // TODO: check privileges
    AvailableChart tmp= AvailableCharts.getByID(id);
    if( !(tmp instanceof DBMapChart) ) throw new IllegalArgumentException(id + " is not a map chart");
    DBMapChart chartconfig= (DBMapChart) tmp;

    List<Map<String, Object>> rows= jdbcTemplate.queryForList(chartconfig.getDataQuery(), session);

    return chartconfig.newResult(rows);
  }
}
