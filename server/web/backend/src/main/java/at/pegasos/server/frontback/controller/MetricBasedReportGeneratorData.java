package at.pegasos.server.frontback.controller;

import java.util.List;
import java.util.Map;

import at.pegasos.server.frontback.data.SessionInfo;
import at.pegasos.server.frontback.reports.SessionTypeFilter;

public class MetricBasedReportGeneratorData {
  public String[] metric_names;
  /**
   * IDs of the metrics to be used in the report
   */
  public List<Long> metrics;
  /**
   * Sessions which can be contained in the report
   */
  public List<SessionInfo> sessions;
  public List<Long> session_ids;
  /**
   * Filter which session are included in the report
   */
  public List<SessionTypeFilter> sessions_filter;
  
  /**
   * Map for all metrics. Tuples are <metric-id, metric-name>
   */
  public Map<Long, String> metric_map;
  
  public MetricBasedReportGeneratorData()
  {
  }
}