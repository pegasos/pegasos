package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.util.*;
import lombok.extern.slf4j.*;
import org.springframework.jdbc.core.*;

import java.util.*;
import java.util.stream.*;

@Slf4j
public class SessionValueReportGenerator extends ReportGenerator {

  /**
   * Input: which data to include in the report
   */
  private final SessionDataFilter[] requestedData;
  /**
   * Input: Filter which session are included in the report
   */
  private final List<SessionTypeFilter> sessions_filter;

  private final Map<String, List<Temp>> values;

  private final Set<Long> personsWithValues;

  private final Set<Long> sessionIds;

  private final JdbcTemplate jdbcTemplate;
  private final SessionInfoRepository sessionInfoRepository;

  public SessionValueReportGenerator(final JdbcTemplate jdbcTemplate, final SessionInfoRepository sessionInfoRepository,
      List<SessionDataFilter> data, List<SessionTypeFilter> filter)
  {
    super();
    this.jdbcTemplate = jdbcTemplate;
    this.sessionInfoRepository = sessionInfoRepository;
    this.requestedData = data.toArray(new SessionDataFilter[0]);
    this.sessions_filter = filter;

    this.values = new HashMap<String, List<Temp>>();
    this.personsWithValues = new HashSet<>();
    this.sessionIds = new HashSet<>();
  }

  public SessionValueReportGenerator(final JdbcTemplate jdbcTemplate, final SessionInfoRepository sessionInfoRepository,
      SessionDataFilter[] dataQuery, List<SessionTypeFilter> filter)
  {
    super();
    this.jdbcTemplate = jdbcTemplate;
    this.sessionInfoRepository = sessionInfoRepository;
    this.requestedData = dataQuery;
    this.sessions_filter = filter;

    this.values = new HashMap<String, List<Temp>>();
    this.personsWithValues = new HashSet<>();
    this.sessionIds = new HashSet<>();
  }

  @Override public ReportSessionData generateReport()
  {
    fetchValues();

    List<SessionInfo> sessions = sessionInfoRepository.findAllByIdInOrderByStarttime(sessionIds);
    Set<String> valueNames = values.keySet();

    // Now build the report

    ReportSessionData report = new ReportSessionData();
    report.sessions = sessions;
    report.users = personsWithValues;
    report.data = new MetricData[valueNames.size()];

    MetricData data;
    int repDataIdx = 0;
    for(Map.Entry<String, List<Temp>> entry : this.values.entrySet())
    {
      List<Temp> seriesValues = entry.getValue();

      int idx = 0;
      data = new MetricData();
      data.name = entry.getKey();
      data.data = new ArrayList<Object>(sessions.size());

      for(SessionInfo session : sessions)
      {
        Temp value = seriesValues.get(idx);
        if( value.sessionId == session.session_id )
        {
          data.data.add(value.value);
          idx++;
        }
        else
        {
          data.data.add(null);
        }
      }

      report.data[repDataIdx++] = data;
    }

    return report;
  }

  private void fetchValues()
  {
    for(SessionDataFilter data : requestedData)
    {
      fetchValues(data);
    }
  }

  private void fetchValues(SessionDataFilter data)
  {
    StringBuilder query = new StringBuilder("SELECT s.session_id, s.starttime, s.userid, tbl.");
    query.append(data.field);
    query.append(" FROM ").append(data.table).append(" tbl ");
    query.append("LEFT JOIN session s ON tbl.session_id = s.session_id WHERE ");

    if( sessions_filter != null && sessions_filter.size() > 0 )
    {
      query.append(SessionFilter.filterToQuery(sessions_filter));
      query.append(" AND ");
    }

    query.append("s.userid IN (");
    query.append(users.stream().map(String::valueOf).collect(Collectors.joining(",")));
    query.append(")");

    query.append(" AND ");
    query.append("starttime > ").append(this.start.getTimeInMillis());
    query.append(" AND endtime < ").append(this.end.getTimeInMillis());

    if( data.where != null )
      query.append(" AND ").append(data.where);

    query.append(" ORDER BY starttime ASC");

    log.trace(query.toString());

    List<Temp> res = jdbcTemplate.query(query.toString(), (rs, rowNum) -> {
      Temp c = new Temp();
      c.sessionId = rs.getLong(1);
      c.startTime = rs.getLong(2);
      c.personId = rs.getLong(3);
      c.value = rs.getObject(4);
      c.name = data.name;
      return c;
    });

    if( res.size() > 0 )
    {
      personsWithValues.addAll(res.stream().map(v -> v.personId).collect(Collectors.toSet()));
      sessionIds.addAll(res.stream().map(v -> v.sessionId).collect(Collectors.toSet()));
      this.values.put(data.name, res);
    }
  }

  private static class Temp {
    public Long sessionId;
    public Long startTime;
    public Long personId;
    public String name;
    public Object value;
  }

}
