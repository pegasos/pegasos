package at.pegasos.server.frontback.charts;

import org.springframework.jdbc.core.JdbcTemplate;

import at.pegasos.server.frontback.data.Type;

public class AlwaysAvailableChart extends AvailableChart {
  
  public AlwaysAvailableChart(String name, Type type)
  {
    super(name, type);
  }

  @Override
  public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    return true;
  }
}
