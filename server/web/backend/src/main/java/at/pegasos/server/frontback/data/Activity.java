package at.pegasos.server.frontback.data;

import java.util.*;

import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.util.*;
import org.springframework.lang.Nullable;

import at.pegasos.server.frontback.ActivityNames;

public class Activity {

  public Activity(Map<String, Object> d)
  {
    this.session_id = Long.parseLong(d.get("session_id").toString());
    this.user_id = (Integer) d.get("userid");
    this.user_firstname = (String) d.get("user_firstname");
    this.user_lastname = (String) d.get("user_lastname");

    if( d.get("teamid") != null )
    {
      this.team_id = Long.parseLong(d.get("teamid").toString());
      if( this.team_id != 0 )
      {
        this.team_name = (String) d.get("team_name");
      }
    }
    else
      this.team_id = 0;

    if( d.get("groupid") != null )
    {
      this.group_id = Long.parseLong(d.get("groupid").toString());
      if( this.group_id != 0 )
      {
        this.group_name = (String) d.get("group_name");
      }
    }
    else
      this.group_id = 0;


    this.activity_name= ActivityNames.getActivityName(ObjectHelper.getIntValue(d.get("activity")),
            ObjectHelper.getIntValue(d.get("param1")), ObjectHelper.getIntValue(d.get("param2")));
    this.date_time= (Long) d.get("starttime");
    
    long endtime= (Long) d.get("endtime");
    long last_update= (Long) d.get("last_update");
    
    this.duration= (int) ((endtime == 0 ? last_update : endtime) - date_time);
    this.duration/= 1000.0;

    this.tzServer = Short.parseShort(d.get("tz_server").toString());
    this.tzLocal = Short.parseShort(d.get("tz_local").toString());
  }

  public Activity(Session session)
  {
    this.session_id= session.session_id;
    this.user_id= session.userId;
    this.user_firstname= session.getUser().firstname;
    this.user_lastname= session.getUser().lastname;
    if( session.getTeam() != null )
    {
      this.team_name= session.getTeam().name;
      this.team_id= session.getTeam().teamid;
    }
    else
    {
      this.team_id= 0;
    }
    if( session.getGroup() != null )
    {
      this.group_name= session.getGroup().name;
      this.group_id= session.getGroup().groupid;
    }
    else
    {
      this.team_id= 0;
    }
    this.activity_name= ActivityNames.getActivityName(session.param0, session.param1, session.param2);
    this.date_time= session.starttime;
    
    this.duration= (int) ((session.endtime == 0 ? session.last_update : session.endtime) - session.starttime);
    this.duration/= 1000.0;

    this.tzServer = session.tzServer;
    this.tzLocal = session.tzLocal;
  }

  /**
   * ID of the user who performed this activity
   */
  public long user_id;
  /**
   * name of the user who performed this activity
   */
  public String user_firstname;
  /**
   * name of the user who performed this activity
   */
  public String user_lastname;

  /**
   * Name of the team for which the activity was performed. Null if it was not a team activity
   */
  @Nullable
  public String team_name;

  /**
   * ID of the team for which the activity was performed. 0 if it was not a team activity
   */
  public long team_id;

  /**
   * Name of the group for which the activity was performed. Null if it was not a team activity
   */
  @Nullable
  public String group_name;

  /**
   * ID of the team for which the activity was perfomed. 0 if it was not a team activity
   */
  public long group_id;

  /**
   * ID of the session
   */
  public long session_id;

  /**
   * Name of the activity (to be translated?). Empty if no name is available
   */
  public String activity_name;

  /**
   * Timestamp when the activity was started
   */
  public long date_time;

  /**
   * Duration of the activity [s]
   */
  public int duration;

  public short tzServer;
  public short tzLocal;

  /**
   * A 'list' of values summarising the activity
   */
  public Map<String, String> list_summary;

  /**
   * A 'list' of reports/downloads available for this activity
   */
  public List<Download> downloads;
}
