package at.pegasos.server.frontback.charts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.jdbc.core.JdbcTemplate;

import at.pegasos.server.frontback.data.DataSeries;
import at.pegasos.server.frontback.data.Type;
import at.pegasos.server.frontback.data.chart.ChartWithPoints;
import at.pegasos.server.frontback.data.chart.ChartWithPoints.Point;
import at.pegasos.server.frontback.data.chart.MultiLineChart;

/**
 * Internal type representing information about an available chart. This type is not exposed. Any
 * relevant information contained in this type should also be present in the frontend and accessible
 * via the unique `name`.
 */
public class DBChartMultiLine extends DBChart {
  
  public enum LineType {
    /**
     * Lines are defined by a field/column (for example sensor_nr), data is contained in a different
     * column . lines[0]= discriminatory field, lines[1] data field
     */
    FIELD,
    /**
     * Lines are separated in several columns of this table. For example acceleration data
     * (lines={acc_x, acc_y, acc_z)
     */
    COLS
  }
  
  private final LineType linetype;
  private final String[] lines;
  
  private final String avail_select;
  private final String query;
  
  public DBChartMultiLine(String name, String tbl, LineType linetype, String[] lines)
  {
    super(name, Type.MultiLine);
    this.linetype= linetype;
    this.lines= lines;
    
    switch( linetype )
    {
      case COLS:
        assert lines.length >= 1;
        StringBuilder s = new StringBuilder("SELECT count(" + lines[0] + ") as c0");
        for(int i = 1; i < lines.length; i++)
          s.append(", count(").append(lines[i]).append(") as c").append(i);
        s.append("from ").append(tbl).append(" where session_id = ?");
        avail_select = s.toString();
        query = "SELECT rec_time, " + String.join(",", lines) + " from " + tbl + " where session_id = ? ORDER BY rec_time ASC";
        break;

      case FIELD:
        avail_select= "select sum(_c_0_) as c from (select " + lines[0] + ", count(" + lines[1] + ") as _c_0_ from " + tbl
            + " where session_id = ? group by " + lines[0] + ") as _s_0";
        query= "SELECT rec_time, " + lines[0] + "," + lines[1] + " from " + tbl + " where session_id = ? ORDER BY " + lines[0] + ", rec_time ASC";
        break;
      default:
        avail_select= null;
        query= null;
        break;
    }
  }
  
  @Override
  public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    
    Map<String, Object> t= jdbcTemplate.queryForList(avail_select, new Object[] {session}).get(0);
    
    BigDecimal h= ((BigDecimal) t.get("c"));

    return h != null && h.longValue() > 0;
  }

  @Override
  public String getDataQuery()
  {
    return query;
  }

  @Override
  public ChartWithPoints newResult(List<Map<String, Object>> rows)
  {
    MultiLineChart ret= null;
    
    switch( linetype )
    {
      case COLS:
        @SuppressWarnings("unchecked")
        ArrayList<Point>[] pseries = (ArrayList<Point>[]) new ArrayList<?>[lines.length];
        int i;
        final int il= lines.length;
        for(i= 0; i < il; i++)
          pseries[i]= new ArrayList<Point>(rows.size());
        
        for(Map<String, Object> row : rows)
        {
          double t= Double.parseDouble(row.get("rec_time").toString());
          for(i= 0; i < il; i++)
          {
            Object o= row.get(lines[i]);
            if( o != null )
            {
              Point p= new Point(t, Double.parseDouble(o.toString()));
              pseries[i].add(p);
            }
          }
        }
        
        List<DataSeries<Point>> series= new ArrayList<DataSeries<Point>>(lines.length);
        for(i= 0; i < il; i++)
        {
          series.add(new DataSeries<Point>(lines[i], pseries[i]));
        }
        
        ret= new MultiLineChart(series);
        
        break;
      case FIELD:
        Map<String, List<Point>> mseries= new HashMap<String, List<Point>>();
        
        for(Map<String, Object> row : rows)
        {
          double t= Double.parseDouble(row.get("rec_time").toString());
          Object odisc= row.get(lines[0]);
          String disc= odisc != null ? odisc.toString() : "";
          Object o= row.get(lines[1]);
          
          if( o != null )
          {
            Point p= new Point(t, Double.parseDouble(o.toString()));

            List<Point> points = mseries.get(disc);
            if( points == null )
            {
              points = new ArrayList<Point>(rows.size());
              mseries.put(disc, points);
            }

            points.add(p);
          }
        }

        List<DataSeries<Point>> seriesField = new ArrayList<DataSeries<Point>>(mseries.size());
        for(Entry<String, List<Point>> s : mseries.entrySet())
        {
          seriesField.add(new DataSeries<Point>(s.getKey(), s.getValue()));
        }

        ret= new MultiLineChart(seriesField);
        
        break;
    }
    
    return ret;
  }
}
