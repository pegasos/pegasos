package at.pegasos.server.frontback.data.notification;

import javax.persistence.*;

@Entity
@Table(name="notification_actions")
public class NotificationAction {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="type")
  private NotificationType type;

  public String description;

  public String action;

  @Column(name = "sort")
  public int order;
}
