package at.pegasos.server.frontback.util;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
  /**
   * Send an email
   * @param email recipient
   * @param firstname first name of the recipient
   * @param lastname last name of the recipient
   * @param subject subject / title of the email
   * @param msgBody body of the mail
   * @throws UnsupportedEncodingException when something fails ...
   * @throws MessagingException when something fails ...
   */
  public static void sendMail(String email, String firstname, String lastname, String subject, String msgBody) throws UnsupportedEncodingException, MessagingException
  {
    String to= email;
    // TODO: fill in correct email
    String from="sender@example.com";
             
    Properties props= new Properties();
    Session session= Session.getDefaultInstance(props, null);
    
    Message msg= new MimeMessage(session);
    msg.setFrom(new InternetAddress(from, "NoReply"));
    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to, firstname + " " + lastname));
    msg.setSubject(subject);
    msg.setText(msgBody);
    Transport.send(msg);
  }
}
