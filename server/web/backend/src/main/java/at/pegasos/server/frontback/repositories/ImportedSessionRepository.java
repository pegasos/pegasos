package at.pegasos.server.frontback.repositories;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

import at.pegasos.server.frontback.dbdata.*;

@Repository
@Transactional(readOnly = true)
public interface ImportedSessionRepository extends JpaRepository<ImportedSession, Long> {

  @Query("FROM ImportedSession i WHERE i.sessionId = :sessionId and type = :type")
  Collection<ImportedSession> findBySessionAndType(long sessionId, String type);
}
