package at.pegasos.server.frontback.data;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class UserValue {
  
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name="ID")
  public long id;
  
  @OneToOne()
  @JoinColumn(name="value_id", insertable= false, updatable = false)
  private UserValueInfo value;
  
  @Column(name="value_id")
  private long value_id;
  
  public long user_id;
  
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_start;
  
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_end;
  
  public void setValue(UserValueInfo value)
  {
    this.value= value;
    value_id= value.id;
  }
  
  public UserValueInfo getValue()
  {
    return this.value;
  }
  
  public abstract double getDoubleValue();
}
