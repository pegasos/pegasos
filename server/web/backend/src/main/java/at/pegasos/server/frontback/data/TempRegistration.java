package at.pegasos.server.frontback.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="temp_registration")
public class TempRegistration {
  
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;
  
  public String club_code;
  
  public String email;
  
  public String forename;
  
  public String surname;
  
  public String password_2896;
  
  public String salt_7863;
  
  public String hash;
  
  public long crtime = 0;  
}
