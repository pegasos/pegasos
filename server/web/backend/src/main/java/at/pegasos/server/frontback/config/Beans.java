package at.pegasos.server.frontback.config;

import at.pegasos.server.frontback.tasks.*;
import org.springframework.context.annotation.*;

@Configuration
public class Beans {
  @Bean public TaskService taskService()
  {
    return new TaskService();
  }
}
