package at.pegasos.server.frontback.data;

import java.util.List;

/**
 * A simple report containing sessions and corresponding data. For each session the data or null will be listed in the data series.
 * `users` holds the info about persons present in the report. `sessions` includes info which person the session "belongs to".
 */
public class ReportSessionData extends Report {
  
  @Override
  public String toString()
  {
    return "ReportSessionData [sessions=" + sessions + ", data=" + data + "]";
    // return "ReportSessionData [sessions=" + sessions != null ? Arrays.toString(sessions.toArray(new Session[0])) : "'null'" + ", data=" + data != null ? Arrays.toString(data) : "'null" + "]";
  }

  public List<SessionInfo> sessions;
  
  public List<SessionInfo> getSessions()
  {
    return sessions;
  }
  
  public MetricData[] data;
  
  public MetricData[] getData()
  {
    return data;
  }
}
