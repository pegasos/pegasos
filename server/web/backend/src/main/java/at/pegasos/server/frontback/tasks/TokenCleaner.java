package at.pegasos.server.frontback.tasks;

import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.scheduling.annotation.*;
import org.springframework.security.oauth2.server.authorization.*;
import org.springframework.stereotype.*;

import java.util.*;

@Component @Slf4j
public class TokenCleaner {

  @Autowired
  OAuth2AuthorizationService service;

  @Autowired
  TokenRepository tokenRepository;

  // @Scheduled(fixedRate = 1000 * 60 * 5)
  @Scheduled(cron = "0 0 * * * *")
  public void reportCurrentTime()
  {
    log.info("Starting expired token removal");
    log.debug("Tokens: {}", tokenRepository.getTokenExpiryAfter(new Date()));
    for(JdbcToken token : tokenRepository.getTokenExpiryAfter(new Date()))
    {
      service.remove(service.findById(token.id));
    }
    log.debug("Tokens afer removal: {}", tokenRepository.getTokenExpiryAfter(new Date()));
  }
}
