package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

@Repository
@Transactional(readOnly = true)
public interface TokenRepository extends JpaRepository<JdbcToken, Long> {

  @Query("FROM JdbcToken t WHERE t.accessTokenExpiresAt < :date")
  Collection<JdbcToken> getTokenExpiryAfter(Date date);

  // public Collection<JdbcToken> find
}
