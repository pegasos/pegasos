package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.controller.ReportController.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.repositories.*;

import java.util.*;
import java.util.stream.*;

public abstract class MetricBasedAccumulatedReportGenerator extends MetricBasedReportGenerator {

  private GroupBy grouping;

  private Aggregate aggregate;

  /**
   * Dates for which data exists
   */
  private final Set<Long> dates= new HashSet<Long>();
  
  /**
   * users for which data exists
   */
  protected Set<Long> users_with_data= new HashSet<Long>();
  
  protected Map<String, Double> values= new HashMap<String, Double>();

  protected ReportUserDatesSingle report;

  public MetricBasedAccumulatedReportGenerator(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names)
  {
    super(metrirep, metrrep, sirepo, metric_names);
  }
  
  public MetricBasedAccumulatedReportGenerator(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names, List<SessionTypeFilter> filter)
  {
    super(metrirep, metrrep, sirepo, metric_names, filter);
  }
  
  public Report generateReport()
  {
    fetchValues();

    final Map<String, List<Double>> valuesMean = new HashMap<>();

    logger.debug("Fetching metrics");
    // Get all the values we have for this report
    List<Metric> allMetrics = getMetrics();
    for(Metric m : allMetrics)
    {
      // when we do not group we do not have a date. i.e. date = 0
      long date = ReportUtil.groupedDate(m.session.starttime, grouping, m);

      dates.add(date);
      users_with_data.add(m.session.userId);
      
      String key= getKey(m, date);
      Double val= values.get(key);
      if( val == null )
      {
        values.put(key, m.value);
      }
      else
      {
        switch(aggregate)
        {
          case MAX:
            values.put(key, Math.max(val, m.value));
            break;

          case MIN:
            values.put(key, Math.min(val, m.value));
            break;

          case SUM:
            values.put(key, val + m.value);
            break;

          case MEAN:
            values.put(key, null);
            // enusre that we have a list present for 'key'
            valuesMean.computeIfAbsent(key, k -> new ArrayList<>());
            // Add current value to the values for this entry
            valuesMean.get(key).add(m.value);
            break;

          case FIRST:
            // do nothing
            break;

          case LAST:
            values.put(key, m.value);

          default:
            throw new IllegalArgumentException("Aggregation type " + aggregate + " not implemented");
        }
      }
    }

    logger.debug("Dates: {}", dates);

    if( aggregate == Aggregate.MEAN )
    {
      for(Map.Entry<String, List<Double>> kv : valuesMean.entrySet())
      {
        values.put(kv.getKey(), kv.getValue().stream().mapToDouble(v -> v).average().orElse(0));
      }
    }

    report = new ReportUserDatesSingle();

    report.dates= dates.stream().sorted().map(d -> {
      Calendar c= Calendar.getInstance();
      c.setTimeInMillis(d);
      return c;
    }).collect(Collectors.toList());
    report.users= new ArrayList<Long>(users_with_data);
    
    finishReport();
    
    logger.debug("finished report " + report);
    
    return report;
  }

  protected abstract List<Metric> getMetrics();

  protected abstract void finishReport();

  public void setAggregate(Aggregate aggr)
  {
    this.aggregate= aggr;
  }
  
  public void setGroupBy(GroupBy grouping)
  {
    this.grouping= grouping;
  }
  
  /**
   * Get the key for the metric.
   * @param m Metric
   * @param date date
   * @return key
   */
  public abstract String getKey(Metric m, long date);
}
