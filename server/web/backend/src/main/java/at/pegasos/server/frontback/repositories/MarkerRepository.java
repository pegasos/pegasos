package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.dbdata.Marker;

@Repository
@Transactional(readOnly = true)
public interface MarkerRepository extends JpaRepository<Marker, Long>  {
  
  List<Marker> findBySessionIdOrderByRecTimeAsc(long sessionId);
}
