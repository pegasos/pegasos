package at.pegasos.server.frontback.tasks;

import java.util.*;

public interface Task {
  float completion();

  Collection<String> messages();

  boolean completed();
}
