package at.pegasos.server.frontback.data;

import java.util.*;
import java.util.stream.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import at.pegasos.server.frontback.dbdata.*;
import lombok.*;
import org.hibernate.annotations.*;

@Entity
@Table(name="teams_groups")
@ToString
public class GroupWithUsers {
  
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue
  @Column(name="id")
  public long groupid;

  /**
   * Name of the group
   */
  @Column(name="name")
  public String name;

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch= FetchType.LAZY)
  @JoinColumn(name="teamid")
  private Team team;

  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
  @WhereJoinTable(clause="deleted = false")
  @JoinTable(
      name = "teams_memberships", 
      joinColumns = { @JoinColumn(name = "groupid") }, 
      inverseJoinColumns = { @JoinColumn(name = "userid") }
  )
  private final Set<TUser> members = new HashSet<>();

  public Set<TUser> getMembers()
  {
    return members;
  }
}
