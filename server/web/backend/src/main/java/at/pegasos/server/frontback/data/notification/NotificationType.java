package at.pegasos.server.frontback.data.notification;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="notification_types")
public class NotificationType {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public int id;

  public String type;

  @OneToMany(mappedBy = "type")
  @OrderBy("order")
  public List<NotificationAction> actions;
}
