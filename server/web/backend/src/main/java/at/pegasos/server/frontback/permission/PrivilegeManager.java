package at.pegasos.server.frontback.permission;

import at.pegasos.server.frontback.dbdata.*;

import java.security.Principal;

public interface PrivilegeManager {
  public boolean hasRole(Principal p, String rolename);
  public boolean hasRole(long user_id, String rolename);
  public boolean hasRole(long user_id, long role_id);
  
  public boolean hasPrivilege(Principal p, String privilege);
  /**
   * Check whether the authorised user has the privilege for the user
   * @param p the logged in user
   * @param privilege name of the privilege
   * @param user_id other user i.e. the user for which access is asked
   * @return true if p has privilege for user_id
   */
  public boolean hasPrivilege(Principal p, String privilege, long user_id);

  boolean hasPrivilegeActivity(Principal p, String privilege, long sessionId);
  boolean hasPrivilegeActivity(Principal p, String privilege, Session session);

  boolean hasPrivilegeUser(Principal p, String privilege, long userid);
}
