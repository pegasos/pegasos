package at.pegasos.server.frontback.controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InsufficientPrivilegesException extends RuntimeException {
}
