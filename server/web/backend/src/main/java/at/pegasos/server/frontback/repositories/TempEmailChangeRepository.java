package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TempEmailChange;

@Repository
@Transactional(readOnly = true)
public interface TempEmailChangeRepository extends JpaRepository<TempEmailChange, Long> {
  
  List<TempEmailChange> findByOldemail(String email);
  
  List<TempEmailChange> findByHash(String hash);
  
  @Transactional
  @Modifying
  @Query("DELETE FROM TempEmailChange t WHERE t.crtime < :date")
  int removeOlderThan(@Param("date") long date);
}
