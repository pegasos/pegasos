package at.pegasos.server.frontback.data.chart;

import java.util.List;

import at.pegasos.server.frontback.data.Type;

/**
 * Data for a simple chart. Simple in this case means just one data series such as a simple line
 * chart or a poincare chart.
 */
public class SimpleChart extends ChartWithPoints {
  private final List<Point> points;
  
  public SimpleChart(Type type, List<Point> points)
  {
    super(type);
    this.points = points;
  }
  
  public Type getType()
  {
    return type;
  }
  
  public List<Point> getPoints()
  {
    return points;
  }
}
