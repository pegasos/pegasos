package at.pegasos.server.frontback.auth;

import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.security.*;
import java.security.interfaces.*;
import java.security.spec.*;
import java.util.*;

import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.dao.DataAccessException;

public class Security {

  public static String sha1(String input) throws NoSuchAlgorithmException
  {
    MessageDigest mDigest= MessageDigest.getInstance("SHA1");
    
    byte[] result= mDigest.digest(input.getBytes());
    StringBuffer sb= new StringBuffer();
    for(int i= 0; i < result.length; i++)
    {
      sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
    }
    
    return sb.toString();
  }
  
  private final static String charsRH= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  private final static int charsRH_L;
  static {
    charsRH_L= charsRH.length();
  }
  
  public static String generateRandomHash() throws NoSuchAlgorithmException
  {
    String randString= "";
    
    for(int i= 0; i < charsRH_L; i++)
    {
      randString+= charsRH.charAt((int) (Math.random() * (charsRH_L - 1)));
    }
    
    return sha1(sha1(randString));
  }
  
  private static final char chars[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
  
  public static String salt(int salt_length)
  {
    String salt = "";
    for (int i = 0; i < salt_length; i++)
    {
      int rand = (int) (Math.random() * 42);
      salt+= chars[rand];
    }
    
    return salt;
  }
  
  public static class Password {
    public String pw_hash;
    public String salt;
  }
  
  public static Password generatePassword(String password) throws NoSuchAlgorithmException, DataAccessException
  {
    String salt= Security.salt(8);
    
    String password_hash= Security.sha1(salt + Security.sha1(password + salt));
    
    Password ret= new Password();
    ret.pw_hash= password_hash;
    ret.salt= salt;
    
    return ret;
  }
  
  /**
   * Check whether the given password is the password of the user. DO NOT EXPOSE THIS TO THE PUBLIC
   * @param salt salt in the database
   * @param password password of the user in the data base
   * @param match_password password to be checked for the match
   * @return true if password matches
   * @throws NoSuchAlgorithmException
   */
  public static boolean checkPassword(String salt, String password, String match_password) throws NoSuchAlgorithmException
  {
    String pwHash= Security.sha1(salt + Security.sha1(match_password + salt));
    return pwHash.equals(password);
  }
  
  /**
   * Check whether the given password is the password of the user. DO NOT EXPOSE THIS TO THE PUBLIC
   * @param user
   * @param match_password password to be checked
   * @return true if password matches
   * @throws NoSuchAlgorithmException
   */
  public static boolean checkPassword(AuthorisationUserDetails user, String match_password) throws NoSuchAlgorithmException
  {
    return checkPassword(user.getSalt(), user.getDbPassword(), match_password);
  }
  public static RSAKey getRsa() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException
  {
    if( Files.exists(Paths.get("private.key")) && Files.exists(Paths.get("public.key")) ) {
      KeyPair keyPair = LoadKeyPair("public.key", "private.key");
      RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
      RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
      // @formatter:off
      return new RSAKey.Builder(publicKey)
              .privateKey(privateKey)
              // .keyID(UUID.randomUUID().toString())
              .keyID("pegasos-uuid")
              .build();
      // @formatter:on
    } else {
      KeyPair keyPair = generateRsaKey();
      RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
      RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
      SaveKeyPair(keyPair, "public.key", "private.key");
      // @formatter:off
      return new RSAKey.Builder(publicKey)
              .privateKey(privateKey)
              .keyID(UUID.randomUUID().toString())
              .build();
      // @formatter:on
    }
  }

  private static KeyPair generateRsaKey()
  {
    KeyPair keyPair;
    try {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
      keyPairGenerator.initialize(4096);
      keyPair = keyPairGenerator.generateKeyPair();
    } catch (Exception ex) {
      throw new IllegalStateException(ex);
    }
    return keyPair;
  }

  /**
   * Saves both public key and private  key to file names specified
   * @param fnpub  file name of public key
   * @param fnpri  file name of private key
   * @throws IOException on write errors?
   */
  private static void SaveKeyPair(KeyPair keyPair, String fnpub,String fnpri) throws IOException
  {
    // save public key
    X509EncodedKeySpec x509EncodedKeySpec= new X509EncodedKeySpec(keyPair.getPublic().getEncoded());
    FileOutputStream fos= new FileOutputStream(Paths.get(fnpub).toFile());
    fos.write(x509EncodedKeySpec.getEncoded());
    fos.close();

    // save private Key
    PKCS8EncodedKeySpec pkcs8EncodedKeySpec= new PKCS8EncodedKeySpec(keyPair.getPrivate().getEncoded());
    fos= new FileOutputStream(Paths.get(fnpri).toFile());
    fos.write(pkcs8EncodedKeySpec.getEncoded());
    fos.close();
  }

  private static KeyPair LoadKeyPair(String fnpub,String fnpri) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    return new KeyPair(readPubKeyFromFile(fnpub), readPrivKeyFromFile(fnpri));
  }

  private static PublicKey readPubKeyFromFile(String keyFileName) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException
  {
    InputStream in = Files.newInputStream(Paths.get(keyFileName));

    // now read the actual key from the file
    ByteArrayOutputStream buffer= new ByteArrayOutputStream(2048);

    // Just to be safe we read it in loop
    int nRead;
    byte[] data= new byte[2048];

    while( (nRead= in.read(data, 0, data.length)) != -1 )
    {
      buffer.write(data, 0, nRead);
    }
    in.close();

    byte[] keyBytes= buffer.toByteArray();

    X509EncodedKeySpec keySpec= new X509EncodedKeySpec(keyBytes);
    KeyFactory fact= KeyFactory.getInstance("RSA");

    return fact.generatePublic(keySpec);
  }

  private static PrivateKey readPrivKeyFromFile(String keyFileName) throws IOException
  {
    try (InputStream in = Files.newInputStream(Paths.get(keyFileName)))
    {
      // now read the actual key from the file
      ByteArrayOutputStream buffer = new ByteArrayOutputStream(2048);

      // Just to be safe we read it in loop
      int nRead;
      byte[] data = new byte[2048];

      while ((nRead = in.read(data, 0, data.length)) != -1)
      {
        buffer.write(data, 0, nRead);
      }

      byte[] keyBytes = buffer.toByteArray();

      PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
      KeyFactory fact = KeyFactory.getInstance("RSA");
      return fact.generatePrivate(keySpec);
    }
    catch (Exception e)
    {
      // throw new RuntimeException("Spurious serialisation error", e);
      System.err.println(e.getMessage());
      e.printStackTrace();
      throw new RuntimeException("Spurious serialisation error", e);
    }
  }
}
