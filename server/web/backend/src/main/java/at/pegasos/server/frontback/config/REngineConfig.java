package at.pegasos.server.frontback.config;

import lombok.extern.slf4j.*;
import org.rosuda.REngine.*;
import org.springframework.context.annotation.*;

import java.lang.reflect.*;

@Configuration
@Slf4j
public class REngineConfig {

  @Bean public REngine createEngine()
  {
    try
    {
      return REngine.engineForClass("org.rosuda.REngine.JRI.JRIEngine", new String[] {"--no-save"}, new REngineStdOutput(), false);
    }
    catch( ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e )
    {
      log.error("Could not create R engine");
      log.error("Error during R engine creation", e);
      return null;
    }
  }
}
