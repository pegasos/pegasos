package at.pegasos.server.frontback.reports;

import lombok.*;

@ToString
public class SessionTypeFilter {
  public Integer param0;
  public Integer param1;
  public Integer param2;
  public int len;

  public SessionTypeFilter()
  {

  }
  
  public SessionTypeFilter(Integer param0)
  {
    this.param0= param0;
    this.param1= null;
    this.param2= null;
    this.len= 1;
  }
  
  public SessionTypeFilter(Integer param0, Integer param1)
  {
    this.param0= param0;
    this.param1= param1;
    this.param2= null;
    this.len= 2;
  }
  
  public SessionTypeFilter(Integer param0, Integer param1, Integer param2)
  {
    this.param0= param0;
    this.param1= param1;
    this.param2= param2;
    this.len= 3;
  }
}