package at.pegasos.server.frontback.data.chart;

import java.util.List;

import at.pegasos.server.frontback.data.DataSeries;
import at.pegasos.server.frontback.data.Type;
import lombok.*;

@ToString
public class MultiLineChart extends ChartWithPoints {
  
  private final List<DataSeries<Point>> data;

  public MultiLineChart(List<DataSeries<Point>> data)
  {
    super(Type.MultiLine);
    this.data= data;
  }
  
  public List<DataSeries<Point>> getDataSeries()
  {
    return data;
  }
}
