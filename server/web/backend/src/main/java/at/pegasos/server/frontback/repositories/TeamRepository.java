package at.pegasos.server.frontback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.pegasos.server.frontback.data.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
  
}
