package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "user")
public class UserDetails {
  
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  public long id;
  
  /**
   * Firstname of the user
   */
  @Column(length= 255, nullable= false, unique= false, name="forename")
  public String firstname;
  
  /**
   * Lastname of the user
   */
  @Column(length= 255, nullable= false, unique= false, name="surname")
  public String lastname;
  
  @Column(length= 255, nullable= false, unique= true)
  public String email;

  public String language;

  public long birthday;
  
  public double height;
  
  public double weight;
  
  public int gender;
  
  @Column(name="data1")
  public Double vo2max;
  
  @Column(name="data2")
  public Long hrmax;
  
  @Column(name="data4")
  public Long hrmin;

  @Column(name="data3")
  public Long fpcalib;

  @Column(name="data1_crtime")
  public Long vo2max_time;
  
  @Column(name="data2_crtime")
  public Long hrmax_time;
  
  @Column(name="data4_crtime")
  public Long hrmin_time;

  @Column(name="data3_crtime")
  public Long fpcalib_time;
}
