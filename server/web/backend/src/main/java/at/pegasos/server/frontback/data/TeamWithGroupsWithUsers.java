package at.pegasos.server.frontback.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.*;
import org.hibernate.annotations.Where;

@Entity
@Table(name="teams")
@ToString
public class TeamWithGroupsWithUsers {
  
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public long teamid;
  
  /**
   * Name of the team
   */
  public String name;

  @ManyToMany
  @Where(clause="deleted = false")
  @JoinTable(
      name = "manages_team",
      joinColumns = { @JoinColumn(name = "teamid") },
      inverseJoinColumns = { @JoinColumn(name = "userid") }
  )
  public Set<TUser> captains= new HashSet<TUser>();
  
  @OneToMany(mappedBy = "team", cascade = { CascadeType.ALL })
  @Where(clause="deleted = false")
  public List<GroupWithUsers> groups = new ArrayList<GroupWithUsers>(0);
}
