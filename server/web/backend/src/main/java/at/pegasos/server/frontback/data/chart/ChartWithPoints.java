package at.pegasos.server.frontback.data.chart;

import at.pegasos.server.frontback.data.Chart;
import at.pegasos.server.frontback.data.Type;
import lombok.*;

public abstract class ChartWithPoints extends Chart {
  @ToString
  public static class Point {
    private double x;
    private double y;
    
    public Point(double x, double y)
    {
      this.x= x;
      this.y= y;
    }
    
    public double getX()
    {
      return x;
    }
    
    public void setX(double x)
    {
      this.x= x;
    }
    
    public double getY()
    {
      return y;
    }
    
    public void setY(double y)
    {
      this.y= y;
    }
  }
  

  public ChartWithPoints(Type type)
  {
    super(type);
  }
  
}
