package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TempRegistration;

@Repository
@Transactional(readOnly = true)
public interface TempRegistrationRepository extends JpaRepository<TempRegistration, Long> {
  
  List<TempRegistration> findByEmail(String email);
  
  List<TempRegistration> findByHash(String hash);
  
  @Transactional
  @Modifying
  @Query("DELETE FROM TempRegistration t WHERE t.crtime < :date")
  int removeOlderThan(@Param("date") long date);
}
