package at.pegasos.server.frontback.data;

import java.util.*;
import java.util.stream.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import at.pegasos.server.frontback.dbdata.*;
import org.hibernate.annotations.*;

@Entity
@Table(name="teams")
public class TeamWithUsers {
  
  /**
   * ID of the team
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  public long teamid;
  
  public int type;
  
  /**
   * Name of the team
   */
  public String name;
  
  public long crtime = 0;
  
  public boolean deleted = false;

  @ManyToMany
  @Where(clause="deleted = false")
  @JoinTable(
      name = "manages_team",
      joinColumns = { @JoinColumn(name = "teamid") },
      inverseJoinColumns = { @JoinColumn(name = "userid") }
  )
  public Set<TUser> captains= new HashSet<TUser>();
  
  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
  @WhereJoinTable(clause="deleted = false")
  @JoinTable(
      name = "teams_memberships", 
      joinColumns = { @JoinColumn(name = "teamid") }, 
      inverseJoinColumns = { @JoinColumn(name = "userid") }
  )
  private final Set<TUser> members = new HashSet<>();

  public Set<TUser> getMembers()
  {
    return members;
  }
}
