package at.pegasos.server.frontback.reports;

import java.util.*;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.Metric;
import at.pegasos.server.frontback.repositories.*;

/**
 * A report in which all metrics of a given type are accumulated for each parameter.
 */
public class MetricBasedAccumulatedReportGeneratorParam extends MetricBasedAccumulatedReportGenerator {
  
  /**
   * metric-param combinations in this report
   */
  private final Set<String> metric_param= new HashSet<String>();

  public MetricBasedAccumulatedReportGeneratorParam(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names, List<SessionTypeFilter> filter)
  {
    super(metrirep, metrrep, sirepo, metric_names, filter);
  }

  @Override protected List<Metric> getMetrics()
  {
    return metrrep.findBySessionIdInAndMetricIdIn(data.session_ids, data.metrics);
  }

  public MetricBasedAccumulatedReportGeneratorParam(MetricInfoRepository metrirep, MetricRepository metrrep, SessionInfoRepository sirepo, String[] metric_names)
  {
    super(metrirep, metrrep, sirepo, metric_names);
  }

  @Override
  public String getKey(Metric m, long date)
  {
    String k= m.metricId + "-" + m.param;
    metric_param.add(k);
    return k + "-" + m.session.userId + "-" + date;
  }
  
  @Override
  protected void finishReport()
  {
    report.series= new ArrayList<DataSeries<Double>>(metric_param.size() * report.users.size());
    
    // Go over all metrics
    for(String mp : metric_param)
    {
      String[] mps = mp.split("-");
      long metricId= Long.parseLong(mps[0]);
      String param= mps[1];
      
      String metric= data.metric_map.get(metricId);
      
      // For each user
      for(Long userId : users_with_data)
      {
        // Create a series metric-user
        List<Double> data= new ArrayList<Double>(report.dates.size());
        for(Calendar d : report.dates)
        {
          data.add(values.get(metricId + "-" + param + "-" + userId + "-" + d.getTimeInMillis()));
        }
        report.series.add(new DataSeries<Double>(metric + "#" + param + "#" + userId, data));
      }
    }
  }
}
