package at.pegasos.server.frontback.auth;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.oauth2.server.authorization.authentication.*;

public class PegasosAuthenticationManager implements AuthenticationManager, AuthenticationProvider {
  static final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();
  private static final Logger logger = LoggerFactory.getLogger(PegasosAuthenticationManager.class);

  static
  {
    AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
  }

  @Autowired UserDetailsService userservice;

  public Authentication authenticate(Authentication auth) throws AuthenticationException
  {
    // logger.error("authenticate: " + auth.getName() + " " + auth.getCredentials());

    try
    {
      AuthorisationUserDetails u = (AuthorisationUserDetails) userservice.loadUserByUsername(auth.getName());

      logger.error("User: {} {}", u, auth.getCredentials());

      try
      {
        String pwHash = Security.sha1(u.getSalt() + Security.sha1(auth.getCredentials() + u.getSalt()));
        if( pwHash.equals(u.getDbPassword()) )
        {
          logger.error("Authenticated");
          return new UsernamePasswordAuthenticationToken(auth.getName(), auth.getCredentials(), AUTHORITIES);
        }
        else
          logger.error("Bad credentials for {}", u);
      }
      catch( NoSuchAlgorithmException e )
      {
        logger.error("NoSuchAlgorithm!");
        e.printStackTrace();
      }
      throw new CustomBadCredentialsException(auth.getName());
    }
    catch( UsernameNotFoundException e )
    {
      logger.error("User '{}' not found", auth.getName());
      throw new UsernameNotFoundException("");
      // throw new BadCredentialsException(e.getMessage());
    }
  }

  @Override public boolean supports(Class<?> authentication)
  {
    logger.error("Supports?: " + authentication + " " + authentication.equals(UsernamePasswordAuthenticationToken.class));
    // return false;
    return authentication.equals(OAuth2AuthorizationCodeRequestAuthenticationToken.class) || authentication.equals(
        UsernamePasswordAuthenticationToken.class);
  }
}