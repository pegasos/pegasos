package at.pegasos.server.frontback.tasks;

import java.util.*;

public class TaskService {
  private final Map<Long, Task> tasks = new HashMap<>();

  private long currentID = 1;

  public synchronized long addTask(Task task)
  {
    tasks.put(currentID, task);
    if( currentID == Long.MAX_VALUE )
    {
      long tmp = currentID;
      currentID = 1;
      return tmp;
    }
    return currentID++;
  }

  public void clean()
  {
    for(Map.Entry<Long, Task> t : tasks.entrySet())
    {
      if( t.getValue().completed() )
      {
        tasks.remove(t.getKey());
      }
    }
  }

  public Task getTask(long taskId)
  {
    return tasks.get(taskId);
  }
}
