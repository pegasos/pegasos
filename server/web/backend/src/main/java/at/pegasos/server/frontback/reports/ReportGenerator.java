package at.pegasos.server.frontback.reports;

import java.util.Calendar;
import java.util.List;

import at.pegasos.server.frontback.data.Report;
import at.pegasos.server.frontback.data.User;

public abstract class ReportGenerator {
  protected User me;
  protected List<Long> users;
  protected Calendar start;
  protected Calendar end;
  
  abstract public Report generateReport();
  
  public ReportGenerator()
  {
    start= Calendar.getInstance();
    start.setTimeInMillis(System.currentTimeMillis());
    start.add(Calendar.WEEK_OF_YEAR, -2);
    
    end= Calendar.getInstance();
    end.setTimeInMillis(System.currentTimeMillis());
  }
  
  public void setStart(Calendar start)
  {
    this.start= start;
  }
  
  public void setEnd(Calendar end)
  {
    this.end= end;
  }
  
  public void setMe(User me)
  {
    this.me= me;
  }
  
  public void setUsers(List<Long> users)
  {
    this.users= users;
  }
}
