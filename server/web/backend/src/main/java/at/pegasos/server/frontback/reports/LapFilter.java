package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.data.*;
import lombok.*;

@ToString public class LapFilter {
  public LapFilterType type;
  public long value;
  public Comparator comp;

  public LapFilter(LapFilterType type, long value, Comparator comp)
  {
    this.type = type;
    this.value = value;
    this.comp = comp;
  }

  public LapFilter() {}
}
