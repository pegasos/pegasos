package at.pegasos.server.frontback.repositories;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.SessionInfo;

@Repository
@Transactional(readOnly = true)
public interface SessionInfoRepository extends JpaRepository<SessionInfo, Long> {

  List<SessionInfo> findByUserIdAndStarttimeAfter(long userid, long starttime);

  List<SessionInfo> findByStarttimeAfter(long starttime);

  List<SessionInfo> findByUserIdIn(Collection<Integer> userids);

  List<SessionInfo> findByUserIdInAndStarttimeAfter(List<Long> users, long starttime);

  @Query("select s from #{#entityName} s where s.userId in :users AND s.starttime >= :start AND s.starttime <= :end ORDER BY starttime ASC")
  List<SessionInfo> findByUserIdInAndTime(@Param("users") List<Long> users, @Param("start") long start, @Param("end") long end);

  @Query("SELECT s from SessionInfo s WHERE s.session_id IN :sessionIds ORDER BY s.starttime ASC")
  List<SessionInfo> findAllByIdInOrderByStarttime(Set<Long> sessionIds);
  /*
  @Query("select s from #{#entityName} s where s.userId in :users AND s.starttime >= :time AND s.starttime <= :time AND s.param0 = :param0")
  List<SessionInfo> findByUserIdInTimeSessionType(@Param("users") List<Long> users, @Param("time") long time, @Param("param0") int param0);
  
  @Query("select s from #{#entityName} s where s.userId in :users AND s.starttime >= :time AND s.starttime <= :time AND s.param0 = :param0 AND s.param1 = :param1")
  List<SessionInfo> findByUserIdInTimeSessionType(@Param("users") List<Long> users, @Param("time") long time, @Param("param0") int param0, @Param("param0") int param1);
  
  @Query("select s from #{#entityName} s where s.userId in :users AND s.starttime >= :time AND s.starttime <= :time AND s.param0 = :param0")
  List<SessionInfo> findByUserIdInTimeSessionType(@Param("users") List<Long> users, @Param("time") long time, @Param("param0") int param0, @Param("param0") int param1, @Param("param2") int param2);
  */
}
