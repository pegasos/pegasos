package at.pegasos.server.frontback.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.UserDetails;

@Repository
@Transactional(readOnly = true)
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
  
  List<UserDetails> findByEmail(String email);
  
  Optional<UserDetails> findById(Long id);
}
