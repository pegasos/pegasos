package at.pegasos.server.frontback.dbdata;

public class TableDescription {
  public final String table;
  public final String[] columns;
  public final boolean sensorNr;

  public TableDescription(String table, String... columns) {
    this.table = table;
    this.columns = columns;
    this.sensorNr = false;

  }

  public TableDescription(String table, String[] columns, boolean sensorNr) {
    this.table = table;
    this.columns = columns;
    this.sensorNr = sensorNr;
  }
}
