package at.pegasos.server.frontback.data;

import java.io.Serializable;
import java.util.List;

public class ValueChanges implements Serializable {
  private static final long serialVersionUID= 3288639985862895514L;

  public static class Change implements Serializable {
    private static final long serialVersionUID= -6517257245807509070L;

    public String name;
    public String value;
  }
  
  public List<ValueChanges.Change> changes;
}
// @RequestBody Filter body