package at.pegasos.server.frontback.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="temp_changeemail")
public class TempEmailChange {
  /**
   * ID
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;
  
  public long userid;
  
  public String oldemail;
  
  public String newemail;
  
  public String hash;
  
  public long crtime = 0;  
}
