package at.pegasos.server.frontback.exports;

import java.io.*;
import java.nio.file.*;

public interface SessionExport {
  /**
   * Get the name of the exported file. This is the name how the downloaded file should be named on the downloading client
   * @return file name
   */
  String getFileName();

  /**
   * Get the exported file
   * @return path to exported file
   */
  Path getFile() throws IOException;

  /**
   * Get the content-type / file-type of the export. Can also be determined using 'Files.probeContentType(file)'
   * @return content type
   */
  String getContentType();
}
