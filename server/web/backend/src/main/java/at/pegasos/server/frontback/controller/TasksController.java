package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.tasks.*;
import io.swagger.v3.oas.annotations.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;

@Slf4j @RestController
@RequestMapping("/v1/tasks")
public class TasksController {

  @Autowired private TaskService taskService;
  @Autowired private PrivilegeManager priv;

  @Operation(summary = "Get the status of a task")
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/status/{taskid}")
  public TaskStatus getTaskStatus(
      Principal p, @PathVariable("taskid") long taskId)
  {
    Task task = taskService.getTask(taskId);
    if( task == null )
      return null;

    return new TaskStatus(taskId, task);
  }
}
