package at.pegasos.server.frontback.charts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import at.pegasos.server.frontback.data.Type;
import at.pegasos.server.frontback.data.chart.ChartWithPoints;
import at.pegasos.server.frontback.data.chart.SimpleChart;

/**
 * Internal type representing information about an available chart. This type is not exposed. Any
 * relevant information contained in this type should also be present in the frontend and accessible
 * via the unique `name`.
 */
public class DBChartSimple extends DBChart {
  private final String tbl;
  private final String col;
  
  private final String query;
  
  public DBChartSimple(String name, Type type, String tbl, String col)
  {
    super(name, type);
    this.tbl = tbl;
    this.col= col;

    query = "SELECT rec_time, " + col + " from " + tbl + " where session_id = ? ORDER BY rec_time ASC";
  }
  
  @Override
  public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    Map<String, Object> t= jdbcTemplate
        .queryForList("SELECT count(" + col + ") as c from " + tbl + " where session_id = ?", new Object[] {session}).get(0);

    return ((Long) t.get("c")) > 0;
  }

  @Override
  public String getDataQuery()
  {
    return query;
  }
  
  @Override
  public ChartWithPoints newResult(List<Map<String, Object>> rows)
  {
    ArrayList<SimpleChart.Point> points= new ArrayList<SimpleChart.Point>(rows.size());
    
    for(Map<String, Object> row : rows)
    {
      if( row.get(col) != null )
      {
        SimpleChart.Point p= new SimpleChart.Point(Double.parseDouble(row.get("rec_time").toString()),
            Double.parseDouble(row.get(col).toString()));
        points.add(p);
      }
    }
    
    return new SimpleChart(type, points);
  }
}
