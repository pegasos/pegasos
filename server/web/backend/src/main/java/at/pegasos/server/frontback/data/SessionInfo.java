package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="session")
public class SessionInfo {

  @Override
  public String toString()
  {
    return "SessionInfo [userId=" + userId + ", session_id=" + session_id + ", param0=" + param0 + ", param1=" + param1 + ", param2="
        + param2 + ", starttime=" + starttime + ", last_update=" + last_update + ", endtime=" + endtime + "]";
  }

  /**
   * ID of the user who performed this activity
   */
  @Column(name="userid")
  public long userId;
  
  /**
   * ID of the session
   */
  @GeneratedValue
  @Id
  @Column(name="session_id")
  public long session_id;
  
  @Column(name="activity")
  public int param0;
  public int param1;
  public int param2;
  
  /**
   * Timestamp when the activity was started
   */
  public long starttime;
  
  public long last_update;
  
  public long endtime;
}
