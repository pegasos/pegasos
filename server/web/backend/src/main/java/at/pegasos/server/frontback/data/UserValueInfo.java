package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "value")
public class UserValueInfo {
  
  @Override
  public String toString()
  {
    return "UserValue [id=" + id + ", name=" + name + ", type=" + type + "]";
  }
  
  @GeneratedValue
  @Id
  @Column(name= "ID")
  public long id;
  
  /*public enum Type {
    INTEGER("int"), DECIMAL("dec"), TEXT("text");
    
    private String t;
    
    private Type(String t)
    {
      this.t= t;
    }
  }*/
  public enum ValueType {
    INTEGER, DECIMAL, TEXT;
  }

  public String name;
  
  private String type;
  
  public ValueType getType()
  {
    if( type.equals("int") )
      return ValueType.INTEGER;
    else if( type.equals("dec") )
      return ValueType.DECIMAL;
    else if( type.equals("text") )
      return ValueType.TEXT;
    else
      throw new IllegalStateException();
  }
}
