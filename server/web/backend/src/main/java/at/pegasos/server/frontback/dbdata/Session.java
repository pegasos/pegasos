package at.pegasos.server.frontback.dbdata;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import at.pegasos.server.frontback.data.*;
import org.hibernate.annotations.*;

@Entity
@Table(name="session")
public class Session {

  @Override
  public String toString()
  {
    return "Session [userId=" + userId + ", user=" + user + ", team=" + team + ", group=" + group + ", session_id=" + session_id
        + ", param0=" + param0 + ", param1=" + param1 + ", param2=" + param2 + ", starttime=" + starttime + ", tz=" + tzServer + "/" + tzLocal + ", last_update="
        + last_update + ", endtime=" + endtime + "]";
  }

  /**
   * ID of the user who performed this activity
   */
  @Column(name="userid")
  public long userId;

  @ManyToOne
  @JoinColumn(name="userid", insertable= false, updatable = false)
  private User user;
  public User getUser()
  {
    return user;
  }

  @ManyToOne(optional = true)
  @JoinColumn(name="teamid", nullable = true, insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  private Team team;
  public long teamid;
  public Team getTeam()
  {
    return team;
  }

  @ManyToOne(optional = true)
  @JoinColumn(name="groupid", nullable = true, insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  private Group group;
  public long groupid;
  public Group getGroup()
  {
    return group;
  }

  /**
   * ID of the session
   */
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name="session_id")
  public long session_id;

  @Column(name="activity")
  public int param0;
  public int param1;
  public int param2;

  /**
   * Timestamp when the activity was started
   */
  public long starttime;

  @Column(name="tz_server")
  public short tzServer;
  @Column(name="tz_local")
  public short tzLocal;

  public long last_update;

  public long endtime;

  public short ai;
}
