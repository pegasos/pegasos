package at.pegasos.server.frontback.config;

import org.springframework.context.annotation.*;

import at.pegasos.server.frontback.permission.*;

@Configuration
public class PrivilegeConfiguration {
  
  @Bean
  public PrivilegeManager privilegeManager()
  {
    return new DatabasePrivilegeManager();
  }
}
