package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.notification.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.services.*;
import io.swagger.v3.oas.annotations.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@RestController
@RequestMapping("/v1/notifications")
public class NotificationsController {

  @Autowired
  NotificationsRepository repo;

  @Autowired
  NotificationService notifier;

  @Autowired
  UserRepository userRepo;

  @Operation(
      summary = "Get all notifications",
      description = "Get a list of all current notifications for the authorised person"
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get")
  public List<Notification> get(Principal p)
  {
    assert p != null;

    return repo.findCurrentForPerson(userRepo.findByEmail(p.getName()).get(0).user_id);
  }

  @Operation(
          summary = "Accept a notification",
          description = "Mark a notification as read. This should only be performed on notifications without actions",
          parameters = {}
  )
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/accept/{nodification}")
  public Response accept(Principal p,
                         @Parameter(required = true, description = "ID of the notification") @PathVariable("nodification") long id)
  {
    assert p != null;

    Optional<Notification> on = repo.findById(id);
    if( on.isPresent() )
    {
      /*Notification n = on.get();
      n.deleted = true;
      repo.save(n);*/
      notifier.setNotificationRead(id);

      return GenericResponse.OkResponse;
    }
    return GenericResponse.FailResponse;
  }
}
