package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TeamWithGroupsWithUsers;

@Repository
@Transactional(readOnly = true)
public interface TeamWithGroupWithUsersRepository extends JpaRepository<TeamWithGroupsWithUsers, Long> {
  
  /*@Query(value = "select t from TeamUsers t where :userid in managers.user.id")
  List<TeamUsers> getForManager(@Param("userid") long userid);*/
  
  List<TeamWithGroupsWithUsers> findByTeamid(long teamid);
  
  @Query(value = "select t from TeamWithGroupsWithUsers t join t.captains as manager with manager.email = :email")
  List<TeamWithGroupsWithUsers> getForManager(@Param("email") String email);

  @Query(value = "select t from TeamWithGroupsWithUsers t join t.groups as group with group.groupid = :groupid")
  List<TeamWithGroupsWithUsers> findByGroup(@Param("groupid") Long groupid);
}
