package at.pegasos.server.frontback.auth;

import org.springframework.security.authentication.*;

public class CustomBadCredentialsException extends BadCredentialsException {
  private final String login;

  public CustomBadCredentialsException(String name)
  {
    super("Bad Credentials");
    this.login = name;
  }

  public String getLogin()
  {
    return login;
  }
}
