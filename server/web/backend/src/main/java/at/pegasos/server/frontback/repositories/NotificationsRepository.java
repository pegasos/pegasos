package at.pegasos.server.frontback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import at.pegasos.server.frontback.data.notification.Notification;

import java.util.List;

@Repository
public interface NotificationsRepository extends JpaRepository<Notification, Long> {

  @Query("SELECT n FROM Notification n where n.user.user_id = :userId AND deleted = false")
  List<Notification> findCurrentForPerson(long userId);
}
