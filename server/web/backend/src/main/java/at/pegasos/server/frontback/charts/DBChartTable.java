package at.pegasos.server.frontback.charts;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.data.chart.*;
import at.pegasos.server.frontback.data.chart.ChartWithPoints.*;
import org.springframework.jdbc.core.*;

import java.util.*;
import java.util.Map.*;

/**
 * Internal type representing information about an available chart. This type is not exposed. Any
 * relevant information contained in this type should also be present in the frontend and accessible
 * via the unique `name`.
 */
public class DBChartTable extends DBChart {

  private final String discfield, valfield;

  private final String avail_select;
  private final String query;

  public DBChartTable(String name, Type type, String tbl, String discfield, String valfield, String where)
  {
    super(name, type);
    this.discfield = discfield;
    this.valfield =  valfield;

    avail_select = "select count(" + valfield + ") as c from " + tbl + " where " + where + " AND session_id = ?";

    query = "SELECT " + discfield + ", " + valfield + " from " + tbl + " where session_id = ? AND " + where + " ORDER BY " + discfield + " ASC";
  }
  
  @Override
  public boolean isAvailable(JdbcTemplate jdbcTemplate, long session)
  {
    Map<String, Object> t = jdbcTemplate.queryForList(avail_select, new Object[] {session}).get(0);

    Long h= ((Long) t.get("c"));

    return h != null && h > 0;
  }

  @Override
  public String getDataQuery()
  {
    return query;
  }

  @Override
  public ChartWithPoints newResult(List<Map<String, Object>> rows)
  {
    Map<String, List<Point>> mseries = new HashMap<String, List<Point>>();
    
    for(Map<String, Object> row : rows)
    {
      Object discfieldo = row.get(discfield);
      String disc = discfieldo != null ? discfieldo.toString() : null;
      Object o = row.get(valfield);

      if( o != null )
      {
        List<Point> points = mseries.get(disc);
        if( points == null )
        {
          points = new ArrayList<Point>(rows.size());
          mseries.put(disc, points);
        }

        points.add(new Point(points.size()+1, Double.parseDouble(o.toString())));
      }
    }

    List<DataSeries<Point>> series = new ArrayList<DataSeries<Point>>(mseries.size());
    for(Entry<String, List<Point>> s : mseries.entrySet())
    {
      series.add(new DataSeries<Point>(s.getKey(), s.getValue()));
    }

    return new MultiLineChart(series);
  }
}
