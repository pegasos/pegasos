package at.pegasos.server.frontback.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.MetricInfo;

@Repository
@Transactional(readOnly = true)
public interface MetricInfoRepository extends JpaRepository<MetricInfo, Long>  {
  
  MetricInfo findByName(String name);

  Collection<MetricInfo> findByNameInOrderByIDAsc(String[] names);
}
