package at.pegasos.server.frontback.config;

import at.pegasos.server.frontback.*;
import at.pegasos.server.frontback.auth.Security;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.source.*;
import com.nimbusds.jose.proc.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.core.*;
import org.springframework.core.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.*;
import org.springframework.security.core.*;
import org.springframework.security.oauth2.core.*;
import org.springframework.security.oauth2.core.endpoint.*;
import org.springframework.security.oauth2.core.oidc.*;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.authorization.*;
import org.springframework.security.oauth2.server.authorization.authentication.*;
import org.springframework.security.oauth2.server.authorization.client.*;
import org.springframework.security.oauth2.server.authorization.config.*;
import org.springframework.security.oauth2.server.authorization.web.authentication.*;
import org.springframework.security.web.*;
import org.springframework.security.web.authentication.*;
import org.springframework.security.web.util.matcher.*;
import org.springframework.util.*;
import org.springframework.web.util.*;

import javax.servlet.http.*;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.time.*;
import java.util.*;

@Configuration(proxyBeanMethods = false)
public class AuthorizationServerConfig {

  private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
  @Autowired AuthenticationProvider authentication;

  @Bean @Order(Ordered.HIGHEST_PRECEDENCE)
  public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception
  {
    OAuth2AuthorizationServerConfigurer<HttpSecurity> authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer<>();
    http.apply(authorizationServerConfigurer);

    RequestMatcher endpointsMatcher = authorizationServerConfigurer.getEndpointsMatcher();

    // @formatter:off
    http.requestMatcher(endpointsMatcher)
        .authorizeRequests(authorizeRequests -> authorizeRequests.anyRequest().authenticated())
        .csrf(csrf -> csrf.ignoringRequestMatchers(endpointsMatcher))
        .apply(authorizationServerConfigurer);

    authorizationServerConfigurer
        .authorizationEndpoint(authorizationEndpoint -> authorizationEndpoint
          //.authorizationRequestConverter(new OAuth2AuthorizationCodeRequestAuthenticationConverter())
          .authorizationResponseHandler(this::sendAuthorizationResponse));

    http.exceptionHandling(exceptions -> exceptions
        .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login")));
    // @formatter:on
    return http.build();
  }

  private void sendAuthorizationResponse(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
      throws IOException
  {

    @SuppressWarnings("unchecked") Map<String, String> details = (Map<String, String>) ((UsernamePasswordAuthenticationToken) authentication.getPrincipal()).getDetails();

    OAuth2AuthorizationCodeRequestAuthenticationToken authorizationCodeRequestAuthentication = (OAuth2AuthorizationCodeRequestAuthenticationToken) authentication;
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(authorizationCodeRequestAuthentication.getRedirectUri())
        .queryParam(OAuth2ParameterNames.CODE, authorizationCodeRequestAuthentication.getAuthorizationCode().getTokenValue());
    if( StringUtils.hasText(authorizationCodeRequestAuthentication.getState()) )
    {
      uriBuilder.queryParam(OAuth2ParameterNames.STATE, authorizationCodeRequestAuthentication.getState());
    }

    if( details.containsKey("login_type") )
    {
      uriBuilder.queryParam("login_type", details.get("login_type"));
    }
    if( details.containsKey("stayloggedin") )
    {
      uriBuilder.queryParam("stayloggedin", details.get("stayloggedin"));
    }
    this.redirectStrategy.sendRedirect(request, response, uriBuilder.toUriString());
  }

  @Bean
  public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate)
  {
    // @formatter:off
    RegisteredClient registeredClient = RegisteredClient.withId("pegasos-web-client")
        .clientId("pegasos-web-client")
        .clientSecret("{noop}secret")
        .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
        .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
        .redirectUri(Config.FRONTEND_URL + "/login")
        .redirectUri(Config.FRONTEND_URL + "/authorize")
        .scope(OidcScopes.OPENID).scope("message.read").scope("message.write")
        .clientSettings(ClientSettings.builder().requireProofKey(true).build())
        .tokenSettings(TokenSettings.builder()
            .accessTokenTimeToLive(Duration.ofMinutes(15))
            .refreshTokenTimeToLive(Duration.ofMinutes(30)).build())
        .build();

    RegisteredClient pegasosClient = RegisteredClient.withId("pegasos-client")
        .clientId("pegasos-client")
        .clientSecret("{noop}secret2")
        .clientAuthenticationMethod(ClientAuthenticationMethod.NONE)
        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        .redirectUri("http://jafx-pegaos-client/login")
        .scope(OidcScopes.OPENID)
        .clientSettings(ClientSettings.builder().requireProofKey(true).build())
        .tokenSettings(TokenSettings.builder()
            .accessTokenTimeToLive(Duration.ofDays(365 * 5)).build())
        .build();

    RegisteredClient pegasosClientAndroid = RegisteredClient.withId("pegasos-client-android")
        .clientId("pegasos-client-android")
        .clientSecret("{noop}secret3")
        .clientAuthenticationMethod(ClientAuthenticationMethod.NONE)
        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        .redirectUri("pegasos://login")
        .redirectUri(Config.FRONTEND_URL)
        .scope(OidcScopes.OPENID)
        .clientSettings(ClientSettings.builder().requireProofKey(true).build())
        .tokenSettings(TokenSettings.builder()
            .accessTokenTimeToLive(Duration.ofDays(365 * 5)).build())
        .build();
    // @formatter:on

    // Save registered client in db as if in-memory
    /*JdbcRegisteredClientRepository registeredClientRepository = new JdbcRegisteredClientRepository(jdbcTemplate);
    registeredClientRepository.save(registeredClient);*/

    InMemoryRegisteredClientRepository registeredClientRepository = new InMemoryRegisteredClientRepository(registeredClient, pegasosClient,
        pegasosClientAndroid);

    return registeredClientRepository;
  }

  @Bean
  public OAuth2AuthorizationService authorizationService(JdbcTemplate jdbcTemplate,
      RegisteredClientRepository registeredClientRepository)
  {
    return new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
  }

  @Bean
  public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate,
      RegisteredClientRepository registeredClientRepository)
  {
    return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
  }

  @Autowired
  public void globalUserDetails(final AuthenticationManagerBuilder auth)
  {
    // Its important that we do not add the userdetailsservice here
    // auth.userDetailsService(users);
    auth.authenticationProvider(authentication);
  }

  @Bean
  public JWKSource<SecurityContext> jwkSource() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException
  {
    RSAKey rsaKey = Security.getRsa();
    JWKSet jwkSet = new JWKSet(rsaKey);
    return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
  }

  @Bean
  public ProviderSettings providerSettings()
  {
    return ProviderSettings.builder().issuer(Config.BACKEND_URL + ":" + Config.BACKEND_PORT).build();
  }

  @Bean
  JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource)
  {
    return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
  }
}
