package at.pegasos.server.frontback.dbdata;

import java.util.*;

import javax.persistence.*;

import at.pegasos.server.frontback.data.*;

@Entity
@Table(name="user_has_role")
public class UserHasRole {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  public long id;
  
  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="user_id", insertable=false, updatable= false)
  private User user;

  @ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="role_id", insertable=false, updatable= false)
  private Role role;

  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_start;

  @Temporal(TemporalType.TIMESTAMP)
  public Calendar date_end;

  public long user_id;

  public long role_id;

  public void setUser(User user)
  {
    this.user= user;
    this.user_id= user.user_id;
  }

  public void setRole(Role role)
  {
    this.role= role;
    this.role_id= role.id;
  }

  public static UserHasRole newRole(User user, Role role)
  {
    UserHasRole ret= new UserHasRole();
    ret.setRole(role);
    ret.setUser(user);
    ret.date_start= Calendar.getInstance();
    ret.date_start.setTimeInMillis(System.currentTimeMillis());
    return ret;
  }

  public void end()
  {
    date_end= Calendar.getInstance();
    date_end.setTimeInMillis(System.currentTimeMillis());
  }
}
