package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.controller.*;
import at.pegasos.server.frontback.dbdata.*;
import lombok.extern.slf4j.*;

import java.util.*;

@Slf4j
public class ReportUtil {
  public static Calendar getWeek(Calendar date)
  {
    Calendar ret = Calendar.getInstance();
    ret.clear();
    ret.set(Calendar.YEAR, date.get(Calendar.YEAR));
    ret.set(Calendar.DAY_OF_YEAR, date.get(Calendar.DAY_OF_YEAR));
    long startts = 0;
    int dow = date.get(Calendar.DAY_OF_WEEK);
    switch( dow )
    {
      case Calendar.MONDAY:
        startts = ret.getTimeInMillis();
        break;
      case Calendar.TUESDAY:
        startts = ret.getTimeInMillis() - 24 * 60 * 60 * 1000;
        break;
      case Calendar.WEDNESDAY:
        startts = ret.getTimeInMillis() - 2 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.THURSDAY:
        startts = ret.getTimeInMillis() - 3 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.FRIDAY:
        startts = ret.getTimeInMillis() - 4 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.SATURDAY:
        startts = ret.getTimeInMillis() - 5 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.SUNDAY:
        startts = ret.getTimeInMillis() - 6 * 24 * 60 * 60 * 1000;
        break;
    }

    ret.setTimeInMillis(startts);

    return ret;
  }

  public static long groupedDate(long starttime, ReportController.GroupBy grouping, Metric m)
  {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(starttime);
    Calendar c2 = null;

    switch( grouping )
    {
      case DAY:
        c2= Calendar.getInstance();
        c2.clear();
        c2.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c2.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR));
        break;

      case MONTH:
        c2= Calendar.getInstance();
        c2.clear();
        c2.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c2.set(Calendar.MONTH, c.get(Calendar.MONTH));
        break;

      case WEEK:
        c2= ReportUtil.getWeek(c);
        break;

      case YEAR:
        c2= Calendar.getInstance();
        c2.clear();
        c2.set(Calendar.YEAR, c.get(Calendar.YEAR));
        break;

      case ALL:
        // c2= Calendar.getInstance();
        break;

      default:
        throw new IllegalArgumentException("Grouping type " + grouping + " not implemented");
    }

    if( c2 != null )
      log.debug("metric processing c: {}-{}-{} c2: {}-{}-{} mId: {} param: {}",
          c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH),
          c2.get(Calendar.YEAR), c2.get(Calendar.MONTH), c2.get(Calendar.DAY_OF_MONTH), m.metricId, m.param);
    else
      log.debug("metric processing c2: null mId: {} param: {}", m.metricId, m.param);

    // when we do not group we do not have a date
    if( c2 != null )
    {
      return c2.getTimeInMillis();
    }
    else
    {
      return 0;
    }
  }
}
