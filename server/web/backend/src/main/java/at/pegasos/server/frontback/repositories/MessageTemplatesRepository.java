package at.pegasos.server.frontback.repositories;

import at.pegasos.server.frontback.services.MessageTemplates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MessageTemplatesRepository extends JpaRepository<MessageTemplates.MessageTemplate, Long> {
  Optional<MessageTemplates.MessageTemplate> findByTemplatenameAndLang(String templateName, String defaultLang);
}
