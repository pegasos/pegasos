package at.pegasos.server.frontback.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.pegasos.server.frontback.data.TeamWithGroups;

@Repository
@Transactional(readOnly = true)
public interface TeamGroupsRepository extends JpaRepository<TeamWithGroups, Long> {
  
  List<TeamWithGroups> findByTeamid(long teamid);
  
  @Query(value = "select t from TeamWithGroups t join t.captains as manager with manager.email = :email")
  List<TeamWithGroups> getForManager(@Param("email") String email);
}
