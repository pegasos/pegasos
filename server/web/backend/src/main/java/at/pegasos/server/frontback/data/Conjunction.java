package at.pegasos.server.frontback.data;

public enum Conjunction {
  AND, OR
}
