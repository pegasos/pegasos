package at.pegasos.server.frontback.data;

public class Response {
  protected String message;
  protected String error;

  public final static String OK= "OK";
  public final static String FAIL = "Fail";

  public static final Response CreatedResponse= new CreatedResponse();

  public Response(final String message)
  {
    this.message= message;
  }
  
  public Response(final String message, final String error)
  {
    this.message= message;
    this.error= error;
  }
  
  public String getMessage()
  {
    return message;
  }
  
  public void setMessage(final String message)
  {
    this.message= message;
  }
  
  public String getError()
  {
    return error;
  }
  
  public void setError(final String error)
  {
    this.error= error;
  }
  
}
