package at.pegasos.server.frontback.controller;

import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.core.io.*;
import org.springframework.http.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.security.*;
import java.util.*;
import java.util.stream.*;

import at.pegasos.server.frontback.config.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.exports.*;
import at.pegasos.server.frontback.permission.*;
import at.pegasos.server.frontback.reports.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.tasks.*;
import at.pegasos.server.frontback.util.*;
import io.swagger.v3.oas.annotations.*;
import lombok.*;
import lombok.extern.slf4j.*;

@Slf4j
@RestController
@RestControllerAdvice
@RequestMapping("/v1/activities")
public class TrainingController {
  
  @SuppressWarnings("unused")
  public static class HDate implements Serializable {
    public int getYear()
    {
      return year;
    }
    public void setYear(int year)
    {
      this.year= year;
    }
    public int getMonth()
    {
      return month;
    }
    public void setMonth(int month)
    {
      this.month= month;
    }
    public int getDay()
    {
      return day;
    }
    public void setDay(int day)
    {
      this.day= day;
    }
    private static final long serialVersionUID= 4318264924425487945L;
    
    public int year;
    public int month;
    public int day;
    @Override
    public String toString()
    {
      return "Date [year=" + year + ", month=" + month + ", day=" + day + "]";
    }
    
    public boolean valid()
    {
      return year != 0 && month != 0 && day != 0;
    }
  }

  @SuppressWarnings("unused")
  @ToString
  public static class Filter implements Serializable {
    public static class Activity implements Serializable {
      private static final long serialVersionUID= 398263132617662662L;
      
      public Short level;
      public Integer param0;
      public Integer param1;
      public Integer param2;
    }
    
    public List<Activity> getActivitytypes()
    {
      return activitytypes;
    }
    public void setActivitytypes(List<Activity> activitytypes)
    {
      this.activitytypes= activitytypes;
    }
    public HDate getStartdate()
    {
      return startdate;
    }
    public void setStartdate(HDate startdate)
    {
      this.startdate= startdate;
    }
    public HDate getEnddate()
    {
      return enddate;
    }
    public void setEnddate(HDate enddate)
    {
      this.enddate= enddate;
    }
    public List<String> getPersons()
    {
      return persons;
    }
    public void setPersons(List<String> persons)
    {
      this.persons= persons;
    }
    private static final long serialVersionUID= 7045546311888346360L;
    
    public List<Activity> activitytypes;
    public HDate startdate;
    public HDate enddate;
    public List<String> persons;
  }
  
  @Autowired
  SessionsRepository repo;
  
  @Autowired
  UserRepository userrepo;
  
  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired PrivilegeManager privilegeManager;

  @Autowired TaskService taskService;

  @Autowired private ApplicationContext applicationContext;

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get")
  public List<Activity> get(Principal p) throws Exception
  {
    assert( p != null );

    User me = userrepo.findByEmail(p.getName()).get(0);

    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(System.currentTimeMillis());
    cal.add(Calendar.WEEK_OF_YEAR, -2);

    List<Session> mysessions = repo.findByUserIdAndStarttimeAfterOrderByStarttimeDesc(me.user_id, cal.getTime().getTime());
    ArrayList<Activity> ret2 = new ArrayList<Activity>(mysessions.size());

    for (Session mysession : mysessions)
    {
      Activity activity = new Activity(mysession);
      Exports.fillInExports(mysession.param0, mysession.param1, mysession.param2, activity, p, applicationContext);
      ret2.add(activity);
    }

    return ret2;
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.POST, value = "/get", consumes = MediaType.APPLICATION_JSON_VALUE)
  public List<Activity> getPost(Principal p, @RequestBody Filter body)
  {
    assert( p != null );
    log.error("POST get " + body.activitytypes + " " + body.startdate + " " + body.enddate + " " + body.persons);
    
    User me= userrepo.findByEmail(p.getName()).get(0);
    
    Calendar cal = Calendar.getInstance();
    Calendar cal2= Calendar.getInstance();
    if( body.startdate.valid() )
    {
      //noinspection MagicConstant
      cal.set(body.startdate.year, body.startdate.month - 1, body.startdate.day - 1, 0, 0, 0);
    }
    else
    {
      cal.setTimeInMillis(System.currentTimeMillis());
      cal.add(Calendar.WEEK_OF_YEAR, -2);
    }
    if( body.enddate.valid() )
    {
      //noinspection MagicConstant
      cal2.set(body.enddate.year, body.enddate.month - 1, body.enddate.day, 23, 59, 59);
    }
    else
    {
      cal2.setTimeInMillis(System.currentTimeMillis());
    }
    
    long starttime= cal.getTimeInMillis();
    long endtime= cal2.getTimeInMillis();
    
    StringBuilder query= new StringBuilder("select s.*, u.forename as user_firstname, u.surname as user_lastname, t.name as team_name, g.name as group_name from session s left join user u on s.userid = u.id left join teams t on s.teamid = t.id left join teams_groups g on s.groupid = g.id where ");
    boolean and_needed= false;
    
    boolean or_needed= false;
    if( body.activitytypes != null && body.activitytypes.size() > 0 )
    {
      query.append("(");
      for(at.pegasos.server.frontback.controller.TrainingController.Filter.Activity a : body.activitytypes)
      {
        if( or_needed )
          query.append("OR");
        or_needed = true;
        query.append("(");
        switch( a.level )
        {
          case 1:
            query.append("activity = ").append(a.param0);
            break;
          case 2:
            query.append("activity = ").append(a.param0).append(" AND param1 = ").append(a.param1);
            break;
          case 3:
            query.append("activity = ").append(a.param0).append(" AND param1 = ").append(a.param1).append(" AND param2 = ").append(a.param2);
            break;
        }
        query.append(")");
        and_needed = true;
      }
      query.append(")");
    }

    if( body.persons.size() > 0 )
    {
      if( and_needed )
      {
        query.append(" AND ");
        and_needed= false;
      }
      query.append(PersonSelect.buildQueryString(body.persons));
      and_needed= true;
    }
    else
    {
      // no persons selected -> return my sessions
      if( and_needed )
      {
        query.append(" AND ");
      }
      query.append("s.userid = ").append(me.user_id);
      and_needed= true;
    }
    
    query.append(" AND ");
    query.append("starttime > ").append(starttime).append(" AND endtime < ").append(endtime).append(" ORDER BY starttime DESC");
    
    log.trace(query.toString());
    
    List<Map<String, Object>> activities= jdbcTemplate.queryForList(query.toString());
    
    ArrayList<Activity> ret= new ArrayList<Activity>(activities.size());
    
    int i= 0;
    for(i= 0; i < activities.size(); i++)
    {
      Map<String, Object> d = activities.get(i);
      Activity a = new Activity(d);
      Exports.fillInExports(ObjectHelper.getIntValue(d.get("activity")),
              ObjectHelper.getIntValue(d.get("param1")), ObjectHelper.getIntValue(d.get("param2")),
              a, p, applicationContext);

      ret.add(a);
    }
    
    return ret;
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/summary/{sessionId}")
  public Activity getActivity(Principal p, @PathVariable("sessionId") long sessionId)
  {
    assert( p != null );

    Optional<Session> os= repo.findById(sessionId);
    if( os.isEmpty() )
      throw new UnkownSessionException();

    Session session = os.get();
    Activity activity = new Activity(session);
    Exports.fillInExports(session.param0, session.param1, session.param2, activity, p, applicationContext);

    return activity;
  }
  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.DELETE, value = "/{sessionId}")
  public GenericResponse deleteSession(Principal p, @PathVariable("sessionId") long sessionId)
  {
    assert( p != null );

    log.error("Delete " + sessionId);

    if( repo.existsById(sessionId) )
    {
      Optional<Session> os = repo.findById(sessionId);
      if( os.isEmpty() )
        throw new UnkownSessionException();

      Session s= os.get();
      if( s.getTeam() != null )
      {
        // are we managing the team
        User me= userrepo.findByEmail(p.getName()).get(0);
        String query= "select TRUE from manages_team m where " + s.getTeam().teamid + "= m.teamid and m.userid = " + me.user_id;
        if( jdbcTemplate.queryForList(query).size() > 0 )
        {
          // TODO: check privilege delete session
          repo.deleteById(sessionId);
          return new GenericResponse(GenericResponse.OK);
        }
        else
        {
          return new GenericResponse(GenericResponse.FAIL, "notmanager");
        }
      }
      else
      {
        // We are the owner
        // TODO: check privilege delete session
        repo.deleteById(sessionId);
        return new GenericResponse(GenericResponse.OK);
      }
    }
    else
    {
      // do not sniff on me or delete me multiple times
      return new GenericResponse(GenericResponse.OK);
    }
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/postprocessor/{sessionId}")
  public GenericResponse runPostProcessor(Principal p, @PathVariable("sessionId") long sessionId)
  {
    if( privilegeManager.hasPrivilegeActivity(p, "run_postprocessor", sessionId) )
    {
      if( AiHelper.callBackground(log, "at.univie.mma.ai.postprocessing.PostProcessor", sessionId) )
      {
        return new GenericResponse(GenericResponse.OK);
      }
      else
      {
        return new GenericResponse(GenericResponse.FAIL);
      }
    }
    else
    {
      return new GenericResponse(GenericResponse.FAIL);
    }
  }

  @Operation(description = "(Re-)run post processor on a set of activities")
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/runpost")
  public TaskStatus runPostProcessorParams(Principal p,
      @RequestParam(required=false, name="pers") List<String> persons,
      @RequestParam(required=false, name="action") String action,
      @RequestParam(name="start") String paramstart,
      @RequestParam(required = false, name="end") String paramend,
      @RequestBody(required=false) List<SessionTypeFilter> sessionFilter)
  {
    User me= userrepo.findByEmail(p.getName()).get(0);
    PostProcessorRunner runner = new PostProcessorRunner();

    log.debug("Run Post Params pers: {}, action: {}, start: {}, end: {}, filter: {}", persons, action, paramstart, paramend, sessionFilter);

    Collection<Activity> activities = querySessions(me.user_id, paramstart, paramend, persons, sessionFilter, false);
    for(Activity a : activities)
    {
      if( privilegeManager.hasPrivilegeActivity(p, "run_postprocessor", a.session_id) )
      {
        if( action != null )
        {
          runner.add(a.session_id, action);
        }
        else
        {
          runner.add(a.session_id);
        }
      }
    }

    long taskId = taskService.addTask(runner);
    runner.run();

    return new TaskStatus(taskId, runner);
  }

  private Collection<Activity> querySessions(long userId, String paramstart, String paramend, List<String> personFilter, List<SessionTypeFilter> sessionFilter, boolean desc)
  {
    StringBuilder query= new StringBuilder(
        "select s.*, u.forename as user_firstname, u.surname as user_lastname, t.name as team_name, g.name as group_name from session s left join user u on s.userid = u.id left join teams t on s.teamid = t.id left join teams_groups g on s.groupid = g.id where ");
    boolean and_needed= false;

    boolean or_needed= false;
    if( sessionFilter != null && sessionFilter.size() > 0 )
    {
      query.append("(");
      for(SessionTypeFilter type : sessionFilter)
      {
        if( or_needed )
          query.append("OR");
        or_needed = true;
        query.append("(");
        switch( type.len )
        {
          case 1:
            query.append("activity = ").append(type.param0);
            break;
          case 2:
            query.append("activity = ").append(type.param0).append(" AND param1 = ").append(type.param1);
            break;
          case 3:
            query.append("activity = ").append(type.param0).append(" AND param1 = ").append(type.param1).append(" AND param2 = ")
                .append(type.param2);
            break;
        }
        query.append(")");
        and_needed = true;
      }
      query.append(")");
    }

    if( personFilter != null && personFilter.size() > 0 )
    {
      if( and_needed )
      {
        query.append(" AND ");
        and_needed = false;
      }
      query.append(PersonSelect.buildQueryString(personFilter));
      and_needed = true;
    }
    else
    {
      // no persons selected -> return my sessions
      if( and_needed )
      {
        query.append(" AND ");
      }
      query.append("s.userid = ").append(userId);
      and_needed = true;
    }

    query.append(" AND starttime > ").append(Time.paramToTimestamp(paramstart));

    if( paramend != null )
      query.append(" AND endtime < ").append(Time.paramToTimestamp(paramend));

    query.append(" ORDER BY starttime ");
    if( desc )
      query.append("DESC");
    else
      query.append("ASC");

    log.debug(query.toString());

    return jdbcTemplate.queryForList(query.toString()).stream().map(Activity::new).collect(Collectors.toList());
  }

  @Operation(description = "Download an activity")
  @PreAuthorize("isAuthenticated()")
  @ResponseBody
  @RequestMapping(method = RequestMethod.GET, value = "{sessionId}/{downloadType}")
  public ResponseEntity<Resource> exportFile(Principal p, @PathVariable("sessionId") long sessionId,
                                             @PathVariable("downloadType") String type) throws IOException
  {
    assert p != null;

    Optional<Session> os = repo.findById(sessionId);
    if( os.isEmpty() )
      throw new UnkownSessionException();

    Session session = os.get();
    SessionExport exporter = Exports.getExporter(privilegeManager, p, session, type, applicationContext);
    try
    {
      String fileName = exporter.getFileName();
      Path file = exporter.getFile();
      Resource resource = new UrlResource(file.toUri());

      if( resource.exists() || resource.isReadable() )
      {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, exporter.getContentType())
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "Content-Disposition")
                .body(resource);
      }
      else
      {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not read file");
      }
    }
    catch( MalformedURLException e )
    {
      throw new RuntimeException("Error: " + e.getMessage());
    }
  }

  @Operation(description = "Create a session for a person")
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.PUT, value = "/{userId}")
  public Response create(Principal p,
                                @Parameter(name = "userId", description = "Person for which the session should be created")
                                @PathVariable("userId") long userId,
                                @Parameter(name = "start", description = "(Local) unix timestamp marking the start of the session")
                                @RequestParam(name = "start") long start,
                                @Parameter(name = "start", description = "(Local) unix timestamp marking the end of the session")
                                @RequestParam(name = "end") long end,
                                @Parameter(name = "tzOffset", description = "timezone offset of the client in minutes")
                                @RequestParam(name = "tzOffset") int tzOffset,
                                @Parameter(name = "param0")
                                @RequestParam(required = false, name = "param0")
                                Integer param0,
                                @Parameter(name = "param1")
                                @RequestParam(required = false, name = "param1")
                                Integer param1,
                                @Parameter(name = "param2")
                                @RequestParam(required = false, name = "param2")
                                Integer param2)
  {
    log.debug("Create session: Person {}, start {}, end {} tzOffset {} params [{}, {}, {}]", userId, start, end, tzOffset, param0, param1, param2);
    if( privilegeManager.hasPrivilege(p, "create_session", userId) )
    {
      Session session = new Session();
      Optional<User> ou = userrepo.findById(userId);
      if(ou.isPresent())
        session.userId = ou.get().user_id;
      else
        return GenericResponse.FailResponse;
      session.starttime = start;
      session.endtime = session.last_update = end;
      if( param0 != null )
      {
        session.param0 = param0;
        if( param1 != null )
        {
          session.param1 = param1;
          if( param2 != null )
          {
            session.param2 = param2;
          }
        }
      }
      session.tzLocal = (short) (tzOffset / 60.0 * 2);
      session.tzServer = (short) (TimeZone.getDefault().getOffset(start) / 1000 / 60 / 60.0 * 2);

      session.ai = 0;
      // session.team = Team.NoTeam;
      // session.group = Group.NoGroup;
      session.teamid = 0;
      session.groupid = 0;

      log.debug("Session: {}", session);
      session = repo.save(session);
      return new CreatedResponse(String.valueOf(session.session_id));
    }
    else
      throw new InsufficientPrivilegesException();
  }
}