package at.pegasos.server.frontback.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "user")
public class TUser {
  
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  public long id;
  
  /**
   * Firstname of the user
   */
  @Column(length= 255, nullable= false, unique= false, name="forename")
  public String firstname;
  
  /**
   * Lastname of the user
   */
  @Column(length= 255, nullable= false, unique= false, name="surname")
  public String lastname;

  @Column(length= 255, nullable= false, unique= true)
  public String email;

  @Override
  public String toString()
  {
    return "TUser [id=" + id + ", " + firstname + ", " + lastname + "]";
  }
  
}
