package at.pegasos.server.frontback.util;

import java.util.*;

public class Time {
  /**
   * Convert a parameter to a calendar
   * @param param string in the form yyyy-mm-dd
   * @param off offset in days
   * @return calendar
   */
  public static Calendar paramToCalendar(String param, int off)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(0);

    String[] h = param.split("-");
    cal.set(Calendar.YEAR, Integer.parseInt(h[0]));
    cal.set(Calendar.MONTH, Integer.parseInt(h[1]) - 1);
    // Add one to include the full day
    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(h[2]) + off);

    return cal;
  }

  /**
   * Convert a parameter to a calendar. If param is null currentTimeMillis is returned
   * @param param string in the form yyyy-mm-dd
   * @return timestamp
   */
  public static long paramToTimestamp(String param)
  {
    if( param == null )
      return System.currentTimeMillis();
    return paramToCalendar(param, 0).getTimeInMillis();
  }
}
