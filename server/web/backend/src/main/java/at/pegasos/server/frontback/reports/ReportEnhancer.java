package at.pegasos.server.frontback.reports;

import at.pegasos.server.frontback.data.Report;

public interface ReportEnhancer {
  public void enhance(Report report);
}
