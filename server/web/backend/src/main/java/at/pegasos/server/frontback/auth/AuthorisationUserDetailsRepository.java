package at.pegasos.server.frontback.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorisationUserDetailsRepository extends JpaRepository<AuthorisationUserDetails, Long> {
  
  List<AuthorisationUserDetails> findByEmail(String email);
}
