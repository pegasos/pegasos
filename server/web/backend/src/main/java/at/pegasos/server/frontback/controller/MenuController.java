package at.pegasos.server.frontback.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.pegasos.server.frontback.data.Menu;

@RestController
@RequestMapping("/menu")
public class MenuController {
  
  @RequestMapping(method = RequestMethod.GET, value = "/get")
  public Menu[] getAvail(Principal p)
  {
    if( p == null )
    {
      return new Menu[] {
          new Menu("rankings", "mainMenu_rankings"),
          new Menu("hilfe", "mainMenu_help"),
          new Menu("impressum", "mainMenu_imprint")
          };
    }
    
    return new Menu[] {new Menu("persoenlichedaten", "mainMenu_personalData"),
        new Menu("team", "mainMenu_team"),
        new Menu("aktuell", "mainMenu_currentEvents"),
        new Menu("tagebuch", "mainMenu_diary"),
        new Menu("rankings", "mainMenu_rankings"),
        new Menu("hilfe", "mainMenu_help"),
        new Menu("impressum", "mainMenu_imprint")
        };
  }
}
