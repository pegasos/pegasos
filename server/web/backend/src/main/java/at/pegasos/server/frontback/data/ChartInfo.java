package at.pegasos.server.frontback.data;

import lombok.*;

@ToString
public class ChartInfo {
  public String name;
  public Type type;
}
