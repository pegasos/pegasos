package at.pegasos.server.frontback.charts;

import org.springframework.jdbc.core.JdbcTemplate;

import at.pegasos.server.frontback.data.Type;

/**
* Internal type representing information about an available chart. This type is not exposed. Any
* relevant information contained in this type should also be present in the frontend and accessible
* via the unique `name`.
*/
public abstract class AvailableChart {
  /**
   * Unique name for the chart / chart-type
   */
  public String name;
  /**
   * Type of the chart (line, map, ...) when displayed on the client
   */
  public Type type;
   
  public AvailableChart(String name, Type type)
  {
    this.name= name;
    this.type= type;
  }

  public abstract boolean isAvailable(JdbcTemplate jdbcTemplate, long session);
}
