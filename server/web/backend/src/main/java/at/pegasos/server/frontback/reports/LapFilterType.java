package at.pegasos.server.frontback.reports;

enum LapFilterType {
  StartTime, Duration, LapNr
}
