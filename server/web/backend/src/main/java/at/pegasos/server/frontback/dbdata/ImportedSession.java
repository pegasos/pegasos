package at.pegasos.server.frontback.dbdata;

import javax.persistence.*;

import at.pegasos.server.frontback.data.*;

@Entity
@Table(name="imported_session")
public class ImportedSession {

  @GeneratedValue
  @Id
  @Column(name="import_id")
  public long ID;
  
  @Column(name="session_id")
  public long sessionId;
  
  /*@ManyToOne(fetch= FetchType.LAZY)
  @JoinColumn(name="session_id", insertable=false, updatable= false)
  public SessionInfo session;*/
  
  public long rec_time;
  
  public String type;
  
  public String data;
}
