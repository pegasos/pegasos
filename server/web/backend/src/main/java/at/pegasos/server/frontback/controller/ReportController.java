package at.pegasos.server.frontback.controller;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.reports.*;
import at.pegasos.server.frontback.repositories.*;
import at.pegasos.server.frontback.services.*;
import at.pegasos.server.frontback.util.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

import java.security.*;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/v1/reports/")
public class ReportController {

  @Autowired UserRepository userrepo;

  public enum GroupBy {
    DAY, WEEK, MONTH, YEAR, ALL
  }

  public enum Aggregate {
    SUM, MAX, MIN, MEAN, NONE,
    FIRST, LAST
  }

  @Autowired ReportGeneratorFactory repgen;

  @Autowired ReportEnhancerFactory enhancerFactory;

  @Autowired PersonSelect personSelect;

  @Autowired private UserValueService valueservice;

  @Autowired private MetricInfoRepository metrirep;

  @Autowired private MetricRepository metrrep;

  @Autowired private SessionInfoRepository sirepo;

  @Autowired private RepositoryService repositoryService;

  @Autowired private JdbcTemplate jdbcTemplate;

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get/{name}")
  public Report getReport(Principal p, @PathVariable("name") String name,
      @RequestParam(required=false, name="start") String paramstart, @RequestParam(required=false, name="end") String paramend,
      @RequestParam(required=false, name="pers") List<String> persons)
  {
    assert( p != null );
    // log.error(paramstart + " " + paramend + " " + persons);
    log.error("get report name {}, start: {}, end: {}, persons: {}", name, paramstart, paramend, persons);
    // logger.error("POST get " + body.activitytypes + " " + body.startdate + " " + body.enddate + " " + body.persons);

    User me = userrepo.findByEmail(p.getName()).get(0);

    ReportGenerator gen = repgen.getReport(name);
    
    if( paramstart == null )
    {
      repgen.setDefaultDate(name, gen);
    }
    else
    {
      log.debug("Setting start to {} / {}", Time.paramToCalendar(paramstart, 0), paramstart);
      gen.setStart(Time.paramToCalendar(paramstart, 0));
    }

    if( paramend != null )
    {
      Calendar end;

      end = Calendar.getInstance();
      end.setTimeInMillis(0);

      String[] h = paramend.split("-");
      end.set(Calendar.YEAR, Integer.parseInt(h[0]));
      end.set(Calendar.MONTH, Integer.parseInt(h[1]) - 1);
      // Add one to include the full day
      end.set(Calendar.DAY_OF_MONTH, Integer.parseInt(h[2]) + 1);

      log.debug("Setting end to {}", end);
      gen.setEnd(end);
    }

    gen.setMe(me);

    if( persons != null )
    {
      log.debug("Set persons: {}", personSelect.getUsers(persons));
      gen.setUsers(personSelect.getUsers(persons));
    }
    else
    {
      gen.setUsers(Collections.singletonList(me.user_id));
    }

    Report report = gen.generateReport();

    ReportEnhancer enhancer = enhancerFactory.getEnhancer(name);
    if( enhancer != null )
      enhancer.enhance(report);

    return report;
  }

  
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.GET, value = "/get/custom")
  public Report getCustomReportGet(Principal p,
      @RequestParam(name="type") String type,
      @RequestParam(name="aggregate", required = false) Aggregate aggregate,
      @RequestParam(name="groupby", required = false) GroupBy groupby,
      @RequestParam(name="values", required = true) List<String> values,
      @RequestParam(name="start") String paramstart,
      @RequestParam(name="end") String paramend,
      @RequestParam(required=false, name="pers") List<String> persons)
  {
    return getCustomReport(p, type, aggregate, groupby, values, paramstart, paramend, persons, null);
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(method = RequestMethod.POST, value = "/get/custom")
  public Report getCustomReportPost(Principal p,
                                @RequestParam(name="type") String type,
                                @RequestParam(name="aggregate", required = false) Aggregate aggregate,
                                @RequestParam(name="groupby", required = false) GroupBy groupby,
                                @RequestParam(name="values", required = true) List<String> values,
                                @RequestParam(name="start") String paramstart,
                                @RequestParam(name="end") String paramend,
                                @RequestParam(required=false, name="pers") List<String> persons,
                                @RequestBody(required=false) ReportFilter filter)
  {
    log.debug("POST getCustomReportPost {}", filter);
    return getCustomReport(p, type, aggregate, groupby, values, paramstart, paramend, persons, filter);
  }

  public Report getCustomReport(Principal p,
                                    String type,
                                    Aggregate aggregate,
                                    GroupBy groupby,
                                    List<String> values,
                                    String paramstart,
                                    String paramend,
                                    List<String> persons,
                                    ReportFilter filter)
  {
    assert( p != null );
    log.debug(
        "Report request type:{}, aggregate: {}, groupby: {}, values: {}, start: {}, end: {}, persons: {}, session-filter: {}, lapQuery: {}, dataQuery: {}",
        type, aggregate, groupby, values, paramstart, paramend, persons, filter != null ? filter.sessionFilter : null,
        filter != null ? filter.lapQuery : null, filter != null ? filter.dataQuery : null);

    User me = userrepo.findByEmail(p.getName()).get(0);

    ReportGenerator gen;

    switch( type )
    {
      case "UserValue":
        gen = new UserValueBasedReportGenerator(valueservice, values.toArray(new String[0]));
        break;

      case "Metric":
        if( filter == null || filter.sessionFilter == null )
          gen = new MetricBasedReportGenerator(metrirep, metrrep, sirepo, values.toArray(new String[0]));
        else
          gen = new MetricBasedReportGenerator(metrirep, metrrep, sirepo, values.toArray(new String[0]), filter.sessionFilter);
        break;

      case "MAS":
      case "MetricAccumulateSimple":
        if( filter == null || filter.sessionFilter == null )
          gen = new MetricBasedAccumulatedReportGeneratorSimple(metrirep, metrrep, sirepo, values.toArray(new String[0]));
        else
          gen = new MetricBasedAccumulatedReportGeneratorSimple(metrirep, metrrep, sirepo, values.toArray(new String[0]),
              filter.sessionFilter);
        ((MetricBasedAccumulatedReportGeneratorSimple) gen).setAggregate(aggregate);
        ((MetricBasedAccumulatedReportGeneratorSimple) gen).setGroupBy(groupby);
        break;

      case "MAP":
      case "MetricAccumulateParam":
        if( aggregate != Aggregate.NONE )
        {
          if( filter == null || filter.sessionFilter == null )
            gen = new MetricBasedAccumulatedReportGeneratorParam(metrirep, metrrep, sirepo, values.toArray(new String[0]));
          else
            gen = new MetricBasedAccumulatedReportGeneratorParam(metrirep, metrrep, sirepo, values.toArray(new String[0]),
                filter.sessionFilter);
          ((MetricBasedAccumulatedReportGeneratorParam) gen).setAggregate(aggregate);
          ((MetricBasedAccumulatedReportGeneratorParam) gen).setGroupBy(groupby);
        }
        else
        {
          if( filter == null )
            gen = new MetricBasedReportGeneratorGrouped(repositoryService, values.toArray(new String[0]), null, null);
          else
            gen = new MetricBasedReportGeneratorGrouped(repositoryService, values.toArray(new String[0]), filter.sessionFilter,
                filter.lapQuery);
          ((MetricBasedReportGeneratorGrouped) gen).setGroupBy(groupby);
        }
        break;

      case "Session":
        if( filter == null || filter.dataQuery == null )
          throw new IllegalArgumentException("Cannot create a report without a dataQuery");
        gen = new SessionValueReportGenerator(jdbcTemplate, sirepo, filter.dataQuery, filter.sessionFilter);
        break;

      default:
        log.error("Unkown report type {}", type);
        return null;
    }

    if( persons != null )
    {
      gen.setUsers(personSelect.getUsers(persons));
    }
    else
    {
      gen.setUsers(Collections.singletonList(me.user_id));
    }

    Calendar start = Time.paramToCalendar(paramstart, 0);
    log.debug("Setting start to {}", start);
    gen.setStart(start);

    Calendar end = Time.paramToCalendar(paramend, 1);
    log.debug("Setting end to {}", end);
    gen.setEnd(end);

    gen.setMe(me);

    log.debug("Generator {}", gen);

    return gen.generateReport();
  }

}
