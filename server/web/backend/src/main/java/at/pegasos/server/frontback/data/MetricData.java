package at.pegasos.server.frontback.data;

import java.util.List;

public class MetricData {
  public String getName()
  {
    return name;
  }

  public List<Object> getData()
  {
    return data;
  }

  /**
   * Name of the metric
   */
  public String name;
  
  public List<Object> data;
}