package at.pegasos.server.frontback.exports;

import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.dbdata.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.jdbc.core.*;

import java.io.*;
import java.nio.file.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.Comparator;

@SuppressWarnings("SimpleDateFormat")
@Slf4j
public class CsvExport implements SessionExport {
  private final static SimpleDateFormat dateFormat;
  static {
    dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
  }

  private final long sessionId;
  private final Path exportedFile;
  private final String fileName;

  public CsvExport(Session session, ApplicationContext applicationContext)
  {
    this.sessionId = session.session_id;
    exportedFile = Paths.get("/tmp/" + sessionId + ".csv");

    jdbcTemplate = applicationContext.getBean(JdbcTemplate.class);

    fileName = session.getUser().firstname + "_" + session.getUser().lastname + "-" + dateFormat.format(session.starttime) + ".csv";
  }

  public static boolean isApplicable(Activity activity, Principal person, ApplicationContext applicationContext)
  {
    return true;
  }

  @Override
  public String getFileName()
  {
    return fileName;
  }

  @Override
  public Path getFile() throws IOException
  {
    if( !Files.exists(exportedFile) )
    {
      exportData();
    }
    return exportedFile;
  }

  @Override
  public String getContentType()
  {
    try
    {
      return Files.probeContentType(exportedFile);
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
  }

  @Autowired
  JdbcTemplate jdbcTemplate;

  private static class TimedData {
    public final long rec_time;
    public final String type;
    public final String sensorNr;
    public final Object data;

    public TimedData(long rec_time, String type, String sensorNr, Object data)
    {
      this.rec_time = rec_time;
      this.data = data;
      this.type = type;
      this.sensorNr = sensorNr;
    }
  }

  private final static TableDescription[] tables = TableDescriptions.tables;

  protected void exportData() throws IOException
  {
    ArrayList<TimedData> data = new ArrayList<TimedData>();
    for(TableDescription tbl : tables)
    {
      List<Map<String, Object>> dd = fetchData(tbl);
      if( tbl.sensorNr )
      {
        for(Map<String, Object> row : dd)
        {
          long recTime = (Long) row.get("rec_time");
          String sensorNr = (String) row.get("sensor_nr");
          for(String col : tbl.columns )
          {
            if( row.get(col) != null )
              data.add(new TimedData(recTime, tbl + "." + col, sensorNr, row.get(col).toString()));
          }
        }
      }
      else
      {
        for(Map<String, Object> row : dd)
        {
          long recTime = (Long) row.get("rec_time");
          for(String col : tbl.columns )
          {
            if( row.get(col) != null )
              data.add(new TimedData(recTime, tbl + "." + col, "", row.get(col).toString()));
          }
        }
      }
    }
    data.sort(Comparator.comparingLong(a -> a.rec_time));
    BufferedWriter writer = Files.newBufferedWriter(exportedFile);
    for(TimedData d : data)
    {
      writer.write(d.rec_time + "," + d.type + "," + d.sensorNr + "," + d.data + "\n");
    }
    writer.close();
  }

  private List<Map<String, Object>> fetchData(TableDescription tbl)
  {
    assert tbl.columns != null;
    String query = "select rec_time, " + (tbl.sensorNr ? "sensor_nr," : "") + String.join(",", tbl.columns) +
            " from " + tbl.table + " where session_id = " + sessionId + " ORDER BY rec_time ASC";
    return jdbcTemplate.queryForList(query);
  }
}
