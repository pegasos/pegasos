package at.pegasos.server.frontback;

import java.util.*;

import at.pegasos.server.frontback.tasks.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class StartupAction implements ApplicationListener<ContextRefreshedEvent> {
  
  private static final Logger LOG= LoggerFactory.getLogger(StartupAction.class);
  
  @Autowired
  private CleanTemp cleantemp;

  @Autowired private TaskService taskService;

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event)
  {
    LOG.info("Application startup");
    
    // run cleaning task 'now' and then every hour
    new Timer().schedule(cleantemp, 1000, 60*60*1000);

    new Timer().schedule(new TimerTask() {
      @Override public void run()
      {
        taskService.clean();
      }
    }, 60*60*1000);
  }
}