package at.pegasos.server.frontback.config;


import at.pegasos.server.frontback.controller.*;
import at.pegasos.server.frontback.dbdata.*;
import at.pegasos.server.frontback.data.*;
import at.pegasos.server.frontback.permission.*;
import java.util.*;
import org.springframework.context.*;
import java.security.*;
import at.pegasos.server.frontback.exports.*;

public class Exports {
  public static void fillInExports(int param0, int param1, int param2, Activity activity, Principal person, ApplicationContext applicationContext)
  {
    if( param0 == 1 )
    {
      if( activity.downloads == null )
        activity.downloads = new ArrayList<>();
      activity.downloads.add(new Download("csv"));
    }
  }

  public static SessionExport getExporter(PrivilegeManager privilegeManager, Principal p, Session session, String type, ApplicationContext applicationContext) throws InsufficientPrivilegesException
  {
    if( privilegeManager.hasPrivilegeActivity(p, "export", session) )
    {
      if ("csv".equals(type)) {
        return new CsvExport(session, applicationContext);
      }
      throw new IllegalStateException("Unexpected value: " + type);
    }
    else
      throw new InsufficientPrivilegesException();
  }
}
