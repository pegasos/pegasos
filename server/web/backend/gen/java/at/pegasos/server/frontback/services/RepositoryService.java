package at.pegasos.server.frontback.services;


import at.pegasos.server.frontback.repositories.*;
import org.springframework.stereotype.*;
import lombok.*;
import org.springframework.beans.factory.annotation.*;

@Service
@Getter
public class RepositoryService {
  @Autowired private UserValueDecRepository userValueDecRepository;
  @Autowired private MetricInfoRepository metricInfoRepository;
  @Autowired private TUserRepository tUserRepository;
  @Autowired private TokenRepository tokenRepository;
  @Autowired private TeamMembershipRepository teamMembershipRepository;
  @Autowired private MetricRepository metricRepository;
  @Autowired private UserRolesRepository userRolesRepository;
  @Autowired private UserValueInfoRepository userValueInfoRepository;
  @Autowired private MarkerRepository markerRepository;
  @Autowired private UserValueService userValueService;
  @Autowired private TeamInvitesRepository teamInvitesRepository;
  @Autowired private TempEmailChangeRepository tempEmailChangeRepository;
  @Autowired private SessionsRepository sessionsRepository;
  @Autowired private TeamRepository teamRepository;
  @Autowired private MessageTemplatesRepository messageTemplatesRepository;
  @Autowired private NotificationsRepository notificationsRepository;
  @Autowired private NotificationTypesRepository notificationTypesRepository;
  @Autowired private TeamGroupsRepository teamGroupsRepository;
  @Autowired private TeamWithUsersRepository teamWithUsersRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private TempRegistrationRepository tempRegistrationRepository;
  @Autowired private UserValueIntRepository userValueIntRepository;
  @Autowired private TeamWithGroupWithUsersRepository teamWithGroupWithUsersRepository;
  @Autowired private SessionInfoRepository sessionInfoRepository;
  @Autowired private UserDetailsRepository userDetailsRepository;
  @Autowired private RoleRepository roleRepository;
  @Autowired private GroupRepository groupRepository;
}
// End of generated class

