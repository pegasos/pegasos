package lib;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import perpot.main.Main;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;


public final class Tcp  {

	private static Main main = null;

	
	private static Socket clientSocket=null;
	private static DataOutputStream outToServer=null;
	private static BufferedReader inFromServer=null;
	private static Handler send_Handler=null;
	private static Timer t1;
	private static LinkedList<byte[]> queue = new LinkedList<byte[]>();
	private static int package_counter; 
	public static int connected=0;  // 0=not connected;1=connected
	public static int sending=0;
	private static int sendintervall=1000; 
	
	
	
	public Tcp (Context context, Main main) {
		this.main=main;
		new LooperThread().start();
		start_timer();
	}
	
	
	public static void close() {
    	Log.e("TCP","ON DESTROY");
      if (clientSocket!=null) {
         if (clientSocket.isConnected()) {	
    	     try {
 	    	     outToServer.close();
 	    	     inFromServer.close();
		         clientSocket.close();
	         }
    	     catch (IOException e) {
  	         }
         }
      }
     if (send_Handler!=null) {send_Handler.getLooper().quit();}
   }
  
	
public static synchronized void send_buffer(byte[] p) {
        	queue.add(queue.size(),p);Log.e("DATA","INSERT DATA");
}	


public static void start_timer() {
    t1 = new Timer();
    t1.schedule(new TimerTask() {@Override public void run() {send();}},0,sendintervall); 
}


 public static synchronized void init(int type) {  // 1=open server connection
	    Message m = send_Handler.obtainMessage();
	    m.arg1=0;	
	    m.arg2=type;
	    m.obj = "";
        send_Handler.sendMessage(m);
}

  
 private static synchronized void send() {
     int count=0;
     
     ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
     Message m = send_Handler.obtainMessage();
     
     
     
	 if ((connected==1) && (sending==0)) {
  	       package_counter=queue.size();
    	   while (queue.size()>0) { 
    		         count++;
   			         byte p[] = queue.poll();
    		         //Log.e("FROM_BUFFER","TIME SINCE START:"+time_since_start+" Sensortpye:"+p[2]+" BYTES IN PACK:"+(p[0]+p[1]*256)+" FOOTER:"+(p[p.length-2]+p[p.length-1]*256));
	    		     try {outputStream.write(p);} catch (IOException e) {Log.e("SENDING","CONCAT ERROR!!");}
	       }
    	   
    	   m.arg1=0;	
	       m.arg2=0;
	       Bundle b = new Bundle();
	       b.putByteArray("msg",outputStream.toByteArray());
	       m.setData(b);
    	   if (count>0) {send_Handler.sendMessage(m); Log.e("BUFFER","SIZE:"+count);Log.e("","-------------------------------------------------------------------");}
	 }
     
}  
  

public static synchronized void inject(byte[] msg,int type) {
    Message m = send_Handler.obtainMessage();
    m.arg1=0;	
    m.arg2=type;
    Bundle b = new Bundle();
    b.putByteArray("msg",msg);
    m.setData(b);
    package_counter=1;
	send_Handler.sendMessage(m);
}



  
public static void set_online() {
	connected=1;sending=0;
}
 
  public static class LooperThread extends Thread {
	 public void run() {
		 
    	 Looper.prepare();
    	 send_Handler = new Handler() {
             public void handleMessage(Message msg) {
            	 
      	           int type=msg.arg2;
       	           String ret="";
         	  
              	  
                   if (connected==0) {    // init or not connected
                	   Log.e("SERVER","TRY TO INIT CONNECTION");
     			      try {
     			    	   clientSocket = new Socket("131.130.176.217", 64111); // MC -PRODUKTIV
     			    	   //clientSocket = new Socket("131.130.176.217", 64222); // HTL
     			    	   //clientSocket = new Socket("131.130.176.217", 64322); // CHRISTIAN
     			    	  
     				        outToServer = new DataOutputStream(clientSocket.getOutputStream());
     		                inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
     		                main.http_handler.obtainMessage(1,0,1,"Server connected").sendToTarget();  // message_nr,server_reply,say_it,text
     		                Log.e("SERVER","CONNECTED!");
     		                if (type==2) {sending=1;connected=1;main.http_handler.obtainMessage(7,0,0,"Trying to restart session").sendToTarget();Log.e("SERVER","Trying to restart server!");} else {connected=1;}
     		         }
     			    catch (Exception e) {
     			        //Log.e("SERVER","CONNECTION NOT ESTABLISHED:"+e.getMessage());
     			        if (type==1) 
     			           {main.http_handler.obtainMessage(2,0,0,"Trying to get server connection...!").sendToTarget();init(1);}  // on startup
     			        else
     			           {main.http_handler.obtainMessage(3,0,0,"Server not connected!").sendToTarget();init(2);}    // on running session
     			        connected=0;
     		        }
              	   }
                   else  {
                	     if ((!clientSocket.isConnected()) || (clientSocket.isInputShutdown()) || clientSocket.isOutputShutdown()) {
         		              Log.e("INTERNET","SERVER ERROR!!!");
         			          if (!clientSocket.isConnected()) {main.http_handler.obtainMessage(4,0,0,"Client not connected!").sendToTarget();}
             		          if (clientSocket.isInputShutdown()) {main.http_handler.obtainMessage(5,0,0,"InputShutdown!").sendToTarget();}
             		          if (clientSocket.isOutputShutdown()) {main.http_handler.obtainMessage(6,0,0,"OutputShutdown!").sendToTarget();}
             		        connected=0;
                        }
              	        else {
             	              try {
             	            	   sending=1;
             	         	       //main.log_e("SEND","TRYING TO SEND..");
             	            	   //inFromServer.skip(10000);
             	         	       outToServer.write(msg.getData().getByteArray("msg"));
             			           outToServer.flush();
             			           for (int zu=1;zu<=package_counter;zu++) {
             			        	  ret=""; 
             			              ret = inFromServer.readLine();
             			              if (ret!=null) {main.http_handler.obtainMessage(8,1,1,ret).sendToTarget();}
             			           }   
             			        
             			           
            	        		   //Log.e("RECEIVE",ret);
       			        	       if (ret==null) {init(2);connected=0;main.log_e("INTERNET","NO RESULT RECEIVED!");outToServer.close();inFromServer.close();clientSocket.close();}
       			        	       sending=0;
       			        	       //main.log_e("SEND","SENT!");
             	              } 
           	                  catch (IOException e) {
           	                	  ret="Server verloren,Versuche Verbindungsaufbau:"+e.getMessage();
         	        	          Log.e("INTERNET","IOEXCEPTION:"+e.getMessage());
         	        	          connected=0;
         	        	          sending=0;
                              }
             	              catch (NullPointerException u) {Log.e("NULL","NULL P");}
           	                  finally {
           	                	 if (connected==0) {init(2);}
           	                  }
           	            }
              	   }  
                }  // end voidhandlemessage
            }; // end handler

         Looper.loop();
    	 
     }
// end run
   
     public void destroy() {
    	 
     }
}; 
 
    
 
}   

 


 


 
