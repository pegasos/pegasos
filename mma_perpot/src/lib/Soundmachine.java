package lib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Locale;
import android.os.Vibrator;

import lib.Tcp.LooperThread;
import perpot.main.Main;
import perpot.main.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.SoundPool;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.View.OnLongClickListener;
import android.widget.TextView;

public class Soundmachine implements  OnInitListener,OnPreparedListener {
	
	public Main main = null;
	private SoundPool soundpool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
	private Handler send_Handler=null;
	private TextToSpeech mTts;
	private ToneGenerator snd;
	private boolean speech_output_available = false;
	private Locale language=Locale.GERMANY;
	private TextView status_field;
	private int[] sounds;
	private MediaPlayer mPlayer;
	
	public Soundmachine (Context context, Main main) {
 		 this.main=main;
 		 status_field = (TextView) main.findViewById(R.id.generel_status);  // INIT STATUSFIELD
 		 
 		 mTts = new TextToSpeech(this.main,this);
 		 snd = new ToneGenerator(ToneGenerator.TONE_DTMF_5,100);
	     new LooperThread().start();
	     sounds = new int[20];
	    
	     sounds[1]=R.raw.a1;
	     sounds[2]=R.raw.a2;
	     sounds[3]=R.raw.a3;
	     sounds[4]=R.raw.a4;
	     sounds[5]=R.raw.a5;
	     sounds[6]=R.raw.a6;
	     sounds[7]=R.raw.a7;
	     sounds[8]=R.raw.a8;
	     sounds[9]=R.raw.a9;
	     sounds[10]=R.raw.a10;
	     
	     sounds[11]=R.raw.a11;
	     sounds[12]=R.raw.a12;
	     sounds[13]=R.raw.a13;
	     sounds[14]=R.raw.a14;
	     sounds[15]=R.raw.a15;
	     sounds[16]=R.raw.a16;
	     
	}
	
	  
	  public void beep() {
		     snd.startTone(ToneGenerator.TONE_PROP_BEEP);
	  }

	  public void say_it(String tx) {
		   if (speech_output_available) {
	          mTts.speak(tx,TextToSpeech.QUEUE_FLUSH,null);
		   }   
	  }
	  
	    
	public static int isNumeric(String value) {
	    try {
	       int number = Integer.parseInt(value);
	       return number;
	    }
	    catch(NumberFormatException e) {
	      return -1;
	    }
	   }
	
	
	public void sound(String message,int say_it) {
		 int x=0;
		 Message m = send_Handler.obtainMessage();
		 
		 if ((x=isNumeric(message))>0) {m.arg1=x;} else {m.arg1=0;} 
	     m.arg2=say_it;
	     m.obj=message;
	     send_Handler.sendMessage(m);
	}
	
	
	public class LooperThread extends Thread {
		
		int id=0;
		
		 public void run() {
	    	 Looper.prepare();
	    	 send_Handler = new Handler() {
	             public void handleMessage(Message msg) {
	            	 play(msg);
	                }  // end voidhandlemessage
	            }; // end handler
	         Looper.loop();
	     }
	}
	
	public void play(Message m) {
		
		Vibrator v = (Vibrator) main.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(1000);
		
		if ((m.arg1>0) && (m.arg1<17)) {
  	     try {
  	    	  mPlayer = MediaPlayer.create(main,sounds[m.arg1]); // in 2nd param u have to pass your desire ringtone
  	    	  mPlayer.setOnPreparedListener(this);
  	    	  mPlayer.prepareAsync();
			  } catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			  }
  	 }
	 else	
     if (m.arg1>17) {
    	 main.http_handler.obtainMessage(m.arg1,0,0,"").sendToTarget();
     }	
		
  	 else {
//  		   status_field.setText((String)msg.obj);
  		   if (m.arg2==1) {beep();say_it((String)m.obj);}}
	}
	
	public void onPrepared(MediaPlayer player) {
	    player.start();
	}

	@Override
	public void onInit(int status) {
		
		 if (Locale.getDefault().getLanguage().contains("de")) {language=Locale.GERMANY;} else {language=Locale.ENGLISH;} 
		 int result = mTts.setLanguage(language);
         if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {Log.e("NO","NO SPEAKER!");} else {speech_output_available=true;}

		
	}
	
	public void close () {
		
		
	}
	
}
