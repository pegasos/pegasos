package sensor;

import java.nio.ByteBuffer;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.GpsStatus.Listener;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import perpot.main.Main;
import perpot.main.R;



public class Gps extends Sensor  implements LocationListener{

	
	private Main main;
	private LocationManager mgr;
	private double gps_signal;
	private TextView gps_text;
	private Location last_location=null;

	private float distance_allowed;
	private long timer=0;
	
	private static byte sensor_type=7;  
	private static int sensor_sample_length=24; // LAT=8 Byte,LON=8 Byte,ALT=2 Byte, SPEED=2 Byte, ACCUR=2 Byte, BEARING 2 Byte 
	
	
	public Gps(Main main,byte sensor_nr,int sensor_samples_per_packet,int sampling_rate) {
		super(main,(byte)sensor_type,sensor_nr,sensor_samples_per_packet,sampling_rate,sensor_sample_length);
		
		 this.main=main;
		 mgr = (LocationManager) main.getSystemService(Context.LOCATION_SERVICE);
		 mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,this);
		 mgr.addGpsStatusListener(onGpsStatusChange);
		 Log.e("GPS","STARTED");
		 gps_text= (TextView) main.findViewById(R.id.gps_text);
		 gps_text.setBackgroundColor(Color.RED);
		 gps_text.setTextColor(Color.BLACK);
	}

 

	@Override
	public void set_data(byte[] in) {
		for (int x=0;x<sensor_sample_length;x++) {getCurrent_data()[x]=in[x];}	// data bytes
	}



	@Override
	public void reset(int serial) {
		// TODO Auto-generated method stub
		
	}



	private final Listener onGpsStatusChange=new GpsStatus.Listener()
    {
            public void onGpsStatusChanged(int event)
            {
                    switch( event )
                    {
                    
                            case GpsStatus.GPS_EVENT_STARTED:
                            	    Log.e("GPS","EVENT STARTED");
                                    // Started...
                                    break ;
                            case GpsStatus.GPS_EVENT_FIRST_FIX:
                            	    //main.say_it(1,"GPS connected");
                            	    //gps_text.setTextColor(Color.GREEN);
                                    break ;
                            case GpsStatus.GPS_EVENT_STOPPED:
                            	   //main.log_e("GPS","EVENT_STOPPED");
                                    // Stopped...
                                    break ;
                            
                            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                            	   int count=0,count1=0;
                                   GpsStatus xGpsStatus = mgr.getGpsStatus(null) ;
                                   Iterable<GpsSatellite> iSatellites = xGpsStatus.getSatellites() ;
                                   Iterator<GpsSatellite> it = iSatellites.iterator() ;
                                   gps_signal=0;
                                   while (it.hasNext() ) {
                                	    count++;
                                        GpsSatellite oSat = (GpsSatellite) it.next() ;
                                        //main.log_e("GPS","LocationActivity - onGpsStatusChange: Satellite "+oSat.getPrn()+": " +oSat.getSnr() ) ;
                                        if (oSat.usedInFix()) {count1++;gps_signal=gps_signal+oSat.getSnr();}
                                    }
                                   
                                   if (count1==0) {count1=1;} 
                                   main.gps_data[4]=gps_signal;
                                   //Log.e("GPS", "STRENGTH:"+gps_signal);
                                   main.gps_data[5]=count1;
                                break ;         
                    }
            }
    } ;
	
    
	public void onLocationChanged(Location location) {
		 byte[] buffer = new byte[24];
		 //LAT=5 Byte,LON=5 Byte,ALT=2 Byte, SPEED=2 Byte, ACCUR=2 Byte
		 short alt = (short) (location.getAltitude()*100);
		 short speed=(short) (location.getSpeed()*100);
		 short accur=(short) (location.getAccuracy()*100);
		 short bearing=(short) location.getBearing();
		 long lat=(long) (location.getLatitude()*100000000);
		 long lon=(long) (location.getLongitude()*100000000);
		 
	 
		   //(byte) (((this.sensor_samples_per_packet>>8<<5))+this.sensor_nr);
		 
		 byte[] bytes1 = ByteBuffer.allocate(8).putLong(lat).array();
		 byte[] bytes2 = ByteBuffer.allocate(8).putLong(lon).array();
		 System.arraycopy(bytes1,0,buffer,0,8);  // LAT
		 System.arraycopy(bytes2,0,buffer,8,8);  // LON

		 
		 
		 buffer[16]= (byte) (alt&0xFF);         
		 buffer[17]= (byte) ((alt>>8) & 0xff);  
		 buffer[18]= (byte) (speed&0xFF);       
		 buffer[19]= (byte) ((speed>>8) & 0xff);
		 buffer[20]= (byte) (accur&0xFF); 
		 buffer[21]= (byte) ((accur>>8) & 0xff); 
		 buffer[22]= (byte) (bearing&0xFF); 
		 buffer[23]= (byte) ((bearing>>8) & 0xff);  
		 if ((main.sensors[0]!=null) && (main.logged_in==1)) {main.sensors[0].set_data(buffer);}
		 
		 
		 double speed1= ((Ant_FP) main.sensors[1]).get_speed();
		 
		 if (last_location==null)
		     {
			  //Log.e("GPS ACC:",""+location.getAccuracy()); 
			  if (location.getAccuracy()<=10) {last_location=location;gps_text.setBackgroundColor(Color.GREEN);gps_text.setText("tracking..");timer=System.currentTimeMillis();}
		     }
		 else { 
			   if (location.getAccuracy()>10) {gps_text.setBackgroundColor(Color.RED);gps_text.setText("offline..");} else {gps_text.setBackgroundColor(Color.GREEN);gps_text.setText("tracking..");} 
		       // Genauigkeit unter 10 Meter Abweichung, FP-Speed>0,5ms, mind. 3m distanz
			   if ((location.getAccuracy()<=10)  &&  (speed1>500) && (last_location.distanceTo(location)>3)) {     
					     main.gps_distance+=last_location.distanceTo(location);
					     main.gps_d+=last_location.distanceTo(location);
					     last_location=location;
                         //Log.e("GPS", "STRENGTH:"+gps_signal);
			   }
		       
		} // end else
	}


	public void onProviderDisabled(String arg0) {
		 mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,this);
	}
	

	public void onProviderEnabled(String arg0) {
		 mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,this);		 
	}


	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}

	
	public void stop_gps() {
		mgr.removeUpdates(this);
		mgr.removeGpsStatusListener(onGpsStatusChange);
	}	
	
	
	public static final double roundDouble(double d, int places) {
	    return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10,
	        (double) places);
	}


}

