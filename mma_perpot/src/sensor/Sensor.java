package sensor;

import java.util.Timer;
import java.util.TimerTask;


import perpot.main.Main;
import android.app.Activity;
import android.util.Log;


public abstract class Sensor extends Activity {

	protected byte sensor_nr; // sensor_nr_activity
	private int sensor_sample_length;
	
	private int sampling_rate=1000; // alle 1000 ms = 1 sekunde
	private byte[] current_data;
	public static int header_length=9;
	
	protected static Main main;
    private Timer t1;
    public abstract void reset(int serial);
    
    
    
    
   public Sensor (Main main,byte sensor_type,byte sensor_nr,int sensor_samples_per_packet,int sampling_rate,int sensor_sample_length) {
	   this.sensor_nr=sensor_nr;
	   this.sampling_rate=sampling_rate;
	   this.setSensor_sample_length(sensor_sample_length);
	   this.main=main;
	 
       // HEADER Part one
	   //packet_length=header_length+((sensor_sample_length+2)*this.sensor_samples_per_packet)+2;
	   set_packet_length();
	   setCurrent_data(new byte[100]);
   	   for (int x=0;x<100;x++) {getCurrent_data()[x]=0;}
   }
   
   
   
   private synchronized void set_packet_length() {
	   // packet_length=header_length+((getSensor_sample_length()+2)*this.sensor_samples_per_packet)+2;
	   // packet=new byte[packet_length];
	   
	   
   	   
	   //Log.e("SET_PACKET_LENGTH","SENSOR:"+sensor_type+" PACKET LENTH:"+packet_length);
	   //Log.e("SENSOR SAMPLE LENGTH",""+sensor_sample_length);
   }
   
   public abstract void set_data(byte[] in);
   
   public void reset_register() {
	   
   }
   
   
   private synchronized void add_data() {
	   // Log.d("Sensor add", "" + getCurrent_data()[0]);
	   main.sv.addSensorData(sensor_nr, getCurrent_data());
	   reset_register();
	   /*
		  int i = (int) (System.currentTimeMillis()-last_measurement);
		  //Log.e("DATA TIMESTAMP",i+"");
		  
		  int tmp=header_length+(sample_counter-1)*(getSensor_sample_length()+2);
	      packet[tmp]=(byte) (i  & 0xFF);          // counter	
	      packet[tmp+1]=(byte) ((i >> 8) & 0xFF);  // counter
	      
	   //   current_data[0]=(byte) (tt);
	   //   current_data[1]=(byte)(tt/256);
	      
	   //   Log.e("Add Data",""+(current_data[0]+current_data[1]*256));
	      for (int x=0;x<getSensor_sample_length();x++) {packet[tmp+2+x]=getCurrent_data()[x];}	// data bytes
	      last_measurement=System.currentTimeMillis();
	      */
	} 		
   
   public synchronized void start_timer() {
       t1 = new Timer();
       t1.schedule(new TimerTask() {@Override public void run() {t1_task();}},0,sampling_rate); 
   }
   
   private synchronized void t1_task() {
	  add_data();
   }
 
   
  /* private Runnable On_Time = new Runnable() {
      public synchronized void run() {
    	 //Log.e("BEFORE_BUFFER","Sensortpye:"+packet[2]+" BYTES IN PACK:"+(packet[0]+packet[1]*256)+" FOOTER:"+(packet[packet.length-2]+packet[packet.length-1]*256));
    	 
         main.socket.send_buffer(packet); 
     } // END RUN
  }; // END Runnable
  */
  
  public void stop_timer() {
     try {t1.cancel();} catch (Exception e) {}
  }
   

  
  public int getSensor_sample_length() {
	return sensor_sample_length;
}




public void setSensor_sample_length(int sensor_sample_length) {
	this.sensor_sample_length = sensor_sample_length;
}



public byte[] getCurrent_data() {
	return current_data;
}




public void setCurrent_data(byte[] current_data) {
	this.current_data = current_data;
}

	  

  
}



