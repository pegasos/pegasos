package sensor;

import java.math.BigDecimal;
import java.util.EnumSet;







import perpot.main.Main;
import perpot.main.R;


import android.content.SharedPreferences;


import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;







import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.DataState;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.ICalculatedRrIntervalReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.IHeartRateDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.IPage4AddtDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusLegacyCommonPcc.IManufacturerAndSerialReceiver;

public class Ant_HR extends Sensor {
	
	
    AntPlusHeartRatePcc hr = null;
    private SharedPreferences preferences = null;
    private TextView hr_text ;
    
    
    private int hr_device_id=0,heart_beat;
    
    
    public String sensorname=null;
    protected Short devicenumber=0;
    // protected byte[] data_register;
    protected long last_data_stamp=0;
    protected static int sensor_sample_length=4;
    
    public boolean hr_info=false;    
    private byte hrv_counter;

	
    public Ant_HR (Main main,int sensor_type,byte sensor_nr,int sensor_samples_per_packet,int sampling_rate) {
		super(main,(byte)sensor_type,sensor_nr,sensor_samples_per_packet,sampling_rate,sensor_sample_length);
		
		preferences = PreferenceManager.getDefaultSharedPreferences(main);
		hr_device_id=preferences.getInt("hr_device_id",0);
		reset(hr_device_id);
		hr_text = (TextView) main.findViewById(R.id.hr_text);
	}
	
	
	


	public void reset(int serial) {
	        //Release the old access if it exists
	        if(hr != null) {
	            hr.releaseAccess();
	            hr = null;
	        }
	        
	        AntPlusHeartRatePcc.requestAccess(main,serial,10, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
	 }  // END HANDLE RESET
	 
	 
	 
	 public void subscribeToHrEvents()
	    {
	        hr.subscribeHeartRateDataEvent(new IHeartRateDataReceiver()
	        {
	            @Override
	            public void onNewHeartRateData(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
	                final int computedHeartRate, final long heartBeatCounter,final BigDecimal heartBeatEventTime, final DataState dataState)
	            
	            //    public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                //     final int computedHeartRate, final long heartBeatCount,final BigDecimal heartBeatEventTime, final DataState dataState)	                
	            
                    
	            {
	                main.runOnUiThread(new Runnable()
	                {                                            
	                    @Override
	                    public void run()
	                    {
	                     heart_beat=computedHeartRate;
	                     hr_text.setText(heart_beat+" bpm");
	                     set_data(null);
	                    }
	                });
	            }
	        });



	        hr.subscribePage4AddtDataEvent(new IPage4AddtDataReceiver()
	        {
	            @Override
	            public void onNewPage4AddtData(final long estTimestamp, final EnumSet<EventFlag> eventFlags,
	                final int manufacturerSpecificByte,
	                final BigDecimal timestampOfPreviousToLastHeartBeatEvent)
	            {
	            	main.runOnUiThread(new Runnable()
	                {                                            
	                    @Override
	                    public void run()
	                    {
	                       // tv_previousToLastHeartBeatEventTimeStamp.setText(String.valueOf(timestampOfPreviousToLastHeartBeatEvent));
	                    }
	                });
	            }
	        });



	        hr.subscribeManufacturerAndSerialEvent(new IManufacturerAndSerialReceiver()
	        {
	            @Override
	            public void onNewManufacturerAndSerial(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int manufacturerID,
	                final int serialNumber)
	            {
	            	main.runOnUiThread(new Runnable()
	                {                                            
	                    @Override
	                    public void run()
	                    {
	                        //tv_estTimestamp.setText(String.valueOf(estTimestamp));

	                        //tv_manufacturerID.setText(String.valueOf(manufacturerID));
	                        //tv_serialNumber.setText(String.valueOf(serialNumber));
	                    	
	                    }
	                });
	            }
	        });

	        hr.subscribeCalculatedRrIntervalEvent(new ICalculatedRrIntervalReceiver() {
				
				@Override
				public void onNewCalculatedRrInterval(long estTimestamp, java.util.EnumSet<EventFlag> eventFlags, java.math.BigDecimal calculatedRrInterval, AntPlusHeartRatePcc.RrFlag rrFlag) {
					Log.d("ANT_HR RR", "t: " + estTimestamp + " " + eventFlags + " calcInt " + calculatedRrInterval + " rrFlag" + rrFlag);
					addRR(calculatedRrInterval.intValue());
				}
			});
	    }

	    protected IPluginAccessResultReceiver<AntPlusHeartRatePcc> base_IPluginAccessResultReceiver =  new IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
	    
	        {
	        //Handle the result, connecting to events on success or reporting failure to user.
	        @Override
	        public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode,
	            DeviceState initialDeviceState)
	        {
	             
	            switch(resultCode)
	            {
	                case SUCCESS:
	                	hr = result;
	                    main.log_e("ANT HR",result.getAntDeviceNumber() + ": " + initialDeviceState);
	                    //main.say_it(1,"Heartratemonitor connected");
	                    sensor_nr=2;  // ANT sensors have fixed sensor nr
	                    subscribeToHrEvents();
	                    set_hr_device_id(result.getAntDeviceNumber());    
	                    break;
	                case SEARCH_TIMEOUT:    
                        reset(hr_device_id);
                        main.log_e("ANT HR","SEARCH TIMEOUT");
	                    break;
	                case CHANNEL_NOT_AVAILABLE:
	                	main.log_e("ANT HR","Channel Not Available");
	                    break;
	                case OTHER_FAILURE:
	                	main.log_e("ANT HR","RequestAccess failed. See logcat for details.");
	                    break;
	                case DEPENDENCY_NOT_INSTALLED:
	                    break;
	                case USER_CANCELLED:
	                	main.log_e("ANT HR","USER CANCELLED");
	                    break;
	                case UNRECOGNIZED:
	                	main.log_e("ANT HR","Failed: UNRECOGNIZED. Upgrade Required?");
	                    break;
	                default:
	                	main.log_e("ANT HR","Unrecognized result: " + resultCode);
	                    break;
	            } 
	        }
	        };

	        //Receives state changes and shows it on the status display line
	        protected  IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver = 
	            new IDeviceStateChangeReceiver()
	        {                    
	            @Override
	            public void onDeviceStateChange(final DeviceState newDeviceState)
	            {
	            	main.runOnUiThread(new Runnable()
	                {                                            
	                    @Override
	                    public void run()
	                    {
	                    	main.log_e("ANT HR",hr.getDeviceName() + ": " + newDeviceState);
	                        if(newDeviceState == DeviceState.DEAD) hr = null;
	                    }
	                });
	            }
	        };


	        public void destroy() {
	            if(hr != null)
	            {
	            	hr.releaseAccess();
	            	hr = null;
	            }
	           
	        }


			@Override
			public void set_data(byte[] in) {
				getCurrent_data()[0]=(byte) heart_beat;  // HR LOW     +3
				getCurrent_data()[1]=0;        // HR HIGH
				// getCurrent_data()[2]=0;        // no hrv data so far
				//getCurrent_data()[2]=0;   // HR_VAR LOW 
				//getCurrent_data()[3]=0;   // HR_VAR HIGH
			}
			
			private void addRR(int calcInt) {
				hrv_counter++;
				getCurrent_data()[2]= (byte) hrv_counter;
				Log.d("HRV_COUNTER", "" + hrv_counter);
				getCurrent_data()[1+hrv_counter*2]= (byte) (calcInt&0xFF);        //hrv low
				getCurrent_data()[2+hrv_counter*2]= (byte) ((calcInt>>8) & 0xff); //hrv High
			}
			
			@Override
			public void reset_register() {
				getCurrent_data()[2]=0;        // no hrv data so far
				hrv_counter= 0;
			}

	 
   private void set_hr_device_id(int id) {
		    	hr_device_id=id;
		    	SharedPreferences.Editor editor = preferences.edit();
				editor.putInt("hr_device_id",id); // value to store
				editor.commit();
   }
			    
			
	
}  // END CLASS
