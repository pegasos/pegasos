package sensor;

import java.math.BigDecimal;
import java.util.EnumSet;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;



import com.dsi.ant.plugins.antplus.*;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.ICalorieDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IComputationTimestampReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IDataLatencyReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IInstantaneousCadenceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IInstantaneousSpeedReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.ISensorStatusReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.IStrideCountReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorHealth;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorLocation;
import com.dsi.ant.plugins.antplus.pcc.AntPlusStrideSdmPcc.SensorUseState;
import com.dsi.ant.plugins.antplus.pcc.defines.BatteryStatus;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.IManufacturerIdentificationReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc.IProductInformationReceiver;

import perpot.main.Main;
import perpot.main.R;
import perpot.main.R.id;
import sensor.Ant_FP;

public class Ant_FP extends Sensor {

	
	AntPlusStrideSdmPcc footpod = null;
    private SharedPreferences preferences = null;
    public String sensorname=null;
    private TextView fp_text = null;

    protected byte[] data_register;
    protected long last_data_stamp=0;
    protected static int sensor_sample_length=8;
    
    
    private int fp_calib_int=1000,fp_strides,fp_device_id=0;
    private double fp_speed,fp_distance,fp_distance100_ohne_calib,fp_last_distance=0;
    private long fp_calories=0;
    
    private int[] fp_buffer; 
    private int fp_counter=0;
    
    
    public Ant_FP (Main main,int sensor_type,byte sensor_nr,int sensor_samples_per_packet,int sampling_rate) {
		super(main,(byte)sensor_type,sensor_nr,sensor_samples_per_packet,sampling_rate,sensor_sample_length);
		fp_buffer= new int[5]; for (int x=0;x<5;x++) {fp_buffer[x]=0;}
	
		
		preferences = PreferenceManager.getDefaultSharedPreferences(main);
		fp_device_id=preferences.getInt("fp_device_id",0);
		reset(fp_device_id);
		fp_text = (TextView) main.findViewById(R.id.fp_text);
	}
	
	
    private double get_fp_speed(double speed) {
    	double ret=0;
    	//fp_buffer[fp_counter]=(int)speed;
    	// int tmp=(int) Math.round(speed*3.6);  // Speed in km/h
    	// if (tmp<7) {tmp=7;}
    	// fp_buffer[fp_counter]=(int) (speed*1000*(main.fp_calibration_factor[tmp-7]/1000.00));
    	fp_buffer[fp_counter]=(int) (speed*1000*(fp_calib_int/1000.00));
    	fp_counter++;
    	if (fp_counter==5) {fp_counter=0;}
    	for (int x=0;x<5;x++) {ret+=fp_buffer[x];}
    	
    	//fp_calibration_factor
    	
    	return ret/5;
    }
	
    
    public void destroy() {
    	if(footpod != null) {
            footpod.releaseAccess();
            footpod = null;
        }
    }
	 

	public void reset(final int serial) {
        if(footpod != null) {
            footpod.releaseAccess();
            footpod = null;
        }
        AntPlusStrideSdmPcc.requestAccess(main,serial,10, base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
	}
	
	
	// GET ALREADY CALIBRATED FP_SPEED IN mm/second            
    public void subscribeToEvents()
          {
              footpod.subscribeInstantaneousSpeedEvent(
                  new IInstantaneousSpeedReceiver()
                  {
                      @Override
                      public void onNewInstantaneousSpeed(
                          final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                          final BigDecimal instantaneousSpeed)
                      {
                      	main.runOnUiThread(
                              new Runnable()
                              {                                            
                                  @Override
                                  public void run()
                                  {
                                  	fp_speed=get_fp_speed(instantaneousSpeed.doubleValue());
                                  }
                              });
                      }
						

                  });

//GET ALREADY CALIBRATED FP_DISTANCE IN m
              footpod.subscribeDistanceEvent(
                  new IDistanceReceiver()
                  {
                      @Override
                      public void onNewDistance(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final BigDecimal cumulativeDistance)
                      {
                      	main.runOnUiThread(
                              new Runnable()
                              {                                            
                                  @Override
                                  public void run()
                                  {
                                  	//main.fp_distance=(cumulativeDistance.doubleValue()-main.start_distance)*(main.get_fp_calib_int()/1000);
                                	if (fp_last_distance==0) {fp_last_distance=cumulativeDistance.doubleValue();} else {
                                  	fp_distance+=(cumulativeDistance.doubleValue()-fp_last_distance)*(fp_calib_int/1000.00);
                                  	fp_distance100_ohne_calib+=cumulativeDistance.doubleValue()-fp_last_distance;
                                  	fp_last_distance=cumulativeDistance.doubleValue();
                                  	fp_text.setText(Math.round((fp_speed/1000.00)*360)/100.00d+" km/h \n"+Math.round(fp_distance)+" m");
                                  	//Log.e("FP CALIB",""+cumulativeDistance.doubleValue());
                                  	set_data(data_register);
                                	}
                                  	
                                  }
                              });
                      }
                  });

              
//GET FP_STRIDE_COUNT                    
              footpod.subscribeStrideCountEvent(
                  new IStrideCountReceiver()
                  {
                      @Override
                      public void onNewStrideCount(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final long cumulativeStrides)
                      {
                      	main.runOnUiThread(
                              new Runnable()
                              {                                            
                                  @Override
                                  public void run()
                                  {
                                  	fp_strides=(int) cumulativeStrides;
                                  }
                              });
                      }
                  });

              footpod.subscribeCalorieDataEvent(
                      new ICalorieDataReceiver()
                      {
                          @Override
                          public void onNewCalorieData(
                              final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                              final long cumulativeCalories)
                          {
                              main.runOnUiThread(
                                  new Runnable()
                                  {                                            
                                      @Override
                                      public void run()
                                      {fp_calories=cumulativeCalories;}
                                  });
                          }
                      });                   

//GET FP SERIAL_ID                    
              footpod.subscribeProductInformationEvent(
                      new IProductInformationReceiver()
                      {                                    
                          @Override
                          //public void onNewProductInformation(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int softwareRevision,
                          //    final long serialNumber)
                            public void onNewProductInformation(final long estTimestamp, final EnumSet<EventFlag> eventFlags, final int mainSoftwareRevision,
                                  final int supplementalSoftwareRevision, final long serialNumber)
                          {
                          	main.runOnUiThread(
                                  new Runnable()
                                  {                                            
                                      @Override
                                      public void run()
                                      {
                                      	//main.set_fp_device_id((int) serialNumber);
                                      }
                                  });
                          }
                      });
              
              
//GET FP_STATUS
              footpod.subscribeSensorStatusEvent(new ISensorStatusReceiver()
              {
                  @Override
                  public void onNewSensorStatus(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                      final SensorLocation sensorLocation, final BatteryStatus batteryStatus,
                      final SensorHealth sensorHealth, final SensorUseState useState)
                  {
                  	main.runOnUiThread(
                          new Runnable()
                          {                                            
                              @Override
                              public void run()
                              {
                                  //tv_estTimestamp.setText(String.valueOf(estTimestamp));
                                  //tv_StatusFlagLocation.setText(sensorLocation.toString());
                                  //tv_StatusFlagBattery.setText(batteryStatus.toString());
                                  //tv_StatusFlagHealth.setText(sensorHealth.toString());
                                  //tv_StatusFlagUseState.setText(useState.toString());
                              }
                          });
                  }
              });

          }; // END SUBSCRIBE TO EVENTS

	
	
	
	
	protected IPluginAccessResultReceiver<AntPlusStrideSdmPcc> base_IPluginAccessResultReceiver =
	       new IPluginAccessResultReceiver<AntPlusStrideSdmPcc>()
	            {
                @Override
                public void onResultReceived(AntPlusStrideSdmPcc result,
                    RequestAccessResult resultCode, DeviceState initialDeviceState)
                {
                    switch(resultCode)
                    {
                        case SUCCESS:
                        	footpod = result;
                            main.log_e("FP ANT",(result.getAntDeviceNumber() + ": " + initialDeviceState));
                            //main.say_it(1,"FootPod connected");
                            sensor_nr=1;  // ANT sensors have fixed sensor nr
    	                    subscribeToEvents();
                            set_fp_device_id(result.getAntDeviceNumber());
                            break;
                        case SEARCH_TIMEOUT:    
                            reset(fp_device_id);
                            main.log_e("ANT FP","SEARCH TIMEOUT");
                        case CHANNEL_NOT_AVAILABLE:
                        	main.log_e("FP ANT","Channel Not Available");
                            break;
                        case OTHER_FAILURE:
                        	main.log_e("FP ANT","RequestAccess failed. See logcat for details.");
                            break;
                        case DEPENDENCY_NOT_INSTALLED:
                        	main.log_e("FP ANT","ANT NOT INSTALLED");
                            break;
                        case USER_CANCELLED:
                        	main.log_e("FP ANT","Cancelled. Do Menu->Reset.");
                            break;
                        case UNRECOGNIZED:
                        	main.log_e("FP ANT","Failed: UNRECOGNIZED. Upgrade Required?");
                            break;
                        default:
                        	main.log_e("FP ANT","Unrecognized result: " + resultCode);
                            break;
                    } 
                }
	      };

                
        protected  IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver = new IDeviceStateChangeReceiver()
                {
                    @Override
                    public void onDeviceStateChange(final DeviceState newDeviceState)
                    {
                        main.runOnUiThread(
                            new Runnable()
                            {                                            
                                @Override
                                public void run()
                                {
                                   // tv_status.setText(sdmPcc.getDeviceName() + ": " + newDeviceState);
                                }
                            });
                    }
                }; 
    
	
	




@Override
public void set_data(byte[] in) {
	getCurrent_data()[0]=(byte) (fp_speed);   // SPEED_MM LOW
	getCurrent_data()[1]=(byte) (fp_speed/256);   // SPEED_MM HIGH 
	getCurrent_data()[2]=(byte) (fp_distance);   // DIST_M 
	getCurrent_data()[3]=(byte) (fp_distance/256);   // DIST_M HIGH 0....25500
	getCurrent_data()[4]=(byte) (fp_strides%255);   // STRIDES LOW 0....255
	getCurrent_data()[5]=0;   // STRIDES HIGH 0....255
	getCurrent_data()[6]=(byte) (fp_calories);   // CALORIES LOW
	getCurrent_data()[7]=(byte) (fp_calories/256);   // CALORIES HIGH
	//Log.e("FPFPFPFP","DISTANCE:"+fp_distance+ " SPEED:"+fp_speed);
}
	

   public void set_fp_device_id(int id) {
	   fp_device_id=id;
	   SharedPreferences.Editor editor = preferences.edit();
	   editor.putInt("fp_device_id",id); // value to store
	   editor.commit();
   }



   public synchronized int get_fp_calib_int() {
      return fp_calib_int;
   }
   
   
   public synchronized void set_fp_calib_int(int x) {
	      fp_calib_int=x;
	   }
   
   public double get_distance() {
	   return fp_distance;
   }
   
   public double get_speed() {
	   return fp_speed;
   }
   
   public void reset_data() {
	   fp_distance = 0.00;
       fp_distance100_ohne_calib = 0.00;
       fp_strides = 0;
       fp_last_distance=0;
   }
   
}  // END CLASS








