package perpot.main;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ResponseCache;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import sensor.Ant_FP;
import sensor.Ant_HR;
import sensor.Gps;
import sensor.Sensor;
import lib.Soundmachine;
import lib.Tcp;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.TextView;
import at.univie.mma.serverinterface.ResponseCallback;
import at.univie.mma.serverinterface.ServerInterface;
import at.univie.mma.serverinterface.ServerSensor;
import at.univie.mma.serverinterface.response.ServerDisconnectedMessage;
import at.univie.mma.serverinterface.response.ServerReconnected;
import at.univie.mma.serverinterface.response.ServerResponse;

@SuppressLint("Override")
public class Main extends Activity implements OnClickListener {
	
	// STATIC SETUP
	private final static int send_period=1000;
	private final static boolean log=true;
	private final static String version="V2";
	
	private static final String SERVER_HOST = "131.130.176.217";
	private static final int SERVER_PORT = 64111;
	private static final int SERVER_SENDDELAY = 1000;
	
	// CLASSES
	private Gps gps;
	public Soundmachine soundmachine;
    public Sensor sensors[];
    public int sensor_number_activity=1;
    public int sensor_index=1;
    private Timer t1;
	
	// UI
	public Button login_button;
	public Button skip_button;
	public Button start_button;
	private int next_action=0; 
	private TextToSpeech mTts;
	private boolean speech_output_available = false;
	private Locale language=Locale.GERMANY;
	
	private ToneGenerator snd;
	private TextView title;
	private EditText username,password,distance;
	private SharedPreferences preferences = null;
	private Integer fp_calib_int;

	public int logged_in=0;
	public int beenden_flag=0;
    //public double calib_faktor=0;  
    public int activity_c=0;
    public TextView status_field;
    private Chronometer time_online;
    
    
    // SENSOR DATA	
	public double gps_data[],heart_var=-1;
	public float gps_distance=0,gps_d=0;
	public boolean gps_calib=false;
	public int[] usb_sensor_lookaside;
	
	public static long starttime;
  
	public int[] fp_calibration_factor;
	/*
	public class Sensor_description {
		public int nr;
        public int type;
        public String description;
        public int sampling_rate;
        public int samples_per_packet;
	}
	*/
	
//	public Sensor_description sensor_description[];  // JUST FOR PAIRING !!!
	
	public ServerInterface sv;
	

	
	public boolean onPrepareOptionsMenu (Menu menu) {
        		
		 if (logged_in>0) {
		     menu.findItem(0).setVisible(false);
		     menu.findItem(1).setVisible(false);
		     menu.findItem(2).setVisible(false);
		     menu.findItem(3).setVisible(false);
		     menu.findItem(4).setVisible(false);
		     menu.findItem(5).setVisible(false);
		     menu.findItem(100).setVisible(true);  // lauf beenden
		     menu.findItem(101).setVisible(false);  // programm beenden
	  	 }
		 else {
			 menu.findItem(0).setVisible(true);
			 menu.findItem(1).setVisible(true);
			 menu.findItem(2).setVisible(true);
		     menu.findItem(3).setVisible(true);
		     menu.findItem(4).setVisible(true);
		     menu.findItem(5).setVisible(true);
		     menu.findItem(100).setVisible(false);  // lauf beenden
		     menu.findItem(101).setVisible(true);  // programm beenden
		 }
		
		return true;
	}
	
	class ServerCallback implements ResponseCallback {
		@Override
		public void messageRecieved(ServerResponse r) {
			if( r != null )
			{
				if( r instanceof ServerReconnected ) {
					Log.e("Message recieved", "Reconnected!");
					Message msg= http_handler.obtainMessage();
					msg.what= 8;
					msg.arg1= 1;
					msg.arg2= 1;
					msg.obj= "2;1;SI_reconnected";
					msg.sendToTarget();
				}
				else if( r instanceof ServerDisconnectedMessage ) {
					Log.e("Message recieved", "Disconnected");
				}
				else
				{
					Log.e("Message recieved", r.getRaw() + " login: " + sv.isLoggedIn());
					Message msg= http_handler.obtainMessage();
					msg.what= 8;
					msg.arg1= 1;
					msg.arg2= 1;
					msg.obj= r.getRaw();
					msg.sendToTarget();
				}
			}
			else {
				Log.e("Message recieved", "empty");
			}
		}
	};
	
	ServerCallback callback= new ServerCallback();
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);
        
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        snd = new ToneGenerator(ToneGenerator.TONE_DTMF_5,100);       // INIT BEEP
        title = (TextView) findViewById(R.id.textView3);  // INIT TITLE
        title.setText(title.getText()+" "+version);
        
        time_online = (Chronometer) findViewById(R.id.time_online);
        
        status_field = (TextView) findViewById(R.id.generel_status);  // INIT STATUSFIELD
                
        login_button = (Button) findViewById(R.id.button_login);
        login_button.setOnClickListener(this);
        login_button.setVisibility(View.INVISIBLE);
        
        username= (EditText) findViewById(R.id.edit_username);
        username.setText(preferences.getString("username",""));
        
        //username.setText("u");
        password= (EditText) findViewById(R.id.edit_password);
        password.setText(preferences.getString("password",""));
        
        // distance field
        distance= (EditText) findViewById(R.id.edit_distance);
        
        activity_c=120;

        // Set DISPLAY TUNED ON !!
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

       
        /*
        sensor_description = new Sensor_description[20]; for (int x=0;x<20;x++) {
        	sensor_description[x]=new Sensor_description();
        	sensor_description[x].description="optional Sensor nummer "+x;
        	sensor_description[x].nr=x;
        	sensor_description[x].samples_per_packet=100;
        	sensor_description[x].sampling_rate=10;
          }
         */
        fp_calibration_factor = new int[14];for (int x=0;x<14;x++) {fp_calibration_factor[x]=1000;}
        sensors=new Sensor[31];   for (int x=1;x<31;x++) {sensors[x]=null;}
        usb_sensor_lookaside=new int[32];for (int x=0;x<31;x++) {usb_sensor_lookaside[x]=-1;}
        gps_data = new double[7];
        
        soundmachine=new Soundmachine(this,this);
        
        // Create Interface to server and attempt connection
        sv= ServerInterface.getInstance(SERVER_HOST, SERVER_PORT, SERVER_SENDDELAY); 

        sv.addResponseCallback(callback);
        
        if( sv.connect() ) //TODO: check for result
        	http_handler.obtainMessage(1,0,1,"Server connected").sendToTarget(); 
        
        sensors[0] = new Gps(this,(byte)0,1,1000);
        open_channels();  // OPEN ANT+; ARDUINO;BLUETOOTH LE LISTENING FOR SENSORS
        
        // Attach sensors to server interface
        sv.setSensorCount(3);
        sv.attachSensor(0, ServerSensor.TYPE_GPS, 1);
        sv.attachSensor(2, ServerSensor.TYPE_HR, 1);
        sv.attachSensor(1, ServerSensor.TYPE_FOOTPOD, 1);
    }

   

private void setHasOptionsMenu(boolean b) {
		// TODO Auto-generated method stub
		
	}

public void say_it(String tx) {
	status_field.setText(tx);
}

    

private void open_channels() {   // CALLED WHEN LOGIN RETURNED SUCESSFULLY
	//sensors[0]=new Gps(this,(byte)0,1,1000);
    sensors[2]= new Ant_HR(this,ServerSensor.TYPE_HR,(byte)2,1,1000);
    sensors[1]= new Ant_FP(this,ServerSensor.TYPE_FOOTPOD,(byte)1,1,1000);
    sensor_index=3;	
    sensor_number_activity=1;
}
    
    
	public boolean onCreateOptionsMenu(Menu menu){
    	menu.add(0,0,0,R.string.sensor_verbinden);
    	menu.add(0,1,1,R.string.footpod_einstellen);
    	menu.add(1,2,2,R.string.perpot_kalibrierung1);
    	menu.add(1,3,3,R.string.perpot_kalibrierung2);
    	menu.add(1,4,4,R.string.perpot_kalibrierung3);
    	menu.add(1,5,5,R.string.perpot_lauf);
    	menu.add(2,100,100,R.string.lauf_beenden);
    	menu.add(2,101,101,R.string.programm_beenden);
    	
    	return true;
   	} 
	
	
	
    
    public boolean onOptionsItemSelected (MenuItem item){

    	switch (item.getItemId()){

    	case 0:  // Pairing
    		      sensors[1].reset(0);
    		      sensors[2].reset(0);
		return true;
   
    	case 1:
    	 // FP CALIB
    		soundmachine.sound("FootPod Kalibrierungslauf wurde gew�hlt",1);
    		say_it("FootPod Kalibrierungslauf wurde gew�hlt");
    		fp_calib_int=1000;
    		((Ant_FP) sensors[1]).set_fp_calib_int(fp_calib_int);
    		//activity_c=150;
    		//activity_c=144;
    		activity_c=145;
	    return true;
    	
    	case 2:     // PP-Calib1
    		say_it("Kalibierungslauf Anf�nger wurde gew�hlt!");
    		soundmachine.sound("Kalibierungslauf Anf�nger wurde gew�hlt!",1);
    		activity_c=131;
    		store_username_password();
        return true;

    	case 3:     // PP-Calib2
    		say_it("Kalibierungslauf Fortgeschrittene wurde gew�hlt!");
    		soundmachine.sound("Kalibierungslauf Fortgeschrittene wurde gew�hlt!",1);
    		activity_c=132;
    		store_username_password();        
    	return true;
    	case 4:     // PP-Calib3
    		say_it("Kalibierungslauf Profis wurde gew�hlt!");
    		soundmachine.sound("Kalibierungslauf Profis wurde gew�hlt!",1);
    		activity_c=133;
    		store_username_password();        
        return true;


    	case 5:     // PP-Lauf
    		say_it("PerPot Lauf wurde gew�hlt, bitte nicht vergessen die Laufdistanz einzugeben!");
    		soundmachine.sound("PerPot Lauf wurde gew�hlt, bitte nicht vergessen die Laufdistanz einzugeben!",1);
    		activity_c=110;
    		store_username_password();
        return true;

        
    	case 100:
    		showDialog(5);
    		soundmachine.sound(getResources().getString(R.string.lauf_wirklich_beenden),1);
    	return true;
    	
        
    	case 101:
    		showDialog(1);
    		soundmachine.sound(getResources().getString(R.string.programm_wirklich_beenden),1);
    	return true;
    	}
    	
    	return false;
    }
     
    public void onBackPressed() {
    	//soundmachine.sound("Programm wirklich beenden?",1);
    	//showDialog(1);
	 }
    
    @Override
    public void onDestroy() {
    	log_e("DESTROY","DESTROY START");
    	close_stuff();
    }
    
    
    public void close_stuff() {
    	log_e("DESTROY","DESTROY START");
    	//super.onDestroy();
    	
    	// Stop Server-Interface
    	sv.stopSending(true);
    	sv.close();
    	
    	
    	for (int x=0;x<31;x++) {if (sensors[x]!=null) {sensors[x].stop_timer();sensors[x]=null;}}
     	if (mTts != null) {mTts.shutdown();}
     	soundmachine.close();
		finish();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	Log.e("RESUME","RESUME");
    }

    
    public final Handler http_handler = new Handler() {
	   @Override
       public void handleMessage(Message msg) {
		   
  	       int message_nr=msg.what;
  	       int action=-1;
  	       int result_code=0;  
  	       int server_reply  =msg.arg1; //0=no, 1=yes, is this message from the Server or directly from the TCP Module
  	       int say_it      =msg.arg2;   //0=no, 1=yes
  	       String return_data     =(String)msg.obj;
  	       String tmp[]=null;
  	       
  	       if (server_reply==1) {       // reply from server
  	    	    tmp = return_data.split("[;]");
   	            action=Integer.valueOf(tmp[0]).intValue();      // info on type of received message
   	            result_code=Integer.valueOf(tmp[1]).intValue();  // 1--> evertythin ok, 0--> error happened  
   	            return_data=tmp[2];  // 1--> evertythin ok, 0--> error happened
  	       }
  	       
  	      //if ((say_it==1) && (action<1000)) {sayit(1,return_data+"dd");}
  	      if (((say_it==1) && (server_reply==0))) {soundmachine.sound(return_data,1);}
  	      if ((message_nr==1) && (logged_in==0)) {login_button.setVisibility(View.VISIBLE);}    // server connected
  	      if (message_nr==2) {login_button.setVisibility(View.INVISIBLE);}  // server not connected
  	      if (message_nr==3) {login_button.setVisibility(View.INVISIBLE);}  // server not connected
  	      if (message_nr==7) // restart (new session bereits aktiv war dann restart oder nur init wenn bereits beendet, wenn noch nicht aktiv dann start !!!) 
	        {
	    	  if ((logged_in==0) && (beenden_flag==0)) {login(2);} else  
	          if ((logged_in==0) && (beenden_flag==1)) {sv.connect();} else {login(3);}
	        }
  	     
  	      
  	      if (message_nr==20) {    // SET COLOR ROT
            findViewById(R.id.tableLayout1).setBackgroundColor(Color.RED);
            findViewById(R.id.gps_table).setBackgroundColor(Color.RED);
            findViewById(R.id.tableRow56).setBackgroundColor(Color.RED);
          }
  	      
  	      if (message_nr==19) {    // SET COLOR GELB
            findViewById(R.id.tableLayout1).setBackgroundColor(0xfffdf8a0);
            findViewById(R.id.gps_table).setBackgroundColor(0xfffdf8a0);
            findViewById(R.id.tableRow56).setBackgroundColor(0xfffdf8a0);
          }
  	      
          if (message_nr==18) {    // SET COLOR GREEN
  	            findViewById(R.id.tableLayout1).setBackgroundColor(0xFF099109);
  	            findViewById(R.id.gps_table).setBackgroundColor(0xFF099109);
  	            findViewById(R.id.tableRow56).setBackgroundColor(0xFF099109);
          }
  	      
  	    
	       switch (action) {
	           case 0:  // login returned
	        	      if (result_code==1) {
	        	    	  if ((activity_c!=150) && (activity_c!=144)) { 	        	    	
	        	    	     fp_calib_int = Integer.parseInt(tmp[4]);  // GET STANDARD-FP-CALIB_FAKTOR FROM SERVER
	        	    	     ((Ant_FP) sensors[1]).set_fp_calib_int(fp_calib_int);
	        	    	     Log.e("FP-CALIB_FAKTOR:",fp_calib_int+"");
	        	    	     
	        	    	     for (int x=0;x<14;x++)  // GET FP-CALIB_FAKTOR FROM SERVER
	        	    	      {
	        	    	      fp_calibration_factor[x]=Integer.valueOf(tmp[x+5]);
	        	    	      Log.e("FP-CALIB_FAKTOR AT "+(x+7)+" Km/h:",Integer.valueOf(tmp[x+5])+"");
	        	    	     }
	        	    	     
	        	    	  }
	        	    	  
	        	    	  
	        	    	  
	        	    	/*
                        for (int i=1;i<=sensor_count;i++) {
                        	sensor_description[i].nr=Integer.valueOf(tmp[(5+(i-1)*5)]);
                        	sensor_description[i].type=Integer.valueOf(tmp[(6+(i-1)*5)]);
                        	sensor_description[i].description=tmp[(7+(i-1)*5)];
                        	sensor_description[i].sampling_rate=Integer.valueOf(tmp[(8+(i-1)*5)]);
                        	sensor_description[i].samples_per_packet=Integer.valueOf(tmp[(9+(i-1)*5)]);
                        }
                        */
	        	    	
                        if (next_action==2) {
                        	logged_in=1;
                        	start_session();	 
                        }

	        	    
	        	        if (next_action==3) {   // RESTART SESSION 
	        	        	 //  RESTART SEQUENCE
	        	        	logged_in=1;
	        	            sv.restartSession();
	        	        }  
	        	      } else { // login failed
	        	    	  login_button.setVisibility(View.VISIBLE);
	        	    	  soundmachine.sound(tmp[2],1);
	        	      }
	           break;
	           
	           case 1: if (result_code==1) {   // Start session returned
	        	       // soundmachine.sound("Neue Session wurde gestartet!",1);
	        	       sv.restartSession();
	        	       
	        	   
	        	       if (activity_c==131) {soundmachine.sound("Lauf wird automatisch bei Erreichen von 6km/h gestartet. Steigerung nach jeweils 3 Minuten um 1km/h.",1);} else 
	        	       if (activity_c==132) {soundmachine.sound("Lauf wird automatisch bei Erreichen von 7km/h gestartet. Steigerung nach jeweils 3 Minuten um 1.5km/h.",1);} else
	        	       if (activity_c==133) {soundmachine.sound("Lauf wird automatisch bei Erreichen von 8km/h gestartet. Steigerung nach jeweils 3 Minuten um 2km/h.",1);} else 
	        	       //if (activity_c==110) {soundmachine.sound("Lauf wird automatisch bei Erreichen von 8km/h gestartet.",1);} 
	        	   
	        	       //time_online.setBase(starttime/1000);
	        	       login_button.setVisibility(View.INVISIBLE);
	        	       time_online.setBase(SystemClock.elapsedRealtime());
	        	       time_online.start();
	        	       distance.setEnabled(false);
	        	       username.setEnabled(false);
	        	 	   password.setEnabled(false);
	        	       for (int x=0;x<31;x++) {if (sensors[x]!=null){sensors[x].start_timer();}
	        	       } 
        	       } else {
        	    	   soundmachine.sound("session error!"+tmp[2],1);
        	       }
               break;
	
	           case 2: if (result_code==1) {
	        	   soundmachine.sound(getResources().getString(R.string.lauf_wird_fortgesetzt),1);
        	       login_button.setVisibility(View.INVISIBLE);
        	       // socket.set_online(); TODO: any action required here?
        	       //for (int x=0;x<31;x++) {if (sensors[x]!=null) {sensors[x].start_timer();}}
    	       } else {
    	    	   soundmachine.sound(return_data,1);
    	       }
               break;
               
	           case 3: if (result_code==1) {
	        	   soundmachine.sound(return_data,1);
	           } 
               break;
               
	           case 4: if (result_code==1) {soundmachine.sound(return_data,1); lauf_beenden();} 
               break;
               
	           
	           
	           case 1000:   // DO NOTHING
	           break;
	       }
	  }
	   
    };  // END http_handler

    
    
    public void store_username_password() {
    	 SharedPreferences.Editor editor = preferences.edit();
		 editor.putString("password",password.getText().toString()); // value to store
		 editor.putString("username",username.getText().toString()); // value to store
		 editor.commit();
    }

	@Override
	public void onClick(View v) {
		Log.e("onClick", "" + v.getId() + " " + R.id.button_login);
		byte[] buffer = new byte[126];
		 soundmachine.sound("",1);
		 store_username_password();
		 switch (v.getId()) {
			case R.id.button_login:
				 login_button.setVisibility(View.INVISIBLE);
			     login(2);  // 2=LOGIN
			     beenden_flag=0;
			break;
	     }
	}
	

public void login(int next) {
	int club_usage=1;
	int language=0;
	
	next_action=next;  // 2=LOGIN;3=RESTART
	
	// START
	if (next==2) {starttime= System.currentTimeMillis();}
	
	sv.login(username.getText().toString(), 
			password.getText().toString(), 
			"" + club_usage, 
			"" + activity_c, 
			language);
}	






public void start_session() {
	
	String tmp_d="0";
	int param2=0;
	
    ((Ant_FP) sensors[1]).reset_data();  // THIS OK SINCE ANT_FP always has sensors[2]
    gps_distance=0;
    gps_d=0;
	
	if (distance.getText().length()>0) {tmp_d=distance.getText().toString();}  
	
    sv.startSending();
    sv.startSensorTime(starttime);
    if ((tmp_d!="0") && ((activity_c==150) || (activity_c==110))) {  // Distanz eingegeben & FP-Kalibrierungslauf // PP Lauf  
    	param2=Integer.parseInt(tmp_d);
    	sv.startActivity(activity_c, param2);
    }
    else
    	sv.startActivity(activity_c);
    
    Log.e("START SESSION","Start Session");
}





  

    
    public void log_e(String left,String right) {
    	if (log) {Log.e(left,right);}
    }


    
    public void lauf_beenden() {
       //soundmachine.sound("1",1);
    
    	
       login_button.setVisibility(View.VISIBLE);
 	   logged_in=0;
 	   activity_c=120;
 	   for (int x=0;x<31;x++) {if (sensors[x]!=null) {sensors[x].stop_timer();}}
 	   
 	   distance.setEnabled(true);
 	   username.setEnabled(true);
 	   password.setEnabled(true);
 	   time_online.stop();
 	   beenden_flag=1;
    }
    

   
 

   
   protected Dialog onCreateDialog(int id) 
   {
       switch (id) 
       {
           case 1: 
        	       return new AlertDialog.Builder(this)
                                   .setIcon(R.drawable.icon)
                                   .setTitle(R.string.programm_wirklich_beenden)
                                   .setPositiveButton("Ok",new DialogInterface.OnClickListener() 
                                   {
                                       public void onClick(DialogInterface dialog,int whichButton)
                                       {        
                                    	   close_stuff();
                                           System.exit(0);
                                    	   //socket.send(1,".");
                                       }
                                    }).setNegativeButton(R.string.abbrechen,new DialogInterface.OnClickListener() 
                                    {
                                           public void onClick(DialogInterface dialog,int whichButton) 
                                           {
                                               //do nothing next time
                                           }
                                     }).create();
        
        	       
           case 5: 
    	       return new AlertDialog.Builder(this)
                               .setIcon(R.drawable.icon)
                               .setTitle(R.string.lauf_wirklich_beenden)
                               .setPositiveButton("Ok",new DialogInterface.OnClickListener() 
                               {
                                   public void onClick(DialogInterface dialog,int whichButton) {lauf_beenden();}
                                }).setNegativeButton(R.string.abbrechen,new DialogInterface.OnClickListener() 
                                {
                                 public void onClick(DialogInterface dialog,int whichButton)  {}
                                }).create();   
        	   
       }
       return null;
   }



   
  
   
   
 
   
   
	
}